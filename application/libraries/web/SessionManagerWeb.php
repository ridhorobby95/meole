<?php

defined('BASEPATH') OR exit('No direct script access allowed');

// inisialisasi
session_start();

class SessionManagerWeb {

    const INDEX = 'meole';

    /**
     * Mendapatkan data session
     * @param string $index
     * @return mixed
     */
    protected static function get($index) {
        return $_SESSION[self::INDEX][$index];
    }

    /**
     * Mengubah data session
     * @param string $index
     * @param mixed $data
     */
    protected static function set($index, $data) {
        return $_SESSION[self::INDEX][$index] = $data;
    }

    /**
     * Menghapus data session
     */
    public static function destroy() {
        unset($_SESSION[self::INDEX]);
    }

    /**
     * Mengambil data flash
     * @return mixed
     */
    public static function getFlash() {
        $flash = $_SESSION[self::INDEX]['flash'];
        unset($_SESSION[self::INDEX]['flash']);

        return $flash;
        ;
    }

    /**
     * Menyimpan data untuk di-flash kemudian
     * @param mixed $data
     */
    public static function setFlash($data) {
        $_SESSION[self::INDEX]['flash'] = $data;
    }

    /**
     * Cek apakah sudah login
     * @return bool
     */
    public static function isAuthenticated() {
        $auth = self::get('auth');

        return empty($auth['isauthenticated']) ? false : true;
    }

    /**
     * Cek apakah sudah administrator
     * @return bool
     */
    public static function isAdministrator() {
        $auth = self::get('auth');
        //die($auth['role']);
        return ($auth['role'] == Role::ADMINISTRATOR || $auth['role']==Role::SUPER_ADMINISTRATOR) ? true : false;
    }

    public static function isSuperAdministrator() {
        $auth = self::get('auth');
        //die($auth['role']);
        return ($auth['role'] == Role::SUPER_ADMINISTRATOR) ? true : false;
    }

    /**
     * Cek apakah sudah management
     * @return bool
     */
    public static function isManagemement() {
        $auth = self::get('auth');

        return ($auth['role'] == Role::STAFF) ? true : false;
    }

    public static function isStaff() {
        $auth = self::get('auth');

        return ($auth['role'] == Role::STAFF || $auth['role'] == Role::ADMINISTRATOR || $auth['role']==Role::SUPER_ADMINISTRATOR) ? true : false;
    }

    public static function isOperator() {
        $auth = self::get('auth');

        return ($auth['role'] == Role::OPERATOR) ? true : false;
    }

    public static function isWarga() {
        $auth = self::get('auth');

        return ($auth['role'] == Role::WARGA) ? true : false;
    }
    
    /**
     * Cek apakah developer
     * @return bool
     */
    public static function isDeveloper() {
        $auth = self::get('auth');

        return (SessionManagerWeb::getUserName() == 'danang') ? true : false;
    }

    /**
     * Mendapatkan userid
     * @return string
     */
    public static function getUserID() {
        $auth = self::get('auth');

        return $auth['userid'];
    }

    /**
     * Mendapatkan username
     * @return string
     */
    public static function getUserName() {
        $auth = self::get('auth');

        return $auth['username'];
    }

    /**
     * Mendapatkan name
     * @return string
     */
    public static function getName() {
        $auth = self::get('auth');

        return $auth['name'];
    }

    public static function getRole() {
        $auth = self::get('auth');

        return $auth['role'];
    }

    public static function getUnitkerjaID() {
        $auth = self::get('auth');

        return $auth['unitkerja_id'];
    }

    public static function getUnitkerjaName() {
        $auth = self::get('auth');

        return $auth['unitkerja_name'];
    }

    public static function getDinasID() {
        $auth = self::get('auth');

        return $auth['dinas_id'];
    }

    public static function getDinasName() {
        $auth = self::get('auth');

        return $auth['dinas_name'];
    }

    /**
     * Mendapatkan foto
     * @return string
     */
    public static function getPhoto($size='48') {
        $foto = self::get('photo');
        if (strstr($foto, 'nopic.png'))
            return $foto;
        else
            return $foto .'/' . $size;
    }

    /**
     * Menyimpan data user
     * @param array $user
     */
    public static function setUser($user) {
        $data = array();
        $data['userid'] = $user['id'];
        $data['username'] = $user['username'];
        $data['name'] = $user['name'];
        $data['role'] = $user['role'];
        $data['unitkerja_id'] = $user['unitkerja_id'];
        $data['dinas_id'] = $user['id_dinas'];
        $data['dinas_name'] = dbGetOne("select name from unitkerja where idunit=".$user['id_dinas']);
        $data['unitkerja_name'] = $user['unitkerja_name'];
        $data['jabatan_id'] = $user['jabatan_id'];
        $data['jabatan_name'] = $user['jabatan_name'];
        $data['isauthenticated'] = true;

        self::set('auth', $data);

        // sekalian foto
        self::setPhoto($user);
    }

    /**
     * Menyimpan data foto
     * @param array $user
     */
    public static function setPhoto($user) {
        if (isset($user['photo']))
            $data = site_url(WEB_Controller::CTL_PATH.'thumb/watermark/'.$user['id']);
        else
            $data = base_url('assets/web/img/nopic.png');

        self::set('photo', $data);
    }

    /**
     * Menyimpan data pesan setelah aksi untuk di-flash
     * @param bool $ok
     * @param string $msg
     */
    public static function setFlashMsg($ok, $msg) {
        $data = array();
        $data['srvok'] = $ok;
        $data['srvmsg'] = $msg;

        self::setFlash($data);
    }

}
