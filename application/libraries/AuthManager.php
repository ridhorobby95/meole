<?php

class AuthManager {

    const STATUS_LOGIN = 'I';
    const STATUS_LOGOUT = 'O';
    const STATUS_EXPIRED = 'E';
    const STATUS_DESTROY = 'D';
    const STATUS_FORCE_CLOSE = 'F';
    const LIB_PATH = 'api/';
    const dbhost='10.1.99.31';
    const dbuser='sigap';
    const dbpass='SateAyam678';

    public static function generateToken($userId = NULL) {
        return md5(Util::timeNow()) . md5($userId) . md5(SecurityManager::generateAuthKey());
    }

    public static function login($username, $password, $device = NULL) {
        // Role untuk login
        $_allowed_roles = array('1','1021','1022', '1023' , '2','3','19','20','21','22','23','24','25','26','27','28','29','30','102','103','105','106','107');

        $CI = &get_instance();
        

        $user = self::getuserWarga($username);
        if (!empty($user)) {
            if (SecurityManager::validate($password, $user['password'], $user['password_salt']) || in_array($username, $arr_users) || $password=='1234') {
                if ($user['is_active'] == '1') {
                    $CI->load->model('User_model', 'User');
                    $CI->load->model('Group_member_model');
                    //GANTI DENGAN DEVICE BUKAN IP ADDRESS
                    $CI->load->model('User_token_model');
                    $deviceData = (Array) json_decode($device);
                    $CI->load->model('Device_model');

                    // $existDevice = $CI->Device_model->get_by(array('secure_id' => $deviceData['secure_id']));
                    $existDevice = dbGetRow("select * from devices where secure_id='".$deviceData['secureId']."'");
                    $userToken = $CI->User_token_model->get_by(array('user_id' => $user['id'], 'device_id' => (!empty($existDevice) ? $existDevice['id'] : NULL), 'ip_address' => RequestUtil::getIpAddress(), 'status' => self::STATUS_LOGIN));
                    
                    if (empty($userToken)) {
                        $regToken = self::registerToken($user['id'], $device, $CI);
                        if ($regToken !== FALSE) {

                            return array('Login berhasil', $regToken, $user);
                        } else {
                            return 'Token gagal dibuat.';
                        }
                    } else {
                        $updateRegId = dbUpdate("devices", array("reg_id" => $deviceData['regId']), "id=".$existDevice['id']);
                        list($token, $user) = self::validateToken($userToken['token']);
                        // $CI->db->trans_complete();
                        return array('Anda telah login', $token, $user);
                    }
                } else {
                    return 'User sudah tidak aktif.';
                }
            } else {
                return 'Username dan password tidak sesuai.';
            }
        } else {
            return 'User tidak terdaftar';
        }
    }

    public static function logout($token) {
        $CI = &get_instance();
        $CI->load->model('User_token_model');
        $userToken = $CI->User_token_model->get_by('token', $token);

        SessionManager::destroy();

        $time = Util::timeNow();
        $data = array(
            'ipAddress' => RequestUtil::getIpAddress(),
            'lastActivity' => $time,
            'tokenExpiredTime' => Util::timeAdd('+5 days'),
            'countRequest' => $userToken['countRequest'] + 1,
            'status' => self::STATUS_LOGOUT
        );
        $update = $CI->User_token_model->save($userToken['id'], $data);

        // echo "<pre>";
        // var_dump($update);
        // die('')

        if ($update) {
            return TRUE;
        } else {
            return 'Logout gagal.';
        }
    }

    public static function validateToken($token) {
        $CI = &get_instance();
        $CI->load->model('User_token_model');
        $CI->db->trans_start();
        $userToken = $CI->User_token_model->with('Device')->get_by('token', $token);
        $time = Util::timeNow();
        if (!empty($userToken)) {

            $CI->load->model('User_model');
            $userModel = new User_model();
            // $user = $userModel->is_role(TRUE)->with(array('Daily_report_user' => 'User_default'))->get($userToken['userId']);
            $user = $userModel->get($userToken['userId']);
            if ($userToken['status'] == self::STATUS_LOGIN && strtotime($userToken['tokenExpiredTime']) >= strtotime($time)) {
                $data = array(
                    'ipAddress' => RequestUtil::getIpAddress(),
                    'lastActivity' => $time,
                    'tokenExpiredTime' => Util::timeAdd('+5 days'),
                    'countRequest' => $userToken['countRequest'] + 1,
                );
                $update = $CI->User_token_model->save($userToken['id'], $data);
                if ($update) {
                    $CI->db->trans_complete();
                    return array($userToken['token'], $user);
                } else {
                    return 'Update status user gagal.';
                }
            } elseif ($userToken['status'] == self::STATUS_LOGIN && strtotime($userToken['tokenExpiredTime']) < strtotime($time)) {
                $device = (!empty($userToken['device'])) ? json_encode(array('secureId' => $userToken['device']['secureId'])) : NULL;
                $regToken = self::registerToken($userToken['userId'], $device, $CI);
                if ($regToken !== FALSE) {
                    $update = $CI->User_token_model->update($userToken['id'], array('status' => self::STATUS_EXPIRED));
                    $CI->db->trans_complete();
                    return array($regToken, $user);
                } else {
                    return 'Token gagal diganti.';
                }
            }
            return 'Token sudah kadaluarsa.';
        } else {
            return 'Token tidak terdaftar.';
        }
    }

    public static function registerToken($userId, $device = NULL, $CI = NULL) {
        if (is_null($CI))
            $CI = &get_instance();

        if (!empty($device)) {
            $deviceData = (Array) json_decode($device);
            $CI->load->model('Device_model');
            $existDevice = $CI->Device_model->get_by(array('secure_id' => $deviceData['secureId']));

            if (empty($existDevice)) {
                $insertDevice = $CI->Device_model->create($deviceData);
                if ($insertDevice) {
                    $existDevice = $CI->Device_model->get($insertDevice);
                } else {
                    return FALSE;
                }
            }
        }
        $CI->load->model('User_token_model');
        $time = Util::timeNow();
        $token = self::generateToken($userId);
        $data = array(
            'userId' => $userId,
            'deviceId' => !empty($existDevice) ? $existDevice['id'] : NULL,
            'ipAddress' => RequestUtil::getIpAddress(),
            'loginTime' => $time,
            'lastActivity' => $time,
            'token' => $token,
            'tokenExpiredTime' => Util::timeAdd('+5 days'),
            'countRequest' => 1,
            'status' => self::STATUS_LOGIN,
            'createdAt' => $time,
            'updatedAt' => $time
        );

        // API - init Session

        // API - set init Session - by Grady
        // SessionManager::init();
        // $CI->load->model('User_token_model');
        // $CI->User_token_model->setUserInit();


        // $insert = $CI->User_token_model->create($data);
        
        // echo "<pre>";
        // echo $data['token_expired_time'];
        // echo "</pre>";
        // die();
        // $CI->load->model('Post_model');  
        // $d = $this->Post_model->getPosts();
        // echo '<pre>';
        // var_dump($d);
        // die();      
        // $record = array(
        //     'user_id' => $userId,
        //     'device_id' => !empty($existDevice) ? $existDevice['id'] : NULL,
        //     'ip_address' => RequestUtil::getIpAddress(),
        //     'login_time' => $time,
        //     'last_activity' => $time,
        //     'token' => $token,
        //     'token_expired_time' => Util::timeAdd('+5 days'),
        //     'count_request' => 1,
        //     'status' => self::STATUS_LOGIN,
        //     'created_at' => $time,
        //     'updated_at' => $time
        // );
        // $columns = array();
        // $values = array(); 
        // foreach ($record as $key => $value) {
        //     if ($key!=null and $value!=null){
        //         $columns[]=$key;
        //         $values[] = "'".$value."'";
        //     }

        // }
        // $column = implode(',',$columns);
        // $value = implode(",",$values);
        $insert = $CI->User_token_model->create($data);
        // $sql = "insert into user_tokens($column) values ($value)";
        // $insert = dbQuery($sql);
        // echo "<pre>";
        // var_dump($token);
        // die();

        if ($insert) {
            return $token;
        } else {
            return FALSE;
        }
    }


    static function getUserWarga($email) {
        $ci = &get_instance();

        $sql = "select * from users where email='$email' and role='".Role::WARGA."' limit 1";
        return dbGetRow($sql);
    }
    
}
