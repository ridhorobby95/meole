<div class="row" id="main-container">
	<div class="col-sm-12 col-md-12" style="padding:0">
		<div class="panel panel-content">
			<div class="panel-heading">
	            <div class="row">
	            	<div class="col-sm-6 col-md-6">
	            		<h4 style="margin:0; font-weight:bold;font-size:26px">Kalender Hari Libur</h4>
	            	</div>
	                <div class="col-sm-6 col-md-6">
	                	<span class="btn btn-sm btn-primary" id="btn_add_holiday" style="float:right;">Tambah Hari Libur</span>
	               	</div>
	            </div>
			</div>
			<div class="panel-body menu-grid">
				<div id="calendar"></div>
			</div>
		</div>
	</div>
</div>

<script type="application/javascript">
	var json = <?= $data['json']; ?>;
    var currentDateTime = new Date();
    var currentDate = new Date(currentDateTime.getFullYear(),currentDateTime.getMonth(), currentDateTime.getDate());

	/*
		Init date
	 */
	for (var i = json.length - 1; i >= 0; i--) {
		json[i]['startDate'] = new Date(json[i]['date']);
		json[i]['endDate'] = new Date(json[i]['date']);
	}

    $('#btn_add_holiday').on('click', function () {
        location.href = "<?= site_url('web/holiday/add') ?>" ;
    });  

	function addEvent(event){
		console.log(event);
	}

	function editEvent(event) {
	    location.href = "<?= site_url('web/holiday/edit') ?>/" + event.id;
	}

	function deleteEvent(event) {
	    location.href = "<?= site_url('web/holiday/delete') ?>/" + event.id;
	}

	$(function() {
	    $('#calendar').calendar({ 
	        enableContextMenu: true,
	        contextMenuItems:[
	            {
	                text: 'Update',
	                click: editEvent
	            },
	            {
	                text: 'Delete',
	                click: deleteEvent
	            }
	        ],
	        selectRange: function(e) {
	            addEvent({ startDate: e.startDate, endDate: e.endDate });
	        },
	        mouseOnDay: function(e) {
	            if(e.events.length > 0) {
	                var content = '';
	                
	                for(var i in e.events) {
	                    content += '<div class="event-tooltip-content">'
	                                    + '<div class="event-name" style="color:' + e.events[i].color + '">' + e.events[i].name + '</div>'
	                                + '</div>';
	                }
	            
	                $(e.element).popover({ 
	                    trigger: 'manual',
	                    container: 'body',
	                    html:true,
	                    content: content
	                });
	                
	                $(e.element).popover('show');
	            }
	        },
	        mouseOutDay: function(e) {
	            if(e.events.length > 0) {
	                $(e.element).popover('hide');
	            }
	        },
	        dayContextMenu: function(e) {
	            $(e.element).popover('hide');
	        },
	        dataSource: json,
	        customDayRenderer: function(element, date) {
	            if(date.getTime() == currentDate.getTime()) {
	                $(element).css('border-radius', '15px');
                	$(element).css('border', '2px solid black');
                	$(element).css('background', 'black');
	                $(element).css('color', 'white');
                	$(element).css('font-weight', 'bold');
	            }
	        },
	    });
	});
</script>
