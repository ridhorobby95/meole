<div class="row">
    <div class="col-md-12">
        <div class="panel">
            <div class="panel-body">
                <div class="row">
                    <?php echo form_open_multipart($path . $class . '/update', array('id' => 'form_data')) ?>
                    <div class="col-sm-8">
                        <div class="row bord-bottom">
                            <label class="col-sm-4">Username</label>
                            <div class="col-sm-8">
                                <?php echo $data['username'] ?>
                            </div>
                        </div>
                        <div class="row bord-bottom">
                            <label for="name" class="col-sm-4">Nama</label>
                            <div class="col-sm-8">
                                <?php echo $data['name'] ?>
                            </div>
                        </div>
                        <div class="row bord-bottom">
                            <label for="name" class="col-sm-4">Sebagai </label>
                            <div class="col-sm-8"><?php echo Role::name($data['role']) ?>
                            <?php if (SessionManagerWeb::isAdministrator()) { ?>
                                <?php if (1==2) { ?>
                                &nbsp;
                                <div class="btn-group  pull-right">
                                  <button type="button" class="btn btn-primary btn-sm" id="btn_role">Ganti Peran</button>
                                  <button type="button" class="btn btn-primary btn-sm dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                    <span class="caret"></span>
                                  </button>
                                  <ul class="dropdown-menu">
                                        <li><a class="ganti_role" style="cursor:pointer" val="O"><?= Role::name('O') ?></a></li>
                                        <li><a class="ganti_role" style="cursor:pointer" val="S"><?= Role::name('S') ?></a></li>
                                        <li><a class="ganti_role" style="cursor:pointer" val="A"><?= Role::name('A') ?></a></li>
                                  </ul>
                                </div>   
                                <?php } ?>                             
                            <?php } ?>
                            
                            <?php if ($data['nm_lemb']) echo '<br>'.$data['nm_lemb'] ?>
                            </div>
                        </div>

                        <div class="row bord-bottom">
                            <label for="name" class="col-sm-4">Dinas</label>
                            <div class="col-sm-8">
                                <?php echo $data['nm_lemb'] ?>
                            </div>
                        </div>
                        <div class="row bord-bottom">
                            <label for="name" class="col-sm-4">Email</label>
                            <div class="col-sm-8">
                                <?php echo $data['email'] ?>
                            </div>
                        </div>
                        <div class="row bord-bottom">
                            <label for="name" class="col-sm-4">No. Handphone</label>
                            <div class="col-sm-8">
                                <?php echo $data['no_hp'] ?>
                            </div>
                        </div>

                        <?php if ($data['role'] == 'O'){ ?>

                        <div class="row bord-bottom">
                            <label for="name" class="col-sm-4">Tiket Dilaporkan</label>
                            <div class="col-sm-8">
                                <?php echo $data['ticket']['lapor'] ?>
                            </div>
                        </div>
                        <div class="row bord-bottom">
                            <label for="name" class="col-sm-4">Tiket Dialihkan</label>
                            <div class="col-sm-8">
                                <?php echo $data['ticket']['alih'] ?>
                            </div>
                        </div>
                        <div class="row bord-bottom">
                            <label for="name" class="col-sm-4">Tiket Direspon</label>
                            <div class="col-sm-8">
                                <?php echo $data['ticket']['respon'] ?>
                            </div>
                        </div>
                        <div class="row bord-bottom">
                            <label for="name" class="col-sm-4">Tiket Proses</label>
                            <div class="col-sm-8">
                                <?php echo $data['ticket']['proses'] ?>
                            </div>
                        </div>
                        <div class="row bord-bottom">
                            <label for="name" class="col-sm-4">Tiket Selesai</label>
                            <div class="col-sm-8">
                                <?php echo $data['ticket']['selesai'] ?>
                            </div>
                        </div>
                        <div class="row bord-bottom">
                            <label for="name" class="col-sm-4">Tiket Diarsipkan</label>
                            <div class="col-sm-8">
                                <?php echo $data['ticket']['arsip'] ?>
                            </div>
                        </div>

                        <?php } 
                        if ($data['role'] == 'W'){ ?>

                        <div class="row bord-bottom">
                            <label for="name" class="col-sm-4">Jumlah Tiket</label>
                            <div class="col-sm-8">
                                <?php echo $data['ticket']['jml'] ?>
                            </div>
                        </div>

                        <?php } ?>
                        <?php if (1==9) { ?>
                        <div class="row-fluid">
                        <br>
                            <h4>Hak akses di Forlap</h4>
                            <hr>
                            <?php foreach ($data['list_peran'] as $peran) { ?>
                            <div class="row bord-bottom">
                            <div class="col-sm-4"><?= $peran['nm_peran'] ?>&nbsp;</div>
                            <div class="col-sm-4"><?= $peran['nm_lemb_induk']?$peran['nm_lemb_induk']:$peran['nm_lemb'] ?>&nbsp;</div>
                            <div class="col-sm-4"><?= !$peran['nm_lemb_induk']?'':$peran['nm_lemb'] ?>&nbsp;</div>
                            </div>
                            <?php } ?>
                        </div>  
                        <?php } ?>                      
                        
                    </div>
                    <div class="col-sm-4 text-center">
                        <div class="row">
                            <label class="col-sm-12">Foto Profil</label>
                        </div>
                        <div class="row">
                            <div class="col-sm-12">
                                <div class="site-photo site-photo-128 site-photo-center">
                                    <img id="img_user" class="img-circle" style="height: 38px;width: 38px;" src="<?= site_url($data['user_photo']) ?>"  />
                                    <label id="label_user" class="label label-info hidden">Simpan profil untuk mengupload foto</label>
                                    <form method="POST" action="<?php site_url('web/user/update/'.$data['id']) ?>" enctype="multipart/form-data">
									<input type="file" name="photo" id="photo" class="hidden">
                                    <br>
                                    <br>
                                    <button type="submit" id="simpan_foto" class="btn btn-primary hidden"><i class="fa fa-save" style="color:white"></i> Simpan</button>
                                    </form>
                                </div>
                            </div>
                        </div>
						<br>
						
                    </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>



<script type="text/javascript">

    <?php 
    if ($data['id'] == SessionManagerWeb::getUserID()) {
     ?>
     $("#img_user").css("cursor", "pointer").click(function () {
        $("#photo").click();
    });
    $("#photo").change(function () {
            $("#label_user").removeClass("hidden");
            $("#simpan_foto").removeClass("hidden");
        });
    <?php } ?>
    
    <?php if (SessionManagerWeb::isAdministrator()) { ?>
    $('.ganti_role').on('click', function() {
        location.href = '<?= site_url('web/user/setrole/'.$data['id']) ?>/' + $(this).attr('val');
    });
    <?php } ?>

    function goSave() {
        $("#form_data").submit();
    }

    function goChangePass() {
        $("#modal_form").modal();
    }

</script>