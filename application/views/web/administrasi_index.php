<div class="row" id="main-container">
	<div class="col-sm-12 col-md-10" style="padding:0">
		<div class="panel panel-content">
			<div class="panel-heading">
				<h4 style="margin:0; font-weight:bold;font-size:26px">Administrasi</h4>
			</div>
			<div class="panel-body menu-grid">

				<div class="col-xs-6 col-md-4">
				<div class="thumbnail">
					<a href="<?= site_url('web/replies') ?>">
					<h1><i class="fa fa-mail-reply-all"></i></h1>
					<div class="caption">
						Template Jawaban
					</div>
					</a>
				</div>
				</div>


				<?php if (SessionManagerWeb::isAdministrator()) { ?>
					<div class="col-xs-6 col-md-4">
					<div class="thumbnail">
						<a href="<?= site_url('web/user/daftar') ?>">
						<h1><i class="fa fa-user"></i></h1>
						<div class="caption">
							Pengguna
						</div>
						</a>
					</div>
					</div>
				<?php } ?>
				<?php if (SessionManagerWeb::isSuperAdministrator()) { ?>
					<div class="col-xs-6 col-md-4">
					<div class="thumbnail">
						<a href="<?= site_url('web/group/daftar_group') ?>">
						<h1><i class="fa fa-users"></i></h1>
						<div class="caption">
							Group
						</div>
						</a>
					</div>
					</div>
				<?php } ?>
				<?php if (SessionManagerWeb::isAdministrator()){ ?>
					<div class="col-xs-6 col-md-4">
					<div class="thumbnail">
						<a href="<?= site_url('web/group/daftar_helpdesk') ?>">
						<h1><i class="glyphicon glyphicon-info-sign"></i></h1>
						<div class="caption">
							Helpdesk
						</div>
						</a>
					</div>
					</div>
				<?php } ?>

				<div class="col-xs-6 col-md-4">
					<div class="thumbnail">
						<a href="<?= site_url('web/statistik') ?>">
						<h1><i class="glyphicon glyphicon-stats"></i></h1>
						<div class="caption">
							Statistik
						</div>
						</a>
					</div>
				</div>

				<?php if (SessionManagerWeb::isSuperAdministrator()) { ?>
					<div class="col-xs-6 col-md-4">
						<div class="thumbnail">
							<a href="<?= site_url('web/statistik/index/1') ?>">
							<h1><i class="glyphicon glyphicon-stats"></i></h1>
							<div class="caption">
								Statistik Kopertis
							</div>
							</a>
						</div>
					</div>
				<?php } ?>

				<?php if (SessionManagerWeb::isAdministrator()){ ?>
					<div class="col-xs-6 col-md-4">
					<div class="thumbnail">
						<a href="<?= site_url('web/holiday') ?>">
						<h1><i class="glyphicon glyphicon-calendar"></i></h1>
						<div class="caption">
							Hari Libur
						</div>
						</a>
					</div>
					</div>
				<?php } ?>
			</div>
		</div>
	</div>
</div>
