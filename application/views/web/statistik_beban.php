<div class="row">
    <div class="col-md-12">        
        <div class="box box-primary">
            <div class="box-body">
                <div class="row">
                    <div class="col-md-12">
                        <h2><?= $title ?> Beban <?= $chosen_helpdesk ?></h2><hr>
                        <?php if (SessionManagerWeb::isSuperAdministrator()) { ?>
                            <form >
                                <label for="name" class="col-md-3">Tampilkan Beban dari : </label>
                                <div class="btn-group" style="margin-left:-50px">
                                    <?php
                                        if ($chosen_helpdesk!='') { ?>
                                            <input type="button" class="btn btn-default btn-xs" id="btn_helpdesk" name="btn_helpdesk" value="<?php echo $chosen_helpdesk ?>">
                                       <?php } else { ?>
                                            <input type="button" class="btn btn-default btn-xs" id="btn_helpdesk" name="btn_helpdesk" value="Pilih...">
                                        <?php }
                                    ?>
                                    
                                    <button type="button" class="btn btn-default btn-xs dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                        <span class="caret"></span>
                                    </button>
                                    <ul class="dropdown-menu">
                                        <li><!-- 
                                            <a style="text-align:left" class="set-helpdesk-dashboard" value="">
                                                Pilih...
                                            </a>
                                        </li> -->
                                        <?php foreach ($list_helpdesk as $k => $v) { ?>
                                        <li>
                                            <a style="text-align:left" class="set-helpdesk-dashboard" value="<?= $v['name'] ?>">
                                                <?= $v['name'] ?>
                                            </a>
                                        </li>
                                        <?php } ?>
                                    </ul>
                                </div>
                            </form>
                        <?php } ?>
                        <?php if ($chosen_helpdesk!=NULL) { ?>
                            <br>
                            <div class="table-responsive">
                                <table class="table table-bordered table-condensed table-striped">
                                    <thead>
                                        <tr>
                                            <th class="text-center" style="width:5px">No</th>
                                            <th class="text-center" >Staff</th>
                                            <th class="text-center col-md-2">Tiket terbuka</th>
                                            <th class="text-center col-md-2">Tiket Diproses</th>
                                            <th class="text-center col-md-2">Tiket Selesai</th>
                                            <th class="text-center col-md-1">Beban</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php
                                        foreach ($data as $v) {

                                            $no++;
                                                // if ($_SESSION['role_selected']==NULL || $_SESSION['role_selected']==substr(strtoupper(Role::name($v['role'][0])),0,1)) {
                                                ?>
                                                    <tr>
                                                        <td class="text-right"><?= $no ?></td>
                                                        <td><a href="<?= site_url('web/user/detail/' . $v['id']) ?>"><img id="img_user" class="img-circle" style="width:30px" src="<?= site_url($v['user_photo']) ?>"  /> <?= $v['name'] ?></a>
                                                        <?php if ($v['bobot']>$avg_bobot[0]['avg_bobot']) {  ?><i class="    glyphicon glyphicon-exclamation-sign" style="color:red" title="Kurang cepat dalam pengerjaan tiket"></i>
                                                        <?php } ?>
                                                        </td>
                                                        <td align="center"><?= $v['b'] ?></td>
                                                        <td align="center"><?= $v['p'] ?></td>
                                                        <td align="center"><?= $v['s'] ?></td>
                                                        <td align="center"><span <?php if ($i=='x') echo ' class="label label-primary"' ?>><?= $v['bobot'] ?></span></td>
                                                    </tr>
                                                <?php 
                                                //}
                                            // }
                                        }
                                        ?>
                                    </tbody>
                                </table>
                            </div>
                        <?php } ?>
                    </div>
                </div>

                <!-- <nav>
                    <ul class="pager">
                        <?php
                        
                        if (empty($page) || $page == 0) {
                            echo '<li class="disabled"><a href = "#">Sebelumnya</a></li>';
                        } else {
                            echo '<li>' . anchor('web/' . $this->router->class . '/' . $this->router->method . '/' . ($page - 1) . Util::paramsToUrlGet($_GET), 'Sebelumnya') . '</li>';
                        }
                        #if (empty($data)) {
                        if ($_SESSION['user_offset']==0){
                            echo '<li class="disabled"><a href = "#">Selanjutnya</a></li>';
                        
                        } else {
                            echo '<li>' . anchor('web/' . $this->router->class . '/' . $this->router->method . '/' . ($page + 1) . Util::paramsToUrlGet($_GET), 'Selanjutnya') . '</li>';
                        }
                        ?>
                    </ul>
                </nav> -->

            </div>
        </div>
    </div>
</div>

<script type="text/javascript">

    function findRole1(rad){
        //document.getElementById("radioValue").value = rad;
        $.ajax({
            type: "POST",
            url: site_url("web/user/daftar"),
           data: {findrole:rad}
        });
        //alert(rad);

    }

    $(document).on('click', '.set-helpdesk-dashboard', function(){
        var val = $(this).attr('value');
        if (val=="" || val=="Pilih...") {
            window.location.replace("<?= site_url('web/statistik/beban') ?>/?");
        } else {
            window.location.replace("<?= site_url('web/statistik/beban') ?>/?h="+val);
        }
    }); 

</script>