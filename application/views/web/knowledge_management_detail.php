<div class="post-section post-cat post-cat-<?php echo $data['categoryId'] ?>">
    <div class="post-title">
        <div class="site-photo site-photo-48 site-photo-left">
            <img src="<?php echo isset($data['user']['photo']) ? site_url($path . 'thumb/watermark/' . $data['user']['id'] . '/48') : $nopic ?>">
        </div>
        <div>
            <div>
                <strong><?php echo $data['user']['name'] ?></strong>
            </div>
            <div class="post-header-info">
                <?php echo FormatterWeb::dateToDiff($data['createdAt']) ?>
                <br>
                <?php
                if (!empty($data['updatedBy'])) {
                    echo 'Last update by ' . $data['updatedBy']['name'];
                }
                ?>
            </div>
        </div>
    </div>
    <div class="post-body">
        <p><strong><?php echo nl2br($data['title']) ?></strong></p>                
        <p><?php echo nl2br($data['description']) ?></p>
        <?php if (!empty($data['postTags'])) { ?>
            <p>
                <?php
                foreach ($data['postTags'] as $tag) {
                    echo anchor('web/knowledge_management/tag?param=' . $tag['name'], '<span class="label label-info">#' . $tag['name'] . '</span> ');
                }
                ?>
            </p>
        <?php } ?>
        <?php if (!empty($data['image'])) { ?>
            <p><a href="<?= TemplateManagerWeb::getPostImageLink($data['image']) ?>" class="image-post"><img src="<?php echo TemplateManagerWeb::getPostImageLink($data['image']) ?>" /></a></p>
        <?php } ?>
        <?php if (!empty($data['link'])) { ?>
            <p><a href="<?php echo $data['link'] ?>"><?php echo $data['link'] ?></a></p>
        <?php } ?>
        <?php if (!empty($data['file'])) { ?>
            <p><a href="<?php echo $data['file']['link'] ?>" target="_blank"><?php echo $data['file']['name'] ?></a></p>
        <?php } ?>
    </div>
    <div class="post-footer">
        <span style="margin-right: 20px">Dilihat <?php echo (int) $data['seen'] ?> Orang</span>
        <?php echo count($data['comments']) ?> komentar
        <a href="#" onclick="goReuse()" style="margin-left: 20px">Gunakan</a>
        <a class="post-right" href="<?php echo site_url($path . $class . '/edit/' . $data['id']) ?>">Edit</a>
    </div>
	<div class="post-footer">
		<div class="btn-group btn-group-justified" role="group">
			<div class="btn-group" role="group">
					<button type="button" class="btn btn-default"><h4><span class="fa fa-mail-reply"></span> <a href="<?php echo site_url($path . ($v['categoryId'] != Category_model::KNOWLEDGE_MANAGEMENT ? $class : 'knowledge_management') . '/detail/' . $v['id']) ?>"><?php echo count($v['comments']) ?> Komentar</a>
                <?php if ($v['user']['id'] == SessionManagerWeb::getUserID() && $v['categoryId'] != Category_model::KNOWLEDGE_MANAGEMENT) { ?>
					</h4><?php } ?>
					</button>
			</div>
			<div class="btn-group" role="group">
					<button type="button" class="btn btn-default"><h4>Prioritas Normal</h4></button>
                
				  </div>
				  <div class="btn-group" role="group">
					<button type="button" class="btn btn-default"><h4>Ketentuan Teknis</h4></button>
				  </div>
		</div>  
    </div>
</div>

<table class="comment-table">
    <?php foreach ($data['comments'] as $v) { ?>
        <tr>
            <td>
                <div class="site-photo site-photo-48">
                    <img src="<?php echo isset($v['user']['photo']) ? site_url($path . 'thumb/watermark/' . $v['user']['id'] . '/48') : $nopic ?>">
                </div>
            </td>
            <td class="comment-info">
                <strong><?php echo $v['user']['name'] ?></strong>
                <br /><?php echo $v['text'] ?>
                <div class="post-header-info">
                    <?php echo FormatterWeb::dateToDiff($v['createdAt']) ?>
                    <?php if ($v['user']['id'] == SessionManagerWeb::getUserID() && $v['candelete'] == 1) { ?>
                        <a class="post-right post-delete" href="javascript:goDeleteComment('<?php echo $v['id'] ?>')">Hapus</a>
                    <?php } ?>
                </div>
            </td>
        </tr>
    <?php } ?>
    <tr>
        <td colspan="2">
            <?php echo form_open($path . 'comment/create') ?>
            <div class="input-group">
                <?php echo form_input(array('name' => 'text',
				'class' => 'form-control',
				'placeholder' => 'Tulis komentar',
				'style'=>'width:97%',
				'rows'=>'2'
				)) ?>
                <?php echo form_hidden('post_id', $data['id']) ?>
                <div class="input-group-btn">
                    <button type="submit" class="btn btn-success">
                        <i class="fa fa-check"></i> Kirim
                    </button>
                </div>
            </div>
            <?php echo form_hidden('referer', base_url(uri_string())) ?>
            </form>
        </td>
    </tr>
</table>

<?php echo form_open(null, array('id' => 'form_referer')) ?>
<?php echo form_hidden('referer') ?>
</form>

<?php echo form_open_multipart($path . $class . '/reuse/' . $data['id'], array('id' => 'form_modal')) ?>
<div class="modal fade" id="modal_form" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Saya menggunakan pengetahuan ini untuk</h4>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-md-12">
                        <?php echo form_textarea('description', null, 'id="status" class="form-control" rows="5" required="required"') ?>
                    </div>
                </div>
            </div>
            <div class="modal-footer" align="center">
                <button type="submit" class="btn btn-success"><i class="fa fa-check"></i> Gunakan</button>
                <button type="button" class="btn btn-primary" data-dismiss="modal"><i class="fa fa-times"></i> Batal</button>
            </div>
        </div>
    </div>
</div>
</form>

<script type="text/javascript">

    var formref = $("#form_referer");
    var back = sessionStorage.getItem("<?php echo $class ?>.back");

    function goBack() {
        if (back)
            location.href = back;
        else
            location.href = "<?php echo site_url($path . $class) ?>";
    }

    function goEdit() {
        location.href = "<?php echo site_url($path . $class . '/edit/' . $data['id']) ?>";
    }

    function goDelete() {
        var hapus = confirm("Apakah anda yakin akan menghapus post ini?");
        if (hapus) {
            formref.attr("action", "<?php echo site_url($path . $class . '/delete/' . $data['id']) ?>");
            formref.find("[name='referer']").val(back);
            formref.submit();
        }
    }

    function goDeleteComment(id) {
        var hapus = confirm("Apakah anda yakin akan menghapus komentar ini?");
        if (hapus) {
            formref.attr("action", "<?php echo site_url($path . 'comment/delete') ?>/" + id);
            formref.find("[name='referer']").val("<?php echo base_url(uri_string()) ?>");
            formref.submit();
        }
    }

    function goReuse() {
        $("#modal_form").modal();
    }

    $(document).ready(function () {
        $(".image-post").colorbox();
    });


</script>