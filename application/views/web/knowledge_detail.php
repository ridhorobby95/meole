<div class="row" id="main-container">
	<div class="col-md-8">

	<?php
		if ($data['post_id']==NULL) {
			$_data = NULL;
			echo '<div class="alert alert-info">Tidak ada data yang bisa ditampilkan</div>';
		} else {
			$style = "background-color:white";
			if ($data['post_status'] == Ticket::SELESAI) {
				$style = "background-color:#efefef;border:1px solid #ccc";
			}
		
		?>
			<div class="post-section" style="<?= $style ?>">	
				<div id="post-<?= $data['post_id'] ?>">
				<?php 
				$_data = $data;
				include(__DIR__.'/_knowledge_detail.php');
				?>
				</div>             
				<?php 
					
				?>
				<table class="comment-table" style="table-layout: fixed">
						<?php foreach($data['comments'] as $v) { ?>
						<tr>
							<td style="width:65px">
								<div class="site-photo">
									<img class="img-circle" style="width:50px" src="<?= site_url($v['user_photo']) ?>">
								</div>
							</td>
							<td class="comment-info">
								<strong><?php echo $v['user_name'] ?> <?php if ($v['user_role'] == 'A') { ?> <span class="glyphicon glyphicon-star-empty"></span><?php } ?></strong>
								<p style="font-weight: normal;word-wrap: break-word;"><?= nl2br($v['text']) ?></p>

								<?php foreach ($v['images'] as $image) {  ?>
					                <a href="<?= $image['medium'] ?>" class="image-comment"><img src="<?= $image['thumb'] ?>" style="max-height:100px" /></a> &nbsp;
								<?php } ?>
								<?php if(!empty($v['files'])) {  ?>
							        <?php 
							            foreach ($v['files'] as $file) { 
							                $links = explode('/',$file);
							                $link = $links[count($links)-1];
							                // die($file);
							        ?>
							            <div style="background-color: #f5f5f5">
							            <strong>
							            <i class="glyphicon glyphicon-file" style="color:#05057d"></i>
							            <!-- <a href="<?php echo $file ?>" target="_blank" > -->
							            <a href="<?php echo site_url($path.'thumb/files/'.$link) ?>" target="_blank" >
							                <?php 
							                    $file_name = explode('-',$file);
							                    $index=0;
							                    $fileName="";
							                    foreach ($file_name as $fname){
							                        if ($index>0) {
							                            $fileName .=$fname;
							                            if ($index<count($file_name)-1) {
							                                $fileName .='-';
							                            }
							                        }
							                        $index++;
							                    }
							                    echo $fileName; 
							                ?>
							            </a></strong>
							            </div>
							        <?php } ?>
							    <?php } ?>
								<!-- <?php if(!empty($v['files'])) {  ?>
							        <hr style="margin-bottom:5px;" />
							        <?php foreach ($v['files'] as $file) { ?>
							            <div style="background-color: #f5f5f5">
							            <strong><a href="<?php echo $file ?>" target="_blank" >
							                <?php 
							                    $fileName = explode('-',$file);
							                    echo $fileName[1]; 
							                ?>
							            </a></strong>
							            </div>
							        <?php } ?>
							    <?php } ?> -->
								<div class="line-space">
									<div class="post-header-info">
										<?php echo FormatterWeb::dateToDiff($v['created_at']) ?>
										<?php //WEB_Controller::isAllowDeleteComment()
										if(($v['user_id'] == SessionManagerWeb::getUserID() || $is_admin_this_group || SessionManagerWeb::isSuperAdministrator()) && $v['candelete']) { ?>
										<a class="post-right post-delete" href="javascript:goDeleteComment('<?php echo $v['id'] ?>')">Hapus</a>
										<?php } ?>
										<?php if (SessionManagerWeb::isStaff() && $v['candelete']) { ?>
										<a class="post-right post-save" href="javascript:goSaveTemplate('<?php echo $v['id'] ?>')" >Simpan</a>
										<?php } ?>
									</div>
								</div>
							</td>
						</tr>
						<?php } ?>
						
						<?php if ($_data['post_status'] != Ticket::SELESAI && $_data['post_type']!=Post_Model::TYPE_DISCLAIMER)  { ?>
						<tr> 
							<td>
								<div class="site-photo site-photo-36">
									<img class="img-circle" style="max-height:50px;max-width:50px" src="<?= site_url($me['user_photo']) ?>">
								</div>
							</td>
							<td class="comment-info">
							
								<form method="post" name="kirim_form" id="kirim_form" action="<?= site_url('web/comment/create') ?>">
								<input type="hidden" name="post_id" id="post_id" value="<?= $data['post_id'] ?>">
								<input type="hidden" name="referer" id="referer" value="<?= base_url(uri_string()) ?>">
								<div class="post-body" style="padding:0;margin:0;margin-bottom:5px;padding-bottom:10px">
									<div class="row-fluid">
									<textarea class="form-control" id="text_kirim" name="text" placeholder="Tulis tanggapan" rows="5"></textarea>
									</div>
									<div class="row" style="margin-bottom:5px;padding-bottom:10px;padding-top:10px">
										<div class="col-md-4">
										<!-- <span class="glyphicon glyphicon-camera" style="cursor:pointer;font-size:26px; color:#777" onclick="$('#gambar').click()"></span> -->
										<div class="btn btn-default btn-sm" onclick="$('#gambar').click()"><span class="fa fa-camera" style="cursor:pointer;color:#777;"></span> &nbsp;Foto</div>
										<?php if (SessionManagerWeb::isStaff()) { ?>
										<!-- &nbsp;<span class="glyphicon glyphicon-folder-open" style="cursor:pointer;font-size:24px" onclick="$('#filedoc').click()"></span> -->
										<div class="btn btn-default btn-sm" onclick="$('#file_upload').click()"><span class="fa fa-file" style="cursor:pointer;color:#777;"></span> &nbsp;File</div>
										<?php } ?>
										</div>		    
									    <div class="col-md-8">
									    	<div class="pull-right">
										    	<span class="btn btn-sm btn-batal">Batal</span>
										    	<span class="btn btn-sm btn-kirim"><i class="fa fa-send"></i> Kirim</span>
									    	</div>
									    </div>
									</div>
									<div class="row">
										<div id="images_preview" class="col-md-12"></div>
										<?php if (SessionManagerWeb::isStaff()) { ?>
										<div id="files_preview" class="col-md-12"></div>
										<?php } ?>
									</div>
								</div>   
								</form>
								<form method="post" name="image_form" id="image_form" enctype="multipart/form-data" action="<?= site_url('web/comment/imageform') ?>">
								<input type="file" id="gambar" name="gambar[]" multiple style="visibility:hidden">
								</form>
								
								<?php if (SessionManagerWeb::isStaff()) { ?>
									<form method="post" name="file_form" id="file_form" enctype="multipart/form-data" action="<?= site_url('web/comment/fileform') ?>">
								        <input type="file" id="file_upload" name="file_upload[]" multiple style="visibility:hidden">
								    </form>
								
								<?php } ?>
								

							</td>
						</tr>
						<?php } ?>
					</table>
				</div>
		<?php } ?>
	</div>

	<div class=" col-md-4 hidden-xs">
	<?php include(__DIR__.'/_information_right.php'); ?>
	</div>
</div>


<?php echo form_open(null,array('id' => 'form_referer')) ?>
	<?php echo form_hidden('referer') ?>
</form>



<script type="text/javascript" src="<?php echo base_url('assets/web/js/jquery.form.js') ?>"></script>
<script type="text/javascript">
	$('.toggle_set').on('click', function() {
		location.href = '<?= site_url('web/post/toggleset/' . $_data['post_id']) ?>';
	})

$('#gambar').on('change',function(){
    $('#image_form').ajaxForm({
        target:'#images_preview',
        error:function(e){
        	alert('Terjadi kesalahan. Silakan diulangi lagi');
        }
    }).submit();
});

<?php if (SessionManagerWeb::getRole() != Role::OPERATOR) { ?>
$('#filedoc').on('change',function(){
    $('#file_form').ajaxForm({
        target:'#files_preview',
        error:function(e){
        	alert('error');
        }
    }).submit();
});
<?php } ?>


$('.btn-batal').on('click', function() {
	$.ajax({
 		url: "<?= site_url('web/comment/resetkirim') ?>", 
 		success: function(result){
	        $("#images_preview").html('');
	        $("#text_kirim").html('');
    	}
	});    
});

$('.btn-kirim').on('click', function() {
	if (!confirm('Anda akan memberi komentar?'))
		return false;

	$('#kirim_form').submit();
});

var formref = $("#form_referer");
var back = sessionStorage.getItem("<?php echo $class ?>.back");

function goBack() {
	if(back)
		location.href = back;
	else
		location.href = "<?php echo site_url($path.$class) ?>";
}

function goChange() {
	if(back)
		location.href = back;
	else
		location.href = "<?php echo site_url($path.$class.'/edit/'.$data['post_id']) ?>";
}

function goEdit() {
	location.href = "<?php echo site_url($path.$class.'/edit/'.$data['post_id']) ?>";
}

function goDelete() {
	var hapus = confirm("Apakah anda yakin akan menghapus post ini?");
	if(hapus) {
		formref.attr("action","<?php echo site_url($path.$class.'/delete/'.$data['post_id']) ?>");
		formref.find("[name='referer']").val(back);
		formref.submit();
	}
}

function goDeleteComment(id) {
	var hapus = confirm("Apakah anda yakin akan menghapus komentar ini?");
	if(hapus) {
		formref.attr("action","<?php echo site_url($path.'comment/delete') ?>/" + id);
		formref.find("[name='referer']").val("<?php echo base_url(uri_string()) ?>");
		formref.submit();
	}
}

function goSaveTemplate(id) {
	$.ajax(
		{
			url: "<?= site_url('web/replies/edit') ?>",
			type:'POST',
			data: {'id_comment':id}, 
			success: function(){
        		window.location.href = '<?php echo site_url('web/replies/edit') ?>';
    		}
    	}
    );
}

 $(document).ready(function () {
    $(".image-post").colorbox();
    $(document).ready(function () {
		<?php if ($_SESSION['sigap']['draft_post']) { ?>
		 $.ajax({
		 		url: "<?= site_url('web/comment/getdraft') ?>", 
		 		success: function(result){
			        $("#images_preview").html(result);
			        $('#text_kirim').html('<?= $_SESSION['sigap']['draft_post_text'] ?>');
		    	}
			});
			
		<?php } ?>
    });
});

</script>

<?php include(__DIR__.'/_post_js.php'); ?>
