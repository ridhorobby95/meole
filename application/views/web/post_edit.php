<?php
    // echo '<pre>';
    // var_dump($data);
    // echo '</pre>';
    // die();
?>
<div class="row" id="main-container">
    <div class="col-md-8">

    <?php
    // todo is_allow_edit

        if ($data['post_id']==NULL) {
            $_data = NULL;
            echo '<div class="alert alert-info">Tidak ada data yang bisa ditampilkan</div>';
        } else {
            $style = "background-color:white";
            if ($data['post_status'] == Ticket::SELESAI) {
                $style = "background-color:#efefef;border:1px solid #ccc";
            }
        
        ?>
            <div class="post-section" style="<?= $style ?>">    
                <div id="post-<?= $data['post_id'] ?>">

                </div>             

<?php 
$_data = $data;
?>                

<div class="post-title">
    <div class="pull-left user-desc">
        <div class="site-photo">
            <img class="img-circle" style="height:50px;width:50px" src="<?= site_url($_data['user_photo']) ?>">
        </div>
    </div>
    <div class="pull-left user-desc">
        <strong><?php echo $_data['user_name'] ?></strong>
        <?php if ($_data['nm_lemb']) echo '<br>'.$_data['nm_lemb']; ?>
        <div class="post-header-info">
            <?php echo FormatterWeb::dateToDiff($_data['post_created_at']) ?>
            <?php /* if($_data['createdAt'] != $_data['updatedAt']) { ?>
            <span class="post-right">Diedit <?php echo FormatterWeb::dateToDiff($_data['updatedAt']) ?></span>
            <?php } */ ?>
        </div>
    </div>
</div>

<div class="post-section" id="new_post">
    <form method="post" name="kirim_form" id="kirim_form" action="<?= site_url('web/post/update/' . $_data['post_id']) ?>">
        <input type="hidden" name="group_kirim" id="group_kirim" value="<?= $list_group[0]['id'] ?>">
        <input type="hidden" name="stick_kirim" id="stick_kirim" value="<?= $_data['is_sticky']?>" >
        <div class="post-body" style="margin-bottom:5px;padding-bottom:10px">
            <div class="row">
                <div class="col-md-12" style="padding:0;">
                <textarea class="form-control" id="judul_kirim" name="judul_kirim" rows="1" style="border:0;border-bottom:1px solid #eee" autofocus placeholder="Judul Post"><?= $_data['post_title'] ?></textarea>
                <textarea class="form-control" id="text_kirim" name="text_kirim" rows="5" style="border:0" autofocus placeholder="ketik pesan Anda di sini"><?= strip_tags($_data['post_description']) ?></textarea></div>
            </div>
            <div class="row" id="kata_kunci" style="display: <?= $_data['post_type'] != Post_model::TYPE_TICKET ? 'none' : '' ?>">
                <hr style="margin:5px"/>
                <div class="col-md-2" style="padding:0;text-align:center;vertical-align: center;margin-top:13px">
                    <strong>Kata Kunci</strong>
                </div>
                <div class="col-md-10" style="padding:0;margin-top:10px;">
                    <?php echo form_dropdown('tag[]', $a_tag, $_data['tags'], 'class="form-control input-sm select2 tag" multiple="multiple" style="width:100%;"') ?>
                    <!-- <textarea class="form-control" id="tag" name="tag" rows="1" style="border:0" autofocus placeholder="Deskripsi Knowledge"></textarea> -->
                </div>
            </div>
            <div class="row" style="padding-top:5px">
                <hr style="margin:5px"/>
                <div class="col-md-2 col-xs-6">
                    <div class="btn btn-default btn-sm" onclick="$('#gambar').click()"><span class="fa fa-camera" style="cursor:pointer;color:#777;"></span> &nbsp;Foto</div>
                    
                </div>
                <div class="col-md-3 col-xs-4" style="padding:0;">    
                    <?php if (SessionManagerWeb::isStaff() and $_data['post_type']!=Post_model::TYPE_DISCLAIMER) { ?>
                        <div class="btn btn-default btn-sm" onclick="$('#file_upload').click()"><span class="fa fa-file" style="cursor:pointer;color:#777;"></span> &nbsp;File</div>
                    <?php } ?>
                </div>
                
                <div class="col-md-2" style="padding:0;">
                    <?php if (SessionManagerWeb::isStaff() && $data['group_name']=='Umum') { ?>
                        <label id="sticky_label">
                        <form>
                        <?php if ($data['is_sticky']=='1') { ?>
                        <input type="checkbox" name="sticky" id='sticky' checked>
                        <?php } else { ?>
                        <input type="checkbox" name="sticky" id='sticky'>
                        <?php } ?>
                        </form>
                        <small><strong>pin post</strong></small> &nbsp;
                        </label>
                    <?php } ?>
                </div>

                <div class="col-md-5">
                    <div class="pull-right">
                        <span class="btn btn-sm btn-batal">Batal</span>
                        <span class="btn btn-sm btn-kirim"><i class="fa fa-save"></i> Simpan</span>
                    </div>
                </div>
            </div>
            <div class="row">
                <div id="images_edit" name="images_edit" class="col-md-12"></div>
                <div id="images_preview" name="images_preview" class="col-md-12"></div>
                <div id="files_edit" name="files_edit" class="col-md-12"></div>
                <div id="files_preview" name="files_preview" class="col-md-12" style="background-color: #f5f5f5"></div>
            </div>
        </div>
    </form>
    <form method="post" name="file_form" id="file_form" enctype="multipart/form-data" action="<?= site_url('web/post/fileform') ?>" class="hidden">
        <input type="file" id="file_upload" name="file_upload[]" multiple style="visibility:hidden">
    </form>
    <form method="post" name="image_form" id="image_form" enctype="multipart/form-data" action="<?= site_url('web/post/imageform') ?>">
        <input type="file" id="gambar" name="gambar[]" multiple style="visibility:hidden">
    </form>
</div>

<script type="text/javascript" src="<?php echo base_url('assets/web/js/jquery.form.js') ?>"></script>
<script>
    $('#gambar').on('change', function() {
        $('#image_form').ajaxForm({
            target: '#images_preview',
            error: function(e) {
                //target: '#images_preview';
                 alert('Terjadi kesalahan. Silakan diulangi lagi');
            }
        }).submit();
    });

    $('#file_upload').on('change', function() {
        $('#file_form').ajaxForm({
            target: '#files_preview',
            error: function(e) {
                //target: '#images_preview';
                 alert('Terjadi kesalahan. Silakan diulangi lagi');
            }
        }).submit();
    });

    $('.group_kiriman').on('click', function() {
        if ($(this).attr('val') == '1')
            $('#sticky_label').show();
        else
            $('#sticky_label').hide();

        $('#group_kirim').val($(this).attr('val'));
        $('#btn_group').html($(this).html());
    });

    $('.btn-batal').on('click', function() {
        $.ajax({
            url: "<?= site_url('web/post/resetkirim') ?>",
            success: function(result) {
                location.href = '<?= site_url('web/post/detail/' . $_data['post_id']) ?>';
            }
        });
    });

    $('.btn-kirim').on('click', function() {
        if (!confirm('Anda akan mengubah pesan ini?'))
            return false;
        if ($('#sticky').is(':checked')) {
            $('#stick_kirim').val('1');
        } else 
            $('#stick_kirim').val('0');
        $('#kirim_form').submit();
    });

    $(document).ready(function() {
        <?php if ($_SESSION['integra']['draft_post']) { ?>
        $.ajax({
            url: "<?= site_url('web/post/getdraft') ?>",
            success: function(result) {
                $("#images_preview").html(result['images']);
                $("#files_preview").html(result['files']);
                $('#judul_kirim').html('<?= $_SESSION['
                    integra ']['
                    draft_post_judul '] ?>');
                $('#text_kirim').html('<?= $_SESSION['
                    integra ']['
                    draft_post_text '] ?>');
                $('#new_post').show();
            }
        });

        <?php } ?>
    });
</script>


                </div>
        <?php } ?>
    </div>

    <div class=" col-md-4 hidden-xs">
    <?php include(__DIR__.'/_information_right.php'); ?>
    </div>
</div>

<?php echo form_open(null,array('id' => 'form_referer')) ?>
    <?php echo form_hidden('referer') ?>
</form>

<script type="text/javascript" src="<?php echo base_url('assets/web/js/jquery.form.js') ?>"></script>
<script type="text/javascript">
    $('.toggle_set').on('click', function() {
        location.href = '<?= site_url('web/post/toggleset/' . $_data['post_id']) ?>';
    })


$('#gambar').on('change',function(){
    $('#image_form').ajaxForm({
        target:'#images_preview',
        error:function(e){
            alert('Terjadi kesalahan. Silakan diulangi lagi');
        }
    }).submit();
});

$(document).ready(function() {
    $.ajax({
        url: "<?= site_url('web/post/getimages/' . $_data['post_id']) ?>",
        success: function(result) {
            $("#images_edit").html(result);
        }
    });
});

function deleteimage(post_id, image_id){
    if (confirm('Hapus gambar ini ?')) {
        $.ajax({
            url: "<?= site_url('web/post/deleteimage/') ?>/"+post_id+'/'+image_id,
            success: function(result) {
                $("#images_edit").html(result);
            }
        });
    }
    
}

function deletefile(post_id, file_id){
    if (confirm('Hapus file ini ?')) {
        $.ajax({
            url: "<?= site_url('web/post/deletefile/') ?>/"+post_id+'/'+file_id,
            success: function(result) {
                $("#files_edit").html(result);
            }
        });
    }
    
}

$(document).ready(function() {
    $.ajax({
        url: "<?= site_url('web/post/getfiles/' . $_data['post_id']) ?>",
        success: function(result) {
            $("#files_edit").html(result);
        }
    });
});

<?php if (SessionManagerWeb::getRole() != Role::OPERATOR) { ?>
$('#filedoc').on('change',function(){
    $('#file_form').ajaxForm({
        target:'#files_preview',
        error:function(e){
            alert('error');
        }
    }).submit();
});
<?php } ?>


$('.btn-batal').on('click', function() {
    $.ajax({
        url: "<?= site_url('web/comment/resetkirim') ?>", 
        success: function(result){
            $("#images_preview").html('');
            $("#text_kirim").html('');
        }
    });    
});

$(".tag").select2({
    placeholder: 'Ketik kata kunci',
    minimumResultsForSearch: Infinity,
    tags: true
});

var formref = $("#form_referer");
var back = sessionStorage.getItem("<?php echo $class ?>.back");

function goBack() {
    if(back)
        location.href = back;
    else
        location.href = "<?php echo site_url($path.$class) ?>";
}

function goChange() {
    if(back)
        location.href = back;
    else
        location.href = "<?php echo site_url($path.$class.'/edit/'.$data['post_id']) ?>";
}

function goEdit() {
    location.href = "<?php echo site_url($path.$class.'/edit/'.$data['post_id']) ?>";
}

function goDelete() {
    var hapus = confirm("Apakah anda yakin akan menghapus post ini?");
    if(hapus) {
        formref.attr("action","<?php echo site_url($path.$class.'/delete/'.$data['post_id']) ?>");
        formref.find("[name='referer']").val(back);
        formref.submit();
    }
}
 
</script>

<?php include(__DIR__.'/_post_js.php'); ?>
