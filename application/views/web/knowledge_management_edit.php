<div class="row">
    <div class="col-md-12">
        <div class="box box-primary">
            <div class="box-body">
                <div class="row">
                    <div class="col-md-12">
                        <?php echo form_open_multipart($path . $class . '/' . (isset($id) ? 'update/' . $id : 'create'), array('id' => 'form_data')) ?>
                        <div class="row bord-bottom">
                            <label for="title" class="col-sm-3">Judul</label>
                            <div class="col-sm-9">
                                <?php echo form_input(array('name' => 'title', 'value' => $data['title'], 'class' => 'form-control input-sm', 'id' => 'title')) ?>
                            </div>
                        </div>
                        <div class="row bord-bottom">
                            <label for="title" class="col-sm-3">Deskripsi</label>
                            <div class="col-xs-12">
                                <?php echo form_textarea(array('name' => 'description', 'value' => $data['description'], 'class' => 'form-control input-sm', 'rows' => 10, 'placeholder' => 'Tulis deskripsi disini')) ?>
                            </div>
                        </div>
                        <div class="row bord-bottom">
                            <label for="title" class="col-sm-3">Tag</label>
                            <div class="col-sm-9">
                                <?php echo form_input(array('name' => 'tags', 'value' => $data['tags'], 'class' => 'form-control input-sm', 'id' => 'tags')) ?>
                                <p class="help-block">[Gunakan hashtag dan spasi untuk pemisah] - #takeiteasy</p>
                            </div>
                        </div>
                        <div class="row bord-bottom">
                            <label for="link" class="col-sm-3">Tautan</label>
                            <div class="col-sm-9">
                                <?php echo form_input(array('name' => 'link', 'value' => $data['link'], 'class' => 'form-control input-sm', 'id' => 'link')) ?>
                            </div>
                        </div>
                        <div class="row bord-bottom">
                            <label for="image" class="col-sm-3">Image</label>
                            <div class="col-sm-6">
                                <?php echo form_upload(array('name' => 'image', 'id' => 'image')) ?>
                            </div>
                        </div>
                        <div class="row bord-bottom">
                            <label for="video" class="col-sm-3">Video</label>
                            <div class="col-sm-6">
                                <?php echo form_upload(array('name' => 'video', 'id' => 'video')) ?>
                            </div>
                        </div>
                        <div class="row bord-bottom">
                            <label for="file" class="col-sm-3">File</label>
                            <div class="col-sm-6">
                                <?php echo form_upload(array('name' => 'file', 'id' => 'file')) ?>
                            </div>
                        </div>
                        <?php echo form_hidden('referer') ?>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">
   
    var back = sessionStorage.getItem("<?php echo $class ?>.back");

    function goBack() {
        if (back)
            location.href = back;
        else
            location.href = "<?php echo site_url($path . $class) ?>";
    }

    function goSave() {
        $("#form_data").submit();
    }

</script>