<?php
    // echo '<pre>';
    // var_dump($data);
    // echo '</pre>';
    // die();
?>
<div class="row">
    <div class="col-md-12">
        <div class="box box-primary">
            <div class="row">
                <div class="col-sm-4 col-md-4" style="margin:10px 0px 0px 10px"><a href="<?php if (in_array($data['type'], array('G','K'))) echo site_url('web/group/daftar_group'); else echo  site_url('web/group/daftar_helpdesk'); ?>" style="color:white"><span class="btn btn-sm btn-success" id="btn_back"><i class="glyphicon glyphicon-chevron-left" style="font-size: 10px"></i> Kembali</a></span></div>
            </div>
            <div class="box-body">
                <div class="row">
                    <div class="col-sm-8">
                        <div class="row bord-bottom">
                            <label for="name" class="col-sm-2">Nama</label>
                            <div class="col-sm-10">
                                <?php echo $data['name'] ?>
                            </div>
                        </div>
                        <div class="row bord-bottom" id='data_group_type'>
                            <label for="name" class="col-sm-2">Tipe</label>
                            <div class="col-sm-8">
                                <?php echo $a_type[$data['type']] ?>
                            </div>
                            <?php if ($data['type']!='H') { ?>
                            <button class="btn btn-warning" id="btn_change_group_type">Ubah</button>
                            <?php } ?>
                        </div>
                        <?php if ($data['type']!='H') { ?>
                        <div class="row bord-bottom hidden" id="form_group_type">
                            <label for="name" class="col-sm-3">Tipe</label>
                            <div class="col-sm-9">
                                <form>
                                    <?php echo form_dropdown('group_type', array('G' => 'GROUP', 'K' => 'KOPERTIS'), $data['type'], 'class="form-control input-sm select2  group_type" id= "group_type" style="width:82%"') ?>
                                    <button class="btn btn-success" id="btn_kirim_group_type">Simpan</button>
                                </form>
                            </div>
                        </div>
                        <?php } ?>
                        <?php if ($data['type']=='H') { ?>
                            <div class="row bord-bottom" id='data_initial'>
                                <label for="name" class="col-sm-2">Inisial</label>
                                <div class="col-sm-8" style="display:inline-block;width:70%">
                                    <?= $data['initial'] ? $data['initial'] : strtoupper(substr($data['name'],0,3)).' (default)' ?>
                                </div>
                                <button class="btn btn-warning" id="btn_change_initial">Ganti</button>
                            </div>
                            <div class="row bord-bottom hidden" id="form_initial">
                                <label for="name" class="col-sm-3">Inisial (3 Huruf)</label>
                                <div class="col-sm-9">
                                    <form>
                                        <input type="text" class="form-control" name="initial_helpdesk" id="initial_helpdesk" placeholder="Masukkan inisial helpdesk (3 huruf)" style="display:inline-block;width:82%">
                                        <button class="btn btn-success" id="btn_kirim_initial">Simpan</button>
                                    </form>
                                </div>
                            </div>
                            <div class="row bord-bottom" id='data_description'>
                                <label for="name" class="col-sm-2">Deskripsi</label>
                                <div class="col-sm-8" style="display:inline-block;width:70%">
                                    <?= $data['description'] ? $data['description'] : 'Belum ada deskripsi' ?>
                                </div>
                                <button class="btn btn-warning" id="btn_change_description">Ubah</button>
                            </div>
                            <div class="row bord-bottom hidden" id="form_description">
                                <label for="name" class="col-sm-3">Deskripsi</label>
                                <div class="col-sm-9">
                                    <form>
                                        <input type="text" class="form-control" name="description_helpdesk" id="description_helpdesk" placeholder="Masukkan deskripsi helpdesk " style="display:inline-block;width:82%">
                                        <button class="btn btn-success" id="btn_kirim_description">Simpan</button>
                                    </form>
                                </div>
                            </div>
                            <div class="row bord-bottom" id='data_quota'>
                                <label for="name" class="col-sm-2">Kuota</label>
                                <div class="col-sm-8" style="display:inline-block;width:70%">
                                    <?= $data['quotaLimit'] ? $data['quotaLimit'] : 'Belum ada quota' ?>
                                </div>
                                <button class="btn btn-warning" id="btn_change_quota">Ubah</button>
                            </div>
                            <div class="row bord-bottom hidden" id="form_quota">
                                <label for="name" class="col-sm-3">Kuota</label>
                                <div class="col-sm-9">
                                    <form>
                                    <?php echo form_dropdown('quota_limit', $a_quota, $data['quotaLimit'], 'class="form-control input-sm select2  quota_limit" id= "quota_limit" style="width:82%"') ?>
                                    <button class="btn btn-success" id="btn_kirim_quota">Simpan</button>
                                    </form>
                                </div>
                            </div>
                        <?php } ?>
                        <div class="row bord-bottom">
                            <br>
                            <!-- <label for="group_members" class="col-sm-2">Admin</label> -->
                            <!-- <div class="col-sm-10">
                            </div> -->
                            <label for="group_members" class="col-sm-2" style="top:70px">Anggota</label>
                            <div class="col-sm-10">
                                <table class="group-table" style="width:100%;table-layout: fixed;" >
                                    <tr>
                                        <form action="<?= site_url('web/group/detail/'.$data['id']) ?>" class="search-form">
                                            <div class="form-group has-feedback">
                                                <label for="search_anggota" class="sr-only">Search</label>
                                                <input type="text" class="form-control" name="search_anggota" id="search_anggota" placeholder="Cari anggota..." value="<?= $_GET['search_anggota'] ?>" style="display:inline;width:70%">
                                                <span class="btn btn-primary" id="btn_find">Cari</span>
                                                <a href="<?php echo site_url('web/group/detail/'.$data['id']) ?>">
                                                    <span class="btn btn-success" id="btn_show_all">Semua</span>
                                                </a>
                                            </div>  
                                        </form>
                                    </tr>
                                    <br>
                                    <?php
                                    // $temp=array();
                                    // $sortedMember=array();
                                    //$temp[$data['groupMembers'][0]['user']['id']]=$data['groupMembers'][0]['user']['name'];
                                    // foreach ($data['groupMembers'] as $member) {
                                    //     $temp[$member['user']['id']]=strtolower($member['user']['name']);
                                    // }
                                    // asort($temp);
                                    // foreach ($temp as $user => $name) {
                                    //     array_push($sortedMember,array("user"=>$user,"name"=>$name));
                                    // }
                                    // $sortedMember = array($temp);
                                    $n = ceil(count($data['groupMembers']) / 5);
                                    for ($i = 0; $i < $n; $i++) { ?>
                                        <tr valign="top">
                                            <?php
                                                for ($j = 0; $j < 5; $j++) {
                                                    $row = $data['groupMembers'][($i * 5) + $j];
                                                    // var_dump($row);
                                                    // die($row);
                                                    $user = $row['userid'];
                                                    $name=$row['name'];
                                                    if (empty($row))
                                                        continue;
                                                    
                                                ?>
                                                <td class="text-center">
                                                    <div class="site-photo site-photo-30 site-photo-center">
                                                        <a href="<?= site_url($path.'user/detail/'.$user) ?>">
                                                           <img class="img-circle" style="width:30px;" src="<?= site_url($row['user_photo']) ?>">
                                                        </a>
                                                        <?php if (SessionManagerWeb::isStaff()) { ?>
                                                            <span class="btn btn-group btn-xs" style="position:relative;background-color:red;font-size: 10px;bottom:8px;margin:0px;padding:0px;right:10px" onClick="javascript:deleteMember('<?php echo $user ?>','<?php echo $data['id'] ?>')"><i class="glyphicon glyphicon-remove" style="color:white" title="Keluarkan user"></i></span>
                                                            <?php if ($data['type']!='H') { ?>
                                                                <span class="btn btn-group btn-xs" style="position:relative;background-color:cornflowerblue;font-size: 10px;top:8px;margin:0px;padding:0px;right:25px" onClick="javascript:setStaff('<?php echo $user ?>','<?php echo $data['id'] ?>')"><i class="glyphicon glyphicon-user" style="color:white" title="Jadikan Staff"></i></span>
                                                            <?php } ?>
                                                        <?php } ?>
                                                    </div>
                                                     <a href="<?= site_url($path.'user/detail/'.$user) ?>">
                                                        <p style="font-size:10px;line-height: 100%;margin-right:30px">
                                                            <?php echo $name; ?>
                                                        </p>
                                                    </a>                                                    
                                                </td>
                                            <?php } ?>
                                        </tr>
                                    


                                    <!-- $n = ceil(count($temp) / 5);
                                    //$n = ceil(count($data['groupMembers']) / 5);
                                    for ($i = 0; $i < $n; $i++) { -->
                                        <!-- ?> -->
                                        <!-- <tr valign="top">
                                            <?php
                                            for ($j = 0; $j < 5; $j++) {
                                                $row = $data['groupMembers'][($i * 5) + $j];
                                                $userid = $row['userId'];
                                                if (empty($row))
                                                    continue;
                                                ?>
                                                <td class="text-center">
                                                    <div class="site-photo site-photo-30 site-photo-center">
                                                        <a href="<?= site_url($path.'user/detail/'.$userid) ?>">
                                                           <img class="img-circle" style="width:30px;" src="<?= site_url('web/thumb/profile/'.$row['user']['id']) ?>">
                                                        </a>
                                                        <span class="btn btn-group btn-xs" style="position:relative;background-color:black;font-size: 10px;bottom:8px;margin:0px;padding:0px;right:10px" onClick="javascript:deleteMember('<?php echo $userid ?>')"><i class="glyphicon glyphicon-remove" style="color:white"></i></span>
                                                    </div>
                                                     <a href="<?= site_url($path.'user/detail/'.$userid) ?>">
                                                        <p style="font-size:10px;line-height: 100%;margin-right:10px">
                                                            <?php echo $row['user']['name']; ?>
                                                        </p>
                                                    </a>                                                    
                                                </td>
                                            <?php } ?>
                                        </tr> -->
                                    <?php } ?>
                                </table>
                            </div>
                            <nav>
                                <div class="text-center">
                                    <ul class="pagination">
                                        <?php
                                        $page = $this->uri->segment(5); 
                                        // if (empty($page)) {
                                        //     $page=0;
                                        // }
                                        ?>
                                        <!-- <form>
                                            <input type="hidden" id="page_value" value="<?php $page ?>">
                                        </form> -->
                                        <?php
                                        // die($page);
                                        if (empty($page) || $page == 0) {
                                            echo '<li class="disabled"><a href = "#">Sebelumnya</a></li>';
                                        } else {
                                            echo '<li>' . anchor('web/' . $this->router->class . '/' . $this->router->method . '/'.$data['id'].'/' . ($page - 1) . Util::paramsToUrlGet($_GET), 'Sebelumnya') . '</li>';
                                        }
                                        #if (empty($data)) {
                                        if ($data[0]['next_page_member']==0){
                                            echo '<li class="disabled"><a href = "#">Selanjutnya</a></li>';
                                        
                                        } else {
                                            echo '<li>' . anchor('web/' . $this->router->class . '/' . $this->router->method . '/'.$data['id'].'/' . ($page + 1) . Util::paramsToUrlGet($_GET), 'Selanjutnya') . '</li>';
                                        }
                                        ?>
                                    </ul>
                                </div>
                            </nav>
                        </div>
                        <div class="row bord-bottom">
                            <label for="name" class="col-sm-2" style="top:20px"><?= $data['type']=='H' ? 'Admin Helpdesk' : 'Staff Group' ?></label>
                            <div class="col-sm-10">
                                <table class="group-table" style="width:100%;table-layout: fixed;" >
                                    <br>
                                    <?php
                                    $n = ceil(count($data['staffMembers']) / 5);
                                    for ($i = 0; $i < $n; $i++) { 
                                        if (count($data['staffMembers'])<4){
                                    ?>
                                        <tr valign="top" style="float:left">
                                        <?php } else { ?>
                                        <tr valign="top">
                                        <?php } ?>
                                            <?php
                                                for ($j = 0; $j < 5; $j++) {
                                                    $row = $data['staffMembers'][($i * 5) + $j];
                                                    // var_dump($row);
                                                    // die($row);
                                                    $user = $row['userid'];
                                                    $name=$row['name'];
                                                    if (empty($row))
                                                        continue;
                                                    
                                                ?>
                                                <td class="text-center">
                                                    <div class="site-photo site-photo-30 site-photo-center">
                                                        <a href="<?= site_url($path.'user/detail/'.$user) ?>">
                                                           <img class="img-circle" style="width:30px;" src="<?= site_url($row['user_photo']) ?>">
                                                        </a>
                                                        <!-- <span class="btn btn-group btn-xs" style="position:relative;background-color:red;font-size: 10px;bottom:8px;margin:0px;padding:0px;right:10px" onClick="javascript:deleteMember('<?php echo $user ?>','<?php echo $data['id'] ?>')"><i class="glyphicon glyphicon-remove" style="color:white" title="Keluarkan user"></i></span> -->
                                                        <!-- <span class="btn btn-group btn-xs" style="position:relative;background-color:orange;font-size: 10px;top:8px;margin:0px;padding:0px;right:25px" onClick="javascript:removeStaff('<?php echo $user ?>','<?php echo $data['id'] ?>')"><i class="glyphicon glyphicon-user" style="color:white" title="Jadikan Anggota"></i></span> -->
                                                        <?php if (SessionManagerWeb::isStaff() and $data['type']!='H') { ?>
                                                            <span class="btn btn-group btn-xs" style="position:relative;background-color:orange;font-size: 10px;bottom:8px;margin:0px;padding:0px;right:10px" onClick="javascript:removeStaff('<?php echo $user ?>','<?php echo $data['id'] ?>')"><i class="glyphicon glyphicon-user" style="color:white" title="Remove Staff"></i></span>
                                                        <?php } ?>  
                                                    </div>
                                                     <a href="<?= site_url($path.'user/detail/'.$user) ?>">
                                                        <p style="font-size:10px;line-height: 100%;margin-right:15px">
                                                            <?php echo $name; ?>
                                                        </p>
                                                    </a> 

                                                </td>
                                            <?php } ?>
                                        </tr>
                                    <?php } ?>
                                </table>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-4 text-center">
                        <div class="row">
                            <label class="col-sm-12">Gambar Profil</label>
                        </div>
                        <div class="row">
                            <div class="col-sm-12">
                                <div class="user-img"> 
                                    <img id="img_photo" class="img-circle" src="<?php echo isset($data['image']) ? $data['image']['thumb']['link'] : $noimg ?>" data-toggle="tooltip" title="Ganti Gambar" />
                                    <label id="label_photo" class="label label-info hidden">Simpan grup untuk mengupload gambar</label>
                                    <?php echo form_open_multipart($path . $class . '/updatephoto/'.$data['id'], array('id' => 'form_data')) ?>
                                    <?php echo form_upload(array('name' => 'image', 'id' => 'image', 'class' => 'hidden'));?>
                                </div>
                            </div>
                        </div>
                        <br>
                        <div class="row">

                            <div class="col-md-12 col-sm-12 col-xs-12  hidden-xs">
                            <?php
                            if (!empty($buttons)) {
                                foreach ($buttons as $btn) {
                                    ?>
                                    <button type="submit" class="btn btn-sm success" 
                                            onClick="goSave()">
                                        <i class="fa fa-save"></i> Simpan
                                    </button>
                                    <?php
                                }
                            }
                            ?>
                                <button type="submit" id="btn_simpan_foto" class="btn btn-sm success hidden" 
                                        onClick="goSave()">
                                    <i class="fa fa-save"></i> Simpan
                                </button>
                            </div>                            
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-10 col-sm-offset-1">
                        <div class="col-sm-6">
                            <select class="select2 form-control" name="tambah_anggota" id="tambah_anggota">
                                <option disabled="" selected="">Nama Anggota</option>
                                <?php foreach ($nonMembers as $value): ?>
                                <option value="<?php echo $value['id'] ?>"><?php echo $value['name'] ?></option>
                                <?php endforeach ?>
                            </select>
                        <!-- <input type="hidden" name="user_id_anggota" id="user_id_anggota"> -->
                            <!-- <input type="text" name="tambah_anggota" placeholder="Nama Anggota" id="tambah_anggota" class="form-control select2"> -->
                        </div>
                        <script>
                            $('.select2').select2();
                        </script>
                        <div class="col-sm-4">
                            <span class="btn btn-primary" id="btn_tambah_anggota">Tambah Anggota</span>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </div>
</div>

<style type="text/css">
   .ui-autocomplete {
    position: absolute;
    top: 100%;
    left: 0;
    z-index: 1000;
    float: left;
    display: none;
    min-width: 160px;   
    padding: 4px 0;
    margin: 0 0 10px 25px;
    list-style: none;
    background-color: #ffffff;
    border-color: #ccc;
    border-color: rgba(0, 0, 0, 0.2);
    border-style: solid;
    border-width: 1px;
    -webkit-border-radius: 5px;
    -moz-border-radius: 5px;
    border-radius: 5px;
    -webkit-box-shadow: 0 5px 10px rgba(0, 0, 0, 0.2);
    -moz-box-shadow: 0 5px 10px rgba(0, 0, 0, 0.2);
    box-shadow: 0 5px 10px rgba(0, 0, 0, 0.2);
    -webkit-background-clip: padding-box;
    -moz-background-clip: padding;
    background-clip: padding-box;
    *border-right-width: 2px;
    *border-bottom-width: 2px;
    cursor: pointer;
}

.ui-menu-item > a.ui-corner-all {
    display: block;
    padding: 3px 15px;
    clear: both;
    font-weight: normal;
    line-height: 18px;
    color: #555555;
    white-space: nowrap;
    text-decoration: none;
}

.ui-state-hover, .ui-state-active {
    color: #ffffff;
    text-decoration: none;
    background-color: #0088cc;
    border-radius: 0px;
    -webkit-border-radius: 0px;
    -moz-border-radius: 0px;
    background-image: none;
}
.select2-container--default .select2-selection--single {
    border-radius: 0;
}
</style>


<script type="text/javascript">

    function goBack($is_helpdesk=0) {
        location.href = "<?php echo site_url($path . $class . '/list_me') ?>";
    }

    function deleteMember(user_id,id_group){
        var group_name;
        var user_name;
        $.ajax({
            url : '<?= site_url('web/post/getgroupname') ?>/'+id_group,
            success: function(result){
                group_name=result;
                $.ajax({
                    url : '<?= site_url('web/post/getusername') ?>/'+user_id,
                    success: function(result){
                        user_name=result;
                        if (confirm("Keluarkan "+user_name +" dari "+group_name+" ?")){
                            location.href = "<?= site_url('web/group/deleteuser/' . $data['id']) ?>/" + user_id;   
                        }
                    }
                });
            }
        });   
    }

    function setStaff(user_id,id_group){
        var group_name;
        var user_name;
        $.ajax({
            url : '<?= site_url('web/post/getgroupname') ?>/'+id_group,
            success: function(result){
                group_name=result;
                $.ajax({
                    url : '<?= site_url('web/post/getusername') ?>/'+user_id,
                    success: function(result){
                        user_name=result;
                        if (confirm("Jadikan "+user_name +" sebagai staff "+group_name+" ?")){
                            location.href = "<?= site_url('web/group/setstaff/' . $data['id']) ?>/" + user_id;   
                        }
                    } 
                });
            }
        });   
    }

    function removeStaff(user_id,id_group){
        var group_name;
        var user_name;
        $.ajax({
            url : '<?= site_url('web/post/getgroupname') ?>/'+id_group,
            success: function(result){
                group_name=result;
                $.ajax({
                    url : '<?= site_url('web/post/getusername') ?>/'+user_id,
                    success: function(result){
                        user_name=result;
                        if (confirm("Jadikan "+user_name +" sebagai staff "+group_name+" ?")){
                            location.href = "<?= site_url('web/group/removestaff/' . $data['id']) ?>/" + user_id;   
                        }
                    }
                });
            }
        });   
    }

    // $(document).ready(function(){
    //     var is_penerima_lain_selected = false;
    //     $( "#tambah_anggota" ).autocomplete({
    //         source: "<?php echo site_url('web/ajax/allusers'); ?>",
    //         minLength: 2,
    //         select: function( event, ui ) {
    //             if (ui.item) {
    //                 $("#user_id_anggota").val(ui.item.id);
    //             }
    //         },
    //         open: function (event, ui) {
    //             $(".ui-autocomplete li.ui-menu-item:odd a").addClass("ui-menu-item-alternate");
    //             $(".ui-autocomplete li.ui-menu-item:hover a").addClass("ui-state-hover");
    //         },
    //         close: function (event, ui) {
    //         },
    //          messages: {
    //                 noResults: '',
    //                 results: function() {}
    //             }            
    //     })
    //     .data( "ui-autocomplete" )._renderItem = function( ul, item ) {
    //         return $( "<div>" )
    //             .data( "ui-autocomplete-item", item )
    //             .append( "<a>"+ item.label+ "</a>" )
    //             .appendTo( ul );
    //     };

    // });

    $('#btn_tambah_anggota').on('click', function () {
        id = $('#tambah_anggota').val();
        location.href = "<?= site_url('web/group/adduser/' . $data['id']) ?>/" + id ;
    });

    $('#btn_find').on('click',function(){
        location.href = "<?= site_url('web/group/detail/'.$data['id'].'?search_anggota=') ?>" + $('#search_anggota').val() ;
    });

    $(function () {
        $("#img_photo").click(function () {
            $("#image").click();

        });
        $("#image").change(function () {
            $("#label_photo").removeClass("hidden");
            $("#btn_simpan_foto").removeClass("hidden");
        });        
    });

    $("#btn_kirim_initial").on('click',function(){        
        var id_group = <?php echo $this->uri->segment(4) ?>;
        var initial = $("#initial_helpdesk").val();
        $.ajax({
            url : '<?= site_url('web/group/ajaxupdateinitial') ?>',
            type: 'post',
            cache: false,
            data: {"id":id_group,"initial":initial},
            success: function(result){
                if (result != 'ERROR') {
                    location.href = "<?= site_url('web/group/detail/') ?>/"+id_group;
                } else {
                    alert('Inisial group harus terdiri dari 3 huruf!');
                }
            } 
        });

    });

    $("#btn_kirim_group_type").on('click',function(){        
        var id_group = <?php echo $this->uri->segment(4) ?>;
        var group_type = $("#group_type").val();

        $.ajax({
            url : '<?= site_url('web/group/ajaxupdategrouptype') ?>',
            type: 'post',
            cache: false,
            data: {"id":id_group,"type":group_type},
            success: function(result){
                if (result != 'ERROR') {
                    location.reload();
                } else {
                    alert('Gagal mengubah tipe grup!');
                }
            } 
        });
    });

    $("#btn_kirim_quota").on('click',function(){        
        var id_group = <?php echo $this->uri->segment(4) ?>;
        var quota = $("#quota_limit").val();

        $.ajax({
            url : '<?= site_url('web/group/ajaxupdatequota') ?>',
            type: 'post',
            cache: false,
            data: {"id":id_group,"quota_limit":quota},
            success: function(result){
                if (result != 'ERROR') {
                    location.reload();
                } else {
                    alert('Gagal mengubah kuota!');
                }
            } 
        });
    });

    $("#btn_kirim_description").on('click',function(){        
        var id_group = <?php echo $this->uri->segment(4) ?>;
        var description = $("#description_helpdesk").val();
        $.ajax({
            url : '<?= site_url('web/group/ajaxupdatedescription') ?>',
            type: 'post',
            cache: false,
            data: {"id":id_group,"description":description},
            success: function(result){
                if (result != 'ERROR') {
                    location.reload();
                } else {
                    alert('Gagal mengubah deskripsi!');
                }
            } 
        });

    });

    $("#btn_change_group_type").on('click',function(){
        $("#data_group_type").addClass('hidden');
        $("#form_group_type").removeClass('hidden');
    });

    $("#btn_change_initial").on('click',function(){
        $("#data_initial").addClass('hidden');
        $("#form_initial").removeClass('hidden');
    });

    $("#btn_change_description").on('click',function(){
        $("#data_description").addClass('hidden');
        $("#form_description").removeClass('hidden');
    });

    $("#btn_change_quota").on('click',function(){
        $("#data_quota").addClass('hidden');
        $("#form_quota").removeClass('hidden');
    });

    $(".quota_limit").select2({
        placeholder: 'Masukkan limit quota',
        minimumResultsForSearch: Infinity
    });

    function goSave() {
        $("#form_data").submit();
    }

</script>