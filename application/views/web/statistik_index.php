<div class="row" id="main-container">
    <div class="col-sm-12 col-md-10" style="padding:0">
        <div class="panel panel-content">
            <div class="panel-heading">
                <h4 style="margin:0; font-weight:bold;font-size:26px">Statistik</h4>
            </div>
            <div class="panel-body menu-grid">

                <div class="col-xs-6 col-md-4">
                    <div class="thumbnail">
                        <a href="<?= ($is_kopertis) ? site_url('web/statistik/bebanKopertis') : site_url('web/statistik/beban') ?>">
                        <h1><i class="glyphicon glyphicon-calendar"></i></h1>
                        <div class="caption">
                            Beban
                        </div>
                        </a>
                    </div>
                </div>

                <div class="col-xs-6 col-md-4">
                    <div class="thumbnail">
                        <a href="<?= ($is_kopertis) ? site_url('web/statistik/performanceKopertis') : site_url('web/statistik/performance/?h='.$my_helpdesk_name) ?>">
                        <h1><i class="glyphicon glyphicon-th-list"></i></h1>
                        <div class="caption">
                            Performance
                        </div>
                        </a>
                    </div>
                </div>

                <?php if (!$is_kopertis) { ?>
                    <div class="col-xs-6 col-md-4">
                        <div class="thumbnail">
                            <a href="<?= site_url('web/statistik/ticket') ?>">
                            <h1><i class="glyphicon glyphicon-stats"></i></h1>
                            <div class="caption">
                                Ticket
                            </div>
                            </a>
                        </div>
                    </div>

                    <div class="col-xs-6 col-md-4">
                        <div class="thumbnail">
                            <a href="<?= site_url('web/statistik/rating') ?>">
                            <h1><i class="glyphicon glyphicon-star"></i></h1>
                            <div class="caption">
                                Rating
                            </div>
                            </a>
                        </div>
                    </div>

                    <div class="col-xs-6 col-md-4">
                        <div class="thumbnail">
                            <a href="<?= site_url('web/statistik/waktu') ?>">
                            <h1><i class="glyphicon glyphicon-time"></i></h1>
                            <div class="caption">
                                Waktu
                            </div>
                            </a>
                        </div>
                    </div>

                    <div class="col-xs-6 col-md-4">
                        <div class="thumbnail">
                            <a href="<?= site_url('web/statistik/keyword') ?>">
                            <h1><i class="glyphicon glyphicon-font"></i></h1>
                            <div class="caption">
                                Keyword
                            </div>
                            </a>
                        </div>
                    </div>

                    <div class="col-xs-6 col-md-4">
                        <div class="thumbnail">
                            <a href="<?= site_url('web/statistik/search') ?>">
                            <h1><i class="glyphicon glyphicon-search"></i></h1>
                            <div class="caption">
                                Search
                            </div>
                            </a>
                        </div>
                    </div>
                <?php } ?>
            </div>

        </div>
    </div>
</div>
