<div class="row">
    <div class="col-md-12">        
        <div class="box box-primary">
            <div class="box-body">
                <div class="row">
                    <div class="col-md-12">
                        <h2><?= $title ?> Rating <?= $chosen_helpdesk ?></h2>
                        <?php if (isset($date_start) and isset($date_end)) { ?>
                            <h6 style="color:grey">Periode : <?php echo $date_start." sampai ".$date_end ?></h6>
                        <?php } ?>
                        <?php if (SessionManagerWeb::isSuperAdministrator()) { ?>
                            <hr>
                            <form >
                                <label for="name" class="col-md-3">Tampilkan Statistik dari : </label>
                                <div class="btn-group" style="margin-left:-50px">
                                    <?php
                                        if ($chosen_helpdesk!='') { ?>
                                            <input type="button" class="btn btn-default btn-xs" id="btn_helpdesk" name="btn_helpdesk" value="<?php echo $chosen_helpdesk ?>">
                                       <?php } else { ?>
                                            <input type="button" class="btn btn-default btn-xs" id="btn_helpdesk" name="btn_helpdesk" value="Semua Unit">
                                        <?php }
                                    ?>
                                    
                                    <button type="button" class="btn btn-default btn-xs dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                        <span class="caret"></span>
                                    </button>
                                    <ul class="dropdown-menu">
                                        <li>
                                            <a style="text-align:left" class="set-helpdesk-dashboard" value="">
                                                Semua Unit
                                            </a>
                                        </li>
                                        <?php foreach ($list_helpdesk as $k => $v) { ?>
                                        <li>
                                            <a style="text-align:left" class="set-helpdesk-dashboard" value="<?= $v['name'] ?>">
                                                <?= $v['name'] ?>
                                            </a>
                                        </li>
                                        <?php } ?>
                                    </ul>
                                </div>
                            </form>
                        <?php } else { ?>
                            <hr style="display:none">
                            <form style="display:none">
                                <label for="name" class="col-md-3">Tampilkan Statistik dari : </label>
                                <div class="btn-group" style="margin-left:-50px">
                                    <?php
                                        if ($chosen_helpdesk!='') { ?>
                                            <input type="button" class="btn btn-default btn-xs" id="btn_helpdesk" name="btn_helpdesk" value="<?php echo $chosen_helpdesk ?>">
                                       <?php } else { ?>
                                            <input type="button" class="btn btn-default btn-xs" id="btn_helpdesk" name="btn_helpdesk" value="Semua Unit">
                                        <?php }
                                    ?>
                                    
                                    <button type="button" class="btn btn-default btn-xs dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                        <span class="caret"></span>
                                    </button>
                                    <ul class="dropdown-menu">
                                        <li>
                                            <a style="text-align:left" class="set-helpdesk-dashboard" value="">
                                                Semua Unit
                                            </a>
                                        </li>
                                        <?php foreach ($list_helpdesk as $k => $v) { ?>
                                        <li>
                                            <a style="text-align:left" class="set-helpdesk-dashboard" value="<?= $v['name'] ?>">
                                                <?= $v['name'] ?>
                                            </a>
                                        </li>
                                        <?php } ?>
                                    </ul>
                                </div>
                            </form>
                        <?php }?>
                        <hr>
                        <form >
                            <div class="input-group col-md-3" style="float:left">
                                <div class="input-group-addon" ><i class="fa fa-calendar"></i></div>
                                    <?php echo form_input(array('name' => 'date_started','class' => 'form-control input-sm', 'value' => (isset($date_start) ? $date_start : NULL),'placeholder' => 'dd-mm-yyyy', 'onchange' => 'dateChange()')) ?>
                            </div>
                            <center><span class="col-xs-1" style="width:3%">-</span></center>
                            <div class="input-group col-md-3" style="float:left">
                                <div class="input-group-addon"><i class="fa fa-calendar"></i></div>
                                    <?php echo form_input(array('name' => 'date_finished','class' => 'form-control input-sm', 'value' => (isset($date_end) ? $date_end : NULL),'placeholder' => 'dd-mm-yyyy', 'onchange' => 'dateChange()')) ?>
                            </div>
                            <span class="btn btn-primary col-md-1" name="btn-filter" id="btn-filter" style="padding-top:4px;padding-bottom: 4px;margin-left:20px">Filter</span>  
                            <span class="btn btn-success col-md-2" name="btn-showall" id="btn-showall" style="padding-top:4px;padding-bottom: 4px;margin-left:20px">Tampilkan Semua</span>  
                        </form>
                        <br>
                        <br>
                        <canvas id="myChart" width="800" height="350"></canvas>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">

    var ctx = document.getElementById("myChart").getContext('2d');
    var myChart = new Chart(ctx, {
        type: 'pie',
        data: {
            labels: ["1 star", "2 star", "3 star", "4 star", "5 star"],
            datasets: [
            {
                label: "Population (millions)",
                backgroundColor: ["#3e95cd", "#8e5ea2","#3cba9f","#e8c3b9","#c45850"],
                data: [<?php echo json_encode($data['rating_satu']) ?>,<?php echo json_encode($data['rating_dua']) ?>,<?php echo json_encode($data['rating_tiga']) ?>,<?php echo json_encode($data['rating_empat']) ?>,<?php echo json_encode($data['rating_lima']) ?>]
            }
            ],
        },
        options: {
            title: {
                display: true,
                text: 'Statistik Rating'
            },
            tooltips: {
                callbacks: {
                    label: function(tooltipItems, data) {
                        var dataset = data.datasets[tooltipItems.datasetIndex];
                        var total = dataset.data.reduce(function(previousValue, currentValue, currentIndex, array) {
                            return parseInt(previousValue) + parseInt(currentValue);
                        });
                        var currentValue = dataset.data[tooltipItems.index];
                        console.log(total);

                        return data.labels[tooltipItems.index] + 
                        " : " + Math.floor(((parseInt(currentValue)/total) * 100)+0.5) + '% (' + currentValue + ')';
                    }
                }
            }
        },
    });


    $( function() {
        $("[name='date_started'], [name='date_finished']").datepicker({format:'dd-mm-yyyy'});
    } );

    function dateChange(){
        var fStart = $("[name='date_started'");
        var fEnd = $("[name='date_finished'");

        var aStart = fStart.val().split('-');
        var aEnd = fEnd.val().split('-');
        var dStart = new Date(aStart[2], aStart[1], aStart[0]);
        var dEnd = new Date(aEnd[2], aEnd[1], aEnd[0]);
        if(dStart > dEnd){
            fEnd.val(fStart.val());
        }
    }

    $("#btn-filter").on('click',function(){
        if ($("[name='date_finished'").val()!='' && $("[name='date_finished'").val()!='') {
            var helpdesk = $('#btn_helpdesk').attr('value');
            if (helpdesk!='' || helpdesk!='Semua' || helpdesk!='Semua Unit'){
                location.href="<?= site_url('web/statistik/rating') ?>?h="+helpdesk+"&start_date="+$("[name='date_started'").val()+'&end_date='+$("[name='date_finished'").val();
            } else {
                location.href="<?= site_url('web/statistik/rating') ?>?start_date="+$("[name='date_started'").val()+'&end_date='+$("[name='date_finished'").val();
            }
        } else {
            alert('Tanggal mulai atau tanggal selesai tidak boleh kosong!');
        }
        
    });
    $("#btn-showall").on('click',function(){
        location.href="<?= site_url('web/statistik/rating') ?>?";
    });

    $(document).on('click', '.set-helpdesk-dashboard', function(){
        var val = $(this).attr('value');
        var start_date = $("[name='date_started'").val();
        var str_start = '';
        var str_end = '';
        if (start_date!=''){
            str_start = '&start_date='+start_date;
        }
        var end_date = $("[name='date_finished'").val();
        if (end_date!=''){
            str_end = '&end_date='+end_date;
        }
        if (val=="" || val=="Semua") {
            window.location.replace("<?= site_url('web/statistik/rating') ?>/?h="+str_start+str_end);
        } else {
            window.location.replace("<?= site_url('web/statistik/rating') ?>/?h="+val+str_start+str_end);
        }
    }); 

</script>