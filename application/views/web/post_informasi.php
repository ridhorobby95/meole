<div class="row">
    <div class="col-md-12">        
        <div class="box box-primary">
            <div class="box-body">
                <div class="row">
                    <div class="col-md-12">
                        <h2><?= $title ?></h2>
                        <div class="table-responsive">
                            <table class="table table-bordered table-condensed table-striped">
                                <thead>
                                    <tr>
                                        <th class="text-center col-md-1" style="width:30px">No</th>
                                        <th class="text-center">Informasi</th>
                                        <th class="text-center col-md-2">Oleh</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php
                                    $no = 0;
                                    $counter = 0;
                                    $total_view = 10;
                                    $page = $this->uri->segment(4);
                                    $view_max = ($page+1)*$total_view;
                                    $view_min = ($page*$total_view)+1;
                                    foreach ($data as $v) {
                                        $counter++;
                                        if ($counter>=$view_min && $counter<=$view_max) {
                                            $no = $counter;
                                            ?>
                                            <tr>
                                                <td class="text-right"><?= $no ?></td>
                                                <td><a href="<?= site_url('web/informasi/detail/' . $v['post_id']) ?>"><?= $v['post_description'] ?></a></td>
                                                <td><?= $v['user_name'] ?></td>
                                            </tr>
                                            <?php
                                        }
                                    }
                                    ?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
                <nav>
                    <ul class="pager">
                        <?php
                        if (empty($page) || $page == 0) {
                            echo '<li class="disabled"><a href = "#">Sebelumnya</a></li>';
                        } else {
                            echo '<li>' . anchor('web/' . $this->router->class . '/' . $this->router->method . '/' . ($page - 1) . Util::paramsToUrlGet($_GET), 'Sebelumnya') . '</li>';
                        }
                        #if (empty($data)) {
                        if ($no==$counter){
                            echo '<li class="disabled"><a href = "#">Selanjutnya</a></li>';
                        
                        } else {
                            echo '<li>' . anchor('web/' . $this->router->class . '/' . $this->router->method . '/' . ($page + 1) . Util::paramsToUrlGet($_GET), 'Selanjutnya') . '</li>';
                        }
                        ?>
                    </ul>
                </nav>
            </div>
        </div>
    </div>
</div>