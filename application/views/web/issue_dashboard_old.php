<div class="row menu-tab">
    <a class="menu-tab-item<?php echo ($method == 'dashboard') ? ' menu-tab-active' : '' ?>" href="<?php echo site_url($path . $class . '/dashboard') ?>">Dashboard</a>
    <a class="menu-tab-item<?php echo ($method == 'for_me') ? ' menu-tab-active' : '' ?>" href="<?php echo site_url($path . $class . '/for_me') ?>">Tugas Saya</a>
    <a class="menu-tab-item<?php echo ($method == 'me') ? ' menu-tab-active' : '' ?>" href="<?php echo site_url($path . $class . '/me') ?>">Beri Tugas</a>
</div>
<div class="row">
    <div class="col-sm-4 col-md-2">
        <div class="panel panel-primary">
            <div class="panel-heading text-center">FILTER</div>
            <div class="panel-body">
                <?php echo form_open() ?>
                <div class="row bord-bottom">
                    <label class="col-xs-12">Project</label>
                    <div class="col-xs-12">
                        <?php echo form_dropdown('project', $a_project, $_POST['project'], 'id="parent-id" class="form-control input-sm"') ?>
                    </div>
                </div>
                <div class="row bord-bottom">
                    <label class="col-xs-12">Jenis</label>
                    <div class="col-xs-12">
                        <?php echo form_dropdown('jenis', $a_type, $_POST['jenis'], 'id="parent-id" class="form-control input-sm"') ?>
                    </div>
                </div>
                <div class="row bord-bottom">
                    <label class="col-xs-12">Priority</label>
                    <div class="col-xs-12">
                        <?php echo form_dropdown('priority', $a_priority, $_POST['priority'], 'id="parent-id" class="form-control input-sm"') ?>
                    </div>
                </div>
                <div class="row bord-bottom">
                    <label for="date" class="col-xs-12">Tanggal Awal</label>
                    <div class="col-xs-12">
                        <div class="input-group">
                            <div class="input-group-addon"><i class="fa fa-calendar"></i></div>
                            <?php echo form_input(array('name' => 'start_date', 'value' => (!empty($_POST['start_date'])) ? $_POST['start_date'] : NULL, 'class' => 'form-control input-sm', 'placeholder' => 'dd-mm-yyyy')) ?>
                        </div>
                    </div>
                </div>
                <div class="row bord-bottom">
                    <label for="date" class="col-xs-12">Tanggal Akhir</label>
                    <div class="col-xs-12">
                        <div class="input-group">
                            <div class="input-group-addon"><i class="fa fa-calendar"></i></div>
                            <?php echo form_input(array('name' => 'end_date', 'value' => (!empty($_POST['end_date'])) ? $_POST['end_date'] : NULL, 'class' => 'form-control input-sm', 'placeholder' => 'dd-mm-yyyy')) ?>
                        </div>
                    </div>
                </div>
                <div class="row bord-bottom">
                    <label for="date" class="col-xs-12">Tampilkan Tugas</label>
                    <div class="col-xs-12">
                        <div class="radio">
                            <?php echo form_radio('assignment_task', 'T', (empty($_POST['assignment_task']) || $_POST['assignment_task'] == 'T' ? TRUE : FALSE)); ?> Team
                        </div>
                        <div class="radio">
                            <?php echo form_radio('assignment_task', 'M', ($_POST['assignment_task'] == 'M' ? TRUE : FALSE)); ?> Hanya Saya
                        </div>
                    </div>
                </div>
                <div class="row bord-bottom">
                    <div class="col-xs-12">
                        <button class="btn btn-success btn-sm btn-block"><i class="fa fa-search"></i> Filter</button>
                    </div>
                </div>
                </form>
            </div>
        </div>
    </div>
    <?php
    $panel = array(
        1 => 'default',
        2 => 'info',
        3 => 'warning',
        4 => 'success',
        5 => 'danger',
    );
    foreach ($data as $key => $value) {
        ?>
        <div class="col-sm-4 col-md-2">
            <div class="panel panel-<?= $panel[$key] ?>">
                <div class="panel-heading text-center"><?= $a_status[$key] ?></div>
                <div class="panel-body">
                    <b><?= count($value) ?> Task</b>
                </div>
                <ul class="list-group" style="max-height: 600px; overflow-y: auto">
                    <?php foreach ($value as $issue) { ?>
                        <li class="list-group-item" style="cursor: pointer;padding: 10px 5px" data-toggle="modal" data-target="#modal-issue-<?= $issue['id'] ?>">
                            <p>
                                <?php foreach ($issue['postUsers'] as $vu) { ?>
                                    <label class="label label-<?= ($selfId == $vu['id']) ? 'warning' : 'info' ?>"><?= strtoupper($vu['username']) ?></label>
                                <?php } ?>
                            </p>
                            <p><?= $issue['title'] ?></p>
                            <?php
                            switch ($issue['priority']) {
                                case Issue_model::PRIORITY_MINOR: $label = 'MINOR';
                                    $labelc = 'success';
                                    break;
                                case Issue_model::PRIORITY_NORMAL: $label = 'NORMAL';
                                    $labelc = 'info';
                                    break;
                                case Issue_model::PRIORITY_IMPORTANT: $label = 'IMPORTANT';
                                    $labelc = 'warning';
                                    break;
                                case Issue_model::PRIORITY_URGENT: $label = 'URGENT';
                                    $labelc = 'danger';
                                    break;
                                default: unset($label, $labelc);
                            }
                            if (!empty($label)) {
                                ?>
                                <label class="label label-<?php echo $labelc ?>"><?php echo $label ?></label>
                            <?php } ?>
                            <label class="label label-danger"><?php echo FormatterWeb::toIndoDate($issue['deadline']) ?></label>
                            <label class="label label-info"><?php echo $a_type[$issue['categoryId']] ?></label>
                            <?php if (!empty($issue['group'])) { ?>
                                <label class="label label-primary"><?= $issue['group']['name'] ?></label>
                            <?php } ?>
                        </li>
                    <?php } ?>
                </ul>
            </div>
            <?php 
            $i = 0;
            foreach ($value as $kd => $v) { ?>
                <div class="modal fade" id="modal-issue-<?= $v['id'] ?>" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
                    <div class="modal-dialog modal-lg" role="document">
                        <div class="modal-content">
                            <div class="modal-body">
                                <div class="post-section post-cat post-cat-<?php echo $v['categoryId'] ?>" style="margin-bottom: 0">
                                    <div>
                                        <div class="site-photo site-photo-48 site-photo-left">
                                            <img src="<?php echo isset($v['user']['photo']) ? site_url($path . 'thumb/watermark/' . $v['user']['id'] . '/48') : $nopic ?>" />
                                        </div>
                                        <div>
                                            <strong><?php echo $v['user']['name'] ?></strong>
                                            <?php
                                            if ($v['type'] == Post_model::TYPE_FRIEND and ! empty($v['postUsers'])) {
                                                $a_nama = array();
                                                foreach ($v['postUsers'] as $vu)
                                                    $a_nama[] = $vu['name'];
                                                ?>
                                                mengirim ke <strong><?php echo implode(', ', $a_nama) ?></strong>
                                            <?php } ?>
                                        </div>
                                        <div class="post-header-info">
                                            <?php echo FormatterWeb::toIndoDate($v['date'], true) ?>
                                        </div>
                                    </div>
                                    <div class="post-body">
                                        <p>
                                            <?php
                                            switch ($v['priority']) {
                                                case Issue_model::PRIORITY_MINOR: $label = 'MINOR';
                                                    $labelc = 'success';
                                                    break;
                                                case Issue_model::PRIORITY_NORMAL: $label = 'NORMAL';
                                                    $labelc = 'info';
                                                    break;
                                                case Issue_model::PRIORITY_IMPORTANT: $label = 'IMPORTANT';
                                                    $labelc = 'warning';
                                                    break;
                                                case Issue_model::PRIORITY_URGENT: $label = 'URGENT';
                                                    $labelc = 'danger';
                                                    break;
                                                default: unset($label, $labelc);
                                            }
                                            if (!empty($label)) {
                                                ?>
                                                <label class="label label-<?php echo $labelc ?>"><?php echo $label ?></label>
                                            <?php } ?>
                                            <?php
                                            switch ($v['status']) {
                                                case Issue_model::STATUS_OPEN: $label = 'OPEN';
                                                    $labelc = 'info';
                                                    break;
                                                case Issue_model::STATUS_START: $label = 'STARTED';
                                                    $labelc = 'info';
                                                    break;
                                                case Issue_model::STATUS_PROGRESS: $label = 'PROGRESS';
                                                    $labelc = 'info';
                                                    break;
                                                case Issue_model::STATUS_DONE: $label = 'DONE';
                                                    $labelc = 'success';
                                                    break;
                                                case Issue_model::STATUS_REOPEN: $label = 'REOPEN';
                                                    $labelc = 'warning';
                                                    break;
                                                case Issue_model::STATUS_CLOSE: $label = 'CLOSE';
                                                    $labelc = 'success';
                                                    break;
                                                case Issue_model::STATUS_INVALID: $label = 'INVALID';
                                                    $labelc = 'success';
                                                    break;
                                                case Issue_model::STATUS_DUPLICATE: $label = 'DUPLICATE';
                                                    $labelc = 'success';
                                                    break;
                                                default: unset($label, $labelc);
                                            }
                                            if (!empty($label)) {
                                                ?>
                                                <label class="label label-<?php echo $labelc ?>"><?php echo $label ?></label>
                                            <?php } ?>
                                            <label class="label label-danger">Deadline: <?php echo FormatterWeb::toIndoDate($v['deadline']) ?></label>
                                        </p>
                                        <?php if (!empty($v['group'])) { ?>
                                            <p><strong><?php echo '[PROJECT] ' . $v['group']['name'] ?></strong></p>
                                        <?php } ?>
                                        <p><strong><?php echo $v['title'] ?></strong></p>
                                        <p><?php echo nl2br($v['description']) ?></p>
                                        <?php if (!empty($v['image'])) { ?>
                                            <p><img src="<?php echo TemplateManagerWeb::getPostImageLink($v['image']) ?>" /></p>
                                        <?php } ?>
                                        <?php if (!empty($v['file'])) { ?>
                                            <p><a href="<?php echo $v['file']['link'] ?>"><?php echo $v['file']['name'] ?></a></p>
                                        <?php } ?>
                                        <?php if (!empty($v['link'])) { ?>
                                            <p><a href="<?php echo $v['link'] ?>"><?php echo $v['link'] ?></a></p>
                                        <?php } ?>
                                    </div>
                                    <div class="post-footer">
                                        <a href="<?php echo site_url($path . $class . '/detail/' . $v['id']) ?>"><?php echo count($v['comments']) ?> komentar</a>
                                        <?php if (!in_array($v['status'], array(Issue_model::STATUS_CLOSE, Issue_model::STATUS_INVALID, Issue_model::STATUS_DUPLICATE))) { ?>
                                            <a class="post-right" href="javascript:showStatus('<?php echo $v['id'] ?>','<?php echo $v['status'] ?>')">Ganti Status</a>
                                        <?php } ?>
                                        <?php if ($v['user']['id'] == SessionManagerWeb::getUserID()) { ?>
                                            <?php /* <a class="post-right" href="<?php echo site_url($path.$class.'/edit/'.$v['id']) ?>">Edit</a> */ ?>
                                            <a class="post-right post-delete" href="javascript:goDelete('<?php echo $v['id'] ?>')">Hapus</a>
                                        <?php } ?>
                                    </div>
                                    <hr>
                                    <table class="comment-table" border="0">
                                        <?php foreach ($v['comments'] as $vd) { ?>
                                            <tr>
                                                <td>
                                                    <div class="site-photo site-photo-48">
                                                        <img src="<?php echo isset($vd['user']['photo']) ? site_url($path . 'thumb/watermark/' . $vd['user']['id'] . '/48') : $nopic ?>">
                                                    </div>
                                                </td>
                                                <td class="comment-info">
                                                    <strong><?php echo $vd['user']['name'] ?></strong>
                                                    <br /><?php echo $vd['text'] ?>
                                                    <div class="post-header-info">
                                                        <?php echo FormatterWeb::dateToDiff($vd['createdAt']) ?>
                                                        <?php if ($vd['user']['id'] == SessionManagerWeb::getUserID() && $vd['candelete'] == 1) { ?>
                                                            <a class="post-right post-delete" href="javascript:goDeleteComment('<?php echo $vd['id'] ?>')">Hapus</a>
                                                        <?php } ?>
                                                    </div>
                                                </td>
                                            </tr>
                                        <?php } ?>
                                        <tr>
                                            <td colspan="2">
                                                <?php echo form_open($path . 'comment/create') ?>
                                                <div class="input-group">
                                                    <?php echo form_input(array('name' => 'text', 'class' => 'form-control', 'placeholder' => 'Tulis komentar')) ?>
                                                    <?php echo form_hidden('post_id', $v['id']) ?>
                                                    <div class="input-group-btn">
                                                        <button type="submit" class="btn btn-success">
                                                            <i class="fa fa-check"></i> Kirim
                                                        </button>
                                                    </div>
                                                </div>
                                                <?php echo form_hidden('referer', base_url(uri_string())) ?>
                                                </form>
                                            </td>
                                        </tr>
                                    </table>
                                </div>
                            </div>
                            <div class="modal-footer" style="margin-top: 0">
                                <?php if($i != 0) { ?>
                                <button type="button" class="btn btn-warning" onclick="goShowDialog('#modal-issue-<?= $value[($kd-1)]['id'] ?>','#modal-issue-<?= $v['id'] ?>')"><i class="fa fa-arrow-left"></i> Prev</button>
                                <?php }
                                if($i != (count($value)-1)) { ?>
                                <button type="button" class="btn btn-warning" onclick="goShowDialog('#modal-issue-<?= $value[($kd+1)]['id'] ?>','#modal-issue-<?= $v['id'] ?>')">Next <i class="fa fa-arrow-right"></i></button>
                                <?php } ?>
                                <button type="button" class="btn btn-danger" data-dismiss="modal"><i class="fa fa-close"></i> Close</button>
                            </div>
                        </div>
                    </div>
                </div>
            <?php $i++; } ?>
        </div>
    <?php } ?>
</div>
<?php echo form_open_multipart($path . $class . '/set_status', array('id' => 'form_modal')) ?>
<div class="modal fade" id="modal_form" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Ganti Status</h4>
            </div>
            <div class="modal-body">
                <div class="row  bord-bottom">
                    <label class="col-md-3">Status Baru</label>
                    <div class="col-md-4">
                        <?php echo form_dropdown('status', $a_status, null, 'id="status" class="form-control input-sm"') ?>
                    </div>
                </div>
                <div class="row">
                    <label class="col-md-3">Keterangan</label>
                    <div class="col-md-9">
                        <?php echo form_textarea('description', null, 'id="status" class="form-control" rows="5"') ?>
                    </div>
                </div>
            </div>
            <div class="modal-footer" align="center">
                <button type="submit" class="btn btn-success"><i class="fa fa-check"></i> Ganti</button>
                <button type="button" class="btn btn-primary" data-dismiss="modal"><i class="fa fa-times"></i> Batal</button>
            </div>
        </div>
    </div>
</div>
</form>

<?php echo form_open(null, array('id' => 'form_delete')) ?>
<?php echo form_hidden('referer') ?>
</form>
<script type="text/javascript">

    var back = "<?php echo base_url(uri_string()) ?>";
    sessionStorage.setItem("<?php echo $class ?>.back", back);

    $("[name='start_date'],[name='end_date']").datepicker({format: 'dd-mm-yyyy'});

    function goAdd() {
        location.href = "<?php echo site_url($path . $class . '/add') ?>";
    }

    function goDelete(id) {
        var hapus = confirm("Apakah anda yakin akan menghapus tugas ini?");
        if (hapus) {
            $("#form_delete").attr("action", "<?php echo site_url($path . $class . '/delete') ?>/" + id);
            $("#form_delete [name='referer']").val(back);
            $("#form_delete").submit();
        }
    }

    function showStatus(id, status) {
        $("#status").val(status);
        $("#form_modal").attr("action", "<?php echo site_url($path . $class . '/set_status') ?>/" + id);

        $("#modal_form").modal();
    }
    
    function goShowDialog(show, hide){
        $(hide).modal("hide");
        $(show).modal("show");
    }
    
    /*setTimeout(function (){
        location.reload();
    }, 60000);*/

</script>