<div class="row">
    <div class="col-md-12">
        <div class="box box-primary">
            <div class="box-header">
                <div class="col-md-12">
                    <?php 
                        $status=$this->uri->segment(3);
                        //die($status);
                        if ($status=='edit') {
                            $show="Edit";
                        } else {
                            $show="Tambahkan";
                        }
                    ?>
                    <h4 style="text-align: center;"><strong><?php echo $show;?> Hari Libur</strong></h4>
                </div>
            </div>
            <div class="box-body">
                <div class="row">
                    <?php 
                    echo form_open_multipart($path . $class . '/' . (isset($data['id']) ? 'update/' . $data['id'] : 'create'), array('id' => 'form_data'));
                    ?>

                    <div class="col-sm-8">
                        <div class="row bord-bottom">
                            <label for="judul" class="col-sm-4">Name</label>
                            <div class="col-sm-8">
                                <?php echo form_input(array('name' => 'name', 'value' => $data['name'], 'class' => 'form-control input-sm', 'id' => 'name','rows' => 1)) ?>
                            </div>
                        </div>
                        <div class="row bord-bottom">
                            <label for="judul" class="col-sm-4">Date</label>
                            <div class="col-sm-8">
                                <?php echo form_input(array('name' => 'date', 'value' => $data['date'] ? date("d M Y", strtotime($data['date'])) : NULL, 'class' => 'form-control input-sm datepicker', 'data-date-format' => 'dd M yyyy', 'id' => 'date','rows' => 2)) ?>
                            </div>
                        </div>
                        <div class="row bord-bottom">
                            <label for="deskripsi" class="col-sm-4">Deskripsi</label>
                            <div class="col-sm-8">
                                <?php echo form_textarea(array('name' => 'description', 'value' => strip_tags($data['description']), 'class' => 'form-control input-sm', 'id' => 'description', 'rows' => 4)) ?>
                            </div>
                        </div>
                        <div class="row bord-bottom">
                            <div class="col-sm-12">
                                <span class="btn btn-sm btn-kirim" style="float:right"><i class="fa fa-save"></i> Simpan</span>
                            </div>
                        </div>
                    </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>



<script type="text/javascript">
    function goBack() {
        location.href = "<?php echo site_url($path . $class) ?>";
    }

    function goSave() {
        $("#form_data").submit();
    }

    $(".btn-kirim").on('click',function(){
         if ($("[name='name']").val()=="") {
            alert("Nama Tidak Boleh Kosong");
        } else if ($("[name='start_date']").val()==""){
            alert("Tanggal Awal Tidak Boleh Kosong");
        } else if ($("[name='end_date']").val()=="" ){
            alert("Tanggal Akhir Tidak Boleh Kosong");
        } else {
            $("#form_data").submit();
        }
    });

    $(".datepicker").datepicker();
</script>