<div class="row">
    <div class="col-md-12">        
        <div class="box box-primary">
            <div class="box-body">
                <div class="row">
                    <div class="col-md-12">
                        <h2><?= $title ?> Ticket <?= $chosen_helpdesk ?></h2>
                        <?php if (isset($date_start) and isset($date_end)) { ?>
                            <h6 style="color:grey">Periode : <?php echo $date_start." sampai ".$date_end ?></h6>
                        <?php } ?>
                        <?php if (SessionManagerWeb::isSuperAdministrator()) { ?>
                            <hr>
                            <form >
                                <label for="name" class="col-md-3">Tampilkan Statistik dari : </label>
                                <div class="btn-group" style="margin-left:-50px">
                                    <?php
                                        if ($chosen_helpdesk!='') { ?>
                                            <input type="button" class="btn btn-default btn-xs" id="btn_helpdesk" name="btn_helpdesk" value="<?php echo $chosen_helpdesk ?>">
                                       <?php } else { ?>
                                            <input type="button" class="btn btn-default btn-xs" id="btn_helpdesk" name="btn_helpdesk" value="Semua Unit">
                                        <?php }
                                    ?>
                                    
                                    <button type="button" class="btn btn-default btn-xs dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                        <span class="caret"></span>
                                    </button>
                                    <ul class="dropdown-menu">
                                        <li>
                                            <a style="text-align:left" class="set-helpdesk-dashboard" value="">
                                                Semua Unit
                                            </a>
                                        </li>
                                        <?php foreach ($list_helpdesk as $k => $v) { ?>
                                        <li>
                                            <a style="text-align:left" class="set-helpdesk-dashboard" value="<?= $v['name'] ?>">
                                                <?= $v['name'] ?>
                                            </a>
                                        </li>
                                        <?php } ?>
                                    </ul>
                                </div>
                            </form>
                        <?php } else { ?>
                            <hr style="display:none">
                            <form style="display:none">
                                <label for="name" class="col-md-3">Tampilkan Statistik dari : </label>
                                <div class="btn-group" style="margin-left:-50px">
                                    <?php
                                        if ($chosen_helpdesk!='') { ?>
                                            <input type="button" class="btn btn-default btn-xs" id="btn_helpdesk" name="btn_helpdesk" value="<?php echo $chosen_helpdesk ?>">
                                       <?php } else { ?>
                                            <input type="button" class="btn btn-default btn-xs" id="btn_helpdesk" name="btn_helpdesk" value="Semua Unit">
                                        <?php }
                                    ?>
                                    
                                    <button type="button" class="btn btn-default btn-xs dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                        <span class="caret"></span>
                                    </button>
                                    <ul class="dropdown-menu">
                                        <li>
                                            <a style="text-align:left" class="set-helpdesk-dashboard" value="">
                                                Semua Unit
                                            </a>
                                        </li>
                                        <?php foreach ($list_helpdesk as $k => $v) { ?>
                                        <li>
                                            <a style="text-align:left" class="set-helpdesk-dashboard" value="<?= $v['name'] ?>">
                                                <?= $v['name'] ?>
                                            </a>
                                        </li>
                                        <?php } ?>
                                    </ul>
                                </div>
                            </form>
                        <?php }?>
                        <hr>
                        <h6><i>NB : untuk hasil yang bagus, pilih tanggal dengan periode 2 bulan atau lebih</i></h6>
                        <form >
                            <div class="input-group col-md-3" style="float:left">
                                <div class="input-group-addon"><i class="fa fa-calendar"></i></div>
                                    <?php echo form_input(array('name' => 'date_started','class' => 'form-control input-sm', 'value' => (isset($date_start) ? $date_start : NULL),'placeholder' => 'dd-mm-yyyy', 'onchange' => 'dateChange(\'s\')')) ?>
                            </div>
                            <center><span class="col-xs-1" style="width:3%">-</span></center>
                            <div class="input-group col-md-3" style="float:left">
                                <div class="input-group-addon"><i class="fa fa-calendar"></i></div>
                                    <?php echo form_input(array('name' => 'date_finished','class' => 'form-control input-sm', 'value' => (isset($date_end) ? $date_end : NULL),'placeholder' => 'dd-mm-yyyy', 'onchange' => 'dateChange(\'e\')')) ?>
                            </div>
                            <span class="btn btn-primary col-md-1" name="btn-filter" id="btn-filter" style="padding-top:4px;padding-bottom: 4px;margin-left:20px">Filter</span>  
                            <span class="btn btn-success col-md-2" name="btn-showall" id="btn-showall" style="padding-top:4px;padding-bottom: 4px;margin-left:20px">Tampilkan Semua</span> 

                            <span class="btn btn-danger" name="btn-lowratefilter" id="btn-lowratefilter" style="padding-top:4px;padding-bottom: 4px;margin-top:20px">Dashboard Rating Rendah</span> 
                        </form>

                        <br>
                        <br>
                        <hr>
                        <div class="table">
                            <div class="row" style="padding-bottom: 10px">
                                <div class="col-md-12" style="padding:3px">
                                    <canvas id="chartTicket" width="500" height="120"></canvas>
                                </div>
                            </div>
                            <div class="row" style="padding-bottom: 10px">
                                <div class="col-md-12" style="padding:3px">
                                    <canvas id="chartWaktu" width="500" height="120"></canvas>
                                </div>
                            </div>
                            <div class="row" style="margin: 50px 0px 10px 0px">
                                <div style="text-align: center;"><b>Statistik Rating</b></div>
                            </div>
                            <div class="row" style="padding-bottom: 10px">
                                <div class="col-md-12" style="padding:3px">
                                    <canvas id="lineChartRating" width="500" height="120"></canvas>
                                </div>
                            </div>
                            <div class="row" style="padding-bottom: 10px">
                                <div class="col-md-12" style="padding:3px">
                                    <canvas id="hBarChartRating" width="500" height="120"></canvas>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>



<script type="text/javascript">
    var elementTicket = document.getElementById("chartTicket").getContext('2d');
    var elementWaktu = document.getElementById("chartWaktu").getContext('2d');
    var elementLineRating = document.getElementById("lineChartRating").getContext('2d');
    var elementBarRating = document.getElementById("hBarChartRating").getContext('2d');

    var chartTicket = new Chart(elementTicket, {
        type: 'line',
        data: {
            labels: <?php echo json_encode($month) ?>,
            datasets: [
            {
                label: "Ticket Created",
                data: <?php echo json_encode($ticket_open) ?>,
                borderColor: "red",
                fill: false
            },
            {
                label: "Ticket Solved",
                data: <?php echo json_encode($ticket_solved) ?>,
                borderColor: "blue",
                fill: false
            }
            ],
        },
        options: {
            title: {
                display: true,
                text: 'Statistik Tiket'
            },
            scales: {
                yAxes: [{
                    display:true,
                    scaleLabel: {
                        display: true,
                        labelString: 'Jumlah Ticket'
                    },
                    ticks: {
                        beginAtZero:true
                    }
                }],
                xAxes: [{
                    display: true,
                    scaleLabel: {
                        display: true,
                        labelString: 'Bulan-Tahun'
                    }
                }]

            },
            elements: {
                line: {
                    tension: 0
                }
            }
        }
    });

    var chartWaktu = new Chart(elementWaktu, {
        type: 'line',
        data: {
            labels: <?php echo json_encode($month); ?>,
            datasets: [
            {
                label: "Response Time",
                data: <?php echo json_encode($response_time); ?>,
                borderColor: "red",
                fill: false
            },
            {
                label: "Processing Time",
                data: <?php echo json_encode($processing_time); ?>,
                borderColor: "orange",
                fill: false
            },
            {
                label: "Solving Time",
                data: <?php echo json_encode($solving_time); ?>,
                borderColor: "green",
                fill: false
            }
            ],
        },
        options: {
            title: {
                display: true,
                text: 'Statistik Waktu'
            },
            scales: {
                yAxes: [{
                    display:true,
                    scaleLabel: {
                        display: true,
                        labelString: 'Hari'
                    },
                    ticks: {
                        beginAtZero:true
                    }
                }],
                xAxes: [{
                    display: true,
                    scaleLabel: {
                        display: true,
                        labelString: 'Bulan-Tahun'
                    }
                }]

            },
            elements: {
                line: {
                    tension: 0
                }
            }
        }
    });

    var lineChartRating = new Chart(elementLineRating, {
        type: 'line',
        data: {
            labels: <?php echo json_encode($month); ?>,
            datasets: [
            {
                label: "Solved",
                data: <?php echo json_encode(Util::toList($rating, 'rating_puas')); ?>,
                borderColor: "green",
                fill: false
            },
            {
                label: "Unsolved",
                data: <?php echo json_encode(Util::toList($rating, 'rating_tidak_puas')); ?>,
                borderColor: "red",
                fill: false
            },]
        },
        options: {
            scales: {
                yAxes: [{
                    display:true,
                    scaleLabel: {
                        display: true,
                        labelString: 'Persentase'
                    },
                    ticks: {
                        beginAtZero:true
                    }
                }],
                xAxes: [{
                    display: true,
                    scaleLabel: {
                        display: true,
                        labelString: 'Bulan-Tahun'
                    }
                }]

            },
            elements: {
                line: {
                    tension: 0
                }
            }
        }
    });

    var barChartRating = new Chart(elementBarRating, {
        type: 'horizontalBar',
        data: {
            labels: ["5 star", "4 star", "3 star", "2 star", "1 star"],
            datasets: [
            {
                label: "Persentase Rating",
                backgroundColor: "#f4ee41",
                data: [<?php echo json_encode($data['rating_lima']) ?>,<?php echo json_encode($data['rating_empat']) ?>,<?php echo json_encode($data['rating_tiga']) ?>,<?php echo json_encode($data['rating_dua']) ?>,<?php echo json_encode($data['rating_satu']) ?>]
            }
            ],
        },
        options: {
            tooltips: {
                callbacks: {
                    label: function(tooltipItems, data) {
                        var dataset = data.datasets[tooltipItems.datasetIndex];
                        var total = dataset.data.reduce(function(previousValue, currentValue, currentIndex, array) {
                            return parseInt(previousValue) + parseInt(currentValue);
                        });
                        var currentValue = dataset.data[tooltipItems.index];
                        console.log(total);

                        return data.labels[tooltipItems.index] + 
                        " : " + Math.floor(((parseInt(currentValue)/total) * 100)+0.5) + '% (' + currentValue + ')';
                    }
                }
            }
        },
    });

    $( function() {
        $("[name='date_started'], [name='date_finished']").datepicker({
            format:'dd-mm-yyyy'
        });
    } );

    function dateChange(e){
        if(e == 's')
            eo = 'e';
        else
            eo = 's';

        var component = {s:$("[name='date_started'"), e:$("[name='date_finished'")}; 

        var aStart = component['s'].val().split('-');
        var aEnd = component['e'].val().split('-');
        var date = {s:new Date(parseInt(aStart[2]), parseInt(aStart[1])-1, parseInt(aStart[0])), e:new Date(parseInt(aEnd[2]), parseInt(aEnd[1])-1, parseInt(aEnd[0]))};

        if(date[e] > new Date()){
            component[e].val(component[eo].val());
        }

        if(date['s'] > date['e']){
            component['e'].val(component['s'].val());
        }
    }

    $(document).on('click', '.set-helpdesk-dashboard', function(){
        var val = $(this).attr('value');
        var start_date = $("[name='date_started'").val();
        var str_start = '';
        var str_end = '';
        if (start_date!=''){
            str_start = '&start_date='+start_date;
        }
        var end_date = $("[name='date_finished'").val();
        if (end_date!=''){
            str_end = '&end_date='+end_date;
        }
        if (val=="" || val=="Semua") {
            window.location.replace("<?= site_url('web/statistik/ticket') ?>/?h="+str_start+str_end);
        } else {
            window.location.replace("<?= site_url('web/statistik/ticket') ?>/?h="+val+str_start+str_end);
        }
    }); 

    $("#btn-lowratefilter").on('click',function(){
        if ($("[name='date_finished'").val()!='' && $("[name='date_finished'").val()!='') {
            var helpdesk = $('#btn_helpdesk').attr('value');
            if (helpdesk!='' || helpdesk!='Semua' || helpdesk!='Semua Unit'){
                location.href="<?= site_url("web/post/lowRatingDashboard") ?>?h="+helpdesk+"&start_date="+$("[name='date_started'").val()+'&end_date='+$("[name='date_finished'").val();
            } else {
                location.href="<?= site_url("web/post/lowRatingDashboard") ?>?start_date="+$("[name='date_started'").val()+'&end_date='+$("[name='date_finished'").val();
            }

            
        } else {
            alert('Tanggal mulai atau tanggal selesai tidak boleh kosong!');
        }
        
    });
    $("#btn-filter").on('click',function(){
        if ($("[name='date_finished'").val()!='' && $("[name='date_finished'").val()!='') {
            var helpdesk = $('#btn_helpdesk').attr('value');
            if (helpdesk!='' || helpdesk!='Semua' || helpdesk!='Semua Unit'){
                location.href="<?= site_url('web/statistik/ticket') ?>?h="+helpdesk+"&start_date="+$("[name='date_started'").val()+'&end_date='+$("[name='date_finished'").val();
            } else {
                location.href="<?= site_url('web/statistik/ticket') ?>?start_date="+$("[name='date_started'").val()+'&end_date='+$("[name='date_finished'").val();
            }

            
        } else {
            alert('Tanggal mulai atau tanggal selesai tidak boleh kosong!');
        }
        
    });
    $("#btn-showall").on('click',function(){
        location.href="<?= site_url('web/statistik/ticket') ?>?";
    });
</script>