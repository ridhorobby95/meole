<!DOCTYPE html>
<html class="login-page">

<head>
    <meta charset="UTF-8">
    <title>Integra E-Office</title>
    <meta content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no' name='viewport'>
    <link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/web/css/bootstrap.min.css') ?>">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/web/css/font-awesome.min.css') ?>">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/web/css/ionicons.min.css') ?>">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/web/css/style.css') ?>">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/web/css/login.css') ?>">
    <link rel="icon" type="image/png" href="<?php echo base_url('assets/web/img/logo.png') ?>">
    <script type="text/javascript" src="<?php echo base_url('assets/web/js/external/html5shiv.js') ?>"></script>
    <script type="text/javascript" src="<?php echo base_url('assets/web/js/external/respond.min.js') ?>"></script>
</head>

<body>
    <div id="preloader">
        <div class="text-center" style="margin-bottom:-10px;">
            <img src="<?php echo base_url('assets/web/img/logo.png') ?>" height="180" id="preloader-content">
        </div>
    </div>
    <div class="container-fluid">
        <div id="main">
            <div class="col-md-12" id="pattern">
                <div class="text-center" id="main_left">

                    <div class="text-center" style="margin-bottom:-10px;">
                        <img src="<?php echo base_url('assets/web/img/logo.png') ?>" height="150">
                    </div>
                    <div class="container">
                        <div class="col-md-7 col-md-push-3">
                            <img class="pull-left logo-sigap" src="<?php echo base_url('assets/web/img/sigap.png') ?>" height="60">
                            <h3 class="login-title pull-left">Sistem Informasi Jaringan<br/> Pengelola PDDIKTI</h3>
                        </div>
                        <div class="clearfix"></div><br/>
                        <div class="col-md-8 col-md-push-2">

                            <?php if(!empty($errmsg)) { ?>

                            <div class="alert alert-danger">
                                <?php echo implode('<br>',$errmsg) ?>
                            </div>
                            <?php } ?>
                            <button type="button" class="btn-login text-shadow" id="btn-intro">Masuk</button>
                        </div>
                    </div>
                    <div class="form-login">
                        <?php echo form_open(); ?>
                        <div class="container">
                            <div class="col-md-8 col-md-push-2">
                                <div class="col-md-6">
                                    <h4><b>Nama Pengguna</b></h4>
                                    <?= form_input('username', set_value('username'), 'class="form-control input-login" required autofocus') ?>
                                </div>
                                <div class="col-md-6">
                                    <h4><b>Sandi</b></h4>
                                    <?php echo form_password(array('name' => 'password','class' => 'form-control input-login')) ?>
                                    <p class="text-shadow pull-right"><a href="#">Lupa Kata Sandi?</a></p>
                                </div>
                            </div>
                            <div class="col-md-12 text-center">
                                <?php echo form_submit(array('value' => 'Masuk','class' => 'btn-login text-shadow', 'id' => 'btn_masuk_akun')) ?>
                                <br/>
                            </div>
                        </div>
                    </div>
                    <h4 class="text-shadow">Belum punya akun? Buat <a href="#">di sini</a></h4>
                </div>
            </div>

        </div>
        <!--<div class="col-md-4" id="main_cont">
				<div class="form-box" id="login-box">
					<div class="text-center" id="header">
						<img src="<?php echo base_url('assets/web/img/logo.png') ?>" height="85">
						<h3>Silahkan Login</h3>
						<p><h5>Masukkan akun Anda dibawah ini </h5></p>
					</div>
					
					<?php echo form_open(); ?>
						<div>
							<?php if(!empty($errmsg)) { ?>
							<div class="alert alert-danger">
								<?php echo implode('<br>',$errmsg) ?>
							</div>
							<?php } ?>
						</div>
						<div class="footer" style="margin-top:50px;">
							<h6><b>Alamat Email</b></h6>
							<?= form_input('username', set_value('username'), 'class="form-control" placeholder="Alamat Email" required autofocus') ?></br>
							<h6><b>Kata Sandi</b></h6>
							<?php echo form_password(array('name' => 'password','class' => 'form-control','placeholder' => 'Kata Sandi')) ?>
							<p><div class="pull-right"><h6><a href="javascript:goLupa()<?php // echo site_url($path.'user/lupapw') ?>">Lupa Password?</a></h6></div>
							
							<?php echo form_submit(array('value' => 'MASUK APLIKASI','class' => 'btn btn-info btn-block', 'id' => 'btn_masuk_akun',)) ?>
						</div>
					</form>
					<div class="support text-center">
						<h5><b>DIPERSEMBAHKAN OLEH</b></h5>
						<img src="<?php echo base_url('assets/web/img/Logo_Kemenristekdikti.png') ?>" height="60">
					</div>
				</div>
		</div>-->
    </div>
    </div>

    <script type="text/javascript" src="<?php echo base_url('assets/web/js/external/jquery.min.js') ?>"></script>
    <script type="text/javascript" src="<?php echo base_url('assets/web/js/bootstrap.min.js') ?>"></script>
    <script type="text/javascript">
        $(document).ready(function() {
            $('#main_left').css("padding-top", Math.max(0, (
                    ($(window).height() - $('#main_left').outerHeight()-125) / 2) + $(window).scrollTop()) + "px");
            $('#btn-intro').click(function() {
                $('.form-login').slideDown(400);
                $(this).hide();
            });
        });
        $.fn.center = function() {
            this.css("position", "absolute");
            this.css("top", Math.max(0, (
                    ($(window).height() - $(this).outerHeight()) / 2) +
                $(window).scrollTop()) + "px");
            this.css("left", Math.max(0, (
                    ($(window).width() - $(this).outerWidth()) / 2) +
                $(window).scrollLeft()) + "px");
            return this;
        }

        $("#preloader").show();
        $("#preloader-content").show().center();

        setTimeout(function() {
            $("#preloader").fadeOut(function(){
                $('#main_left').css("opacity",1);
            });
        }, 1000);
		
		

        function goLupa() {
            alert("Fitur sedang dalam pengerjaan. Untuk sekarang coba ingat ingat password anda atau hubungi admin bila anda merasa tidak mampu :D");
        }
    </script>
</body>

</html>