<div class="row menu-tab">
	<a class="menu-tab-item<?php echo ($method == 'for_new') ? ' menu-tab-active' : '' ?>" href="<?php echo site_url($path.$class.'/for_new') ?>">Mau Ngapain</a>
	<a class="menu-tab-item<?php echo ($method == 'for_old') ? ' menu-tab-active' : '' ?>" href="<?php echo site_url($path.$class.'/for_old') ?>">Udah lewat</a>
</div>
<?php
	if(empty($data)) {
?>
<div class="alert alert-info">Belum ada data <?php echo $title ?></div>
<?php
	}
	else {
		foreach($data as $v) {
			$dt = substr($v['date'],0,10);
			if($dt != $cdt) {
				$cdt = $dt;
?>
<div class="todo-header"><?php echo FormatterWeb::toIndoDate($dt,true) ?></div>
<?php		} ?>
<div class="post-section">
	<div class="post-header">
		<div class="todo-col todo-col-<?php echo $v['color'] ?>">
			<?php echo substr($v['title'],0,1) ?>
		</div>
		<div>
			<div class="todo-title">
				<label class="label label-info"><?php echo substr($v['date'],11,5) ?></label>
				<strong><?php echo $v['title'] ?></strong>
			</div>
			<div class="post-header-info">
				<?php echo $v['description'] ?>
			</div>
		</div>
	</div>
	<div class="post-footer">
		<?php if($v['status'] == Todo_model::STATUS_DONE) { ?>
		<span class="todo-selesai">Selesai</span>
		<?php } else { ?>
		<a href="javascript:goSelesai('<?php echo $v['id'] ?>')">Set Selesai</a>
		<?php } ?>
		<a class="post-right post-delete" href="javascript:goDelete('<?php echo $v['id'] ?>')">Hapus</a>
	</div>
</div>
<?php
		}
	}
?>

<?php echo form_open(null,array('id' => 'form_referer')) ?>
	<?php echo form_hidden('status') ?>
	<?php echo form_hidden('referer') ?>
</form>

<script type="text/javascript">

var formref = $("#form_referer");
var back = "<?php echo base_url(uri_string()) ?>";

sessionStorage.setItem("<?php echo $class ?>.back",back);

function goAdd() {
	location.href = "<?php echo site_url($path.$class.'/add') ?>";
}

function goDelete(id) {
	var hapus = confirm("Apakah anda yakin akan menghapus to do ini?");
	if(hapus) {
		formref.attr("action","<?php echo site_url($path.$class.'/delete') ?>/" + id);
		formref.find("[name='referer']").val(back);
		formref.submit();
	}
}

function goSelesai(id) {
	formref.attr("action","<?php echo site_url($path.$class.'/update') ?>/" + id);
	formref.find("[name='status']").val("<?php echo Todo_model::STATUS_DONE ?>");
	formref.find("[name='referer']").val(back);
	formref.submit();
}

</script>