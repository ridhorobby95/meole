<div class="row">
	<div class="col-md-8">
    <?php
    foreach ($data as $v) {
        if ($list_sticky[0]['post_id']!=$v['post_id']){
    		$style = "background-color:white";
    		if ($v['post_status'] == Ticket::SELESAI) {
    			$style = "background-color:#efefef;border:1px solid #ccc";
    		}
            ?>
    		
    		<div class="post-section" style="<?= $style ?>">    
                <div id="post-<?= $v['post_id'] ?>">
    			<?php 
    			$_data = $v;

    			include(__DIR__.'/_'.$detail.'_detail.php');
    			 ?>
                </div>
               
    		</div>
        <?php }
    }
?>
	</div>
	<div class=" col-md-4 hidden-xs">
	<?php include(__DIR__.'/_information_right.php'); ?>
	</div>
</div>



<!-- <nav>
    <ul class="pager">
        <?php
        $page = $this->uri->segment(4);
        if (empty($page) || $page == 0) {
            echo '<li class="disabled"><a href = "#">Sebelumnya</a></li>';
        } else {
            echo '<li>' . anchor('web/' . $this->router->class . '/' . $this->router->method . '/' . ($page - 1) . Util::paramsToUrlGet($_GET), 'Sebelumnya') . '</li>';
        }
        if ($_SESSION['next_page_post']==0) {
            echo '<li class="disabled"><a href = "#">Selanjutnya</a></li>';
        } else {
            echo '<li>' . anchor('web/' . $this->router->class . '/' . $this->router->method . '/' . ($page + 1) . Util::paramsToUrlGet($_GET), 'Selanjutnya') . '</li>';
        }
        ?>
    </ul>
</nav> -->


<?php echo form_open(null, array('id' => 'form_delete')) ?>
<?php echo form_hidden('referer') ?>
</form>

<?php include(__DIR__.'/_post_js.php'); ?>

<script>
    var back = "<?php echo base_url(uri_string()) ?>";
    sessionStorage.setItem("<?php echo $class ?>.back", back);

    function goAdd() {
        location.href = "<?php echo site_url($path . $class . '/add') ?>";
    }

    function goDelete(id) {
        var hapus = confirm("Apakah anda yakin akan menghapus post ini?");
        if (hapus) {
            $("#form_delete").attr("action", "<?php echo site_url($path . $class . '/delete') ?>/" + id);
            $("#form_delete [name='referer']").val(back);
            $("#form_delete").submit();
        }
    }

    $('#div_cover')
        .mouseenter(function(){
            $("#btn_edit_cover").removeClass("hidden");
    })
        .mouseleave(function(){
            $("#btn_edit_cover").addClass("hidden");
    });

    $(function () {
        $("#btn_edit_cover").click(function () {
            $("#cover").click();
        });
        $("#cover").change(function () {
            // $("#label_cover").removeClass("hidden");
            // $("#btn_simpan_cover").removeClass("hidden");
            // $("#btn_edit_cover").addClass("hidden");
            $("#form_data_cover").submit();
        });        
    });

    // function goSaveCover() {
    //     // $("#btn_simpan_cover").addClass("hidden");
    //     // $("#label_cover").addClass("hidden");
    //     // $("#btn_edit_cover").removeClass("hidden");
    //     $("#form_data_cover").submit();
    // }

<?php if ($method == 'group') { ?>
        function goBack() {
            location.href = "<?php echo site_url($path . 'group/list_me') ?>";
        }
<?php } ?>

    $(document).ready(function () {
        $(".image-post").colorbox();
    });

</script>