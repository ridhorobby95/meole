<div class="row" id="main-container">
    <div class="col-sm-12 col-md-10" style="padding:0">
        <div class="panel panel-content" style="background-color: transparent;">
            <div class="panel-heading" style="padding: 0px 15px;">
                <h4 style="margin:0; font-weight:bold;font-size:26px">Statistik</h4>
            </div>
            <div class="panel-body menu-grid">


                <?php if (!$is_kopertis) { ?>
                    <div class="col-xs-6 col-md-4" style="padding: 0px 15px 0px 0px;width: 30%;">
                        <a href="<?= site_url('web/statistik/ticket') ?>" >
                        <div class="thumbnail hvr-grow-shadow hvr-underline-from-center" >
                            
                            <h1>
                                <!-- <img src="\helpdesk\assets\web\img\svg\weight.svg"> -->
                                <!-- <i class="glyphicon glyphicon-stats"></i> -->
                            </h1>
<!--                             <img src="<?= site_url('assets/web/img/svg/bell.svg')?>" style="width:10%;margin-bottom: 3%"> -->
                            <img src="<?= site_url('assets/web/img/svg/weight.svg')?>" style="width: 20%;margin-bottom: 7%;">
<!--                             <img src="\helpdesk\assets\web\img\svg\weight.svg" style="width: 20%;margin-bottom: 7%;"> -->
                            <div class="caption">
                                Tiket
                            </div>
                            
                        </div>
                        </a>
                    </div>
                <?php } ?>

                <div class="col-xs-6 col-md-4" style="padding: 0px 15px 0px 0px;width: 30%;">
                    <a href="<?= site_url('web/statistik/user/') ?>">
                    <div class="thumbnail hvr-grow-shadow hvr-underline-from-center">
                        <h1>
                            <!-- <i class="glyphicon glyphicon-user"></i> -->
                        </h1>
                        <img src="<?= site_url('assets/web/img/svg/user.svg')?>" style="width: 20%;margin-bottom: 7%;">
<!--                         <img src="\helpdesk\assets\web\img\svg\user.svg" style="width: 20%;margin-bottom: 7%;"> -->
                        <div class="caption">
                            User
                        </div>
                        
                    </div>
                    </a>
                </div>

            </div>
        </div>
    </div>
</div>
