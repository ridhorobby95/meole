<div class="row">
    <div class="col-md-12">
        <div class="box box-primary">
            <div class="box-body">
                <div class="row">
                    <div class="col-md-12">
                        <div class="table-responsive">
                            <table class="table table-bordered table-condensed table-striped">
                                <thead>
                                    <tr>
                                        <th class="text-center">No</th>
                                        <th class="text-center">Nama</th>
                                        <th class="text-center">Username</th>
                                        <th class="text-center">Role</th>
                                        <th class="text-center">Status</th>
                                        <th class="text-center">Aksi</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php
                                    $no = 0;
                                    foreach ($data as $v) {
                                        $no++;
                                        ?>
                                        <tr>
                                            <td class="text-right"><?= $no ?></td>
                                            <td><?= $v['name'] ?></td>
                                            <td><?= $v['username'] ?></td>
                                            <td class="text-center"><?php
                                                switch ($v['role']['id']) {
                                                    case Role::ADMINISTRATOR:
                                                        echo '<span class="label label-success">' . $v['role']['name'] . '</span>';
                                                        break;
                                                    case Role::MANAGEMENT:
                                                        echo '<span class="label label-info">' . $v['role']['name'] . '</span>';
                                                        break;
                                                    case Role::EMPLOYEE:
                                                        echo '<span class="label label-warning">' . $v['role']['name'] . '</span>';
                                                        break;
                                                }
                                                ?></td>
                                            <td class="text-center"><?php
                                                switch ($v['status']) {
                                                    case Status::ACTIVE:
                                                        echo '<span class="label label-success">Aktif</span>';
                                                        break;
                                                    case Status::DRAFT:
                                                        echo '<span class="label label-warning">Draft</span>';
                                                        break;
                                                    case Status::INACTIVE:
                                                        echo '<span class="label label-danger">Tidak Aktif</span>';
                                                        break;
                                                    case Status::VOID:
                                                        echo '<span class="label label-danger">Dihapus</span>';
                                                        break;
                                                }
                                                ?></td>
                                            <td>
                                                <?php
                                                echo anchor('web/user/edit/' . $v['id'], '<i class="fa fa-pencil"></i>', 'class="text-success" title="Ubah ' . $v['name'] . '"') . '&nbsp;&nbsp;';
                                                echo anchor('web/user/reset/' . $v['id'], '<i class="fa fa-refresh"></i>', 'class="text-warning" title="Reset Password ' . $v['name'] . '"') . '&nbsp;&nbsp;';
                                                echo anchor('web/user/delete/' . $v['id'], '<i class="fa fa-trash"></i>', 'class="text-danger"  title="Hapus ' . $v['name'] . '"');
                                                ?>
                                            </td>
                                        </tr>
                                        <?php
                                    }
                                    ?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>