<!DOCTYPE html>
<html class="login-page">

<head>
    <meta charset="UTF-8">
    <title>MEOLE</title>
    <meta content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no' name='viewport'>
    <link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/web/css/bootstrap.min.css') ?>">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/web/css/font-awesome.min.css') ?>">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/web/css/ionicons.min.css') ?>">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/web/css/style.css') ?>">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/web/css/login.css') ?>">
    <link rel="icon" type="image/png" href="<?php echo base_url('assets/web/img/aci-logo.png') ?>">
    <script type="text/javascript" src="<?php echo base_url('assets/web/js/external/html5shiv.js') ?>"></script>
    <script type="text/javascript" src="<?php echo base_url('assets/web/js/external/respond.min.js') ?>"></script>
</head>

<body>
    <div id="preloader">
        <div class="text-center" style="margin-bottom:-10px;">
            <img src="<?php echo base_url('assets/web/img/aci-logo.png') ?>" height="180" id="preloader-content">
        </div>
    </div>
    <div class="container-fluid">
        <div id="main">
            <div class="col-md-12" id="pattern">
                <div class="text-center" id="main_left">

                    <div class="text-center" style="margin-bottom:-10px;">
                        <img id="logo-utama" src="<?php echo base_url('assets/web/img/aci-logo.png') ?>" height="150">
                    </div>
                    <div class="container">
                        <div class="col-md-8 col-md-push-2 logo-text text-center">
                            <!-- <img style="margin-left:60px" class="pull-left logo-sigap" src="<?php echo base_url('assets/web/img/sigap.png') ?>" height="60"> -->
                            <center><h2 class="login-title">Poso Meole</h2><br/><p class="login-subtitle">Sistem pelaporan peristiwa berbasis visual dan lokasi Kabupaten Poso</p><center>
                        </div>
                        <div class="clearfix"></div><br/>
                        <div class="col-md-8 col-md-push-2" id="form-intro">

                            <?php if(!empty($errmsg)) { ?>

                            <div class="alert alert-danger">
                                <?php echo implode('<br>',$errmsg) ?>
                            </div>
                            <?php } ?>
                            <button type="button" class="btn-login text-shadow" id="btn-intro" style="position:relative;right:10px">Masuk</button>
                            <div style="display:inline;">
                                <a href="https://play.google.com/store/apps/details?id=id.go.integra.posomeole">
                                    <img src="<?php echo base_url('assets/web/img/googleplay.png') ?>" width="28%" height="28%" style="left:5px;top:-4px;position:relative;">
                                </a>
                            </div>
                            <!--
                            <h4 class="text-shadow">Belum punya akun? Buat <a href="#" data-section="#form-intro" data-target="#form-daftar" class="disini">di sini</a></h4>
                            -->
                        </div>
                    </div>
                    
                    <div id="form-lupapass">
                        <?php echo form_open(); ?>
                        <div class="container">
                            <div class="col-md-8 col-md-push-2">
                                <h2>Lupa Sandi Anda?</h2>
                                <p class="text-shadow">Silahkan masukkan alamat email akun SIGAP Anda, sistem akan mengirimkan informasi untuk reset sandi.</p>
                                <hr/>
                                <br/>
                                <h4 style="margin-bottom:0;"><b>Email Anda</b></h4>
                                <div class="col-md-8 col-md-push-2">
                                    <?= form_input('email', set_value('email'), 'class="form-control input-login" required autofocus') ?>
                                <br/>
                                        <?php echo form_submit(array('value' => 'Ubah Sandi','class' => 'btn-login text-shadow', 'id' => 'btn_ubah_pass')) ?>
                                        <h4 class="text-shadow">Sudah punya akun? Masuk <a href="#" data-section="#form-lupapass" data-target="#form-login" class="disini">di sini</a></h4>
                                </div>
                            </div>
                        </div>
                        </form>
                    </div>
                </div>
            </div>

            <div class="col-md-12" id="login-pattern" style="height: 100%; padding-left: 0px;">
                <div class="col-md-6" style="background-color:rgba(255,255,255,0.9);height: 100%; padding: 40px; padding-left: 100px;">
                    <div class="text-left" style="margin-bottom:-10px;">
                        <img id="logo-utama" src="<?php echo base_url('assets/web/img/aci-logo.png') ?>" height="150">
                    </div>
                    <div class="col-md-10 logo-text">
                        <!-- <img style="margin-left:60px" class="pull-left logo-sigap" src="<?php echo base_url('assets/web/img/sigap.png') ?>" height="60"> -->
                        <h1 class="login-title" style="text-align: left;">Poso Meole</h1><br/><p class="login-subtitle">Masukkan username dan password anda<br> (Sesuaikan dengan aplikasi E-Office Kabupaten Poso)</p>
                    </div>
                    <div id="form-login">
                        <?php echo form_open(); ?>
                            <div class="col-md-10"  style="padding-top: 30px;">
                                <div class="input-icons"> 
                                <i class="fa fa-user fa-lg" style="margin-left: -10px; margin-top: 10px;"></i> <?= form_input('username', set_value('username'), 'class="form-control input-login-form" placeholder="Masukkan username" required autofocus') ?>
                                </div>
                                
                            </div>
                            <div class="col-md-10" style="padding-top: 5px;">
                                <div class="input-icons">
                                <i class="fa fa-lock fa-lg" style="margin-left: -10px; margin-top: 10px;"></i>
                                <?php echo form_password(array('name' => 'password','class' => 'form-control input-login-form'), set_value('password'), ' placeholder="Masukkan password" ') ?>
                                </div>
                                
                                <!--
                                <p class="text-shadow pull-right"><a href="#" data-section="#form-login" data-target="#form-lupapass" class="disini">Lupa Kata Sandi?</a></p>
                                -->
                            </div>
                            <div class="col-md-8" style="padding-top: 50px; margin-bottom: 10px; text-align: center;">
                                <?php echo form_submit(array('value' => 'Masuk','class' => 'btn-login-form')) ?>
                                <br/>
                                <!--
                                <h4 class="text-shadow">Belum punya akun? Buat <a href="#" data-section="#form-login" data-target="#form-daftar" class="disini">di sini</a></h4>
                                -->
                            </div>

                            <div class="col-md-8" style="text-align: center;">
                                <h6>Copyright &copy; Pemerintah Kabupaten Poso 2019.</h6>
                            </div>
                        </form>
                    </div>
                    
                        
                </div>
                <div class="col-md-6" style="padding:0px;">
                    <div class="logo-text text-center">
                        <!-- <img style="margin-left:60px" class="pull-left logo-sigap" src="<?php echo base_url('assets/web/img/sigap.png') ?>" height="60"> -->
                        <h1 class="login-title" style="text-align: center;">Poso Meole Android</h1><br/><p class="login-subtitle">Download Aplikasi Poso Meole di Play Store untuk menjadi Pelapor</p>
                    </div>
                    <div class="text-center" style="margin-bottom:-10px;">
                        <img src="<?php echo base_url('assets/web/img/android.png') ?>" height="500">
                    </div>
                    <div class="text-center" style="margin-bottom:-10px;">
                        <a href="https://play.google.com/store/apps/details?id=id.go.integra.posomeole">
                        <img src="<?php echo base_url('assets/web/img/googleplay.png') ?>" width="28%" height="28%" style="left:5px;top:-4px;position:relative;"></a>
                    </div>
                </div>
            </div>

        </div>

    </div>
    </div>
    </div>

    <script type="text/javascript" src="<?php echo base_url('assets/web/js/external/jquery.min.js') ?>"></script>
    <script type="text/javascript" src="<?php echo base_url('assets/web/js/bootstrap.min.js') ?>"></script>
    <script type="text/javascript">
        $(document).ready(function() {
            $('#login-pattern').hide();
            $('#banner-pattern').hide();
            $('#main_left').css("padding-top", Math.max(0, (
                ($(window).height() - $('#main_left').outerHeight() - 125) / 2) + $(window).scrollTop()) + "px");
            $('#btn-intro').click(function() {
                // $('#form-login').slideDown(400);
                $('#form-login').show();

                $('#pattern').hide();
                $('#login-pattern').show();
                // $("#logo-utama").animate({height: "300px"});

                // $('#form-login').show("slide", { direction: "left" }, 1000);
                $('#form-intro').hide();
            });
            $('.disini').click(function() {
                var section = $(this).data('section');
                var target = $(this).data('target');
                $(target).slideDown(400);
                $(section).hide();
                if(target != "#form-login"){
                    $('.logo-text').hide();
                } else {
                    $('.logo-text').show();
                }
            });
        });
        $.fn.center = function() {
            this.css("position", "absolute");
            this.css("top", Math.max(0, (
                    ($(window).height() - $(this).outerHeight()) / 2) +
                $(window).scrollTop()) + "px");
            this.css("left", Math.max(0, (
                    ($(window).width() - $(this).outerWidth()) / 2) +
                $(window).scrollLeft()) + "px");
            return this;
        }

        $("#preloader").show();
        $("#preloader-content").show().center();

        setTimeout(function() {
            $("#preloader").fadeOut(function() {
                $('#main_left').css("opacity", 1);
            });
        }, 1000);



        function goLupa() {
            alert("Fitur sedang dalam pengerjaan. Untuk sekarang coba ingat ingat password anda atau hubungi admin bila anda merasa tidak mampu :D");
        }
    </script>
</body>

</html>