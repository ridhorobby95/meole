
<div class="row">
	<div class="col-md-8">
        <?php 
            if (SessionManagerWeb::isStaff()) {
                                
        ?>
        <div class="nav-top">
        <ul class="nav nav-pills" style="margin-right:5px">
            <li class="pull-right">
                <?php 
                    if ($list_my_helpdesk[0]==NULL) { 
                        $is_disable_button = "disabled";
                    }
                ?>
                <button class="btn btn-add <?= $is_disable_button?>" id="btn-buat-knowledge" ><i class="fa fa-pencil"></i>&nbsp;Buat Knowledge</button>

            </li>
        </ul>
        </div>
        <?php } ?>

<?php 
    if (SessionManagerWeb::isStaff()) { 
        include(__DIR__.'/_knowledge_kirim.php'); 
    }
?>

<?php

if (!empty($list_sticky)){ 
        $counter=0;
         foreach ($list_sticky as $v) {
            if ($v['post_type']=='K'){
                $temp[$counter] = $v;
                $counter++;
            }
         }
         if (!empty($temp)) 
            foreach ($temp as $v) {
?>

            <div class="post-section" style="background-color:#ffffce">    
                <div id="post-<?= $v['post_id'] ?>">
            <?php 
                $_data = $v;
                include(__DIR__.'/_knowledge_detail.php');
            ?>
                </div>
                     
            </div>
    <?php }
    }

if (empty($data)) {
    ?>
    <div class="alert alert-info">Tidak ada data yang bisa ditampilkan</div>
    <?php
} else {
    foreach ($data as $v) {
        
        if ($v['is_sticky']!='1') {
    		$style = "background-color:white";
    		if ($v['post_status'] == Ticket::SELESAI) {
    			$style = "background-color:#efefef;border:1px solid #ccc";
    		}
            ?>
    		
    		<div class="post-section" style="<?= $style ?>">    
                <div id="post-<?= $v['post_id'] ?>">
    			<?php 

    			$_data = $v;
        //         echo '<pre>';
        // var_dump($_data);
        // echo '</pre>';
        // die();
    			include(__DIR__.'/_knowledge_detail.php');
    			 ?>
                </div>
               
    		</div>
        <?php
        }
	}	
}
?>
	</div>
	<div class=" col-md-4 hidden-xs">
	<?php include(__DIR__.'/_information_right.php'); ?>
	</div>
</div>



<nav>
    <div class="text-left">
        <ul class="pagination">
            <?php
            $page = $this->uri->segment(4);
            if (empty($page) || $page == 0) {
                echo '<li class="disabled"><a href = "#">Sebelumnya</a></li>';
            } else {
                echo '<li>' . anchor('web/' . $this->router->class . '/' . $this->router->method . '/' . ($page - 1) . Util::paramsToUrlGet($_GET), 'Sebelumnya') . '</li>';
            }
            if ($data[0]['next_page_knowledge']==0) {
                echo '<li class="disabled"><a href = "#">Selanjutnya</a></li>';
            } else {
                echo '<li>' . anchor('web/' . $this->router->class . '/' . $this->router->method . '/' . ($page + 1) . Util::paramsToUrlGet($_GET), 'Selanjutnya') . '</li>';
            }
            ?>
        </ul>
    </div>
</nav>

<?php echo form_open(null, array('id' => 'form_delete')) ?>
<?php echo form_hidden('referer') ?>
</form>

<script>
    var back = "<?php echo base_url(uri_string()) ?>";
    sessionStorage.setItem("<?php echo $class ?>.back", back);

    function goAdd() {
        location.href = "<?php echo site_url($path . $class . '/add') ?>";
    }

    function goDelete(id) {
        var hapus = confirm("Apakah anda yakin akan menghapus knowledge ini?");
        if (hapus) {
            $("#form_delete").attr("action", "<?php echo site_url($path . $class . '/delete') ?>/" + id);
            $("#form_delete [name='referer']").val(back);
            $("#form_delete").submit();
        }
    }

    $(document).ready(function () {
        $(".image-post").colorbox();
    });

</script>
<?php include(__DIR__.'/_post_js.php'); ?>