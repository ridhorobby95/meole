<div class="row">
    <div class="col-md-12">
        <div class="box box-primary">
            <div class="box-body">
                <div class="row">
                    <?php echo form_open_multipart($path . $class . '/' . (isset($id) ? 'update/' . $id : 'create'), array('id' => 'form_data')) ?>
                    <div class="col-sm-8">
                        <div class="row bord-bottom">
                            <label for="name" class="col-sm-4">Nama</label>
                            <div class="col-sm-8">
                                <?php echo form_input(array('name' => 'name', 'value' => $data['name'], 'class' => 'form-control input-sm', 'id' => 'name')) ?>
                            </div>
                        </div>
                        <div class="row bord-bottom">
                            <label for="description" class="col-sm-4">Deskripsi</label>
                            <div class="col-sm-8">
                                <?php echo form_input(array('name' => 'description', 'value' => $data['description'], 'class' => 'form-control input-sm', 'id' => 'description')) ?>
                            </div>
                        </div>
                        <div class="row bord-bottom">
                            <label for="owner" class="col-sm-4">Pemilik</label>
                            <div class="col-sm-8">
                                <?php echo form_dropdown('owner', $a_user, $data['owner']['id'], 'id="owner" class="form-control input-sm"') ?>
                            </div>
                        </div>
                        <div class="row bord-bottom">
                            <label for="group_members" class="col-sm-4">Anggota</label>
                            <div class="col-sm-8">
                                <div id="div_users" class="site-listbox">
                                    <table>
                                        <tr>
                                            <td colspan="2">
                                                <?php echo form_input(array('class' => 'form-control input-sm', 'id' => 'group_members_users_filter', 'placeholder' => 'Cari Teman', 'autocomplete' => 'off')) ?>
                                            </td>
                                        </tr>
                                        <?php foreach ($a_user as $k => $v) { ?>
                                            <tr>
                                                <td><?php echo form_checkbox(array('name' => 'members[]', 'id' => 'group_members_' . $k, 'value' => $k, 'checked' => (in_array($k, $data['membersID'])), 'class' => 'simple')) ?></td>
                                                <td><label for="group_members_<?php echo $k ?>" class="labelinput"><?php echo $v ?></label></td>
                                            </tr>
                                        <?php } ?>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-4 text-center">
                        <div class="row">
                            <label class="col-sm-12">Gambar Profil</label>
                        </div>
                        <div class="row">
                            <div class="col-sm-12">
                                <div class="site-photo site-photo-128 site-photo-center">
                                    <img id="img_photo" src="<?php echo isset($data['image']) ? $data['image']['thumb']['link'] : $noimg ?>" data-toggle="tooltip" title="Upload Gambar" />
                                </div>
                                <label id="label_photo" class="label label-info hidden">Simpan grup untuk mengupload gambar</label>
                                <?php echo form_upload(array('name' => 'image', 'id' => 'image', 'class' => 'hidden')) ?>
                            </div>
                        </div>
                    </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">
    changeType();

    $(function () {
        $("#img_photo").click(function () {
            $("#image").click();
        });
        $("#image").change(function () {
            $("#label_photo").removeClass("hidden");
        });
        
        $("#type").change(function(){
           changeType(); 
        });
        
        $("#group_members_users_filter").keyup(function () {
            var str = $(this).val();

            $("#div_users tr:hidden").show();
            if (str != "") {
                $(".labelinput").each(function () {
                    if ($(this).html().toLowerCase().search(str.toLowerCase()) == -1)
                        $(this).parents("tr:eq(0)").hide();
                });
            }
        });
        
        $("#div_users [type='checkbox']").change(function() {
            value = $(this).attr("value");
            label = $("#div_selected [data-id='" + value  + "']");
            check = $(this).prop("checked");
            if(check) {
                if(label.length == 0)
                    $("#div_selected").append('<div data-id="' + value + '">' + $("label[for='" + $(this).attr("id") + "']").text() + '</div>');
            }
            else {
                if(label.length != 0)
                    label.remove();
            }
        });

<?php if (!isset($id)) { ?>
            $("#owner").val("<?php echo SessionManagerWeb::getUserID() ?>");
<?php } ?>
    });
    
    function changeType(){
        if($("#type").val() == "<?= Group_model::TYPE_PROJECT ?>"){
            $("#parent-id-block").show();
        }else{
	    $("#parent-id").val("");
            $("#parent-id-block").hide();
        }
    }

    function goBack() {
        location.href = "<?php echo site_url($path . $class . '/list_me') ?>";
    }

    function goSave() {
        $("#form_data").submit();
    }

</script>