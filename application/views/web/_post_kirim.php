<?php
// echo '<pre>';
// var_dump($list_helpdesk);
// die();
                        ?>
<?php
    $group_selected_hidden_btn = "hidden";
    $helpdesk_selected_hidden_btn = "hidden";
    $judul_kirim_hidden = "";
    $helpdesk_display = "none";
    
    if ($_SESSION['group_selected']=='0') {
        $helpdesk_hidden = "hidden";
        if ($is_allow_kirim_sebagai) {
            $helpdesk_display = "inline-block";
        }

    }
    $increment=0;
    foreach ($list_group as $group) {
        if ($group['id']==(int)$_SESSION['group_selected']){
            $index=$increment;
        }
        $increment++;
    }
?>
<div class="post-section" id="new_post_to" style="display: none">
    <div class="post-body" style="margin-bottom:0px;padding-bottom:0px">
        <div class="row" style="top:-5px">
            <div class="col-md-6" style="padding:0px;margin:0px">
                <div class="panel panel-primary" style="margin-right:5px">
                    <div class="panel-heading" style="width:100%;text-align:left"> 
                        <center><h3 class="panel-title"><b> Pilih Helpdesk</b> </h3></center>
                    </div>
                    <div class="panel-body" id="panel-body-helpdesk" style="padding:15px 15px 0px 15px;">
                        <ul style="padding:0px" id="list-helpdesk">
                        <?php 
                        
                        foreach ($list_helpdesk as $helpdesk) { ?>
                            <li style="list-style: none;display:none">
                            <div class="post-section" style="margin-bottom: 10px">
                                <div class="btn btn-default helpdesk" id="helpdesk_section" helpdesk_id="<?= $helpdesk['id'] ?>" helpdesk_name="<?= $helpdesk['name'] ?>" style="padding:2px 10px 10px 10px;cursor:pointer;text-align:left;white-space: normal;width:100%">
                                    <strong>
                                        <?= $helpdesk['name'].'<br>'; ?>
                                    </strong>
                                    <hr style="margin:0px;padding:0px">
                                    <span>
                                        <?= $helpdesk['description']; ?>
                                    </span>
                                    <br>
                                    <span class="pull-right btn btn-primary btn-xs" style="position:relative;width:100%;margin-top:5px">
                                        <strong>
                                            Tulis Pesan <i class="fa fa-angle-double-right"></i>
                                        </strong>
                                    </span>
                                </div>
                            </div>
                            </li>
                        <?php } ?>
                        </ul>
                    </div>
                    <div id="load-more-helpdesk" style="position:relative;width:100%" class="btn btn-primary" >
                        <center>
                        <strong><i class="fa fa-angle-double-down"></i></strong>
                        </center>
                    </div> 
                </div>
            </div>
            <div class="col-md-6" style="padding:0px;margin:0px">
                <div class="panel panel-warning" >
                    <div class="panel-heading" style="width:100%;text-align:left;color:white;background-color:#f39c12;border-color:#f39c12"> 
                        <center><h3 class="panel-title"><b> Pilih Group</b> </h3></center>
                    </div>
                    <div class="panel-body" id="panel-body-group" style="padding:15px 15px 0px 15px;">
                        <center>
                        <ul style="padding:0px" id="list-group">
                        <?php if (SessionManagerWeb::isStaff()) { ?>
                        <li style="list-style: none;display:none">
                        <div class="post-section" style="margin-bottom: 10px;width:45%;display:inline-block;border:0px">
                            <center><div class="btn btn-default group" id="group_section" group_id="1" group_name="Umum" style="padding:2px 10px 10px 10px;cursor:pointer;text-align:left;white-space: normal;width:100%;border-radius: 12px;">
                                <center><strong>
                                    Umum
                                </strong></center>
                            </div></center>
                        </div>
                        </li>
                        <?php } ?>
                        <?php 
                            foreach ($list_group as $group) { ?>
                            <li style="list-style: none;display:none;">
                            <?php if ($group['id']!='1') { ?>
                                <div class="post-section" style="margin-bottom: 10px;width:45%;display:inline-block;border:0px">
                                    <center><div class="btn btn-default group" id="group_section" group_id="<?= $group['id'] ?>" group_name="<?= $group['name'] ?>" style="padding:2px 10px 10px 10px;cursor:pointer;text-align:left;white-space: normal;width:100%;border-radius: 12px">
                                        <center><strong>
                                            <?= $group['name'].'<br>'; ?>
                                        </strong></center>
                                    </div></center>
                                </div>
                            <?php } ?>
                            </li>
                        <?php } ?>
                        </ul>
                        </center>
                    </div>
                    <div id="load-more-group" style="position:relative;width:100%" class="btn btn-warning" >
                        <center>
                        <strong><i class="fa fa-angle-double-down"></i></strong>
                        </center>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="post-section" id="new_post" style="display: none">
    <div class="alert k-alert-blue-edit" id="alert-kirim-ke" style="margin-bottom:5px">
    </div>
    <form method="post" name="kirim_form" id="kirim_form" action="<?= site_url('web/post/kirim') ?>">
        <input type="hidden" name="group_kirim" id="group_kirim" value="<?= $list_group[$index]['id'] ?>">
        <input type="hidden" name="helpdesk_kirim" id="helpdesk_kirim" value="">
        <input type="hidden" name="group_khusus_kirim" id="group_khusus_kirim" value="<?= $list_group[$index]['id'] ?>">
        <input type="hidden" value='0' id="kirim_status">
        <?php if (isset($_SESSION['group_selected'])) { ?>
            <?php if ($_SESSION['group_selected']!='0') { ?>
                <input type='hidden' name ='group_tujuan' id='group_tujuan' value="Group">
                <input type='hidden' name ='group_khusus_tujuan' id='group_khusus_tujuan' value="<?= $list_group[$index]['name'] ?>">
            <?php } else { ?>
                <input type='hidden' name ='group_tujuan' id='group_tujuan' value="Helpdesk">
            <?php } ?>
        <?php } else { ?>
            <input type='hidden' name ='group_tujuan' id='group_tujuan' value="">
            <input type='hidden' name ='group_khusus_tujuan' id='group_khusus_tujuan' value="">
        <?php } ?>
        <input type='hidden' name ='helpdesk_tujuan' id='helpdesk_tujuan' value="">
        
        <input type="hidden" name="stick_kirim" id="stick_kirim" value='0' >
        <div class="post-body" style="margin-bottom:5px;padding-bottom:10px">
            <div class="row">

                <div class="col-md-1" style="padding:0;text-align:center;">
                    <img class="img-circle" style="height:30px;width:30px" src="<?= site_url($me['user_photo']) ?>" />
                </div>
                <div class="col-md-11" style="padding:0;">
                <textarea class="form-control <?= $helpdesk_hidden ?>" id="judul_kirim" name="judul_kirim" rows="1" style="border:0;border-bottom:1px solid #eee" autofocus placeholder="Judul Post"></textarea>
                <textarea class="form-control" id="text_kirim" name="text_kirim" rows="5" style="border:0" autofocus placeholder="ketik pesan Anda di sini"></textarea></div>
            </div>
            <?php if (1==0) {?>
            <div class="row" id="kata_kunci" style="display: <?= $helpdesk_display ?>">
                <?php } ?>
            <div class="row" id="kata_kunci">
                <hr style="margin:5px"/>
                <div class="col-md-2" style="padding:0;text-align:center;vertical-align: center;margin-top:13px">
                    <strong>Kata Kunci</strong>
                </div>
                <div class="col-md-10" style="padding:0;margin-top:10px;">
                    <?php echo form_dropdown('tag[]', $a_tag, $data['tags'], 'class="form-control input-sm select2 tag" multiple="multiple" style="width:100%;"') ?>
                    <!-- <textarea class="form-control" id="tag" name="tag" rows="1" style="border:0" autofocus placeholder="Deskripsi Knowledge"></textarea> -->
                </div>
            </div>
            <div class="row" style="padding-top:5px">
                <hr style="margin:5px"/>
                <div class="col-md-4 col-xs-4">
                    <div class="btn k-btn-transition btn-sm" onclick="$('#gambar').click()">
                        <span class="fa fa-image" style="cursor:pointer;"></span> &nbsp;Foto
                    </div>
                    <!-- <br> -->
                    <?php if (SessionManagerWeb::isStaff()) { ?>
                        
                        <div class="btn k-btn-transition btn-sm" onclick="$('#file_upload').click()"><span class="fa fa-file" style="cursor:pointer;"></span> &nbsp;File</div>
                    <?php } ?>
                    <label id="label_kirim_sebagai" style="display: <?= $helpdesk_display ?>">
                        <hr style=" visibility:hidden; margin:0;padding:0;margin-top: 5px" />
                        <small><strong>Kirim sebagai</strong></small> &nbsp;
                        <input type="hidden" name="user_id_anggota" id="user_id_anggota">
                        <input type="textarea" rows='1' name="kirim_sebagai" placeholder="Nama" id="kirim_sebagai" class="form-control" style="width:50%;display: inline;font-size: 12px;">
                        <hr style=" visibility:hidden; margin:0;padding:0;margin-top: 5px" />
                    </label>
                    <hr style=" visibility:hidden; margin:0;padding:0;margin-top: 5px" />
                </div>
                <div class="col-md-4 col-xs-4" style="padding:0;left:-30px">  
                    <div class="btn btn-default btn-sm" style="visibility: hidden"><span class="fa fa-camera" style="cursor:pointer;color:#777;"></span> &nbsp;Foto</div>  
                    <div style="padding:0px;float:left">
                        <?php if (SessionManagerWeb::isAdministrator()) { ?>
                            <label id="sticky_label" style="display: none">
                                <form>
                                    <input type="checkbox" name="sticky" id='sticky'>
                                </form>
                                <small><strong>pin post</strong></small> &nbsp;
                            </label>
                        <?php } ?>
                    </div>
                    <hr style=" visibility:hidden; margin:0;padding:0;margin-top: 5px" />
                </div>
                <div class="col-md-4" style="float:right;padding:0">
                    <div class="pull-right">
                        <span class="btn btn-sm k-btn-batal">
                            <i class="fa fa-exclamation-circle k-mg"></i>Batal</span>
                        <!-- <span class="btn btn-sm btn-kirim" data-toggle="modal" data-target="#post_kirim_modal"><i class="fa fa-send"></i> Kirim</span> -->
                        <span class="btn btn-sm k-btn-kirim" onclick="javascript:checkGroupTujuan()" >
                            <i class="fa fa-send "></i> Kirim</span>
                    </div>
                </div>
            </div>
            <div class="row" style="margin-left:0px">
                <i style="font-size:12px">
                    format foto: <b>.jpg .jpeg .png</b> (dibawah <b>1MB</b>)
                </i>
            </div>
            <div class="row">
                <div id="images_preview" name="images_preview" class="col-md-12"></div>
            </div>
            <div class="row">
                <div id="files_preview" name="files_preview" class="col-md-12"></div>
            </div>
        </div>
    </form>
    <form method="post" name="file_form" id="file_form" enctype="multipart/form-data" action="<?= site_url('web/post/fileform') ?>" class="hidden">
        <input type="file" id="file_upload" name="file_upload[]" multiple style="visibility:hidden">
    </form>
    <form method="post" name="image_form" id="image_form" enctype="multipart/form-data" action="<?= site_url('web/post/imageform') ?>" class="hidden">
        <input type="file" id="gambar" name="gambar[]" multiple style="visibility:hidden">
    </form>
</div>

<style type="text/css">
   .ui-autocomplete {
    position: absolute;
    top: 100%;
    left: 0;
    z-index: 1000;
    float: left;
    display: none;
    min-width: 160px;   
    padding: 4px 0;
    margin: 0 0 10px 25px;
    list-style: none;
    background-color: #ffffff;
    border-color: #ccc;
    border-color: rgba(0, 0, 0, 0.2);
    border-style: solid;
    border-width: 1px;
    -webkit-border-radius: 5px;
    -moz-border-radius: 5px;
    border-radius: 5px;
    -webkit-box-shadow: 0 5px 10px rgba(0, 0, 0, 0.2);
    -moz-box-shadow: 0 5px 10px rgba(0, 0, 0, 0.2);
    box-shadow: 0 5px 10px rgba(0, 0, 0, 0.2);
    -webkit-background-clip: padding-box;
    -moz-background-clip: padding;
    background-clip: padding-box;
    *border-right-width: 2px;
    *border-bottom-width: 2px;
    cursor: pointer;
}

.ui-menu-item > a.ui-corner-all {
    display: block;
    padding: 3px 15px;
    clear: both;
    font-weight: normal;
    line-height: 18px;
    color: #555555;
    white-space: nowrap;
    text-decoration: none;
}

.ui-state-hover, .ui-state-active {
    color: #ffffff;
    text-decoration: none;
    background-color: #0088cc;
    border-radius: 0px;
    -webkit-border-radius: 0px;
    -moz-border-radius: 0px;
    background-image: none;
}
</style>

<script type="text/javascript" src="<?php echo base_url('assets/web/js/jquery.form.js') ?>"></script>
<script>
    $('#btn_kirim_pesan').on('click',function(){
        $('#new_post_to').toggle();
    });

    // $('#btn_kirim_pesan').on('click',function(){
    //     $('#new_post').toggle();
    //     $('#btn_kirim_pesan').addClass('disabled');
    // });

    $('#gambar').on('change', function() {
        $('#image_form').ajaxForm({
            target: '#images_preview',
            error: function(e) {
                 alert('Terjadi kesalahan. Silakan diulangi lagi');
            }
        }).submit();
    });

    $('#file_upload').on('change', function() {
        $('#file_form').ajaxForm({
            target: '#files_preview',
            error: function(e) {
                //target: '#images_preview';
                 alert('Terjadi kesalahan. Silakan diulangi lagi');
            }
        }).submit();
    });

    $('.helpdesk_kiriman').on('click', function() {
        $('#group_kirim').val($(this).attr('val'));
        $('#helpdesk_kirim').val($(this).attr('val'));
        $('#btn_helpdesk').html($(this).html());
        $.ajax({
            url: "<?= site_url('web/post/getgroupname') ?>/"+$(this).attr('val'),
            success: function(result) {
                $('#helpdesk_tujuan').val(result);
                
            }
        });
    });

    $('.group_khusus_kiriman').on('click', function() {      
        if ($(this).attr('val') == '1')
            $('#sticky_label').show();
        else
            $('#sticky_label').hide(); 
        $('#group_kirim').val($(this).attr('val'));
        $('#group_khusus_kirim').val($(this).attr('val'));
        $('#btn_group_khusus').html($(this).html());
        $.ajax({
            url: "<?= site_url('web/post/getgroupname') ?>/"+$(this).attr('val'),
            success: function(result) {
                $('#group_khusus_tujuan').val(result);
                
            }
        });
    });

    $('.btn-batal').on('click', function() {
        $.ajax({
            url: "<?= site_url('web/post/resetkirim') ?>",
            success: function(result) {
                $('#images_preview').html('');
                $('#files_preview').html('');
                $('#judul_kirim').html('');
                $('#judul_kirim').val('');
                $('#text_kirim').html('');
                $('#text_kirim').val('');
                $('#new_post').hide();
                $('#btn_kirim_pesan').removeClass('disabled');
            }
        });
    });

    $('.btn.btn-default.helpdesk').on('click', function() {
        $('#btn_kirim_pesan').addClass('disabled');
        var id = $(this).attr('helpdesk_id');
        var name = $(this).attr('helpdesk_name');
        var description = $(this).attr('helpdesk_description');
        $.ajax({
            url: "<?= site_url('web/post/setSessionHelpdeskSelected') ?>",
            type: 'post',
            cache: false,
            data: {"id":id,"name":name, "description":description},
            success: function(result) {
                <?php if ($_SESSION['allow_kirim_sebagai']) { ?>
                    $('#label_kirim_sebagai').show();
                    $('#kata_kunci').show();
                <?php } ?>
                $("#alert-kirim-ke").html(result);
                $("#sticky_label").hide();
                $('#group_tujuan').val('Helpdesk');
                $('#helpdesk_kirim').val(id);
                $('#helpdesk_tujuan').val(name);
                $('#btn_group').html('Helpdesk');
                $('#div_kirim_group_khusus').addClass('hidden');
                // $('#label_kirim_sebagai').show();
                $('#judul_kirim').addClass('hidden');
                $('#div_kirim_helpdesk').removeClass('hidden');
                $('#btn_helpdesk').html('Pilih...');
                $('#group_kirim').val('0');
                $('#helpdesk_kirim').val("");
                $('#group_khusus_kirim').val("");
                $.ajax({
                    url: "<?= site_url('web/post/getgroupname') ?>/0",
                    success: function(result) {
                        $('#group_tujuan').val(result);
                        $('#group_kirim').val(id);
                        $('#helpdesk_kirim').val(id);
                        $('#btn_helpdesk').html(name);
                        $.ajax({
                            url: "<?= site_url('web/post/getgroupname') ?>/"+id,
                            success: function(result) {
                                $('#helpdesk_tujuan').val(result);
                                
                            }
                        });
                    }
                });
                $('#new_post').toggle();
                $('#new_post_to').toggle();
            }
        });
    });

    $('.btn.btn-default.group').on('click', function() {
        $('#btn_kirim_pesan').addClass('disabled');
        var id = $(this).attr('group_id');
        var name = $(this).attr('group_name');
        if (id=='1') {
            $("#sticky_label").show();
        } else {
            $("#sticky_label").hide();
        }$.ajax({
            url: "<?= site_url('web/post/setSessionGroupSelected') ?>",
            type: 'post',
            cache: false,
            data: {"id":id,"name":name},
            success: function(result) {
                $('#label_kirim_sebagai').hide();
                $('#kata_kunci').hide();
                $("#alert-kirim-ke").html(result);
                $('#group_tujuan').val('Group');
                $('#judul_kirim').removeClass('hidden');
                $('#helpdesk_kirim').val("");
                $('#group_kirim').val(id);
                $('#group_khusus_kirim').val(id);
                $.ajax({
                    url: "<?= site_url('web/post/getgroupname') ?>/"+id,
                    success: function(result) {
                        $('#group_khusus_tujuan').val(result);
                        
                    }
                });
            }
        });

        $('#new_post').toggle();
        $('#new_post_to').toggle();
    });

    function kirim_pesan(){
        if ($('#kirim_status').attr('value')=='1') {
            if ($('#sticky').is(':checked')) {
                $('#stick_kirim').val('1');
            }
            $('#kirim_form').submit();
        }
    }

    $(document).ready(function() {
        <?php if ($_SESSION['integra']['draft_post']) { ?>
        $.ajax({
            url: "<?= site_url('web/post/getdraft') ?>",
            success: function(result) {
                $("#images_preview").html(result['images']);
                $('#files_preview').html(result['files']);
                $('#judul_kirim').html('<?= $_SESSION['
                    integra ']['
                    draft_post_judul '] ?>');
                $('#text_kirim').html('<?= $_SESSION['
                    integra ']['
                    draft_post_text '] ?>');
                $('#new_post').show();
            }
        });

        <?php } ?>
    });

    $(document).ready(function(){
        var is_penerima_lain_selected = false;
        $( "#kirim_sebagai" ).autocomplete({
            <?php if (SessionManagerWeb::isStaff()) { ?>
                source: "<?php echo site_url('web/ajax/allusers'); ?>",
            <?php } else { ?>
                source: "<?php echo site_url('web/ajax/groupusers'); ?>",
            <?php } ?>
            minLength: 2,
            select: function( event, ui ) {
                if (ui.item) {
                    $("#user_id_anggota").val(ui.item.id);
                }
            },
            open: function (event, ui) {
                $(".ui-autocomplete li.ui-menu-item:odd a").addClass("ui-menu-item-alternate");
                $(".ui-autocomplete li.ui-menu-item:hover a").addClass("ui-state-hover");
            },
            close: function (event, ui) {
            },
             messages: {
                    noResults: '',
                    results: function() {}
                }            
        })
        .data( "ui-autocomplete" )._renderItem = function( ul, item ) {
            return $( "<div>" )
                .data( "ui-autocomplete-item", item )
                .append( "<a>"+ item.label+ "</a>" )
                .appendTo( ul );
        };
    });

    $(document).ready(function () {
        $('.btn.btn-default.helpdesk').removeClass('hover');
    });

    $(document).ready(function () {
        size_li = $("#list-helpdesk li").size();
        x=3;
        if (size_li<=x) {
            $('#load-more-helpdesk').addClass('hidden');
        }
        $('#list-helpdesk li:lt('+x+')').show();
        $('#load-more-helpdesk').click(function () {
            x= (x+2 <= size_li) ? x+2 : size_li;
            $('#list-helpdesk li:lt('+x+')').show();
            if (size_li<=x) {
                $('#load-more-helpdesk').addClass('hidden');
            }
        });
    });

    $(document).ready(function () {
        size_li = $("#list-group li").size();
        x=20;
        if (size_li<=x) {
            $('#load-more-group').addClass('hidden');
        }
        $('#list-group li:lt('+x+')').css("display","inline");
        $('#load-more-group').click(function () {
            x= (x+8 <= size_li) ? x+8 : size_li;
            $('#list-group li:lt('+x+')').css("display","inline");
            if (size_li<=x) {
                $('#load-more-group').addClass('hidden');
            }
        });
    });

    $(".tag").select2({
        placeholder: 'Ketik kata kunci',
        minimumResultsForSearch: Infinity,
        tags: true
    });
</script>