<div class="row">
    <div class="col-md-12">        
        <div class="box box-primary">
            <div class="box-body">
                <div class="row">
                    <div class="col-md-12">
                        <h2><?= $title ?> Keyword</h2><hr>
                        <div class="table-responsive">
                            <div class="row">
                                <div class="col-md-6">
                                    <table class="table table-bordered table-condensed table-striped">
                                        <thead>
                                            <tr>
                                                <th class="text-center" style="width:5px">No</th>
                                                <th class="text-center" >Keyword</th>
                                                <th class="text-center col-md-2">Jumlah Tiket</th>
                                                <th class="text-center col-md-2">Jumlah KM</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <?php
                                            foreach ($data as $v) {
                                                $no++;

                                                    // if ($_SESSION['role_selected']==NULL || $_SESSION['role_selected']==substr(strtoupper(Role::name($v['role'][0])),0,1)) {
                                                    ?>
                                                        <tr>
                                                            <td class="text-right"><?= $no ?></td>
                                                            <td align="text-left">
                                                                <?php if ($v['tiket'] && !$v['knowledge']) {  ?>
                                                                <i class="glyphicon glyphicon-exclamation-sign" style="color:red" title="Keyword tidak mempunyai KM"></i>
                                                                <?php } ?>
                                                                <?= $v['name'] ?>
                                                            </td>
                                                            <td align="center"><?= $v['tiket'] ? $v['tiket'] : '-' ?></td>
                                                            <td align="center"><?= $v['knowledge'] ? $v['knowledge'] : '-' ?></td>
                                                        </tr>
                                                    <?php 
                                                    //}
                                                // }
                                            }
                                            ?>
                                        </tbody>
                                    </table>    
                                </div>
                                <div class="col-md-6">
                                    <canvas id="myChart" width="750" height="<?= count($keywords) * 60 ?>"></canvas>
                                </div>
                            </div>
                        </div>
                        <br>
                    </div>
                </div>

            </div>
        </div>
    </div>
</div>

<script type="text/javascript">
    var ctx = document.getElementById("myChart").getContext('2d');
    var myChart = new Chart(ctx, {
        type: 'horizontalBar',
        data: {
            labels: <?php echo json_encode($keywords) ?>,
            datasets: <?php echo json_encode($chart_data) ?>,
        },
        options: {
            scales: {
                yAxes: [{
                    display:true,
                    scaleLabel: {
                        display: true,
                        labelString: 'Keyword'
                    }
                }],
                xAxes: [{
                    display: true,
                    scaleLabel: {
                        display: true,
                        labelString: 'Jumlah'
                    },
                    ticks: {
                        beginAtZero:true
                    }
                }]

            },
            elements: {
                line: {
                    tension: 0
                }
            }
        }
    });
</script>