<div class="row">
    <div class="col-md-12">        
        <div class="box box-primary">
            <div class="box-body">
                <div class="row">
                    <div class="col-md-12">
                        <h2><?php if ($this->router->method=='daftar_helpdesk') echo ' Helpdesk'; else echo $title; ?></h2>
                        <div class="table-responsive">
                            <table class="table table-bordered table-condensed table-striped">
                                <thead>
                                    <tr>
                                        <th class="text-center" style="width: 5%">No</th>
                                        <th class="text-center" style="width:70%">Nama</th>
                                        <?= $this->router->method=='daftar_helpdesk' ?  "<th>Kuota</th>" : ""; ?>
                                        <th class="text-center" style="width:10%">Jumlah Anggota</th>
                                        <th class="text-center" style="width:15%">Aksi</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php
                                    $no = 0;
                                    foreach ($data as $v) {
                                        $no++;
                                        ?>
                                        <tr>
                                            <td class="text-right"><?= $no ?></td>
                                            <td><a href="<?= site_url('web/group/detail/' . $v['id']) ?>"><?= $v['name'] ?></a></td>
                                            <?= $this->router->method=='daftar_helpdesk' ?  '<td>'.$v['quota_limit'].'</td>' : ''; ?>
                                            <td><?= $v['jumlah'] ?></a>
                                            </td>
                                            <td>
                                                <?php
                                                    echo anchor('web/group/detail/' . $v['id'], '<i class="fa fa-plus" style="color:slateblue" ></i>', 'class="text-success"  title="Tambah Anggota"'). '&nbsp;&nbsp;';
                                                    echo anchor('web/group/detail/' . $v['id'], '<i class="fa fa-pencil"></i>', 'class="text-success" title="Ubah ' . $v['name'] . '"') . '&nbsp;&nbsp;';
                                                    // echo anchor('web/group/delete/' . $v['id'], '<i class="fa fa-trash"></i>', 'class="text-danger"  title="Hapus ' . $v['id'] . '"');
                                                    
                                                ?>
                                                <?php if ($data[0]['type']!="H" or SessionManagerWeb::isSuperAdministrator()) { ?>
                                                    <a href="javascript:hapus_<?= $this->uri->segment(3)=='daftar_helpdesk' ? 'helpdesk' : 'group' ?>(<?php echo $v['id'] ?>,'<?php echo $v['name'] ?>')" class="text-danger" title="Hapus  <?php echo $v['name'] ?> ">
                                                        <i class="fa fa-trash"></i>
                                                    </a>    
                                                <?php } ?>
                                            </td>
                                        </tr>
                                        <?php
                                    }
                                    ?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
                <?php if (SessionManagerWeb::isSuperAdministrator()) { ?>
                    <br><br>
                    <div class="row">
                        <div class="col-sm-4 col-md-4">
                            <input type="text" name="group_name" id="group_name" placeholder="Nama <?php if ($this->router->method=='daftar_helpdesk') echo ' Helpdesk'; else echo 'Group'; ?>" class="form-control" style="width:300px">
                        </div>
                        <?php if($this->router->method !='daftar_helpdesk'){ ?> 
                        <div class="col-sm-4 col-md-2">
                            <?php echo form_dropdown('group_type', array('G' => 'GROUP', 'K' => 'KOPERTIS'), $data['type'], 'class="form-control group_type" id= "group_type"') ?>
                        </div>
                        <?php } ?>
                        <div class="col-sm-4 col-md-4"><span class="btn btn-sm btn-primary" id="btn_tambah_<?php if ($this->router->method=='daftar_helpdesk') echo 'helpdesk'; else echo 'group'; ?>">Tambah <?php if ($this->router->method=='daftar_helpdesk') echo ' Helpdesk'; else echo 'Group'; ?></span></div>
                    </div>
                <?php } ?>
            </div>


        </div>

    </div>

</div>

<script>
    $('#btn_tambah_group').on('click', function () {
        var name = $('#group_name').val();
        var type = $('#group_type').val();
        location.href = "<?= site_url('web/group/add') ?>/" + name + "/" + type;
    });    

    $('#btn_tambah_helpdesk').on('click', function () {
        var name = $('#group_name').val();
        location.href = "<?= site_url('web/group/addhelpdesk') ?>/" + name ;
    });

    function hapus_group(id,name) {
        if (confirm('Hapus group "'+name+'" ?')){
            location.href = "<?= site_url('web/group/delete') ?>/"+id;
        }
    }

    function hapus_helpdesk(id,name) {
        if (confirm('Hapus helpdesk "'+name+'" ?')){
            location.href = "<?= site_url('web/group/delete_helpdesk') ?>/"+id;
        }
    }

</script>