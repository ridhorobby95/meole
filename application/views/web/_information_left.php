<?php
	// fungsi membuat side bar
	function createSideBar($arrmenu,&$i) {
		if(empty($arrmenu[$i]))
		return '';
		
		$menu = $arrmenu[$i];
		
		if($menu['namamenu'] != ':separator') {
			$level = $menu['levelmenu'];
			$j=$i+1;
			$nextlevel = $arrmenu[$j]['levelmenu'];
			
			if(!empty($menu['namafile']))
				$href = site_url($menu['namafile']);
			else
				$href = 'javascript:void(0)';
			
			if(empty($menu['faicon']))
				$icon = 'desktop';
			else
				$icon = $menu['faicon'];
			
			if($nextlevel > $level)
				$class = 'treeview';
			else
				$class = '';
			
			$nama = '<span>'.$menu['namamenu'].'</span>';
			if($level == 0 or $nextlevel <= $level)
				$nama = '<i class="fa fa-'.$icon.'"></i> '.$nama;
			else if($level == 1)
				$nama = '<i class="fa fa-angle-right"></i> '.$nama;
			else
				$nama = '<i class="fa fa-angle-double-right"></i> '.$nama;
			
			if($nextlevel > $level)
				$nama .= ' <i class="fa pull-right fa-angle-down"></i>';
			
			$nama = '<a href="'.$href.'">'.$nama.'</a>';
			
			$str = '<li'.(empty($class) ? '' : ' class="'.$class.'"').'>'.$nama;
			if($nextlevel > $level) {
				$str .= "\n".'<ul class="treeview-menu">'."\n";
				$i++;
				$str .= createSideBar($arrmenu,$i);
				$str .= '</ul>'."\n";
			}
			else
				$str .= '</li>'."\n";
		}
		$j=$i+1;
		$nextlevel = $arrmenu[$j]['levelmenu'];
		if($nextlevel < $level)
			return $str;
		else {
			$i++;
			return $str.createSideBar($arrmenu,$i);
		}
	}
?>
<section class="sidebar">
	<div class="sidebar-title">
		<div class="site-photo site-photo-48 site-photo-left">
			<a href="<?php echo site_url($path.'user/me') ?>"><img class="img-circle" src="<?php echo SessionManagerWeb::getPhoto() ?>/48" /></a>
		</div>
		INTEGRA
		<?php echo SessionManagerWeb::getUserName() ?>
	</div>
	<hr style="margin-bottom:0" />
	<ul class="sidebar-menu">
		<?php
			$i = 0;
			echo createSideBar($arrmenu,$i);
		?>
	</ul>
	<hr style="margin:0" />
	<ul class="sidebar-menu">
		<li>
			<a href="<?php echo site_url($path.'notification/me') ?>">
				<i class="fa fa-globe"></i>
				<span>
					Notifikasi
					<?php if(!empty($jmlnotif)) { ?>
					<span style="color:red">(<?php echo ($jmlnotif > 99) ? '99+' : $jmlnotif ?>)</span>
					<?php } ?>
				</span>
			</a>
		</li>
		<li>
			<a href="<?php echo site_url($path.'user/logout') ?>">
				<i class="fa fa-sign-out"></i> <span>Logout</span>
			</a>
		</li>
	</ul>
</section>