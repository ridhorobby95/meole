<div class="post-section post-cat post-cat-<?php echo $data['categoryId'] ?>">
    <div>
        <div class="site-photo site-photo-48 site-photo-left">
            <img src="<?php echo isset($data['user']['photo']) ? site_url($path . 'thumb/watermark/' . $data['user']['id'] . '/48') : $nopic ?>">
        </div>
        <div>
            <div>
                <strong><?php echo $data['user']['name'] ?></strong>
                <?php
                if ($data['type'] == Post_model::TYPE_FRIEND and ! empty($data['postUsers'])) {
                    $a_nama = array();
                    foreach ($data['postUsers'] as $v)
                        $a_nama[] = $v['name'];
                    ?>
                    mengirim ke <strong><?php echo implode(', ', $a_nama) ?></strong>
                <?php } ?>
            </div>
            <div class="post-header-info">
                <?php echo FormatterWeb::toIndoDate($data['date'], true) ?>
            </div>
        </div>
    </div>
    <div class="post-body">
        <p>
            <?php
            switch ($data['priority']) {
                case Issue_model::PRIORITY_MINOR: $label = 'MINOR';
                    $labelc = 'success';
                    break;
                case Issue_model::PRIORITY_NORMAL: $label = 'NORMAL';
                    $labelc = 'info';
                    break;
                case Issue_model::PRIORITY_IMPORTANT: $label = 'IMPORTANT';
                    $labelc = 'warning';
                    break;
                case Issue_model::PRIORITY_URGENT: $label = 'URGENT';
                    $labelc = 'danger';
                    break;
                default: unset($label, $labelc);
            }
            if (!empty($label)) {
                ?>
                <label class="label label-<?php echo $labelc ?>"><?php echo $label ?></label>
            <?php } ?>
            <?php
            switch ($data['status']) {
                case Issue_model::STATUS_OPEN: $label = 'OPEN';
                    $labelc = 'info';
                    break;
                case Issue_model::STATUS_START: $label = 'STARTED';
                    $labelc = 'info';
                    break;
                case Issue_model::STATUS_PROGRESS: $label = 'PROGRESS';
                    $labelc = 'info';
                    break;
                case Issue_model::STATUS_DONE: $label = 'DONE';
                    $labelc = 'success';
                    break;
                case Issue_model::STATUS_REOPEN: $label = 'REOPEN';
                    $labelc = 'warning';
                    break;
                case Issue_model::STATUS_CLOSE: $label = 'FINISH';
                    $labelc = 'success';
                    break;
                default: unset($label, $labelc);
            }
            if (!empty($label)) {
                ?>
                <label class="label label-<?php echo $labelc ?>"><?php echo $label ?></label>
            <?php } ?>
            <label class="label label-danger">Deadline: <?php echo FormatterWeb::toIndoDate($data['deadline']) ?></label>
        </p>
        <p><strong><?php echo $data['title'] ?></strong></p>
        <p><?php echo nl2br($data['description']) ?></p>
        <?php if (!empty($data['image'])) { ?>
                <p><a href="<?= TemplateManagerWeb::getPostImageLink($data['image']) ?>" class="image-post"><img src="<?php echo TemplateManagerWeb::getPostImageLink($data['image']) ?>" /></a></p>
        <?php } ?>
        <?php if (!empty($data['file'])) { ?>
            <p><a href="<?php echo $data['file']['link'] ?>"><?php echo $data['name'] ?></a></p>
        <?php } ?>
        <?php if (!empty($data['link'])) { ?>
            <p><a href="<?php echo $data['link'] ?>"><?php echo $data['link'] ?></a></p>
        <?php } ?>
        <p><strong>Reporter</strong></p>
        <p>
            <?php echo $data['reportter'] ?><?php echo empty($data['reporterPhone']) ? '' : ', Telp: ' . $data['reporterPhone'] ?>
            <?php echo empty($data['reporterEmail']) ? '' : ' (<a href="mailto:' . $data['reporterEmail'] . '">' . $data['reporterEmail'] . '</a>)' ?>
        </p>
    </div>
    <div class="post-footer">
        <?php echo count($data['comments']) ?> komentar
    </div>
</div>

<table class="comment-table">
    <?php foreach ($data['comments'] as $v) { ?>
        <tr>
            <td>
                <div class="site-photo site-photo-48">
                    <img src="<?php echo isset($v['user']['photo']) ? site_url($path . 'thumb/watermark/' . $v['user']['id'] . '/48') : $nopic ?>">
                </div>
            </td>
            <td class="comment-info">
                <strong><?php echo $v['user']['name'] ?></strong>
                <br /><?php echo $v['text'] ?>
                <div class="post-header-info">
                    <?php echo FormatterWeb::dateToDiff($v['createdAt']) ?>
                    <?php if ($v['user']['id'] == SessionManagerWeb::getUserID() && $v['candelete'] == 1) { ?>
                        <a class="post-right post-delete" href="javascript:goDeleteComment('<?php echo $v['id'] ?>')">Hapus</a>
                    <?php } ?>
                </div>
            </td>
        </tr>
    <?php } ?>
    <tr>
        <td colspan="2">
            <?php echo form_open($path . 'comment/create') ?>
            <div class="input-group">
                <?php echo form_input(array('name' => 'text', 'class' => 'form-control', 'placeholder' => 'Tulis komentar')) ?>
                <?php echo form_hidden('post_id', $data['id']) ?>
                <div class="input-group-btn">
                    <button type="submit" class="btn btn-success">
                        <i class="fa fa-check"></i> Kirim
                    </button>
                </div>
            </div>
            <?php echo form_hidden('referer', base_url(uri_string())) ?>
            </form>
        </td>
    </tr>
</table>

<?php echo form_open(null, array('id' => 'form_referer')) ?>
<?php echo form_hidden('referer') ?>
</form>

<script type="text/javascript">

    var formref = $("#form_referer");
    var back = sessionStorage.getItem("<?php echo $class ?>.back");

    function goBack() {
        if (back)
            location.href = back;
        else
            location.href = "<?php echo site_url($path . $class) ?>";
    }

    function goEdit() {
        location.href = "<?php echo site_url($path . $class . '/edit/' . $data['id']) ?>";
    }

    function goDelete() {
        var hapus = confirm("Apakah anda yakin akan menghapus tugas ini?");
        if (hapus) {
            formref.attr("action", "<?php echo site_url($path . $class . '/delete/' . $data['id']) ?>");
            formref.find("[name='referer']").val(back);
            formref.submit();
        }
    }

    function goDeleteComment(id) {
        var hapus = confirm("Apakah anda yakin akan menghapus komentar ini?");
        if (hapus) {
            formref.attr("action", "<?php echo site_url($path . 'comment/delete') ?>/" + id);
            formref.find("[name='referer']").val("<?php echo base_url(uri_string()) ?>");
            formref.submit();
        }
    }
    
    $(document).ready(function () {
        $(".image-post").colorbox();
    });

</script>