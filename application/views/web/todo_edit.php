<div class="row">
	<div class="col-md-12">
		<div class="box box-primary">
			<div class="box-body">
				<div class="row">
					<div class="col-md-12">
						<?php echo form_open($path.$class.'/'.(isset($id) ? 'update/'.$id : 'create'),array('id' => 'form_data')) ?>
							<div class="row bord-bottom">
								<label for="title" class="col-sm-3">Mau Ngapain ?</label>
								<div class="col-sm-9">
									<?php echo form_input(array('name' => 'title','value' => $data['title'],'class' => 'form-control input-sm','id' => 'title')) ?>
								</div>
							</div>
							<div class="row bord-bottom">
								<label for="description" class="col-sm-3">Lebih Detail</label>
								<div class="col-sm-9">
									<?php echo form_input(array('name' => 'description','value' => $data['description'],'class' => 'form-control input-sm','id' => 'description')) ?>
								</div>
							</div>
							<div class="row bord-bottom">
								<label for="date" class="col-sm-3">Kapan</label>
								<div class="col-sm-3">
									<div class="input-group">
										<div class="input-group-addon"><i class="fa fa-calendar"></i></div>
										<?php echo form_input(array('name' => 'date','value' => (isset($id) ? FormatterWeb::formatDate($data['date']) : date('d-m-Y')),'class' => 'form-control input-sm','placeholder' => 'dd-mm-yyyy')) ?>
									</div>
								</div>
								<div class="col-sm-2">
									<?php echo form_input(array('name' => 'time','value' => (isset($id) ? substr($data['date'],11,5) : date('H:i')),'class' => 'form-control input-sm','id' => 'time')) ?>
								</div>
							</div>
							<div class="row bord-bottom">
								<label class="col-sm-3">Warna</label>
								<div class="col-sm-9">
									<div id="div_col" class="todo-col todo-col-<?php echo (isset($id) ? $data['color'] : 0) ?>">A</div>
									<div style="overflow-x:scroll">
										<div style="width:1350px">
											<?php for($i=0;$i<=16;$i++) { ?>
											<div class="todo-col todo-col-<?php echo $i ?>" data-col="<?php echo $i ?>">A</div>
											<?php } ?>
										</div>
									</div>
									<?php echo form_hidden('color',(isset($id) ? $data['color'] : 0)) ?>
								</div>
							</div>
							<div class="row bord-bottom">
								<label class="col-sm-3">Mau diingatkan ga ?</label>
								<div class="col-sm-6">
									<?php echo form_label(form_radio(array('name' => 'is_remind_me','value' => 'T','id' => 'is_remind_me_T','checked' => ($data['is_remind_me'] == 'F' ? false : true))).' Ya','is_remind_me_T',array('class' => 'labelinput')) ?>
									&nbsp;
									<?php echo form_label(form_radio(array('name' => 'is_remind_me','value' => 'F','id' => 'is_remind_me_F','checked' => ($data['is_remind_me'] == 'F' ? true : false))).' Tidak','is_remind_me_F',array('class' => 'labelinput')) ?>
								</div>
							</div>
							<?php echo form_hidden('referer') ?>
						</form>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

<script type="text/javascript">

var back = sessionStorage.getItem("<?php echo $class ?>.back");

$(function() {
	$("[name='date']").datepicker({format:'dd-mm-yyyy'});
	$("[name='time']").timepicker({
		showInputs: false,
		showMeridian: false,
		defaultTime: false
	});
	$("[name='referer']").val(back);
	
	$("[data-col]").css("cursor","pointer").click(function() {
		var color = $(this).attr("data-col");
		
		$("#div_col").attr("class","todo-col todo-col-" + color);
		$("[name='color']").val(color);
	});
});

function goBack() {
	if(back)
		location.href = back;
	else
		location.href = "<?php echo site_url($path.$class) ?>";
}

function goSave() {
	$("#form_data").submit();
}

</script>