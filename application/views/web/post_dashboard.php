

<div class="row k-btn-filter">

    <div class="col-sm-12 col-md-6">
        <p>
            <button type="button" class="btn btn-lg k-filter-bl" data-toggle="modal" data-target="#KFilterBlue" style="border: 2px solid #03A9F4;">
                <i class="fa fa-filter" style="padding-right: 12%;"></i>
                Filter
            </button>
            &nbsp;&nbsp;
            <?php if ($filter['is_filtered']) { ?>
                <button type="button" class="btn btn-lg k-del-filter-bl" onClick="javascript:clearFilter()">
                    <i class="fa fa-close" style="padding-right: 5%;"></i>
                    Hapus Filter
                </button>
            <?php } ?>
        </p>

    </div>


    <div class="modal fade" id="KFilterBlue" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header" style="border-bottom: 0px solid;">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <i class="fa fa-close" style="padding-right: 12%;"></i>
                        <!-- <span aria-hidden="true">&times;</span> -->
                    </button>
                    <h4 class="modal-title col-md-4" id="myModalLabel" style="color:#4FC3F7;">
                        <i class="fa fa-filter" style=""></i>
                        <span class="k-sanspro">Filter</span>
                    </h4>
                </div>
                <br>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-sm-12 col-md-12">
                            <label for="select_tugas_dari" class="col-md-4">Tampilkan tugas dari : </label>
                            <?php echo form_dropdown('select_tugas_dari', $list_staff_helpdesk, $tugas_dari, ' id="select_tugas_dari" class="form-control input-sm select2 tugas_dari" style="width:60%;"') ?>
                        </div>
                    </div>
                    <br>

                    <div class="row" style="margin-bottom: 10px">
                        <div class="col-sm-12 col-md-12">
                            <form >
                                <label for="name" class="col-md-4">Tanggal: </label>
                                <div class="input-group col-md-3" style="float:left">
                                    <div class="input-group-addon" ><i class="fa fa-calendar"></i></div>
                                        <?php echo form_input(array('name' => 'date_started','class' => 'form-control input-sm', 'value' => $_SESSION['date_started'],'placeholder' => 'dd-mm-yyyy', 'onchange' => 'dateChange()', 'autocomplete'=>'off')) ?>
                                </div>
                                <center><span class="col-xs-1" style="width:3%">-</span></center>
                                <div class="input-group col-md-3" style="float:left">
                                    <div class="input-group-addon"><i class="fa fa-calendar"></i></div>
                                        <?php echo form_input(array('name' => 'date_finished','class' => 'form-control input-sm', 'value' => $_SESSION['date_finished'],'placeholder' => 'dd-mm-yyyy', 'onchange' => 'dateChange()', 'autocomplete'=>'off')) ?>
                                </div>
                                <span class="btn btn-primary col-md-1 hidden" name="btn-filter" id="btn-filter" style="padding-top:4px;padding-bottom: 4px;margin-left:20px">Filter</span>  
                                <span class="btn btn-success col-md-2 hidden" name="btn-showall" id="btn-showall" style="padding-top:4px;padding-bottom: 4px;margin-left:20px">Tampilkan Semua</span>  
                            </form>
                        </div>
                    </div>
                    <br>
                    <?php if (SessionManagerWeb::isStaff()) { ?>
                        <div class="row">

                            <div class="col-sm-12 col-md-12">
                                <label for="name" class="col-md-4">Tampilkan tiket saya : </label>

                                    <input type="checkbox" data-size="mini" data-toggle="toggle" data-on="Ya" data-off="Tidak" data-onstyle="primary" data-offstyle="danger" id="show_my_ticket" value="<?= $_SESSION['show_my_ticket'] ?>" <?= (isset($_SESSION['show_my_ticket'])) ? 'checked':'' ?>>

<!--                                 <input class="input-sm form-input hidden" type="checkbox" id="show_my_ticket" value="<?= $_SESSION['show_my_ticket'] ?>" <?= (isset($_SESSION['show_my_ticket'])) ? 'checked':'' ?>>

                                <div class="btn-group hidden">
                                    <?php 
                                    
                                        $url = explode(site_url(),current_url());
                                        $after_site_url = $url[1];
                                        $filter_status = "";
                                        if (isset($status_selected)) {
                                            $filter_status = "&status=".$status_selected;
                                        }
                                        if (!isset($_SESSION['show_my_ticket'])) {
                                    ?>

                                            <a id="checkbox_show_my_ticket_uncheck" href="#" onClick="changeShowMyTicket(1)"><i class="glyphicon glyphicon-unchecked" style="color:red"></i></a>
                                        <?php } 
                                        else  { ?>
                                            <a id="checkbox_show_my_ticket_check" href="#" onClick="changeShowMyTicket(0)"><i class="glyphicon glyphicon-check" style="color:green"></i></a>
                                        <?php }
                                    ?>
                                </div> -->
                            </div>
                        </div>

                        <br>
                    <?php } ?>
                </div>
                <div class="modal-footer" style="border-top: 0px solid;text-align: center;margin-top: 0px;">
                    <button type="button" class="btn k-btn-terapkan" data-dismiss="modal" style="" onClick="javascript:filter()">Terapkan Filter</button>
                </div>
            </div>
        </div>
    </div>

    <div class="col-md-1">
    </div>
    <div class="col-md-5">
        <div class="row">
            <div class="col-sm-12 col-md-12" style="">
                
                <div class="btn-group pull-right ">
                    <?php 
                        $arr_sort_by = array(
                            0 => "Terlama",
                            1 => "Terbaru",
                            2 => "Terakhir Diproses"
                        );
                        echo form_dropdown('sort_by', $arr_sort_by,$_SESSION['sort_by'], 'id="sort_by" disabled class="form-input input-sm custom-select-trigger"'); 
                    ?>
                    <!-- <select id="sources" name="sources" class="custom-select sources">
                        <option value='0' selected>Terlama</option>
                        <option value='1'>Terbaru</option>
                        <option value='2'>Terakhir Diproses</option>
                    </select> -->
                    
                </div>
                <label for="name" class="col-md-6 pull-right" style="margin: 2.5% 0%;text-align: right;">Urutkan Berdasarkan  </label>
            </div>
        </div>

    </div>
        <?php if ($filter['is_filtered']){ ?>
            <div class="col-sm-12 col-md-12">
                <div class="alert k-alert-blue" id="" style="margin-bottom: 1%;padding: 1%;">
                    <strong><?= $filter['text'] ?>
                    </strong>
                </div>
            </div>
        <?php } ?>
        <div class="col-sm-12 col-md-2 btn hidden" style="padding: 0px;box-shadow: none;margin-bottom: 0%;">
            <div class="alert" id="" style="margin-bottom: 1%;padding: 5%;cursor:pointer;width: 60%;">
                <strong>
                    <span class="k-btn-filter-close">Hilangkan Filter</span>
                </strong>
            </div>
        </div>
</div>
<div class="row">
    <?php

    foreach ($data as $key => $value) {
        ?>
        <div class="col-sm-6 col-md-6" id="div_<?= $key ?>">
            <div class="panel panel-<?= $panel[$key] ?>">
                <div class="panel-heading" style="background-color:<?= Status::statusColor($key); ?>;color:white">
                    <b><i class="fa <?= Status::statusIcon($key) ?>" style="color:white"></i> <?= Status::statusName($key) ?></b>
                    <b class="k-btn-value "><?= $value['jumlah']    ?> Pos</b>
                </div>
                <ul class="list-group" style="max-height: 600px; overflow-y: auto; overflow-x: hidden;">
                    <?php foreach ($value['data'] as $_data) { 
                        // echo '<pre>';
                        // var_dump($_data);
                        // echo '</pre>';
                        // die();
                        // if($_data['post_user_id']==)
                    ?>
                        <li onclick="detailPost(<?= $_data['id'] ?>)" class="list-group-item post-cat" style="cursor: pointer;" >
                        <div class="post-dashboard">
                        <div class="post-title">
                            <div class="row">
                                <div class="col-md-2" style="background-image:url('<?php  echo site_url() ?>assets/uploads/<?php echo $_data['nama_file'] ?>'); background-repeat: no-repeat;background-size:cover; background-position: center; height: 115px; width: 115px;">
                                    
                                </div>
                                <div class="col-md-7" style="padding-right: 0px;">
                                    <?php  if($_data['is_private'] == 0){
                                        $color = '#00AEEF';
                                        $status = 'Public';
                                        $icon = 'globe';
                                        } 
                                        else{
                                            $color = '#F44336';
                                            $status = 'Private';
                                            $icon = 'lock';
                                        }
                                        ?>
                                    <h5 style="color:<?= $color ?>"><i class="fa fa-<?= $icon ?> fa-lg" style="color:<?= $color  ?>"></i>&nbsp;<?= $status ?></h5>
                                    <h5><?php echo $_data['nama_dinas'] ?></h5>
<!--                                     <div class="pull-left user-desc" style="margin-left: 0px;">
                                        <div class="site-photo">
                                            <img class="img-circle" style="height:40px;width:40px;" src="<?=  site_url('assets/web/img/avatar.png') ?>">
                                        </div>
                                    </div> -->
                                    <div class="pull-left user-desc" style="margin-left: 0px;">
                                        <strong style="font-size: 12px;"><?php echo $_data['nama_user'] ?></strong>
                                        <div class="post-header-info">
                                            <?php echo FormatterWeb::dateToDiff($_data['created_at']) ?>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        </div>
                        </li>
                    <?php } ?>
                    <?php 
                    // if ($created_by_me==1) {
                    //     $filter_create_by_me = "&created_by_me=1";
                    // }
                    
                    ?>
                       
                </ul>
            </div>
        </div>
    <?php } ?>
</div>

<div class="modal fade" id="dashboard_modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog" style="max-width: 600px">
        <div class="modal-content">
            <div class="modal-body" id="dashboard_content" style="min-height:100px"></div>
            <div class="modal-footer">
                <div class="text-center">
                    <span class="btn btn-sm btn-default" data-dismiss="modal">Tutup</span>
                </div>
            </div>
        </div>
    </div>
</div>

<script>
    function detailPost(id){
        var url = "<?= site_url('web/post/detail')  ?>/"+id;    
        $(location).attr('href',url);
    }
    $(document).ready(function () {
        $(".select2").select2({
            tags: true
        });
        $("#select_tugas_dari").select2({
            placeholder: 'Pilih Staff'
        });
    });

    $(window).load(function(){
        $("#sort_by").removeAttr("disabled");

    });

    var id_post;
    $(document).on('click', '.post-dashboard', function() {
        id_post = $(this).attr('id-post');
    }); 

    $( function() {
        $("[name='date_started'], [name='date_finished']").datepicker({
            format:'dd-mm-yyyy'
        });
    } );

    function dateChange(){
        var fStart = $("[name='date_started'");
        var fEnd = $("[name='date_finished'");
 
        var aStart = fStart.val().split('-');
        var aEnd = fEnd.val().split('-');
        var dStart = new Date(aStart[2], aStart[1], aStart[0]);
        var dEnd = new Date(aEnd[2], aEnd[1], aEnd[0]);
        if(dStart > dEnd){
            fEnd.val(fStart.val());
        }
    }

    $("#btn-showall").on('click',function(){
        location.href="<?= site_url('web/post/dashboard') ?>?reset=1";
    });

    $("#btn-filter").on('click',function(){
        var helpdesk = $('#btn_helpdesk').attr('value');
        if (helpdesk!='' || helpdesk!='Semua' || helpdesk!='Semua Unit'){
            location.href="<?= site_url('web/post/dashboard') ?>?h="+helpdesk+"&start_date="+$("[name='date_started'").val()+'&end_date='+$("[name='date_finished'").val();
        } else {
            location.href="<?= site_url('web/post/dashboard') ?>?start_date="+$("[name='date_started'").val()+'&end_date='+$("[name='date_finished'").val();
        }
        
    });

    function clearFilter(){
        window.location.replace("<?= site_url('web/post/dashboard') ?>");
    }

    function doAlert() {
        alert('test');
      if (checkboxElem.checked) {
        alert ("hi");
      } else {
        alert ("bye");
      }
    }

    // function getnameuser(user_id){
    //     $.ajax({
    //         url : '<?= site_url('web/post/getusername') ?>/'+user_id,
    //         success: function(result){
    //             $("#btn_staff").val(result);
    //         }
    //     }); 
    // }


    $("#sort_by").on('change', function (){
        var sort_by = document.getElementById("sort_by").value;
        $.ajax({
            url : '<?= site_url('web/post/ajaxSetSort') ?>',
            type: 'post',
            cache: false,
            data: {"sort_by":sort_by},
            success: function(respon){
                window.location.reload();
                // window.location.href = window.location.href;
            }
        }); 
    });

    $("[name='showall']").on('ifChanged', function (event) {
        $("#form_check").submit();
    });

    function changeTugasDari(){
        <?php 
            unset($_SESSION['show_my_ticket']);
        ?>
        var val = $("#select_tugas_dari").val();
        var val_helpdesk = $('#btn_helpdesk').attr('value');
        if (val_helpdesk=="" || val_helpdesk=="Global" || val_helpdesk==null){
            window.location.replace("<?= site_url('web/post/dashboard') ?>/"+val);
        } else if (val=="" || val=="Semua") {
            window.location.replace("<?= site_url('web/post/dashboard') ?>/?h="+val_helpdesk);
        } else {
            window.location.replace("<?= site_url('web/post/dashboard') ?>/"+val+'?h='+val_helpdesk);
        }
    }

    // untuk show my ticket
    // function changeShowMyTicket(val){
    //     if (val=="0" || val==0){
    //         document.getElementById("show_my_ticket").value="0";
    //     } else {
    //         document.getElementById("show_my_ticket").value="1";
    //     }
    // }

    function filter(){
        // filter path sitenya
        var site = "<?= site_url('web/post/dashboard') ?>";
        var val = $("#select_tugas_dari").val(); 
        if (val!="" && val!="Semua") {
            site += "/"+val;
        } 


        /* FILTER */
        // helpdesk
        var val_helpdesk = $('#btn_helpdesk').attr('value');
        var get = '?get=1';
        if (val_helpdesk!="" && val_helpdesk!="Global" && val_helpdesk!=null) {
            get += "&h="+val_helpdesk;
        }

        // tanggal
        var start_date = $("[name='date_started'").val();
        var end_date = $("[name='date_finished'").val();
        if (start_date!=null && start_date!=''){
            get +="&start_date="+start_date;
        }
        if (end_date!=null && end_date!=''){
            get +="&end_date="+end_date;
        }

        // tiket saya
        var show_my_ticket = document.getElementById("show_my_ticket").checked;
        if (show_my_ticket){
            get += "&show_my_ticket=1";
        } else {
            get += "&show_my_ticket=0";
        }

        // redirect
        window.location.replace(site+get);
    }
</script>


<?php include(__DIR__.'/_post_js.php'); ?>