<?php
	if (empty($data)) {
?>
	<div class="alert alert-info">Belum ada data <?php echo $title ?></div>
<?php
	}
	else {
?>
<!-- <a href="javascript:goReadAll()" >
	<span class="btn btn-warning btn-group" style="margin-bottom:10px"><i class="glyphicon glyphicon-ok"></i>&nbsp;Set Baca Semua</span>
</a> -->
<div style="margin-bottom: 10px">
	<?php
        if (!empty($buttons)) {
            foreach ($buttons as $btn) {
                ?>
                <button type="<?php echo empty($btn['submit']) ? 'button' : 'submit' ?>" class="btn btn-sm btn-<?php echo $btn['type'] ?>"
                    <?php echo (empty($btn['click']) ? '' : ' onClick="' . $btn['click'] . '"') ?> <?php echo $btn['add'] ?>>
                    <i class="fa fa-<?php echo $btn['icon'] ?>"></i> <?php echo $btn['label'] ?>
                </button>
                <?php
            }
        }
    ?>
</div>
<table class="notification-table">
	<?php
		foreach($data as $v) {
			// memilih link
			switch($v['referenceType']) {
				case Notification_model::TYPE_POST_DETAIL: $link = 'post/detail'; break;
				case Notification_model::ACTION_TICKET_ASSIGN: $link = 'post/detail'; break;
				case Notification_model::TYPE_DAILY_REPORT_DETAIL: $link = 'daily_report/detail'; break;
				case Notification_model::TYPE_ISSUE_DETAIL: $link = 'issue/detail'; break;
				case Notification_model::TYPE_TODO_DETAIL: $link = 'todo/detail'; break;
				case Notification_model::TYPE_GROUP_DETAIL: $link = 'group/detail'; break;
				default: unset($link);
			}
	?>
	<tr>
		<td>
			<a data-id="<?php echo $v['id'] ?>" href="<?= site_url($path.'post/detail/'.$v['reference_id'].'/'.$v['id']) ?>">
			<table class="comment-table<?php echo $v['status'] == 'R' ? '' : ' notification-unread' ?>" style="border:0px;">
				<tr>
					<td >
						<div class="site-photo site-photo-48">
							<img 	class="img-circle" style="width:40px" 
									src="<?= site_url('assets/uploads/'.str_replace(' ', '_', $v['foto_profil'])) ?>">
						</div>
					</td>
					<td class="comment-info">
						<div class="col-md-10">
							<strong><?=  $v['sender_name'] ?></strong>
							<?php echo $v['message'] ?>
							
							<div class="post-header-info">
								<?php echo FormatterWeb::dateToDiff($v['created_at']) ?>
								
							</div>
						</div>
						<div class="col-md-2" style="text-align: right;">
							<img style="width: 50px; height: 50px;" src="<?= site_url('assets/uploads/'.$v['nama_file'])  ?>">
						</div>
						

					</td>
				</tr>
			</table>
			</a>
		</td>
	</tr>
	<?php } ?>
</table>
<br>
<?php
	}
?>

<script type="text/javascript">

$(function() {
	$(".notification-table a").mousedown(function() {
		$.get("<?php echo site_url($path.'notification/read') ?>" + "/" + $(this).attr("data-id"));
	});
});

function goReadAll() {
	location.href = "<?php echo site_url($path.'notification/read_all') ?>";
}

</script>