<div class="row menu-tab">
    <a class="menu-tab-item<?php echo ($method == 'dashboard') ? ' menu-tab-active' : '' ?>" href="<?php echo site_url($path . $class . '/dashboard') ?>">Dashboard</a>
    <a class="menu-tab-item<?php echo ($method == 'for_me') ? ' menu-tab-active' : '' ?>" href="<?php echo site_url($path . $class . '/for_me') ?>">Tugas Saya</a>
    <a class="menu-tab-item<?php echo ($method == 'me') ? ' menu-tab-active' : '' ?>" href="<?php echo site_url($path . $class . '/me') ?>">Beri Tugas</a>
</div>
<?php
echo form_open(null, array('id' => 'form_check', 'method' => 'get'));
echo form_label(form_checkbox('showall', 1, $_GET['showall']) . ' Tampilkan tugas DONE, CLOSE, INVALID, dan DUPLICATE', null, array('class' => 'label-check'));
echo form_hidden('checkshowall', 1);
echo '</form>';
?>
<?php
if (empty($data)) {
    ?>
    <div class="alert alert-info">Belum ada data <?php echo $title ?></div>
    <?php
} else {
    foreach ($data as $v) {
        ?>
        <div class="post-section post-cat post-cat-<?php echo $v['categoryId'] ?>">
            <div>
                <div class="site-photo site-photo-48 site-photo-left">
                    <img src="<?php echo isset($v['user']['photo']) ? site_url($path . 'thumb/watermark/' . $v['user']['id'] . '/48') : $nopic ?>" />
                </div>
                <div>
                    <strong><?php echo $v['user']['name'] ?></strong>
                    <?php
                    if ($v['type'] == Post_model::TYPE_FRIEND and ! empty($v['postUsers'])) {
                        $a_nama = array();
                        foreach ($v['postUsers'] as $vu)
                            $a_nama[] = $vu['name'];
                        ?>
                        mengirim ke <strong><?php echo implode(', ', $a_nama) ?></strong>
                    <?php } ?>
                </div>
                <div class="post-header-info">
                    <?php echo FormatterWeb::toIndoDate($v['date'], true) ?>
                </div>
            </div>
            <div class="post-body">
                <p>
                    <?php
                    switch ($v['priority']) {
                        case Issue_model::PRIORITY_MINOR: $label = 'MINOR';
                            $labelc = 'success';
                            break;
                        case Issue_model::PRIORITY_NORMAL: $label = 'NORMAL';
                            $labelc = 'info';
                            break;
                        case Issue_model::PRIORITY_IMPORTANT: $label = 'IMPORTANT';
                            $labelc = 'warning';
                            break;
                        case Issue_model::PRIORITY_URGENT: $label = 'URGENT';
                            $labelc = 'danger';
                            break;
                        default: unset($label, $labelc);
                    }
                    if (!empty($label)) {
                        ?>
                        <label class="label label-<?php echo $labelc ?>"><?php echo $label ?></label>
                    <?php } ?>
                    <?php
                    switch ($v['status']) {
                        case Issue_model::STATUS_OPEN: $label = 'OPEN';
                            $labelc = 'info';
                            break;
                        case Issue_model::STATUS_START: $label = 'STARTED';
                            $labelc = 'info';
                            break;
                        case Issue_model::STATUS_PROGRESS: $label = 'PROGRESS';
                            $labelc = 'info';
                            break;
                        case Issue_model::STATUS_DONE: $label = 'DONE';
                            $labelc = 'success';
                            break;
                        case Issue_model::STATUS_REOPEN: $label = 'REOPEN';
                            $labelc = 'warning';
                            break;
                        case Issue_model::STATUS_CLOSE: $label = 'CLOSE';
                            $labelc = 'success';
                            break;
                        case Issue_model::STATUS_INVALID: $label = 'INVALID';
                            $labelc = 'success';
                            break;
                        case Issue_model::STATUS_DUPLICATE: $label = 'DUPLICATE';
                            $labelc = 'success';
                            break;
                        default: unset($label, $labelc);
                    }
                    if (!empty($label)) {
                        ?>
                        <label class="label label-<?php echo $labelc ?>"><?php echo $label ?></label>
                    <?php } ?>
                    <label class="label label-danger">Deadline: <?php echo FormatterWeb::toIndoDate($v['deadline']) ?></label>
                </p>
                <?php if (!empty($v['group'])) { ?>
                    <p><strong><?php echo '[PROJECT] ' . $v['group']['name'] ?></strong></p>
                <?php } ?>
                <p><strong><?php echo $v['title'] ?></strong></p>
                <p><?php echo nl2br($v['description']) ?></p>
                <?php if (!empty($v['image'])) { ?>
                    <p><a href="<?= TemplateManagerWeb::getPostImageLink($v['image']) ?>" class="image-post"><img src="<?php echo TemplateManagerWeb::getPostImageLink($v['image']) ?>" /></a></p>
                <?php } ?>
                <?php if (!empty($v['file'])) { ?>
                    <p><a href="<?php echo $v['file']['link'] ?>"><?php echo $v['file']['name'] ?></a></p>
                <?php } ?>
                <?php if (!empty($v['link'])) { ?>
                    <p><a href="<?php echo $v['link'] ?>"><?php echo $v['link'] ?></a></p>
                <?php } ?>
            </div>
            <div class="post-footer">
                <a href="<?php echo site_url($path . $class . '/detail/' . $v['id']) ?>"><?php echo count($v['comments']) ?> komentar</a>
                <?php if (!in_array($v['status'], array(Issue_model::STATUS_CLOSE, Issue_model::STATUS_INVALID, Issue_model::STATUS_DUPLICATE))) { ?>
                    <a class="post-right" href="javascript:showStatus('<?php echo $v['id'] ?>','<?php echo $v['status'] ?>')">Ganti Status</a>
                <?php } ?>
                <?php if ($v['user']['id'] == SessionManagerWeb::getUserID()) { ?>
                    <?php /* <a class="post-right" href="<?php echo site_url($path.$class.'/edit/'.$v['id']) ?>">Edit</a> */ ?>
                    <a class="post-right post-delete" href="javascript:goDelete('<?php echo $v['id'] ?>')">Hapus</a>
                <?php } ?>
            </div>
        </div>
        <?php
    }
}
?>
<nav>
    <ul class="pager">
        <?php
        $page = $this->uri->segment(4);
        if (empty($page) || $page == 0) {
            echo '<li class="disabled"><a href = "#">Sebelumnya</a></li>';
        } else {
            echo '<li>' . anchor('web/' . $this->router->class . '/' . $this->router->method . '/' . ($page - 1) . Util::paramsToUrlGet($_GET), 'Sebelumnya') . '</li>';
        }
        if (empty($data)) {
            echo '<li class="disabled"><a href = "#">Selanjutnya</a></li>';
        } else {
            echo '<li>' . anchor('web/' . $this->router->class . '/' . $this->router->method . '/' . ($page + 1) . Util::paramsToUrlGet($_GET), 'Selanjutnya') . '</li>';
        }
        ?>
    </ul>
</nav>
<?php echo form_open(null, array('id' => 'form_delete')) ?>
<?php echo form_hidden('referer') ?>
</form>

<?php echo form_open_multipart($path . $class . '/set_status', array('id' => 'form_modal')) ?>
<div class="modal fade" id="modal_form" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Ganti Status</h4>
            </div>
            <div class="modal-body">
                <div class="row  bord-bottom">
                    <label class="col-md-3">Status Baru</label>
                    <div class="col-md-4">
                        <?php echo form_dropdown('status', $a_status, null, 'id="status" class="form-control input-sm"') ?>
                    </div>
                </div>
                <div class="row">
                    <label class="col-md-3">Keterangan</label>
                    <div class="col-md-9">
                        <?php echo form_textarea('description', null, 'id="status" class="form-control" rows="5"') ?>
                    </div>
                </div>
            </div>
            <div class="modal-footer" align="center">
                <button type="submit" class="btn btn-success"><i class="fa fa-check"></i> Ganti</button>
                <button type="button" class="btn btn-primary" data-dismiss="modal"><i class="fa fa-times"></i> Batal</button>
            </div>
        </div>
    </div>
</div>
</form>

<script type="text/javascript">

    var back = "<?php echo base_url(uri_string()) ?>";
    sessionStorage.setItem("<?php echo $class ?>.back", back);

    function goAdd() {
        location.href = "<?php echo site_url($path . $class . '/add') ?>";
    }

    function goDelete(id) {
        var hapus = confirm("Apakah anda yakin akan menghapus tugas ini?");
        if (hapus) {
            $("#form_delete").attr("action", "<?php echo site_url($path . $class . '/delete') ?>/" + id);
            $("#form_delete [name='referer']").val(back);
            $("#form_delete").submit();
        }
    }

    function showStatus(id, status) {
        $("#status").val(status);
        $("#form_modal").attr("action", "<?php echo site_url($path . $class . '/set_status') ?>/" + id);

        $("#modal_form").modal();
    }

    $("[name='showall']").on('ifChanged', function (event) {
        $("#form_check").submit();
    });

    $(document).ready(function () {
        $(".image-post").colorbox();
    });


</script>