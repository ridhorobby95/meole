
<div class="row">
	<div class="col-md-8">
        <?php 
            if (SessionManagerWeb::isStaff()) {
                                
        ?>
        <div class="nav-top">
        <ul class="nav nav-pills" style="margin-right:5px">
            <li class="pull-right">
                
                <button class="btn btn-add" onclick="$('#new_post').toggle()"><i class="fa fa-pencil"></i>&nbsp;Buat Knowledge</button>

            </li>
        </ul>
        </div>
        <?php } ?>

<?php 
    if (SessionManagerWeb::isStaff()) { 
        include(__DIR__.'/_knowledge_kirim.php'); 
    }
?>

<?php
if (empty($data)) {
    ?>
    <div class="alert alert-info">Tidak ada data yang bisa ditampilkan</div>
    <?php
} else {
    foreach ($data as $v) {
        
        if ($v['is_sticky']!='1') {
    		$style = "background-color:white";
    		if ($v['post_status'] == Ticket::SELESAI) {
    			$style = "background-color:#efefef;border:1px solid #ccc";
    		}
            ?>
    		
    		<div class="post-section" style="<?= $style ?>">    
                <div id="post-<?= $v['post_id'] ?>">
    			<?php 

    			$_data = $v;
        //         echo '<pre>';
        // var_dump($_data);
        // echo '</pre>';
        // die();
    			include(__DIR__.'/_knowledge_detail.php');
    			 ?>
                </div>
               
    		</div>
        <?php
        }
	}	
}
?>
	</div>
	<div class=" col-md-4 hidden-xs">
	<?php include(__DIR__.'/_information_right.php'); ?>
	</div>
</div>



<nav>
    <ul class="pager">
        <?php
        $page = $this->uri->segment(4);
        if (empty($page) || $page == 0) {
            echo '<li class="disabled"><a href = "#">Sebelumnya</a></li>';
        } else {
            echo '<li>' . anchor('web/' . $this->router->class . '/' . $this->router->method . '/' . ($page - 1) . Util::paramsToUrlGet($_GET), 'Sebelumnya') . '</li>';
        }
        if ($data[0]['next_page_knowledge']==0) {
            echo '<li class="disabled"><a href = "#">Selanjutnya</a></li>';
        } else {
            echo '<li>' . anchor('web/' . $this->router->class . '/' . $this->router->method . '/' . ($page + 1) . Util::paramsToUrlGet($_GET), 'Selanjutnya') . '</li>';
        }
        ?>
    </ul>
</nav>

<?php echo form_open(null, array('id' => 'form_delete')) ?>
<?php echo form_hidden('referer') ?>
</form>

<script>
    var back = "<?php echo base_url(uri_string()) ?>";
    sessionStorage.setItem("<?php echo $class ?>.back", back);

    function goAdd() {
        location.href = "<?php echo site_url($path . $class . '/add') ?>";
    }

    function goDelete(id) {
        var hapus = confirm("Apakah anda yakin akan menghapus knowledge ini?");
        if (hapus) {
            $("#form_delete").attr("action", "<?php echo site_url($path . $class . '/delete') ?>/" + id);
            $("#form_delete [name='referer']").val(back);
            $("#form_delete").submit();
        }
    }

    $(document).on('click', '.set-rating', function(){
        if (!confirm('Anda akan melakukan rating tiket ini'))
            return;
        
        var id = $(this).attr('id-post');
        var val = $(this).attr('val');
        var post_status;
        $.ajax({
            success: function(result){
                post_status = result;
                $.ajax({
                    url : '<?= site_url('web/post/ajaxsetrating') ?>',
                    type: 'post',
                    cache: false,
                    data: {"id":id,"rating":val},
                    success: function(respon){
                        if (respon != 'ERROR') {
                            alert('Terima kasih sudah melakukan rating');
                            <?php if ($method == 'dashboard') { ?>location.href = location.href;<?php } ?>
                            $('#sidebar-wrapper').load('post'+ ' .sidebar');
                            reloadDetail(id, respon);
                        }
                        else {
                            alert('Terjadi kesalahan. Silakan coba lagi atau hubungi Administrator');
                        }
                    }
                });
            }
        });     
    }); 

    $(document).ready(function () {
        $(".image-post").colorbox();
    });

</script>