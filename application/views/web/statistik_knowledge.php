<div class="row" id="main-container">
    <div class="col-sm-12 col-md-10" style="padding:0">
        <div class="panel panel-content" style="background-color: transparent;">
            <div class="panel-heading" style="padding: 0px 15px;">
                <h4 style="margin:0; font-weight:bold;font-size:26px">Statistik</h4>
            </div>
            <div class="panel-body menu-grid">

                <div class="col-xs-6 col-md-4">
                    <a href="<?= site_url('web/statistik/keyword') ?>">
                    <div class="thumbnail hvr-grow-shadow hvr-underline-from-center">
                        
<!--                         <h1><i class="glyphicon glyphicon-font"></i></h1> -->
<!--                    <div class="caption">
                            Keyword
                        </div> -->
                        
                        <img src="<?= site_url('assets/web/img/svg/font.svg')?>" style="width: 20%;margin-bottom: 7%;">
<!--                         <img src="\helpdesk\assets\web\img\svg\font.svg" style="width: 20%;margin-bottom: 7%;"> -->
                        <div class="caption">
                            Keyword
                        </div>
                        
                    </div>
                    </a>
                </div>

                <div class="col-xs-6 col-md-4">
                    <a href="<?= site_url('web/statistik/search') ?>">
                    <div class="thumbnail hvr-grow-shadow hvr-underline-from-center">
                        <img src="<?= site_url('assets/web/img/svg/magnifying-glass.svg')?>" style="width: 20%;margin-bottom: 7%;">
<!--                         <img src="\helpdesk\assets\web\img\svg\magnifying-glass.svg" style="width: 20%;margin-bottom: 7%;"> -->
                        <div class="caption">
                            Search
                        </div>

<!--                         <h1><i class="glyphicon glyphicon-search"></i></h1>
                        <div class="caption hvr-grow-shadow hvr-underline-from-center">
                            Search
                        </div> -->
                        
                    </div>
                    </a>
                </div>
            </div>
        </div>
    </div>
</div>
