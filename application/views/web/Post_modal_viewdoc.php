<div id="viewDoc" class="overlay" style="">
  <!-- Overlay content -->
  <div style="margin-top:5px;">
  <button type="button" class="close" data-dismiss="modal" aria-hidden="true" onclick="closeNav()" style="color:#fff;font-size:40px;margin-right:20px;">&times;</button>
    <center><label id="titleDoc" style="float:middle;font-size:18px;color:#fff;vertical-align: middle;margin-top:8px;"></label></center>
  </div>
  <div id="viewDocContent" class="overlay-content" style="text-align: center;">
    <!-- <iframe id="content" src="https://docs.google.com/gview?url=http://doc.integrasolusi.com/assets/uploads/posts/files/704f5cd57d85b27e2973642fe7799818-sample2.doc&embedded=true" frameborder="0" scrolling="no" seamless style="width:100%;float:middle;"></iframe> -->
  </div>
</div>
<style>
.overlay {
    /* Height & width depends on how you want to reveal the overlay (see JS below) */    
    height: 100%;
    width: 0;
    position: fixed; /* Stay in place */
    z-index: 5000; /* Sit on top */
    left: 0;
    top: 0;
    background-color: rgb(0,0,0); /* Black fallback color */
    background-color: rgba(0,0,0, 0.9); /* Black w/opacity */
    overflow-x: hidden; /* Disable horizontal scroll */
    transition: 0.5s; /* 0.5 second transition effect to slide in or slide down the overlay (height or width, depending on reveal) */
}

/* Position the content inside the overlay */
.overlay-content {
    position: relative;
    height: 90%;
    width: 90%; 
    top: 2%;
    left:5%;
}

/* The navigation links inside the overlay */
.overlay a {
    padding: 8px;
    text-decoration: none;
    font-size: 36px;
    color: #818181;
    display: block; /* Display block instead of inline */
    transition: 0.3s; /* Transition effects on hover (color) */
}

/* When you mouse over the navigation links, change their color */
.overlay a:hover, .overlay a:focus {
    color: #f1f1f1;
}

/* Position the close button (top right corner) */
.overlay .closebtn {
    position: absolute;
    top: 20px;
    right: 45px;
    font-size: 60px;
}

/* When the height of the screen is less than 450 pixels, change the font-size of the links and position the close button again, so they don't overlap */
@media screen and (max-height: 450px) {
    .overlay a {font-size: 20px}
    .overlay .closebtn {
        font-size: 40px;
        top: 15px;
        right: 35px;
    }
}
</style>
<script>
var head = $("#iframe").contents().find("head");
var css = '<style type="text/css">' +
          '.ndfHFb-c4YZDc-Wrql6b{display:none}; ' +
          '</style>';
$(head).append(css);
jQuery(document).ready(function() {
    var height = $(window).height();
    $('iframe').css('height', height*0.9);
}); 
</script>