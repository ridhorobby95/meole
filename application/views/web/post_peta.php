<?php if ($belum_rating): ?>
    <div class="alert k-alert-red" id="belum-rating" style="cursor:pointer">
        <strong>Ada post yang belum anda rating. Klik disini untuk memberikan rating ! <i class="fa fa-info-circle pull-right k-fa-info-m"></i>
        </strong>
    </div>
<?php endif ?>
<div></div>

<div class="nav-tabs-custom">

	<div class="panel panel-success">
 
		<div class="panel-body" style="background-color: #f7f7f7">
			<!-- <div class="col-md6">
				<ul class="nav nav-pills nav-justified">
		        	<li class="active"><a href="#tab_1-1" data-toggle="tab" aria-expanded="true" id="swd1">Laporan Publik</a></li>
		        	<li class=""><a href="#tab_2-2" data-toggle="tab" aria-expanded="false" id="swd2">Laporan Private</a></li>                          
		    	</ul>
			</div> -->
			<div class="row">
				<div class="col-md-2"></div>
				<div class="col-md-8">
					<ul class="nav nav-pills nav-justified">
			        	<li class="active"><a href="#tab_1-1" data-toggle="tab" aria-expanded="true" id="swd1">Laporan Publik</a></li>
			        	<li class=""><a href="#tab_2-2" data-toggle="tab" aria-expanded="false" id="swd2">Laporan Private</a></li>                          
			    	</ul>
				</div>
				<div class="col-md-2 text-right">
					<button class="btn btn-add" data-toggle="modal" data-target="#legendModal"><i class="fa fa-flag"></i> Legenda</button>
				</div>
			</div>
		</div>

		<!-- Modal -->
		<div class="modal fade" id="legendModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
		  <div class="modal-dialog" role="document">
		    <div class="modal-content">
		      <div class="modal-header">
		        <center><h2 class="modal-title" id="exampleModalLabel">Legenda Peta</h2></center>
		        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
		          <span aria-hidden="true">&times;</span>
		        </button>
		      </div>
		      <div class="modal-body">
		        <table class="table">
				  <thead>
				    <tr>
				      <th scope="col">Lambang / warna</th>
				      <th scope="col">Status</th>
				    </tr>
				  </thead>
				  <tbody>
				    <tr>
				      <th scope="row"><div style="background-color: <?= Status::statusColor(Status::STATUS_DILAPORKAN);	 ?>;width:150px;height: 30px;"></div></th>
				      <td>Dilaporkan</td>
				    </tr>
				    <tr>
				      <th scope="row"><div style="background-color: <?= Status::statusColor(Status::STATUS_RESPON);	 ?>;width:150px;height: 30px;"></div></th>
				      <td>Direspon</td>
				    </tr>
				    <tr>
				      <th scope="row"><div style="background-color: <?= Status::statusColor(Status::STATUS_PROSES);	 ?>;width:150px;height: 30px;"></div></th>
				      <td>Proses</td>
				    </tr>
				    <tr>
				      <th scope="row"><div style="background-color: <?= Status::statusColor(Status::STATUS_SELESAI);	 ?>;width:150px;height: 30px;"></div></th>
				      <td>Selesai</td>
				    </tr>
				  </tbody>
				</table>
		      </div>
		      <div class="modal-footer">
		        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
		      </div>
		    </div>
		  </div>
		</div>

		<div class="tab-content">
	    	<div class="tab-pane active" id="tab_1-1">
	            <div id="map" style="width: 100%; height: 100%; position: fixed;"></div>
	        </div><!-- /.tab-pane -->
	        <div class="tab-pane" id="tab_2-2">
	        	
	        </div><!-- /.tab-pane -->
	                
			<div id="map2" style="width: 100%; height: 100%; position: fixed;"></div>
	    </div><!-- /.tab-content -->
	</div>

    
    
</div>


 <!-- <a href="<?php echo base_url('web/user/logout') ?>">logout</a>  -->