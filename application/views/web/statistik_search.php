<div class="row">
    <div class="col-md-12">        
        <div class="box box-primary">
            <div class="box-body">
                <div class="row">
                    <div class="col-md-12">
                        <h2><?= $title ?> Search</h2>
                        <br>
                        <form >
                            <div class="input-group col-md-3" style="float:left">
                                <div class="input-group-addon" ><i class="fa fa-calendar"></i></div>
                                    <?php echo form_input(array('name' => 'start_date','class' => 'form-control input-sm datepicker', 'value' => (isset($date_start) ? $date_start : NULL),'placeholder' => 'dd-mm-yyyy', 'data-date-format' => 'dd-mm-yyyy')) ?>
                            </div>
                            <center><span class="col-xs-1" style="width:3%">-</span></center>
                            <div class="input-group col-md-3" style="float:left">
                                <div class="input-group-addon"><i class="fa fa-calendar"></i></div>
                                    <?php echo form_input(array('name' => 'end_date','class' => 'form-control input-sm datepicker', 'value' => (isset($date_end) ? $date_end : NULL),'placeholder' => 'dd-mm-yyyy','data-date-format' => 'dd-mm-yyyy' )) ?>
                            </div>
                            <span class="btn btn-primary col-md-1" name="btn-filter" id="btn-filter" style="padding-top:4px;padding-bottom: 4px;margin-left:20px">Filter</span>  
                            <span class="btn btn-success col-md-2" name="btn-showall" id="btn-showall" style="padding-top:4px;padding-bottom: 4px;margin-left:20px">Tampilkan Semua</span>  
                        </form>
                    </div>
                </div>
            </div>
            <div class="box-body">
                <div class="row">
                    <div class="col-md-12">
                        <div class="table-responsive">
                            <table class="table table-bordered table-condensed table-striped">
                                <thead>
                                    <tr>
                                        <th class="text-center" style="width:5px">No</th>
                                        <th class="text-center" >Keyword</th>
                                        <th class="text-center col-md-2">Jumlah Search</th>
                                        <th class="text-center col-md-2">Jumlah KM</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php
                                    foreach ($data as $v) {
                                        $no++;

                                            // if ($_SESSION['role_selected']==NULL || $_SESSION['role_selected']==substr(strtoupper(Role::name($v['role'][0])),0,1)) {
                                            ?>
                                                <tr>
                                                    <td class="text-right"><?= $no ?></td>
                                                    <td align="text-left">
                                                        <?= $v['keyword'] ?>
                                                    </td>
                                                    <td align="center"><?= $v['search_count'] ? $v['search_count'] : '-' ?></td>
                                                    <td align="center"><?= $v['knowledge_count'] ? $v['knowledge_count'] : '-' ?></td>
                                                </tr>
                                            <?php 
                                            //}
                                        // }
                                    }
                                    ?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>



<script type="text/javascript">
    $(".datepicker").datepicker().on("changeDate", 
        function(e){
            arrsDate = $("[name = 'start_date']").val().split("-");
            arreDate = $("[name = 'end_date']").val().split("-");
            sDate = new Date(arrsDate[2],arrsDate[1],arrsDate[0]);
            eDate = new Date(arreDate[2],arreDate[1],arreDate[0]);

            if(!isNaN(sDate) && !isNaN(eDate)){
                if(sDate > eDate){
                    if(e.target.name == 'start_date'){
                        $("[name = 'end_date']").val($("[name = 'start_date']").val());
                    }else{
                        $("[name = 'start_date']").val($("[name = 'end_date']").val());
                    }
                }
            }
        }
    );

    $("#btn-filter").on('click',function(){
        if ($("[name='end_date'").val()!='' && $("[name='end_date'").val()!='') {
            location.href="<?= site_url('web/statistik/search') ?>?start_date="+$("[name='start_date'").val()+'&end_date='+$("[name='end_date'").val();
        } else {
            alert('Tanggal mulai atau tanggal selesai tidak boleh kosong!');
        }
        
    });
    $("#btn-showall").on('click',function(){
        location.href="<?= site_url('web/statistik/search') ?>?";
    });
</script>