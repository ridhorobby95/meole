<div class="row">
    <div class="col-md-12">
        <div class="panel">
            <div class="panel-body">
                <div class="row">
                    <?php echo form_open_multipart($path . $class . '/update', array('id' => 'form_data')) ?>
                    <div class="col-sm-8">
                        <div class="row bord-bottom">
                            <label class="col-sm-4">Username</label>
                            <div class="col-sm-8">
                                <?php echo $data['username'] ?>
                            </div>
                        </div>
                        <div class="row bord-bottom">
                            <label for="name" class="col-sm-4">Nama</label>
                            <div class="col-sm-8">
                                <?php echo $data['name'] ?>
                            </div>
                        </div>
                        <div class="row bord-bottom">
                            <label for="name" class="col-sm-4">Sebagai </label>
                            <div class="col-sm-8"><?php echo Role::name($data['role']) ?>
                                <?php if ($data['nm_lemb']) echo '<br>'.$data['nm_lemb'] ?>
                            </div>
                        </div>
                        <div class="row-fluid">
                        <br>
                            <h4>Hak akses di Forlap</h4>
                            <hr>
                            <?php foreach ($data['list_peran'] as $peran) { ?>
                            <div class="row bord-bottom">
                            <div class="col-sm-4"><?= $peran['nm_peran'] ?>&nbsp;</div>
                            <div class="col-sm-4"><?= $peran['nm_lemb_induk']?$peran['nm_lemb_induk']:$peran['nm_lemb'] ?>&nbsp;</div>
                            <div class="col-sm-4"><?= !$peran['nm_lemb_induk']?'':$peran['nm_lemb'] ?>&nbsp;</div>
                            </div>
                            <?php } ?>
                        </div>                        
                        
                    </div>
                    <div class="col-sm-4 text-center">
                        <div class="row">
                            <label class="col-sm-12">Foto Profil</label>
                        </div>
                        <div class="row">
                            <div class="col-sm-12">
                                <div class="site-photo site-photo-128 site-photo-center edit_photo">
                                    <img id="img_user" class="img-circle" src="<?= site_url($data['user_photo']) ?>" data-toggle="tooltip" title="Edit Foto" />
									<!-- <span class="edit_foto">Edit Foto</span> -->
                                </div>
                                <label id="label_user" class="label label-info hidden">Simpan profil untuk mengupload foto</label>
                                <?php echo form_upload(array('name' => 'photo', 'id' => 'photo', 'class' => 'hidden')) ?>
                            </div>
                        </div>
						<br>
						<div class="row">
							<div class="col-md-12 col-sm-12 col-xs-12  hidden-xs">
                            <?php
                            if (!empty($buttons)) {
                                foreach ($buttons as $btn) {
                                    ?>
                                    <button type="<?php echo empty($btn['submit']) ? 'button' : 'submit' ?>" class="btn btn-primary btn-sm <?php echo $btn['type'] ?>" 
                                            <?php echo (empty($btn['click']) ? '' : ' onClick="' . $btn['click'] . '"') ?> <?php echo $btn['add'] ?>>
                                        <i class="fa fa-<?php echo $btn['icon'] ?>"></i> <?php echo $btn['label'] ?>
                                    </button>
                                    <?php
                                }
                            }
                            ?>
							</div>
							
							
						</div>
						
                    </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>

<?php echo form_open_multipart($path . $class . '/change_password', array('id' => 'form_modal')) ?>
<div class="modal fade" id="modal_form" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-sm">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span></button>
                <b class="modal-title">Ganti Password</b>
            </div>
            <div class="modal-body">
                <div class="row">
                    <label class="col-md-4">Sekarang</label>
                    <div class="col-md-8">
                        <?php echo form_password(array('name' => 'oldPassword', 'class' => 'form-control', 'placeholder' => 'Password Sekarang')) ?>
                    </div>
                </div>
                <div class="vspace"></div>
                <div class="row">
                    <label class="col-md-4">Baru</label>
                    <div class="col-md-8">
                        <?php echo form_password(array('name' => 'newPassword', 'class' => 'form-control', 'placeholder' => 'Password Baru', 'id' => 'newPassword')) ?>
                    </div>
                </div>
                <div class="vspace"></div>
                <div class="row">
                    <label class="col-md-4">Ulangi</label>
                    <div class="col-md-8">
                        <?php echo form_password(array('name' => 'confirmPassword', 'class' => 'form-control', 'placeholder' => 'Ulangi Password Baru', 'id' => 'confirmPassword')) ?>
                    </div>
                </div>
            </div>
            <div class="modal-footer" align="center">
                <button type="submit" class="btn btn-save"><i class="fa fa-check"></i> Ganti </button>
                <button type="button" class="btn btn-batal" data-dismiss="modal"><i class="fa fa-times"></i> Batal </button>
            </div>
        </div>
    </div>
</div>
</form>

<script type="text/javascript">

    $(function () {
        $("#img_user").css("cursor", "pointer").click(function () {
            $("#photo").click();
        });
        $("#photo").change(function () {
            $("#label_user").removeClass("hidden");
        });
        $("#form_modal").submit(function () {
            // password baru dan konfirmasinya harus sama
            if ($("#newPassword").val() != $("#confirmPassword").val()) {
                alert("Password Baru dan pengulangannya tidak sama");
                return false;
            }
        });
        $("#daily_report_users_filter").keyup(function () {
            var str = $(this).val();

            $("#div_users tr:hidden").show();
            if (str != "") {
                $(".labelinput").each(function () {
                    if ($(this).html().toLowerCase().search(str.toLowerCase()) == -1)
                        $(this).parents("tr:eq(0)").hide();
                });
            }
        });
        $("#div_users [type='checkbox']").change(function () {
            value = $(this).attr("value");
            label = $("#div_selected [data-id='" + value + "']");
            check = $(this).prop("checked");
            if (check) {
                if (label.length == 0)
                    $("#div_selected").append('<div data-id="' + value + '">' + $("label[for='" + $(this).attr("id") + "']").text() + '</div>');
            } else {
                if (label.length != 0)
                    label.remove();
            }
        });
    });

    function goSave() {
        $("#form_data").submit();
    }

    function goChangePass() {
        $("#modal_form").modal();
    }

</script>