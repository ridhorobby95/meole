<div class="row">
	<div class="col-md-8">
        <?php if ($by_tag){ ?>
            <div class="alert alert-warning">Hasil pencarian tag "<?php echo $_GET['tag'] ?>"</div>
        <?php } else { ?>
            <div class="alert alert-warning">Hasil pencarian untuk "<?php echo $_GET['search'] ?>"</div>
        <?php } ?>
        
        <?php if (empty($ticket) && empty($umum) && empty($group) && empty($knowledge) && empty($informasi)) { ?>
            <div class="alert alert-info">Tidak ada data yang bisa ditemukan</div>
        <?php }  ?>
        <?php if (!empty($knowledge)) { ?>
            <div class="panel panel-primary">
                <div class="panel-heading btn" id="panel-heading-knowledge" style="width:100%;text-align:left"> 
                    <h3 class="panel-title" id="panel-title-knowledge"><span class="glyphicon glyphicon-list"></span><b>  Pencarian di Knowledge (<?= $counter['knowledge'] ?>)</b></h3> 
                </div>
                <div class="panel-body" style="display: none" id="panel-body-knowledge">
                    <?php     
                    
                    if (empty($knowledge)) {
                        ?>
                        <div class="alert alert-info">Tidak ada data yang bisa ditampilkan</div>
                        <?php
                    } else {
                        $count=1;
                        foreach ($knowledge as $v) {
                            if ($count<=3){
                                $style = "background-color:white";
                                if ($v['post_status'] == Ticket::SELESAI) {
                                    $style = "background-color:#efefef;border:1px solid #ccc";
                                }
                                    ?>
                                        
                                <div class="post-section" style="<?= $style ?>">    
                                    <div id="post-<?= $v['post_id'] ?>">
                                        <?php 
                                        $_data = $v;

                                        include(__DIR__.'/_knowledge_detail.php');
                                        ?>
                                    </div>
                                </div>
                                <?php 
                            }
                            $count++;
                        }   
                    }
                    ?>
                </div>    
                <?php if ($count>4) { ?>
                    <div id="see-all-knowledge" style="display:none">
                        <a target="_blank" href="<?= site_url('web/'.$class.'/detail/knowledge?search='.$_GET['search']) ?>" style="color:white;font-size:14px">
                            <div style="position:relative;width:100%" class="btn btn-primary" >
                                <center>
                                <strong>See all</strong>
                                
                                </center>
                            </div> 
                        </a>
                    </div>
                <?php } ?> 
            </div>
        <?php } ?>
        <?php if (!empty($informasi)) { ?>
            <div class="panel panel-success">
                <div class="panel-heading btn" id="panel-heading-informasi" style="width:100%;text-align:left"> 
                    <h3 class="panel-title" id="panel-title-informasi"><span class="glyphicon glyphicon-list"></span><b>  Pencarian di Informasi (<?= $counter['informasi'] ?>)</b></h3> 
                </div>
                <div class="panel-body" style="display: none" id="panel-body-informasi">
                    <?php     
                    if (empty($informasi)) {
                        ?>
                        <div class="alert alert-info">Tidak ada data yang bisa ditampilkan</div>
                        <?php
                    } else {
                        $count=1;
                        foreach ($informasi as $v) {
                            if ($count<=3){
                                $style = "background-color:white";
                                if ($v['post_status'] == Ticket::SELESAI) {
                                    $style = "background-color:#efefef;border:1px solid #ccc";
                                }
                                    ?>
                                        
                                <div class="post-section" style="<?= $style ?>">    
                                    <div id="post-<?= $v['post_id'] ?>">
                                        <?php 
                                        $_data = $v;

                                        include(__DIR__.'/_informasi_detail.php');
                                        ?>
                                    </div>
                                </div>
                                <?php 
                            }
                            $count++;
                        }   
                    }
                    ?>
                </div>    
                <?php if ($count>4) { ?>
                    <div id="see-all-informasi" style="display:none">
                        <a target="_blank" href="<?= site_url('web/'.$class.'/detail/informasi?search='.$_GET['search']) ?>" style="color:white;font-size:14px">
                            <div style="position:relative;width:100%" class="btn btn-primary" >
                                <center>
                                <strong>See all</strong>
                                
                                </center>
                            </div> 
                        </a> 
                    </div>
                <?php } ?> 
            </div>
        <?php } ?>
        <?php if (!empty($umum)) { ?>
            <div class="panel panel-warning">
                <div class="panel-heading btn" id="panel-heading-umum" style="width:100%;text-align:left"> 
                    <h3 class="panel-title" id="panel-title-umum"><span class="glyphicon glyphicon-list"></span><b>  Pencarian di Group Umum (<?= $counter['umum'] ?>)</b></h3> 
                </div>
                <div class="panel-body" style="display: none" id="panel-body-umum">
                    <?php     
                    if (empty($umum)) {
                        ?>
                        <div class="alert alert-info">Tidak ada data yang bisa ditampilkan</div>
                        <?php
                    } else {
                        $count=1;
                        foreach ($umum as $v) {
                            if ($count<=3){
                                $style = "background-color:white";
                                if ($v['post_status'] == Ticket::SELESAI) {
                                    $style = "background-color:#efefef;border:1px solid #ccc";
                                }
                                    ?>
                                        
                                <div class="post-section" style="<?= $style ?>">    
                                    <div id="post-<?= $v['post_id'] ?>">
                                        <?php 
                                        $_data = $v;

                                        include(__DIR__.'/_post_detail.php');
                                        ?>
                                    </div>
                                </div>
                                <?php 
                            }
                            $count++;
                        }   
                    }
                    ?>
                </div>  
                <?php if ($count>4) { ?>
                    <div id="see-all-umum" style="display:none">
                        <a target="_blank" href="<?= site_url('web/'.$class.'/detail/umum?search='.$_GET['search']) ?>" style="color:white;font-size:14px">
                            <div style="position:relative;width:100%" class="btn btn-primary" >
                                <center>
                                <strong>See all</strong>
                                
                                </center>
                            </div> 
                        </a>
                    </div>
                <?php } ?> 
            </div>
        <?php } ?>
        <?php if (!empty($group)) { ?>
            <div class="panel panel-warning">
                <div class="panel-heading btn" id="panel-heading-group" style="width:100%;text-align:left"> 
                    <h3 class="panel-title" id="panel-title-group"><span class="glyphicon glyphicon-list"></span><b>  Pencarian di Group (<?= $counter['group'] ?>)</b></h3> 
                </div>
                <div class="panel-body" style="display: none" id="panel-body-group">
                    <?php     
                    if (empty($group)) {
                        ?>
                        <div class="alert alert-info">Tidak ada data yang bisa ditampilkan</div>
                        <?php
                    } else {
                        $count=1;
                        foreach ($group as $v) {
                            if ($count<=3){
                                $style = "background-color:white";
                                if ($v['post_status'] == Ticket::SELESAI) {
                                    $style = "background-color:#efefef;border:1px solid #ccc";
                                }
                                    ?>
                                        
                                <div class="post-section" style="<?= $style ?>">    
                                    <div id="post-<?= $v['post_id'] ?>">
                                        <?php 
                                        $_data = $v;

                                        include(__DIR__.'/_post_detail.php');
                                        ?>
                                    </div>
                                </div>
                                <?php 
                            }
                            $count++;
                        }   
                    }
                    ?>
                </div>    
                <?php if ($count>4) { ?>
                    <div id="see-all-group" style="display:none" >
                        <a target="_blank" href="<?= site_url('web/'.$class.'/detail/group?search='.$_GET['search']) ?>" style="color:white;font-size:14px">
                            <div style="position:relative;width:100%" class="btn btn-primary" >
                                <center>
                                <strong>See all</strong>
                                
                                </center>
                            </div> 
                        </a> 
                    </div>
                <?php } ?> 
            </div>
        <?php } ?>
        <?php if (!empty($ticket)) { ?>
            <div class="panel panel-danger">
                <div class="panel-heading btn" id="panel-heading-tiket" style="width:100%;text-align:left"> 
                    <h3 class="panel-title" id="panel-title-tiket"><span class="glyphicon glyphicon-list"></span><b>  Pencarian di Tiket (<?= $counter['ticket'] ?>)</b></h3> 
                </div>
                <div class="panel-body" style="display: none" id="panel-body-tiket">
                    <?php     
                    if (empty($ticket)) {
                        ?>
                        <div class="alert alert-info">Tidak ada data yang bisa ditampilkan</div>
                        <?php
                    } else {
                        $count=1;
                        foreach ($ticket as $v) {
                            if ($count<=3){
                                $style = "background-color:white";
                                if ($v['post_status'] == Ticket::SELESAI) {
                                    $style = "background-color:#efefef;border:1px solid #ccc";
                                }
                                    ?>
                                        
                                <div class="post-section" style="<?= $style ?>">    
                                    <div id="post-<?= $v['post_id'] ?>">
                                        <?php 
                                        $_data = $v;

                                        include(__DIR__.'/_'.$detail.'_detail.php');
                                        ?>
                                    </div>
                                </div>
                                <?php 
                            }
                            $count++;
                        }   
                    }
                    ?>
                </div> 
                <?php if ($count>4) { ?>
                    <div id="see-all-tiket" style="display:none">
                        <a target="_blank" href="<?= site_url('web/'.$class.'/detail/ticket?search='.$_GET['search']) ?>" style="color:white;font-size:14px">
                            <div style="position:relative;width:100%" class="btn btn-primary" >
                                <center>
                                <strong>See all</strong>
                                
                                </center>
                            </div> 
                        </a>
                    </div>
                <?php } ?>  
            </div>
        <?php } ?>
    </div>
	<div class=" col-md-4 hidden-xs">
	<?php include(__DIR__.'/_information_right.php'); ?>
	</div>
</div>

<?php echo form_open(null, array('id' => 'form_delete')) ?>
<?php echo form_hidden('referer') ?>
</form>

<?php include(__DIR__.'/_post_js.php'); ?>

<script>
    var back = "<?php echo base_url(uri_string()) ?>";
    sessionStorage.setItem("<?php echo $class ?>.back", back);

    function goAdd() {
        location.href = "<?php echo site_url($path . $class . '/add') ?>";
    }

    function goDelete(id) {
        var hapus = confirm("Apakah anda yakin akan menghapus post ini?");
        if (hapus) {
            $("#form_delete").attr("action", "<?php echo site_url($path . $class . '/delete') ?>/" + id);
            $("#form_delete [name='referer']").val(back);
            $("#form_delete").submit();
        }
    }

    $('#panel-heading-knowledge').on('click',function(){
        if ($('#panel-title-knowledge').attr('class')=='panel-title') {
            $('#panel-title-knowledge').addClass('text-center');
            $('#panel-body-knowledge').css({
                'display' : 'block'
            });
            $('#see-all-knowledge').css({
                'display' : 'block'
            });  
        } else {
            $('#panel-title-knowledge').removeClass('text-center');
            $('#panel-body-knowledge').css({
                'display' : 'none'
            });
            $('#see-all-knowledge').css({
                'display' : 'none'
            });
        }
        
    });

    $('#panel-heading-informasi').on('click',function(){
        if ($('#panel-title-informasi').attr('class')=='panel-title') {
            $('#panel-title-informasi').addClass('text-center');
            $('#panel-body-informasi').css({
                'display' : 'block'
            });
            $('#see-all-informasi').css({
                'display' : 'block'
            });  
        } else {
            $('#panel-title-informasi').removeClass('text-center');
            $('#panel-body-informasi').css({
                'display' : 'none'
            });
            $('#see-all-informasi').css({
                'display' : 'none'
            });
        }
        
    });

    $('#panel-heading-umum').on('click',function(){
        if ($('#panel-title-umum').attr('class')=='panel-title') {
            $('#panel-title-umum').addClass('text-center');
            $('#panel-body-umum').css({
                'display' : 'block'
            });
            $('#see-all-umum').css({
                'display' : 'block'
            });  
        } else {
            $('#panel-title-umum').removeClass('text-center');
            $('#panel-body-umum').css({
                'display' : 'none'
            });
            $('#see-all-umum').css({
                'display' : 'none'
            });
        }
        
    });

    $('#panel-heading-group').on('click',function(){
        if ($('#panel-title-group').attr('class')=='panel-title') {
            $('#panel-title-group').addClass('text-center');
            $('#panel-body-group').css({
                'display' : 'block'
            });
            $('#see-all-group').css({
                'display' : 'block'
            });  
        } else {
            $('#panel-title-group').removeClass('text-center');
            $('#panel-body-group').css({
                'display' : 'none'
            });
            $('#see-all-group').css({
                'display' : 'none'
            });
        }
        
    });

    $('#panel-heading-tiket').on('click',function(){
        if ($('#panel-title-tiket').attr('class')=='panel-title') {
            $('#panel-title-tiket').addClass('text-center');
            $('#panel-body-tiket').css({
                'display' : 'block'
            });
            $('#see-all-tiket').css({
                'display' : 'block'
            });  
        } else {
            $('#panel-title-tiket').removeClass('text-center');
            $('#panel-body-tiket').css({
                'display' : 'none'
            });
            $('#see-all-tiket').css({
                'display' : 'none'
            });
        }
        
    });

    $('#div_cover')
        .mouseenter(function(){
            $("#btn_edit_cover").removeClass("hidden");
    })
        .mouseleave(function(){
            $("#btn_edit_cover").addClass("hidden");
    });

    $(function () {
        $("#btn_edit_cover").click(function () {
            $("#cover").click();
        });
        $("#cover").change(function () {
            // $("#label_cover").removeClass("hidden");
            // $("#btn_simpan_cover").removeClass("hidden");
            // $("#btn_edit_cover").addClass("hidden");
            $("#form_data_cover").submit();
        });        
    });

    // function goSaveCover() {
    //     // $("#btn_simpan_cover").addClass("hidden");
    //     // $("#label_cover").addClass("hidden");
    //     // $("#btn_edit_cover").removeClass("hidden");
    //     $("#form_data_cover").submit();
    // }

<?php if ($method == 'group') { ?>
        function goBack() {
            location.href = "<?php echo site_url($path . 'group/list_me') ?>";
        }
<?php } ?>

    $(document).ready(function () {
        $(".image-post").colorbox();
    });

</script>