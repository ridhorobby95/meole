<div class="row">
    <div class="col-md-12">        
        <div class="box box-primary">
            <div class="box-body">
                <div class="row">
                    <div class="col-md-12">
                        <div class="table-responsive">
                            <br>
                            <div class="row">
                                <div class="col-sm-12 col-md-12"><span class="btn btn-sm btn-primary" id="btn_tambah_replies" style="float:right;">Tambah Jawaban</span></div>
                            </div>
                            <br>
                            <table class="table table-bordered table-condensed table-striped">
                                <thead>
                                    <tr>
                                        <th class="text-center col-md-1" style="width:30px">No</th>
                                        <th class="text-center col-md-4">Judul</th>
                                        <th class="text-center">Deskripsi</th>
                                        <th class="text-center col-md-2">Aksi</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php
                                    $page = $this->uri->segment(4);
                                    $no = ($page > 1) ? ($page * $limit - $limit + 1) : 1;
                                    foreach ($data as $v) {
                                        ?>
                                        <tr>
                                            <td class="text-right"><?= $no ?></td>
                                            <td><?= $v['name'] ?></td>
                                            <td><?= $v['reply'] ?></td>
                                            <td>
                                                <?php
                                                echo anchor('web/replies/edit/' . $v['id'], '<i class="fa fa-pencil"></i>', 'class="text-success" title="Ubah ' . $v['name'] . '"') . '&nbsp;&nbsp;';
                                                // echo anchor('javascript::hapus_jawaban('.$v['id'].')', '<i class="fa fa-trash"></i>', 'class="text-danger" title="Hapus ' . $v['name'] . '"' );
                                                ?>
                                                <a href="javascript:hapus_jawaban(<?php echo $v['id'] ?>,'<?php echo $v['name'] ?>')" class="text-danger" title="Hapus  <?php echo $v['name'] ?> ">
                                                    <i class="fa fa-trash"></i>
                                                </a>
                                            </td>
                                        </tr>
                                        <?php
                                        $no++;
                                    }
                                    ?>
                                </tbody>
                            </table>
                        </div>

                    </div>
                </div>

                <nav>
                    <div class="text-center">
                        <ul class="pagination">
                            <?php
                                $buffer = 3;
                                $before = false;
                                $after = false;
                                #BEFORE
                                for ($i = 1; $i <= $pagination; $i++) {
                                    if ($i <= $page - $buffer) {
                                        $before = true;
                                    }
                                }
                                if ($before) {
                                    echo '<li>' . anchor('web/' . $this->router->class . '/' . $this->router->method . '/1' . Util::paramsToUrlGet($_GET), 'Awal') . '</li>';
                                }
                                if (empty($page) || $page == 1) {
                                    echo '<li class="disabled"><a href = "#">Sebelumnya</a></li>';
                                } else {
                                    echo '<li>' . anchor('web/' . $this->router->class . '/' . $this->router->method . '/' . ($page - 1) . Util::paramsToUrlGet($_GET), 'Sebelumnya') . '</li>';
                                }
                                if ($before) {
                                    ?>
                                        <li class="disabled"><a href="#">...</a></li>
                                    <?php
                                }
                                #PAGINATION
                                for ($i = 1; $i <= $pagination; $i++) {
                                    if ($i >= $page + $buffer) {
                                        $after = true;
                                        continue;
                                    }
                                    if ($i <= $page - $buffer) {
                                        $before = true;
                                        continue;
                                    }
                                    empty($page) ? $page = 1 : false;
                                    ?>
                                        <li <?php echo ($page == $i ? 'class="disabled"' : ''); ?>><a href="<?php echo site_url() ?>web/<?php echo $this->router->class ?>/<?php echo $this->router->method ?>/<?php echo $i; ?><?php echo Util::paramsToUrlGet($_GET) ?>"><?php echo $i; ?></a></li>
                                    <?php
                                }
                                #AFTER
                                if ($after) {
                                    ?>
                                        <li class="disabled"><a href="#">...</a></li>
                                    <?php
                                }
                                if ($page == $pagination){
                                    echo '<li class="disabled"><a href = "#">Selanjutnya</a></li>';
                                
                                } else {
                                    echo '<li>' . anchor('web/' . $this->router->class . '/' . $this->router->method . '/' . ($page + 1) . Util::paramsToUrlGet($_GET), 'Selanjutnya') . '</li>';
                                }
                                if ($after) {
                                    echo '<li>' . anchor('web/' . $this->router->class . '/' . $this->router->method . '/' . ($pagination) . Util::paramsToUrlGet($_GET), 'Akhir') . '</li>';
                                }
                            ?>
                        </ul>
                    </div>
                </nav>

            </div>
        </div>
    </div>
</div>

<script>
    $('#btn_tambah_replies').on('click', function () {
        location.href = "<?= site_url('web/replies/add') ?>" ;
    });  
    function hapus_jawaban(id,name) {
        if (confirm('Hapus jawaban "'+name+'" ?')){
            location.href = "<?= site_url('web/replies/delete') ?>/"+id;
        }
    }  
</script>

<?php include(__DIR__.'/_post_js.php'); ?>