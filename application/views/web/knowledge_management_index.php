<div class="row">
    <div class="col-sm-12 col-sm-6 mar-bot-10px">
        <form class="form-inline" method="get" action="<?= site_url('/web/knowledge_management') ?>">
            <div class="input-group">
                <?php echo form_input(array('name' => 'search', 'value' => $_GET['search'], 'class' => 'form-control', 'id' => 'search', 'placeholder' => 'Masukkan kata kunci')) ?>
                <span class="input-group-btn">
                    <button class="btn btn-info">Cari</button>
                </span>
            </div>
        </form>
    </div>
</div>

<?php
if (empty($data)) {
    ?>
    <div class="alert alert-info">Belum ada data <?php echo $title ?></div>
    <?php
} else {
    foreach ($data as $v) {
        ?>
        <div class="post-section post-cat post-cat-<?php echo $v['categoryId'] ?>">
            <div class="post-title">
                <div class="site-photo site-photo-48 site-photo-left">
                    <img src="<?php echo isset($v['user']['photo']) ? site_url($path . 'thumb/watermark/' . $v['user']['id'] . '/48') : $nopic ?>" />
                </div>
                <div>
                    <div>
                        <strong><?php echo $v['user']['name'] ?></strong>
                    </div>
                    <div class="post-header-info">
                        <?php echo FormatterWeb::dateToDiff($v['updatedAt']); ?>
                        <br>
                        <?php
                        if (!empty($v['updatedBy'])) {
                            echo 'Last update by ' . $v['updatedBy']['name'];
                        }
                        ?>
                    </div>
                </div>
            </div>
            <div class="post-body">
                <p><strong><?php echo nl2br($v['title']) ?></strong></p>                
                <p style="height:60px;line-height:20px; overflow:hidden;"><?php echo nl2br($v['description']) ?></p>
                <p><a href="<?php echo site_url($path . 'knowledge_management' . '/detail/' . $v['id']) ?>"> Selengkapnya...</a></p>
                <?php if (!empty($v['postTags'])) { ?>
                    <p>
                        <?php
                        foreach ($v['postTags'] as $tag) {
                            echo anchor('web/knowledge_management/tag?param=' . $tag['name'], '<span class="label label-info">#' . $tag['name'] . '</span> ');
                        }
                        ?>
                    </p>
                <?php } ?>
                <?php if (!empty($v['image'])) { ?>
                    <p><a href="<?= TemplateManagerWeb::getPostImageLink($v['image']) ?>" class="image-post"><img src="<?php echo TemplateManagerWeb::getPostImageLink($v['image']) ?>" /></a></p>
                <?php } ?>
                <?php if (!empty($v['link'])) { ?>
                    <p><a href="<?php echo $v['link'] ?>"><?php echo $v['link'] ?></a></p>
                <?php } ?>
                <?php if (!empty($v['file'])) { ?>
                    <p><a href="<?php echo $v['file']['link'] ?>" target="_blank"><?php echo $v['file']['name'] ?></a></p>
                <?php } ?>
            </div>
            <div class="post-footer">
				
				<span style="margin-right: 20px">Dilihat <?php echo (int) $v['seen'] ?> Orang</span>
                <a href="<?php echo site_url($path . $class . '/detail/' . $v['id']) ?>"><?php echo count($v['comments']) ?> komentar</a>
                <a class="post-right" href="<?php echo site_url($path . $class . '/edit/' . $v['id']) ?>">Edit</a>
                <?php if ($v['user']['id'] == SessionManagerWeb::getUserID()) { ?><a class="post-right post-delete" href="javascript:goDelete('<?php echo $v['id'] ?>')">Hapus</a><?php } ?>
				
				
				<div class="btn-group btn-group-justified" role="group">
					<div class="btn-group" role="group">
							<button type="button" class="btn btn-default"><h4><span style="margin-right: 20px">Dilihat <?php echo (int) $v['seen'] ?> Orang</span></h4>
							</button>
					</div>
					<div class="btn-group" role="group">
						<button type="button" class="btn btn-default"><h4>
						<a href="<?php echo site_url($path . $class . '/detail/' . $v['id']) ?>"><?php echo count($v['comments']) ?> komentar</a>
						</h4></button>
				  </div>
				  <div class="btn-group" role="group">
					<button type="button" class="btn btn-default"><h4>
					<a class="post-right" href="<?php echo site_url($path . $class . '/edit/' . $v['id']) ?>">Edit</a>
					</h4></button>
				  </div>
				<?php if ($v['user']['id'] == SessionManagerWeb::getUserID()) { ?>
				  <div class="btn-group" role="group">
					<button type="button" class="btn btn-default"><h4>
                    <a class="post-right post-delete" href="javascript:goDelete('<?php echo $v['id'] ?>')">Hapus</a>
					</h4>
					</button>
				  </div>
                <?php } ?>
				</div>
                
            </div>
        </div>
        <?php
    }
}
?>
<nav>
    <ul class="pager">
        <?php
        $page = $this->uri->segment(4);
        if (empty($page) || $page == 0) {
            echo '<li class="disabled"><a href = "#">Sebelumnya</a></li>';
        } else {
            echo '<li>' . anchor('web/' . $this->router->class . '/' . $this->router->method . '/' . ($page - 1) . Util::paramsToUrlGet($_GET), 'Sebelumnya') . '</li>';
        }
        if (empty($data)) {
            echo '<li class="disabled"><a href = "#">Selanjutnya</a></li>';
        } else {
            echo '<li>' . anchor('web/' . $this->router->class . '/' . $this->router->method . '/' . ($page + 1) . Util::paramsToUrlGet($_GET), 'Selanjutnya') . '</li>';
        }
        ?>
    </ul>
</nav>
<?php echo form_open(null, array('id' => 'form_delete')) ?>
<?php echo form_hidden('referer') ?>
</form>

<script type="text/javascript">

    var back = "<?php echo base_url(uri_string()) ?>";
    sessionStorage.setItem("<?php echo $class ?>.back", back);

    function goAdd() {
        location.href = "<?php echo site_url($path . $class . '/add') ?>";
    }

    function goDelete(id) {
        var hapus = confirm("Apakah anda yakin akan menghapus post ini?");
        if (hapus) {
            $("#form_delete").attr("action", "<?php echo site_url($path . $class . '/delete') ?>/" + id);
            $("#form_delete [name='referer']").val(back);
            $("#form_delete").submit();
        }
    }

<?php if ($method == 'group') { ?>
        function goBack() {
            location.href = "<?php echo site_url($path . 'group/list_me') ?>";
        }
<?php } ?>

    $(document).ready(function () {
        $(".image-post").colorbox();
    });

</script>