<?php 
    $hidden_if_not_super_admin = "hidden";
    $my_helpdesk = $list_my_helpdesk[0]['id'];
    if (SessionManagerWeb::isSuperAdministrator()) {
        $my_helpdesk = "";
        $hidden_if_not_super_admin = "";
    } 
?>
<div class="row">
    <div class="col-md-12">
        <div class="box box-primary">
            <div class="box-body">

                <div class="row">
                    <?php 
                    echo form_open_multipart($path . $class . '/' . (isset($data['id']) ? 'update/' . $data['id'] : 'create'), array('id' => 'form_data'));
                    ?>
                    <!-- <input type="hidden" name="helpdesk_kirim" id="helpdesk_kirim" value="<?= $my_helpdesk ?>"> -->
                    <div class="col-sm-7">
                        <?php 
                            $status=$this->uri->segment(3);
                            //die($status);
                            if ($status=='edit') {
                                $show="Edit";
                            } else {
                                $show="Tambahkan";
                            }
                        ?>
                        <h4 style="float: right;"><strong><?php echo $show;?> Template Jawaban</strong></h4>
                    </div>
                    
                    <br>
                    <br>

                    <div class="col-sm-8">

                        <?php if (SessionManagerWeb::isSuperAdministrator()) { ?>
                            <div class="row bord-bottom <?= $hidden_if_not_super_admin ?>">
                                <label for="judul" class="col-sm-4">Kirim Ke</label>
                                <div class=" col-sm-8 btn-group" >
                                    <button type="button" class="btn btn-default btn-sm" id="btn_helpdesk">Pilih....</button>
                                    <button type="button" class="btn btn-default btn-sm dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" id="btn_for_helpdesk">
                                        <span class="caret"></span>
                                    </button>
                                    <ul class="dropdown-menu">
                                        <?php foreach ($list_helpdesk as $helpdesk) {
                                            if (strtolower($helpdesk['name']) == 'umum' || strtolower($helpdesk['name']) == 'helpdesk') {
                                                continue;
                                            } ?>
                                                <li>
                                                    <a class="helpdesk_kiriman" style="cursor:pointer" val="<?= $helpdesk['id'] ?>">
                                                            <?= $helpdesk['name'] ?>
                                                    </a>
                                                    <input type='hidden' id='helpdesk_tujuan' val="<?= $helpdesk['name'] ?>">
                                                </li>
                                        <?php 
                                        }
                                        ?>
                                    </ul>
                                </div>
                            </div>
                        <?php } ?>
                        <div class="row bord-bottom">
                            <label for="judul" class="col-sm-4">Judul</label>
                            <div class="col-sm-8">
                                <?php echo form_textarea(array('name' => 'name', 'value' => $data['name'], 'class' => 'form-control input-sm', 'id' => 'name','rows' => 1)) ?>
                            </div>
                        </div>
                        <div class="row bord-bottom">
                            <label for="deskripsi" class="col-sm-4">Deskripsi Jawaban</label>
                            <div class="col-sm-8">
                                <?php echo form_textarea(array('name' => 'reply', 'value' => strip_tags($data['reply']), 'class' => 'form-control input-sm', 'id' => 'reply', 'rows' => 5)) ?>
                            </div>
                        </div>
                        <div class="row bord-bottom">
                            <div class="col-sm-12">
                                <span class="btn btn-sm btn-kirim" style="float:right"><i class="fa fa-send"></i> Kirim</span>
                            </div>
                        </div>
                    </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>



<script type="text/javascript">

    $(function () {

    });

    function goBack() {
        location.href = "<?php echo site_url($path . $class) ?>";
    }

    function goSave() {
        $("#form_data").submit();
    }

    $('.helpdesk_kiriman').on('click', function() {
        $('#btn_helpdesk').html($(this).html());
        $('#helpdesk_kirim').val($(this).attr('val'));
        $.ajax({
            url: "<?= site_url('web/post/getgroupname') ?>/"+$(this).attr('val'),
            success: function(result) {
                $('#helpdesk_tujuan').val(result);
                
            }
        });
    });

    $(".btn-kirim").on('click',function(){
         if (checkEmptyPostReplies()) {
            alert("Pesan tidak boleh kosong!");
        } else if (checkEmptyJudulReplies()){
            alert("Judul pesan tidak boleh kosong!");
        } else if ($("#helpdesk_kirim").val()=="" ){
            alert("Helpdesk Tujuan harus dipilih terlebih dahulu!");
        } else {
            $("#form_data").submit();
        }
    });

    function checkEmptyPostReplies(){
        var text;
        text = $("#reply").val();
        text = text.replace(/\s+/g, '');
        if (text==""){
            $("#reply").val('');
            return true;
        } else{
            return false;
        }
    }

    function checkEmptyJudulReplies(){
        var judul;
        judul = $("#name").val();
        judul = judul.replace(/\s+/g, '');
        if (judul==""){
            $("#name").val('');
            return true;
        } else{
            return false;
        }
    }

</script>
<?php include(__DIR__.'/_post_js.php'); ?>