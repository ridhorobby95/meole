<div class="row">
	<div class="col-md-8">
        <div class="alert alert-warning">Hasil pencarian untuk "<?php echo $_GET['search'] ?>"</div>
        <?php if (empty($ticket) && empty($umum) && empty($group) && empty($knowledge) && empty($informasi)) { ?>
            <div class="alert alert-info">Tidak ada data yang bisa ditemukan</div>
        <?php } ?>
        <?php if (!empty($ticket)) { ?>
            <div class="panel panel-warning">
                <div class="panel-heading"> 
                    <h3 class="panel-title text-center"><b>Pencarian di Tiket</b></h3> 
                </div>
                <div class="panel-body">
                    <?php     
                    if (empty($ticket)) {
                        ?>
                        <div class="alert alert-info">Tidak ada data yang bisa ditampilkan</div>
                        <?php
                    } else {
                        $count=1;
                        foreach ($ticket as $v) {
                            if ($count<=3){
                                $style = "background-color:white";
                                if ($v['post_status'] == Ticket::SELESAI) {
                                	$style = "background-color:#efefef;border:1px solid #ccc";
                                }
                                    ?>
                                		
                                <div class="post-section" style="<?= $style ?>">    
                                    <div id="post-<?= $v['post_id'] ?>">
                                    	<?php 
                                    	$_data = $v;

                                    	include(__DIR__.'/_post_detail.php');
                                		?>
                                    </div>
                                </div>
                                <?php 
                            }
                            $count++;
                        }	
                    }
                    ?>
                </div> 
                <?php if ($count>4) { ?>
                    <a href="<?= site_url('web/search/detail/ticket?search='.$_GET['search']) ?>" style="color:white;font-size:14px">
                        <div style="position:relative;width:100%" class="btn btn-primary" >
                            <center>
                            <strong>See all</strong>
                            
                            </center>
                        </div> 
                    </a>
                <?php } ?>  
            </div>
        <?php } ?>
        <?php if (!empty($umum)) { ?>
            <div class="panel panel-warning">
                <div class="panel-heading"> 
                    <h3 class="panel-title text-center"><b>Pencarian di Group Umum</b></h3> 
                </div>
                <div class="panel-body">
                    <?php     
                    if (empty($umum)) {
                        ?>
                        <div class="alert alert-info">Tidak ada data yang bisa ditampilkan</div>
                        <?php
                    } else {
                        $count=1;
                        foreach ($umum as $v) {
                            if ($count<=3){
                                $style = "background-color:white";
                                if ($v['post_status'] == Ticket::SELESAI) {
                                    $style = "background-color:#efefef;border:1px solid #ccc";
                                }
                                    ?>
                                        
                                <div class="post-section" style="<?= $style ?>">    
                                    <div id="post-<?= $v['post_id'] ?>">
                                        <?php 
                                        $_data = $v;

                                        include(__DIR__.'/_post_detail.php');
                                        ?>
                                    </div>
                                </div>
                                <?php 
                            }
                            $count++;
                        }   
                    }
                    ?>
                </div>  
                <?php if ($count>4) { ?>
                    <a href="<?= site_url('web/search/detail/umum?search='.$_GET['search']) ?>" style="color:white;font-size:14px">
                        <div style="position:relative;width:100%" class="btn btn-primary" >
                            <center>
                            <strong>See all</strong>
                            
                            </center>
                        </div> 
                    </a>
                <?php } ?> 
            </div>
        <?php } ?>
        <?php if (!empty($group)) { ?>
            <div class="panel panel-warning">
                <div class="panel-heading"> 
                    <h3 class="panel-title text-center"><b>Pencarian di Group</b></h3> 
                </div>
                <div class="panel-body">
                    <?php     
                    if (empty($group)) {
                        ?>
                        <div class="alert alert-info">Tidak ada data yang bisa ditampilkan</div>
                        <?php
                    } else {
                        $count=1;
                        foreach ($group as $v) {
                            if ($count<=3){
                                $style = "background-color:white";
                                if ($v['post_status'] == Ticket::SELESAI) {
                                    $style = "background-color:#efefef;border:1px solid #ccc";
                                }
                                    ?>
                                        
                                <div class="post-section" style="<?= $style ?>">    
                                    <div id="post-<?= $v['post_id'] ?>">
                                        <?php 
                                        $_data = $v;

                                        include(__DIR__.'/_post_detail.php');
                                        ?>
                                    </div>
                                </div>
                                <?php 
                            }
                            $count++;
                        }   
                    }
                    ?>
                </div>    
                <?php if ($count>4) { ?>
                    <a href="<?= site_url('web/search/detail/group?search='.$_GET['search']) ?>" style="color:white;font-size:14px">
                        <div style="position:relative;width:100%" class="btn btn-primary" >
                            <center>
                            <strong>See all</strong>
                            
                            </center>
                        </div> 
                    </a> 
                <?php } ?> 
            </div>
        <?php } ?>
        <?php if (!empty($knowledge)) { ?>
            <div class="panel panel-warning">
                <div class="panel-heading"> 
                    <h3 class="panel-title text-center"><b>Pencarian di Knowledge</b></h3> 
                </div>
                <div class="panel-body">
                    <?php     
                    if (empty($knowledge)) {
                        ?>
                        <div class="alert alert-info">Tidak ada data yang bisa ditampilkan</div>
                        <?php
                    } else {
                        $count=1;
                        foreach ($knowledge as $v) {
                            if ($count<=3){
                                $style = "background-color:white";
                                if ($v['post_status'] == Ticket::SELESAI) {
                                    $style = "background-color:#efefef;border:1px solid #ccc";
                                }
                                    ?>
                                        
                                <div class="post-section" style="<?= $style ?>">    
                                    <div id="post-<?= $v['post_id'] ?>">
                                        <?php 
                                        $_data = $v;

                                        include(__DIR__.'/_post_detail.php');
                                        ?>
                                    </div>
                                </div>
                                <?php 
                            }
                            $count++;
                        }   
                    }
                    ?>
                </div>    
                <?php if ($count>4) { ?>
                    <a href="<?= site_url('web/search/detail/knowledge?search='.$_GET['search']) ?>" style="color:white;font-size:14px">
                        <div style="position:relative;width:100%" class="btn btn-primary" >
                            <center>
                            <strong>See all</strong>
                            
                            </center>
                        </div> 
                    </a>
                <?php } ?> 
            </div>
        <?php } ?>
        <?php if (!empty($informasi)) { ?>
            <div class="panel panel-warning">
                <div class="panel-heading"> 
                    <h3 class="panel-title text-center"><b>Pencarian di Informasi</b></h3> 
                </div>
                <div class="panel-body">
                    <?php     
                    if (empty($informasi)) {
                        ?>
                        <div class="alert alert-info">Tidak ada data yang bisa ditampilkan</div>
                        <?php
                    } else {
                        $count=1;
                        foreach ($informasi as $v) {
                            if ($count<=3){
                                $style = "background-color:white";
                                if ($v['post_status'] == Ticket::SELESAI) {
                                    $style = "background-color:#efefef;border:1px solid #ccc";
                                }
                                    ?>
                                        
                                <div class="post-section" style="<?= $style ?>">    
                                    <div id="post-<?= $v['post_id'] ?>">
                                        <?php 
                                        $_data = $v;

                                        include(__DIR__.'/_post_detail.php');
                                        ?>
                                    </div>
                                </div>
                                <?php 
                            }
                            $count++;
                        }   
                    }
                    ?>
                </div>    
                <?php if ($count>4) { ?>
                    <a href="<?= site_url('web/search/detail/informasi?search='.$_GET['search']) ?>" style="color:white;font-size:14px">
                        <div style="position:relative;width:100%" class="btn btn-primary" >
                            <center>
                            <strong>See all</strong>
                            
                            </center>
                        </div> 
                    </a> 
                <?php } ?> 
            </div>
        <?php } ?>
    </div>
	<div class=" col-md-4 hidden-xs">
	<?php include(__DIR__.'/_information_right.php'); ?>
	</div>
</div>

<?php echo form_open(null, array('id' => 'form_delete')) ?>
<?php echo form_hidden('referer') ?>
</form>

<?php include(__DIR__.'/_post_js.php'); ?>

<script>
    var back = "<?php echo base_url(uri_string()) ?>";
    sessionStorage.setItem("<?php echo $class ?>.back", back);

    function goAdd() {
        location.href = "<?php echo site_url($path . $class . '/add') ?>";
    }

    function goDelete(id) {
        var hapus = confirm("Apakah anda yakin akan menghapus post ini?");
        if (hapus) {
            $("#form_delete").attr("action", "<?php echo site_url($path . $class . '/delete') ?>/" + id);
            $("#form_delete [name='referer']").val(back);
            $("#form_delete").submit();
        }
    }

    $('#div_cover')
        .mouseenter(function(){
            $("#btn_edit_cover").removeClass("hidden");
    })
        .mouseleave(function(){
            $("#btn_edit_cover").addClass("hidden");
    });

    $(function () {
        $("#btn_edit_cover").click(function () {
            $("#cover").click();
        });
        $("#cover").change(function () {
            // $("#label_cover").removeClass("hidden");
            // $("#btn_simpan_cover").removeClass("hidden");
            // $("#btn_edit_cover").addClass("hidden");
            $("#form_data_cover").submit();
        });        
    });

    // function goSaveCover() {
    //     // $("#btn_simpan_cover").addClass("hidden");
    //     // $("#label_cover").addClass("hidden");
    //     // $("#btn_edit_cover").removeClass("hidden");
    //     $("#form_data_cover").submit();
    // }

<?php if ($method == 'group') { ?>
        function goBack() {
            location.href = "<?php echo site_url($path . 'group/list_me') ?>";
        }
<?php } ?>

    $(document).ready(function () {
        $(".image-post").colorbox();
    });

</script>