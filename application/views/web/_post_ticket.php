		<div class="post-title">
			<div class="site-photo site-photo-38 site-photo-left">
				<img class="img-circle" src="<?php echo isset($_data['user_photo']) ? site_url($path.'thumb/watermark/'.$_data['post_user_id'].'/38') : $nopic ?>">
			</div>
			<div>
					<strong><?php echo $_data['user_name'] ?></strong>
					<?php if($_data['type'] == Post_model::TYPE_GROUP) { ?>
					mengirim di <strong><?php echo $_data['group_name'] ?></strong>
					<?php }
						else if($_data['type'] == Post_model::TYPE_FRIEND and !empty($_data['postUsers'])) {
							$a_nama = array();
							foreach($_data['postUsers'] as $v)
								$a_nama[] = $v['name'];
					?>
					mengirim ke <strong><?php echo implode(', ',$a_nama) ?></strong>
					<?php } ?>
	             <?php if ($_data['ticket_kode']) { ?>
	             	<div class="pull-right" align="right">
	             	<a href="<?= site_url($path . 'post/detail/' . $_data['post_id']) ?>"><span class="fa fa-bookmark"></span> <?= $_data['ticket_kode'] ?></a><br>
	             	<div style="clear:both;height:5px">&nbsp;</div>
					
					<?php if ($_SESSION['integra']['auth']['userid'] == $_data['post_user_id'] && (1==2)) { ?>
						<div style="clear: both;margin-bottom: 5px"></div>
						<?php if ($_data['ticket_status'] == 'B') { ?>
						<div class="pull-right"><span class="btn btn-xs btn-success toggle_set"  p_id="<?= $_data['post_id'] ?>"><i class="glyphicon glyphicon-ok"></i> Set Selesai</span></div>
						<?php } else { ?>
						<div class="pull-right"><span class="btn btn-xs btn-warning toggle_set"  p_id="<?= $_data['post_id'] ?>"><i class="glyphicon glyphicon-share-alt"></i> Set Buka</span></div>
					<?php } ?>

	             	</div>
	             <?php  } ?>
				</div>
				<div class="post-header-info">
					<?php echo FormatterWeb::dateToDiff($_data['post_created_at']) ?>
					<?php /* if($_data['createdAt'] != $_data['updatedAt']) { ?>
					<span class="post-right">Diedit <?php echo FormatterWeb::dateToDiff($_data['updatedAt']) ?></span>
					<?php } */ ?>
				</div>
			</div>
		</div>
		<div class="post-body">
				<?php if ($_data['post_title']) { ?>
				<h4><b><?php echo nl2br($_data['post_title']) ?></b></h4>
				<?php } ?>
				<p><?php echo nl2br($_data['post_description']) ?></p>

			<?php foreach ($_data['images'] as $image) { ?>
                <a href="<?= $image['ori'] ?>" class="image-post"><img src="<?= $image['thumb'] ?>" style="max-height:100px" /></a> &nbsp;
				<?php } ?>
			<?php } ?>

			<?php if(!empty($_data['link'])) { ?>
			<p><h5><a href="<?php echo $_data['link'] ?>"><?php echo $_data['link'] ?></a></h5></p>
			<?php } ?>
			<?php if(!empty($_data['file'])) { ?>
			<p><a href="<?php echo $_data['file']['link'] ?>" target="_blank"><?php echo $_data['file']['name'] ?></a></p>
			<?php } ?>

			<?php if (SessionManagerWeb::isAdministrator()) { ?>
			<div class="row-fluid">
				<hr style="margin:5px;margin-left: 0;margin-right: 0">
				<?php if ($_data['ticket_status'] == 'B') { ?>
				<div class="pull-right"><span class="btn btn-xs btn-success toggle_set"  p_id="<?= $_data['post_id'] ?>"><i class="glyphicon glyphicon-ok"></i> Set Selesai</span></div>
				<?php } else { ?>
				<div class="pull-right"><span class="btn btn-xs btn-warning toggle_set"  p_id="<?= $_data['post_id'] ?>"><i class="glyphicon glyphicon-share-alt"></i> Set Buka</span></div>
				<?php } ?>
				<div class="pull-right"><span class="btn btn-xs btn-success tugaskan"  p_id="<?= $_data['post_id'] ?>"><i class="glyphicon glyphicon-user"></i> Tugaskan</span>&nbsp;</div>
				<div style="clear: both"></div>
			</div>
			<?php } ?>
		</div>
        <div class="post-footer">
					<a href="<?php echo site_url($path . ($_data['categoryId'] != Category_model::KNOWLEDGE_MANAGEMENT ? $class : 'knowledge_management') . '/detail/' . $_data['post_id']) ?>" class="btn btn-footer">
					<span class="fa fa-comment"></span>&nbsp;
					<?php echo (int) $_data['comment_number'] ?> Komentar
					<?php if ($_data['post_user_id'] == SessionManagerWeb::getUserID() && $_data['categoryId'] != Category_model::KNOWLEDGE_MANAGEMENT) { ?>
					<?php } ?>
					</a>
            
	             	<?php 
	             		$label = 'red'; 
						if ($_data['ticket_status'] == Ticket::BUKA) 
							$label = 'red'; 
						elseif ($_data['ticket_status'] == Ticket::SELESAI) 
							$label = 'green';
					?>
            <div class="pull-right">
					<span class="label label-<?= $label ?>"><?= Ticket::name($_data['ticket_status']) ?></span>
					<span class="label label-blue"><?= $_data['ticket_tujuan'] ?></span>
                </div>
        </div>
