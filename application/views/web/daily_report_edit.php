<div class="row">
    <div class="col-md-12">
        <div class="box box-primary">
            <div class="box-body">
                <div class="row">
                    <div class="col-md-12">
                        <?php echo form_open_multipart($path . $class . '/' . (isset($id) ? 'update/' . $id : 'create'), array('id' => 'form_data')) ?>
                        <div class="row bord-bottom">
                            <label for="post_users" class="col-sm-3">Laporkan Ke</label>
                            <div class="col-sm-5">
                                <div id="div_selected" style="margin-bottom:5px">
                                    <?php foreach ($data['usersID'] as $v) { ?>
                                        <div data-id="<?php echo $v ?>"><?php echo $a_user[$v] ?></div>
                                    <?php } ?>
                                </div>
                                <?php // echo form_multiselect('postUsers[]',$a_user,$data['postUsers'],'id="post_users" size="5" class="form-control"') ?>
                                <div id="div_users" class="site-listbox">
                                    <table>
                                        <tr>
                                            <td colspan="2">
                                                <?php echo form_input(array('class' => 'form-control input-sm', 'id' => 'post_users_filter', 'placeholder' => 'Cari Teman', 'autocomplete' => 'off')) ?>
                                            </td>
                                        </tr>
                                        <?php foreach ($a_user as $k => $v) { ?>
                                            <tr>
                                                <td><?php echo form_checkbox(array('name' => 'postUsers[]', 'id' => 'post_users_' . $k, 'value' => $k, 'checked' => (in_array($k, $data['usersID'])), 'class' => 'simple')) ?></td>
                                                <td><label for="post_users_<?php echo $k ?>" class="labelinput"><?php echo $v ?></label></td>
                                            </tr>
                                        <?php } ?>
                                    </table>
                                </div>
                            </div>
                        </div>

                        <div class="row bord-bottom">
                            <label class="col-sm-7">=== Selamat Pagi Rekan SEVIMA ;)===</label>
                        </div>
                        <div class="row bord-bottom">
                            <label for="date" class="col-sm-3">Tanggal</label>
                            <div class="col-sm-5">
                                <div class="input-group">
                                    <div class="input-group-addon"><i class="fa fa-calendar"></i></div>
                                    <?php echo form_input(array('name' => 'date', 'value' => (isset($id) ? FormatterWeb::formatDate($data['date']) : date('d-m-Y')), 'class' => 'form-control input-sm', 'placeholder' => 'dd-mm-yyyy', 'readonly' => 'readonly')) ?>
                                </div>
                            </div>
                        </div>
                        <div class="row bord-bottom">
                            <div class="col-xs-12">
                                <?php echo form_textarea(array('name' => 'workdone', 'value' => set_value('workdone', $data['workdone']), 'class' => 'form-control input-sm', 'rows' => 6, 'placeholder' => 'Yang sudah beres :D', 'style' => 'margin-bottom:5px')) ?>
                                <?php echo form_textarea(array('name' => 'issue', 'value' => set_value('issue', $data['issue']), 'class' => 'form-control input-sm', 'rows' => 4, 'placeholder' => 'Ada Masalah Ga ?', 'style' => 'margin-bottom:5px')) ?>
                                <?php echo form_textarea(array('name' => 'description', 'value' => set_value('description', $data['description']), 'class' => 'form-control input-sm', 'rows' => 6, 'placeholder' => 'Apa yang mau dikerjakan Hari ini (supaya besok lebih nyantai)')) ?>
                            </div>
                        </div>
                        <div class="row bord-bottom">
                            <label for="date" class="col-sm-3">Terpaksa Lembur?</label>
                            <div class="col-sm-5">
                                <?php echo form_checkbox('is_over_time', 1, (($data['is_over_time'] == 1) ? true : false)) ?>
                            </div>
                        </div>
                        <div class="row bord-bottom">
                            <label for="date" class="col-sm-3">Jam Lembur/Part Time</label>
                            <div class="col-sm-5">
                                <div class="input-group">
                                    <?php echo form_input(array('name' => 'start_work', 'value' => $data['start_work'], 'class' => 'form-control input-sm', 'placeholder' => '08:30')) ?>
                                    <span class="input-group-addon" id="sizing-addon3">-</span>
                                    <?php echo form_input(array('name' => 'end_work', 'value' => $data['end_work'], 'class' => 'form-control input-sm', 'placeholder' => '16:30')) ?>
                                </div>
                            </div>
                        </div>
                        <div class="row bord-bottom">
                            <label for="link" class="col-sm-3">Link URL</label>
                            <div class="col-sm-9">
                                <?php echo form_input(array('name' => 'link', 'value' => $data['link'], 'class' => 'form-control input-sm', 'id' => 'link')) ?>
                            </div>
                        </div>
                        <div class="row bord-bottom">
                            <label for="image" class="col-sm-3">Gambar</label>
                            <div class="col-sm-6">
                                <?php echo form_upload(array('name' => 'image', 'id' => 'image')) ?>
                            </div>
                        </div>
                        <div class="row bord-bottom">
                            <label for="video" class="col-sm-3">Video (Jgn gede2 ya)</label>
                            <div class="col-sm-6">
                                <?php echo form_upload(array('name' => 'video', 'id' => 'video')) ?>
                            </div>
                        </div>
                        <div class="row bord-bottom">
                            <label for="file" class="col-sm-3">File Attach</label>
                            <div class="col-sm-6">
                                <?php echo form_upload(array('name' => 'file', 'id' => 'file')) ?>
                            </div>
                        </div>
                        <?php echo form_hidden('referer') ?>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">

    var back = sessionStorage.getItem("<?php echo $class ?>.back");

    $(function () {
        $("[name='date']").datepicker({format: 'dd-mm-yyyy'});
        $("[name='referer']").val(back);

        $("#post_users_filter").keyup(function () {
            var str = $(this).val();

            $("#div_users tr:hidden").show();
            if (str != "") {
                $(".labelinput").each(function () {
                    if ($(this).html().toLowerCase().search(str.toLowerCase()) == -1)
                        $(this).parents("tr:eq(0)").hide();
                });
            }
        });

        $("#div_users [type='checkbox']").change(function () {
            value = $(this).attr("value");
            label = $("#div_selected [data-id='" + value + "']");
            check = $(this).prop("checked");
            if (check) {
                if (label.length == 0)
                    $("#div_selected").append('<div data-id="' + value + '">' + $("label[for='" + $(this).attr("id") + "']").text() + '</div>');
            } else {
                if (label.length != 0)
                    label.remove();
            }
        });
    });

    function goBack() {
        if (back)
            location.href = back;
        else
            location.href = "<?php echo site_url($path . $class) ?>";
    }

    function goSave() {
        $("#form_data").submit();
    }

    $(document).ready(function () {
        $(".image-post").colorbox();
    });


</script>