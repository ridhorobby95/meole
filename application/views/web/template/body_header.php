	<?php 
	// die($_SESSION['is_show_alert_login']); 
	?>
	<?php if (SessionManagerWeb::isAuthenticated()) { ?>
		<nav class="navbar navbar-default navbar-fixed-top">
			<header class="header">
				<div class="container-fluid">         
                    <div class="row">
					<div class="col-md-12">
					<ul class="nav navbar-nav navbar-left">
						<li>
						<a href="#menu-toggle"  id="menu-toggle">
							<button type="button" class="navbar-toggle collapsed" data-toggle="offcanvas"  aria-expanded="false" href="#menu-toggle">
								<span class="sr-only">Toggle navigation</span>
								<span class="icon-bar"></span>
								<span class="icon-bar"></span>
								<span class="icon-bar"></span>
							</button>
						 </a>
						 </li>
						 <li>
							<div class="navbar-brand">
								<div class="text-center">
									<a href="<?= site_url('web/post') ?>">
									<img src="<?php echo base_url('assets/web/img/aci-logo.png') ?>" height="38">
									<h3 class="navbar-title hidden-sm hidden-xs">Poso Meole</h3>
									</a>
								</div>
							</div>
						</li>
						<li>
						</li>
					
					</ul>
					<!-- Top Menu Items -->
							<ul class="nav navbar-nav navbar-right">
								<li class="dropdown hidden-sm hidden-xs">
									<?php if ($type=='M'){ ?>
									<a href="<?php 	echo site_url($path.'post?type=T') ?>" style="padding-right:5px;">
										<i style="color:#FFF" class="fa fa-list big-icon" title="Tampilan lini masa"></i>
									</a>
									<?php } else{ ?>
									<a href="<?php 	echo site_url($path.'post?type=M') ?>" style="padding-right:5px;">
										<i style="color:#FFF" class="fa fa-map big-icon border-icon" title="Tampilan peta"></i>
									</a>
									<?php 	} ?>
									
								</li>
								<li class="dropdown hidden-sm hidden-xs">
									<a href="<?php echo site_url($path.'notification/me') ?>" style="padding-right:5px;">
										<i style="color:#FFF" class="fa fa-bell big-icon border-icon"></i><?php if (!empty($jmlnotif)) { ?>
											<span class="label label-notif"><?php echo ($jmlnotif > 99) ? '99+' : $jmlnotif ?></span>
										<?php } ?>									
									
									</a>
								</li>
								<li class="dropdown hidden-sm hidden-xs">
									<a href="#" class="dropdown-toggle" data-toggle="dropdown" style="padding:10px;">
										<div class="site-photo site-photo-left">
										<img class="img-circle" style="height:30px;width:30px" src="<?= SessionManagerWeb::getPhoto() ?>" />
										</div>                                        &nbsp;
										<b><?php echo ucfirst(SessionManagerWeb::getName()) ?></b>&nbsp;
										<b class="fa fa-angle-down"></b>
									</a>
									<ul class="dropdown-menu">
										<li>
											<a href="<?php echo site_url($path . 'user/detail') ?>"><i class="fa fa-fw fa-user"></i> Profile</a>
										</li>
										<li>
											<a href="<?php echo site_url($path . 'user/logout') ?>"><i class="fa fa-fw fa-power-off"></i> Log Out</a>
										</li>
									</ul>
								</li>
							</ul>
					<ul class="nav navbar-nav navbar-left" style="margin-left:25px; margin-right: 10px;width:40%">
						<li class="dropdown" style="width:100%"> 	
							<form action="<?= site_url('web/post/search') ?>" method="POST" class="search-form">						
								<div class="form-group has-feedback" style="width:100%">
									<label for="search" class="sr-only">Search</label>
									<input type="text" class="form-control" name="keyword" id="search" placeholder="search" value="<?= $_SESSION['meole_search']['keyword']  ?>">
									<!-- <a href="javascript:search()"> -->
                                    	<span class="glyphicon glyphicon-search form-control-feedback" id="icon_search"></span>
                                    
								</div>
							</form>
						</li>
					</ul>	
						
					</div>
					</div>
				</div>
				

			</header>
		</nav>
		<body></body>
		<div class="hidden-xs" style="height:40px"></div>
		
		<!-- Menu Toggle Script -->
		<script>
		$("#menu-toggle").click(function(e) {
			e.preventDefault();
			$("#wrapper").toggleClass("toggled");
			$(".sidebar").toggle("slide");
    	});

    	$(document).ready(function() {
    		<?php 	
    			if($hide == true){
    				?>
    					$("#wrapper").removeClass("toggled");
						$(".sidebar").hide();
    				<?php
    			}
    		 ?>
			
    	});

	function tog(){
		method = "<?php echo $method ?>";
		if (method!='edit') {
			$('.btn-batal').click();	
		}
		<?php if ($_SESSION['is_show_alert_login']==1) { ?>
			$('#login_modal').modal('toggle');

		<?php 
			$_SESSION['is_show_alert_login']=0;
			} 
		?>
		if (window.screen.availWidth<=480) {
			$("#menu-toggle").click();
		}
	}
	
	function search(){
        location.href = "<?= site_url('web/search/?search=') ?>" + $('#search').val() ;
    }
	
	
    </script>
<?php } ?>


