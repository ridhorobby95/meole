<section class="sidebar" style="background-color: white; margin-left: 0px; box-shadow: 0px 6px 6px #e0e0e0;     width: 247px; padding-left: 20px; padding-top: 2px; padding-bottom: 10px;
">
	<!--  -->
	<ul class="sidebar-menu">
		<li><a href="<?= site_url('web/post?type=M') ?>" class="k-sidebar-green active">
			<span class="label">
				<i class="fa fa-map fa-2x"></i>
				<!-- <img src="\helpdesk\assets\web\img\svg\home.svg" style="width: 10%;margin-bottom: 3%; "> -->
				<!-- <i class="fa fa-home"></i> -->
			</span> Peta</a>
		</li>
		<li class="divider"></li>
		<li><a href="<?= site_url('web/post?type=T') ?>" class="k-sidebar-green">
			<span class="label">
				<i class="fa fa-list fa-2x"></i>
				<!-- <img src="\helpdesk\assets\web\img\svg\home.svg" style="width: 10%;margin-bottom: 3%; "> -->
				<!-- <i class="fa fa-home"></i> -->
			</span> Linimasa</a>
		</li>
		<li class="divider"></li>
		<li><a href="#"><b>Beranda</b></a></li>
		<li><a href="<?= site_url('web/post/dashboard') ?>" class="k-sidebar-green <?= $this->uri->segment(3)=='dashboard' && !$this->uri->segment(4) ? 'active' : '' 	 ?>">
			<span class="label">
				<i class="fa fa-tachometer fa-2x"></i>
				<!-- <img src="\helpdesk\assets\web\img\svg\home.svg" style="width: 10%;margin-bottom: 3%; "> -->
				<!-- <i class="fa fa-home"></i> -->
			</span> Semua Laporan</a>
		</li>
		<li><a href="<?= site_url('web/post/dashboard/'.Status::STATUS_DILAPORKAN) ?>" class="k-sidebar-green <?= $this->uri->segment(3)=='dashboard' && $this->uri->segment(4)==Status::STATUS_DILAPORKAN ? 'active' : '' 	 ?>">
			<span class="label">
				<i class="fa fa-book fa-2x"></i>
				<!-- <img src="\helpdesk\assets\web\img\svg\home.svg" style="width: 10%;margin-bottom: 3%; "> -->
				<!-- <i class="fa fa-home"></i> -->
			</span> Lapor</a>
		</li>
		<li><a href="<?= site_url('web/post/dashboard/'.Status::STATUS_RESPON) ?>" class="k-sidebar-green <?= $this->uri->segment(3)=='dashboard' && $this->uri->segment(4)==Status::STATUS_RESPON ? 'active' : '' 	 ?>">
			<span class="label">
				<i class="fa fa-bullhorn fa-2x"></i>
				<!-- <img src="\helpdesk\assets\web\img\svg\home.svg" style="width: 10%;margin-bottom: 3%; "> -->
				<!-- <i class="fa fa-home"></i> -->
			</span> Respon</a>
		</li>
		<li><a href="<?= site_url('web/post/dashboard/'.Status::STATUS_PROSES) ?>" class="k-sidebar-green <?= $this->uri->segment(3)=='dashboard' && $this->uri->segment(4)==Status::STATUS_PROSES ? 'active' : '' 	 ?>">
			<span class="label">
				<i class="fa fa-clock-o fa-2x"></i>
				<!-- <img src="\helpdesk\assets\web\img\svg\home.svg" style="width: 10%;margin-bottom: 3%; "> -->
				<!-- <i class="fa fa-home"></i> -->
			</span> Proses</a>
		</li>
		<li><a href="<?= site_url('web/post/dashboard/'.Status::STATUS_SELESAI) ?>" class="k-sidebar-green <?= $this->uri->segment(3)=='dashboard' && $this->uri->segment(4)==Status::STATUS_SELESAI ? 'active' : '' 	 ?>">
			<span class="label">
				<i class="fa fa-check fa-2x"></i>
				<!-- <img src="\helpdesk\assets\web\img\svg\home.svg" style="width: 10%;margin-bottom: 3%; "> -->
				<!-- <i class="fa fa-home"></i> -->
			</span> Selesai</a>
		</li>
		<li><a href="<?= site_url('web/post/dashboard/'.Status::STATUS_DIARSIPKAN) ?>" class="k-sidebar-green <?= $this->uri->segment(3)=='dashboard' && $this->uri->segment(4)==Status::STATUS_DIARSIPKAN ? 'active' : '' 	 ?>">
			<span class="label">
				<i class="fa fa-archive fa-2x"></i>
				<!-- <img src="\helpdesk\assets\web\img\svg\home.svg" style="width: 10%;margin-bottom: 3%; "> -->
				<!-- <i class="fa fa-home"></i> -->
			</span> Diarsipkan</a>
		</li>
		<br>
		<li class="divider"></li>
		<li><a href="#"><b></b></a></li>
		<li><a href="<?= site_url('web/replies') ?>" class="k-sidebar-green">
			<span class="label">
				<i class="fa fa-reply fa-2x"></i>
				<!-- <img src="\helpdesk\assets\web\img\svg\home.svg" style="width: 10%;margin-bottom: 3%; "> -->
				<!-- <i class="fa fa-home"></i> -->
			</span> Template Jawaban</a>
		</li>
		<li><a href="<?= site_url('web/post/rates') ?>" class="k-sidebar-green">
			<span class="label">
				<i class="fa fa-star-half-o fa-2x"></i>
				<!-- <img src="\helpdesk\assets\web\img\svg\home.svg" style="width: 10%;margin-bottom: 3%; "> -->
				<!-- <i class="fa fa-home"></i> -->
			</span> Rating</a>
		</li>
		
		<?php if (SessionManagerWeb::isAdministrator()) { ?>
		<li>
			<a href="<?= site_url('web/user/daftar') ?>" class="k-sidebar-green">
				<span class="label"><i class="fa fa-user fa-2x"></i></span>Pengguna
			</a>
		</li>
<!-- 		<li>
			<a href="<?= site_url('web/user/daftar') ?>" class="k-sidebar-green">
				<span class="label"><i class="fa fa-users fa-2x"></i></span>Dinas
			</a>
		</li> -->
		<?php } ?>
			
	</ul>

	<br>
        <h6>Copyright &copy; Pemkab Poso 2019.</h6>

</section>

<style type="text/css">
	li{
		padding-top: 5px;
		padding-bottom: 5px;
	}
</style>