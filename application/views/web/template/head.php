<meta charset="utf-8">
<title><?= 'MEOLE - ' . $title ?></title>
<meta name="description" content="MEOLE">
<meta name="author" content="MEOLE">
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
<link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/web/css/bootstrap.min.css') ?>">
<link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/web/css/font-awesome.min.css') ?>">
<link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/web/css/ionicons.min.css') ?>">
<link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/web/css/style.css') ?>">
<link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/web/css/horizontal-menu/style.css') ?>">
<link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/web/css/custom.css') ?>">
<link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/web/css/datepicker/datepicker3.css') ?>">
<link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/web/css/colorbox/colorbox.css') ?>">
<link rel="icon" type="image/png" href="<?php echo base_url('assets/web/img/aci-logo.png') ?>">
<script type="text/javascript" src="<?php echo base_url('assets/web/js/external/html5shiv.js') ?>"></script>
<script type="text/javascript" src="<?php echo base_url('assets/web/js/external/respond.min.js') ?>"></script>
<script type="text/javascript" src="<?php echo base_url('assets/web/js/external/jquery.min.js') ?>"></script>
<script type="text/javascript" src="<?php echo base_url('assets/web/js/jquery-ui-1.10.3.min.js') ?>"></script>
<script type="text/javascript" src="<?php echo base_url('assets/web/js/bootstrap.min.js') ?>"></script>
<!-- <script type="text/javascript" src="<?php echo base_url('assets/web/js/AdminLTE/app.js') ?>"></script> -->
<script type="text/javascript" src="<?php echo base_url('assets/web/js/plugins/cookie/js.cookie.js') ?>"></script>
<script type="text/javascript" src="<?php echo base_url('assets/web/js/plugins/chart.js/Chart.js') ?>"></script>
<script type="text/javascript" src="<?php echo base_url('assets/web/js/plugins/chart.js/Chart.min.js') ?>"></script>
<script type="text/javascript" src="<?php echo base_url('assets/web/js/plugins/colorbox/jquery.colorbox-min.js') ?>"></script>
<script type="text/javascript" src="<?php echo base_url('assets/web/js/leaflet.js') ?>"></script>
<script type="text/javascript" src="<?php echo base_url('assets/web/js/plugins/datepicker/bootstrap-datepicker.js') ?>"></script>
<script type="text/javascript" src="<?php echo base_url('assets/web/js/plugins/timepicker/bootstrap-timepicker.min.js') ?>"></script>

<link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/web/css/select2.css') ?>">
<!-- <script type="text/javascript" src="<?php echo base_url('assets/web/js/external/select2.full.js') ?>"></script> -->

<link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/web/css/select2.min.css') ?>">
<script type="text/javascript" src="<?php echo base_url('assets/web/js/external/select2.full.min.js') ?>"></script>

<link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/web/css/bootstrap-year-calendar.min.css') ?>">
<link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/web/css/leaflet.css') ?>">
<script type="text/javascript" src="<?php echo base_url('assets/web/js/bootstrap-year-calendar.min.js') ?>"></script>

<!-- <script src="//code.jquery.com/jquery-1.11.1.min.js"></script> -->

<link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/web/css/bootstrap-toggle.min.css') ?>">
<script type="text/javascript" src="<?php echo base_url('assets/web/js/bootstrap-toggle.min.js') ?>"></script>

<!-- <link href="https://gitcdn.github.io/bootstrap-toggle/2.2.2/css/bootstrap-toggle.min.css" rel="stylesheet">
<script src="https://gitcdn.github.io/bootstrap-toggle/2.2.2/js/bootstrap-toggle.min.js"></script>
 -->