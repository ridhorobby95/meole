
<div class="container-fluid">
    <div id="wrapper" class="toggled" style="padding-left: 0px;">

        <!-- Page Content -->
        <div id="page-content-wrapper">

            <div class="container-fluid">
                <aside class="right-side" style="margin-top: 0px;">
                    <section class="content" style="padding-left: 0px; padding-right: 0px;">
                                               
                         <!-- Sidebar -->
                        <div style="position: absolute; z-index: 2;">
                            <?php echo $sidebar ?>
                        </div>
                        <!-- /#sidebar-wrapper -->
                        
                        <?php 
                        $view = $class.'_'.$method;
                        // echo '<pre>';
                        // var_dump($show_filter_helpdesk_in);
                        // die();
                        if (SessionManagerWeb::isStaff() and (in_array($class,$show_filter_helpdesk_in_class) or in_array($method,$show_filter_helpdesk_in_method) ) and !in_array($view,$exclude_filter_helpdesk_in_view)) { ?>
                            <div class="" style="margin-bottom: 1%;background-color: #B3E5FC;border-radius: 3px;cursor:pointer;font-size:20px;padding:10px 20px 5px 20px;">
                                <strong>
                                    <left>
                                        <?= $method=='dashboard' ? ucwords($method) : ucwords($class) ?>
                                        <div class="btn-group">
                                            <input type="button" class="btn btn-success" id="btn_helpdesk" name="btn_helpdesk" value="<?= $_SESSION['helpdesk_selected'] ?  $_SESSION['helpdesk_selected'] :  'Global' ?>" style="font-size:20px;background-color: transparent;color:#333;border-color:transparent;box-shadow: 0px 0px 0px 0px;font-weight:bold;bottom:2px;padding-left: 2px">
                                            <?php if (SessionManagerWeb::isStaff()){ ?>
                                                <button type="button" class="k-btn k-btn-title btn-xs hvr-ripple-out dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" style="">
                                                    <span class="caret"></span>
                                                </button>
                                            <?php } ?>
                                            <ul class="dropdown-menu">
                                                <li>
                                                    <a style="text-align:left" class="set-helpdesk" value="">
                                                        Global
                                                    </a>
                                                </li>
                                                <?php foreach ($list_helpdesk as $k => $v) { ?>
                                                <li>
                                                    <a style="text-align:left" class="set-helpdesk" value="<?= $v['name'] ?>">
                                                        <?= $v['name'] ?>
                                                    </a>
                                                </li>
                                                <?php } ?>
                                            </ul>
                                        </div>

                                    </left>
                                </strong>
                            </div>
                        <?php } ?>

                        <?php if (isset($srvok)) { ?>
                            <div class="alert alert-<?php echo ($srvok ? 'success' : 'danger') ?> alert-dismissable">
                                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                                <?php echo $srvmsg ?>
                            </div>
                        <?php } ?>
                        <div style="min-height:700px">
                            <!-- <div style="position:relative">
                                <img src="http://localhost/helpdesk/assets/uploads/groups/images/large-4a161bf46c6d2282262d0f6f5d5929f47f388834d07bc489fd9a695d8ae0d8cb-img-20150922-091002.jpg" height="100" width="500">
                                 h2 style="position:absolute;" ><span>Hey I am text on an Image</span></h2>
                             </div> -->
                            <!-- <br>
                            <br> -->
                            <?php echo $body ?>
                        </div>
                    </section>
                </aside>
            </div>
            
        </div>
        <!-- /#page-content-wrapper -->

    </div>
    
    <!-- /#wrapper -->
</div>
<style>#ajax-loader {display:none;z-index: 111001;position: fixed;left: 0;top: 0;right: 0;bottom: 0;margin: auto;}
@media screen and (max-width: 480px){
    /*#sidebar-wrapper {
        display: none;
    }*/
    /*#wrapper.toggled{
        padding-left: 0px;
    }*/
}
</style>

<img src="<?= base_url() ?>assets/web/img/ajax-loader.gif" id="ajax-loader">
<script>
  $( document ).ajaxStart(function() {$( "#ajax-loader" ).show();});
  $( document ).ajaxComplete(function() {$( "#ajax-loader" ).hide();});
</script> 
