<div class="post-body">
    <div class="row">
        <?php if ($this->uri->segment(3) == 'detail') { ?>
        <div class="col-md-3 col-sm-3" style="cursor:pointer;">
            <div class="view" style="background-image:url('<?php echo $_data['nama_file'] ?>'); background-repeat: no-repeat;background-size:cover; background-position: center; height: 248px; " data_id="<?= $_data['id']  ?>">
            </div>
        <?php } else{ ?>
        <div class="col-md-3 col-sm-3" style="cursor:pointer;" onclick="detailPost(<?= $_data['id'] ?>)">
            <div style="background-image:url('<?php  echo site_url() ?>assets/uploads/<?php echo $_data['nama_file'] ?>'); background-repeat: no-repeat;background-size:cover; background-position: center; height: 248px; " >
            </div>
        <?php  } ?>
            
        </div>
        <div class="col-md-9 col-sm-9" style="padding-left: 27px;">
            <div class="row" style="margin-top:10px;">
                <?php  if($_data['is_private'] == 0){
                            $color = '#00AEEF';
                            $status = 'Public';
                            $icon = 'globe';
                        } 
                        else{
                            $color = '#F44336';
                            $status = 'Private';
                            $icon = 'lock';
                        }
                        ?>
                <div class="col-md-4 col-xs-4 status-private" style="margin-top: 0px;">
                    
                    <h4 style="color:<?= $color ?>; <?= ($this->uri->segment(3) == 'detail') ? 'margin-top:0px;' : '';  ?>"><i class="fa fa-<?= $icon ?> fa-lg" style="color:<?= $color  ?>;"></i>&nbsp;<?= $status ?>
                    <?php if ($this->uri->segment(3) == 'detail' && $_data['status'] != Status::STATUS_DIALIHKAN): ?>
                    <button class="btn" onclick="choosePrivate('pilih')" style="background-color: white; border-color: #212121;">Ubah</button>

                    <?php endif ?>
                    </h4>
                    
                </div>
                <div class="col-md-4 ubah-private hidden" style="margin-top:10px;">
                    <form method="post" action="<?= site_url('web/post/updatePrivate/'.$_data['id']) ?>" onsubmit="return confirm('apakah anda yakin?')">  
                        <select class="form-control pull-right select2" name="private">
                            <option value="1" <?= $_data['is_private'] == 1 ? 'selected' : '' ?>>Private</option>
                            <option value="0" <?= $_data['is_private'] == 0 ? 'selected' : '' ?>>Public</option>
                        </select>
                        <button type="submit" class="btn btn-primary"> Simpan</button>
                        <a href="#" onclick="choosePrivate('batal')" title="batal"><span><i style="color:#f44336" class="fa fa-close"></i></span></a>
                    </form>
                </div>
                <div class="col-md-8 col-xs-8 status-bar">
                    <?php   
                        $color = 'white';
                        $label = '<i class="fa fa-check"></i>';
                     ?>
                    
                    <div class="pull-right ticket-desc" style="width:23%; background-color: <?= Status::statusColor($_data['status']);    ?> ">
                        <h5 class="text-center">
                            <a href="<?= site_url($path . 'post/detail/' . $_data['post_id']) ?>" style="color:<?= $color   ?>">
                                <?= Status::statusName($_data['status']) ?>
                            </a><br>
                        </h5>
                    </div>
                    <?php if ($this->uri->segment(3) == 'detail' && $_data['status'] != Status::STATUS_DIALIHKAN &&  $_data['status'] != Status::STATUS_SELESAI): ?>
                    <div class="pull-right" style="margin-right: 10px;">
                        <button class="btn" onclick="chooseStatus('pilih')" style="background-color: white; border-color: #212121;">Ubah</button>
                    </div>
                    
                    <?php endif ?>
                </div>
                <div class="col-md-8 ubah-status hidden" style="margin-top:10px;">
                    <form method="post" action="<?= site_url('web/post/updateStatus/'.$_data['id']) ?>" onsubmit="return confirmStatus()">
                        <div class="pull-right" style="margin-right: 10px;">
                            <button type="submit" class="btn btn-primary"> Simpan</button>
                            <a href="#" onclick="chooseStatus('batal')" title="batal"><span><i style="color:#f44336" class="fa fa-close"></i></span></a>
                        </div>
                        <div class="pull-right" style="margin-right: 10px;">
                            
                                <select class="form-control pull-right select-status" name="status" id="statusChange">
                                    <?php foreach (Status::getArray() as $key=>$value): ?>
                                        <?php if ($key != Status::STATUS_DILAPORKAN): ?>
                                        <option value="<?= $key ?>" <?= $key==$_data['status'] ? 'selected' : '' ?>><?=   $value ?></option>
                                        <?php endif ?>
                                        
                                    <?php endforeach ?>
                                </select>
                            
                        </div>
                    </form>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12 col-sm-12" style="margin-bottom: 10px;">
                    <?php if ($this->uri->segment(3) != 'unrated'): ?>
                         <?php if (SessionManagerWeb::isAdministrator()){ ?>
                            <a href="<?= site_url('web/post/detail/'.$_data['id']) ?>"><h3 style="color:#333">
                                <?php echo $_data['nama_dinas'] ?>
                            </h3></a>
                            <p>
                                <i><b>Ditugaskan kepada <?= $_data['petugas']  ?></b></i>
                            </p>
                            <?php } else { ?>
                                <p>
                                    <i><b>Ditugaskan kepada <?= $_data['petugas']  ?></b></i>
                                </p>
                            <?php } ?>
                    <?php endif ?>
                   
                    
                    <?php if ($this->uri->segment(3) == 'detail'){ ?>
                    <div class="row" style="border-top: 1px #000 solid;border-bottom: 1px #000 solid; margin-right: 1px; border-color: #E0E0E0; text-align: center;">
                        <div class="col-md-2 vl">
                            <span><i class="fa fa-thumbs-up fa-lg"></i>&nbsp; <?= $_data['vote'] ?> Votes</span>
                        </div>
                        <div class="col-md-2 vl">
                            <span><i class="fa fa-eye fa-lg"></i>&nbsp; <?= $_data['view'] ?> Views</span>
                        </div>
                        <div class="col-md-3 vl">
                            <span><i class="fa fa-phone fa-lg"></i>&nbsp; <?= $_data['no_hp'] ?></span>
                        </div>
                        
                        <div class="col-md-5 vl" style="border:none!important;">
                            <span onclick="goMaps(<?= $_data['latitude'] ?>,<?= $_data['longitude'] ?>)" style="cursor: pointer;"><i class="fa fa-map-marker fa-lg"></i>&nbsp; <?= substr($_data['alamat'], 0, 40); ?></span>
                        </div>
                    </div>
                    <?php } else{ ?>
                    <hr>
                    <?php   } ?>
                    
                    <p style="margin-top: 10px;">
                        <?php echo nl2br($_data['description']) ?><br>
                        <?php if ($this->uri->segment(3) != 'detail'): ?>
                         <a target="_blank" href="https://www.openstreetmap.org/#map=19/?q=<?= $_data['latitude'] ?>,<?= $_data['longitude'] ?>"><i class="fa fa-map-marker fa-lg"></i>&nbsp;<?= $_data['alamat'] ?></a>  
                        <?php endif ?>
                       
                    </p>
                </div>
            </div>
            <div class="row" style="margin-bottom: 10px; padding-right: 18px;">
                <div class="col-md-12">
                    <div class="pull-left user-desc">
                        <div class="site-photo">
                            <img class="img-circle profile-picture" src="<?=  site_url('assets/uploads/'.$_data['foto_profil']) ?>">
                        </div>
                    </div>
                    <div class="pull-left user-desc" style="margin-top: 4%;">
                        <strong><?= $_data['nama_user'] ?   $_data['nama_user'] : 'Tidak Diketahui' ?></strong>
                        <div class="post-header-info">
                            <?php echo FormatterWeb::dateToDiff($_data['created_at']) ?>
                        </div>
                    </div>
                    <div class="pull-right user-desc icon-bottom rating" style="margin-top: 6%;">
                        
                        <?php if ($this->uri->segment(3) == 'detail'){ ?>
                        <span class="detail-icon-bottom"><i class="fa fa-comments fa-lg"></i> <?= $_data['jumlah_diskusi'] ?> Diskusi</span>
                        <span class="detail-icon-bottom"><i class="fa fa-comments fa-lg"></i> <?= $_data['jumlah_komentar'] ?> Komentar</span>
                        <span class="detail-icon-bottom"><i class="fa fa-share-alt fa-lg"></i> <?= '0' ?> Dibagikan</span>
                        
                        <?php } else{ ?>
                        <?php if ($this->uri->segment(3) != 'unrated'){ ?>
                            <?php if ($this->uri->segment(3) == 'rates'){ ?>
                                <?php if ($_data['rating_exist']){ ?>
                                    <div class="star-rating-post">
                                        <p class="total-rating"><b><?php echo $_data['rating'] ?></b> <span class="fa fa-star"></span></p>
                                    </div>
                                <?php } else{ ?>
                                    <div class="star-rating">
                                        <span class="fa fa-star-o" data-rating="1"></span>
                                        <span class="fa fa-star-o" data-rating="2"></span>
                                        <span class="fa fa-star-o" data-rating="3"></span>
                                        <span class="fa fa-star-o" data-rating="4"></span>
                                        <span class="fa fa-star-o" data-rating="5"></span>
                                        <input type="hidden" name="rating" class="rating-value" value="0">
                                        <input type="hidden" name="post_id" class="rating-id" value="<?php echo $_data['id'] ?>">
                                    </div>
                                <?php } ?>
                                
                            <?php } else{?>
                                <span><i class="fa fa-thumbs-up fa-lg"></i> <?= $_data['vote'] ?> Votes</span>
                            <?php } ?>
                            
                        <?php } else { ?>
                            <div class="star-rating">
                                <span class="fa fa-star-o" data-rating="1"></span>
                                <span class="fa fa-star-o" data-rating="2"></span>
                                <span class="fa fa-star-o" data-rating="3"></span>
                                <span class="fa fa-star-o" data-rating="4"></span>
                                <span class="fa fa-star-o" data-rating="5"></span>
                                <input type="hidden" name="rating" class="rating-value" value="0">
                                <input type="hidden" name="post_id" class="rating-id" value="<?php echo $_data['id'] ?>">
                            </div>
                        <?php } ?>
                        
                        <?php } ?>
                        
                    </div>
                </div>
                <!-- <div class="col-md-6 align-bottom">
                    <
                </div> -->
                
            </div>            
            
            
        </div>
    </div>

</div>

<?php include('Post_modal_viewdoc.php'); ?>
<?php include('_post_js.php'); ?>
<script type="text/javascript">
    function openNav() {
        document.getElementById("viewDoc").style.width = "100%";
    }

    function closeNav() {
        document.getElementById("viewDoc").style.width = "0%";
    }
    
    function confirmStatus(){
        var val = $('#statusChange').val();
        if(val == 'S'){
            var a = confirm('Apakah anda yakin? \nKetika status menjadi selesai, maka tiket dianggap telah selesai dan status tidak bisa diubah kembali');
        }else{
            var a = confirm('Apakah anda yakin?');
        }
        if(a){
            return true;
        }else{
            return false;
        }
    }
    $(document).on('click','.view', function(){
        // alert($(this).attr('data_id'));
        var id = $(this).attr('data_id');
        if(id == "0"){
            alert("Anda belum memilih foto");
        }else{
            viewDocument(id);
        }   
    });

    function viewDocument(id){
        var http = new XMLHttpRequest();
        http.open("POST", "<?php echo site_url('/web/post/view/');?>" + "/" + id, true);
        http.setRequestHeader("Content-type","application/x-www-form-urlencoded");
        var param = "";
        http.send(param);
        http.onload = function() {
            var response = JSON.parse(http.responseText);
            // console.log(response);
            var namaFile = response['nama_file'];
            var title = response['title'];
            var filename = response['filename'];
            document.getElementById("titleDoc").textContent = title;
            var extss = filename.split(".");
            var exts = extss[extss.length-1];
            var ext = exts.toLowerCase();
            var divContent = document.getElementById("viewDocContent");
            if(ext == "jpg" || ext =="png" || ext == "jpeg" || ext=='gif' || ext=='bmp'){
                inner = "<img src='<?php echo base_url("assets/uploads/posts/photos/");?>/"+filename+"' alt='"+ext+"' style='height:100%;float:middle;'/>";
            }
            divContent.innerHTML = inner;
            openNav();
        }
    }


    function detailPost(id){
        var url = "<?= site_url('web/post/detail')  ?>/"+id;    
        $(location).attr('href',url);
    }

    function goMaps(latitude, longitude){
        var url = "http://maps.google.com/?q="+latitude+","+longitude;   
        // $(location).attr('href',url);
        window.open(url);
    }

    function choosePrivate(pilihan){
        if(pilihan=='pilih'){
            $('.status-private').addClass('hidden');
            $('.ubah-private').removeClass('hidden');
        }else{
            $('.status-private').removeClass('hidden');
            $('.ubah-private').addClass('hidden');
        }
        
    }

    function chooseStatus(pilihan){
        if(pilihan=='pilih'){
            $('.status-bar').addClass('hidden');
            $('.ubah-status').removeClass('hidden');
        }else{
            $('.status-bar').removeClass('hidden');
            $('.ubah-status').addClass('hidden');
        }
        
    }

    $(".select2").select2({
        width: '40%'
    });

    $(".select-status").select2({
        width: '200px'
    });

</script>