<?php 
	$this->load->model('Dinas_diversion_model');
	$this->load->model('Comment_model');
 ?>
<script>var g_abs_url = '<?= base_url() ?>index.php/web/';</script>
<script src="<?= base_url('assets/web/js/lov.js') ?>"></script>
<div class="row" style="margin-bottom: 10px;">
    <div class="col-md-4" style="padding-left: 0px;">
        <button class="btn btn-basic btn-filter" onclick="goBack()"><i class="fa fa-chevron-left"></i> Kembali</button>
    </div>
    <div class="col-md-8">
    	<?php if ($data['status'] != Status::STATUS_DIALIHKAN && !$detail_diversion): // jika statusnya bukan di alihkan dan tidak memiliki data detail_diversion ?> 
    	<form>
	    	<div class="row">
	    		<?php if (SessionManagerWeb::isAdministrator() && $data['status']!=Status::STATUS_PROSES): ?>
	    		<div class="col-md-6">
	    			<label>Pindahkan Operator kepada</label>
	    			<select class="form-control pull-right select-option-operator" name="operator">
	    					<option value=""></option>
	    				<?php foreach ($listOperator as $operator): ?>
	    					<?php if ($operator['id'] != $data['petugas_id']): ?>
	    					<option value="<?= $operator['id']  ?>" id="operator_<?= $operator['id'] ?>"><?= $operator['name'] ?></option>	
	    					<?php endif ?>
	    					
	    				<?php endforeach ?>
			    	</select>
	    		</div>
	    		<?php endif ?>
	    		<?php if ($data['petugas_id'] == SessionManagerWeb::getUserID() || SessionManagerWeb::isAdministrator() ): ?>
	    		<div class="col-md-6 <?= !SessionManagerWeb::isAdministrator() || $data['status']==Status::STATUS_PROSES || $data['status']==Status::STATUS_SELESAI  ? 'pull-right' : ''  ?>">
	    			<label>Pindahkan Post ke</label>
	    			<select class="form-control pull-right select-option-dinas" name="dinas" id="ganti-dinas">
	    						<option value=""></option>
			    		<?php foreach ($dinas as $din): ?>
			    				<?php if ($din['idunit'] != $data['dinas_id']): ?>
			    				<option value="<?= $din['idunit'] ?>" id="dinas_<?= $din['idunit'] ?>"><?= $din['name'] ?></option>
			    				<?php endif ?>
			    				
			    		<?php endforeach ?>
			    	</select>
	    		</div>
	    		<?php endif ?>
	    	</div>
    	</form>
    	<?php endif ?>
    	
    	

    	<!-- <select class="form-control pull-right select2" style="width: 200px;">
    		<option>a</option>
    		<option>a</option>
    		<option>a</option>
    	</select> -->
    </div>
</div>
<div class="row" id="main-container">
	<?php
		if ($data['id']==NULL) {
			$_data = NULL;
			echo '<div class="alert alert-info">Tidak ada data yang bisa ditampilkan</div>';
		} else {
			$style = "background-color:white";
			if ($data['post_status'] == Ticket::SELESAI) {
				$style = "background-color:#efefef;border:1px solid #ccc";
			}
		
		?>
			<div class="post-section" style="<?= $style ?>">	
				<div id="post-<?= $data['post_id'] ?>">
				<?php 
				$_data = $data;
				include(__DIR__.'/_post_detail.php');
				?>
				</div>             
				<?php 
					
				?>
			</div>
		<?php } ?>
</div>
<?php if ($data['status'] == Status::STATUS_DIALIHKAN && $detail_diversion['status'] == 'S' && SessionManagerWeb::isAdministrator()): ?>
<div class="row" style="background-color: white; padding-bottom: 10px; margin-bottom: 15px;">
	<div class="col-md-12">
		<h4>Detail Pengajuan Pengalihan Dinas</h4>
		<form method="post" action="<?= site_url('web/post/saveDiversionApprove/'.$detail_diversion['id']) ?>">
		<input type="hidden" name="post_id" value="<?= $data['id'] ?>">
		<table class="table">
			<tr>
				<td><b>Nama Petugas</b></td>
				<td><?= $detail_diversion['nama_petugas']  ?></td>
			</tr>
			<tr>
				<td><b>Dialihkan ke</b></td>
				<td><?= $detail_diversion['nama_dinas']  ?></td>
			</tr>
			<tr>
				<td><b>Alasan</b></td>
				<td><?= $detail_diversion['reason']  ?></td>
			</tr>
			<tr>
				<td><b>Tindakan</b></td>
				<td>
					<select class="select-option-approve" name="approve" required="">
						<option value=""></option>
						<option value="A">Terima</option>
						<option value="R">Tolak</option>
						<option value="D">Alihkan ke dinas lain</option>
					</select>
				</td>
			</tr>
			<tr class="alih-dinas" style="display: none;">
				<td><b>Pilih Dinas</b></td>
				<td>
					<select class="form-control pull-right select-option-dinas" name="dinas" id="choose_dinas">
	    						<option value=""></option>
			    		<?php foreach ($dinas as $din): ?>
			    				<?php if ($din['idunit'] != $detail_diversion['dinas_id'] && $din['idunit'] != $data['dinas_id']): ?>
			    				<option value="<?= $din['idunit'] ?>" id="<?= $din['idunit'] ?>"><?= $din['name'] ?></option>
			    				<?php endif ?>
			    				
			    		<?php endforeach ?>
			    	</select>
				</td>
			</tr>
			<tr class="alih-alasan" style="display: none;">
				<td><b>Alasan</b></td>
				<td>
					<textarea class="form-control" name="alasan"></textarea>
				</td>
			</tr>
		</table>
			<button type="submit" class="btn btn-primary pull-right"><i class="fa fa-floppy-o" style="color: white"></i> Simpan</button>
		</form>
	</div>
</div>
<?php endif ?>

<?php if (isset($detail_diversion) && (($data['status'] != Status::STATUS_DIALIHKAN && $detail_diversion['status'] != 'S' && SessionManagerWeb::isAdministrator()) || !SessionManagerWeb::isAdministrator() )     ): ?>
<div class="row" style="background-color: white; padding-bottom: 10px; margin-bottom: 15px;">
	<div class="col-md-12">
			<a href="#pengalihan" data-toggle="collapse" class="collapse-a" style="text-decoration: none; color:#333333">
				<h4>Detail Keputusan Pengajuan Pengalihan Dinas 
					<span class="pull-right collapse-span">
						<i class="fa fa-chevron-down rotate down"></i>
					</span>
				</h4>
			</a>
		</div>
	<div class="col-md-12 collapse in" id="pengalihan">
		<input type="hidden" name="post_id" value="<?= $data['id'] ?>">
		<table class="table">
			<tr>
				<td><b>Nama Petugas</b></td>
				<td><?= $detail_diversion['nama_petugas']  ?></td>
			</tr>
			<tr>
				<td><b>Dialihkan ke</b></td>
				<td><?= $detail_diversion['nama_dinas']  ?></td>
			</tr>
			<tr>
				<td><b>Alasan Pemindahan</b></td>
				<td><?= $detail_diversion['reason']  ?></td>
			</tr>
			<tr>
				<td><b>Keputusan</b></td>
				<td><?= Dinas_diversion_model::statusName($detail_diversion['status'])  ?></td>
			</tr>
			<?php if ($detail_diversion['status'] == Dinas_diversion_model::DIALIHKAN): ?>
			<tr>
				<td><b>Dinas Keputusan Admin</b></td>
				<td><?= $detail_diversion['nama_dinas_2']  ?></td>
			</tr>
			<?php endif ?>
			<?php if ($detail_diversion['reason_confirm'] != '' || $detail_diversion['reason_confirm'] != null): ?>
			<tr>
				<td><b>Alasan Admin</b></td>
				<td><?= $detail_diversion['reason_confirm']  ?></td>
			</tr>
			<?php endif ?>
			
			
		</table>
	</div>
</div>
<?php endif ?>
<div class="row" style="background-color: white; padding-bottom: 10px; margin-bottom: 15px;">
	<div class="col-md-12">
		<a href="#diskusi" data-toggle="collapse" style="text-decoration: none; color:#333333"><h4>Diskusi (<?= $data['jumlah_diskusi']  ?>) <span class="pull-right"><i class="fa fa-chevron-down rotate pull-right"></i></span></h4></a>
	</div>
	<div class="col-md-12" style="padding-left: 48px;">
		
	</div>
	<div class="col-md-12 collapse" id="diskusi" style="padding-left: 48px;">
		<hr>
			
			<?php foreach ($discuss as $com): ?>
			<div class="media">
			  <a class="pull-left" href="#">
			    <img class="img-circle profile-picture" src="<?=  site_url('assets/uploads/'.str_replace(' ', '_',$com['foto_profil'] )) ?>">
			  </a>
			  <div class="media-body">
			    <h4 class="media-heading"><?= $com['nama_user']  ?></h4>
			    <p class="discuss-content"><?= $com['text']  ?></p>
			    <p class="discuss-date"><?php echo FormatterWeb::dateToDiff($com['created_at']) ?></p>
			  </div>
			</div>
			<?php endforeach ?>
			<div class="media">
			  <a class="pull-left" href="#">
			    <img class="img-circle profile-picture" src="<?=  SessionManagerWeb::getPhoto() ?>">
			  </a>
			  <div class="media-body">
			    <h4 class="media-heading"><?= SessionManagerWeb::getName()  ?></h4>
			    	<form method="post" action="<?= site_url('web/comment/create/detail')  ?>">
						
						<input type="hidden" name="post_id" value="<?= $data['id']  ?>">
						<input type="hidden" name="type" value="<?= Comment_model::DISKUSI  ?>">
						<textarea class="form-control" style="height:125px;resize:none; margin-top: 10px;" id="text_discuss" name="text"></textarea>
						<br>
						<button class="pull-right btn btn-primary" type="submit"> Kirim</button>
					</form>
					<button class="pull-right btn btn-info" style="margin-right: 10px;" onclick="openLOV('btn_template_jawaban','templatejawaban', -50, -200, 'addTemplateJawaban')"> Template Jawaban</button>
			  </div>
			</div>
			
		
	</div>
</div>

<div class="row" style="background-color: white; padding-bottom: 15px; margin-bottom: 15px;">
	<div class="col-md-12">
		<a href="#komentar" data-toggle="collapse" style="text-decoration: none; color:#333333;"><h4>Komentar Masyarakat (<?= $data['jumlah_komentar']  ?>) <span class="pull-right"><i class="fa fa-chevron-down rotate pull-right"></i></span></h4></a>
	</div>
	<div class="col-md-12 collapse" id="komentar" style="padding-left: 48px;">
		<hr>
		<?php foreach ($response as $res): ?>
		<div class="media">
		  <a class="pull-left" href="#">
		    <img class="img-circle profile-picture" src="<?=  site_url('assets/uploads/'.str_replace(' ', '_',$res['foto_profil'] )) ?>">
		  </a>
		  <div class="media-body">
		    <h4 class="media-heading"><?= $res['nama_user']  ?></h4>
		    <p class="discuss-content"><?= $res['text']  ?></p>
		    <p class="discuss-date"><?php echo FormatterWeb::dateToDiff($res['created_at']) ?></p>
		  </div>
		</div>
		
		<hr style="margin-top: 0px;">
		<?php endforeach ?>
		

		<?php if (1==0): ?>
		<div class="row">
			<!-- <div class="col-md-1"></div> -->
			<div class="col-md-12" style="text-align: center; vertical-align: center;">
				<center><a href="">Lihat Tanggapan lainnya</a></center>
			</div>
		</div>
		<?php endif ?>
		
		<hr>
		<div class="media">
		  <a class="pull-left" href="#">
		    <img class="img-circle profile-picture" src="<?=  SessionManagerWeb::getPhoto() ?>">
		  </a>
		  <div class="media-body">
		  	<h4 class="media-heading"><?= SessionManagerWeb::getName()  ?></h4>
		    <form method="post" action="<?= site_url('web/comment/create/detail')  ?>">
				<input type="hidden" name="post_id" value="<?= $data['id']  ?>">
				<input type="hidden" name="type" value="<?= Comment_model::KOMENTAR  ?>">
				<textarea class="form-control" style="height:50px;resize:none; margin-top: 10px;" name="text"></textarea>
				<br>
				<button class="pull-right btn btn-primary" type="submit"> Kirim</button>
			</form>
		  </div>
		</div>
		<hr style="margin-top: 0px;">
	</div>
</div>


<div class="modal fade" id="alasan-pindah" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <center><h4 class="modal-title" id="exampleModalLabel">Tuliskan alasan anda</h4></center>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <form method="post" action="<?= site_url('web/post/changeDinas')  ?>">
      <div class="modal-body">
      		<input type="hidden" name="post_id" id="post_id" value="">
      		<input type="hidden" name="dinas_id" id="dinas_id" value="">
      		<input type="hidden" name="petugas_id" id="petugas_id" value="">
        	<textarea name="reason" id="alasan-anda" class="form-control" style="height: 100px;"></textarea>
      </div>
      <div class="modal-footer">
      	<button type="submit" class="btn btn-primary">Simpan</button>
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
      </div>
      </form>
    </div>
  </div>
</div>


<script type="text/javascript" src="<?php echo base_url('assets/web/js/jquery.form.js') ?>"></script>
<script type="text/javascript">

function goBack() {
  window.history.back();
}

function addTemplateJawaban($id) {
	var val = $id;
	$.ajax(
		{
			url: "<?= site_url('web/post/ajaxreply') ?>/"+ val, 
			success: function(result){
        		$("#text_discuss").html(result);
        		$("#text_discuss").val(result);
    		}
    	}
    );		
}

$(".select-option-dinas").select2({
					width: '100%',
					placeholder: 'pilih instansi',
				});

$(".select-option-approve").select2({
					width: '50%',
					placeholder: 'terima?',
				});
$(".select-option-operator").select2({
					width: '100%',
					placeholder: 'pilih operator',
				});

$('#ganti-dinas').on('change', function() {
	if (this.value!=""){
		  var dinas = $('#dinas_'+this.value).html();
		  var cek = confirm('Apakah anda akan memindahkan post ini ke '+dinas+'?');
		  if(cek){
		  	$("#alasan-anda").attr("placeholder", "Contoh: saya memindahkan ke "+dinas+" karena......");
		  	$('#alasan-pindah').modal('show');
		  	$("#post_id").val(<?= $data['id']  ?>);
		  	$("#dinas_id").val(this.value);
		  	$("#petugas_id").val(<?= $data['petugas_id']  ?>);
		  }else{
		  	$(this).val('').trigger('change');
		  }
	}
  
});

$('.select-option-operator').on('change', function() {
  var operator = $('#operator_'+this.value).html();
  	if (this.value!=""){
	  var cek = confirm('Apakah anda akan memindahkan post ini ke '+operator+'?');
	  if(cek){
	  	var operator_id = this.value;
	  	var post_id = <?= $data['id'] ?>;
	  	$.ajax({
			url : '<?= site_url('web/post/ajaxChangeOperator') ?>',
			type: 'post',
			cache: false,
			data: {"post_id":post_id,"operator_id":operator_id},
			success: function(respon){
				if(respon != "ERROR"){
					if(respon == '0'){
						alert('Berhasil mengubah operator');
						location.reload(true);
					}else{
						alert('Gagal mengubah operator');
					}
				}else{
					alert('Anda tidak bisa mengakses fungsi ini');
				}
			}
		});
	  }else{
	  	$(this).val('').trigger('change');
	  }
  	}
});

$('.select-option-approve').on('change', function() {
  if (this.value == 'D') {
  	$('.alih-dinas').show();
  	$("#choose_dinas").prop('required',true);
  	$('.alih-alasan').show();
  }else if(this.value=='R'){
  	$('.alih-dinas').hide();
  	$("#choose_dinas").prop('required',false);
  	$('.alih-alasan').show();
  }else{
  	$('.alih-dinas').hide();
  	$("#choose_dinas").prop('required',false);
  	$('.alih-alasan').hide();
  }
});

$(".rotate").click(function () {
    $(this).toggleClass("down");
});

$(".collapse-a").click(function () {
    $(this).find('.rotate').toggleClass('down');
})
</script>

