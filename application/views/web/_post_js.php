<script type="text/javascript" src="<?php echo base_url('assets/web/js/jquery.form.js') ?>"></script>

<script>

$(document).ready(function(){
	$(document).on('mouseover', '.star-rating .fa', function(e) {
	    $(this).siblings('input.rating-value').val($(this).attr('data-rating'));
		return SetRatingStar();
	});

	$(document).on('mouseout', '.star-rating .fa', function(e) {
	    $(this).siblings('input.rating-value').val('');
		return SetRatingStar();
	});

	SetRatingStar();

	$(document).on('click', '.star-rating .fa', function() {
	  	$(this).siblings('input.rating-value').val($(this).data('rating'));
	    var id = $(this).siblings('input.rating-id').val();
	    var value = $(this).siblings('input.rating-value').val();
	    var th = $(this);
		$.ajax({
	        url: "rating",
	        type: "post",
	        data: {post_id : id, rating : value} ,
	        success: function (response) {
	        	console.log(response);
	        	th.parents('.rating').html('<div class="star-rating-post"><p class="total-rating"><b>'+response+'</b> &nbsp<span class="fa fa-star"></span></p></div>')
	        },
	        error: function(response) {
	        	console.log('asd');
	        }
	    });

	});
});

var SetRatingStar = function() {
	  return $('.star-rating .fa').each(function() {
	  	// alert($(this).siblings('input.rating-value').val());
	    if (parseInt($(this).siblings('input.rating-value').val()) >= parseInt($(this).data('rating'))) {
	      return $(this).removeClass('fa-star-o').addClass('fa-star');
	    } else {
	      return $(this).removeClass('fa-star').addClass('fa-star-o');
	    }
	  });
	};

function reloadDetail(id, respon) {
	$('#post-' + id).html(respon);
}

$(document).on('click','.set-status',set_this_status);
function set_this_status(){
	var id = $(this).attr('id-post');
	var val = $(this).attr('val');
	var post_status;
	$.ajax({
		url : '<?= site_url('web/post/getstatuspost') ?>/'+id,
		success: function(result){
			post_status = result;
			$.ajax({
				url : '<?= site_url('web/post/ajaxsetstatus') ?>',
				type: 'post',
				cache: false,
				data: {"id":id,"status":val},
				success: function(respon){
					if (respon != 'ERROR') {
						<?php if ($method == 'dashboard') { ?>location.href = location.href;<?php } ?>
						$('#sidebar-wrapper').load('post'+ ' .sidebar');
						reloadDetail(id, respon);
						if (post_status!=val) {
							ctl = '<?php echo $class ?>';
							method = '<?php echo $method ?>';
							if (ctl=='post' && method!='detail') {
								var countDown = 5;
								var x = setInterval(function() {
								    countDown--;
								    if (countDown < 0) {
								        clearInterval(x);
								        $('#post-' + id_post).parent('.post-section').remove();
								    }
								}, 1000);
							}
						}
					}
					else {
						alert('Terjadi kesalahan. Silakan coba lagi atau hubungi Administrator');
					}
				}
			});
		}
	});	
}

$(document).on('click','.set-salin',salin_this_post);

function salin_this_post(){
	var id = $(this).attr('id-post');
	var val = $(this).attr('val');
	var helpdesk = $(this).attr('helpdesk');
	var post;
	$.ajax({
		url : '<?= site_url('web/post/sessionsavesalin') ?>',
		type: 'post',
		cache: false,
		data: {"post_id":id,"helpdesk":val},
		success: function(respon){
			$('#post_salin_modal').modal('toggle');
		}
	});

	
	//if (confirm('Salin tiket ini ke Helpdesk '+helpdesk+'?')){
		// $.ajax({
		// 	url : '<?//= site_url('web/post/ajaxsalintiket') ?>',
		// 	type: 'post',
		// 	cache: false,
		// 	data: {"id":id,"helpdesk":val},
		// 	success: function(respon){
		// 		if (respon != 'ERROR') {
		// 			location.href = location.href;
		// 		}
		// 		else {
		// 			alert('Terjadi kesalahan. Silakan coba lagi atau hubungi Administrator');
		// 		}
		// 	}
		// });
	//}	
}

function salin_ok(){
	// var id = $(this).attr('id-post');
	// var val = $(this).attr('val');
	// var helpdesk = $(this).attr('helpdesk');
	// var post;
	var text = $('#text_salin').val();
	$.ajax({
		url : '<?= site_url('web/post/ajaxsalintiket') ?>',
		type: 'post',
		cache: false,
		data: {"text":text},
		success: function(respon){
			if (respon != 'ERROR') {
				location.href = location.href;
			}
			else {
				alert('Terjadi kesalahan. Silakan coba lagi atau hubungi Administrator');
			}
		}
	});
}

$(document).on('click','.set-pindah',pindah_this_post);

function pindah_this_post(){
	var id = $(this).attr('id-post');
	var val = $(this).attr('val');
	var helpdesk = $(this).attr('helpdesk');
	var post;
	//if (confirm('Pindahkan tiket ini ke Helpdesk '+helpdesk+'?')){
		$.ajax({
			url : '<?= site_url('web/post/sessionsavepindah') ?>',
			type: 'post',
			cache: false,
			data: {"post_id":id,"helpdesk":val},
			success: function(respon){
				$('#post_pindah_modal').modal('toggle');
			}
		});
		
		
		// $.ajax({
		// 	url : '<?//= site_url('web/post/ajaxpindahtiket') ?>',
		// 	type: 'post',
		// 	cache: false,
		// 	data: {"id":id,"helpdesk":val},
		// 	success: function(respon){
		// 		if (respon != 'ERROR') {
		// 			location.href = location.href;
		// 		}
		// 		else {
		// 			alert('Terjadi kesalahan. Silakan coba lagi atau hubungi Administrator');
		// 		}
		// 	}
		// });
	//}	
}

function pindah_ok(){
	// var id = $(this).attr('id-post');
	// var val = $(this).attr('val');
	// var helpdesk = $(this).attr('helpdesk');
	// var post;
	var text = $('#text_pindah').val();
	$.ajax({
		url : '<?= site_url('web/post/ajaxpindahtiket') ?>',
		type: 'post',
		cache: false,
		data: {"text":text},
		success: function(respon){
			if (respon != 'ERROR') {
				location.href = location.href;
			}
			else {
				alert('Terjadi kesalahan. Silakan coba lagi atau hubungi Administrator');
			}
		}
	});
}

$(document).on('click','.set-helpdesk',change_session_helpdesk);

function change_session_helpdesk(){
	var id = $(this).attr('id-post');
	var val = $(this).attr('value');
	var post;
	$.ajax({
		url : '<?= site_url('web/post/ajaxsethelpdesksession') ?>',
		type: 'post',
		cache: false,
		data: {"id":id,"helpdesk":val},
		success: function(respon){
			if (respon != 'ERROR') {
				location.href = location.href;
			}
			else {
				alert('Terjadi kesalahan. Silakan coba lagi atau hubungi Administrator');
			}
		}
	});
}

// $(document).on('click','.set-helpdesk-kanan-knowledge',change_session_knowledge);

// function change_session_knowledge(){
// 	var id = $(this).attr('id-post');
// 	var val = $(this).attr('value');
// 	var post;
// 	$.ajax({
// 		url : '<?= site_url('web/post/ajaxchangeknowledgesession') ?>',
// 		type: 'post',
// 		cache: false,
// 		data: {"id":id,"helpdesk":val},
// 		success: function(respon){
// 			if (respon != 'ERROR') {
// 				location.href = location.href;
// 			}
// 			else {
// 				alert('Terjadi kesalahan. Silakan coba lagi atau hubungi Administrator');
// 			}
// 		}
// 	});
// }

// $(document).on('click','.set-helpdesk-kanan-informasi',change_session_informasi);

// function change_session_informasi(){
// 	var id = $(this).attr('id-post');
// 	var val = $(this).attr('value');
// 	var post;
// 	$.ajax({
// 		url : '<?= site_url('web/post/ajaxchangeinformasisession') ?>',
// 		type: 'post',
// 		cache: false,
// 		data: {"id":id,"helpdesk":val},
// 		success: function(respon){
// 			if (respon != 'ERROR') {
// 				location.href = location.href;
// 			}
// 			else {
// 				alert('Terjadi kesalahan. Silakan coba lagi atau hubungi Administrator');
// 			}
// 		}
// 	});
// }

$(document).on('click', '.set-staff-dashboard', function(){
	<?php 
		unset($_SESSION['show_my_ticket']);
	?>
	var val = $(this).attr('value');
	var val_helpdesk = $('#btn_helpdesk').attr('value');
	if (val_helpdesk=="" || val_helpdesk=="Global" || val_helpdesk==null){
		window.location.replace("<?= site_url('web/post/dashboard') ?>/"+val);
	} else if (val=="" || val=="Semua") {
		window.location.replace("<?= site_url('web/post/dashboard') ?>/?h="+val_helpdesk);
	} else {
		window.location.replace("<?= site_url('web/post/dashboard') ?>/"+val+'?h='+val_helpdesk);
	}
	
}); 

$(document).on('click', '.set-staff-low-rate-dashboard', function(){
	<?php 
		unset($_SESSION['show_my_ticket']);
	?>
	var val = $(this).attr('value');
	var val_helpdesk = $('#btn_helpdesk').attr('value');
	if (val_helpdesk=="" || val_helpdesk=="Global" || val_helpdesk==null){
		window.location.replace("<?= site_url('web/post/lowRatingDashboard') ?>/"+val);
	} else if (val=="" || val=="Semua") {
		window.location.replace("<?= site_url('web/post/lowRatingDashboard') ?>/?h="+val_helpdesk);
	} else {
		window.location.replace("<?= site_url('web/post/lowRatingDashboard') ?>/"+val+'?h='+val_helpdesk);
	}
	
}); 

$(document).on('click', '.set-helpdesk-dashboard', function(){
	var val = $(this).attr('value');
	var val_staff = "<?php echo $this->uri->segment(4); ?>";
	if (val_staff=="" || val_staff=="Semua" || val_staff==null){
		window.location.replace("<?= site_url('web/post/dashboard') ?>/?h="+val);
	} else if (val=="" || val=="Semua") {
		window.location.replace("<?= site_url('web/post/dashboard') ?>/"+val_staff);
	}else {
		window.location.replace("<?= site_url('web/post/dashboard') ?>/"+val_staff+'/?h='+val);
	}
}); 

var id_post;
$(document).on('click', '.tugaskan', function() {
	id_post = $(this).attr('id-post');
	var user_id;
	user_id = '<?php echo SessionManagerWeb::getUserID() ?>';
	var is_administrator = '<?php echo SessionManagerWeb::isAdministrator() ?>';
	$.ajax({
		url : '<?= site_url('web/post/getassigns') ?>/'+id_post,
		success: function(result){
			var user_assign = result.split(" ");
			$("input[name='staff_ditugaskan']").each(function(index) {
				for (var i = 0; i < user_assign.length-1; i++) {
					if ($(this).attr('value')==user_assign[i]){

						$(this).prop('checked',true);
						if (user_assign[i]==user_id && is_administrator!='1'){
							$(this).attr('disabled','disabled');
						}
						
						$(this).parent('.icheckbox_minimal').addClass('checked');
						$(this).parent('.icheckbox_minimal').prop('aria-checked',true);
						
					}
	        	}
			});
		}
	});	
}); 

function bataltugaskan() {
	$("input[name='staff_ditugaskan']").each(function(index) {
		if ($(this).is(':checked')) {
			$(this).removeProp('checked');
			$('.icheckbox_minimal').removeClass('checked');
			$('.icheckbox_minimal').prop('aria-checked',false);
		}
	});	
	showStaff();
}

function tugaskan() {
	str = '';
	var status_post;
	$("input[name='staff_ditugaskan']").each(function(index) {
		if ($(this).is(':checked')) {
			str += $(this).val() + ',';
			$(this).removeProp("checked");
			$('.icheckbox_minimal').removeClass('checked');
			$('.icheckbox_minimal').prop('aria-checked',false);
		}
	});	
	
	$.ajax({
		url : '<?= site_url('web/post/getstatuspost') ?>/'+id_post,
		success: function(result){
			status_post = result;
			$.ajax({
				url : '<?= site_url('web/post/ajaxassignticket') ?>',
				type: 'post',
				cache: false,
				data: {"id":id_post,"list_staff":str},
				success: function(respon){
					if (respon != 'ERROR') {
						<?php if ($method == 'dashboard') { ?>location.href = location.href;<?php } ?>
						reloadDetail(id_post, respon);
						ctl = '<?php echo $class ?>';
						method = '<?php echo $method?>';
						if (status_post!='<?php echo Ticket::PROSES ?>' && ctl!='post' && method!='detail') {
							var countDown = 5;
							var x = setInterval(function() {
							    countDown--;
							    if (countDown < 0) {
							        clearInterval(x);
							        $('#post-' + id_post).parent('.post-section').remove();
							    }
							}, 1000);
						}
					}
					else {
						alert('Terjadi kesalahan. Silakan coba lagi atau hubungi Administrator');
					}
					bataltugaskan();
					$('#sidebar-wrapper').load('post'+ ' .sidebar');
				}
			});
		}
	});	
	
	
}

function stickPost(id, is_sticky) {
    if (is_sticky == '1') {
        stick = confirm('Hilangkan pin post ini ?');
    } else {
        stick = confirm('Jadikan post ini sebagai pin post ?');
    }
    if (stick) {
        $.ajax({
            url: "<?= site_url('web/post/stick') ?>/" + id,
            type: 'POST',
            data: {
                'sticky': is_sticky
            },
            success: function() {
                window.location.href = "<?= site_url('web/post/stick') ?>?id=" + id + "&stick=" + is_sticky;
            }
        });
    }
}

function hapusPost(id) {
	if (confirm('Anda yakin akan menghapus post ini?')) {
		location.href = '<?= site_url('web/'.$class.'/hapus') ?>/'+id;
	}
}

function checkGroupTujuan(){
	if (checkEmptyPost()){
		alert("Pesan tidak boleh kosong!");
	} else if ($('#group_tujuan').attr('value')==""){
        alert("Pilih tujuan pesan terlebih dahulu!");
    } else if ($('#group_tujuan').attr('value')=="Helpdesk") {
    	if ($('#helpdesk_tujuan').attr('value')==""){
    		alert("Pilih helpdesk tujuan terlebih dahulu!");
    	} else {
    		$('#post_kirim_modal').modal('toggle');
    	}
    	
    } else if ($('#group_tujuan').attr('value')=="Group") {
    	checkEmptyJudul();
    	if ($('#group_khusus_tujuan').attr('value')==""){
    		alert("Pilih group tujuan terlebih dahulu!");
    	} else {
	    	if (confirm('Anda akan mengirim pesan ke '+$('#group_khusus_tujuan').attr('value')+' ?')) {
	    		if ($('#sticky').is(':checked')) {
		            $('#stick_kirim').val('1');
		        }
		        $('#btn_kirim_pesan').removeClass('disabled');
		        post_ok();
	    	}
	    }
    } 
}

function checkEmptyPost(){
    var text;
    text = $('#text_kirim').val();
    text = text.replace(/\s+/g, '');
    if (text==""){
        $('#text_kirim').val('');
        return true;
    } else{
        return false;
    }
}

function checkEmptyJudul(){
    var judul;
    judul = $('#judul_kirim').val();
    judul = judul.replace(/\s+/g, '');
    if (judul==""){
        $('#judul_kirim').val('');
        return true;
    } else{
        return false;
    }
}

function post_ok(){
	$('#kirim_status').val('1');
	kirim_pesan();
}

function post_batal(){
	$('#kirim_status').val('0');
}

function set_status_ok(){
	$('#set_status_status').val('1');
	kirim_pesan();
}

function set_status_batal(){
	$('#set_status_status').val('0');
}


function showStaff(){
	$('#btn_tugaskan_staff').addClass('btn-primary');
	$('#modal_staff').removeClass('hidden');
	$('#btn_tugaskan_staff_kopertis').removeClass('btn-primary');
	$('#modal_staff_kopertis').addClass('hidden');
}

function showStaffKopertis(){
	$('#btn_tugaskan_staff_kopertis').addClass('btn-primary');
	$('#modal_staff_kopertis').removeClass('hidden');
	$('#btn_tugaskan_staff').removeClass('btn-primary');
	$('#modal_staff').addClass('hidden');
}

$(document).on('click', '.set-rating', function(){
	if (!confirm('Anda akan melakukan rating tiket ini'))
		return;
	
	var id = $(this).attr('id-post');
	var val = $(this).attr('val');
	var post_status;
	$.ajax({
		success: function(result){
			post_status = result;
			$.ajax({
				url : '<?= site_url('web/post/ajaxsetrating') ?>',
				type: 'post',
				cache: false,
				data: {"id":id,"rating":val},
				success: function(respon){
					if (respon != 'ERROR') {
						alert('Terima kasih sudah melakukan rating');
						<?php if ($method == 'dashboard') { ?>location.href = location.href;<?php } ?>
						$('#sidebar-wrapper').load('post'+ ' .sidebar');
						reloadDetail(id, respon);
					}
					else {
						alert('Terjadi kesalahan. Silakan coba lagi atau hubungi Administrator');
					}
				}
			});
		}
	});		
}); 
</script>

<div class="modal fade" id="post_tugas_modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
	<div class="modal-dialog" style="max-width: 400px">
		<div class="modal-content">
			<div class="modal-header">
				<h4 class="modal-title" id="myModalLabel">Ditugaskan kepada : </h4>
				<span class="btn btn-primary" id='btn_tugaskan_staff' onclick="javascript:showStaff()">Staff</span>
				<span class="btn " id='btn_tugaskan_staff_kopertis' onclick="javascript:showStaffKopertis()">Staff Group</span>
			</div>
			<div class="modal-body" style="max-height:200px;overflow:scroll;padding-left:30px" id="modal_staff">
				<?php foreach ($list_staff as $staff) { ?>
					<div class="row">
						<label>
							<?php if (SessionManagerWeb::isSuperAdministrator()) {
								$from_helpdesk = ' ( Helpdesk - '. $staff['helpdesk_name'].' )';
							} ?>
							<input type="checkbox" name="staff_ditugaskan" id="staff_ditugaskan" value="<?= $staff['id'] ?>"
							> <img class="img-circle" style="width:30px" src="<?= site_url($staff['user_photo']) ?>" > <?= $staff['name'].$from_helpdesk ?>
						</label>
					</div>
				<?php } ?>
			</div>
			<div class="modal-body hidden" style="max-height:200px;overflow:scroll;padding-left:30px" id="modal_staff_kopertis">
				<?php foreach ($list_staff_kopertis as $staff) { ?>
					<div class="row">
						<label>
							<input type="checkbox" name="staff_ditugaskan" id="staff_ditugaskan" value="<?= $staff['id'] ?>"
							> <img class="img-circle" style="width:30px" src="<?= site_url($staff['user_photo']) ?>" > <?= $staff['name'].' (Staff Group - '. $staff['group_name'] .')' ?>
						</label>
					</div>
				<?php } ?>
				
			</div>
			<h6 style="margin-left: 10px"><i>*staff tidak dapat menghilangkan penugasan tiketnya, namun bisa mengajak orang lain untuk menyelesaikan tiketnya</i></h6>
			<div class="modal-footer">

				<div class="text-center">
					<span class="btn btn-sm btn-primary" onclick="tugaskan()" data-dismiss="modal"><?= xSaveIcon() ?> Simpan</span>
					<span class="btn btn-sm btn-default" onclick="bataltugaskan()" data-dismiss="modal"><?= xBatalIcon() ?> Batal</span>
				</div>
			</div>
		</div>
	</div>
</div>

<div class="modal fade" id="post_kirim_modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
	<div class="modal-dialog" style="max-width: 400px;top:15%;">
		<div class="modal-content" style="background-color:#ffffce">
			<div class="modal-header">
				<center><h4 class="modal-title" id="myModalLabel"><strong style="color:red">Perhatian !</strong></h4></center>
			</div>
			<div class="modal-body" style="max-height:200px;padding-left:30px">
					<div class="row">
						<label style="text-align: center">
							Pastikan anda sudah melihat fitur Knowledge Base sebelum membuat tiket. 
							Tiket yang dibuat akan didistribusikan kepada tim helpdesk pusat dan akan dipantau penyelesaiannya.
						</label>
					</div>
			</div>
			<div class="modal-footer">
				<div class="text-center">
					<span class="btn btn-sm btn-primary" onclick="post_ok()" data-dismiss="modal">Lanjutkan</span>
					<span class="btn btn-sm btn-default" onclick="post_batal()" data-dismiss="modal"> Batal</span>
				</div>
			</div>
		</div>
	</div>
</div>

<div class="modal fade" id="post_salin_modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
	<div class="modal-dialog" style="max-width: 400px;top:15%;">
		<div class="modal-content" style="background-color:#ffffbf">
			<div class="modal-header">
				<center><h4 class="modal-title" id="myModalLabel"><strong style="color:red">Salin Tiket</strong></h4></center>
			</div>
			<div class="modal-body" style="max-height:200px;">
				<div class="row">
					<textarea class="form-control" id="text_salin" name="text_salin" rows="5" style="border:0;height: 100%" autofocus placeholder="ketik alasan Anda menyalin tiket di sini"></textarea>
				</div>
			</div>
			<div class="modal-footer">
				<div class="text-center">
					<span class="btn btn-sm btn-primary" onclick="salin_ok()" data-dismiss="modal">Salin</span>
					<span class="btn btn-sm btn-default" data-dismiss="modal"> Batal</span>
				</div>
			</div>
		</div>
	</div>
</div>

<div class="modal fade" id="post_pindah_modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
	<div class="modal-dialog" style="max-width: 400px;top:15%;">
		<div class="modal-content" style="background-color:#ffffbf">
			<div class="modal-header">
				<center><h4 class="modal-title" id="myModalLabel"><strong style="color:red">Pindah Tiket</strong></h4></center>
			</div>
			<div class="modal-body" style="max-height:200px;">
					<div class="row">
						<textarea class="form-control" id="text_pindah" name="text_pindah" rows="5" style="border:0;height: 100%" autofocus placeholder="ketik alasan Anda memindahkan tiket di sini"></textarea>
					</div>
			</div>
			<div class="modal-footer">
				<div class="text-center">
					<span class="btn btn-sm btn-primary" onclick="pindah_ok()" data-dismiss="modal">Lanjutkan</span>
					<span class="btn btn-sm btn-default" data-dismiss="modal"> Batal</span>
				</div>
			</div>
		</div>
	</div>
</div>

<div class="modal fade" id="login_modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
	<div class="modal-dialog" style="max-width: 400px;top:15%;">
		<div class="modal-content" style="background-color:#ffffce">
			<div class="modal-header">
				<center><h4 class="modal-title" id="myModalLabel"><strong style="color:red">Selamat Datang di SIGAP</strong></h4></center>
			</div>
			<div class="modal-body" style="max-height:200px;padding-left:30px">
					<div class="row">
						<label style="text-align: center">
							Fakta : <br>
							"Sebagian besar pertanyaan sudah dibahas dan dijawab di knowledgebase"<br><br>

							Guna membuat layanan ini lebih efisien, mohon pastikan anda melakukan pencarian dengan kata kunci permasalahan anda terlebih dahulu sebelum membuat tiket kepada Helpdesk.
						</label>
					</div>
			</div>
			<div class="modal-footer">
				<div class="text-center">
					<span class="btn btn-sm btn-primary" data-dismiss="modal">Lanjutkan</span>
				</div>
			</div>
		</div>
	</div>
</div>

<div class="modal fade" id="set_status_modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
	<div class="modal-dialog" style="max-width: 400px;top:15%;">
		<div class="modal-content" style="background-color:#ffffce">
			<div class="modal-header">
				<center><h4 class="modal-title" id="myModalLabel"><strong style="color:red">Perhatian !</strong></h4></center>
			</div>
			<div class="modal-body" style="max-height:200px;padding-left:30px">
					<div class="row">
						<label style="text-align: center">
							Klik lanjutkan.
						</label>
					</div>
			</div>
			<div class="modal-footer">
				<div class="text-center">
					<span class="btn btn-sm btn-primary" onclick="set_status_ok()" data-dismiss="modal">Lanjutkan</span>
					<span class="btn btn-sm btn-default" onclick="set_status_batal()" data-dismiss="modal"> Batal</span>
				</div>
			</div>
		</div>
	</div>
</div>

