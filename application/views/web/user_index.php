<div class="alert alert-info">Klik foto untuk mengirimkan post kepada teman</div>
<?php
	$n = ceil(count($data)/2);
	for($i=0;$i<$n;$i++) {
?>
<div class="row">
	<?php
		for($j=0;$j<2;$j++) {
			$v = $data[($i*2)+$j];
			if(empty($v))
				continue;
	?>
	<div class="col-sm-6">
		<div class="post-section user-section">
			<div class="site-photo site-photo-100 site-photo-left" data-id="<?php echo $v['id'] ?>">
				<img src="<?php echo isset($v['photo']) ? site_url($path.'thumb/watermark/'.$v['id'].'/100') : $nopic ?>" />
			</div>
			<div class="user-label"><strong><?php echo $v['name'] ?></strong></div>
			<div class="site-label user-label user-email"><?php echo $v['email'] ?></div>
			<div class="site-label user-label user-ym"><?php echo $v['ym'] ?></div>
			<div class="site-label user-label user-phone"><?php echo $v['phone'] ?></div>
			<div class="user-label">
				<?php if(empty($presence[$v['id']])) { ?>
				<label class="label label-danger">Tidak Masuk</label>
				<?php } else { ?>
				<label class="label label-success">Masuk</label>
				<?php } ?>
			</div>
		</div>
	</div>
	<?php } ?>
</div>
<?php } ?>

<script type="text/javascript">

var back = "<?php echo base_url(uri_string()) ?>";
sessionStorage.setItem("post.back",back);

$(function() {
	$("[data-id]").css("cursor","pointer").click(function() {
		location.href = "<?php echo site_url($path.'post/add') ?>/" + $(this).attr("data-id");
	});
})

</script>