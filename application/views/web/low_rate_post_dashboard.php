
<?php
if($group_kopertis){
    echo form_open(null, array('id' => 'form_check', 'method' => 'get'));
    echo form_label(form_checkbox('showall', 1, $_SESSION['showall']) . 'LIHAT SEMUA TIKET '.strtoupper($group_kopertis['name']), null, array('class' => 'label-check'));
    echo form_hidden('checkshowall', 1);
    echo '</form>';
}
?>
<?php 
if (SessionManagerWeb::isStaff()) { ?>
<div class="row">

    <div class="col-sm-12 col-md-12">
    <label for="name" class="col-md-2">Tampilkan tugas dari : </label>
            <?php echo form_dropdown('select_tugas_dari', $list_staff_helpdesk, $_GET['id_user'], ' id="select_tugas_dari" class="form-control input-sm select2 tugas_dari" style="width:30%;" onChange="javascript:changeTugasDari()"') ?>

            <div class="btn-group hidden">
                <!-- <input type="button" class="btn btn-default btn-xs" id="btn_staff" name="btn_staff" onload="javascript:getnameuser('<?php echo $this->uri->segment(4) ?>')"> -->
                <?php
                    if ($this->uri->segment(4)!=NULL) {?>
                        <input type="button" class="btn btn-default btn-xs" id="btn_staff" name="btn_staff" value="<?php echo $this_user['name']?>">
                   <?php } else { ?>
                        <input type="button" class="btn btn-default btn-xs" id="btn_staff" name="btn_staff" value="Semua">
                    <?php }
                ?>
                
                <button type="button" class="btn btn-default btn-xs dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                <span class="caret"></span>
                            </button>
                <ul class="dropdown-menu">
                    <li>
                        <a style="text-align:left" class="set-staff-low-rate-dashboard" value="">
                            Semua
                        </a>
                    </li>
                    <?php foreach ($list_staff_helpdesk as $k => $v) { ?>
                    <li>
                        <a style="text-align:left" class="set-staff-low-rate-dashboard" value="<?= $v['id'] ?>">
                            <?= $v['name'] ?>
                        </a>
                    </li>
                    <?php } ?>
                </ul>
            </div>
        <!-- <input type="text" id="tampil_tugas" name="tampil_tugas" style="width:30%" value="all">
        <span class="btn btn-xs btn-default tugaskan" id-post="<?= $_data['post_id'] ?>" data-toggle="modal" data-target="#post_tugas_modal" ><i class="fa fa-user"></i> Tugaskan</span> -->
    </div>
</div>
<?php } ?>
<?php if (SessionManagerWeb::isStaff()) { ?>
<div class="row">

    <div class="col-sm-12 col-md-12">
        <label for="name" class="col-md-2">Tampilkan tiket saya : </label>
        <div class="btn-group">
            <span onClick="javascript:changeShowMyTicket()">
                <?php if($_GET['show_my_ticket']){ ?> 
                    <i class="glyphicon glyphicon-check" style="color:green"></i>
                <?php } else { ?>
                    <i class="glyphicon glyphicon-unchecked" style="color:red"></i>
                <?php } ?>
            </span>
        </div>
    </div>
</div>
<hr>
<?php } ?>

<div class="row" style="margin-bottom: 10px">
    <div class="col-sm-12 col-md-12">
        <label for="name" class="col-md-2">Urutkan Berdasarkan  </label>
        <div class="btn-group">
            <?php 
                $arr_sort_by = array(
                    0 => "Terlama",
                    1 => "Terbaru",
                    2 => "Terakhir Diproses"
                );
                echo form_dropdown('sort_by', $arr_sort_by,$_SESSION['sort_by'], 'id="sort_by" disabled class="form-control input-sm"'); 
            ?>
        </div>
    </div>
</div>
<?php if (1==2) { ?>
<div class="row">
    <div class="col-sm-12 col-md-12">
        <div class="panel panel-primary">
            <a data-toggle="collapse" href="#kolapsFilter" aria-expanded="false" aria-controls="collapseExample">		
                <div class="panel-heading">FILTER
                    <span class="glyphicon glyphicon-chevron-down pull-right"></span>
                </div>
            </a>
            <div class="collapse" id="kolapsFilter">
                <div class="panel-body">
                    <?php echo form_open('web/issue/dashboard', array('method' => '')) ?>

                    <div class="col-md-4">
                        <div class="row bord-bottom">
                            <label class="col-xs-12">Project</label>
                            <div class="col-xs-12">
                                <?php echo form_dropdown('project', $a_project, $_GET['project'], 'id="parent-id" class="form-control input-sm"') ?>
                            </div>
                        </div>
                        <div class="row bord-bottom">
                            <label class="col-xs-12">Jenis</label>
                            <div class="col-xs-12">
                                <?php echo form_dropdown('jenis', $a_type, $_GET['jenis'], 'id="parent-id" class="form-control input-sm"') ?>
                            </div>
                        </div>
                    </div>

                    <div class="col-md-4">
                        <div class="row bord-bottom">
                            <label for="date" class="col-xs-12">Tanggal Awal</label>
                            <div class="col-xs-12">
                                <div class="input-group input30">
                                    <div class="input-group-addon"><i class="fa fa-calendar"></i></div>
                                    <?php echo form_input(array('name' => 'start_date', 'value' => (!empty($_GET['start_date'])) ? $_GET['start_date'] : NULL, 'class' => 'form-control input-sm', 'placeholder' => 'dd-mm-yyyy')) ?>
                                </div>
                            </div>
                        </div>
                        <div class="row bord-bottom">
                            <label for="date" class="col-xs-12">Tanggal Akhir</label>
                            <div class="col-xs-12">
                                <div class="input-group input30">
                                    <div class="input-group-addon"><i class="fa fa-calendar"></i></div>
                                    <?php echo form_input(array('name' => 'end_date', 'value' => (!empty($_GET['end_date'])) ? $_GET['end_date'] : NULL, 'class' => 'form-control input-sm', 'placeholder' => 'dd-mm-yyyy')) ?>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="col-md-4">
                        <div class="row bord-bottom">
                            <label class="col-xs-12">Priority</label>
                            <div class="col-xs-12">
                                <?php echo form_dropdown('priority', $a_priority, $_GET['priority'], 'id="parent-id" class="form-control input-sm"') ?>
                            </div>
                        </div>                
                        <div class="row bord-bottom">
                            <label for="date" class="col-xs-12">Tampilkan Tugas</label>
                            <div class="col-xs-8">
                                <div class="radio" style="margin-top:0; margin-bottom:2px;">
                                    <?php echo form_radio('assignment_task', 'T', (empty($_GET['assignment_task']) || $_GET['assignment_task'] == 'T' ? TRUE : FALSE)); ?> Team
                                    &nbsp;
                                    <?php echo form_radio('assignment_task', 'M', ($_GET['assignment_task'] == 'M' ? TRUE : FALSE)); ?> Hanya Saya
                                </div>
                            </div>
                            <div class="col-xs-4">
                                <button class="btn btn-success btn-sm btn-block"><i class="fa fa-search"></i> Filter</button>
                            </div>
                        </div>
                    </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
<?php } ?>
<div class="row">
    <?php
    $panel = array(
        6 => 'default',
        1 => 'info',
        3 => 'warning',
        'B'=>'danger',
        'P'=>'warning',
        4 => 'success',
        'S'=>'success',
//        5 => 'danger',
    );
        $value = $data;
        $icon = '<span class="fa fa-star"></span>';
        $bg_color = '#cc3300';
        $show_rating=TRUE;
        $id = 'id="div_close"';
        ?>
        <div class="col-sm-6 col-md-12"" <?= $id ?>>
            <div class="panel panel-warning">
                <div class="panel-heading text-center" style="background-color:<?= $bg_color ?>;color:white"><?= $icon ?>TIKET RATING RENDAH</div>
                <div class="panel-body">
                    <b><?= $jml_post ?> Pesan</b>
                </div>
                <ul class="list-group" style="max-height: 600px; overflow-y: auto; overflow-x: hidden;">
                    <?php foreach ($value as $_data) { 
                        // echo '<pre>';
                        // var_dump($_data);
                        // echo '</pre>';
                        // die();
                        // if($_data['post_user_id']==)
                    ?>
                        <li class="list-group-item post-cat post-cat-<?php echo $post['categoryId'] ?>" style="cursor: pointer;padding: 10px 5px" >
                        <div class="post-dashboard" id-post="<?= $_data['post_id'] ?>" data-toggle="modal" data-target="#dashboard_modal">
                        <div class="post-title">
                            <table width="100%">
                                <tr>
                                    <td style="width: 15%;vertical-align: top">
                                        <div class="site-photo">
                                        <img class="img-circle" style="width:40px" src="<?= site_url($_data['user_photo']) ?>">
                                    </div>                      
                                    </td>
                                    <td style="width: 80%;vertical-align: top;font-size:smaller">
                                        <strong><?php echo $_data['user_name'] ?></strong>
                                        <?php if ($_data['nm_lemb']) echo '<br>'.$_data['nm_lemb']; ?>
                                        <div class="post-header-info" style="font-size:smaller">
                                            <?php echo FormatterWeb::dateToDiff($_data['post_created_at']) ?>
                                            <?php /* if($_data['createdAt'] != $_data['updatedAt']) { ?>
                                            <span class="post-right">Diedit <?php echo FormatterWeb::dateToDiff($_data['updatedAt']) ?></span>
                                            <?php } */ ?>
                                        </div>
                                        
                                    </td>
                                    <td style="vertical-align: top;text-align: right">
                                        <small class="pull-right text-center" style="margin:1px">
                                     <?php if ($_data['kode_ticket']) {  ?>
                                        <a href="<?= site_url($path . 'post/detail/' . $_data['post_id']) ?>"> <?= $_data['group_initial'] ? strtoupper($_data['group_initial']).'_'.$_data['kode_ticket'] : strtoupper(substr($_data['group_name'],0,3)).'_'.$_data['kode_ticket'] ?></a><br>
                                        <div style="clear:both;height:5px">&nbsp;</div>
                                     <?php  } ?>
                                        </small>
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="3" colspan="post-body"><p><?php echo nl2br(word_limiter($_data['post_description'],10)) ?></p></td>
                                </tr>
                                <tr>
                                    <td colspan="3">
                                        <hr style="margin-bottom:5px;" />
                                        <div class="pull-left">
                                            <?php if ($show_rating and $_data['rating']){
                                                for ($i=1;$i<=5;$i++) { 
                                                    if ($i<=$_data['rating']) {
                                            ?>
                                                    <i class="glyphicon glyphicon-star" style="color:#FFD700;text-shadow: 2px 2px 2px white"></i> 
                                            <?php   } else { ?>
                                                
                                                    <i class="glyphicon glyphicon-star" style="color:#C0C0C0"></i>
                                            <?php   }
                                                }
                                            } ?>
                                        </div>
                                        <?php if ($_data['assigns']) { ?>
                                            
                                            <div class="pull-right">
                                                <small>Ditugaskan ke</small>&nbsp;
                                                <?php foreach ($_data['assigns'] as $staff) { 
                                                    if(!$is_admin_group && SessionManagerWeb::isOperator() && $staff['groups']) {
                                                ?>
                                                <img class="img-circle" style="width:20px" title="<?= $staff['groups']['name'] ?>" src="<?= site_url($staff['groups']['group_photo']) ?>"> <small title="<?= $staff['groups']['name'] ?>"><b><?= 'Staff '.xShortenNama($staff['groups']['name']) ?></b></small>&nbsp;
                                                <?php
                                                    }else{
                                                ?>
                                                <img class="img-circle" style="width:20px" title="<?= $staff['name'] ?>" src="<?= site_url($staff['user_photo']) ?>"> <small title="<?= $staff['name'] ?>"><b><?= xShortenNama($staff['name']) ?></b></small>&nbsp;
                                                <?php
                                                    }
                                                } ?>
                                            </div>
                                        <?php } ?>
                                    </td>
                                </tr>
                            </table>
                        </div>

                        </div>
                        </li>
                    <?php } ?>
                    <?php 
                    // if ($created_by_me==1) {
                    //     $filter_create_by_me = "&created_by_me=1";
                    // }
                    if (isset($_SESSION['show_my_ticket'])){
                        $filter_my_ticket = "&show_my_ticket=1";
                    }
                    if ($jml_post[$key]>10 and $status_selected!=$key) {  ?>
                        <div class="panel-footer">
                            <!-- <a href="<?//= site_url('web/post?s='.$key) ?>" class="btn btn-primary form-control">Lihat Semuanya</a> -->

                            <a href="<?= current_url().'/?status='.$key.$filter_my_ticket ?>" class="btn btn-primary form-control">Lihat Semuanya</a>
                        </div>
                    <?php } ?>
                </ul>
            </div>
        </div>
</div>

<div class="modal fade" id="dashboard_modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog" style="max-width: 600px">
        <div class="modal-content">
            <div class="modal-body" id="dashboard_content" style="min-height:100px"></div>
            <div class="modal-footer">
                <div class="text-center">
                    <span class="btn btn-sm btn-default" data-dismiss="modal">Tutup</span>
                </div>
            </div>
        </div>
    </div>
</div>
<?php 
    echo form_open(null, array('id' => 'form_filter', 'method' => 'get'));
    echo form_hidden('id_user', $_GET['id_user']);
    echo form_hidden('show_my_ticket', $_GET['show_my_ticket']);
    echo form_hidden('start_date', $_GET['start_date']);
    echo form_hidden('end_date', $_GET['end_date']);
    echo '</form>';
?>
<script>

    $(document).ready(function () {
        $("#select_tugas_dari").select2({
            placeholder: 'Pilih Staff'
        });
    });

    var id_post;
    $(document).on('click', '.post-dashboard', function() {
        id_post = $(this).attr('id-post');
    }); 

    function doAlert() {
        alert('test');
      if (checkboxElem.checked) {
        alert ("hi");
      } else {
        alert ("bye");
      }
    }

    // function getnameuser(user_id){
    //     $.ajax({
    //         url : '<?= site_url('web/post/getusername') ?>/'+user_id,
    //         success: function(result){
    //             $("#btn_staff").val(result);
    //         }
    //     }); 
    // }

    $('#dashboard_modal').on('shown.bs.modal', function (e) {
        $('#dashboard_content').html('Sedang dimuat..');
        $.ajax({
            url : '<?= site_url('web/post/ajaxgetpost') ?>',
            type: 'post',
            cache: false,
            data: {"id":id_post},
            success: function(respon){
                if (respon != 'ERROR') {
                    $('#dashboard_content').html(respon);
                    $(".image-post").colorbox();
                }
                else {
                    alert('Terjadi kesalahan. Silakan coba lagi atau hubungi Administrator');
                }

            }
        }); 

    });  

    $(window).load(function(){
        $("#sort_by").removeAttr("disabled");
    });

    $("#sort_by").on('change', function (){
        var sort_by = document.getElementById("sort_by").value;
        $.ajax({
            url : '<?= site_url('web/post/ajaxSetSort') ?>',
            type: 'post',
            cache: false,
            data: {"sort_by":sort_by},
            success: function(respon){
                window.location.reload();
                // window.location.href = window.location.href;
            }
        }); 
    });

    $("[name='showall']").on('ifChanged', function (event) {
        $("#form_check").submit();
    });

    function changeShowMyTicket(){
        if($("[name='show_my_ticket']").val() == 1)
            $("[name='show_my_ticket']").val(null);
        else
            $("[name='show_my_ticket']").val(1);

        $("#form_filter").submit();
    }

    function changeTugasDari(){
        <?php 
            unset($_SESSION['show_my_ticket']);
        ?>
        var val = $("#select_tugas_dari").val();
        $("[name='id_user']").val(val);

        $("#form_filter").submit();

        // if (val_helpdesk=="" || val_helpdesk=="Global" || val_helpdesk==null){
        //     window.location.replace("<?= site_url($after_site_url) ?>&"+val);
        // } else if (val=="" || val=="Semua") {
        //     window.location.replace("<?= site_url($after_site_url) ?>&?h="+val_helpdesk);
        // } else {
        //     window.location.replace("<?= site_url($after_site_url) ?>&"+val+'?h='+val_helpdesk);
        // }
    }
</script>

<?php include(__DIR__.'/_post_js.php'); ?>