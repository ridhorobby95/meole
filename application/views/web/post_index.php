
<?php if ($belum_rating): ?>
    <div class="alert k-alert-red" id="belum-rating" style="cursor:pointer">
        <strong>Ada post yang belum anda rating. Klik disini untuk memberikan rating ! <i class="fa fa-info-circle pull-right k-fa-info-m"></i>
        </strong>
    </div>
<?php endif ?>

<div class="row" style="margin-bottom: 10px; margin-top: 10px;">

    <?php if (!$nofilter): ?>
        <div class="col-md-12">
            <button class="btn btn-basic btn-filter" data-toggle="modal" data-target="#filterModal"><i class="fa fa-filter"></i> Filter</button>
        </div>
    <?php endif ?>
    
</div>
<!-- Modal -->
        <div class="modal fade" id="filterModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
          <div class="modal-dialog" role="document">
            <div class="modal-content">
              <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">&times;</span>
                </button>
                <center><h2 class="modal-title" id="exampleModalLabel">Filter</h2></center>
                
              </div>
              <form method="POST" action="<?= site_url('web/post/search')  ?>">
                <input type="hidden" name="page" value="<?= $this->uri->segment(4) ?>">
              <div class="modal-body">

                <div class="row">
                    <div class="col-sm-12 col-md-12">
                        <label for="select_tugas_dari" class="col-md-3">Kata kunci : </label>

                        <input type="text" name="keyword" class="col-md-5" value="<?= $_SESSION['meole_search']['keyword']  ?>" style="width:60%; margin-right: 0px;" placeholder="Masukkan kata kunci">
                        <button type="button" class="input-group-addon no-bg-addon" style="width: 36px; height: 26px; cursor: pointer; background-color: white;" onclick="remove_filter('keyword')">
                            <i class="fa fa-close" aria-hidden="true" style="color: #FF5722;">
                            </i>
                        </button>
                        <!-- <button class="btn btn-danger col-md-1" style="height: 27px;"></button> -->
                    </div>
                    <br>
                    <br>
                    <?php if (SessionManagerWeb::isAdministrator()): ?>
                    <div class="col-sm-12 col-md-12">
                        <label for="select_tugas_dari" class="col-md-3">Dinas : </label>
                        <select class="form-control pull-right filter-dinas" name="dinas[]" multiple="multiple" style="width:60%;">
                                <option value=""></option>
                            <?php foreach ($dinas as $din): ?>
                                    <?php if ($din['idunit'] != $data['dinas_id']): ?>
                                    <option value="<?= $din['idunit'] ?>" id="dinas_<?= $din['idunit'] ?>" <?= in_array($din['idunit'], $_SESSION['meole_search']['dinas']) ? "selected" : ""  ?>><?= $din['name'] ?></option>
                                    <?php endif ?>
                            <?php endforeach ?>
                        </select>
                        <button type="button" class="input-group-addon no-bg-addon" style="width: 36px; height: 40px; cursor: pointer;background-color: white;" onclick="remove_filter('dinas')">
                            <i class="fa fa-close" aria-hidden="true" style="color: #FF5722;">
                            </i>
                        </button>
                    </div>
                    <br>
                    <br>
                    <br>
                    <?php endif ?>
                    
                    <div class="col-sm-12 col-md-12">
                        <label for="select_tugas_dari" class="col-md-3">Urutkan : </label>
                        <select class="form-control pull-right filter-order" name="order" style="width:60%;">
                                <option value=""></option>
                                <option value="desc" <?= ($_SESSION['meole_search']['order'] == "desc") ? "selected" : "" ?> >Terbaru</option>
                                <option value="asc" <?= ($_SESSION['meole_search']['order'] == "asc") ? "selected" : "" ?>>Terlama</option>
                        </select>
                        <button type="button" class="input-group-addon no-bg-addon" style="width: 36px; height: 26px; cursor: pointer;background-color: white;" onclick="remove_filter('order')">
                            <i class="fa fa-close" aria-hidden="true" style="color: #FF5722;">
                            </i>
                        </button>
                    </div>
                    <br>
                    <br>
                    <div class="col-sm-12 col-md-12">
                        <label for="select_tugas_dari" class="col-md-3">Private : </label>
                        <select class="form-control pull-right filter-private" name="private" style="width:60%;">
                                <option value=""></option>
                                <?php foreach ($list_private as $key => $prv): ?>
                                <option value="<?= $key  ?>" <?= ($_SESSION['meole_search']['private'] != null && $_SESSION['meole_search']['private'] == $key) ? "selected" : "" ?>><?= $prv  ?></option>
                                <?php endforeach ?>
                        </select>
                        <button type="button" class="input-group-addon no-bg-addon" style="width: 36px; height: 26px; cursor: pointer;background-color: white;" onclick="remove_filter('private')">
                            <i class="fa fa-close" aria-hidden="true" style="color: #FF5722;">
                            </i>
                        </button>
                    </div>
                    <br>
                    <br>
                    <div class="col-sm-12 col-md-12">
                        <label for="select_tugas_dari" class="col-md-3">Status : </label>
                        <select class="form-control pull-right filter-status" name="status[]" multiple="multiple" style="width:60%;">
                                <option value=""></option>
                                <?php foreach ($list_status as $key => $stat): ?>
                                <option value="<?= $key  ?>" <?= in_array($key, $_SESSION['meole_search']['status']) ? "selected" : ""  ?>><?= $stat  ?></option>
                                <?php endforeach ?>
                        </select>
                        <button type="button" class="input-group-addon no-bg-addon" style="width: 36px; height: 40px; cursor: pointer;background-color: white;" onclick="remove_filter('status')">
                            <i class="fa fa-close" aria-hidden="true\" style="color: #FF5722;">
                            </i>
                        </button>
                    </div>
                </div>
              </div>
              <div class="modal-footer">
                <button type="submit" class="btn k-btn-terapkan">Terapkan Filter</button>
              </div>
              </form>
            </div>
          </div>
        </div>
<?php   
if (empty($data)) {
    ?>
    <div class="alert alert-info">Tidak ada data yang bisa ditampilkan</div>
    <?php
} else {
    foreach ($data as $v) {
        $style = "background-color:white";
        ?>
            <div class="post-section" style="<?= $style ?>">    
                <div id="post-<?= $v['id'] ?>">
                <?php 
                $_data = $v;

            include(__DIR__.'/_post_detail.php');
                 ?>
                </div>
               
            </div>
        <?php
    }
}

?>
   
<nav>
    <div class="text-left" style="text-align: center;">
        <ul class="pagination">
            <?php
            $page = $this->uri->segment(4);
            if (empty($page) || $page == 0) {
                echo '<li class="disabled"><a href = "#">Sebelumnya</a></li>';
            } else {
                echo '<li>' . anchor('web/' . $this->router->class . '/' . $this->router->method . '/' . ($page - 1) . Util::paramsToUrlGet($_GET), 'Sebelumnya') . '</li>';
            }
            if (!$next_page) {
                echo '<li class="disabled"><a href = "#">Selanjutnya</a></li>';
            } else {
                echo '<li>' . anchor('web/' . $this->router->class . '/' . $this->router->method . '/' . ($page + 1) . Util::paramsToUrlGet($_GET), 'Selanjutnya') . '</li>';
            }
            ?>
        </ul>
    </div>
</nav>

<script>
    function remove_filter(filter){
        var a = window.confirm("Apakah anda yakin menghapus filter "+filter+"?");
        if(a){
            window.location.href="<?= site_url('web/post/removefilter') ?>/"+filter;
        }
        
    }

    
    
   

    $(".filter-dinas").select2({
        width: '60%',
        placeholder: 'pilih dinas',
        dropdownParent: $("#filterModal")
    });

    $(".filter-order").select2({
        width: '60%',
        placeholder: 'urutkan berdasarkan',
        dropdownParent: $("#filterModal")
    });

    
    $(".filter-status").select2({
        width: '60%',
        height: '40%',
        placeholder: 'pilih status',
        dropdownParent: $("#filterModal")
    });

    
    // console.log(a);


    $(".filter-private").select2({
        width: '60%',
        placeholder: 'pilih private',
        dropdownParent: $("#filterModal")
    });

    var back = "<?php echo base_url(uri_string()) ?>";
    sessionStorage.setItem("<?php echo $class ?>.back", back);

    function goAdd() {
        location.href = "<?php echo site_url($path . $class . '/add') ?>";
    }

    function goDelete(id) {
        var hapus = confirm("Apakah anda yakin akan menghapus post ini?");
        if (hapus) {
            $("#form_delete").attr("action", "<?php echo site_url($path . $class . '/delete') ?>/" + id);
            $("#form_delete [name='referer']").val(back);
            $("#form_delete").submit();
        }
    }

    $('#belum-rating').on('click',function(){
        location.href = "<?php echo site_url('web/post/unrated') ?>";
    });

    $('#div_cover')
        .mouseenter(function(){
            $("#btn_edit_cover").removeClass("hidden");
            $("#btn_edit_group_photo").removeClass("hidden");
    })
        .mouseleave(function(){
            $("#btn_edit_cover").addClass("hidden");
            $("#btn_edit_group_photo").addClass("hidden");
    });

    $("#btn_edit_cover").click(function () {
        $("#cover").click();
    });
    $("#cover").change(function () {
        $("#form_data_cover").submit();
    });   
    $("#btn_edit_group_photo").click(function () {
        $("#image").click();
    });
    $("#image").change(function () {
        $("#form_data_group_photo").submit();
    });  

    // $(function () {
    //     $("#btn_edit_cover").click(function () {
    //         $("#cover").click();
    //     });
    //     $("#cover").change(function () {
    //         $("#form_data_cover").submit();
    //     });   
    //     $("#btn_edit_group_photo").click(function () {
    //         $("#image").click();
    //     });
    //     $("#image").change(function () {
    //         $("#form_data_group_photo").submit();
    //     });      
    // });

    // function goSaveCover() {
    //     // $("#btn_simpan_cover").addClass("hidden");
    //     // $("#label_cover").addClass("hidden");
    //     // $("#btn_edit_cover").removeClass("hidden");
    //     $("#form_data_cover").submit();
    // }

<?php if ($method == 'group') { ?>
        function goBack() {
            location.href = "<?php echo site_url($path . 'group/list_me') ?>";
        }
<?php } ?>

    $(document).ready(function () {
        $(".image-post").colorbox();
    });

</script>