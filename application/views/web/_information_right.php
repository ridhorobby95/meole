
<?php if ($this->uri->segment(3) == 'detail' && ($data['petugas_id'] == SessionManagerWeb::getUserID() || SessionManagerWeb::isAdministrator())){ ?>
<div class="row" style="background-color: white; margin-left: -1px; margin-right: 4px;">
	<div class="col-md-12">
		<h3>Log Aktivitas</h3>
		<hr>
		<?php if (count($log) == 0){ ?>
		<div class="row" style="margin-bottom: 10px;">
				
				<center><p class="log-content">Belum ada aktivitas</p></center>
		</div>
		<?php } else{ ?>
		<?php foreach ($log as $key => $value): ?>
		<div class="row" style="padding-left: 15px;">
			<div class="col-md-2" style="padding-left: 0px;padding-right: 0px;"><img class="img-circle profile-picture-small" src="<?=  site_url('assets/uploads/'.str_replace(' ', '_', $value['foto_profil'])) ?>" style="margin-right: 0px;"></div>
			<div class="col-md-10">
				<strong><?= $value['nama_user'] ?></strong>
				<p class="log-content"><?= $value['description']  ?></p>
				<p class="log-date"><?php echo FormatterWeb::dateToDiff($value['created_at']) ?></p>
			</div>
		</div>
		<hr>
		<?php endforeach ?>
		<?php } ?>
		
	</div>
</div>
<?php } else {?>

<div class="row" style="background-color: white; margin-left: -1px; margin-right: 4px;">

	<div class="col-md-12">
		<h3>Postingan Favorit</h3>
		<hr>
		<?php foreach ($favorit as $fav): ?>
		<div class="row" style="padding-left: 15px;">
			<div class="col-md-4" style="padding-left: 0px;padding-right: 0px;"><img style="height: 96px; width:96px; cursor:pointer;" src="<?=  site_url('assets/uploads/'.$fav['nama_file']) ?>" style="margin-right: 0px;" onclick="goDetail(<?= $fav['id']  ?>)"></div>
			<div class="col-md-8" style="padding-top: 5px;">
				<?php 
				if (strlen($fav['description']) > 30) {
                    $str = substr($fav['description'], 0, 30) . '...';
                } else {
                    $str = $fav['description'];
                }

				 ?>
				<p class="log-content"><?= $str  ?></p>
				<p style="font-size: 12px; color: #bbb;"><?= $fav['nama_user']  ?></p>
				<p style="font-size: 12px; color: #bbb;"><i style="color:#bbb;" class="fa fa-thumbs-up"></i> <?= $fav['vote']  ?> votes</p>
			</div>
		</div>
		<hr>
		<?php endforeach ?>
	</div>
</div>

<script type="text/javascript">
	function goDetail(id){
		window.location.href = "<?= site_url('web/post/detail/')  ?>/"+id;
	}
</script>
<?php } ?>


