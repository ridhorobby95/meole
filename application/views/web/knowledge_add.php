<div class="row">
    <div class="col-md-12">
        <div class="post-section" id="new_post">
            <form method="post" name="kirim_form" id="kirim_form" action="<?= site_url('web/knowledge/kirim') ?>">
                <input type="hidden" name="group_kirim" id="group_kirim" value="">
                <input type="hidden" name="stick_kirim" id="stick_kirim" value='0' >
                <div class="post-body" style="margin-bottom:5px;padding-bottom:10px">
                    <div class="row">
                        <div class="col-md-1" style="padding:0;text-align:center;">
                            <img class="img-circle" style="height:30px;width:30px" src="<?= site_url($me['user_photo']) ?>" />
                        </div>
                        <div class="col-md-11" style="padding:0;">
                        <textarea class="form-control" id="judul_kirim" name="judul_kirim" rows="1" style="border:0;border-bottom:1px solid #eee" autofocus placeholder="Judul Knowledge"><?= $data['post_title'] ?></textarea>
                        <textarea class="form-control" id="text_kirim" name="text_kirim" rows="15" style="border:0" autofocus placeholder="Deskripsi Knowledge"><?= $data['post_description'] ?></textarea></div>
                        <div class="col-md-2" style="padding:0;text-align:center;vertical-align: center;margin-top:13px">
                            <strong>Kata Kunci</strong>
                        </div>
                        <div class="col-md-10" style="padding:0;margin-top:10px;">
                            <?php echo form_dropdown('tag[]', $a_tag, $data['tags'], 'class="form-control input-sm select2 tag" multiple="multiple" style="width:100%"') ?>
                            <!-- <textarea class="form-control" id="tag" name="tag" rows="1" style="border:0" autofocus placeholder="Deskripsi Knowledge"></textarea> -->
                        </div>
                    </div>
                    <div class="row" style="padding-top:5px">
                        <hr style="margin:5px"/>
                        <div class="col-md-2 col-xs-3">
                            <div class="btn btn-default btn-sm" onclick="$('#gambar').click()"><span class="fa fa-camera" style="cursor:pointer;color:#777;"></span> &nbsp;Foto</div>
                            <hr style=" visibility:hidden; margin:0;padding:0;margin-top: 5px" />
                            <div class="btn btn-default btn-sm" onclick="$('#file_upload').click()"><span class="fa fa-file" style="cursor:pointer;color:#777;"></span> &nbsp;File</div>
                            <hr style=" visibility:hidden; margin:0;padding:0;margin-top: 5px" />
                            <!-- <hr style=" visibility:hidden; margin:0;padding:0;margin-top: 5px" /> -->
                            <!-- <div class="btn btn-default btn-sm" onclick="$('#file_upload').click()"><span class="fa fa-file" style="cursor:pointer;color:#777;"></span> &nbsp;File</div> -->
                            <!-- <hr style=" visibility:hidden; margin:0;padding:0;margin-top: 5px" /> -->
                        </div>
                        <!-- <div class="col-md-2 col-xs-2" style="padding:0;">    
                            <div class="btn btn-default btn-sm" onclick="$('#file_upload').click()"><span class="fa fa-file" style="cursor:pointer;color:#777;"></span> &nbsp;File</div>
                        </div> -->
                        <div class="col-md-5 col-xs-5" style="padding:0;">    
                            <small><strong>Kirim ke</strong></small> &nbsp;
                            <div class="btn-group">
                            <!-- <?= $list_group[$index]['name'] ?> -->
                                <button type="button" class="btn btn-default btn-sm" id="btn_group">Pilih....</button>
                                <button type="button" class="btn btn-default btn-sm dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" id="btn_for_group">
                                    <span class="caret"></span>
                                </button>
                                <ul class="dropdown-menu">
                                    <?php foreach ($list_my_helpdesk as $group) {
                                        if ($group['id']=='1') {  
                                            if (SessionManagerWeb::isStaff()) { ?>
                                                <li>
                                                    <a class="group_kiriman" style="cursor:pointer" val="<?= $group['id'] ?>">
                                                            <?= $group['name'] ?>
                                                    </a>
                                                    <input type='hidden' id='group_tujuan' val="<?= $group['id'] ?>">
                                                </li>
                                            <?php }  
                                        } else { ?>
                                            <li>
                                                
                                                <a class="group_kiriman" style="cursor:pointer" val="<?= $group['id'] ?>">
                                                        <?= $group['name'] ?>
                                                </a>
                                                <input type='hidden' id='group_tujuan' val="<?= $group['name'] ?>">
                                            </li>
                                    <?php }
                                    }
                                    ?>
                                </ul>

                            </div>
                            

                            <?php if ($is_allow_kirim_sebagai and 1==0) { ?>
                                <label id="label_kirim_sebagai" style="display: none">
                                    <hr style=" visibility:hidden; margin:0;padding:0;margin-top: 5px" />
                                    <small><strong>Kirim sebagai</strong></small> &nbsp;
                                    <input type="hidden" name="user_id_anggota" id="user_id_anggota">
                                    <input type="text" name="kirim_sebagai" placeholder="Nama" id="kirim_sebagai" class="form-control" style="width:50%;display: inline;font-size: 12px;">
                                    <hr style=" visibility:hidden; margin:0;padding:0;margin-top: 5px" />
                                </label>
                            <?php } ?>
                            <hr style=" visibility:hidden; margin:0;padding:0;margin-top: 5px" />
                            <div style="padding:0;">
                                <form >
                                    <input type="checkbox" name="sticky" id='sticky'>
                                </form>
                                <small><strong>pin post</strong></small> &nbsp;
                                
                            </div>
                        </div>

                        <div class="col-md-5">
                            <div class="pull-right">
                                <span class="btn btn-sm btn-batal">Batal</span>
                                <span class="btn btn-sm btn-kirim"><i class="fa fa-send"></i> Kirim</span>
                            </div>
                        </div>
                    </div>
                    <div class="row" style="margin-left:0px">
                        <i style="font-size:12px">
                            format foto: .jpg .jpeg .png (dibawah 1MB)
                        </i>
                    </div>
                    <div class="row">
                        <div id="images_preview" name="images_preview" class="col-md-12"></div>
                    </div>
                    <div class="row">
                        <div id="files_preview" name="files_preview" class="col-md-12"></div>
                    </div>
                </div>
            </form>
            <form method="post" name="file_form" id="file_form" enctype="multipart/form-data" action="<?= site_url('web/post/fileform') ?>" class="hidden">
                <input type="file" id="file_upload" name="file_upload[]" multiple style="visibility:hidden">
            </form>
            <form method="post" name="image_form" id="image_form" enctype="multipart/form-data" action="<?= site_url('web/post/imageform') ?>" class="hidden">
                <input type="file" id="gambar" name="gambar[]" multiple style="visibility:hidden">
            </form>
        </div>

    </div>
</div>

<script type="text/javascript" src="<?php echo base_url('assets/web/js/jquery.form.js') ?>"></script>
<script>
    $('#gambar').on('change', function() {
        $('#image_form').ajaxForm({
            target: '#images_preview',
            error: function(e) {
                alert('Terjadi kesalahan. Silakan diulangi lagi');
            }
        }).submit();
    });

    $('#file_upload').on('change', function() {
        $('#file_form').ajaxForm({
            target: '#files_preview',
            error: function(e) {
                 alert('Terjadi kesalahan. Silakan diulangi lagi');
            }
        }).submit();
    });

    $('.group_kiriman').on('click', function() {
        $('#group_kirim').val($(this).attr('val'));
        $('#btn_group').html($(this).html());
    });

    $('#btn-buat-knowledge').on('click',function(){
        $('#new_post').toggle();
        $('#btn-buat-knowledge').addClass('disabled');
    });

    $('.btn-batal').on('click', function() {
        $.ajax({
            url: "<?= site_url('web/knowledge/resetkirim') ?>",
            success: function(result) {
                $("#images_preview").html('');
                $("#files_preview").html('');
                $("#judul_kirim").html('');
                $("#text_kirim").html('');
                $("#judul_kirim").val('');
                $("#text_kirim").val('');
                $('#new_post').hide();
                $('#btn-buat-knowledge').removeClass('disabled');
            }
        });
    });

    $('.btn-kirim').on('click', function() {        
        if ($('#sticky').is(':checked')) {
            $('#stick_kirim').val('1');
        }
        if (checkEmptyJudul()){
            alert('Judul knowledge tidak boleh kosong!');
        } else if (checkEmptyPost()){
            alert('Deskripsi knowledge tidak boleh kosong!');
        } else if ($('#group_kirim').val()==''){
            alert('Helpdesk tujuan harus dipilih!');
        } else if (confirm('Anda akan membuat knowledge?')){
            $('#btn-buat-knowledge').removeClass('disabled');
            $('#kirim_form').submit();    
        }        
    });

    $(document).ready(function() {
        <?php if ($_SESSION['integra']['draft_post']) { ?>
        $.ajax({
            url: "<?= site_url('web/knowledge/getdraft') ?>",
            success: function(result) {
                $("#images_preview").html(result['images']);
                $('#files_preview').html(result['files']);
                $('#judul_kirim').html('<?= $_SESSION['
                    integra ']['
                    draft_post_judul '] ?>');
                $('#text_kirim').html('<?= $_SESSION['
                    integra ']['
                    draft_post_text '] ?>');
                $('#new_post').show();
            }
        });


        <?php } ?>
        $(".tag").select2({
            placeholder: 'Ketik kata kunci',
            minimumResultsForSearch: Infinity,
            tags: true
        });

    });
</script>
<?php include(__DIR__.'/_post_js.php'); ?>