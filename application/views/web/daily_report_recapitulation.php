<div class="row menu-tab">
    <a class="menu-tab-item<?php echo ($method == 'me') ? ' menu-tab-active' : '' ?>" href="<?php echo site_url($path . $class . '/me') ?>">Laporan Saya</a>
    <a class="menu-tab-item<?php echo ($method == 'for_me') ? ' menu-tab-active' : '' ?>" href="<?php echo site_url($path . $class . '/for_me') ?>">Laporan Team</a>
    <a class="menu-tab-item<?php echo ($method == 'recapitulation') ? ' menu-tab-active' : '' ?>" href="<?php echo site_url($path . $class . '/recapitulation') ?>">Rekap Bulanan</a>
</div>
<div class="row">
    <div class="col-md-12">
        <div class="box box-primary">
            <div class="box-body">
                <div class="row">
                    <div class="col-md-12">
                        <div class="row bord-bottom">
                            <label for="date" class="col-sm-3">Laporan pada </label>
                            <?php echo form_open(); ?>
                            <div class="col-sm-5">
                                <div class="input-group">
                                    <?php echo form_dropdown('month', FormatterWeb::$months, set_value('month', date('m')), 'class="form-control input-sm" id="month"'); ?>
                                    <span class="input-group-addon" id="sizing-addon3">-</span>
                                    <?php echo form_dropdown('year', $years, set_value('year', date('Y')), 'class="form-control input-sm" id="year"'); ?>
                                </div>
                            </div>
                            <?php echo form_close(); ?>
                        </div>
                    </div>
                    <div class="col-md-12">
                        <div class="table-responsive">
                            <div class="hidden-xs">
                                <table class="table table-bordered table-condensed table-striped">
                                    <thead>
                                        <tr>
                                            <th class="text-center" style="width:30px">No</th>
                                            <th class="text-center">Nama</th>
                                            <th class="text-center" style="width:70px">Jumlah Hadir <?php if (SessionManagerWeb::isAdministrator() || SessionManagerWeb::isManagemement()) { ?><i>(Telat)</i> <?php } ?></th>
                                            <th class="text-center" style="width:70px">Jumlah Lap.</th>
                                            <th class="text-center" style="width:105px">Jumlah Jam Kerja</th>
                                            <th class="text-center" style="width:70px">Jumlah Lembur</th>
                                            <th class="text-center" style="width:105px">Jumlah Jam Lembur</th>
                                            <th class="text-center" style="width:60px">% Lap.</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php
                                        $no = 0;
                                        foreach ($recapitulations as $rkp) {
                                            $no++;
                                            ?>
                                            <tr>
                                                <td class="text-right"><?= $no ?></td>
                                                <td><?= $rkp['name'] ?></td>
                                                <?php if (SessionManagerWeb::isAdministrator() || SessionManagerWeb::isManagemement()) { ?>
                                                    <td class="text-right"><?= (empty($rkp['presence']) ? 0 : $rkp['presence'] . ' <i>(' . (empty($rkp['late']) ? 0 : $rkp['late']) . ')</i>') ?></td>
                                                <?php } else { ?>
                                                    <td class="text-right"><?= (empty($rkp['presence']) ? 0 : $rkp['presence'] ) ?></td>
                                                <?php } ?>
                                                <td class="text-right"><?= (empty($rkp['countreport']) ? 0 : $rkp['countreport']) ?></td>
                                                <td class="text-right"><?= FormatterWeb::toCountHourMinute(empty($rkp['totalworktime']) ? 0 : ($rkp['totalworktime'] / 60)) ?></td>
                                                <td class="text-right"><?= (empty($rkp['overtime']) ? 0 : $rkp['overtime']) ?></td>
                                                <td class="text-right"><?= FormatterWeb::toCountHourMinute(empty($rkp['totalworkovertime']) ? 0 : ($rkp['totalworkovertime'] / 60)) ?></td>
                                                <td class="text-right"><?php echo round(((int) $rkp['countreport'] / (int) $maxreport) * 100, 2) ?> %</td>
                                            </tr>
                                            <?php
                                        }
                                        ?>
                                    </tbody>
                                </table>
                            </div>
                            <div class="visible-xs">
                                <table class="table table-bordered table-condensed table-striped">
                                    <thead>
                                        <tr>
                                            <th class="text-center">No</th>
                                            <th class="text-center">Nama</th>
                                            <th class="text-center">Hdr</th>
                                            <th class="text-center">Lap</th>
                                            <th class="text-center">J.Krj</th>
                                            <th class="text-center">Lmb</th>
                                            <th class="text-center">J.Lmb</th>
                                            <th class="text-center">%</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php
                                        $no = 0;
                                        foreach ($recapitulations as $rkp) {
                                            $no++;
                                            ?>
                                            <tr>
                                                <td class="text-right"><?= $no ?></td>
                                                <td><?= ucfirst($rkp['username']) ?></td>
                                                <td class="text-right"><?= (empty($rkp['presence']) ? 0 : $rkp['presence']) ?></td>
                                                <td class="text-right"><?= (empty($rkp['countreport']) ? 0 : $rkp['countreport']) ?></td>
                                                <td class="text-right"><?= strtr(FormatterWeb::toCountHourMinute(empty($rkp['totalworktime']) ? 0 : ($rkp['totalworktime'] / 60)), array(' Jam ' => ':', ' Mnt' => '')) ?></td>
                                                <td class="text-right"><?= (empty($rkp['overtime']) ? 0 : $rkp['overtime']) ?></td>
                                                <td class="text-right"><?= strtr(FormatterWeb::toCountHourMinute(empty($rkp['totalworkovertime']) ? 0 : ($rkp['totalworkovertime'] / 60)), array(' Jam ' => ':', ' Mnt' => '')) ?></td>
                                                <td class="text-right"><?php echo round(((int) $rkp['countreport'] / (int) $maxreport) * 100, 2) ?></td>
                                            </tr>
                                            <?php
                                        }
                                        ?>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript">
    $(document).ready(function () {
        $("#month, #year").change(function () {
            $("form").submit();
        })
    })
</script>