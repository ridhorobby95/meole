	
		<div class="post-title">
		    <div class="pull-left user-desc" style="width:75%">
		    	<!--strong><?php echo $_data['user_name'] ?></strong-->
		        <?php //if ($_data['nm_lemb']) echo '<br>'.$_data['nm_lemb']; ?>
		        <strong><a href="<?= site_url('web/informasi/detail/'.$_data['post_id']) ?>"><?php if ($_data['post_title']) { ?>
				<b style="font-size:20px"><?php echo nl2br($_data['post_title']) ?></b>
				<?php } ?></a></strong><br>
				<b style="font-size:12px"><?php echo $_data['user_name'] ?></b>
		        <div class="post-header-info">
		            <?php echo FormatterWeb::dateToDiff($_data['post_created_at']) ?>
		            <?php /* if($_data['createdAt'] != $_data['updatedAt']) { ?>
		            <span class="post-right">Diedit <?php echo FormatterWeb::dateToDiff($_data['updatedAt']) ?></span>
		            <?php } */ ?>
		        </div>
		    </div>
		    <?php 
		 		$bg = '#f39c12';
		 		$color = 'white';
			?>
			<div class="pull-right ticket-desc" style="width:120px">
		        <h5>
		            <?php if ($_data['is_sticky'] && SessionManagerWeb::isStaff()) { ?><!-- <i class="glyphicon glyphicon-pushpin"></i> -->

		            <button type="submit" class="btn btn-group btn-xs" onClick="javascript:stickInformasi('<?php echo $_data['post_id'] ?>','<?php echo $_data['is_sticky'] ?>')" style="background-color:#03A9F4;box-shadow: none;">
		            <i class="glyphicon glyphicon-pushpin" style="color:white"></i>
		            </button>
		                                    
		            <?php } ?>
		            <i>INFO </i><?php echo $_data['group_name'] ?>
		        </h5>
		    </div>

		</div>
		<div class="post-body">
				<p><?php echo nl2br($_data['post_description']) ?></p>

			<!-- <?php foreach ($_data['images'] as $image) { ?>
                <a href="<?= $image['medium'] ?>" class="image-post"><img src="<?= $image['thumb'] ?>" style="max-height:100px" /></a> &nbsp;
			<?php } ?> -->
			<?php 
		        foreach ($_data['images'] as $image) {
		            // $medium_images = explode('/',$image['medium']);
		            // $medium_image = $medium_images[count($medium_images)-1];
		            // $thumb_images = explode('/',$image['thumb']);
		            // $thumb_image = $thumb_images[count($thumb_images)-1];
		     ?>
		        <!-- <a href="<?= $image['medium'] ?>" class="image-post"><img src="<?= $image['thumb'] ?>" style="max-height:100px" /></a> &nbsp; -->
		        <a href="<?= $image['original'] ?>" class="image-post"><img src="<?= $image['thumb'] ?>" style="max-height:100px" /></a> &nbsp; 
		    <?php } ?>
			
			<?php if(!empty($_data['link'])) { ?>
			<p><h5><a href="<?php echo $_data['link'] ?>"><?php echo $_data['link'] ?></a></h5></p>
			<?php } ?>
			<?php if(!empty($_data['file'])) { ?>
			<p><a href="<?php echo $_data['file']['link'] ?>" target="_blank"><?php echo $_data['file']['name'] ?></a></p>
			<?php } ?>
			<!-- Edit/Hapus-->
			<!--div class="line-space">
				<div class="post-header-info">
					<?php if($_data['post_user_id'] == SessionManagerWeb::getUserID() || SessionManagerWeb::isAdministrator()) { ?>
						<a class="post-left post-edit" href="">Edit</a>
						<a class="post-right post-delete" href="" >Hapus</a>

					<?php } ?>
				</div>
			</div-->
			<!-- <?php if(!empty($_data['files'])) {  ?>
		        <hr style="margin-bottom:5px;" />
		        <?php foreach ($_data['files'] as $file) { ?>
		            <div style="background-color: #f5f5f5">
		            <strong><a href="<?php echo $file ?>" target="_blank" >
		                <?php 
		                    $fileName = explode('-',$file);
		                    echo $fileName[1]; 
		                ?>
		            </a></strong>
		            </div>
		        <?php } ?>
		    <?php } ?> -->
		    <?php if(!empty($_data['files'])) {  ?>
		        <hr style="margin-bottom:5px;" />
		        <?php 
		            foreach ($_data['files'] as $file) { 
		                $links = explode('/',$file);
		                $link = $links[count($links)-1];
		                // die($file);
		        ?>
		            <div style="background-color: #f5f5f5">
		            <strong>
		            <i class="glyphicon glyphicon-file" style="color:#05057d"></i>
		            <!-- <a href="<?php echo $file ?>" target="_blank" > -->
		            <a href="<?php echo site_url($path.'thumb/files/'.$link) ?>" target="_blank" > 
		                <?php 
		                    $fileName = explode('-',$file);
		                    echo $fileName[1]; 
		                ?>
		            </a></strong>
		            </div>
		        <?php } ?>
		    <?php } ?>
			<!-- <hr style="margin-bottom:5px;" />
 -->		    <!-- <div class="pull-left">
		    <?php $jml_view = ($_data['view_number']) ? $_data['view_number'] : 0 ?>
		    <small>dilihat <strong style="color:red"><?php echo $jml_view ?></strong> user</small>&nbsp; 
		    <strong style="color:black;font-size: 14px"><?php echo $jml_view ?></strong>  &nbsp;<span class="glyphicon glyphicon-eye-open" style="font-size:12px" > </span>
		    </div> -->
		</div>
		
		<div class="post-footer">
			<div class="row-fluid">
				<!--div class="btn-group">
					<a href="<?= site_url($path . 'knowledge/detail/' . $_data['post_id']) ?>">	
					<b><span class="fa fa-comment"></span>
					<?php echo (int) $_data['comment_number'] ?> Tanggapan
					<?php if ($_data['post_user_id'] == SessionManagerWeb::getUserID() && $_data['categoryId'] != Category_model::KNOWLEDGE_MANAGEMENT) { ?>
					<?php } ?></b>
					</a>					
				</div-->

				
				<div class="btn-group">
			        <?php $jml_view = ($_data['view_number']) ? $_data['view_number'] : 0 ?>
			        <!-- <small>dilihat <strong style="color:red"><?php echo $jml_view ?></strong> user</small>&nbsp;  -->
			        <span class="glyphicon glyphicon-eye-open" style="font-size:12px" > </span>  &nbsp;<strong style="color:black;font-size: 12px"><?php echo $jml_view ?></strong>   &nbsp;
		        </div>
		        <?php if ($_data['post_type']!=Post_Model::TYPE_DISCLAIMER) { ?>
		            <div class="btn-group">
		                <a href="<?= site_url($path . 'informasi/detail/' . $_data['post_id']) ?>">
		                    <b><span class="fa fa-comment"></span>
		    					<?php echo $_data['comment_number'] ?> Tanggapan
		    					<?php if ($_data['post_user_id'] == SessionManagerWeb::getUserID() && $_data['categoryId'] != Category_model::KNOWLEDGE_MANAGEMENT) { ?>
		    					<?php } ?></b>
		                </a>
		            </div>
		        <?php } ?>
		        <?php if ($_data['hak_akses']['edit_hapus'] || SessionManagerWeb::isSuperAdministrator() || $_data['post_user_id']==SessionManagerWeb::getUserID()) { 
				?>
			    <div class="btn-group" style="margin-left: 5px">
			        <a href="<?= site_url($path . 'informasi/edit/' . $_data['post_id']) ?>" class="btn btn-warning btn-xs" id="btn_edit">Edit</a>
			        <span class="btn btn-danger btn-xs" id="btn_hapus_post" onclick="hapusPost(<?= $_data['post_id'] ?>)">Hapus</span>
			    </div>
					<?php if ($_data['is_sticky']=='1') { ?>
	                <div class="btn-group  pull-right">
	                	<!--form-->
					   <form method="post" name="kirim_sticky" id="kirim_sticky" action="<?= site_url('web/informasi/stick/'. $_data['post_id']) ?>">
					   	<input type='hidden' id='is_sticky' name='is_sticky' value="<?php echo $_data['is_sticky']?>"> 
					   </form>
		                  <!-- <button type="button" class="btn btn-group btn-xs" style="margin-left:5px;background-color:orange"><a style="color:white;" href="javascript:stickKnowledge('<?php echo $_data['post_id'] ?>','<?php echo $_data['is_sticky'] ?>')">Hilangkan Pin Post
			                  </a>
			                </button> -->
		                  
		                  	<!--input type="checkbox" class="btn btn-primary btn-xs dropdown-toggle" name="sticky_checkbox" color="red" ?> 
		                </form-->
	                  
	                  <!--button type="button" class="btn btn-primary btn-xs" id="btn_role">Set Status</button>
	                  <button type="button" class="btn btn-primary btn-xs dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
	                    <span class="caret"></span>
	                  </button-->
	                </div>
	                <br>   
				<?php } 
				} ?>
			</div>  
        </div>

<script type="text/javascript" src="<?php echo base_url('assets/web/js/jquery.form.js') ?>"></script>   
<script>

	function stickInformasi(id,is_sticky){
		//(id+' '+is_sticky);
		if (is_sticky=='1') {
			stick = confirm('Hilangkan informasi ini dari Pin Post ?');
		} else {
			stick = confirm('Masukkan informasi ini ke Pin Post ?');
		}
		//alert("<?= site_url('web/knowledge/stick')?>/"+id);
		if (stick) {
			//alert(stick);
			//$('#kirim_sticky').submit();
			$.ajax(
				{
					url: "<?= site_url('web/informasi/stick') ?>/"+ id,
					type:'POST',
					data: {'sticky':is_sticky}, 
					success: function(){
						//alert('berhasil');
		        		window.location.href = "<?= site_url('web/informasi/stick') ?>?id="+ id+"&stick="+is_sticky;
		    		}
		    	}
		    );
		}
	}

</script>    

