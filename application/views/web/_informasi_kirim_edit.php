<div class="post-section" id="new_post">
    <form method="post" name="kirim_form" id="kirim_form" action="<?= site_url('web/informasi/kirim') ?>">
        <input type="hidden" name="group_kirim" id="group_kirim" value="<?= $list_group[0]['id'] ?>">
        <input type="hidden" name="stick_kirim" id="stick_kirim" value='0' >
        <div class="post-body" style="margin-bottom:5px;padding-bottom:10px">
            <div class="row">

                <div class="col-md-1" style="padding:0;text-align:center;">
                    <img class="img-circle" style="height:30px;width:30px" src="<?= site_url($me['photo']) ?>" />
                </div>
                <div class="col-md-11" style="padding:0;">
                <textarea class="form-control" id="judul_kirim" name="judul_kirim" rows="1" style="border:0;border-bottom:1px solid #eee" autofocus placeholder="Judul Informasi"><?php echo $data['post_title'] ?></textarea>
                <textarea class="form-control" id="text_kirim" name="text_kirim" rows="5" style="border:0" autofocus placeholder="Deskripsi Informasi"><?php echo $data['post_description'] ?></textarea></div>
            </div>
            <div class="row" style="padding-top:5px">
                <hr style="margin:5px"/>
                <div class="col-md-2">
                    <div class="btn btn-default btn-sm" onclick="$('#gambar').click()"><span class="fa fa-camera" style="cursor:pointer;color:#777;"></span> &nbsp;Foto</div>
                </div>
                <div class="col-md-3" style="padding:0;">    
                </div>
                <div class="col-md-2" style="padding:0;">
                    <form >
                        <input type="checkbox" name="sticky" id='sticky' <?php if ($data['is_sticky']=='1') echo "checked" ?>>
                    </form>
                    <small><strong>pin post</strong></small> &nbsp;
                    
                </div>
                <div class="col-md-5">
                    <div class="pull-right">
                        <span class="btn btn-sm btn-batal">Batal</span>
                        <span class="btn btn-sm btn-kirim"><i class="fa fa-send"></i> Kirim</span>
                    </div>
                </div>
            </div>
            <div class="row">
                <div id="images_preview" name="images_preview" class="col-md-12"></div>
            </div>
        </div>
    </form>
    <form method="post" name="image_form" id="image_form" enctype="multipart/form-data" action="<?= site_url('web/post/imageform') ?>">
        <input type="file" id="gambar" name="gambar[]" multiple style="visibility:hidden">
    </form>
</div>

<script type="text/javascript" src="<?php echo base_url('assets/web/js/jquery.form.js') ?>"></script>
<script>

    // function loadGambar($image){
    //     //alert($image);
    //     $('#gambar').val()=$image;
    //     $('#image_form2').ajaxForm({
    //         target: '#images_preview',
    //         error: function(e) {
    //             alert('Terjadi kesalahan. Silakan diulangi lagi');
    //         }
    //     }).submit();
    // }

    $('#gambar').on('change', function() {
        $('#image_form').ajaxForm({
            target: '#images_preview',
            error: function(e) {
                alert('Terjadi kesalahan. Silakan diulangi lagi');
            }
        }).submit();
    });

    $('.group_kiriman').on('click', function() {
        $('#group_kirim').val($(this).attr('val'));
        $('#btn_group').html($(this).html());
    });

    $('.btn-batal').on('click', function() {
        $.ajax({
            url: "<?= site_url('web/informasi/resetkirim') ?>",
            success: function(result) {
                $("#images_preview").html('');
                $('#judul_kirim').html('');
                $('#text_kirim').html('');
                $('#new_post').hide();
            }
        });
    });

    $('.btn-kirim').on('click', function() {
        if (!confirm('Anda akan membuat knowledge?'))
            return false;
        if ($('#sticky').is(':checked')) {
            $('#stick_kirim').val('1');
        }

        $('#kirim_form').submit();
    });

    $(document).ready(function() {
        <?php if ($_SESSION['integra']['draft_post']) { ?>
        $.ajax({
            url: "<?= site_url('web/informasi/getdraft') ?>",
            success: function(result) {
                $("#images_preview").html(result);
                $('#judul_kirim').html('<?= $_SESSION['
                    integra ']['
                    draft_post_judul '] ?>');
                $('#text_kirim').html('<?= $_SESSION['
                    integra ']['
                    draft_post_text '] ?>');
                $('#new_post').show();
            }
        });

        <?php } ?>
    });
</script>