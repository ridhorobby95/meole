<div class="row">
	<div class="col-md-8">
        <?php if ($this->router->method!='ticketdone') { ?>
            <?php if ($belum_rating){ ?><div class="alert alert-warning" id="belum-rating"><strong>Mohon berikan rating pada tiket !</strong></div>
            <?php } ?>
        <?php } ?>

        <?php
                
            if (empty($data)) {
                ?>
                <div class="alert alert-success"><?php if ($this->uri->segment(3)=='ticketdone') echo 'Tidak ada data.'; else {  ?>Anda sudah memberikan rating ke seluruh tiket !<?php } ?></div>
                <?php
            } else {
                foreach ($data as $v) {
                	$style = "background-color:#efefef;border:1px solid #ccc";
                		
                ?>
                	<div class="post-section" style="<?= $style ?>">    
                        <div id="post-<?= $v['post_id'] ?>">
                		<?php
                            $_data = $v;
                            include(__DIR__.'/_post_detail.php');
                		?>
                        </div>
                           
                	</div>
                <?php 
                }
            }	
        ?>
	</div>
	<div class=" col-md-4 hidden-xs">
	   <?php include(__DIR__.'/_information_right.php'); ?>
	</div>
</div>



<nav>
    <ul class="pager">
        <?php
        $page = $this->uri->segment(5);

        if (empty($page) || $page == 0) {
            echo '<li class="disabled"><a href = "#">Sebelumnya</a></li>';
        } else {
            if ($this->router->method=='ticketdone'){
                echo '<li>' . anchor('web/' . $this->router->class . '/' . $this->router->method . '/' .$this->uri->segment(4).'/'. ($page - 1) . Util::paramsToUrlGet($_GET), 'Sebelumnya') . '</li>';
            } else {
                echo '<li>' . anchor('web/' . $this->router->class . '/' . $this->router->method . '/' . ($page - 1) . Util::paramsToUrlGet($_GET), 'Sebelumnya') . '</li>';
            }
            
        }
        if ($data[0]['next_page_post']==0) {
            echo '<li class="disabled"><a href = "#">Selanjutnya</a></li>';
        } else {
            if ($this->router->method=='ticketdone'){
                echo '<li>' . anchor('web/' . $this->router->class . '/' . $this->router->method . '/' .$this->uri->segment(4).'/'. ($page + 1) . Util::paramsToUrlGet($_GET), 'Selanjutnya') . '</li>';
            } else {
                echo '<li>' . anchor('web/' . $this->router->class . '/' . $this->router->method . '/' . ($page + 1) . Util::paramsToUrlGet($_GET), 'Selanjutnya') . '</li>';
            }
        }
        ?>
    </ul>
</nav>

<?php echo form_open(null, array('id' => 'form_delete')) ?>
<?php echo form_hidden('referer') ?>
</form>

<?php include(__DIR__.'/_post_js.php'); ?>

<script>
    var back = "<?php echo base_url(uri_string()) ?>";
    sessionStorage.setItem("<?php echo $class ?>.back", back);

    function goAdd() {
        location.href = "<?php echo site_url($path . $class . '/add') ?>";
    }

    function goDelete(id) {
        var hapus = confirm("Apakah anda yakin akan menghapus post ini?");
        if (hapus) {
            $("#form_delete").attr("action", "<?php echo site_url($path . $class . '/delete') ?>/" + id);
            $("#form_delete [name='referer']").val(back);
            $("#form_delete").submit();
        }
    }

    // $('#belum-rating').on('click',function(){
    //     location.href = "<?php echo site_url('web/post/index?s=S') ?>";
    // });

<?php if ($method == 'group') { ?>
        function goBack() {
            location.href = "<?php echo site_url($path . 'group/list_me') ?>";
        }
<?php } ?>

    $(document).ready(function () {
        $(".image-post").colorbox();
    });

</script>