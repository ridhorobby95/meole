
<div class="row" id="main-container">
    <div class="col-md-8">

        <?php
            if ($data['post_id']==NULL) {
                $style = "background-color:white";
        ?>
        <div class="post-section" style="<?= $style ?>">           
            <?php 
            $_data = $data;
            ?>                
            <div class="post-section" id="new_post">
                <form method="post" name="kirim_form" id="kirim_form" action="<?= site_url('web/post/kirim_disclaimer') ?>">
                    <div class="post-body" style="margin-bottom:5px;padding-bottom:10px">
                        <div class="row">

                            <div class="col-md-1" style="padding:0;text-align:center;">
                                <img class="img-circle" style="height:30px;width:30px" src="<?= site_url($me['user_photo']) ?>" />
                            </div>
                            <div class="col-md-11" style="padding:0;">
                            <textarea class="form-control" id="text_kirim" name="text_kirim" rows="5" style="border:0" autofocus placeholder="ketik pesan Anda di sini"></textarea></div>
                        </div>
                        <div class="row" style="padding-top:5px">
                            <hr style="margin:5px"/>
                            <div class="col-md-2 col-xs-4">
                                <div class="btn btn-default btn-sm" onclick="$('#gambar').click()"><span class="fa fa-camera" style="cursor:pointer;color:#777;"></span> &nbsp;Foto</div>
                            </div>
                            <div class="col-md-4" style="float:right;padding:0">
                                <div class="pull-right">
                                    <span class="btn btn-sm btn-batal">Batal</span>
                                    <span class="btn btn-sm btn-kirim"><i class="fa fa-send"></i> Kirim</span>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div id="images_preview" name="images_preview" class="col-md-12"></div>
                        </div>
                    </div>
                </form>
                <form method="post" name="image_form" id="image_form" enctype="multipart/form-data" action="<?= site_url('web/post/imageform') ?>">
                    <input type="file" id="gambar" name="gambar[]" multiple style="visibility:hidden">
                </form>
            </div>
        </div>
                <?php } ?>
        </div>

        <div class=" col-md-4 hidden-xs">
        <?php include(__DIR__.'/_information_right.php'); ?>
        </div>
</div>

<script type="text/javascript" src="<?php echo base_url('assets/web/js/jquery.form.js') ?>"></script>
<script>
    $('#gambar').on('change', function() {
        $('#image_form').ajaxForm({
            target: '#images_preview',
            error: function(e) {
                //target: '#images_preview';
                 alert('Terjadi kesalahan. Silakan diulangi lagi');
            }
        }).submit();
    });

    $('.btn-batal').on('click', function() {
        $.ajax({
            url: "<?= site_url('web/post/resetkirim') ?>",
            success: function(result) {
                $('#images_preview').html('');
                $('#text_kirim').html('');
                $('#new_post').hide();
                window.location = "<?= site_url('web/post') ?>";
            }
        });
    });

    $('.btn-kirim').on('click', function() {
        if (!confirm('Anda akan mengirim pesan ke Disclaimer ?'))
            return false;
        $('#kirim_form').submit();
    });

    $(document).ready(function() {
        <?php if ($_SESSION['integra']['draft_post']) { ?>
        $.ajax({
            url: "<?= site_url('web/post/getdraft') ?>",
            success: function(result) {
                $("#images_preview").html(result['images']);
                $('#text_kirim').html('<?= $_SESSION['
                    integra ']['
                    draft_post_text '] ?>');
                $('#new_post').show();
            }
        });

        <?php } ?>
    });
</script>

<?php include(__DIR__.'/_post_js.php'); ?>
