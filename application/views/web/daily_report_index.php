<div class="row menu-tab">
    <a class="menu-tab-item<?php echo ($method == 'me') ? ' menu-tab-active' : '' ?>" href="<?php echo site_url($path . $class . '/me') ?>">Laporan Saya</a>
    <a class="menu-tab-item<?php echo ($method == 'for_me' or $method == 'user') ? ' menu-tab-active' : '' ?>" href="<?php echo site_url($path . $class . '/for_me') ?>">Laporan Team</a>
    <a class="menu-tab-item<?php echo ($method == 'recapitulation') ? ' menu-tab-active' : '' ?>" href="<?php echo site_url($path . $class . '/recapitulation') ?>">Rekap Bulanan</a>
</div>
<?php if(!empty($a_user)) { ?>
<div class="row">
    <div class="col-md-12 mar-bot-10px">
        <form class="form-inline">
            Anggota Team : <?php echo form_dropdown('userid', array('' => '-- Semua --') + $a_user, ($method == 'user' ? $this->uri->segment(4) : null), 'id="userid" class="form-control input-sm"') ?>
        </form>
    </div>
</div>
<?php } ?>
<?php
if (empty($data)) {
    ?>
    <div class="alert alert-info">Belum ada data <?php echo $title ?></div>
    <?php
} else {
    foreach ($data as $v) {
        ?>
        <div class="post-section">
            <div>
                <div class="site-photo site-photo-48 site-photo-left" data-id="<?php echo $v['user']['id'] ?>">
                    <img src="<?php echo isset($v['user']['photo']) ? site_url($path.'thumb/watermark/'.$v['user']['id'].'/48') : $nopic ?>" />
                </div>
                <div>
                    <div>
                        <span data-id="<?php echo $v['user']['id'] ?>">
                            <strong><?php echo $v['user']['name'] ?></strong>
                        </span>
                        <?php
                        if ($v['type'] == Post_model::TYPE_FRIEND and ! empty($v['postUsers'])) {
                            $a_nama = array();
                            foreach ($v['postUsers'] as $vu)
                                $a_nama[] = $vu['name'];
                            ?>
                            melaporkan ke <strong><?php echo implode(', ', $a_nama) ?></strong>
                        <?php } ?>
                    </div>
                    <div class="post-header-info">
                        <?php echo FormatterWeb::toIndoDate($v['date'], true) ?>
                    </div>
                </div>
            </div>
            <div class="post-body">
                <?php if (!empty($v['workdone'])) { ?>
                    <p><strong>Yang sudah beres :D</strong></p>
                    <p style="margin-bottom:20px"><?php echo nl2br($v['workdone']) ?></p>
                <?php } ?>
                <?php if (!empty($v['issue'])) { ?>
                    <p><strong>Ada Masalah Nih :</strong></p>
                    <p style="margin-bottom:20px"><?php echo nl2br($v['issue']) ?></p>
                <?php } ?>
                <?php if (!empty($v['description'])) { ?>
                    <p><strong>Yang mau dilakukan Hari ini :</strong></p>
                    <p style="margin-bottom:20px"><?php echo nl2br($v['description']) ?></p>
                <?php } ?>
                <?php if ($v['isOverTime'] == 1) { ?>
                    <p>Jam Lembur : <?php echo $v['startWork'] ?> - <?php echo $v['endWork'] ?></p>
                    <?php
                } else {
                    if (!empty($v['startWork']) || !empty($v['endWork'])) {
                        ?>
                        <p>Jam Kerja : <?php echo $v['startWork'] ?> - <?php echo $v['endWork'] ?></p>
                        <?php
                    }
                }
                ?>
                <?php if (!empty($v['image'])) { ?>
                <p><a href="<?= TemplateManagerWeb::getPostImageLink($v['image']) ?>" class="image-post"><img src="<?php echo TemplateManagerWeb::getPostImageLink($v['image']) ?>" /></a></p>
                <?php } ?>
                <?php if (!empty($v['link'])) { ?>
                    <p><a href="<?php echo $v['link'] ?>"><?php echo $v['link'] ?></a></p>
                <?php } ?>
                <?php if (!empty($v['file'])) { ?>
                    <p><a href="<?php echo $v['file']['link'] ?>" target="_blank"><?php echo $v['file']['name'] ?></a></p>
                <?php } ?>
            </div>
            <div class="post-footer">
                <a href="<?php echo site_url($path . $class . '/detail/' . $v['id']) ?>"><?php echo count($v['comments']) ?> komentar</a>
                <?php if ($v['user']['id'] == SessionManagerWeb::getUserID()) { ?>
                    <?php /* <a class="post-right" href="<?php echo site_url($path.$class.'/edit/'.$v['id']) ?>">Edit</a> */ ?>
                    <a class="post-right post-delete" href="javascript:goDelete('<?php echo $v['id'] ?>')">Hapus</a>
                <?php } ?>
            </div>
        </div>
        <?php
    }
}
?>
<nav>
    <ul class="pager">
        <?php
        if($method == 'user') {
            $userid = $this->uri->segment(4);
            $page = $this->uri->segment(5);
        }
        else
            $page = $this->uri->segment(4);
        
        if (empty($page) || $page == 0) {
            echo '<li class="disabled"><a href = "#">Sebelumnya</a></li>';
        } else {
            echo '<li>' . anchor('web/' . $this->router->class . '/' . $this->router->method . '/' . ($method == 'user' ? $userid.'/' : '') . ($page - 1), 'Sebelumnya') . '</li>';
        }
        if (empty($data)) {
            echo '<li class="disabled"><a href = "#">Selanjutnya</a></li>';
        } else {
            echo '<li>' . anchor('web/' . $this->router->class . '/' . $this->router->method . '/' . ($method == 'user' ? $userid.'/' : '') . ($page + 1), 'Selanjutnya') . '</li>';
        }
        ?>
    </ul>
</nav>

<?php echo form_open(null, array('id' => 'form_delete')) ?>
<?php echo form_hidden('referer') ?>
</form>

<script type="text/javascript">

    var back = "<?php echo base_url(uri_string()) ?>";
    sessionStorage.setItem("<?php echo $class ?>.back", back);
    
    $(function() {
        $("#userid").change(function() {
            if($(this).val())
                location.href = "<?php echo site_url($path . $class . '/user') ?>/" + $(this).val();
            else
                location.href = "<?php echo site_url($path . $class . '/for_me') ?>";
        });
    });

    function goAdd() {
        location.href = "<?php echo site_url($path . $class . '/add') ?>";
    }

    function goDelete(id) {
        var hapus = confirm("Apakah anda yakin akan menghapus laporan harian ini?");
        if (hapus) {
            $("#form_delete").attr("action", "<?php echo site_url($path . $class . '/delete') ?>/" + id);
            $("#form_delete [name='referer']").val(back);
            $("#form_delete").submit();
        }
    }
    
    <?php if($method == 'for_me') { ?>
    $(function() {
        $("[data-id]").css("cursor","pointer").click(function() {
            location.href = "<?php echo site_url($path.$class.'/user') ?>/" + $(this).attr("data-id");
        });
    });
    <?php } ?>
    
</script>