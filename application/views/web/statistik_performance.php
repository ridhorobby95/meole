<?php 
// echo '<pre>';
// var_dump($_SESSION);
// echo '</pre>';
// die();
    $view_filter_date = 'none';
    if ($chosen_helpdesk!=NULL) {
        $view_filter_date = "inline";
    }
?>

<div class="row">
    <div class="col-md-12">        
        <div class="box box-primary">
            <div class="box-body">
                <div class="row">
                    <div class="col-md-12">
                        <h2><?= $title ?> Performance <?= $chosen_helpdesk ?></h2>
                        <?php 
                            $filter_date = '';
                            if (isset($date_start) and isset($date_end)) {
                                $filter_date = "&start_date=".$date_start."&end_date=".$date_end;
                         ?>
                            <h6 style="color:grey">Periode : <?php echo $date_start." sampai ".$date_end ?></h6>
                        <?php } ?>
                        <?php if (SessionManagerWeb::isSuperAdministrator()) { ?>
                            <hr>
                            <form >
                                <label for="name" class="col-md-3">Tampilkan Statistik dari : </label>
                                <div class="btn-group" style="margin-left:-50px">
                                    <?php
                                        if ($chosen_helpdesk!='') { ?>
                                            <input type="button" class="btn btn-default btn-xs" id="btn_helpdesk" name="btn_helpdesk" value="<?php echo $chosen_helpdesk ?>">
                                       <?php } else { ?>
                                            <!-- <input type="button" class="btn btn-default btn-xs" id="btn_helpdesk" name="btn_helpdesk" value="Semua Unit"> -->
                                            <input type="button" class="btn btn-default btn-xs" id="btn_helpdesk" name="btn_helpdesk" value="Pilih...">
                                        <?php }
                                    ?>
                                    
                                    <button type="button" class="btn btn-default btn-xs dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                        <span class="caret"></span>
                                    </button>
                                    <ul class="dropdown-menu">
                                        <!-- <li>
                                            <a style="text-align:left" class="set-helpdesk-dashboard" value="">
                                                Semua Unit
                                            </a>
                                        </li> -->
                                        <?php foreach ($list_helpdesk as $k => $v) { ?>
                                        <li>
                                            <a style="text-align:left" class="set-helpdesk-dashboard" value="<?= $v['name'] ?>">
                                                <?= $v['name'] ?>
                                            </a>
                                        </li>
                                        <?php } ?>
                                    </ul>
                                </div>
                            </form>
                        <?php } else { ?>
                            <hr style="display:none">
                            <form style="display:none">
                                <label for="name" class="col-md-3">Tampilkan Statistik dari : </label>
                                <div class="btn-group" style="margin-left:-50px">
                                    <?php
                                        if ($chosen_helpdesk!='') { ?>
                                            <input type="button" class="btn btn-default btn-xs" id="btn_helpdesk" name="btn_helpdesk" value="<?php echo $chosen_helpdesk ?>">
                                       <?php } else { ?>
                                            <input type="button" class="btn btn-default btn-xs" id="btn_helpdesk" name="btn_helpdesk" value="Semua Unit">
                                        <?php }
                                    ?>
                                    
                                    <button type="button" class="btn btn-default btn-xs dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                        <span class="caret"></span>
                                    </button>
                                    <ul class="dropdown-menu">
                                        <li>
                                            <a style="text-align:left" class="set-helpdesk-dashboard" value="">
                                                Semua Unit
                                            </a>
                                        </li>
                                        <?php foreach ($list_helpdesk as $k => $v) { ?>
                                        <li>
                                            <a style="text-align:left" class="set-helpdesk-dashboard" value="<?= $v['name'] ?>">
                                                <?= $v['name'] ?>
                                            </a>
                                        </li>
                                        <?php } ?>
                                    </ul>
                                </div>
                            </form>
                        <?php }?>
                        <hr>
                        <form style="display:<?= $view_filter_date ?>">
                            <div class="input-group col-md-3" style="float:left;">
                                <div class="input-group-addon" ><i class="fa fa-calendar"></i></div>
                                    <?php echo form_input(array('name' => 'date_started','class' => 'form-control input-sm', 'value' => (isset($date_start) ? $date_start : NULL),'placeholder' => 'dd-mm-yyyy', 'onchange' => 'dateChange()', 'autocomplete'=>'off')) ?>
                            </div>
                            <center><span class="col-xs-1" style="width:3%">-</span></center>
                            <div class="input-group col-md-3" style="float:left">
                                <div class="input-group-addon"><i class="fa fa-calendar"></i></div>
                                    <?php echo form_input(array('name' => 'date_finished','class' => 'form-control input-sm', 'value' => (isset($date_end) ? $date_end : NULL),'placeholder' => 'dd-mm-yyyy', 'onchange' => 'dateChange()', 'autocomplete'=>'off')) ?>
                            </div>
                            <span class="btn btn-primary col-md-1" name="btn-filter" id="btn-filter" style="padding-top:4px;padding-bottom: 4px;margin-left:20px">Filter</span>  
                            <span class="btn btn-success col-md-2" name="btn-showall" id="btn-showall" style="padding-top:4px;padding-bottom: 4px;margin-left:20px">Tampilkan Semua</span>  
                        </form>
                        <br>
                        <br>
                        <br>
                        <?php  if ($chosen_helpdesk!=NULL) { ?>
                            <div style="margin-bottom: 10px;">
                                <?php 
                                    $std['response_time'] = array();
                                    $std['response_time']['hari'] = floor($std[0]['avg_response']/60/24);
                                    $std['response_time']['jam'] = floor($std[0]['avg_response']/60)%24;
                                    $std['response_time']['menit'] = $std[0]['avg_response']%60;
                                    $std['processing_time'] = array();
                                    $std['processing_time']['hari'] = floor($std[0]['avg_process']/60/24);
                                    $std['processing_time']['jam'] = floor($std[0]['avg_process']/60)%24;
                                    $std['processing_time']['menit'] = $std[0]['avg_process']%60;
                                    $std['solving_time'] = array();
                                    $std['solving_time']['hari'] = floor($std[0]['avg_solving']/60/24);
                                    $std['solving_time']['jam'] = floor($std[0]['avg_solving']/60)%24;
                                    $std['solving_time']['menit'] = $std[0]['avg_solving']%60;
                                ?>
                                <div class="col-xs-3 col-md-2" style="padding:0">
                                    <div class="thumbnail"  style="margin:0px;padding:0">
                                        <div class="caption" style="padding-bottom: 0px">
                                            <center>
                                                <?= //$std[0]['total_s'] 
                                                $std_all[0]['total_s'];
                                                ?>
                                                <p style="font-size:11px">Tiket Selesai</p>
                                            </center>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-xs-3 col-md-3" style="padding:0">
                                    <div class="thumbnail" style="margin:0px;padding:0">
                                        <div class="caption" style="padding-bottom: 0px">
                                            <center>
                                            <?php if ($std['response_time']['hari']>0) { ?>
                                                <?= $std['response_time']['hari'] ?><i style="font-size:10px;color:red">hari</i>
                                            <?php } ?>
                                            <?php if ($std['response_time']['jam']>0) { ?>
                                                <?= $std['response_time']['jam'] ?><i style="font-size:10px;color:blue">jam</i>
                                            <?php } ?>
                                            <?php if ($std['response_time']['menit']>=0) { ?>
                                                <?= $std['response_time']['menit'] ?><i style="font-size:10px;color:green">mnt</i>
                                            <?php } ?>
                                            <p style="font-size: 11px">
                                            Average Response Time</p>
                                            </center>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-xs-3 col-md-3" style="padding:0">
                                    <div class="thumbnail" style="margin:0px;padding:0">
                                        <div class="caption" style="padding-bottom: 0px">
                                            <center>
                                            <?php if ($std['processing_time']['hari']>0) { ?>
                                                <?= $std['processing_time']['hari'] ?><i style="font-size:10px;color:red">hari</i>
                                            <?php } ?>
                                            <?php if ($std['processing_time']['jam']>0) { ?>
                                                <?= $std['processing_time']['jam'] ?><i style="font-size:10px;color:blue">jam</i>
                                            <?php } ?>
                                            <?php if ($std['processing_time']['menit']>=0) { ?>
                                                <?= $std['processing_time']['menit'] ?><i style="font-size:10px;color:green">mnt</i>
                                            <?php } ?>
                                            <p style="font-size: 11px">Average Processing Time</p>
                                            </center>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-xs-3 col-md-2" style="padding:0">
                                    <div class="thumbnail" style="margin:0px;padding:0">
                                        <div class="caption" style="padding-bottom: 0px">
                                            <center>
                                            <?php if ($std['solving_time']['hari']>0) { ?>
                                                <?= $std['solving_time']['hari'] ?><i style="font-size:10px;color:red">hari</i>
                                            <?php } ?>
                                            <?php if ($std['solving_time']['jam']>0) { ?>
                                                <?= $std['solving_time']['jam'] ?><i style="font-size:10px;color:blue">jam</i>
                                            <?php } ?>
                                            <?php if ($std['solving_time']['menit']>=0) { ?>
                                                <?= $std['solving_time']['menit'] ?><i style="font-size:10px;color:green">mnt</i>
                                            <?php } ?>
                                            <p style="font-size: 11px">
                                            Average Solving Time</p>
                                            </center>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-xs-3 col-md-2" style="padding:0">
                                    <div class="thumbnail" style="margin:0px;padding:0">
                                        <div class="caption" style="padding-bottom: 0px">
                                            <center>
                                            <?= round($std[0]['solvability'],2) ?>%
                                            <p style="font-size: 11px">
                                            %Solvability</p>
                                            </center>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <br>
                            <br>
                            <br>
                            <div class="table-responsive">
                                <table class="table table-bordered table-condensed table-striped">
                                    <thead>
                                        <tr>
                                            <th class="text-center" style="width:5px">No</th>
                                            <th class="text-center" >Staff</th>
                                            <th class="text-center col-md-1">Tiket Selesai</th>
                                            <th class="text-center">Rata-rata<br> Response Time</th>
                                            <th class="text-center">Rata-rata<br> Processing Time</th>
                                            <th class="text-center">Rata-rata<br> Solving Time</th>
                                            <th class="text-center col-md-1">%Solvability</th>
                                            <th class="text-center col-md-1">Poin</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php
                                        foreach ($data as $k => $v) {
                                            $no++;
                                                // if ($_SESSION['role_selected']==NULL || $_SESSION['role_selected']==substr(strtoupper(Role::name($v['role'][0])),0,1)) {
                                                    $rt = array();
                                                    $rt['hari'] = floor($v['response_time']/60/24);
                                                    $rt['jam'] = floor($v['response_time']/60)%24;
                                                    $rt['menit'] = $v['response_time']%60;
                                                    $pt = array();
                                                    $pt['hari'] = floor($v['processing_time']/60/24);
                                                    $pt['jam'] = floor($v['processing_time']/60)%24;
                                                    $pt['menit'] = $v['processing_time']%60;
                                                    $st = array();
                                                    $st['hari'] = floor($v['solving_time']/60/24);
                                                    $st['jam'] = floor($v['solving_time']/60)%24;
                                                    $st['menit'] = $v['solving_time']%60;
                                                ?>
                                                    <tr>
                                                        <td class="text-right"><?= $no ?></td>
                                                        <td><a href="<?= site_url('web/user/detail/' . $v['id']) ?>"><img id="img_user" class="img-circle" style="width:30px" src="<?= site_url($v['user_photo']) ?>"  /> <?= $v['name'] ?></a>
                                                        </td>
                                                        <td align="right"><a href ="<?= site_url('web/search/ticketdone').'/'.$v['id'].'/'.'?h='.$chosen_helpdesk.$filter_date ?>"><?= $data_all[$k]['s'] ?>
                                                                    
                                                            </a>
                                                        </td>
                                                        <td align="right" >
                                                            <?php if ($rt['hari']>0) { ?>
                                                                <?= $rt['hari'] ?><i style="font-size:10px;color:red">hari</i>
                                                            <?php } ?>
                                                            <?php if ($rt['jam']>0) { ?>
                                                                <?= $rt['jam'] ?><i style="font-size:10px;color:blue">jam</i>
                                                            <?php } ?>
                                                            <?php if ($rt['menit']>=0) { ?>
                                                                <?= $rt['menit'] ?><i style="font-size:10px;color:green">mnt</i>
                                                            <?php } ?>
                                                        </td>
                                                        <td align="right">
                                                            <?php if ($pt['hari']>0) { ?>
                                                                <?= $pt['hari'] ?><i style="font-size:10px;color:red">hari</i>
                                                            <?php } ?>
                                                            <?php if ($pt['jam']>0) { ?>
                                                                <?= $pt['jam'] ?><i style="font-size:10px;color:blue">jam</i>
                                                            <?php } ?>
                                                            <?php if ($pt['menit']>=0) { ?>
                                                                <?= $pt['menit'] ?><i style="font-size:10px;color:green">mnt</i>
                                                            <?php } ?>
                                                        </td>
                                                        <td align="right">
                                                            <?php if ($st['hari']>0) { ?>
                                                                <?= $st['hari'] ?><i style="font-size:10px;color:red">hari</i>
                                                            <?php } ?>
                                                            <?php if ($st['jam']>0) { ?>
                                                                <?= $st['jam'] ?><i style="font-size:10px;color:blue">jam</i>
                                                            <?php } ?>
                                                            <?php if ($st['menit']>=0) { ?>
                                                                <?= $st['menit'] ?><i style="font-size:10px;color:green">mnt</i>
                                                            <?php } ?>
                                                        </td>
                                                        <td align="right"><?= $v['solvability'] ?><i style="font-size:12px;">%</i></td>
                                                        <td align="right"><?= $v['poin'] ?></td>
                                                        <?php if ($_SESSION['sigap']['auth']['username']=='integra' and 1==0){ ?>
                                                            <td align="right"><?= $v['track'] ?></td>
                                                        <?php } ?>
                                                    </tr>
                                                <?php 
                                                //}
                                            //}
                                        }
                                        ?>
                                        
                                    </tbody>
                                </table>
                            </div>
                        <?php } ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">

    $( function() {
        $("[name='date_started'], [name='date_finished']").datepicker({format:'dd-mm-yyyy'});
    } );

    function dateChange(){
        var fStart = $("[name='date_started'");
        var fEnd = $("[name='date_finished'");

        var aStart = fStart.val().split('-');
        var aEnd = fEnd.val().split('-');
        var dStart = new Date(aStart[2], aStart[1], aStart[0]);
        var dEnd = new Date(aEnd[2], aEnd[1], aEnd[0]);
        if(dStart > dEnd){
            fEnd.val(fStart.val());
        }
    }

    $("#btn-filter").on('click',function(){
        var unit_helpdesk = $("#btn_helpdesk").attr('value');
        if (unit_helpdesk=='Semua Unit')
            unit_helpdesk='';
        if ($("[name='date_finished'").val()!='' && $("[name='date_finished'").val()!='') {
            var helpdesk = $('#btn_helpdesk').attr('value');
            if (helpdesk!='' || helpdesk!='Semua' || helpdesk!='Semua Unit'){
                location.href="<?= site_url('web/statistik/performance') ?>?h="+helpdesk+"&start_date="+$("[name='date_started'").val()+'&end_date='+$("[name='date_finished'").val();
            } else {
                location.href="<?= site_url('web/statistik/performance') ?>/?h="+unit_helpdesk+"&start_date="+$("[name='date_started'").val()+'&end_date='+$("[name='date_finished'").val();
            }
        } else {
            alert('Tanggal mulai atau tanggal selesai tidak boleh kosong!');
        }
        
    });
    $("#btn-showall").on('click',function(){
        location.href="<?= site_url('web/statistik/performance') ?>";
    });

    function findRole1(rad){
        //document.getElementById("radioValue").value = rad;
        $.ajax({
            type: "POST",
            url: site_url("web/user/daftar"),
           data: {findrole:rad}
        });
        //alert(rad);

    }

    $(document).on('click', '.set-helpdesk-dashboard', function(){
        var val = $(this).attr('value');
        var start_date = $("[name='date_started'").val();
        var str_start = '';
        var str_end = '';
        if (start_date!=''){
            str_start = '&start_date='+start_date;
        }
        var end_date = $("[name='date_finished'").val();
        if (end_date!=''){
            str_end = '&end_date='+end_date;
        }
        if (val=="" || val=="Semua") {
            window.location.replace("<?= site_url('web/statistik/performance') ?>/?h="+str_start+str_end);
        } else {
            window.location.replace("<?= site_url('web/statistik/performance') ?>/?h="+val+str_start+str_end);
        }
    }); 
</script>
