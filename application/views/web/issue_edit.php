<div class="row">
    <div class="col-md-12">
        <div class="box box-primary">
            <div class="box-body">
                <div class="row">
                    <div class="col-md-12">
                        <?php echo form_open_multipart($path . $class . '/' . (isset($id) ? 'update/' . $id : 'create'), array('id' => 'form_data')) ?>
                        <div class="row bord-bottom">
                            <label for="project-id" class="col-sm-3">Project</label>
                            <div class="col-sm-9">
                                <?php echo form_dropdown('group_id', $a_project, $data['groupId'], 'id="project-id" class="form-control input-sm"') ?>
                            </div>
                        </div>
                        <div class="row bord-bottom">
                            <label for="post_users" class="col-sm-3">Kirim Ke</label>
                            <div class="col-sm-4">
                                <?php // echo form_multiselect('postUsers[]',$a_user,$data['postUsers'],'id="post_users" size="5" class="form-control"') ?>
                                <div id="div_users" class="site-listbox">
                                    <table>
                                        <tr>
                                            <td colspan="2">
                                                <?php echo form_input(array('class' => 'form-control input-sm', 'id' => 'post_users_filter', 'placeholder' => 'Cari Teman', 'autocomplete' => 'off')) ?>
                                            </td>
                                        </tr>
                                        <?php foreach ($a_user as $k => $v) { ?>
                                            <tr>
                                                <td><?php echo form_checkbox(array('name' => 'postUsers[]', 'id' => 'post_users_' . $k, 'value' => $k, 'checked' => (in_array($k, $data['usersID'])), 'class' => 'simple list-user')) ?></td>
                                                <td><label for="post_users_<?php echo $k ?>" class="labelinput"><?php echo $v ?></label></td>
                                            </tr>
                                        <?php } ?>
                                    </table>
                                </div>
                            </div>
                            <label for="category_id" class="col-sm-2">Kategori</label>
                            <div class="col-sm-3">
                                <?php echo form_dropdown('category_id', $a_kategori, $data['categoryId'], 'id="category_id" class="form-control input-sm"') ?>
                            </div>
                        </div>
                        <div class="row bord-bottom">
                            <label for="date" class="col-sm-3">Tanggal Tugas</label>
                            <div class="col-sm-4">
                                <div class="input-group">
                                    <div class="input-group-addon"><i class="fa fa-calendar"></i></div>
                                    <?php echo form_input(array('name' => 'date', 'value' => (isset($id) ? FormatterWeb::formatDate($data['date']) : date('d-m-Y')), 'class' => 'form-control input-sm', 'placeholder' => 'dd-mm-yyyy')) ?>
                                </div>
                            </div>
                            <label for="priority" class="col-sm-2">Prioritas</label>
                            <div class="col-sm-3">
                                <?php echo form_dropdown('priority', $a_prioritas, $data['priority'], 'id="priority" class="form-control input-sm"') ?>
                            </div>
                        </div>
                        <div class="row bord-bottom">
                            <label for="deadline" class="col-sm-3">Deadline</label>
                            <div class="col-sm-4">
                                <div class="input-group">
                                    <div class="input-group-addon"><i class="fa fa-calendar"></i></div>
                                    <?php echo form_input(array('name' => 'deadline', 'value' => (isset($id) ? FormatterWeb::formatDate($data['deadline']) : date('d-m-Y')), 'class' => 'form-control input-sm', 'placeholder' => 'dd-mm-yyyy')) ?>
                                </div>
                            </div>
                            <label for="status" class="col-sm-2">Status</label>
                            <div class="col-sm-3">
                                <?php echo form_dropdown('status', $a_status, $data['status'], 'id="status" class="form-control input-sm"') ?>
                            </div>
                        </div>
                        <div class="row bord-bottom">
                            <label for="title" class="col-sm-3">Judul Tugas</label>
                            <div class="col-sm-9">
                                <?php echo form_input(array('name' => 'title', 'value' => $data['title'], 'class' => 'form-control input-sm', 'id' => 'title')) ?>
                            </div>
                        </div>
                        <div class="row bord-bottom">
                            <div class="col-xs-12">
                                <?php echo form_textarea(array('name' => 'description', 'value' => $data['description'], 'class' => 'form-control input-sm', 'rows' => 10, 'placeholder' => 'Tulis deskripsi tugas disini')) ?>
                            </div>
                        </div>
                        <div class="row bord-bottom">
                            <label for="reportter" class="col-sm-3">Nama Reporter</label>
                            <div class="col-sm-9">
                                <?php echo form_input(array('name' => 'reportter', 'value' => $data['reportter'], 'class' => 'form-control input-sm', 'id' => 'reportter')) ?>
                            </div>
                        </div>
                        <div class="row bord-bottom">
                            <label for="reporter_email" class="col-sm-3">Email Reporter</label>
                            <div class="col-sm-9">
                                <?php echo form_input(array('name' => 'reporter_email', 'value' => $data['reporter_email'], 'class' => 'form-control input-sm', 'id' => 'reporter_email')) ?>
                            </div>
                        </div>
                        <div class="row bord-bottom">
                            <label for="reporter_phone" class="col-sm-3">Telp Reporter</label>
                            <div class="col-sm-9">
                                <?php echo form_input(array('name' => 'reporter_phone', 'value' => $data['reporter_phone'], 'class' => 'form-control input-sm', 'id' => 'reporter_phone')) ?>
                            </div>
                        </div>
                        <div class="row bord-bottom">
                            <label for="link" class="col-sm-3">Tautan</label>
                            <div class="col-sm-9">
                                <?php echo form_input(array('name' => 'link', 'value' => $data['link'], 'class' => 'form-control input-sm', 'id' => 'link')) ?>
                            </div>
                        </div>
                        <div class="row bord-bottom">
                            <label for="image" class="col-sm-3">Image</label>
                            <div class="col-sm-6">
                                <?php echo form_upload(array('name' => 'image', 'id' => 'image')) ?>
                            </div>
                        </div>
                        <div class="row bord-bottom">
                            <label for="video" class="col-sm-3">Video</label>
                            <div class="col-sm-6">
                                <?php echo form_upload(array('name' => 'video', 'id' => 'video')) ?>
                            </div>
                        </div>
                        <div class="row bord-bottom">
                            <label for="file" class="col-sm-3">File</label>
                            <div class="col-sm-6">
                                <?php echo form_upload(array('name' => 'file', 'id' => 'file')) ?>
                            </div>
                        </div>
                        <?php echo form_hidden('referer') ?>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">

    var back = sessionStorage.getItem("<?php echo $class ?>.back");

    $(function () {
        $("[name='date'],[name='deadline']").datepicker({format: 'dd-mm-yyyy'});
        $("[name='referer']").val(back);

        $("#post_users_filter").keyup(function () {
            var str = $(this).val();

            $("#div_users tr:hidden").show();
            if (str != "") {
                $(".labelinput").each(function () {
                    if ($(this).html().toLowerCase().search(str.toLowerCase()) == -1)
                        $(this).parents("tr:eq(0)").hide();
                });
            }
        });

        /*$("#project-id").change(function () {
            $('.list-user').attr('checked', false);
            if ($(this).val() != "") {
                $.get("<?= site_url('web/group/group_member'); ?>/" + $(this).val(), function (data) {
                    user = JSON.parse(data);
                    for (i = 0; i < user.length; i++) {
                        $('#post_users_' + user[i]).attr('checked', true);
                    }
                });
            }
        });*/

    });

    function goBack() {
        if (back)
            location.href = back;
        else
            location.href = "<?php echo site_url($path . $class) ?>";
    }

    function goSave() {
        $("#form_data").submit();
    }
</script>