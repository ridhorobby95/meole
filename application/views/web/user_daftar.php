<div class="row">
    <div class="col-md-12">        
        <div class="box box-primary">
            <div class="box-body">
                <div class="row">
                    <div class="col-md-12">
                        <h2><?= $title ?></h2><hr>
                        <form action="<?= site_url('web/user/daftar') ?>" class="search-form" style="position:relative ;width:600px;bottom:15px">
                            <div class="form-group has-feedback">
                                <label for="search" class="sr-only">Search</label>
                                <input type="text" class="form-control" name="search" id="search" placeholder="search" value="<?= $_GET['search'] ?>">
                                <span class="glyphicon glyphicon-search form-control-feedback"></span>
                            </div>       
                        </form>
                        <a href="<?= site_url('web/user/daftar') ?>?r=All">
                            <?php 
                                if ($_SESSION['role_selected']==NULL) {
                                    echo '<span class="btn btn-xs btn-primary"> Semua </span>';
                                } else {
                                    echo '<span class="btn btn-xs btn-default"> Semua </span>';
                                }
                            ?>
                        </a>
                        <?php if (1==9 and SessionManagerWeb::isSuperAdministrator()) { ?>
                        <a href="<?= site_url('web/user/daftar') ?>?r=SUPERADMINISTRATOR">
                            <?php 
                                if ($_SESSION['role_selected']=='R') {
                                    echo '<span class="btn btn-xs btn-primary"> Super Administrator </span>';
                                } else {
                                    echo '<span class="btn btn-xs btn-default"> Super Administrator </span>';
                                }
                            ?>
                        </a>
                        <?php } ?>
                        <a href="<?= site_url('web/user/daftar') ?>?r=ADMINISTRATOR">
                            <?php 
                                if ($_SESSION['role_selected']=='A') {
                                    echo '<span class="btn btn-xs btn-primary"> Administrator </span>';
                                } else {
                                    echo '<span class="btn btn-xs btn-default"> Administrator </span>';
                                }
                            ?>
                        </a>
                        <?php if(1==9) { ?>
                        <a href="<?= site_url('web/user/daftar') ?>?r=STAFF">
                            <?php 
                                if ($_SESSION['role_selected']=='S') {
                                    echo '<span class="btn btn-xs btn-primary"> Staff </span>';
                                } else {
                                    echo '<span class="btn btn-xs btn-default"> Staff </span>';
                                }
                            ?>
                        </a>
                        <?php } ?>
                        <a href="<?= site_url('web/user/daftar') ?>?r=NONOPERATOR">
                            <?php 
                                if ($_SESSION['role_selected']=='N') {
                                    echo '<span class="btn btn-xs btn-primary"> Non Operator </span>';
                                } else {
                                    echo '<span class="btn btn-xs btn-default"> Non Operator </span>';
                                }
                            ?>
                        </a>
                        <a href="<?= site_url('web/user/daftar') ?>?r=OPERATOR">
                            <?php 
                                if ($_SESSION['role_selected']=='O') {
                                    echo '<span class="btn btn-xs btn-primary"> Operator </span>';
                                } else {
                                    echo '<span class="btn btn-xs btn-default"> Operator </span>';
                                }
                            ?>
                        </a>
                        <a href="<?= site_url('web/user/daftar') ?>?r=WARGA">
                            <?php 
                                if ($_SESSION['role_selected']=='W') {
                                    echo '<span class="btn btn-xs btn-primary"> Warga </span>';
                                } else {
                                    echo '<span class="btn btn-xs btn-default"> Warga </span>';
                                }
                            ?>
                        </a>
                        <br>
                        <br>
                        <div class="row-fluid hidden" style="margin-bottom: 5px"><a href="<?= site_url('web/user/getstaff') ?>" class="btn btn-xs btn-primary"><i class="glyphicon glyphicon-download"></i> Ambil Staff</a>
                        <a href="<?= site_url('web/user/resync_all') ?>" class="btn btn-xs btn-warning hidden" style="margin-left: 10px"><i class="glyphicon glyphicon-refresh"></i> Sinkronkan</a></div>
        
                        <div class="table-responsive">
                            <table class="table table-bordered table-condensed table-striped">
                                <thead>
                                    <tr>
                                        <th class="text-center" style="width: 30px">No</th>
                                        <th class="text-center col-md-3">Username</th>
                                        <th class="text-center">Nama</th>
                                        <th class="text-center col-md-2">Role</th>
                                        <?php if($_SESSION['role_selected']!='W' and $_SESSION['role_selected']!='A') {?>
                                        <th class="text-center col-md-3">Institusi</th>
                                        <?php } ?>

                                        <?php if (($_SESSION['role_selected']=='S' or $_SESSION['role_selected']=='A' or $_SESSION['role_selected']=='R') and 1==9) { ?>
                                            <?php if ($_SESSION['role_selected']!='R') { ?>
                                                <th class="text-center col-md-1">Helpdesk</th>
                                            <?php } ?>
                                            <th class="text-center col-md-1">Handle Tiket</th>
                                        <?php } ?>
                                        <?php if ($_SESSION['role_selected']=='O' and 1==2) { ?>
                                            <th class="text-center col-md-1">Banned</th>
                                        <?php } 
                                            if(1==9){
                                        ?>            
                                        <th class="text-center">&nbsp;Aksi</th>
                                        <?php } ?>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php
                                    
                                    // $counter = 0;
                                    // $total_view = 20;
                                    // $limit = $total_view;
                                    // $no_view=($page*$total_view);
                                    // $view_max = ($page+1)*$total_view;
                                    // $view_min = ($page*$total_view)+1;
                                    $page = $this->uri->segment(4);
                                    $no = ($page > 1) ? ($page * $limit - $limit + 1) : 1;
                                    foreach ($data as $v) {
                                        // if ($counter>=$view_min && $limit>0) {
                                        //     $no=$counter;

                                            //echo $_POST['findrole'];
                                            // if ($radio==strtoupper(Role::name($v['role'])) || $radio=="ALL") {
                                        if ($_SESSION['role_selected']==NULL || $_SESSION['role_selected']==substr(strtoupper(Role::name($v['role'][0])),0,1) || $v['role']=='R') {
                                                // $no_view++;
                                                // $limit=$limit-1;
                                            ?>
                                                <tr>
                                                    <td class="text-right"><?= $no ?></td>
                                                    <td><a href="<?= site_url('web/user/detail/' . $v['id']) ?>"><img id="img_user" class="img-circle" style="width:30px" src="<?= site_url($v['user_photo']) ?>"  /> <?= $v['username'] ?></a></td>
                                                    <td><a href="<?= site_url('web/user/detail/' . $v['id']) ?>"><?= $v['name'] ?></a></td>
                                                    <td>
                                                        <? // (SessionManagerWeb::isSuperAdministrator()) ? strtoupper(Role::name($v['role'])) : $v['role'] ?>
                                                        <?= (SessionManagerWeb::isAdministrator()) ? strtoupper(Role::name($v['role'])) : $v['role'] ?>
                                                            
                                                        </td>
                                                    <?php if($_SESSION['role_selected']!='W' and $_SESSION['role_selected']!='A') {?>
                                                    <td><?= $v['nm_lemb'] ?></td>
                                                    <?php } ?>

                                                    <?php if ($_SESSION['role_selected']!='R' and $_SESSION['role_selected']!='O' and 1==9) { ?>
                                                    <td><?= $v['helpdesk_name']  ?></td>
                                                    <?php } ?>

                                                    <?php if (($_SESSION['role_selected']=='S' or $_SESSION['role_selected']=='A' || $_SESSION['role_selected']=='R') and 1==9) { ?>
                                                        <td style="text-align: center">
                                                        <?php if ($v['is_excluded']) { ?>
                                                            <a href="<?= site_url('web/user/setexclude/' . $v['id'].'/'.$v['is_excluded']) ?>"><i class="glyphicon glyphicon-unchecked" style="color:red"></i></a>
                                                        <?php } else { ?>
                                                            <a href="<?= site_url('web/user/setexclude/' . $v['id'].'/'.$v['is_excluded']) ?>"><i class="glyphicon glyphicon-check"></i></a>
                                                        <?php } ?>
                                                        
                                                        </td>
                                                    <?php } ?>

                                                    <?php if ($_SESSION['role_selected']=='O' and 1==2) { ?>
                                                        <td style="text-align: center">
                                                        <?php if ($v['is_banned']==0) { ?>
                                                            <a href="<?= site_url('web/user/banUser/' . $v['id'].'/'.$v['is_banned']) ?>"><i class="glyphicon glyphicon-unchecked" style="color:green"></i></a>
                                                        <?php } else { ?>
                                                            <a href="<?= site_url('web/user/banUser/' . $v['id'].'/'.$v['is_banned']) ?>"><i class="glyphicon glyphicon-check" style="color:red"></i></a>
                                                        <?php } ?>
                                                        
                                                        </td>
                                                    <?php } 
                                                        if (1==9) {
                                                    ?>                                                    
                                                    <td>
                                                        <a href="<?= site_url('web/user/resync/' . $v['id']) ?>" title="Refresh"><i class="glyphicon glyphicon-refresh"></i></a>
                                                        <?php if (in_array($v['role'], array('S', 'A', 'R'))) { ?>
                                                            <a href="<?= site_url('web/user/spreadTicket/' . $v['id']) ?>" title="Sebarkan Tiket" style="margin-left: 5px" ><i class="glyphicon glyphicon-random"></i></a>
                                                        <?php } ?>
                                                        <?php if (in_array($v['role'], array('S', 'A', 'R'))) { ?>
                                                            <a onclick="lanjutkan('<?= $v['id'] ?>')" title="Nonaktifkan Staff" style="margin-left: 5px;color:red;cursor:pointer" ><i class="glyphicon glyphicon-off"></i></a>
                                                        <?php } ?>
                                                        
                                                    </td>
                                                    <?php } ?>
                                                </tr>
                                            <?php 
                                            //}
                                        }
                                        $no++;
                                    }
                                    ?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>

                <nav>
                    <div class="text-center">
                        <ul class="pagination">
                            <?php
                            $page = $this->uri->segment(4);
                            if (empty($page) || $page == 0) {
                                echo '<li class="disabled"><a href = "#">Sebelumnya</a></li>';
                            } else {
                                echo '<li>' . anchor('web/' . $this->router->class . '/' . $this->router->method . '/' . ($page - 1) . Util::paramsToUrlGet($_GET), 'Sebelumnya') . '</li>';
                            }
                            if ($data[0]['user_offset']==0) {
                                echo '<li class="disabled"><a href = "#">Selanjutnya</a></li>';
                            } else {
                                echo '<li>' . anchor('web/' . $this->router->class . '/' . $this->router->method . '/' . ($page + 1) . Util::paramsToUrlGet($_GET), 'Selanjutnya') . '</li>';
                            }
                            ?>
                        </ul>
                    </div>
                    <!-- <ul class="pager">
                        <?php
                        
                        if (empty($page) || $page == 0) {
                            echo '<li class="disabled"><a href = "#">Sebelumnya</a></li>';
                        } else {
                            echo '<li>' . anchor('web/' . $this->router->class . '/' . $this->router->method . '/' . ($page - 1) . Util::paramsToUrlGet($_GET), 'Sebelumnya') . '</li>';
                        }
                        #if (empty($data)) {
                        if ($_SESSION['user_offset']==0){
                            echo '<li class="disabled"><a href = "#">Selanjutnya</a></li>';
                        
                        } else {
                            echo '<li>' . anchor('web/' . $this->router->class . '/' . $this->router->method . '/' . ($page + 1) . Util::paramsToUrlGet($_GET), 'Selanjutnya') . '</li>';
                        }
                        ?>
                    </ul> -->
                </nav>

            </div>
        </div>
    </div>
</div>

<!--  MODAL   -->
<div class="modal fade" id="user_nonactive_modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog" style="max-width: 400px;top:15%;">
        <div class="modal-content" style="background-color:#ffffce">
            <div class="modal-header">
                <center><h4 class="modal-title" id="myModalLabel"><strong style="color:red">Perhatian !</strong></h4></center>
            </div>
            <div class="modal-body" style="max-height:200px;padding-left:30px">
                <div class="row">
                    <label style="text-align: center">
                        Anda akan menonaktifkan staff ini.
                        Staff yang dinonaktifkan akan dikeluarkan dari helpdesk, tidak menghandle tiket lagi, serta tiket-tiket sebelumnya akan disebarkan ke anggota helpdesk lainnya.
                    </label>
                </div>
            </div>
            <div class="modal-footer">
                <div class="text-center">
                    <input type="text" value="" id="user_id_modal" class="hidden">
                    <span class="btn btn-sm btn-primary" onclick="next()" data-dismiss="modal">Lanjutkan</span>
                    <span class="btn btn-sm btn-default" data-dismiss="modal"> Batal</span>
                </div>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">

    function lanjutkan(user_id=null){
        document.getElementById('user_id_modal').value = user_id;
        $('#user_nonactive_modal').modal('toggle');
    }

    function next(){
        var user_id = document.getElementById('user_id_modal').value;
        $.ajax({
            url : "<?= site_url('/web/user/ajaxDisableStaff');?>",
            type: 'POST',
            data: {'user_id':user_id},
            success:function(result){
                if (result==false){
                    alert("Staff ini tidak dapat dinonaktifkan karena tidak ada staff lain pada Helpdesknya.");
                } else {
                    alert("Sukses menonaktifkan staff");
                    location.reload();
                }
                
                
            },
            error:function(){
                alert('Gagal menonaktifkan staff ini. Silahkan coba beberapa saat lagi atau Hubungi Administrator.');
            }
        });
    }

    function findRole1(rad){
        //document.getElementById("radioValue").value = rad;
        $.ajax({
            type: "POST",
            url: site_url("web/user/daftar"),
           data: {findrole:rad}
        });
        //alert(rad);

    }
</script>