<div class="row" id="main-container">
    <div class="col-sm-12 col-md-10" style="padding:0">
        <div class="panel panel-content">
            <div class="panel-heading">
                <h4 style="margin:0; font-weight:bold;font-size:26px">Group</h4>
            </div>
            <div class="panel-body menu-grid">

                <div class="col-xs-6 col-md-4">
                    <div class="thumbnail">
                        <a href="<?= site_url('web/group/daftar_group') ?>">
                        <h1><i class="fa fa-users"></i></h1>
                        <div class="caption">
                            Group
                        </div>
                        </a>
                    </div>
                </div>

                <div class="col-xs-6 col-md-4">
                    <div class="thumbnail">
                        <a href="<?= site_url('web/group/daftar_helpdesk') ?>">
                        <h1><i class="glyphicon glyphicon-calendar"></i></h1>
                        <div class="caption">
                            Helpdesk
                        </div>
                        </a>
                    </div>
                </div>
            </div>

        </div>
    </div>
</div>
