<div class="row">
    <div class="col-md-12">
        <div class="box box-primary">
            <div class="box-body">
                <div class="row">
                    <?php echo form_open_multipart($path . $class . '/' . (isset($id) ? 'update/' . $id : 'create'), array('id' => 'form_data')) ?>
                    <div class="col-sm-8">
                        <div class="row bord-bottom">
                            <label class="col-sm-4">Username</label>
                            <div class="col-sm-8">
                                <?php echo form_input(array('name' => 'username', 'value' => $data['username'], 'class' => 'form-control input-sm', 'id' => 'username')) ?>
                            </div>
                        </div>
                        <div class="row bord-bottom">
                            <label for="name" class="col-sm-4">Nama</label>
                            <div class="col-sm-8">
                                <?php echo form_input(array('name' => 'name', 'value' => $data['name'], 'class' => 'form-control input-sm', 'id' => 'name')) ?>
                            </div>
                        </div>

                    </div>
                    <div class="col-sm-4 text-center">
                        <div class="row">
                            <label class="col-sm-12">Foto Profil</label>
                        </div>
                        <div class="row">
                            <div class="col-sm-12">
                                <div class="site-photo site-photo-128 site-photo-center">
                                    <img id="img_user" src="<?php echo isset($data['photo']) ? site_url($path . 'thumb/watermark/' . $data['id'] . '/128') : $nopic ?>" data-toggle="tooltip" title="Upload Foto" />
                                </div>
                                <label id="label_user" class="label label-info hidden">Simpan profil untuk mengupload foto</label>
                                <?php echo form_upload(array('name' => 'photo', 'id' => 'photo', 'class' => 'hidden')) ?>
                            </div>
                        </div>
                    </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>

<?php echo form_open_multipart($path . $class . '/change_password', array('id' => 'form_modal')) ?>
<div class="modal fade" id="modal_form" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-sm">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Ganti Password</h4>
            </div>
            <div class="modal-body">
                <div class="row">
                    <label class="col-md-4">Sekarang</label>
                    <div class="col-md-8">
                        <?php echo form_password(array('name' => 'oldPassword', 'class' => 'form-control', 'placeholder' => 'Password Sekarang')) ?>
                    </div>
                </div>
                <div class="vspace"></div>
                <div class="row">
                    <label class="col-md-4">Baru</label>
                    <div class="col-md-8">
                        <?php echo form_password(array('name' => 'newPassword', 'class' => 'form-control', 'placeholder' => 'Password Baru', 'id' => 'newPassword')) ?>
                    </div>
                </div>
                <div class="vspace"></div>
                <div class="row">
                    <label class="col-md-4">Ulangi</label>
                    <div class="col-md-8">
                        <?php echo form_password(array('name' => 'confirmPassword', 'class' => 'form-control', 'placeholder' => 'Ulangi Password Baru', 'id' => 'confirmPassword')) ?>
                    </div>
                </div>
            </div>
            <div class="modal-footer" align="center">
                <button type="submit" class="btn btn-success"><i class="fa fa-check"></i> Ganti</button>
                <button type="button" class="btn btn-primary" data-dismiss="modal"><i class="fa fa-times"></i> Batal</button>
            </div>
        </div>
    </div>
</div>
</form>

<script type="text/javascript">

    $(function () {
        $("[name='rank_date'],[name='birth_date']").datepicker({format: 'dd-mm-yyyy'});
        $("#img_user").css("cursor", "pointer").click(function () {
            $("#photo").click();
        });
        $("#photo").change(function () {
            $("#label_user").removeClass("hidden");
        });
        $("#form_modal").submit(function () {
            // password baru dan konfirmasinya harus sama
            if ($("#newPassword").val() != $("#confirmPassword").val()) {
                alert("Password Baru dan pengulangannya tidak sama");
                return false;
            }
        });
        $("#daily_report_users_filter").keyup(function () {
            var str = $(this).val();

            $("#div_users tr:hidden").show();
            if (str != "") {
                $(".labelinput").each(function () {
                    if ($(this).html().toLowerCase().search(str.toLowerCase()) == -1)
                        $(this).parents("tr:eq(0)").hide();
                });
            }
        });
        $("#div_users [type='checkbox']").change(function () {
            value = $(this).attr("value");
            label = $("#div_selected [data-id='" + value + "']");
            check = $(this).prop("checked");
            if (check) {
                if (label.length == 0)
                    $("#div_selected").append('<div data-id="' + value + '">' + $("label[for='" + $(this).attr("id") + "']").text() + '</div>');
            } else {
                if (label.length != 0)
                    label.remove();
            }
        });
    });

    function goBack() {
        location.href = "<?php echo site_url($path . $class) ?>";
    }

    function goSave() {
        $("#form_data").submit();
    }

    function goChangePass() {
        $("#modal_form").modal();
    }

</script>