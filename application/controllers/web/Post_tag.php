<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Post_tag extends WEB_Controller {

    protected $title = 'Post Tag';

    function __construct() {
        parent::__construct();
        $this->load->helper('text');
        $this->load->model("Post_model");
        $this->load->model("User_model");
        $this->load->model("Group_model");
    }

    public function index(){
        redirect('web/post');
    }

    /**
     * Halaman daftar post public
     * @param int $page
     */
    public function search($page=0) {
        unset($_SESSION['group_selected']);
        unset($_SESSION['home_status']);
        $_SESSION['home_status']['dummy'] = 1;
        $this->data['buttons'] = $buttons;
        $this->data['title'] = 'Lini Masa';
        $helpdesk = $this->Group_model->getGroupBy('name',$_SESSION['helpdesk_selected']);

        $this->load->model("Search_log_model");
        $log_data = array(  "keyword" => $this->input->get('search', true),
                            "user_id" => SessionManagerWeb::getUserID());
        $insert = $this->Search_log_model->insert($log_data, TRUE, FALSE);

        $tag_param = $this->input->get('search',true);

        // check tags
        $post_tag_ids = $this->Post_model->checkPostTag($tag_param);
        $arr_post_ids = array();
        foreach ($post_tag_ids as $post_tag_id) {
            $arr_post_ids[] = $post_tag_id['post_id'];
        }
        $post_ids = implode(',', $arr_post_ids);

        $posts = $this->Post_model->show_sql(false)
                            ->filter("and p.id in ($post_ids)")
                            ->getPosts(SessionManagerWeb::getUserID(),$page, 'filter_for_tag_purpose12tiga',$_SESSION['home_status'],$helpdesk['id']);

        foreach ($posts as $k_post => $post) {
            foreach ($post['tags'] as $k_tag => $tag) {
                $str = $tag_param;
                $temp='';
                $p_tag = explode($str,$tag['name']);
                $temp=implode("<span style='color:yellow'>".$str."</span>",$p_tag);
                unset($posts[$k_post]['tags'][$k_tag]['name']);
                $posts[$k_post]['tags'][$k_tag]['name'] = $temp;
            }
        }

        $this->data['data'] = $posts;
        $t1 = array();
        $t2 = array();
        $t3 = array();
        $t4 = array();
        $t5 = array();
        $counter = array();
        $counter['knowledge'] = 0;
        $counter['group'] = 0;
        $counter['ticket'] = 0;
        $counter['informasi'] = 0;
        $counter['umum'] = 0;
        for ($i=0;$i<5;$i++){
            foreach ($this->data['data'] as $dat) {
                if ($i==0) {
                    if ($dat['post_type']=='T') {
                        if (SessionManagerWeb::isStaff() || $this->Group_member_model->_isAdminGroup(SessionManagerWeb::getUserID())){
                            array_push($t1 ,$dat);
                            $counter['ticket']++;
                        } else {
                            if ($dat['post_user_id']==SessionManagerWeb::getUserId()){
                                array_push($t1 ,$dat);
                                $counter['ticket']++;
                            }
                        }
                        
                    }
                }
                if ($i==1){
                    if ($dat['group_name']=='Umum') {
                        array_push($t2 ,$dat);
                        $counter['umum']++;
                    }
                }
                if ($i==2){
                    if ($dat['post_type']=='G' and $dat['group_name']!='HELPDESK' and $dat['group_name']!='Umum') {
                        array_push($t3 ,$dat);
                        $counter['group']++;
                    }
                }
                if ($i==3){
                    if ($dat['post_type']=='K') {
                        // $dat['group_name']='Knowledge';
                        array_push($t4 ,$dat);
                        $counter['knowledge']++;
                    }
                }
                if ($i==4){
                    if ($dat['post_type']=='I') {
                        // $dat['group_name']='Informasi';
                        array_push($t5 ,$dat);
                        $counter['informasi']++;
                    }
                }
            }
        }
        $this->data['counter']=$counter;
        $this->data['ticket']=$t1;
        $this->data['umum']=$t2;
        $this->data['group']=$t3;
        $this->data['knowledge']=$t4;
        $this->data['informasi']=$t5;
        $this->data['by_tag'] = true;
        unset($this->data['data']);

        $this->data['list_staff'] = $this->User_model->getListStaff(SessionManagerWeb::getUserID());

        $this->template->viewDefault('search_index', $this->data);    
    }

    /**
     * Halaman daftar post public
     * @param string $page
     */
    public function detail($place) {
        unset($_SESSION['group_selected']);
        unset($_SESSION['home_status']);
        $_SESSION['home_status']['dummy'] = 1;
        $this->data['buttons'] = $buttons;
        $this->data['title'] = 'Lini Masa';

        $tag_param = $this->input->get('search',true);

        // check tags
        $post_tag_ids = $this->Post_model->checkPostTag($tag_param);
        $arr_post_ids = array();
        foreach ($post_tag_ids as $post_tag_id) {
            $arr_post_ids[] = $post_tag_id['post_id'];
        }
        $post_ids = implode(',', $arr_post_ids);

        $posts = $this->Post_model->show_sql(false)
                            ->filter("and p.id in ($post_ids)")
                            ->getPosts(SessionManagerWeb::getUserID(),$page, 'filter_for_tag_purpose12tiga',$_SESSION['home_status'],$helpdesk['id']);

        foreach ($posts as $k_post => $post) {
            foreach ($post['tags'] as $k_tag => $tag) {
                $str = $tag_param;
                $temp='';
                $p_tag = explode($str,$tag['name']);

                $temp=implode("<span style='color:yellow'>".$str."</span>",$p_tag);
                unset($posts[$k_post]['tags'][$k_tag]['name']);
                $posts[$k_post]['tags'][$k_tag]['name'] = $temp;
            }

        }

        $this->data['data'] = $posts;
        $t1 = array();
        $t2 = array();
        $t3 = array();
        $t4 = array();
        $t5 = array();
        for ($i=0;$i<5;$i++){
            foreach ($this->data['data'] as $dat) {
                if ($i==0) {
                    if ($dat['post_type']=='T') {
                        array_push($t1 ,$dat);
                    }
                }
                if ($i==1){
                    if ($dat['group_name']=='Umum') {
                        array_push($t2 ,$dat);
                    }
                }
                if ($i==2){
                    if ($dat['post_type']=='G' and $dat['group_name']!='HELPDESK' and $dat['group_name']!='Umum') {
                        array_push($t3 ,$dat);
                    }
                }
                if ($i==3){
                    if ($dat['post_type']=='K') {
                        $dat['group_name']='Knowledge';
                        array_push($t4 ,$dat);
                    }
                }
                if ($i==4){
                    if ($dat['post_type']=='I') {
                        $dat['group_name']='Informasi';
                        array_push($t5 ,$dat);
                    }
                }
            }
        }
        unset($this->data['data']);
        $detail = 'post';
        if ($place=="ticket"){
            $this->data['data']=$t1;
            $link = 'post';
        } else if ($place=="umum"){
            $this->data['data']=$t2;
            $detail = 'post';
        } else if ($place=="group"){
            $this->data['data']=$t3;
            $detail = 'post';
        } else if ($place=="knowledge"){
            $this->data['data']=$t4;
            $detail = 'knowledge';
        } else {
            $this->data['data']=$t5;
            $detail = 'informasi';
        }
        $this->data['detail'] = $detail;

        $this->data['list_staff'] = $this->User_model->getListStaff(SessionManagerWeb::getUserID());
        $this->template->viewDefault('search_detail', $this->data);
    }

}
