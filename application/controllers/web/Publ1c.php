<?php

# untuk keperluan public tanpa membutuhkan login

class Publ1c extends CI_Controller
{
	// const LIB_PATH = 'web/';
	// const dbhost=;
	// const dbuser=;
	// const dbpass=;
	private $uri_segments = array();
    public function __construct() {
        parent::__construct();

    }
    
    public function index() {
        die();
    }


    function checkDB(){
    	$default = $this->load->database('default', true);
    	$upload = $this->load->database('upload', true);
    	$temp = $this->load->database('temp', true);
    	echo '<pre>';
    	$sql = "select * from users limit 100 offset 0";
    	var_dump(dbGetRows($sql, $default));
    	// var_dump($default);
    	// var_dump($upload);
    	// var_dump($temp);
    	die();
    }


    function syncUser(){
    	$persuratan = $this->load->database('persuratan', true);
    	$listUser = dbGetRows("select * from users", $persuratan);
    	// echo "<pre>";print_r($listUser);die();
    	foreach ($listUser as $user) {
    		if($user['iduser'] != 1){
    			// table user
    			$checkUser = dbGetOne("select id from users where id_user_persuratan=".$user['iduser']);
    			if(!$checkUser){
    				echo $user['nip']."<br>";
    				$dataInsert = array(
    					'name'			=> $user['nama'],
    					'username'		=> $user['username'],
    					'password'		=> $user['password'],
    					'password_salt'	=> $user['salt'],
    					'is_active'		=> 1,
    					'photo'			=> null,
    					'role'			=> Role::OPERATOR,
    					'updated_at'    => date("Y-m-d H:i:s"),
    					'is_excluded'	=> 0,
    					'email'			=> $user['email'],
    					'no_hp'			=> $user['nohp'],
    					'id_user_persuratan' => $user['iduser'],
    					'nip'			=> $user['nip']
    				);
    				$insert = dbInsert("users", $dataInsert);
    			}else{
    				$dataUpdate = array(
    					'name'			=> $user['nama'],
    					'username'		=> $user['username'],
    					'password'		=> $user['password'],
    					'password_salt'	=> $user['salt'],
    					'is_active'		=> 1,
    					'photo'			=> null,
    					'role'			=> Role::OPERATOR,
    					'updated_at'    => date("Y-m-d H:i:s"),
    					'is_excluded'	=> 0,
    					'email'			=> $user['email'],
    					'no_hp'			=> $user['nohp'],
    					'nip'			=> $user['nip']
    				);
    				$insert = dbUpdate("users", $dataUpdate, "id_user_persuratan=".$user['iduser']);
    			}

    		}
    	}
    }

    function syncUserJabatan(){
    	$persuratan = $this->load->database('persuratan', true);
    	$getUserJabatan = dbGetRows("select * from userrole", $persuratan);
    	foreach ($getUserJabatan as $key => $value) {
    		echo "<pre>";print_r($value);echo "<pre>";
    		$getUser = dbGetOne("select id from users where id_user_persuratan=".$value['iduser']);
    		if($getUser){ // jika usernya ada
    			$getJabatan = dbGetOne("select idjabatan from jabatan where idjabatan_persuratan=".$value['idjabatan']);
    			if($getJabatan){ // jika jabatannya ada
    				$getUserJabatan = dbGetOne("select id from user_jabatan where user_id=".$getUser." jabatan_id=".$getJabatan);
    				if(!$getUserJabatan){ //jika data user jabatan tidak ada, maka di insertkan
    					$dataInsert = array(
    						'user_id'	=> $getUser,
    						'jabatan_id'=> $getJabatan
    					);
    					$insert = dbInsert("user_jabatan", $dataInsert);
    					echo "berhasil<br>";
    				}else{
    					echo "gagal3<br>";
    				}
    			}else{
    				echo "gagal2<br>";
    			}
    		}else{
    			echo "gagal1<br>";
    		}

    	}
    	// echo "<pre>";print_r($getUserJabatan);die();
    }

    function setOperator(){
    	$getUserJabatan = dbGetRows("select uj.id,u.*, j.idjabatan, uk.idunit, uk.level as level,uk.iddinas as id_dinas
    								 from user_jabatan uj
    								 left join users u on uj.user_id = u.id
    								 left join jabatan j on uj.jabatan_id = j.idjabatan
    								 left join unitkerja uk on j.idunit = uk.idunit


    		where u.is_excluded=0");
    	foreach ($getUserJabatan as $uj) {
    		if($uj['level'] == 1 || $uj['level'] == 2){
    			$updateRole = dbUpdate("users", array('role' => Role::NONOPERATOR, 'id_dinas' => $uj['id_dinas']), "id=".$uj['id']);
    		}else{
    			$updateRole = dbUpdate("users", array('id_dinas' => $uj['id_dinas']), "id=".$uj['id']);
    		}
    	}

    }

    function getDinas($idunit){
    	$persuratan = $this->load->database('persuratan', true);
    	$getDetailUnit = dbGetRow("select idunit,idparent, level from unitkerja where idunit=".$idunit, $persuratan);
    	if($getDetailUnit['level'] == 2){
    		return $idunit;
    	}
    	elseif($getDetailUnit['level'] == 1 || !$getDetailUnit ){
    		return null;
    	}
    	else{
    		return self::getDinas($getDetailUnit['idparent']);
    	}
    }
    function syncPersuratan(){
    	$persuratan = $this->load->database('persuratan', true);

    	// $sql = "select * from unitkerja";
    	// $data = dbGetRows($sql, $persuratan);
    	// echo "<h1><b>unitkerja</b></h1></br>";
    	// foreach ($data as $key => $value) {
    	// 	$is_exist = dbGetOne("select 1 from unitkerja where idunit=".$value['idunit']);
    	// 	echo $value['nama']."<br>";
    	// 	if($is_exist){
    	// 		$dataUpdate = array(
	    // 			'idparent'	=> $value['idparent'],
	    // 			'kodeunit'	=> $value['kodeunit'],
	    // 			'name'		=> $value['nama'],
	    // 			'singkatan'	=> $value['singkatan'],
	    // 			'isactive'	=> $value['isactive'],
	    // 			'updated_at'=> Util::timeNow(),
	    // 			'urutan'	=> $value['urutan'],
	    // 			'level'		=> $value['level'],
	    // 		);
	    // 		$update = dbUpdate('unitkerja', $dataUpdate, " idunit=".$value['idunit']);
	    // 		echo "update ";
	    // 		if($update){
	    // 			echo "berhasil<br>";
	    // 		}else{
	    // 			echo "gagal<br>";
	    // 		}
    	// 	}else{
    	// 		$dataInsert = array(
	    // 			'idunit'	=> $value['idunit'],
	    // 			'idparent'	=> $value['idparent'],
	    // 			'kodeunit'	=> $value['kodeunit'],
	    // 			'name'		=> $value['nama'],
	    // 			'singkatan'	=> $value['singkatan'],
	    // 			'isactive'	=> $value['isactive'],
	    // 			'created_at'=> Util::timeNow(),
	    // 			'updated_at'=> Util::timeNow(),
	    // 			'urutan'	=> $value['urutan'],
	    // 			'level'		=> $value['level'],
	    // 			'idunit_persuratan'	=> $value['idunit'],
	    // 			'iddinas'	=> self::getDinas($value['idunit'])
	    // 		);
	    // 		$insert= dbInsert('unitkerja', $dataInsert);
	    // 		echo "insert ";
	    // 		if($insert){
	    // 			echo "berhasil<br>";
	    // 		}else{
	    // 			echo "gagal<br>";
	    // 		}
    	// 	}	
    	// }

    	
    	$sql = "select * from jabatan where idjabatan!=1";
    	$data = dbGetRows($sql, $persuratan);
    	echo "<h1><b>jabatan</b></h1></br>";
    	foreach ($data as $key => $value) {
    		$is_exist = dbGetOne("select 1 from jabatan where idjabatan=".$value['idjabatan']);
    		echo $value['nama']."<br>";
    		if($is_exist){
    			$dataUpdate = array(
	    			'idunit'	=> $value['idunit'],
	    			'name'		=> $value['nama'],
	    			'singkatan'	=> $value['singkatan'],
	    			'eselon'	=> $value['eselon'],
	    			'isactive'	=> $value['isactive'],
	    			'updated_at'=> Util::timeNow()
	    		);
	    		$update = dbUpdate('jabatan', $dataUpdate, " idjabatan=".$value['idjabatan']);
	    		echo "update ";
	    		if($update){
	    			echo "berhasil<br>";
	    		}else{
	    			echo "gagal<br>";
	    		}
    		}else{
    			$dataInsert = array(
	    			'idjabatan'	=> $value['idjabatan'],
	    			'idunit'	=> $value['idunit'],
	    			'name'		=> $value['nama'],
	    			'singkatan'	=> $value['singkatan'],
	    			'eselon'	=> $value['eselon'],
	    			'isactive'	=> $value['isactive'],
	    			'created_at'=> Util::timeNow(),
	    			'updated_at'=> Util::timeNow(),
	    			'idjabatan_persuratan' => $value['idjabatan']
	    		);
	    		$insert= dbInsert('jabatan', $dataInsert);
	    		echo "insert ";
	    		if($insert){
	    			echo "berhasil<br>";
	    		}else{
	    			echo "gagal<br>";
	    		}
    		}	
    	}

    }

    function testSQLServer(){
    	$host='10.1.99.31';
		$dbuser='sigap';
		$dbpass='SateAyam678';
		$conn = mssql_connect($host, $dbuser, $dbpass);
		$sql = "select * from man_akses.pengguna p ";
		$res = mssql_query($sql);
		echo '<table border=1>';
		while ($row = mssql_fetch_assoc($res)) {
			echo '<tr>';
			foreach ($row as $k => $v) {
				echo '<td>' . $k . '</td>';
			}
			echo '</tr>';
			echo '<tr>';
			foreach ($row as $k => $v) {
				echo '<td>' . $v . '</td>';
			}
			echo '</tr>';
		}
		echo '</table>';
		die();
    }

    function testupdate(){
    	$sql = "update posts set id=NULL where id = 15197";
    	$hasil = dbQuery($sql);    	
    	echo '<pre>';
    	var_dump($hasil);
    	die();
    }

    function getLastUpdate(){
    	$this->load->database();
		$sql_postgres = "select * from settings order by userforlap_lastupdate desc limit 1";
		$setting = dbGetRow($sql_postgres);
		$userlast_update = $setting['userforlap_lastupdate'];
		$peranlast_update = $setting['peranforlap_lastupdate'];
		echo '<pre>';
		var_dump($setting);
		die();
    }

    function getUpdatedUserandRoles($user=NULL,$pass=NULL){
    	$this->load->database();
		$sql_postgres = "select * from settings order by userforlap_lastupdate desc limit 1";
		$setting = dbGetRow($sql_postgres);
		$userlast_update = $setting['userforlap_lastupdate'];
		$peranlast_update = $setting['peranforlap_lastupdate'];
		if ($user=='hajarbleh' and $pass=='arya___jon') {
			$this->load->library('web/AuthManagerWeb');

			//Update user
			$users = AuthManagerWeb::getAllUserSqlServerUpdated($userlast_update);
			$total_users = count($users);
			echo 'Updated Users : <br><br>';
			echo '<table border=1>';
			$counter = 0;   
			foreach ($users as $key => $user) {
				if ($counter==0){
					echo '<tr>';
					echo '<td>No.</td>';
					foreach ($user as $k => $v) {
						echo '<td>' . $k . '</td>';
					}
					echo '</tr>';
				}

				$counter=1;
				echo '<tr>';
				echo '<td>'.$key.'</td>';
				foreach ($user as $k => $v) {
					echo '<td>' . $v . '</td>';
				}
				echo '</tr>';

			}
			echo '</table>';

			$record = array();
			if ($total_users==0)
            	$record['userforlap_lastupdate'] = $userlast_update;
            else 
            	$record['userforlap_lastupdate'] = date("Y-m-d H:i:s");

            //Update Roles
            $roles = AuthManagerWeb::getAllRoleSqlServerUpdated($peranlast_update);
			$total_roles = count($roles);
			echo '<br><br>Updated Roles : <br><br>';
			echo '<table border=1>';
			$counter = 0;
			foreach ($roles as $key => $role) {
				if ($counter==0){
					echo '<tr>';
					foreach ($role[0] as $k => $v) {
						echo '<td>' . $k . '</td>';
					}
					echo '</tr>';
				}

				$counter=1;
				foreach ($role as $k => $v) {
					echo '<tr>';
					foreach ($v as $ke => $ve) {
						echo '<td>' . $ve . '</td>';
					}
					echo '</tr>';
				}
				

			}
			echo '</table>';
			die('berhasil mendapatkan role dan users');
    	} else {
	    	echo '<pre>';
			var_dump($setting);
			die();
    	}
    }

    function updateUsersandRoles($user=NULL,$pass=NULL){
    	$this->load->database();
		// $sql_postgres = "select * from settings order by userforlap_lastupdate desc limit 1";
		// $setting = dbGetRow($sql_postgres);
		$setting["userforlap_lastupdate"] = "2017-09-01 00:00:00";
		$setting["peranforlap_lastupdate"] = "2017-09-01 00:00:00";
		$userlast_update = $setting['userforlap_lastupdate'];
		$peranlast_update = $setting['peranforlap_lastupdate'];

		if ($user=='hajarbleh' and $pass=='arya___jon') {
			$this->load->library('web/AuthManagerWeb');
			$this->load->model('User_model', 'User');

			//Update user
			$users = AuthManagerWeb::getAllUserSqlServerUpdated($userlast_update);
			$total_users = count($users);
			echo 'Updated Users : <br><br>';
			echo '<table border=1>';
			$counter = 0;   
			foreach ($users as $key => $user) {
				$this->User->updateUserFromDikti($user);
				if ($counter==0){
					echo '<tr>';
					echo '<td>No.</td>';
					foreach ($user as $k => $v) {
						echo '<td>' . $k . '</td>';
					}
					echo '</tr>';
				}

				$counter=1;
				echo '<tr>';
				echo '<td>'.$key.'</td>';
				foreach ($user as $k => $v) {
					echo '<td>' . $v . '</td>';
				}
				echo '</tr>';

			}
			echo '</table>';

			$record = array();
			if ($total_users==0)
            	$record['userforlap_lastupdate'] = $userlast_update;
            else 
            	$record['userforlap_lastupdate'] = date("Y-m-d H:i:s");

            //Update Roles
            $roles = AuthManagerWeb::getAllRoleSqlServerUpdated($peranlast_update);
			$total_roles = count($roles);
			echo '<br><br>Updated Roles : <br><br>';
			echo '<table border=1>';
			$counter = 0;
			foreach ($roles as $key => $role) {
				$id = pg_escape_string($key);
				$sql = "select username from users where id='$id'";
        		$username = dbGetOne($sql);
				$role_forlap = AuthManagerWeb::getrolesqlserver($username);
				$this->User->updateRoleFromDikti($key, $role_forlap);
				if ($counter==0){
					echo '<tr>';
					foreach ($role[0] as $k => $v) {
						echo '<td>' . $k . '</td>';
					}
					echo '</tr>';
				}

				$counter=1;
				foreach ($role as $k => $v) {
					echo '<tr>';
					foreach ($v as $ke => $ve) {
						echo '<td>' . $ve . '</td>';
					}
					echo '</tr>';
				}
				

			}
			echo '</table>';
            if ($total_roles==0)
            	$record['peranforlap_lastupdate'] = $peranlast_update;
            else 
            	$record['peranforlap_lastupdate'] = date("Y-m-d H:i:s");

            //insert last update
            dbInsert('settings', $record);
			die('berhasil mengupdate users');
    	} else {
	    	echo '<pre>';
			var_dump($setting);
			die();
    	}
    }

    function updateUsers($user=NULL,$pass=NULL){
    	$this->load->database();
		$sql_postgres = "select * from settings order by userforlap_lastupdate desc limit 1";
		$setting = dbGetRow($sql_postgres);
		$userlast_update = $setting['userforlap_lastupdate'];
		$peranlast_update = $setting['peranforlap_lastupdate'];
		// echo '<pre>';
		// var_dump($setting);
		// die();
		if ($user=='hajarbleh' and $pass=='arya___jon') {
			$this->load->library('web/AuthManagerWeb');
			$this->load->model('User_model', 'User');

			//Update user
			$users = AuthManagerWeb::getAllUserSqlServerUpdated($userlast_update);
			$total_users = count($users);
			echo 'Updated Users : <br><br>';
			echo '<table border=1>';
			$counter = 0;   
			foreach ($users as $key => $user) {
				$this->User->updateUserFromDikti($user);
				if ($counter==0){
					echo '<tr>';
					echo '<td>No.</td>';
					foreach ($user as $k => $v) {
						echo '<td>' . $k . '</td>';
					}
					echo '</tr>';
				}

				$counter=1;
				echo '<tr>';
				echo '<td>'.$key.'</td>';
				foreach ($user as $k => $v) {
					echo '<td>' . $v . '</td>';
				}
				echo '</tr>';

			}
			echo '</table>';

			$record = array();
			if ($total_users==0)
            	$record['userforlap_lastupdate'] = $userlast_update;
            else 
            	$record['userforlap_lastupdate'] = date("Y-m-d H:i:s");

            //Update Roles
            $roles = AuthManagerWeb::getAllRoleSqlServerUpdated($peranlast_update);
			$total_roles = count($roles);
			echo '<br><br>Updated Roles : <br><br>';
			echo '<table border=1>';
			$counter = 0;
			foreach ($roles as $key => $role) {
				$role_forlap = AuthManagerWeb::getrolesqlserver($key);
				$this->User->updateRoleFromDikti($key, $role_forlap);
				if ($counter==0){
					echo '<tr>';
					foreach ($role[0] as $k => $v) {
						echo '<td>' . $k . '</td>';
					}
					echo '</tr>';
				}

				$counter=1;
				foreach ($role as $k => $v) {
					echo '<tr>';
					foreach ($v as $ke => $ve) {
						echo '<td>' . $ve . '</td>';
					}
					echo '</tr>';
				}
				

			}
			echo '</table>';
            if ($total_roles==0)
            	$record['peranforlap_lastupdate'] = $peranlast_update;
            else 
            	$record['peranforlap_lastupdate'] = date("Y-m-d H:i:s");

            //insert last update
            dbInsert('settings', $record);
			die('berhasil mengupdate users');
    	} else {
	    	echo '<pre>';
			var_dump($setting);
			die();
    	}
    }

	function migrasipengguna() {
		die();
		header("Content-type: text/html");
		set_time_limit(900);

		$this->load->database();
		
		$rows_source = $this->_fromSqlServer();
		//$rows_source = $this->_fromTemp();
		ob_start();
		$is_flush = false;
		
		$i=0;
		$id_pengguna = 'x';
		foreach ($rows_source as $row) {
			$i++;
			echo $i . '. ';
			//echo $row['id_pengguna'] . '<br>';	//continue;

			$nm_pengguna = pg_escape_string($row['nm_pengguna']);
			$username = pg_escape_string($row['username']);
			$id_peran = $row['id_peran'];
			$nm_peran = pg_escape_string($row['nm_peran']);
			$id_organisasi = $row['id_organisasi'];
			$nm_lemb = pg_escape_string($row['nm_lemb']);
			$id_organisasi_induk = $row['id_organisasi_induk'];
			$nm_lemb_induk = pg_escape_string($row['nm_lemb_induk']);

			if ($row['id_pengguna'] != $id_pengguna) {

				$sql = "select 1 from pengguna where id_pengguna='{$row['id_pengguna']}'";
				if (dbGetOne($sql)) 
					$sql = "update pengguna set nm_pengguna='$nm_pengguna', username='$usename' where id_pengguna='{$row['id_pengguna']}'";
				else
					$sql = "insert into pengguna (id_pengguna, nm_pengguna, username) values ('{$row['id_pengguna']}', '$nm_pengguna', '$username')";	
				$ret = dbQuery($sql);
				if ($is_flush) {
					if ($ret) 
						echo '<div style="color:blue">OK:'.$sql.'</div>';
					else
						echo '<div style="color:red">ERROR:'.$sql.'</div>';
				}

				$sql = "delete from pengguna_peran where id_pengguna='{$row['id_pengguna']}'";
				$ret = dbQuery($sql);
				if ($is_flush) {
					if ($ret) 
						echo '<div style="color:blue">OK:'.$sql.'</div>';
					else
						echo '<div style="color:red">ERROR:'.$sql.'</div>';
				}
			}

			$id_pengguna = $row['id_pengguna'];

			
			$sql = "select 1 from peran where id_peran='$id_peran'";
			if (dbGetOne($sql)) 
				$sql = "update peran set nm_peran='$nm_peran' where id_peran='$id_peran'";
			else
				$sql = "insert into peran (id_peran, nm_peran) values ('$id_peran', '$nm_peran')";
			$ret = dbQuery($sql);
			if ($is_flush) {
				if ($ret) 
					echo '<div style="color:blue">OK:'.$sql.'</div>';
				else
					echo '<div style="color:red">ERROR:'.$sql.'</div>';
			}


			$sql = "select 1 from pengguna_peran where id_pengguna='$id_pengguna' and id_peran='$id_peran' and id_organisasi='$id_organisasi'";
			if (dbGetOne($sql)) {
				$sql = "update pengguna_peran set nm_lemb='$nm_lemb', id_organisasi_induk='$id_organisasi_induk', nm_lemb_induk='$nm_lemb_induk' 
				where id_pengguna='$id_pengguna' and id_peran='$id_peran' and id_organisasi='$id_organisasi'";
				$ret = dbQuery($sql);
				if ($is_flush) {
					if ($ret) 
						echo '<div style="color:blue">OK:'.$sql.'</div>';
					else
						echo '<div style="color:red">ERROR:'.$sql.'</div>';
				}
			}
			else {
				$sql = "insert into pengguna_peran (id_pengguna, id_peran, id_organisasi, nm_lemb, id_organisasi_induk, nm_lemb_induk) values ('$id_pengguna', '$id_peran', '$id_organisasi', '$nm_lemb_induk', '$id_organisasi_induk', '$nm_lemb_induk')";
				$ret = dbQuery($sql);
				if ($is_flush) {
					if ($ret) 
						echo '<div style="color:blue">OK:'.$sql.'</div>';
					else
						echo '<div style="color:red">ERROR:'.$sql.'</div>';
				}

			}


			if ($is_flush) {
				flush();
				ob_flush();		
			}	

		}

	}

	function getLog() {
		header("Content-type: text/html");
		set_time_limit(900);

		$this->load->database();
		// $sql = "select * from settings ";
		// $rows_setting = dbGetRows($sql);
		// echo '<pre>';
		// var_dump($rows_setting);
		// echo '</pre>';
		$sql = "select distinct id_organisasi from users";
		$data['rows_jumlah_organisasi'] = dbGetRows($sql);
		$sql = "select distinct u.id_organisasi from user_log ul 
				left join users u on u.id=ul.user_id
				where aksi='login' and u.role='O'";
		$data['rows_organisasi_log'] = dbGetRows($sql);
		$sql = "select count(*) 
			from (select user_id, username from user_log where aksi='login' group by user_id, username) x";
		$data['rows_user_log'] = dbGetRows($sql);
		$sql = "select count(*) from posts";
		$data['rows_posts'] = dbGetRows($sql);
		$sql = "select count(*) from posts where type='T'";
		$data['rows_ticket'] = dbGetRows($sql);
		$sql = "select count(*) from posts where type='G'";
		$data['rows_Group'] = dbGetRows($sql);
		$sql = "select count(*) from posts where type='K'";
		$data['rows_KM'] = dbGetRows($sql);
		$sql = "select pg_size_pretty(pg_database_size('helpdesk'))";
		$data['storage_heldesk'] = dbGetRow($sql);
		$sql = "select pg_size_pretty(pg_database_size('helpdesk_upload'))";
		$data['storage_blob'] = dbGetRow($sql);
		$sql = "select 	distinct l.id,
						l.name,
						m.jml_organisasi,
		 				l.jml_organisasi_login,
						(cast(l.jml_organisasi_login as float)/cast(m.jml_organisasi as float)) as persentase 
				from
				(	select log.id,log.name,count(log.id_organisasi) as jml_organisasi_login 
					from (
						select distinct g.id,g.name, u.id_organisasi from groups g 
						left join group_members gm on gm.group_id=g.id
						left join user_log ul on ul.user_id = gm.user_id
						left join users u on u.id=gm.user_id
						where (g.name ilike '%Kopertis%' or g.name ilike '%PTN%' or g.name ilike '%PTK%' or g.name ilike '%PTA%') and ul.aksi='login'
						order by g.id 
					) log
					group by id,name
				) l
				left join
				(	select members.id,members.name,count(id_organisasi) as jml_organisasi
					from(
						select distinct g.id, g.name,id_organisasi from groups g 
						left join group_members gm on gm.group_id=g.id
						left join users u on u.id=gm.user_id
						where (g.name ilike '%Kopertis%' or g.name ilike '%PTN%' or g.name ilike '%PTK%' or g.name ilike '%PTA%')
						order by g.id
					) members
					group by members.id, members.name
				) m on m.id=l.id
				order by persentase";
		$data['map_kopertis'] = dbGetRows($sql);

		$html = "<table border=1>";
		$html .= '<th>jumlah organisasi</th>';
		$html .= '<th>organisasi login</th>';
		$html .= '<th>user login</th>';
		$html .= '<th>posts</th>';
		$html .= '<th>ticket</th>';
		$html .= '<th>group posts</th>';
		$html .= '<th>KM</th>';
		$html .= '<th>storage posts</th>';
		$html .= '<th>storage upload</th>';
		$html .= '<th>codingan</th>';
		$html .= '<tr>';
		$html .= '<td>'.count($data['rows_jumlah_organisasi']).'</td>';
		$html .= '<td>'.count($data['rows_organisasi_log']).'</td>';

		foreach ($data['rows_user_log'] as $row) {
			$html .= '<td>'.$row['count'].'</td>';
			# code...
		}
		foreach ($data['rows_posts'] as $row) {
			$html .= '<td>'.$row['count'].'</td>';
			# code...
		}
		foreach ($data['rows_ticket'] as $row) {
			$html .= '<td>'.$row['count'].'</td>';
			# code...
		}
		foreach ($data['rows_Group'] as $row) {
			$html .= '<td>'.$row['count'].'</td>';
			# code...
		}
		foreach ($data['rows_KM'] as $row) {
			$html .= '<td>'.$row['count'].'</td>';
			# code...
		}
			$html .= '<td>'.$data['storage_heldesk']['pg_size_pretty'].'</td>';
			$html .= '<td>'.$data['storage_blob']['pg_size_pretty'].'</td>';
			$html .= '<td>1.2 GB</td>';
		$html .= '</tr>';
		$html .= '</table>';
		$html .= '<br><br><br>';
		$html .= "<table border=1>";
		$html .= '<th>nama kopertis</th>';
		$html .= '<th>jumlah organisasi</th>';
		$html .= '<th>jumlah organisasi login</th>';
		$html .= '<th>persentase organisasi login</th>';
		
		foreach ($data['map_kopertis'] as $mk) {
			$persentase = round($mk['persentase']*100,0);
			$html .= '<tr>';
			$html .= '<td>'.$mk['name'].'</td>';
			$html .= '<td>'.$mk['jml_organisasi'].'</td>';
			$html .= '<td>'.$mk['jml_organisasi_login'].'</td>';
			$html .= '<td>'.$persentase.'%</td>';
			$html .= '</tr>';
		}
		
		$html .= '</table>';

		echo $html;
		die();
		
	}

	function _fromSqlServer() {
		$host='10.1.99.31';
		$dbuser='sigap';
		$dbpass='SateAyam678';
		$conn = mssql_connect($host, $dbuser, $dbpass);

		$offset=0;
		$next=100;

		$loop = true;
		$rows = array();
		/*
		while ($loop) {
			$sql = "select  convert(nvarchar(36), rp.id_pengguna) as id_pengguna,  p.nm_pengguna, p.username, rp.id_peran, r.nm_peran, convert(nvarchar(36), rp.id_organisasi) as id_organisasi, o.nm_lemb, convert(nvarchar(36), o2.id_organisasi) as id_organisasi_induk, o2.nm_lemb as nm_lemb_induk
				from man_akses.pengguna p 
				join man_akses.role_pengguna rp on p.id_pengguna=rp.id_pengguna
				join man_akses.peran r on rp.id_peran=r.id_peran 
				join man_akses.unit_organisasi o on rp.id_organisasi=o.id_organisasi 
				left join man_akses.unit_organisasi o2 on o2.id_organisasi=o.id_induk_organisasi 
				where p.soft_delete=0 and rp.soft_delete=0 and p.a_aktif=1
				order by p.nm_pengguna, rp.id_peran, rp.id_organisasi
				offset $offset rows fetch next $next rows only";
			$res = mssql_query($sql);
			if (!$res)
				$loop=$false;
			while ($row = mssql_fetch_assoc($res)) {
				$rows[] = $row;
			}
			$offset += $next;
		}
		*/
		$sql = "select  convert(nvarchar(36), rp.id_pengguna) as id_pengguna,  p.nm_pengguna, p.username, rp.id_peran, r.nm_peran, convert(nvarchar(36), rp.id_organisasi) as id_organisasi, o.nm_lemb, convert(nvarchar(36), o2.id_organisasi) as id_organisasi_induk, o2.nm_lemb as nm_lemb_induk
			from man_akses.pengguna p 
			join man_akses.role_pengguna rp on p.id_pengguna=rp.id_pengguna
			join man_akses.peran r on rp.id_peran=r.id_peran 
			join man_akses.unit_organisasi o on rp.id_organisasi=o.id_organisasi 
			left join man_akses.unit_organisasi o2 on o2.id_organisasi=o.id_induk_organisasi 
			where p.soft_delete=0 and rp.soft_delete=0 and p.a_aktif=1
			order by p.id_pengguna, rp.id_peran, rp.id_organisasi";
		$res = mssql_query($sql);
		while ($row = mssql_fetch_assoc($res)) {
			$rows[] = $row;
		}
		// var_dump($rows);die();
		return $rows;	
	}

	function _fromTemp() {
		$host='localhost';
		$dbuser='postgres';
		$dbpass='admin';
		$conn = pg_connect("host=$host dbname=temp user=$dbuser password=$dbpass");

		$sql = "select * from pengguna order by nm_pengguna, id_peran";
		$res = pg_query($conn, $sql);
		$rows = array();
		while ($row = pg_fetch_assoc($res)) {
			$rows[] = $row;
		}
		return $rows;	
	}

	function updatePostKePusdatin($group_id=NULL){
		$sql = "select id from posts p where p.group_id is null and p.type!='D'";
		$postids = dbGetRows($sql);
		$data = array();
		$data['group_id']=$group_id;
		foreach ($postids as $arr_id) {
			$id=$arr_id['id'];
			dbUpdate('posts',$data,"id=$id");
		}
	}

	function setStatusPost($dari,$ke){
		$sql = "select id from posts p where p.status='$dari' and p.type='T' order by p.id limit 20 offset 0 ";
		$postids = dbGetRows($sql);
		$data = array();
		foreach ($postids as $arr_id) {
			$data['status']=$ke;
			$id=$arr_id['id'];
			dbUpdate('posts',$data,"id=$id");
		}
	}

	function setTemplateJawaban($ke){
		$sql = "select id from post_replies pr";
		$post_replies_id = dbGetRows($sql);
		$data = array();
		foreach ($post_replies_id as $key => $value) {
			$data['helpdesk_id'] = $ke;
			$id = $value['id'];
			dbUpdate('post_replies',$data,"id=$id");
		}
		die($ke);
	}

	// cron
	function generate_daily_log($username, $password){
		if($username != "usernamesaktibuatlangsungtembus" || $password != "langsungtembus12345678910" )
			die("Username/password tidak terdaftar.");

		$sql = "select id, keyword
				from warehouse_search_logs
				where date = '".date('Y-m-d')."'";
		die($sql);
		$saved_keywords = array();
		$keywords = dbGetRows($sql);
		foreach($keywords as $key => $keyword){
			$saved_keywords[] = $keyword['keyword'];
			$keywords[$keyword['keyword']] = $keyword;
		}

		$sql = "select keyword as keyword, count(keyword) as count, created_at::date as date
				from search_logs
				where created_at::date = '".date('Y-m-d')."'::date
				group by keyword, created_at::date";
		$data = dbGetRows($sql);
		$result = TRUE;

		$this->load->model("Warehouse_search_log_model");
		foreach ($data as $key => $value) {
			if(!in_array($value['keyword'], $saved_keywords))
				$insert = $this->Warehouse_search_log_model->insert($value, FALSE, TRUE);
			else{
				$insert = $this->Warehouse_search_log_model->update($keywords[$value['keyword']]['id'], $value);
			}

			if(!$insert || !$update)
				$result = $insert;
		}

		if($result)
			echo "Berhasil generate report";
		else
			echo "Gagal generate report";

	}

	function sendMobileNotification() {
        if (!$this->config->item('fcm_allow_notification'))
            return false;

        $url = $this->config->item('fcm_url');
        $google_api_key = $this->config->item('fcm_api_key');

        $str_iduser = $_POST['str_iduser'];
        $post_id = (int) $_POST['post_id'];
        $type = pg_escape_string($_POST['type']);
        $title = $_POST['title'];
        $message = $_POST['message'];

        if (!$str_iduser)
            return false;

        $this->load->database();

        $user_ids = explode(',', $str_iduser);
        $users = array();
        // foreach ($user_ids as $iduser) {
        //     // $sql = "select num from userbadge where iduser=$iduser and jenis='$jenis' and id=$id";
        //     // $num_notifikasi = dbGetOne($sql);
        //     // if ($num_notifikasi) {
        //     //     $sql = "update userbadge set num=num+1 where iduser=$iduser and jenis='$jenis' and id=$id";
        //     //     dbQuery($sql);
        //     // }
        //     // else {
        //     //     $sql = "insert into userbadge (iduser, jenis, id, num) values ($iduser, '$jenis', $id, 1) ";
        //     //     dbQuery($sql);
        //     // }

        //     // $sql = "select sum(num) from userbadge where iduser=$iduser";
        //     // $num_notifikasi = dbGetOne($sql);
        //     // $users[$iduser]['num_notifikasi'] = $num_notifikasi;
        // }


        $sql = "select d.id as device_id, ut.user_id, d.manufacture, d.reg_id as registration_id from user_tokens ut left join devices d on d.id=ut.device_id where ut.user_id in ($str_iduser) order by ut.id desc";
        $rows = dbGetRows($sql);

        foreach ($rows as $row) {
            $user_id = $row['user_id'];
            // $num_notifikasi = $users[$iduser]['num_notifikasi'];

            // $sql = "select sum(num) from userbadge where iduser=$iduser";

            $registration_id = $row['registration_id'];

            if ($row['manufacture'] == 'apple') {
                $fields = array(
                    'to' => $registration_id,
                    'data' => array('id'=>$id, 'type'=>$type),
                    'notification' => array(
                        'title' => $title, 
                        'message' => $message, 
                        'sound'=>'default',
                        'post_id' => $post_id,
                        'click_action' => 'NOTIFIKASI'
                    )
                );
            }
            else {
                $fields = array(
                    'to' => $registration_id,
                    'data' => array(
                        'title' => $title, 
                        'message' => $message, 
                        'post_id'=>$post_id, 
                        'type'=>$type
                    )
                );                
            }
            echo 'test<pre>';
            vaR_dump($fields);
            die();
            $headers = array(
                  'Authorization:key='.$google_api_key,
                  'Content-Type: application/json'
             );

            $ch = curl_init();
            curl_setopt($ch, CURLOPT_URL, $url);
            curl_setopt($ch, CURLOPT_POST, true);
            curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
            curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($fields));

            $result = curl_exec($ch);
            //if($result === false)    die('Curl failed ' . curl_error());
            curl_close($ch);

        }
    }

    function testsendmobilenotification($user_id) {
        $url = $this->config->item('fcm_url');
        $google_api_key = $this->config->item('fcm_api_key');

        $str_iduser = $user_id;
        $post_id = (int) 16770;
        $type = "post created";
        $title = "sigap";
        $message = "ini testing dari arya";

        $this->load->database();

        $users = array();

        $sql = "select d.id as device_id, ut.user_id, d.manufacture, d.reg_id as registration_id from user_tokens ut left join devices d on d.id=ut.device_id where ut.user_id='$user_id' order by ut.id desc";
        $row = dbGetRow($sql);
        echo '<pre>';
        var_dump($row);

        $user_id = $row['user_id'];

        // $sql = "select sum(num) from userbadge where iduser=$iduser";

        // $registration_id = $row['device_id'];
        $registration_id = $row['registration_id'];

        $fields = array(
            'to' => $registration_id,
            'data' => array(
                'title' => $title, 
                'message' => $message, 
                'post_id'=>$post_id, 
                'type'=>$type, 
                'badge'=>10
            )
        );  

        $headers = array(
              'Authorization:key='.$google_api_key,
              'Content-Type: application/json'
         );
        echo '<pre>';
        var_dump($headers);
        echo '<pre>';
        var_dump($fields);
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($fields));
        $result = curl_exec($ch);
        $httpcode = curl_getinfo($ch,CURLINFO_HTTP_CODE);
        var_dump($result);
        var_dump($httpcode);
        if($result === false)    die('Curl failed ' . curl_error());
        curl_close($ch);
    }

	function generateAlias(){
    	$sql = "select pa.id as id, pa.post_id, pa.user_id, pa.group_id as alias, g.id as helpdesk_id, g.type, g.name  from post_assign pa
				left join group_members gm on gm.user_id=pa.user_id
				left join groups g on g.id=gm.group_id
				where g.type in ('H') and pa.group_id is null";
    	$assigns = dbGetRows($sql);
    	$post_ids = array();
    	$comment_ids = array();
    	foreach ($assigns as $k_assign => $v_assign) {
    		echo $v_assign['post_id'].'<br>';
    		// $post_ids[] = $v_assign['post_id'];
    		$record = array();
    		$record['group_id'] = $v_assign['helpdesk_id'];
    		if (dbUpdate('post_assign', $record, "id=".$v_assign['id'])){
		    	$sql = "select c.id, c.post_id, c.candelete, c.user_id, g.id as helpdesk_id from comments c 
		    			left join group_members gm on gm.user_id=c.user_id
						left join groups g on g.id=gm.group_id
						where g.type in ('H')
		    			and  c.candelete='1' and c.post_id=".$v_assign['post_id']." and c.user_id='".$v_assign['user_id']."'";
		    	$comments = dbGetRows($sql);    
		    	foreach ($comments as $k_comment => $v_comment) {
		    		$record = array();
		    		$record['group_id'] = $v_comment['helpdesk_id'];
		    		dbUpdate('comments', $record, "id=".$v_comment['id']);
	    		}
    		}

    	}
    }

    function sendToMember($limit = 5){
	    $this->load->model('Group_member_model');
	    $this->load->model('Notification_user_model');

    	$sql = "select * 
    			from 	notification_users
    			where sent_at is null 
    			and group_id is not null
    			limit $limit";

    	$rows = dbGetRows($sql);
    	foreach ($rows as $row) {
	        $group_members = $this->Group_member_model->get_many_by('group_id', $row['group_id']);

            $upd = $this->Notification_user_model->update_by(
            	array('group_id' => $row['group_id'], 'notification_id' => $row['notification_id']), 
            	array(	'status' => Notification_user_model::STATUS_NOTIFICATION, 
            			'sent_at' => Util::timeNow(), 
            			'updated_at' => Util::timeNow())
            );

            if($upd){
	            foreach ($group_members as $group_member) {
	                $receiver[] = array(
	                    'user_id' => $group_member['userId'],
	                    'notification_id' => $row['notification_id'],
	                    'status' => Notification_user_model::STATUS_NOTIFICATION,
	                    'created_at' => Util::timeNow(),
	                    'updated_at' => Util::timeNow()
	                );
	            }
	        }
    	}

        $ins = TRUE;
        if (!empty($receiver)){
        	$ins = $this->Notification_user_model->insert_many($receiver, FALSE, TRUE);
        }

        if($ins)
        	echo "Berhasil generate notification user";
        else
        	echo "Gagal generate notification user";
        
    }

    // cron   
    function generateMobileNotification($limit = 5, $debug = FALSE){
    	$debug = (bool) $debug;

    	$sql = "select n.id, n.reference_id, n.message, p.user_id, n.action, n.reference_type
				from notifications n, posts p
				where n.reference_id = p.id
				and n.id in (select notification_id
							from notification_users
							where sent_at is null
							group by notification_id)
    			order by n.created_at desc
    			limit $limit";

    	$rows = dbGetRows($sql);
    	$this->load->model("Notification_model");
    	$this->load->model("Notification_user_model");

    	$success = TRUE;
    	$successRows = array();
    	$rowsHeader;

        $upd = $this->Notification_user_model->update_by(
            array('notification_id' => Util::toList($rows, 'id'), 'sent_at' => NULL), 
            array('sent_at' => Util::timeNow())
        );

    	foreach ($rows as $row) {
    		if(!$rowsHeader){
    			foreach ($row as $key => $value) {
    				$rowsHeader[] = $key;
    			}
    		}
    		$process = time();

    		if(!$this->Notification_model->mobileNotification($row['action'], $row['reference_id'], $row['user_id'], $row['id'])){
    			$row['status'] = "FALSE";
    			$row['processTime'] = time() - $process;
    			$successRows[] = $row;
    		}else{
    			$row['status'] = "TRUE";
    			$row['processTime'] = time() - $process;
    			$successRows[] = $row;
		        $this->Notification_user_model->update_by(
		            array('notification_id' => $row['id']), 
		            array('is_sent' => '1') 
		        );
    		}
    	}

    	$rowsHeader[] = 'status kirim';
    	$rowsHeader[] = 'lama proses (s)';

    	if($success)
    		echo "<b>Berhasil mengirim notifikasi mobile</b>";
    	else
    		echo "<b>Gagal mengirim notifikasi mobile</b>";

    	if($successRows && $debug){
	    	echo "<table border=1>";
	    		echo "<tr>";
	    	foreach ($rowsHeader as $value) {
	    		echo "<th>".ucwords($value)."</th>";
	    	}
	    		echo "</tr>";
	    	foreach ($successRows as $row) {
	    		echo "<tr>";
	    		foreach ($row as $key => $value) {
	    			echo "<td>".$value."</td>";
	    		}
	    		echo "</tr>";
	    	}
	    	echo "</table>";
    	}
    }

	function testics(){
    	$ical = new ICal(site_url('assets/uploads/files/MyCal.ics'));
    	print_r($ical->events());
    }

    function generateWeekends($year){
    	die('no access here');
    	$this->load->database();
		$sql = "select s.a::timestamp as weekend
			    from generate_series('2017-01-01'::date, '2050-12-31', '1 day') s(a)
			    where extract(dow from s.a) in (0,6);";	
		$weekends = dbGetRows($sql);

		$this->load->model('Holiday_model');
		foreach ($weekends as $k_weekend => $v_weekend) {
			$holiday = array();
			$holiday['name'] = 'Weekend';
			$holiday['description'] = 'Weekend';
			$holiday['date'] = $v_weekend['weekend'];
			$this->Holiday_model->create($holiday);
		}
    }

    public function resetQuotaUser($group_id=NULL, $user_id=null){
    	$this->load->model("Post_quota_model");

    	$filter_group = "and id='".$group_id."'";
    	if ($group_id==NULL) {
    		$filter_group = "";
    	}
    	$filter_user = "";
    	if ($user_id!=null){
    		$filter_user = "and p.user_id='".$user_id."'";
    	}

    	$sql = " select * from groups where type='H' $filter_group";
    	$rows = dbGetRows($sql);
    	if ($rows==NULL || $rows==false){
    		die('Group Tidak ada!');
    	}
    	$groups = array();
    	// foreach group
    	foreach ($rows as $key => $row) {
    		$sql = "select count(1) as quota, p.user_id from posts p 
    				left join post_rating pr on pr.post_id=p.id 
    				where p.type='T' and (p.is_delete=0 or p.is_delete is null) 
    				and p.group_id='".$row['id']."' $filter_user 
    				and (p.status in ('B', 'P', 'S') and pr.post_id is null) 
    				group by p.user_id";
    		$quotas = dbGetRows($sql);
    		foreach ($quotas as $quota) {
	    		$data = array();
	    		$data['quota'] = $quota['quota'];
	    		
	    		$sql = "select * from post_quotas where user_id='".$quota['user_id']."' and group_id='".$row['id']."' ";
	    		$post_quota = dbGetRow($sql);
	    		if ($post_quota==NULL || $post_quota==false) {
	    			$data['user_id'] = $quota['user_id'];
	    			$data['group_id'] = $row['id'];
	    			$this->Post_quota_model->insert($data, TRUE, FALSE);
	    		} else {
	    			$this->Post_quota_model->update($post_quota['id'], $data);
	    		}
    		}    		
    	}
    }

    public function syncForlap(){
    	$temp = $this->load->database('temp', true);
    	$host='10.1.99.31';
		$dbuser='sigap';
		$dbpass='SateAyam678';
		$conn = mssql_connect($host, $dbuser, $dbpass);
		$offset=0;
		$next=100;

		$loop = true;
		$rows = array();
		$sql = "select  convert(nvarchar(36), rp.id_pengguna) as id_pengguna,  p.nm_pengguna, p.username, rp.id_peran, r.nm_peran, convert(nvarchar(36), rp.id_organisasi) as id_organisasi, o.nm_lemb, convert(nvarchar(36), o2.id_organisasi) as id_organisasi_induk, o2.nm_lemb as nm_lemb_induk
			from man_akses.pengguna p 
			join man_akses.role_pengguna rp on p.id_pengguna=rp.id_pengguna
			join man_akses.peran r on rp.id_peran=r.id_peran 
			join man_akses.unit_organisasi o on rp.id_organisasi=o.id_organisasi 
			left join man_akses.unit_organisasi o2 on o2.id_organisasi=o.id_induk_organisasi 
			where p.soft_delete=0 and rp.soft_delete=0 and p.a_aktif=1
			order by p.id_pengguna, rp.id_peran, rp.id_organisasi";
		$res = mssql_query($sql);
		while ($row = mssql_fetch_assoc($res)) {
			dbInsert('pengguna', $row, $temp);
		}
    }

}

?>