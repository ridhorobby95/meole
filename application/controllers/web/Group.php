<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Group extends WEB_Controller {

    protected $title = 'Group';

    /**
     * Constructor
     */
    public function __construct() {
        parent::__construct();

        $this->data['noimg'] = base_url('assets/web/img/logo.png');
    }

    function index($page=0) {
        $this->template->viewDefault($this->view,$this->data);
    }

    /**
     * Halaman daftar grup user
     */
    public function list_me($page = 0) {
        // tombol
        $buttons = array();
        $buttons[] = array('label' => 'Tambah Grup', 'type' => 'success', 'icon' => 'plus', 'click' => 'goAdd()');

        $this->data['buttons'] = $buttons;

        if (SessionManagerWeb::isAdministrator())
            $this->data['data'] = $this->model->getAll();
        else
            $this->data['data'] = $this->model->getMe(SessionManagerWeb::getUserID());

        $this->data['a_type'] = Group_model::getType();

        $this->template->viewDefault($this->class . '_index', $this->data);
    }

    public function detail($id,$page=0) {
        // parent::detail($id);

        // set "sudah baca"
        $this->load->model('Notification_model');
        $this->load->model('Notification_user_model');

        $this->Notification_user_model->setAsRead(SessionManagerWeb::getUserID(), Notification_model::TYPE_GROUP_DETAIL, $id);

        // ambil detail
        $data = $this->model->get($id);
        // echo '<pre>';
        // vaR_dump($data);
        // die();
        unset($data['groupMembers']);
        $this->load->model('Group_member_model','members');
        $data['groupMembers'] = $this->members->getMembers($id,$page,$this->input->get('search_anggota'));
        $data['staffMembers'] = $this->members->getStaffs($id,0,$this->input->get('search_anggota'));
        if ($data['type']=='H') {
            $data['staffMembers'] = array();
            foreach ($data['groupMembers'] as $key => $admin) {
                if ($this->User_model->isAdministrator($admin['userid'])){
                    $data['staffMembers'][] = $admin;
                }

            }
        }
        $this->load->model('User_model', 'users');
        $this->data['nonMembers'] = $this->users->getUserNonMemberGroup($id);
        // echo '<pre>';
        // var_dump($this->data['nonMembers']);
        // die();

        // cek user
        // if ($data['owner']['id'] != SessionManagerWeb::getUserID())
        //     unset($this->data['buttons']['edit'], $this->data['buttons']['delete']);

        $this->data['a_quota'] = array(
            "1" => "1",
            "2" => "2",
            "3" => "3",
            "4" => "4",
            "5" => "5",
            "6" => "6",
            "7" => "7",
            "8" => "8",
            "9" => "9",
            "10" => "10",
        );
        // ambil detail
        $this->data['data'] = $data;

        $this->data['a_type'] = Group_model::getType();

        //jangan dihapus dulu
        // // combo box
        // $this->load->model('User_model');
        // $rows = $this->User_model->order_by('name')->get_all();
        // $a_user = array();
        // foreach ($rows as $row)
        //     $a_user[$row['id']] = $row['name'];

        $this->template->viewDefault($this->view, $this->data);
    }

    /**
     * Halaman edit grup
     * @param int $id
     */
    public function edit($id = null) {
        parent::edit($id);

        if (isset($id))
            $data = $this->model->get($id);
        else
            $data = array();

        // cek user
        // echo '<pre>';
        // var_dump($data);
        // echo '</pre>';
        // die();
        if ($id!=null) {
            $ok = false;
            $msg = 'Tidak dapat mengubah grup ini';

            SessionManagerWeb::setFlashMsg($ok, $msg);
            redirect($this->ctl . '/detail/' . $id);
        }

        // untuk multiselect
        $tdata = array();
        foreach ($data['groupMembers'] as $v) {
            if ($v['groupMemberRole']['id'] == Group_member_model::ROLE_MEMBER)
                $tdata[] = $v['user']['id'];
        }

        $data['membersID'] = $tdata;

        $this->data['data'] = $data;

        $this->data['a_type'] = Group_model::getType();

        $this->data['a_group'] = $this->model->getDropdownByType(Group_model::TYPE_DIVISION, array('' => 'Tidak memiliki divisi'));

        // combo box
        $this->load->model('User_model');
        $rows = $this->User_model->order_by('name')->get_all();

        $a_user = array();
        foreach ($rows as $row)
            $a_user[$row['id']] = $row['name'];

        $this->data['a_user'] = $a_user;

        $this->template->viewDefault($this->view, $this->data);
    }

    /**
     * Membuat data baru
     */
    public function create() {
        $data = $this->postData;
        $data['userId'] = (empty($data['owner']) ? SessionManagerWeb::getUserID() : $data['owner']);
        $data['members'] = empty($data['members']) ? NULL : implode(',', $data['members']);

        if ($_FILES['image']['error'] == UPLOAD_ERR_OK)
            $data['image'] = $_FILES['image']['name'];
        else
            unset($_FILES['image']);
        $insert = $this->model->create($data);
        if ($insert === true) {
            SessionManagerWeb::setFlashMsg(true, 'Berhasil membuat grup');
            $this->redirect();
        } else {
            if (!is_string($insert)) {
                $validation = $this->model->getErrorValidate();
                if (empty($validation))
                    $msg = 'Gagal membuat grup';
                else
                    $msg = implode('<br />', $validation);
            } else
                $msg = $insert;

            SessionManagerWeb::setFlashMsg(false, $msg);
            redirect($this->ctl . '/add');
        }
    }

    /**
     * Edit data
     * @param int $id
     */
    public function updatephoto($id = null) {
        //$data = $this->model->get($id);
        $data = $this->postData;

        if ($_FILES['image']['error'] == UPLOAD_ERR_OK)
            $data['image'] = $_FILES['image']['name'];
        else
            unset($_FILES['image']);
        // echo '<pre>';
        // var_dump($data);
        // echo '</pre>';
        // die();
        $update = $this->model->savephoto($id, $data);
        //die($id);
        // if ($update === true) {
        //     $ok = true;
        //     $msg = 'Berhasil mengubah grup';
        // } else {
        //     $ok = false;
        //     if (!is_string($update)) {
        //         $validation = $this->model->getErrorValidate();
        //         if (empty($validation))
        //             $msg = 'Gagal mengubah grup';
        //         else
        //             $msg = implode('<br />', $validation);
        //     } else
        //         $msg = $update;
        // }
        // SessionManagerWeb::setFlashMsg($ok, $msg);
        redirect($this->ctl . '/detail/' . $id);
    }

    public function ajaxupdateinitial() {
        $data = $this->input->post(null,true);
        if (strlen($data['initial'])>3 or trim($data['initial'], ' ')=='') 
            die('ERROR');
        $update = $this->model->updateInitial($data);
        die($update);
    }

    public function ajaxupdategrouptype() {
        $data = $this->input->post(null,true);
        if ($data['type']=="") {
            die('ERROR');
        }
        
        $update = $this->model->updateGroupType($data);
        die( $update);
    }

    public function ajaxupdatedescription() {
        $data = $this->input->post(null,true);
        if ($data['description']=="") {
            die('ERROR');
        }
        $update = $this->model->updateDescription($data);
        die( $update);
    }

    public function ajaxupdatequota() {
        $data = $this->input->post(null,true);
        if ($data['quota_limit']=="") {
            die('ERROR');
        }
        $update = $this->model->updateQuotaLimit($data);
        die( $update);
    }

    /**
     * Edit data
     * @param int $id
     */
    public function updatecover($id = null) {
        //$data = $this->model->get($id);
        $data = $this->postData;

        if ($_FILES['cover']['error'] == UPLOAD_ERR_OK){
            $arr_name=explode(' ',$_FILES['cover']['name']);
            $data['cover'] = implode('_',$arr_name);;
        }
        else{
            unset($_FILES['cover']);
            redirect('web/post?g=' . $id);
        }
        // echo '<pre>';
        // var_dump($data);
        // echo '</pre>';
        $update = $this->model->savecover($id, $data);
        //die($id);
        // if ($update === true) {
        //     $ok = true;
        //     $msg = 'Berhasil mengubah grup';
        // } else {
        //     $ok = false;
        //     if (!is_string($update)) {
        //         $validation = $this->model->getErrorValidate();
        //         if (empty($validation))
        //             $msg = 'Gagal mengubah grup';
        //         else
        //             $msg = implode('<br />', $validation);
        //     } else
        //         $msg = $update;
        // }
        // SessionManagerWeb::setFlashMsg($ok, $msg);
        redirect('web/post?g=' . $id);
    }

    /**
     * Edit data
     * @param int $id
     */
    public function updategroupphoto($id = null) {
        $data = $this->postData;
        if ($_FILES['image']['error'] == UPLOAD_ERR_OK){
            $arr_name=explode(' ',$_FILES['image']['name']);
            $data['image'] = implode('_',$arr_name);;
        }
        else{
            unset($_FILES['image']);
            redirect('web/post?g=' . $id);
        }
        $update = $this->model->savephoto($id, $data);
        redirect('web/post?g=' . $id);
    }

    /**
     * Edit data
     * @param int $id
     */
    public function update($id) {
        $data = $this->model->get($id);
        if ($data['owner']['id'] == SessionManagerWeb::getUserID()) {
            $data = $this->postData;
            $data['userId'] = (empty($data['owner']) ? SessionManagerWeb::getUserID() : $data['owner']);
            $data['members'] = empty($data['members']) ? NULL : implode(',', $data['members']);

            if ($_FILES['image']['error'] == UPLOAD_ERR_OK)
                $data['image'] = $_FILES['image']['name'];
            else
                unset($_FILES['image']);

            $update = $this->model->save($id, $data);
            if ($update === true) {
                $ok = true;
                $msg = 'Berhasil mengubah grup';
            } else {
                $ok = false;
                if (!is_string($update)) {
                    $validation = $this->model->getErrorValidate();
                    if (empty($validation))
                        $msg = 'Gagal mengubah grup';
                    else
                        $msg = implode('<br />', $validation);
                } else
                    $msg = $update;
            }
        }
        else {
            $ok = false;
            $msg = 'Tidak dapat mengubah grup ini';
        }

        SessionManagerWeb::setFlashMsg($ok, $msg);
        redirect($this->ctl . '/edit/' . $id);
    }

    /**
     * Hapus data
     * @param int $id
     */
    public function delete($id) {

        //$data['owner'] = $this->model->get($id);
        //die(SessionManagerWeb::getUserID());
        // if ($data['owner']['id'] == SessionManagerWeb::getUserID()) {
        //if ($this->isAdmin()) {
        if (SessionManagerWeb::isAdministrator()){
            $delete = $this->model->delete($id);

            if ($delete === true) {
                $ok = true;
                $msg = 'Berhasil menghapus grup';
            } else {
                $ok = false;
                $msg = 'Gagal menghapus grup';
            }
        } else {
            $ok = false;
            $msg = 'Tidak dapat menghapus grup ini';
        }

        SessionManagerWeb::setFlashMsg($ok, $msg);
        // $this->redirect();
        redirect($this->ctl . '/daftar_group');
    }

    public function delete_helpdesk($id) {

        //$data['owner'] = $this->model->get($id);
        //die(SessionManagerWeb::getUserID());
        // if ($data['owner']['id'] == SessionManagerWeb::getUserID()) {
        //if ($this->isAdmin()) {
        if (SessionManagerWeb::isAdministrator()){
            $delete = $this->model->delete($id);

            if ($delete === true) {
                $ok = true;
                $msg = 'Berhasil menghapus helpdesk';
            } else {
                $ok = false;
                $msg = 'Gagal menghapus helpdesk';
            }
        } else {
            $ok = false;
            $msg = 'Tidak dapat menghapus helpdesk ini';
        }

        SessionManagerWeb::setFlashMsg($ok, $msg);
        // $this->redirect('daftar_helpdesk');
        redirect($this->ctl . '/daftar_helpdesk');
    }

    /**
     * Redirect, biasanya setelah tambah atau hapus data
     */
    protected function redirect() {
        $back = $this->postData['referer'];
        if (empty($back))
            redirect($this->ctl . '/daftar');
        else
            header('Location: ' . $back);
    }
    
    public function group_member($id){
      $group = $this->model->with('Group_member')->get($id);
      $users = Util::toList($group['groupMembers'], 'userId', array(SessionManagerWeb::getUserID()));
      echo json_encode($users);die;
    }

    function daftar() {
        $rows = $this->model->getDaftar();
        foreach ($rows as $row) {
            $list_group[] = $row;
        }

        foreach($list_group as $key=>$val){
            $group[$key]=$val['name'];
        }
        array_multisort($group,SORT_NATURAL| SORT_FLAG_CASE, $list_group);
        $this->data['data'] = $list_group;

        $this->template->viewDefault($this->view, $this->data);
    }

    function daftar_group() {
        // $rows = $this->model->getDaftarGroup();
        // foreach ($rows as $row) {
        //     $list_group[] = $row;
        // }

        // foreach($list_group as $key=>$val){
        //     $group[$key]=$val['name'];
        // }
        // array_multisort($group,SORT_NATURAL| SORT_FLAG_CASE, $list_group);
        // $this->data['data'] = $list_group;
        // $this->template->viewDefault($this->class.'_daftar', $this->data);
         $otherdb = $this->load->database('temp', TRUE);
        // $a = dbGetRow('select * from posts limit 10');
         $a = $otherdb->get('pengguna');
         $b = $a->result_array();
        // echo "<pre>";print_r($b);die();
    }

    function daftar_helpdesk() {
        $rows = $this->model->getDaftarHelpdesk();
        if (!SessionManagerWeb::isSuperAdministrator()){
            $rows = $this->Group_model->getDaftarMyHelpdesk(SessionManagerWeb::getUserID());
        }
        foreach ($rows as $row) {
            $list_group[] = $row;
        }

        foreach($list_group as $key=>$val){
            $group[$key]=$val['name'];
        }
        array_multisort($group,SORT_NATURAL| SORT_FLAG_CASE, $list_group);
        $this->data['data'] = $list_group;
        
        $this->template->viewDefault($this->class.'_daftar', $this->data);
    }

    function add($name, $type) {
        $record = array('name' => urldecode($name),
                        'type' => urldecode($type));
        if ($name!=NULL and trim($name,' ')!=''){
            if ($this->model->add($record)){
                SessionManagerWeb::setFlashMsg(true, "Berhasil menambah Group/Kopertis");
            } else {
                SessionManagerWeb::setFlashMsg(false,"Gagal menambah Group/Kopertis");
            }
        } else {
            SessionManagerWeb::setFlashMsg(false,"Nama Group/Kopertis tidak boleh kosong");
        }
        redirect($this->ctl . '/detail/'.$this->db->insert_id());        
    }

    function addhelpdesk($name) {
        $name = urldecode($name);
        if ($name!=NULL and trim($name,' ')!=''){
            if ($this->model->addhelpdesk($name)){
                SessionManagerWeb::setFlashMsg(true, "Berhasil menambah Helpdesk ");
            } else {
                SessionManagerWeb::setFlashMsg(false,"Gagal menambah Helpdesk");
            }
        } else {
            SessionManagerWeb::setFlashMsg(false,"Nama Helpdesk tidak boleh kosong");
        }
        // redirect($this->ctl . '/daftar_helpdesk');
        redirect($this->ctl . '/detail/'.$this->db->insert_id());        
    }

    function adduser($group_id, $user_id) {
        if ($user_id!=NULL and $user_id!='' and strtolower($user_id)!='null'){
            $this->model->addUser($group_id, $user_id);
            SessionManagerWeb::setFlashMsg(true, "Berhasil menambah user.");
        } else {
            SessionManagerWeb::setFlashMsg(false, "Gagal menambah user. User tidak ada.");
        }
        redirect($this->ctl . '/detail/' . $group_id);
    }

    function deleteuser($group_id, $user_id) {
        if ($this->model->deleteUser($group_id, $user_id)){
            SessionManagerWeb::setFlashMsg(true, "Berhasil mengeluarkan user dari group ini.");
        } else {
            SessionManagerWeb::setFlashMsg(false, "Gagal mengeluarkan user.");
        }
        redirect($this->ctl . '/detail/' . $group_id);
    }

    function setstaff($group_id, $user_id) {
        $this->model->setStaff($group_id, $user_id);
        redirect($this->ctl . '/detail/' . $group_id);
    }

    function removestaff($group_id, $user_id) {
        $this->model->removeStaff($group_id, $user_id);
        redirect($this->ctl . '/detail/' . $group_id);
    }

}
