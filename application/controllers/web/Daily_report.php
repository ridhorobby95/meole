<?php

defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * Controller Daily_report
 * @author Sevima
 * @version 1.0
 */
class Daily_report extends WEB_Controller {

    protected $title = 'Laporan';

    /**
     * Halaman default
     * @param int $page
     */
    public function index() {
        redirect($this->ctl . '/me');
    }

    /**
     * Halaman daftar laporan user
     * @param int $page
     */
    public function me($page = 0) {
        // tombol
        $buttons = array();
        $buttons[] = array('label' => 'Lapor Komandan !', 'type' => 'success', 'icon' => 'plus', 'click' => 'goAdd()');

        $this->data['buttons'] = $buttons;
        $this->data['data'] = $this->model->getMe(SessionManagerWeb::getUserID(), $page);

        $this->template->viewDefault($this->class . '_index', $this->data);
    }

    /**
     * Halaman daftar laporan yang bisa dilihat user
     * @param int $page
     */
    public function for_me($page = 0) {
        if (SessionManagerWeb::isAdministrator())
            $this->data['data'] = $this->model->getAll(SessionManagerWeb::getUserID(), $page);
        else
            $this->data['data'] = $this->model->getForMe(SessionManagerWeb::getUserID(), $page);
        
        // filter user
        $this->load->model('User_model');
        $rows = $this->User_model->order_by('name')->get_all();
        
        $a_user = array();
        foreach ($rows as $row)
            $a_user[$row['id']] = $row['name'];
        
        $this->data['a_user'] = $a_user;

        $this->template->viewDefault($this->class . '_index', $this->data);
    }

    /**
     * Halaman laporan user tertentu
     * @param int $page
     */
    public function user($userid, $page = 0) {
        if (SessionManagerWeb::isAdministrator())
            $this->data['data'] = $this->model->getMe($userid, $page);
        else
            $this->data['data'] = $this->model->getForMe(SessionManagerWeb::getUserID(), $page, null, $userid);
        
        // filter user
        $this->load->model('User_model');
        $rows = $this->User_model->order_by('name')->get_all();
        
        $a_user = array();
        foreach ($rows as $row)
            $a_user[$row['id']] = $row['name'];
        
        $this->data['a_user'] = $a_user;

        $this->template->viewDefault($this->class . '_index', $this->data);
    }

    /**
     * Halaman edit post
     * @param int $id
     */
    public function edit($id = null) {
        parent::edit($id);

        if (!isset($id)) {
            // ambil user default
            $data = array();

            $this->load->model('Daily_report_user_model');
            $default = $this->Daily_report_user_model->get_many_by('user_id', SessionManagerWeb::getUserID());

            $users = array();
            foreach ($default as $v)
                $users[] = array('id' => $v['userDefault']);

            $data['postUsers'] = $users;
        } else
            $data = $this->model->get_by('posts.id', $id);

        // cek user
        if (isset($id) and $data['userId'] != SessionManagerWeb::getUserID()) {
            $ok = false;
            $msg = 'Tidak dapat mengubah laporan harian ini';

            SessionManagerWeb::setFlashMsg($ok, $msg);
            redirect($this->ctl . '/detail/' . $id);
        }

        // untuk multiselect
        if (!empty($data)) {
            $tdata = array();
            foreach ($data['postUsers'] as $v)
                $tdata[] = $v['id'];

            $data['usersID'] = $tdata;
        }

        $this->data['data'] = $data;

        // combo box
        $this->load->model('User_model');
        $rows = $this->User_model->order_by('name')->get_all();

        $a_user = array();
        foreach ($rows as $row)
            $a_user[$row['id']] = $row['name'];

        $this->data['a_user'] = $a_user;

        $this->template->viewDefault($this->view, $this->data);
    }

    /**
     * Halaman detail laporan
     * @param int $id
     */
    public function detail($id) {
        parent::detail($id);

        // ambil detail
        $data = $this->model->get_by('posts.id', $id);

        // cek user
        if ($data['userId'] != SessionManagerWeb::getUserID())
            unset($this->data['buttons']['edit'], $this->data['buttons']['delete']);

        $this->data['data'] = $data;

        $this->template->viewDefault($this->view, $this->data);
    }

    /**
     * Membuat data baru
     */
    public function create() {
        if ($_FILES['image']['error'] != UPLOAD_ERR_OK)
            unset($_FILES['image']);
        if ($_FILES['video']['error'] != UPLOAD_ERR_OK)
            unset($_FILES['video']);
        if ($_FILES['file']['error'] != UPLOAD_ERR_OK)
            unset($_FILES['file']);

        $data = $this->postData;
        $data['user_id'] = SessionManagerWeb::getUserID();
        $data['image'] = isset($_FILES['image']) ? $_FILES['image']['name'] : NULL;
        $data['video'] = isset($_FILES['video']) ? $_FILES['video']['name'] : NULL;
        $data['file'] = isset($_FILES['file']) ? $_FILES['file']['name'] : NULL;

        $data['postUsers'] = empty($data['postUsers']) ? NULL : implode(',', $data['postUsers']);
        $data['date'] = empty($data['date']) ? date('Y-m-d') : FormatterWeb::formatDate($data['date']);

        $insert = $this->model->create($data);
        if ($insert === true) {
            SessionManagerWeb::setFlashMsg(true, 'Berhasil membuat laporan harian');
            $this->redirect();
        } else {
            if (!is_string($insert)) {
                $validation = $this->model->getErrorValidate();
                if (empty($validation))
                    $msg = 'Gagal membuat laporan harian';
                else
                    $msg = implode('<br />', $validation);
            } else
                $msg = $insert;

            SessionManagerWeb::setFlashMsg(false, $msg);
            redirect($this->ctl . '/add');
        }
    }

    /**
     * Edit data
     * @param int $id
     */
    /* public function update($id) {
      $data = $this->model->get($id);
      if($data['userId'] == SessionManagerWeb::getUserID()) {
      $data = $this->postData;
      $data['user_id'] = SessionManagerWeb::getUserID();
      $data['postUsers'] = empty($data['postUsers']) ? NULL : implode(',',$data['postUsers']);
      $data['date'] = empty($data['date']) ? NULL : FormatterWeb::formatDate($data['date']);

      if($_FILES['image']['error'] == UPLOAD_ERR_OK)
      $data['image'] = $_FILES['image']['name'];
      else
      unset($_FILES['image']);

      if($_FILES['video']['error'] == UPLOAD_ERR_OK)
      $data['video'] = $_FILES['video']['name'];
      else
      unset($_FILES['video']);

      if($_FILES['file']['error'] == UPLOAD_ERR_OK)
      $data['file'] = $_FILES['file']['name'];
      else
      unset($_FILES['file']);

      $update = $this->model->save($id,$data);
      if($update === true) {
      $ok = true;
      $msg = 'Berhasil mengubah laporan harian';
      }
      else {
      $ok = false;
      if(!is_string($update)) {
      $validation = $this->model->getErrorValidate();
      if(empty($validation))
      $msg = 'Gagal mengubah laporan harian';
      else
      $msg = implode('<br />',$validation);
      }
      else
      $msg = $update;
      }
      }
      else {
      $ok = false;
      $msg = 'Tidak dapat mengubah laporan harian ini';
      }

      SessionManagerWeb::setFlashMsg($ok,$msg);
      redirect($this->ctl.'/edit/'.$id);
      } */

    /**
     * Hapus data
     * @param int $id
     */
    public function delete($id) {
        $data = $this->model->get_by('posts.id', $id);
        if ($data['userId'] == SessionManagerWeb::getUserID()) {
            $this->load->model('Post_model');

            $delete = $this->Post_model->delete($id);

            if ($delete === true) {
                $ok = true;
                $msg = 'Berhasil menghapus laporan harian';
            } else {
                $ok = false;
                $msg = 'Gagal menghapus laporan harian';
            }
        } else {
            $ok = false;
            $msg = 'Tidak dapat menghapus laporan harian ini';
        }

        SessionManagerWeb::setFlashMsg($ok, $msg);
        $this->redirect();
    }

    /**
     * Redirect, biasanya setelah tambah atau hapus data
     */
    protected function redirect() {
        $back = $this->postData['referer'];
        if (empty($back))
            redirect($this->ctl . '/me');
        else
            header('Location: ' . $back);
    }

    public function recapitulation() {
        $this->data['title'] = 'Rekap ' . $this->title;
        $this->data['years'] = Util::toMap($this->model->getYear(), 'year', 'year');
        if (SessionManagerWeb::isAdministrator() || SessionManagerWeb::isManagemement())
            $this->data['recapitulations'] = $this->model->getRecapitulation($this->postData['month'], $this->postData['year']);
        else
            $this->data['recapitulations'] = $this->model->getRecapitulation($this->postData['month'], $this->postData['year'], SessionManagerWeb::getUserID());

        $this->load->model('Work_day_model');
        $this->data['maxreport'] = $this->Work_day_model->getCount($this->postData['month'], $this->postData['year']);
        $this->template->viewDefault($this->class . '_recapitulation', $this->data);
    }

}
