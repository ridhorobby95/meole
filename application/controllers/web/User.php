<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class User extends WEB_Controller {

    protected $title = 'Pengguna';

    public function index() {
        if (!SessionManagerWeb::isAdministrator() && !SessionManagerWeb::isManagemement())
            redirect($this->ctl . '/me');

        $this->data['data'] = $this->model->order_by('name')->is_role(TRUE)->get_all();
        $this->data['title'] = 'Daftar Pengguna';

        $this->template->viewDefault($this->class . '_list', $this->data);
    }

    /**
     * Halaman daftar teman user
     */
    public function list_me() {
        $this->data['title'] = 'Teman';
        $this->data['data'] = $this->model->getListFor(SessionManagerWeb::getUserID());

        // ambil status hadir
        $this->data['presence'] = $this->model->getListPresence();

        $this->template->viewDefault($this->class . '_index', $this->data);
    }

    /**
     * Halaman profil user
     */
    public function me() {
        // tombol
        $buttons = array();
        $buttons['save'] = array('label' => 'Simpan', 'type' => 'success', 'icon' => 'save', 'click' => 'goSave()');
        $this->data['buttons'] = $buttons;
        $this->data['title'] = 'Profil';

        $this->edit(SessionManagerWeb::getUserID());
    }

    /**
     * Halaman edit user
     * @param int $id
     */
    public function edit($id = null) {
        // edit user lain hanya untuk admin
        if ($id != SessionManagerWeb::getUserID() and ! SessionManagerWeb::isAdministrator() and ! SessionManagerWeb::isManagemement())
            redirect($this->ctl . '/me');

        parent::edit($id);

        $this->model->show_all(true); // biar muncul yang privat

        if (isset($id)){
            $data = $this->model->getUser($id, true);
            $data['user_photo'] = $this->model->getUserPhoto($data['id'],$data['photo']);
        }
        else
            $data = array();

        $this->data['data'] = $data;

        $this->template->viewDefault($this->view, $this->data);
    }


    /**
     * Logout
     */
    public function logout() {
        AuthManagerWeb::logout();
        foreach ($_SESSION as $key => $value) {
            $_SESSION[$key] = null;
        }
        redirect($this->getCTL('site'));
    }

    /**
     * Edit data user
     * @param int $id jika tidak ada dianggap data pribadi
     */
    public function update($id = null) {
        $data = $this->postData;
        $isself = (empty($id) ? true : false);
        
        if ($isself) {
            $id = SessionManagerWeb::getUserID();
            $data['username'] = SessionManagerWeb::getUserName();
            unset($data['status']);
        }

        if ($_FILES['photo']['error'] == UPLOAD_ERR_OK)
            $data['photo'] = $_FILES['photo']['name'];
        else
            unset($_FILES['photo']);

        $update = $this->model->save($id, $data);
        if ($update === true) {
            $ok = true;
            $msg = 'Berhasil mengubah profil';

            // sekalian foto
            $user = $this->model->getUser($id);
            SessionManagerWeb::setPhoto($user);
        } else {
            $ok = false;
            if (!is_string($update)) {
                $validation = $this->model->getErrorValidate();
                if (empty($validation))
                    $msg = 'Gagal mengubah profil';
                else
                    $msg = implode('<br />', $validation);
            } else
                $msg = $update;
        }

        SessionManagerWeb::setFlashMsg($ok, $msg);
        if ($isself)
            redirect($this->ctl . '/detail');
        else
            redirect($this->ctl . '/detail/' . $id);
    }

    function daftar($page=0) {

        if (isset($_GET['r'])) {
            //$_SESSION['role_selected'] = $_GET['r'];
            // if ($_GET['r']=='SUPERADMINISTRATOR'){
            //     $_SESSION['role_selected'] = "R";
            // } else
            if ($_GET['r']=='ADMINISTRATOR'){
                $_SESSION['role_selected'] = "A";
            // } elseif ($_GET['r']=='STAFF') {
            //     $_SESSION['role_selected']="S";
            } elseif ($_GET['r']=='OPERATOR') {
                $_SESSION['role_selected'] = "O";
            } elseif ($_GET['r']=='NONOPERATOR') {
                $_SESSION['role_selected'] = "N";
            } elseif ($_GET['r']=='WARGA') {
                $_SESSION['role_selected'] = "W";
            } else {
                unset($_SESSION['role_selected']);
            }

            redirect('web/user/daftar');
        } 
        $this->data['limit'] = $this->model->limit;
        // $this->data['pagination'] = ceil($this->model->getCount($_SESSION['role_selected'],$this->input->get('search')) / $this->model->limit);
        $this->data['data'] = $this->model->getUsers(SessionManagerWeb::getUserID(),$page,$_SESSION['role_selected'],$this->input->get('search'));
        // echo "<pre>";print_r($this->data['data']);die();
        $this->template->viewDefault($this->view, $this->data);
    }

    function detail($id) {
        if(!$id){
            $id = SessionManagerWeb::getUserID();
        }
        $this->data['data'] = $this->model->getUser($id, true);
        $this->data['data']['user_photo'] = $this->model->getUserPhoto($this->data['data']['id'],str_replace(' ', '_', $this->data['data']['photo']));
        // echo "<pre>";print_r($this->data['data']);die();
        $this->template->viewDefault($this->view, $this->data);
    }

    function setrole($id, $role) {
        if (!SessionManagerWeb::isAdministrator()) {
            SessionManagerWeb::setFlashMsg(false, 'Anda tidak berhak mengubah data pengguna ini');
            redirect('web/user/detail/' . $id);
        }
        $this->model->updateUserRole($id, $role);
        redirect('web/user/detail/' . $id);
    }

    function setexclude($id, $exclude) {
        if (!SessionManagerWeb::isAdministrator()) {
            SessionManagerWeb::setFlashMsg(false, 'Anda tidak berhak mengubah data pengguna ini');
            redirect('web/user/detail/' . $id);
        }
        $this->model->updateUserExclude($id, $exclude);
        redirect('web/user/daftar/');
    }

    //untuk ban user
    // function banUser($id, $is_banned) {
    //     if (!SessionManagerWeb::isAdministrator()) {
    //         SessionManagerWeb::setFlashMsg(false, 'Anda tidak berhak mengubah data pengguna ini');
    //         redirect('web/user/detail/' . $id);
    //     }
    //     $this->model->updateUserBanned($id, $is_banned);
    //     redirect('web/user/daftar/');
    // }

    function getlastupdate() {
        // die('restricted');
        $sql = "select * from users";
        $users_sigap = dbGetRows($sql);
        // $user_id_sigap = array();
        // foreach ($users_sigap as $u_sigap) {
        //     array_push($user_id_sigap,$u_sigap['id']);
        // }
        $users_forlap = AuthManagerWeb::getallusersqlserver();
        $total_users_forlap = count($users_forlap);
        $total_users_sigap = count($users_sigap);
        echo 'user_forlap : '.$total_users_forlap."\n";
        echo 'user_sigap : '.$total_users_sigap."\n";
        echo '<pre>';
        var_dump($users_forlap);
        echo '</pre>';
        die();
        // $user_id_forlap = array();
        // foreach ($users_forlap as $u_forlap) {
        //     array_push($user_id_forlap,$u_forlap['id_pengguna']);
        //     if (!in_array($u_forlap['id_pengguna'], $user_id_sigap)){
        //         $id= $u_forlap['id_pengguna'];
        //         $this->load->model('User_model', 'User');
        //         $this->User->updateUserFromDikti($u_forlap);
        //         $roles = AuthManagerWeb::getrolesqlserver($u_forlap['username']);
        //         $this->User->updateRoleFromDikti($id, $roles);
        //     }
        // }
        // $counter=0;
        // foreach ($users_sigap as $u_sigap) {
        //     $id = $u_sigap['id'];
        //     if (!in_array($u_sigap['id'],$user_id_forlap)){
        //         $sql = "delete from users where id='$id'";
        //         dbQuery($sql);
        //     }
            
        //     if (($u_sigap['role']=="" || $u_sigap['role']==NULL) || ($u_sigap['id_organisasi']=="" || $u_sigap['id_organisasi']==NULL)){
        //         $this->load->model('User_model', 'User');
        //         $roles = AuthManagerWeb::getrolesqlserver($u_sigap['username']);
        //         if (!empty($roles)){
        //             $this->User->updateRoleFromDikti($id, $roles);
        //         }
        //         $counter++;
        //     }
        // }
        // echo $counter;
        // die();
        // redirect($_SERVER['HTTP_REFERER']);
    }

    function checkLastUpdate() {
        $record = array();
        // $str_date_now = date("Y-m-d H:i:s");
        // $record['userforlap_lastupdate'] = $str_date_now;
        // $record['peranforlap_lastupdate'] = $str_date_now;
        // dbInsert('settings', $record);
        $sql = "select * from settings";
        $settings = dbGetRows($sql);
        echo '<pre>';
        var_dump($settings);
        echo '</pre>';
        $sql = "select * from users where updated_at is not null";
        $users_sigap = dbGetRows($sql);
        $users_forlap = AuthManagerWeb::getallusersqlserver();
        $total_users_forlap = count($users_forlap);
        $total_users_sigap = count($users_sigap);
        echo 'user_forlap : '.$total_users_forlap."\n";
        echo 'user_sigap : '.$total_users_sigap."\n";
        echo '<pre>';
        var_dump($users_sigap);
        echo '</pre>';
        echo '<pre>';
        var_dump($users_forlap);
        echo '</pre>';
        die();
    }

    function resync_user_all() {
        // die('restricted');
        $sql = "select * from users";
        $users_sigap = dbGetRows($sql);
        $user_id_sigap = array();
        foreach ($users_sigap as $u_sigap) {
            array_push($user_id_sigap,$u_sigap['id']);
        }
        $users_forlap = AuthManagerWeb::getallusersqlserver();
        $total_users_forlap = count($users_forlap);
        $total_users_sigap = count($users_sigap);
        echo 'user_forlap : '.$total_users_forlap."\n";
        echo 'user_sigap : '.$total_users_sigap."\n";
        die();
        $user_id_forlap = array();
        foreach ($users_forlap as $u_forlap) {
            array_push($user_id_forlap,$u_forlap['id_pengguna']);
            if (!in_array($u_forlap['id_pengguna'], $user_id_sigap)){
                $id= $u_forlap['id_pengguna'];
                $this->load->model('User_model', 'User');
                $this->User->updateUserFromDikti($u_forlap);
                $roles = AuthManagerWeb::getrolesqlserver($u_forlap['username']);
                $this->User->updateRoleFromDikti($id, $roles);
            }
        }
        $counter=0;
        foreach ($users_sigap as $u_sigap) {
            $id = $u_sigap['id'];
            if (!in_array($u_sigap['id'],$user_id_forlap)){
                $sql = "delete from users where id='$id'";
                dbQuery($sql);
            }
            
            if (($u_sigap['role']=="" || $u_sigap['role']==NULL) || ($u_sigap['id_organisasi']=="" || $u_sigap['id_organisasi']==NULL)){
                $this->load->model('User_model', 'User');
                $roles = AuthManagerWeb::getrolesqlserver($u_sigap['username']);
                if (!empty($roles)){
                    $this->User->updateRoleFromDikti($id, $roles);
                }
                $counter++;
            }
        }
        echo $counter;
        die();
        redirect($_SERVER['HTTP_REFERER']);
    }

    function resync_all() {
        die('restricted');
        $sql = "select id,username from users";
        $users = dbGetRows($sql);
        // echo '<pre>';
        // var_dump($users);
        // echo '</pre>';
        // die();
        foreach ($users as $user) {
            $id= $user['id'];
            $user_auth = AuthManagerWeb::getusersqlserver($user['username']);
                    
            // TODO: perlu field is_delete/is_active utk table users
            if (!$user_auth || $user_auth['a_aktif']!=1 || $user_auth['soft_delete']==1) {
                $sql = "delete from users where id='$id'";
                dbQuery($sql);
            }

            $this->load->model('User_model', 'User');
            $this->User->updateUserFromDikti($user_auth);

            $roles = AuthManagerWeb::getrolesqlserver($user['username']);

            $this->User->updateRoleFromDikti($id, $roles);
        }

        redirect($_SERVER['HTTP_REFERER']);
    }

    function resync($user_id) {
        $id = pg_escape_string($user_id);
        $sql = "select username from users where id='$id'";
        $username = dbGetOne($sql);

        $user = AuthManagerWeb::getusersqlserver($username);
        // TODO: perlu field is_delete/is_active utk table users
        if (!$user) {
            $sql = "delete from users where id='$id'";
            dbQuery($sql);
        }

        $this->load->model('User_model', 'User');
        $this->User->updateUserFromDikti($user);

        $roles = AuthManagerWeb::getrolesqlserver($username);

        $this->User->updateRoleFromDikti($user_id, $roles);

        redirect($_SERVER['HTTP_REFERER']);

    }

    function getstaff() {
        $this->load->model('User_model', 'User');
        $users = $this->User->getStaffFromForlap();
        foreach ($users as $user) {
            $this->User->updateUserFromDikti($user);
            $roles = AuthManagerWeb::getrolesqlserver($user['username']);
            $this->User->updateRoleFromDikti($roles);
        }
        redirect($_SERVER['HTTP_REFERER']);
    }

    function spreadTicket($user_id){
        $name = $this->model->filter(" where id='".$user_id."'")->column("name")->getOne();

        $this->load->model("Post_model");
        $undone_tickets = $this->Post_model->table("post_assign pa")
                                            ->join(" left join posts p on pa.post_id=p.id  ")
                                            ->column("pa.id as post_assign_id, pa.post_id, p.group_id, p.kode, p.title, p.description ")
                                            ->filter(" where pa.user_id='".$user_id."' 
                                                    and p.type='T' and p.status in ('B','P') and p.is_delete=0")
                                            ->order("order by pa.post_id")
                                            ->getAll();
        foreach ($undone_tickets as $k_ticket => $v_ticket) {
            $post_assigns = $this->Post_model->table("posts p")
                                            ->join(" left join post_assign pa on pa.post_id=p.id  ")
                                            ->column("pa.id as post_assign_id, pa.post_id, p.group_id, p.kode, p.title, p.description ")
                                            ->filter(" where p.id='".$v_ticket['post_id']."' ")
                                            ->order("order by pa.post_id")
                                            ->getAll();

            $total_assigns = count($post_assigns);

            // Delete dari orang yang ditugaskan
            if ($this->Post_model->deleteAssign($v_ticket['post_assign_id'])){
                if ($total_assigns<=1){
                    $this->Post_model->rotateTicket($v_ticket['post_id'], $v_ticket['group_id'], false);
                    $record = array();
                    $record['status'] = Post_model::STATUS_OPEN;
                    dbUpdate("posts", $record, "id=".$v_ticket['post_id']);
                }
            } 
        }
        SessionManagerWeb::setFlashMsg(true, 'Sukses menyebarkan tiket dari '.$name);
        redirect($_SERVER['HTTP_REFERER']);
    }

    function ajaxDisableStaff(){
        $this->load->model("Group_model");
        $this->load->model("Group_member_model");

        $user_id = $this->input->post("user_id", true);

        $name = $this->model->column("name")->filter("where id='".$user_id."'")->getOne();

        // Keluarin dari helpdesk
        $group = $this->Group_member_model->getMyHelpdeskGroup($user_id);
        // Cek apakah ada member lain yang handle ticket dalam group
        $members = $this->Group_member_model->getMembers($group['id'], 0, NULL, 1000);
        unset($members['next_page_member']);
        $is_ok = 0;
        foreach ($members as $k_member => $v_member) {

            if ($v_member['userid']!=$user_id){
                $is_excluded = $this->model->column("is_excluded")->filter("where id='".$v_member['userid']."'")->getOne();
                if ($is_excluded!=1){
                    $is_ok++;
                }
            }
        }
        if ($is_ok==0){
            echo false;
            die();
        }
        $this->Group_member_model->delete_by(array('group_id' => $group['id'], 'user_id'=>$user_id));

        // Tidak Handle Tiket
        $this->model->save($user_id, array('is_excluded'=>1));

        // Sebarkan tiket
        $this->spreadTicket($user_id);
    
        SessionManagerWeb::setFlashMsg(true, 'Sukses menonaktifkan '.$name.' sebagai staff');

        die($name);
    
    }

}