<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Informasi extends WEB_Controller {

    protected $title = 'Informasi';

    function __construct() {
        parent::__construct();
        $this->load->helper('text');
        $this->load->model("Post_model","informasi");
        $this->model = $this->informasi;
    }

    /**
     * Halaman daftar post public
     * @param int $page
     */
    public function index($page = 0) {
        // echo '<pre>';
        // var_dump($_GET);
        // die();
        // if (isset($_GET['h'])) {
        //     if ($_GET['h']){
        //         $_SESSION['chosen_helpdesk'] = $_GET['h'];
        //     } 
        //     if ($_GET['h']==''){
        //         unset($_SESSION['chosen_helpdesk']);
        //     }
        // }
        if (isset($_GET['g'])) {
            $_SESSION['group_selected'] = $_GET['g'];
            unset($_SESSION['home_status']);
            if ($_SESSION['group_selected']!='I')
                // unset($_SESSION['chosen_helpdesk']);
            $_SESSION['home_status']['dummy'] = 1;
            if ($_SESSION['group_selected'] == 'A')
                unset($_SESSION['group_selected']);
            redirect('web/informasi');
        }
        if (isset($_GET['s'])) {
            $_SESSION['group_selected']='0';
            $s = $_GET['s'];
            unset($_SESSION['home_status']);
            // unset($_SESSION['chosen_helpdesk']);
            $_SESSION['home_status'][$s] = 1;
            redirect('web/post');
        } 

        // if (isset($_SESSION['chosen_helpdesk'])) {
        //     if ($_SESSION['chosen_helpdesk']){
        //         $this->data['chosen_helpdesk'] = $_SESSION['chosen_helpdesk'];
        //     } 
        // }

        $buttons = array();
        $buttons[] = array('label' => 'Ayo Posting !', 'type' => 'success', 'icon' => 'plus', 'click' => 'goAdd()');

        $this->data['buttons'] = $buttons;
        $this->data['title'] = 'Informasi';

        $posts = $this->informasi->getInformations(SessionManagerWeb::getUserID(), $_SESSION['helpdesk_selected'], $page, $this->input->get('search'));
        // if ($_SESSION['sigap']['auth']['username']=='integra'){
        //     echo 'user integra dipakai testing dulu yaa hehehe';
        //     echo '<pre>';
        //     var_dump($posts);
        //     echo '</pre>';
        //     die(); 
        // }
        
        foreach ($posts as &$post) {
            $post['post_description']=$this->checkURL($post['post_description']);
            if (str_word_count($post['post_description']) > 30) {
                $post['post_description'] = word_limiter($post['post_description'], 30);
                $post['post_description'] .= ' <a href="'.site_url('web/informasi/detail/' . $post['post_id']).'">Lanjut</a>';
            }
        }

        $this->data['data'] = $posts;
        // echo '<pre>';
        // var_dump($this->data);
        // echo '</pre>';
        // die();
        // $this->data['list_informasi'] = $this->model->getListInformasi();
        // $this->data['list_sticky'] = $this->model->getListSticky();
        $this->load->model('User_model', 'User');
        $this->data['list_staff'] = $this->User->getListStaff(SessionManagerWeb::getUserID());
        //die($this->view);
        

        $this->template->viewDefault($this->view, $this->data);
    }

    /**
     * Halaman detail post
     * @param int $id
     */
    public function detail($id) {
        //$this->load->model("Post_model","informasi");
        //die("text");
        parent::detail($id);

        // set "sudah baca"
        $this->load->model('Notification_model');
        $this->load->model('Notification_user_model');
        
        $this->Notification_user_model->setAsRead(SessionManagerWeb::getUserID(), Notification_model::TYPE_POST_DETAIL, $id);
        //die("test");
        // ambil detail
        $this->model->addView($id,SessionManagerWeb::getUserID());
        $data = $this->model->getInformasi(SessionManagerWeb::getUserID(),$id);
        $data['post_description']=$this->checkURL($data['post_description']);
        
        // $comment_ids = array();
        // foreach ($data['comments'] as $d) {

        //     $comment_ids[] = $d['id'];
        // }
        // $images = $this->model->getImagesByCommentIDs($comment_ids);
        // foreach ($data['comments'] as &$d) {
        //     $d['text'] = $this->checkURL($d['text']);
        //     if ($images[$d['id']])
        //         $d['images'] = $images[$d['id']];
        // }

        // $files = $this->model->getFilesByCommentIDs($comment_ids);
        // foreach ($data['comments'] as &$d) {
        //     if ($files[$d['id']])
        //         $d['files'] = $files[$d['id']];
        // }

        // cek user
        if ($data['userId'] != SessionManagerWeb::getUserID())
            unset($this->data['buttons']['edit'], $this->data['buttons']['delete']);


        $this->data['data'] = $data;

        if (!empty($data)) {
            $this->load->model('Tracker_model');
            $this->Tracker_model->post($id, SessionManagerWeb::getUserID());
        }

        $this->load->model('Replies_model', 'Reply');
        $this->data['list_reply'] = $this->Reply->getList();

        $this->load->model('User_model', 'User');
        $this->data['list_staff'] = $this->User->getListStaff(SessionManagerWeb::getUserID());
        //die($this->view);
        //$this->view="post_detail";
        //die($this->view);
        $this->template->viewDefault($this->view, $this->data);
    }

    public function hapus($id) {
        $this->isAllowEditDeletePost($id);
        if ($this->model->delete($id)) {
            $ok = true;
            $msg = 'Berhasil menghapus informasi';
        }
        else {
            $ok = false;
            $msg = 'Gagal menghapus informasi';            
        }
        SessionManagerWeb::setFlashMsg($ok, $msg);
        redirect('web/informasi');
    }

    function update($id)  {
        if ($_POST) {
            $data = array();
            $data['title'] = $this->input->post('judul_kirim',true);
            $data['description'] = $this->input->post('text_kirim',true);
            // $data['description'] = $this->input->post('text_kirim');
            //$data['is_sticky'] = $_POST['stick_kirim'];

            $ret = $this->model->update($data, $id);
            if ($ret) {
                $this->_moveFileTemp($id);
                unset($_SESSION['sigap']['draft_post']);
                $this->db->trans_commit();
                SessionManagerWeb::setFlashMsg(true, 'Berhasil mengubah informasi');
                
                redirect('web/informasi/detail/' . $id);
            } else {
                if (empty($validation))
                    $msg = 'Gagal mengubah informasi';
                else
                    $msg = implode('<br />', $validation);

                SessionManagerWeb::setFlashMsg(false, $msg);
                redirect('web/informasi/edit/' . $id);
            }            
        }    
    }    

    function kirim()  {

        if ($_POST) {
            $data = array();
            $data['user_id'] = SessionManagerWeb::getUserID();
            $data['title'] = $this->input->post('judul_kirim',true);
            $data['description'] = $this->input->post('text_kirim',true);
            // $data['description'] = $this->input->post('text_kirim');
            $data['category_id'] = 1;
            $data['type']=Post_model::TYPE_INFORMATION;
            $data['status'] = 'B';
            $data['is_sticky'] = $_POST['stick_kirim'];
            $data['id_organisasi'] = SessionManagerWeb::getOrganisasiID();
            $data['group_id'] = $this->input->post('group_kirim',true);
            $insert = $this->model->create($data, TRUE, TRUE);

            if ($insert) {
                $this->db->trans_begin();
                if ($_POST['stick_kirim']) {
                    $this->model->setSticky($insert, 1, 'I');
                }

                if (!$this->_moveFileTemp($insert)) {
                    $this->db->trans_rollback();

                    SessionManagerWeb::setFlashMsg(false, 'Gagal membuat Pesan');
                }
                else {
                    unset($_SESSION['sigap']['draft_post']);

                    $this->db->trans_commit();

                    SessionManagerWeb::setFlashMsg(true, 'Berhasil membuat Pesan');
                }
                
                $this->redirect();
            } else {
                if (!is_string($insert)) {
                    $validation = $this->model->getErrorValidate();
                    if (empty($validation))
                        $msg = 'Gagal membuat postingan';
                    else
                        $msg = implode('<br />', $validation);
                } else
                    $msg = $insert;

                SessionManagerWeb::setFlashMsg(false, $msg);
                redirect($this->ctl . '/add');
            }            

        }    
    }

    /**
     * Halaman tambah data
     * @param int $userid
     */
    public function add($userid = null) {
        die();
    }

    /**
     * Halaman edit post
     * @param int $id
     */
    public function edit($id = null) {
        $this->isAllowEditDeletePost($id);
        $data = $this->model->getInformasi(SessionManagerWeb::getUserID(),$id);
        $this->data['data'] = $data;
        $this->template->viewDefault($this->view, $this->data);
    }

    function kirim_edit($id)  {

        if ($_POST) {
            $data = array();
            $data['id'] = $id;
            $data['title'] = $this->input->post('judul_kirim',true);
            $data['description'] = $this->input->post('text_kirim',true);
            // $data['description'] = $this->input->post('text_kirim');
            $data['category_id'] = 1;
            $data['type']=Post_model::TYPE_INFORMATION;
            $data['is_sticky'] = $_POST['stick_kirim'];
            $data['id_organisasi'] = SessionManagerWeb::getOrganisasiID();
            $insert = $this->model->create($data, TRUE, TRUE);

            if ($insert) {
                $this->db->trans_begin();
                if ($_POST['stick_kirim']) {
                    $this->model->setSticky($insert, 1, 'I');
                }

                if (!$this->_moveFileTemp($insert)) {
                    $this->db->trans_rollback();

                    SessionManagerWeb::setFlashMsg(false, 'Gagal membuat Pesan');
                }
                else {
                    unset($_SESSION['sigap']['draft_post']);

                    $this->db->trans_commit();

                    SessionManagerWeb::setFlashMsg(true, 'Berhasil membuat Pesan');
                }
                
                $this->redirect();
            } else {
                if (!is_string($insert)) {
                    $validation = $this->model->getErrorValidate();
                    if (empty($validation))
                        $msg = 'Gagal membuat postingan';
                    else
                        $msg = implode('<br />', $validation);
                } else
                    $msg = $insert;

                SessionManagerWeb::setFlashMsg(false, $msg);
                redirect($this->ctl . '/add');
            }            

        }    
    }

    function stick() {
        if ($_GET) {
            if ($this->input->get('stick')=='1') {
                $data='0';
            } else {
                $data='1';
            }

            $insert = $this->model->setPinPost($_GET['id'],$data);

            $this->redirect();
        }
    }

    /**
     * Hapus data
     * @param int $id
     */
    public function delete($id) {
        die();
        $data = $this->model->get_by('posts.id', $id);
        if ($data['userId'] == SessionManagerWeb::getUserID()) {
            $delete = $this->model->delete($id);

            if ($delete === true) {
                $ok = true;
                $msg = 'Berhasil menghapus postingan';
            } else {
                $ok = false;
                $msg = 'Gagal menghapus postingan';
            }
        } else {
            $ok = false;
            $msg = 'Tidak dapat menghapus postingan ini';
        }

        SessionManagerWeb::setFlashMsg($ok, $msg);
        $this->redirect($this->ctl);
    }

}
