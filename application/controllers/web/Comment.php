<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Comment extends WEB_Controller {

    public function coba(){
        $this->load->model('Comment_model');
        $this->load->model('Notification_model');
        $this->Notification_model->generate(Notification_model::ACTION_COMMENT_CREATE, 7375,SessionManagerWeb::getUserID());
        // $comment = $this->Comment_model->get(7358);
        // echo "<pre>";print_r($comment);die();
    }

    /**
     * Membuat data baru
     */
    public function create($redirect='dashboard') {
        $data = $this->input->post();
        // echo "<pre>";print_r($data);die();
        $data['user_id'] = SessionManagerWeb::getUserID();

        if (SessionManagerWeb::isOperator()) {
            $data['text'] = $this->input->post('text', true);
        }

        $insert = $this->model->create($data, TRUE, TRUE);

        if ($insert) {
            $this->db->trans_commit();
            $this->load->model('Notification_model');
            $this->Notification_model->generate(Notification_model::ACTION_COMMENT_CREATE, $insert,SessionManagerWeb::getUserID());
            SessionManagerWeb::setFlashMsg(true, 'Berhasil membuat tanggapan ');
        } else {
            if (!is_string($insert)) {
                $validation = $this->model->getErrorValidate();
                if (empty($validation))
                    $msg = 'Gagal membuat tanggapan';
                else
                    $msg = implode('<br />', $validation);
            } else
                $msg = $insert;

            SessionManagerWeb::setFlashMsg(false, $msg);
        }
        if($redirect == 'detail'){
            $post_id = $data['post_id'];
            redirect('web/post/detail/'.$post_id);
        }else{
            redirect('web/post/'.$redirect);
        }
        
    }

    /**
     * Hapus data
     * @param int $id
     */
    public function delete($id) {
        $this->load->model('Comment_model', 'Model');
        $comment = $this->model->get($id);
        if (($comment['userId'] == SessionManagerWeb::getUserID() && $comment['candelete'] == 1) || $this->User_model->isAdministrator(SessionManagerWeb::getUserID())) {
            $delete = $this->Model->deletes($id);

            if ($delete) {
		      $this->load->model('Notification_model');
		      $delete = $this->Notification_model->delete_by(array('reference_id' => $id, 'reference_type' => Notification_model::TYPE_POST_DETAIL));

                $ok = true;
                $msg = 'Berhasil menghapus komentar';
            } else {
                $ok = false;
                $msg = 'Gagal menghapus komentar';
            }
        } else {
            $ok = false;
            $msg = 'Tidak dapat menghapus komentar ini';
        }

        SessionManagerWeb::setFlashMsg($ok, $msg);
        $this->redirect($comment);
    }

    /**
     * Redirect, biasanya setelah tambah atau hapus data
     * @param array $data
     */
    protected function redirect($data) {
        $back = $this->postData['referer'];
        if (empty($back)) {
            $id = $data['post_id'];
            if (empty($id))
                $id = $data['postId'];

            // cek di post
            $this->load->model('Post_model');

            $post = $this->Post_model->get($id);
            if (empty($post['id'])) {
                // cek di laporan
                $this->load->model('Daily_report_model');

                $post = $this->Daily_report_model->get($id);
                if (empty($post['id']))
                    redirect($this->back);
                else
                    redirect($this->getCTL('daily_report/detail/' . $id));
            }
            else {
                if ($post['categoryId'] == Category_model::TASK)
                    redirect($this->getCTL('issue/detail/' . $id));
                else
                    redirect($this->getCTL('post/detail/' . $id));
            }
        } else
            header('Location: ' . $back);
    }


    function imageform() {
        return parent::imageform('posts/temp2');
    }

    function fileform() {
        return parent::fileform('posts/temp2');
    }

    function resetkirim() {
        return parent::resetkirim('posts/temp2');
    }

    function getdraft() {
        $str = $this->_getdraftimages('posts/temp2');
        die($str);
    }

}
