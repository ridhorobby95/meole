<?php

defined('BASEPATH') OR exit('No direct script access allowed');


class Administrasi extends WEB_Controller {

	function index() {
		if (isset($_GET['g'])) {
            $_SESSION['group_selected'] = $_GET['g'];
            unset($_SESSION['home_status']);
            if ($_SESSION['group_selected'] == 'A')
                unset($_SESSION['group_selected']);
            redirect('web/administrasi');
        }

		$this->template->viewDefault($this->view,$this->data);
	}

}
