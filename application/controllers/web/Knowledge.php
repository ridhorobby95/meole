<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Knowledge extends WEB_Controller {

    protected $title = 'Knowledge';

    function __construct() {
        parent::__construct();
        $this->load->helper('text');
        // $this->load->model('Post_model', 'Post');
        $this->model = $this->Post;
    }

    /**
     * Halaman daftar post public
     * @param int $page
     */
    public function index($page = 0) {
        // if (isset($_GET['h'])) {
        //     if ($_GET['h']){
        //         $_SESSION['chosen_helpdesk'] = $_GET['h'];
        //     } 
        //     if ($_GET['h']==''){
        //         unset($_SESSION['chosen_helpdesk']);
        //     }
        // }
        if (isset($_GET['g'])) {
            $_SESSION['group_selected'] = $_GET['g'];
            unset($_SESSION['home_status']);
            // if ($_SESSION['group_selected']!='K')
            //     unset($_SESSION['chosen_helpdesk']);
            $_SESSION['home_status']['dummy'] = 1;
            if ($_SESSION['group_selected'] == 'A')
                unset($_SESSION['group_selected']);
            redirect('web/knowledge');
        }
        if (isset($_GET['s'])) {
            $_SESSION['group_selected']='0';
            $s = $_GET['s'];
            unset($_SESSION['home_status']);
            // unset($_SESSION['chosen_helpdesk']);
            $_SESSION['home_status'][$s] = 1;
            redirect('web/post');
        } 

        // if (isset($_SESSION['chosen_helpdesk'])) {
        //     if ($_SESSION['chosen_helpdesk']){
        //         $this->data['chosen_helpdesk'] = $_SESSION['chosen_helpdesk'];
        //     } 
        // }


        $this->data['buttons'] = $buttons;
        $this->data['title'] = 'Knowledge Base';
        $posts = $this->Post->getKnowledges(SessionManagerWeb::getUserID(),$_SESSION['helpdesk_selected'],$page, $this->input->get('search'));
        foreach ($posts as &$post) {
            $post['post_description']=$this->checkURL($post['post_description']);
            if (str_word_count($post['post_description']) > 30) {
                $post['post_description'] = word_limiter($post['post_description'], 30);
                $post['post_description'] .= ' <a href="'.site_url('web/knowledge/detail/' . $post['post_id']).'">Lanjut</a>';
            }
        }
        $this->data['data'] = $posts;
        // foreach ($this->data['data'] as &$d) {
        //     $d['tag']=$this->model->getPostTag($d['post_id']);
        // }
        $this->load->model('User_model', 'User');
        $this->data['list_staff'] = $this->User->getListStaff(SessionManagerWeb::getUserID());
        $this->load->model('Tag_model');
        $this->data['a_tag'] = $this->Tag_model->getListTags();
        $this->template->viewDefault($this->view, $this->data);
    }

    /**
     * Halaman detail post
     * @param int $id
     */
    public function detail($id) {
        
        parent::detail($id);

        // set "sudah baca"
        $this->load->model('Notification_model');
        $this->load->model('Notification_user_model');

        $this->Notification_user_model->setAsRead(SessionManagerWeb::getUserID(), Notification_model::TYPE_POST_DETAIL, $id);

        // ambil detail
        $this->model->addView($id,SessionManagerWeb::getUserID());
        $data = $this->model->getKnowledge($id,SessionManagerWeb::getUserID());
        $data['post_description']=$this->checkURL($data['post_description']);

        // cek user
        if ($data['userId'] != SessionManagerWeb::getUserID())
            unset($this->data['buttons']['edit'], $this->data['buttons']['delete']);

        // $data['tag']=$this->model->getPostTag($data['post_id']);
        $this->data['data'] = $data;
        if (!empty($data)) {
            $this->load->model('Tracker_model');
            $this->Tracker_model->post($id, SessionManagerWeb::getUserID());
        }

        $this->data['list_informasi'] = $this->model->getListInformasi();
        $this->data['list_sticky'] = $this->model->getListSticky();
        $this->load->model('User_model', 'User');
        $this->data['list_staff'] = $this->User->getListStaff(SessionManagerWeb::getUserID());
        
        $this->template->viewDefault($this->view, $this->data);
    }

    /**
     * Halaman tambah data
     * @param int $userid
     */
    public function add($id) {
        $data = $this->model->addFromHelpdesk($id, SessionManagerWeb::getUserID());
        if($data){
            $this->data['data'] = $data;
        }
        $this->load->model('Tag_model');
        $this->data['a_tag'] = $this->Tag_model->getListTags();
        $this->template->viewDefault($this->view, $this->data);
    }

    /**
     * Halaman edit post
     * @param int $id
     */
    public function edit($id) {
        // parent::detail($id);
        $this->isAllowEditDeletePost($id);
        $data = $this->model->getKnowledge($id);

        $tags=array();
        $tag_arr=$this->model->getPostTag($data['post_id']);
        foreach ($tag_arr as $tag){
            $tags[]=$tag['name'];
        }
        $data['tags']=$tags;
        $this->data['data'] = $data;
        $this->load->model('Tag_model');
        $this->data['a_tag'] = $this->Tag_model->getListTags();
        $this->template->viewDefault($this->view, $this->data);
    }

    /**
     * Hapus data
     * @param int $id
     */
    public function delete($id) {
        die();
        $data = $this->model->get_by('posts.id', $id);
        if ($data['userId'] == SessionManagerWeb::getUserID()) {
            $delete = $this->model->delete($id);

            if ($delete === true) {
                $ok = true;
                $msg = 'Berhasil menghapus postingan';
            } else {
                $ok = false;
                $msg = 'Gagal menghapus postingan';
            }
        } else {
            $ok = false;
            $msg = 'Tidak dapat menghapus postingan ini';
        }

        SessionManagerWeb::setFlashMsg($ok, $msg);
        $this->redirect();
    }

    public function hapus($id) {
        $this->isAllowEditDeletePost($id);
        if ($this->model->delete($id)) {
            $ok = true;
            $msg = 'Berhasil menghapus knowledge';
        }
        else {
            $ok = false;
            $msg = 'Gagal menghapus knowledge';            
        }
        SessionManagerWeb::setFlashMsg($ok, $msg);
        redirect('web/knowledge');
    }

    function kirim()  {
        
        if ($_POST) {
            $data = array();
            $data['user_id'] = SessionManagerWeb::getUserID();
            $data['title'] = $this->input->post('judul_kirim',true);
            $data['description'] = $this->input->post('text_kirim',true);
            $data['postTags'] = $this->input->post('tag',true);
            $data['group_id'] = $this->input->post('group_kirim',true);
            // $data['description'] = $this->input->post('text_kirim');
            $data['category_id'] = 1;
            
            $data['type'] = Post_model::TYPE_KNOWLEDGE;

	        $data['status'] = 'B';
            $data['is_sticky'] = $_POST['stick_kirim'];
            $data['id_organisasi'] = SessionManagerWeb::getOrganisasiID();
            $insert = $this->model->create($data, TRUE, TRUE);
            if ($insert) {
                if ($_POST['stick_kirim']) {
                    $this->model->setSticky($insert, 1, 'K');
                }
                $this->db->trans_begin();
				if (!$this->_moveFileTemp($insert)) {
					$this->db->trans_rollback();
                    SessionManagerWeb::setFlashMsg(false, 'Gagal membuat Knowledge');
				}
				else {
					unset($_SESSION['sigap']['draft_post']);
					$this->db->trans_commit();
                    SessionManagerWeb::setFlashMsg(true, 'Berhasil membuat Knowledge');
				}
				
                $this->redirect();
            } else {
                if (!is_string($insert)) {
                    $validation = $this->model->getErrorValidate();
                    if (empty($validation))
                        $msg = 'Gagal membuat Knowledge';
                    else
                        $msg = implode('<br />', $validation);
                } else
                    $msg = $insert;

                SessionManagerWeb::setFlashMsg(false, $msg);
                redirect($this->ctl . '/add');
            }            
        }    
    }

    function update($id)  {
        if ($_POST) {
            $data = array();
            $data['title'] = $this->input->post('judul_kirim',true);
            $data['description'] = $this->input->post('text_kirim',true);
            $data['postTags'] = $this->input->post('tag',true);
            // $data['description'] = $this->input->post('text_kirim');
            //$data['is_sticky'] = $_POST['stick_kirim'];

            $ret = $this->model->update($data, $id);
            if ($ret) {
                $this->_moveFileTemp($id);
                unset($_SESSION['sigap']['draft_post']);
                $this->db->trans_commit();
                SessionManagerWeb::setFlashMsg(true, 'Berhasil mengubah knowledge');
                
                redirect('web/knowledge/detail/' . $id);
            } else {
                if (empty($validation))
                    $msg = 'Gagal mengubah knowledge';
                else
                    $msg = implode('<br />', $validation);

                SessionManagerWeb::setFlashMsg(false, $msg);
                redirect('web/knowledge/edit/' . $id);
            }            
        }    
    }    

    function ajaxgetpost() {
        $id = (int) $_POST['id'];
        $this->data['_data'] = $this->model->getKnowledge($id);
        $this->load->view('web/_knowledge_detail', $this->data);
        return;
    }

    function stick() {
        if ($_GET) {
            if ($this->input->get('stick')=='1') {
                $data='0';
            } else {
                $data='1';
            }
            $insert = $this->model->setPinPost($_GET['id'],$data);

            $this->redirect();
        }
    }

    public function tag($page = 0) {
        $tag = $this->input->get('param');
        if (empty($tag)) {
            SessionManagerWeb::setFlashMsg(FALSE, 'Invalid Parameter');
            redirect($this->ctl);
        }
        $this->data['buttons'] = $buttons;
        $this->data['title'] = 'Knowledge Management - #' . $tag;
        $this->data['data'] = $this->model->getKnowledges(SessionManagerWeb::getuserID(),NULL,$page, NULL,$tag);
        // $this->data['data'] = $this->model->getKnowledgesByTag($page,$tag);
        // foreach ($this->data['data'] as &$d) {
        //     $d['tag']=$this->model->getPostTag($d['post_id']);
        // }
        $this->load->model('Tag_model');
        $a_tag = $this->Tag_model->getListTags();
        $this->data['a_tag'] = array('' => '') + $a_tag;

        $this->template->viewDefault($this->view, $this->data);
    }

}
