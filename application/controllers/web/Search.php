<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Search extends WEB_Controller {

    protected $title = 'Search';

    function __construct() {
        parent::__construct();
        $this->load->helper('text');
        $this->load->model('Post_model', 'Post');
        $this->model = $this->Post;
        $this->load->model('User_model');
        $this->load->model('Group_member_model');
    }

    /**
     * Halaman daftar post public
     * @param int $page
     */
    public function index($page = 0) {
        $this->db->debug = true;
        unset($_SESSION['group_selected']);
        unset($_SESSION['home_status']);
        $_SESSION['home_status']['dummy'] = 1;
        $this->data['buttons'] = $buttons;
        $this->data['title'] = 'Lini Masa';
        $helpdesk = $this->Group_model->getGroupBy('name',$_SESSION['helpdesk_selected']);

        $this->load->model("Search_log_model");
        $log_data = array(  "keyword" => $this->input->get('search', true),
                            "user_id" => SessionManagerWeb::getUserID());
        $insert = $this->Search_log_model->insert($log_data, TRUE, FALSE);

        $posts = $this->model->getPosts(SessionManagerWeb::getUserID(),$page, $this->input->get('search',true),$_SESSION['home_status'],$helpdesk['id']);
        
        foreach ($posts as &$post) {
            //jika nanti perlu ada limit post yang ditampilkan
            // if (str_word_count($post['post_description']) > 30) {
            //     $post['post_description'] = word_limiter($post['post_description'], 30);
            //     $post['post_description'] .= ' <a href="'.site_url('web/post/detail/' . $post['post_id']).'">Lanjut</a>';
            // }
            // $str=$this->getMatchedString($post['post_description'],$this->input->get('search'));
            $str = $this->input->get('search');
            $temp='';
            $p_desc = explode($str,$post['post_description']);
            $temp=implode("<span style='background-color:yellow'>".$str."</span>",$p_desc);
            unset($post['post_description']);
            $post['post_description']=$temp;
            if ($post['post_title']!=NULL) {
                // $str = $this->getMatchedString($post['post_title'],$this->input->get('search'));
                $str = $this->input->get('search');
                $temp='';
                $p_title = explode($str,$post['post_title']);
                $temp=implode("<span style='background-color:yellow'>".$str."</span>",$p_title);
                unset($post['post_title']);
                $post['post_title']=$temp;
            }
            if ($post['user_name']!=NULL) {
                // $str=$this->getMatchedString($post['user_name'],$this->input->get('search'));
                $str = $this->input->get('search');
                $temp='';
                $p_un = explode($str,$post['user_name']);
                $temp=implode("<span style='background-color:yellow'>".$str."</span>",$p_un);
                unset($post['user_name']);
                $post['user_name']=$temp;
            }
            if ($post['kode_ticket']!=NULL) {
                $temp='';
                $p_kt = explode($this->input->get('search'),$post['kode_ticket']);
                $temp=implode("<span style='background-color:#e08e0b;margin:0;padding:0'>".$this->input->get('search')."</span>",$p_kt);
                unset($post['kode_tiket']);
                $post['kode_ticket']=$temp;
            }
        }

        $this->data['data'] = $posts;
        $t1 = array();
        $t2 = array();
        $t3 = array();
        $t4 = array();
        $t5 = array();
        $counter = array();
        $counter['knowledge'] = 0;
        $counter['group'] = 0;
        $counter['ticket'] = 0;
        $counter['informasi'] = 0;
        $counter['umum'] = 0;
        for ($i=0;$i<5;$i++){
            foreach ($this->data['data'] as $dat) {
                if ($i==0) {
                    if ($dat['post_type']=='T') {
                        if (SessionManagerWeb::isStaff() || $this->Group_member_model->_isAdminGroup(SessionManagerWeb::getUserID())){
                            array_push($t1 ,$dat);
                            $counter['ticket']++;
                        } else {
                            if ($dat['post_user_id']==SessionManagerWeb::getUserId()){
                                array_push($t1 ,$dat);
                                $counter['ticket']++;
                            }
                        }
                        
                    }
                }
                if ($i==1){
                    if ($dat['group_name']=='Umum') {
                        array_push($t2 ,$dat);
                        $counter['umum']++;
                    }
                }
                if ($i==2){
                    if ($dat['post_type']=='G' and $dat['group_name']!='HELPDESK' and $dat['group_name']!='Umum') {
                        array_push($t3 ,$dat);
                        $counter['group']++;
                    }
                }
                if ($i==3){
                    if ($dat['post_type']=='K') {
                        // $dat['group_name']='Knowledge';
                        array_push($t4 ,$dat);
                        $counter['knowledge']++;
                    }
                }
                if ($i==4){
                    if ($dat['post_type']=='I') {
                        // $dat['group_name']='Informasi';
                        array_push($t5 ,$dat);
                        $counter['informasi']++;
                    }
                }
            }
        }
        $this->data['counter']=$counter;
        $this->data['ticket']=$t1;
        $this->data['umum']=$t2;
        $this->data['group']=$t3;
        $this->data['knowledge']=$t4;
        $this->data['informasi']=$t5;
        unset($this->data['data']);

        $this->load->model('User_model', 'User');
        $this->data['list_staff'] = $this->User->getListStaff(SessionManagerWeb::getUserID());

        $this->template->viewDefault($this->view, $this->data);
    }

    /**
     * Halaman daftar post public
     * @param string $page
     */
    public function detail($place) {
        unset($_SESSION['group_selected']);
        unset($_SESSION['home_status']);
        $_SESSION['home_status']['dummy'] = 1;
        $this->data['buttons'] = $buttons;
        $this->data['title'] = 'Lini Masa';
        $posts = $this->model->getPosts(SessionManagerWeb::getUserID(),$page, $this->input->get('search'),$_SESSION['home_status'],$_SESSION['group_selected']);
        foreach ($posts as &$post) {
            // $str=$this->getMatchedString($post['post_description'],$this->input->get('search'));
            $str = $this->input->get('search');
            $temp='';
            $p_desc = explode($str,$post['post_description']);
            $temp=implode("<span style='background-color:yellow'>".$str."</span>",$p_desc);
            unset($post['post_description']);
            $post['post_description']=$temp;
            if ($post['post_title']!=NULL) {
                // $str=$this->getMatchedString($post['post_title'],$this->input->get('search'));
                $str = $this->input->get('search');
                $temp='';
                $p_title = explode($str,$post['post_title']);
                $temp=implode("<span style='background-color:yellow'>".$str."</span>",$p_title);
                unset($post['post_title']);
                $post['post_title']=$temp;
            }
            if ($post['user_name']!=NULL) {
                // $str=$this->getMatchedString($post['user_name'],$this->input->get('search'));
                $str = $this->input->get('search');
                $temp='';
                $p_un = explode($str,$post['user_name']);
                $temp=implode("<span style='background-color:yellow'>".$str."</span>",$p_un);
                unset($post['user_name']);
                $post['user_name']=$temp;
            }
            if ($post['kode_ticket']!=NULL) {
                $temp='';
                $p_kt = explode($this->input->get('search'),$post['kode_ticket']);
                $temp=implode("<span style='background-color:#e08e0b;margin:0;padding:0'>".$this->input->get('search')."</span>",$p_kt);
                unset($post['kode_tiket']);
                $post['kode_ticket']=$temp;
            }
        }

        $this->data['data'] = $posts;
        $t1 = array();
        $t2 = array();
        $t3 = array();
        $t4 = array();
        $t5 = array();
        for ($i=0;$i<5;$i++){
            foreach ($this->data['data'] as $dat) {
                if ($i==0) {
                    if ($dat['post_type']=='T') {
                        array_push($t1 ,$dat);
                    }
                }
                if ($i==1){
                    if ($dat['group_name']=='Umum') {
                        array_push($t2 ,$dat);
                    }
                }
                if ($i==2){
                    if ($dat['post_type']=='G' and $dat['group_name']!='HELPDESK' and $dat['group_name']!='Umum') {
                        array_push($t3 ,$dat);
                    }
                }
                if ($i==3){
                    if ($dat['post_type']=='K') {
                        $dat['group_name']='Knowledge';
                        array_push($t4 ,$dat);
                    }
                }
                if ($i==4){
                    if ($dat['post_type']=='I') {
                        $dat['group_name']='Informasi';
                        array_push($t5 ,$dat);
                    }
                }
            }
        }
        unset($this->data['data']);
        if ($place=="ticket"){
            $this->data['data']=$t1;
        } else if ($place=="umum"){
            $this->data['data']=$t2;
        } else if ($place=="group"){
            $this->data['data']=$t3;
        } else if ($place=="knowledge"){
            $this->data['data']=$t4;
        } else {
            $this->data['data']=$t5;
        }
        $this->load->model('User_model', 'User');
        $this->data['list_staff'] = $this->User->getListStaff(SessionManagerWeb::getUserID());
        $this->template->viewDefault($this->view, $this->data);
    }

     /**
     * Halaman daftar post public
     * @param string $page
     */
    public function rating($page) {
        $this->data['buttons'] = $buttons;
        $this->data['title'] = 'Lini Masa';
        $posts = $this->model->notRatedPosts(SessionManagerWeb::getUserID(),$page, $this->input->get('search'));
        foreach ($posts as &$post) {
            // $post['post_description']=$this->checkUrl($post['post_description']);
            // die();
            $post['post_description']=$this->checkURL($post['post_description']);
            if (str_word_count($post['post_description']) > 30) {
                $post['post_description'] = word_limiter($post['post_description'], 30);
                $post['post_description'] .= ' <a href="'.site_url('web/post/detail/' . $post['post_id']).'">Lanjut</a>';
            }
        }

        $this->data['data'] = $posts;

        $this->load->model('User_model', 'User');
        $this->data['list_staff'] = $this->User->getListStaff(SessionManagerWeb::getUserID());
        $this->template->viewDefault($this->view, $this->data);
    }

    public function ticketDone($user_id=NULL, $page){
        $this->data['buttons'] = $buttons;
        $this->data['title'] = 'Lini Masa';
        if ($_GET) {
            $this->data['date_start']=$this->input->get('start_date');
            $this->data['date_end']=$this->input->get('end_date');
            if ($_GET['h']!='' || $_GET['h']!=NULL){
                $group_helpdesk = $this->Group_model->getGroupBy('name',$this->input->get('h'));
            }
        } else {
            unset($this->data['date_start']);
            unset($this->data['date_end']);
        }
        $posts = $this->model->getDoneTicket($user_id, $group_helpdesk['id'] , $page, $this->data['date_start'],$this->data['date_end']);
        foreach ($posts as &$post) {
            // $post['post_description']=$this->checkUrl($post['post_description']);
            // die();
            $post['post_description']=$this->checkURL($post['post_description']);
            if (str_word_count($post['post_description']) > 30) {
                $post['post_description'] = word_limiter($post['post_description'], 30);
                $post['post_description'] .= ' <a href="'.site_url('web/post/detail/' . $post['post_id']).'">Lanjut</a>';
            }
        }

        $this->data['data'] = $posts;

        $this->load->model('User_model', 'User');
        $this->data['list_staff'] = $this->User->getListStaff(SessionManagerWeb::getUserID());
        // echo '<pre>';
        // var_dump($posts);
        // die();
        $this->template->viewDefault($this->class.'_rating', $this->data);
    }

}
