<?php

defined('BASEPATH') OR exit('No direct script access allowed');


class Holiday extends WEB_Controller {

	function index() {
		$data['data'] = $this->model->get_all();

		// SET WARNA
		foreach ($data['data'] as $key => $value) {
			$data['data'][$key]['color'] = 'red';
		}
		$data['json'] = json_encode($data['data']);

		// {date: yyyy-mm-dd, badge: boolean, title: string, body: string, footer: string, classname: string}
		$this->data['data'] = $data;

		$this->template->viewDefault($this->view,$this->data);
	}

	function countWorkDays($start_date, $end_date){
		$start_date = "2018-5-18";
		$end_date = "2018-5-21";
		var_dump($this->model->getWorkDays($start_date, $end_date));
		return $this->model->getWorkDays($start_date, $end_date);
	}

	function importmanual($filename){
		$ical = new ICAL(site_url('assets/uploads/files/'.$filename));
		$events = $ical->events();
		// echo "<pre>";
		// var_dump($events);
		// die();
		foreach ($events as $key => $value) {
			$data = array(
				'name' => substr($value['SUMMARY;LANGUAGE=en-us'], 11),
				'date' => $value['DTSTART'],
				'description' => $value['DESCRIPTION'],
			);
			$insert = $this->model->insert($data);
			if(!$insert){
				var_dump($insert);
				die();
			}
		}
	}

    function add() {
        $this->edit();
    }

    public function edit($id = null) {
        if ($id) {
            $this->data['data'] = $this->model->get($id);
            $this->data['id'] = $id;
        }

        $this->template->viewDefault('holiday_edit', $this->data);
    }

    function create() {
        $data = $this->input->post(null,true);

        $this->model->create($data);
        $this->redirect('web/holiday');
    }

    function update($id) {
        $data = $this->input->post(null,true);
        $this->model->update($id,$data);
        $this->redirect('web/holiday');
    }

    function delete($id) {
        $this->model->delete($id);
        $this->redirect('web/holiday');
    }


}
