<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Replies extends WEB_Controller {

    protected $title = 'Daftar Jawaban';

    function __construct() {
        parent::__construct();
        $this->load->model('Replies_model', 'Model');
    }

    public function index($page=1) {
        $this->data['limit'] = $this->Model->limit;
        $this->data['pagination'] = ceil($this->Model->getCount($helpdesk_selected) / $this->Model->limit);
        $this->data['data'] = $this->Model->getList($page,SessionManagerWeb::getUserID());
        $this->data['show_helpdesk'] = $helpdesk_selected;
        $this->data['title'] = 'Daftar Jawaban';
        // echo "<pre>";print_r($this->data);die();
        $this->template->viewDefault($this->class . '_list', $this->data);
    }

    function add() {
        $this->edit();
    }

    public function edit($id = null) {
        if ($_POST['id_comment']) {
            $_SESSION['reply_edit'] = $_POST['id_comment'];
            exit;
        }

        if ($_SESSION['reply_edit']){
            $query = $this->db->query("SELECT text from comments WHERE id=".$_SESSION['reply_edit'].";");
            foreach ($query->result() as $row) {
                $comment = $row->text;
            }
            $this->data['data']['reply']=$comment;
            unset($_SESSION['reply_edit']);
        }

        if ($id) {
            $this->data['data'] = $this->Model->getRow($id);
            $this->data['id'] = $id;
        }

        $this->template->viewDefault('replies_edit', $this->data);
    }

    function create() {
        $data = $this->input->post(null,true);
        $data['user_id'] = SessionManagerWeb::getUserID();
        // echo "<pre>";print_r($data);die();
        $this->model->create($data);
        $this->redirect('web/replies');
    }

    function update($id) {
        $data = $this->input->post(null,true);
        $this->model->update($id,$data);
        $this->redirect('web/replies');
    }

    function delete($id) {
        $this->model->delete($id);
        $this->redirect('web/replies');
    }


}