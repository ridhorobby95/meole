<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Statistik extends WEB_Controller {

    protected $title = 'Statistik';
    

    function __construct() {
        parent::__construct();
        $this->load->helper('text');
        $this->load->model('Statistik_model', 'Statistik');
        $this->model = $this->Statistik;
    }

    function index($is_kopertis=false) {
        $this->data['my_helpdesk_name'] = $this->data['list_my_helpdesk'][0]['name'];
        if (SessionManagerWeb::isSuperAdministrator()){
            $this->data['my_helpdesk_name'] = '';
        } 
        $this->data['is_kopertis'] = $is_kopertis;
        
        $this->template->viewDefault($this->view,$this->data);
    }

    function helpdesk($is_kopertis=false) {
        $this->data['my_helpdesk_name'] = $this->data['list_my_helpdesk'][0]['name'];
        if (SessionManagerWeb::isSuperAdministrator()){
            $this->data['my_helpdesk_name'] = '';
        } 
        $this->data['is_kopertis'] = $is_kopertis;
        
        $this->template->viewDefault($this->view,$this->data);
    }

    function knowledge($is_kopertis=false) {
        $this->data['my_helpdesk_name'] = $this->data['list_my_helpdesk'][0]['name'];
        if (SessionManagerWeb::isSuperAdministrator()){
            $this->data['my_helpdesk_name'] = '';
        } 
        $this->data['is_kopertis'] = $is_kopertis;
        
        $this->template->viewDefault($this->view,$this->data);
    }

    function user($is_kopertis=false) {
        $this->data['my_helpdesk_name'] = $this->data['list_my_helpdesk'][0]['name'];
        if (SessionManagerWeb::isSuperAdministrator()){
            $this->data['my_helpdesk_name'] = '';
        } 
        $this->data['is_kopertis'] = $is_kopertis;
        
        $this->template->viewDefault($this->view,$this->data);
    }

    function performance(){
        $helpdesk_default = $this->Group_model->getGroupDefault($user_id);
        if ($_GET) {
            $this->data['date_start']=$this->input->get('start_date');
            $this->data['date_end']=$this->input->get('end_date');
            $this->data['chosen_helpdesk'] = $this->input->get('h');
        } else {
            unset($this->data['date_start']);
            unset($this->data['date_end']);
            unset($this->data['chosen_helpdesk']);
        }
        if(SessionManagerWeb::isSuperAdministrator())
            $this->data['list_helpdesk'][] = array("name" => "Kopertis");

        if ($this->data['chosen_helpdesk']!=NULL){
            if($this->data['chosen_helpdesk'] == 'Kopertis'){
                $this->data['temp_all'] = $this->model->getUserStat(NULL, $this->data['date_start'],$this->data['date_end'], true);
                $this->data['data'] = $this->model->getUserStatRatingedTicket(NULL, $this->data['date_start'],$this->data['date_end'], true);
                $this->data['std_all'] = $this->model->getStd(NULL,$this->data['date_start'],$this->data['date_end'], true);
                $this->data['std'] = $this->model->getStdRatingedTicket(NULL,$this->data['date_start'],$this->data['date_end'], true);
            }else{
                $this->data['temp_all'] = $this->model->getUserStat($this->data['chosen_helpdesk'],$this->data['date_start'],$this->data['date_end']);
                $this->data['data'] = $this->model->getUserStatRatingedTicket($this->data['chosen_helpdesk'],$this->data['date_start'],$this->data['date_end']);
                $this->data['std_all'] = $this->model->getStd($this->data['chosen_helpdesk'],$this->data['date_start'],$this->data['date_end']);
                $this->data['std'] = $this->model->getStdRatingedTicket($this->data['chosen_helpdesk'],$this->data['date_start'],$this->data['date_end']);
            }

            $counter=0;
            foreach ($this->data['temp_all'] as $key => $value) {
                $is_in = 0;
                foreach ($this->data['data'] as $k => $v) {
                    if ($value['id']==$v['id']){
                        $counter++;
                        $is_in=1;
                    }
                }
                if ($is_in==0) {
                    $counter++;
                    $this->data['data'][$counter] = $value;
                }
            }
            foreach ($this->data['data'] as &$data){
                // $data['track'] = "round((0.5*".(string)$data['s']."/".(string)$this->data['std'][0]['avg_s']."+0.4*".(string)$this->data['std'][0]['avg_process']."/".(string)$data['processing_time']."+0.1* ".(string)$this->data['std'][0]['avg_response']."/".(string)$data['response_time'].")*".(string)$data['solvability']."/10,3)";
                // $data['poin'] = round((0.5*$data['s']/$this->data['std'][0]['avg_s']+0.4*$this->data['std'][0]['avg_process']/$data['processing_time']+0.1*$this->data['std'][0]['avg_response']/$data['response_time'])*$data['solvability']/10,3);

                // track
                // $data['track'] = "round(((0.5*".(string)$this->data['std'][0]['avg_process']."/".(string)$data['processing_time']."+0.5* ".(string)$this->data['std'][0]['avg_response']."/".(string)$data['response_time'].")*".(string)$data['solvability'].")".(string)$data['s']."/".(string)$this->data['std_all'][0]['total_s'].",3)";

                // point
                $data['poin'] = round(((0.5*$this->data['std'][0]['avg_process']/$data['processing_time']+0.5*$this->data['std'][0]['avg_response']/$data['response_time'])*$data['solvability'])*$data['s']/$this->data['std_all'][0]['total_s'],3);

            }

            //sorting
            foreach ($this->data['data'] as $key => $row){
                $user = $this->User_model->getUser($row['id']);
                $this->data['data'][$key]['user_photo'] = $this->User_model->getUserPhoto($row['id'],$user['photo']);
                $poin[$key] = $row['poin'];
                $ticket_selesai[$key]=$row['s'];
            }

            array_multisort($poin, SORT_DESC, $ticket_selesai, SORT_DESC, $this->data['data']);
            foreach ($this->data['data'] as $key => $value) {
                foreach ($this->data['temp_all'] as $k => $v) {
                    if ($value['id']==$v['id']){
                        $this->data['data_all'][$key] = $v;
                    }
                }
                if($this->data['chosen_helpdesk'] == 'Kopertis'){
                    $kopertis = $this->Group_member_model->getAdminKopertis($value['id']);
                    $this->data['data'][$key]['name'] = $value['name'].'( '.$kopertis['name'].' )';
                }
            }

            // $this->data['chosen_helpdesk'] = $helpdesk;
        }
        // echo "<pre>";print_r($this->data['std']);die();
        $this->template->viewDefault($this->view, $this->data);
    }

    function performanceKopertis(){
        if ($_GET) {
            $this->data['date_start']=$this->input->get('start_date');
            $this->data['date_end']=$this->input->get('end_date');
            $date_start = $this->data['date_start'];
            $date_end = $this->data['date_end'];
        } else {
            unset($this->data['date_start']);
            unset($this->data['date_end']);

        }
        
        $this->data['temp_all'] = $this->model->getUserStat(NULL, $this->data['date_start'],$this->data['date_end'], true);
        $this->data['data'] = $this->model->getUserStatRatingedTicket(NULL, $this->data['date_start'],$this->data['date_end'], true);
        $this->data['std_all'] = $this->model->getStd(NULL,$this->data['date_start'],$this->data['date_end'], true);
        $this->data['std'] = $this->model->getStdRatingedTicket(NULL,$this->data['date_start'],$this->data['date_end'], true);

        $counter=0;
        foreach ($this->data['temp_all'] as $key => $value) {
            $is_in = 0;
            foreach ($this->data['data'] as $k => $v) {
                if ($value['id']==$v['id']){
                    $counter++;
                    $is_in=1;
                }
            }
            if ($is_in==0) {
                $counter++;
                $this->data['data'][$counter] = $value;
            }
        }
        foreach ($this->data['data'] as &$data){
            // $data['track'] = "round((0.5*".(string)$data['s']."/".(string)$this->data['std'][0]['avg_s']."+0.4*".(string)$this->data['std'][0]['avg_process']."/".(string)$data['processing_time']."+0.1* ".(string)$this->data['std'][0]['avg_response']."/".(string)$data['response_time'].")*".(string)$data['solvability']."/10,3)";
            // $data['poin'] = round((0.5*$data['s']/$this->data['std'][0]['avg_s']+0.4*$this->data['std'][0]['avg_process']/$data['processing_time']+0.1*$this->data['std'][0]['avg_response']/$data['response_time'])*$data['solvability']/10,3);
            $data['track'] = "round(((0.5*".(string)$this->data['std'][0]['avg_process']."/".(string)$data['processing_time']."+0.5* ".(string)$this->data['std'][0]['avg_response']."/".(string)$data['response_time'].")*".(string)$data['solvability'].")".(string)$data['s']."/".(string)$this->data['std_all'][0]['total_s'].",3)";

            $data['poin'] = round(((0.5*$this->data['std'][0]['avg_process']/$data['processing_time']+0.5*$this->data['std'][0]['avg_response']/$data['response_time'])*$data['solvability'])*$data['s']/$this->data['std_all'][0]['total_s'],3);

        }

        //sorting
        foreach ($this->data['data'] as $key => $row){
            // $this->load->model('Group_member_model');
            // $user = $this->Group_member_model->getAllAdminKopertis();
            $user = $this->User_model->getUser($row['id']);
            $this->data['data'][$key]['user_photo'] = $this->User_model->getUserPhoto($row['id'],$user['photo']);
            $poin[$key] = $row['poin'];
            $ticket_selesai[$key]=$row['s'];
        }

        array_multisort($poin, SORT_DESC, $ticket_selesai, SORT_DESC, $this->data['data']);
        foreach ($this->data['data'] as $key => $value) {
            foreach ($this->data['temp_all'] as $k => $v) {
                if ($value['id']==$v['id']){
                    $this->data['data_all'][$key] = $v;
                }
            }
            $kopertis = $this->Group_member_model->getAdminKopertis($value['id']);
            $this->data['data'][$key]['name'] = $value['name'].'( '.$kopertis['name'].' )';
        }
        $this->template->viewDefault($this->view, $this->data);
    }

    function beban(){
        if ($_GET['h']) {
            $this->data['chosen_helpdesk'] = $this->input->get('h',true);
            // $helpdesk = $this->Group_model->getGroupBy('name',$this->data['chosen_helpdesk']);
        }

        if(SessionManagerWeb::isSuperAdministrator())
            $this->data['list_helpdesk'][] = array("name" => "Kopertis");

        if (!SessionManagerWeb::isSuperAdministrator()) {
            $my_helpdesk = $this->data['list_my_helpdesk'][0];
            $this->data['chosen_helpdesk'] = $my_helpdesk['name'];
        }

        if($this->data['chosen_helpdesk'] == 'Kopertis'){
            $this->data['data'] = $this->model->getUserBeban(NULL, true);
            $this->data['avg_bobot'] = $this->model->getAvgUserBeban(NULL, true);
            $this->load->model('Group_member_model');
            foreach ($this->data['data'] as $key => $value) {
                $kopertis = $this->Group_member_model->getAdminKopertis($value['id']);
                $this->data['data'][$key]['name'] = $value['name'].'( '.$kopertis['name'].' )';
            }
        }else{
            $this->data['data'] = $this->model->getUserBeban($this->data['chosen_helpdesk']);
            $this->data['avg_bobot'] = $this->model->getAvgUserBeban($this->data['chosen_helpdesk']);
        }

        $this->template->viewDefault($this->view, $this->data);
    }

    function bebanKopertis(){
        $this->data['data'] = $this->model->getUserBeban(NULL, true);
        $this->data['avg_bobot'] = $this->model->getAvgUserBeban(NULL, true);
        $this->load->model('Group_member_model');
        foreach ($this->data['data'] as $key => $value) {
            $kopertis = $this->Group_member_model->getAdminKopertis($value['id']);
            $this->data['data'][$key]['name'] = $value['name'].'( '.$kopertis['name'].' )';
        }
        $this->template->viewDefault($this->view, $this->data);
    }

    function ticket(){
        if ($_GET) {
            $this->data['date_start']=$this->input->get('start_date');
            $this->data['date_end']=$this->input->get('end_date');
            $this->data['chosen_helpdesk'] = $this->input->get('h');
            // $helpdesk = $this->Group_model->getGroupBy('name',$this->data['chosen_helpdesk']);
        } else {
            unset($this->data['date_start']);
            unset($this->data['date_end']);
            unset($this->data['chosen_helpdesk']);
        }
        // $this->data['data']=$this->model->getStatTicket($this->data['date_start'],$this->data['date_end']);
        if (!SessionManagerWeb::isSuperAdministrator()) {
            $my_helpdesk = $this->data['list_my_helpdesk'][0];
            $this->data['chosen_helpdesk'] = $my_helpdesk['name'];
        }

        /*
         * SETUP DATA UNTUK STATISTIK BARCHART RATING
         */
        $this->data['data'] = $this->model->getStatRating($this->data['chosen_helpdesk'],$this->data['date_start'],$this->data['date_end']);

        /*
         * SETUP DATA UNTUK STATISTIK RATING
         */
        $dRating = $this->model->getRatingData($this->data['chosen_helpdesk'],$this->data['date_start'],$this->data['date_end']);

        foreach ($dRating as $key => $value) {
            $total = $value['rating_puas'] + $value['rating_tidak_puas'];
            $value['rating_puas'] = round($value['rating_puas'] / $total * 100);
            $value['rating_tidak_puas'] = round($value['rating_tidak_puas'] / $total * 100);
            $this->data['rating'][$key] = $value;
        }

        /*
         * SETUP DATA UNTUK STATISTIK JUMLAH TIKET
         */
        $created = array();
        $solved = array();
        $dHelpdesk = $this->model->getTicketRange($this->data['chosen_helpdesk'],$this->data['date_start'],$this->data['date_end']);
        foreach ($dHelpdesk as $k => $d) {
            $created_arr = $this->model->getTicketCreated($this->data['chosen_helpdesk'],$d['month'],$d['year']);
            $created[] = $created_arr['created'];
            $solved_arr = $this->model->getTicketSolved($this->data['chosen_helpdesk'],$d['month'],$d['year']);
            $solved[] = $solved_arr['solved'];
            $data['ticket'][$k]['month'] = $d['month'];
            $data['ticket'][$k]['year'] = $d['year'];
            $data['ticket'][$k]['open'] = $created_arr['created'];
            $data['ticket'][$k]['solved'] = $solved_arr['solved'];
        }

        $index=0;
        $this->data['month']=array();
        $this->data['ticket_open']=array();
        $this->data['ticket_solved'] = array();
        $this->data['response_time']=array();
        $this->data['processing_time']=array();
        $this->data['solving_time']=array();
        foreach ($data['ticket'] as $d) {
            $this->data['month'][$index]=date('M', mktime(0, 0, 0, (int)$d['month'], 10)).'-'.$d['year'];
            if ($d['open']==NULL){
                $this->data['ticket_open'][$index]=0;
            } else {
                $this->data['ticket_open'][$index]=(int)$d['open'];
            }
            if ($d['solved']==NULL){
                $this->data['ticket_solved'][$index]=0;
            } else {
                $this->data['ticket_solved'][$index]=(int)$d['solved'];
            }
            $index++;
        }

        /*
         * SETUP DATA UNTUK STATISTIK WAKTU PENGERJAAN TIKET
         */
        $dWaktu = $this->model->getStatWaktu($this->data['chosen_helpdesk'],$this->data['date_start'],$this->data['date_end']);
        $index=0;
        foreach ($dWaktu as $d) {
            $this->data['response_time'][$index]=floor($d['response_time']/60/24);
            $this->data['processing_time'][$index]=floor($d['processing_time']/60/24);
            $this->data['solving_time'][$index]=floor($d['solving_time']/60/24);
            $index++;
        }
        // echo '<pre>';
        // var_dump($this->data['month'], $this->data['ticket_open'], $this->data['ticket_solved']);
        // die();
        $this->template->viewDefault($this->view, $this->data);
    }

    function waktu(){
        if ($_GET) {
            $this->data['date_start']=$this->input->get('start_date');
            $this->data['date_end']=$this->input->get('end_date');
            $this->data['chosen_helpdesk'] = $this->input->get('h');
        } else {
            unset($this->data['date_start']);
            unset($this->data['date_end']);
            unset($this->data['chosen_helpdesk']);
        }
        if (!SessionManagerWeb::isSuperAdministrator()) {
            $my_helpdesk = $this->data['list_my_helpdesk'][0];
            $this->data['chosen_helpdesk'] = $my_helpdesk['name'];
        }
        $this->data['data'] = $this->model->getStatWaktu($this->data['chosen_helpdesk'],$this->data['date_start'],$this->data['date_end']);
        $this->data['month'] = array();
        // $this->data['year'] = array();
        $this->data['response_time']=array();
        $this->data['processing_time']=array();
        $this->data['solving_time']=array();
        $index=0;
        foreach ($this->data['data'] as $d) {
            $this->data['month'][$index]=date('M', mktime(0, 0, 0, (int)$d['month'], 10)).'-'.$d['year'];
            // $this->data['year']=$d['year'];
            $this->data['response_time'][$index]=floor($d['response_time']/60/24);
            $this->data['processing_time'][$index]=floor($d['processing_time']/60/24);
            $this->data['solving_time'][$index]=floor($d['solving_time']/60/24);
            $index++;
        }
        $this->template->viewDefault($this->view, $this->data);
    }

    function rating(){
        if ($_GET) {
            $this->data['date_start']=$this->input->get('start_date');
            $this->data['date_end']=$this->input->get('end_date');
            $this->data['chosen_helpdesk'] = $this->input->get('h');
        } else {
            unset($this->data['date_start']);
            unset($this->data['date_end']);
            unset($this->data['chosen_helpdesk']);
        }
        if (!SessionManagerWeb::isSuperAdministrator()) {
            $my_helpdesk = $this->data['list_my_helpdesk'][0];
            $this->data['chosen_helpdesk'] = $my_helpdesk['name'];
        }
        $this->data['data'] = $this->model->getStatRating($this->data['chosen_helpdesk'],$this->data['date_start'],$this->data['date_end']);
        $this->template->viewDefault($this->view, $this->data);
    }

    function keyword(){
        $this->data['data'] = $this->model->getStatKeyword();
        
        $this->data['chart_data'] = 
        array(
            array(
                'label' => 'Tiket Ada KM',
                'fill' => false,
                'backgroundColor' => 'green',
            ),
            array(
                'label' => 'Tiket Tidak Ada KM',
                'fill' => false,
                'backgroundColor' => 'red',
            ),
        );
        foreach ($this->data['data'] as $key => $data) {

            if($data['tiket'] && !$data['knowledge']){
                $this->data['chart_data'][1]['data'][] = $data['tiket'];
                $this->data['chart_data'][0]['data'][] = 0;
            }
            else{
                $this->data['chart_data'][1]['data'][] = 0;
                $this->data['chart_data'][0]['data'][] = $data['tiket'];
            }

            $this->data['keywords'][] = $data['name'];
        }

        // echo "<pre>";
        // var_dump(json_encode($this->data['chart_data']));
        // die();
        $this->template->viewDefault($this->view, $this->data);
    }

    function search(){
        if ($_GET) {
            $this->data['date_start']=$this->input->get('start_date');
            $this->data['date_end']=$this->input->get('end_date');
        } else {
            unset($this->data['date_start']);
            unset($this->data['date_end']);
        }
        
        $filter_date = null;
        if($this->data['date_start'] && $this->data['date_end']){
            $filter_date['date_start'] = FormatterWeb::formatDate($this->data['date_start']);
            $filter_date['date_end'] = FormatterWeb::formatDate($this->data['date_end']);
        }

        $this->data['data'] = $this->model->getStatSearch($filter_date['date_start'], $filter_date['date_end']);

        $this->template->viewDefault($this->view, $this->data);
    }

    function system(){
        $this->data['data'] = $this->model->getLog();
        $this->template->viewDefault($this->view, $this->data);
    }

}

?>