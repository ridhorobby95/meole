<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Post extends WEB_Controller {

    protected $title = 'Post';

    function __construct() {
        parent::__construct();
        $this->load->helper('text');
        $this->data['nocover'] = base_url('assets/web/img/cover.jpg');
        $this->data['noimg'] = base_url('assets/web/img/logo.png');
    }



    /**
     * Halaman daftar post public
     * @param int $page
     */
    public function index($page = 0) {
        if($_GET['type']){
            $dinas_id = SessionManagerWeb::getDinasID();
            $this->data['type'] = $_GET['type'];
            if($_GET['type'] == 'M'){
                $this->data['title'] = 'Maps Helpdesk';
                $this->load->model('Unitkerja_model');
                $this->data['dinas'] = $this->Unitkerja_model->column('idunit, name')->order('name asc')->show_sql(false)->getAll('where level=2');

                $this->data['listPost_public'] = $this->model->filter("where is_private=0".(SessionManagerWeb::isAdministrator() ? "" : " and dinas_id=$dinas_id"))->getAll('semua', SessionManagerWeb::getUserID());
                $this->data['listPost_private'] = $this->model->filter("where is_private=1".(SessionManagerWeb::isAdministrator() ? "" : " and dinas_id=$dinas_id"))->getAll('semua', SessionManagerWeb::getUserID());
                // echo "<pre>";print_r($this->data['listPost']);die();;
                $this->template->viewDefaultMaps('post_peta', $this->data);
            }else{
                if($_GET['type'] == 'T'){
                // echo "<pre>";print_r($_SESSION['meole_search']);die();
                $this->data['title'] = 'Linimasa';
                $this->load->model('Unitkerja_model');
                $this->data['dinas'] = $this->Unitkerja_model->column('idunit, name')->order('name asc')->show_sql(false)->getAll('where level=2');
                $this->data['favorit'] = $this->model->getFavorit((!SessionManagerWeb::isAdministrator()) ? " and dinas_id=".SessionManagerWeb::getDinasID() : null ,false);
                if(SessionManagerWeb::isAdministrator()){
                    $filter = "where dinas_id is not null";
                }else{
                    $filter = "where dinas_id=$dinas_id";
                }
                $order = "asc";
                if($_SESSION['meole_search']){
                    if($_SESSION['meole_search']['keyword']){
                        $keyword = $_SESSION['meole_search']['keyword'];
                        $filter .= " and description like '%$keyword%' ";
                    }
                    if($_SESSION['meole_search']['order']){
                        $order = $_SESSION['meole_search']['order'];
                    }
                    if($_SESSION['meole_search']['private']){
                        $private = $_SESSION['meole_search']['private'];
                        $filter .=" and is_private=".$private;
                    }
                    if($_SESSION['meole_search']['status']){

                        $filter_status = "";
                        $status = $_SESSION['meole_search']['status'];
                        $temp = 0;
                        foreach ($status as $stat) {
                            if($temp == 0){
                                $filter_status .="'".$stat."'";
                            }else{
                                $filter_status .=",'".$stat."'";
                            }
                            $temp++;
                        }
                        // echo $filter_status;die();
                        $filter .=" and status in ($filter_status)";
                    }
                    if($_SESSION['meole_search']['dinas']){
                        $filter_dinas = "";
                        $dinas = $_SESSION['meole_search']['dinas'];
                        $temp = 0;
                        foreach ($dinas as $din) {
                            if($temp == 0){
                                $filter_dinas .=$din;
                            }else{
                                $filter_dinas .=",".$din;
                            }
                            $temp++;
                        }
                        // echo $filter_status;die();
                        $filter .=" and dinas_id in ($filter_dinas)";
                    }
                }
                // echo $order;die();
                $this->data['data'] = $this->model->filter($filter)->getAll($page*10,SessionManagerWeb::getUserID(), $order);
                $this->data['list_private'] = Status::getArrayPrivate();
                $this->data['list_status'] = Status::getArray();
                $this->data['jumlah_data'] = dbGetCount('select * from posts '.(SessionManagerWeb::isAdministrator() ? "$filter" : " where dinas_id=$dinas_id"));
               
                // $page = 0;
                // if($_GET['page']){
                //     $page = $_GET['page'];
                // }
                // echo $this->data['jumlah_data'];die();
                if(($page+1) * 10 <= $this->data['jumlah_data']){
                    $this->data['next_page'] = true;
                }
                $this->data['page'] = $page;
                // echo "<pre>";print_r($this->data['belum_rating']);die();
                $this->template->viewDefault($this->view, $this->data);
                }
            }
        }else{
            $this->data['title'] = 'Linimasa Helpdesk';
            $this->load->model('Unitkerja_model');
            $this->data['dinas'] = $this->Unitkerja_model->column('idunit, name')->order('name asc')->show_sql(false)->getAll('where level=2');

            $this->data['listPost_public'] = $this->model->filter("where is_private=0".(SessionManagerWeb::isAdministrator() ? "" : " and dinas_id=$dinas_id"))->getAll($page,SessionManagerWeb::getUserID());
            $this->data['listPost_private'] = $this->model->filter("where is_private=1".(SessionManagerWeb::isAdministrator() ? "" : " and dinas_id=$dinas_id"))->getAll($page,SessionManagerWeb::getUserID());
            // echo "<pre>";print_r($this->data['listPost']);die();;
            $this->template->viewDefaultMaps('post_peta', $this->data);
        }
        
        
    }

    public function unRated($page=0){
        $this->data['title'] = 'Post Belum Dirating';
        $id_toList = Util::toList($this->data['belum_rating'],'id');
        $ids = implode(',', $id_toList);
        $filter = " where p.id in ($ids)";
        $this->data['data'] = $this->model->filter($filter)->getAll($page*10,SessionManagerWeb::getUserID(), $order);
        $this->data['jumlah_data'] = dbGetCount("select * from posts p $filter");
        if(($page+1) * 10 <= $this->data['jumlah_data']){
            $this->data['next_page'] = true;
        }
        $this->data['page'] = $page;
        $this->data['favorit'] = $this->model->getFavorit((!SessionManagerWeb::isAdministrator()) ? " and dinas_id=".SessionManagerWeb::getDinasID() : null ,false);
        $this->data['nofilter'] = true;
        $this->template->viewDefault('post_index', $this->data);
        // echo "<pre>";print_r($this->data['data']);die();
        
    }

    public function rates(){
        $this->data['title'] = 'Post Dirating';
        $this->data['nofilter'] = true;
        $this->data['data'] = $this->model->filter('and pa.user_id='.SessionManagerWeb::getUserID())->getAllRates($page*10,SessionManagerWeb::getUserID(), $order, SessionManagerWeb::getRole());
        $this->data['jumlah_data'] = dbGetCount("select p.id from posts p left join post_assign pa p.id = pa.post_id where status='S' and pa.user_id=".SessionManagerWeb::getUserID());
        if(($page+1) * 10 <= $this->data['jumlah_data']){
            $this->data['next_page'] = true;
        }
        $this->data['page'] = $page;
        $this->data['favorit'] = $this->model->getFavorit((!SessionManagerWeb::isAdministrator()) ? " and dinas_id=".SessionManagerWeb::getDinasID() : null ,false);
        $this->data['nofilter'] = true;
        $this->template->viewDefault('post_index', $this->data);
        // echo "<pre>";print_r($this->data['data']);die();
    }

    public function rating(){
        $data = $this->input->post();
        $data['tipe'] = SessionManagerWeb::getRole();
        $this->load->model('Post_rating_model');
        $insert = $this->Post_rating_model->create($data,true,true);
        if($insert){
            echo $data['rating'];
        }else{
            return false;
        }
        // echo "<pre>";print_r($data);die();
    }

    public function dashboard($status = null, $page=0) {
        $dinas_id = SessionManagerWeb::getDinasID();
        $this->data['title'] = "Beranda";
        if($status!= null){
            $this->load->model('Unitkerja_model');
            $this->data['dinas'] = $this->Unitkerja_model->column('idunit, name')->order('name asc')->show_sql(false)->getAll('where level=2');
            $this->data['data'] = $this->model->filter("where status='".$status."'".(SessionManagerWeb::isAdministrator() ? "" : " and dinas_id=$dinas_id"))->getAll($page,SessionManagerWeb::getUserID());
            $this->template->viewDefault('post_index', $this->data);
        }else{
            $this->data['data'][Status::STATUS_DILAPORKAN]['data'] = $this->model->filter("where p.status='".Status::STATUS_DILAPORKAN."'".(SessionManagerWeb::isAdministrator() ? "" : " and dinas_id=$dinas_id and pa.user_id=".SessionManagerWeb::getUserID()))->getAll(0, SessionManagerWeb::getUserID());
            $this->data['data'][Status::STATUS_DILAPORKAN]['jumlah'] = count($this->data['data'][Status::STATUS_DILAPORKAN]['data']);
            $this->data['data'][Status::STATUS_RESPON]['data'] = $this->model->filter("where p.status='".Status::STATUS_RESPON."'".(SessionManagerWeb::isAdministrator() ? "" : " and dinas_id=$dinas_id and pa.user_id=".SessionManagerWeb::getUserID()))->getAll(0,SessionManagerWeb::getUserID());
            $this->data['data'][Status::STATUS_RESPON]['jumlah'] = count($this->data['data'][Status::STATUS_RESPON]['data']);
            $this->data['data'][Status::STATUS_PROSES]['data'] = $this->model->filter("where p.status='".Status::STATUS_PROSES."'".(SessionManagerWeb::isAdministrator() ? "" : " and dinas_id=$dinas_id and pa.user_id=".SessionManagerWeb::getUserID()))->getAll(0,SessionManagerWeb::getUserID());
            $this->data['data'][Status::STATUS_PROSES]['jumlah'] = count($this->data['data'][Status::STATUS_PROSES]['data']);
            $this->data['data'][Status::STATUS_SELESAI]['data'] = $this->model->filter("where p.status='".Status::STATUS_SELESAI."'".(SessionManagerWeb::isAdministrator() ? "" : " and dinas_id=$dinas_id and pa.user_id=".SessionManagerWeb::getUserID()))->getAll(0,SessionManagerWeb::getUserID());
            $this->data['data'][Status::STATUS_SELESAI]['jumlah'] = sizeof($this->data['data'][Status::STATUS_SELESAI]['data']);
            $this->data['data'][Status::STATUS_DIARSIPKAN]['data'] = $this->model->filter("where p.status='".Status::STATUS_DIARSIPKAN."'".(SessionManagerWeb::isAdministrator() ? "" : " and dinas_id=$dinas_id and pa.user_id=".SessionManagerWeb::getUserID()))->getAll(0,SessionManagerWeb::getUserID());
            $this->data['data'][Status::STATUS_DIARSIPKAN]['jumlah'] = sizeof($this->data['data'][Status::STATUS_DIARSIPKAN]['data']);

            // print_r($this->data['data'][Status::STATUS_SELESAI]['data']);die();
            $this->data['fullpage'] = true;
            // echo "<pre>";print_r($this->data['data']);die();
            $this->template->viewDefault($this->view, $this->data);   
        }
             
    }

    public function search($function){
        unset($_SESSION['meole_search']);
        // echo "<pre>";print_r($this->input->post());die();
        $keyword = $this->input->post("keyword", true);
        $order = $this->input->post("order", true);
        $dinas = $this->input->post("dinas", true);
        $private = $this->input->post("private", true);
        $status = $this->input->post("status", true);
        // echo "<pre>";print_r($this->input->post());die();
        if ($keyword!=NULL and $keyword!=''){//from search navbar
            // echo "masuk sini";die();
            $_SESSION['meole_search']['keyword'] = $keyword;
        }
        if ($order!=NULL and $order!=''){//from search navbar
            // echo "masuk sini";die();
            $_SESSION['meole_search']['order'] = $order;
        }
        if ($private!=NULL and $private!=''){//from search navbar
            // echo "masuk sini";die();
            $_SESSION['meole_search']['private'] = $private;
        }

        if ($status!=NULL and $status!=''){//from search navbar
            $_SESSION['meole_search']['status'] = $status;
        }

        if ($dinas!=NULL and $dinas!=''){//from search navbar
            $_SESSION['meole_search']['dinas'] = $dinas;
        }

        // echo "<pre>";print_r( $_SESSION['meole_search']);die();

        header('Location: ' . site_url("web/".$this->class.'/'.$function.'?type=T'));
    }

    public function removeFilter($filter){
        // echo $filter;die();
        // echo $filter;die();
        unset($_SESSION['meole_search'][$filter]);
        // echo "<pre>";print_r($_SESSION['meole_search']);die();
        header('Location: ' . site_url("web/".$this->class.'/index?type=T'));
    }

    public function view($id){
        $detail = $this->model->getPostBy("id", $id);

        $ciConfig = $this->config->item('utils');
        $user_id = md5($detail['user_id'] . $this->config->item('encryption_key'));
        $path = $ciConfig['full_upload_dir'].'posts/photos/';
        $enName = Image::getFileName($user_id, Image::IMAGE_ORIGINAL, $detail['file']);

        if (strlen($detail['description']) > 30) {
            $str = substr($detail['description'], 0, 30) . '...';
        } else {
            $str = $detail['description'];
        }
        $res['filename'] = $enName;
        $res['title'] = $str;
        // $res['path'] = $path.$enName;
        echo json_encode($res);



    }


    /**
     * Halaman untuk mengganti operator
     * @param post data post_id, dan dinas_id
     */
    public function ajaxChangeOperator(){
        if(SessionManagerWeb::isAdministrator()){
            $post = $this->input->post();
            $prev_op = dbGetOne("select user_id from post_assign where post_id=".$post['post_id']);
            $gantiOperator = $this->model->changeOperator($post['operator_id'], $post['post_id']);
            if($gantiOperator){
                $updatePrivate = $this->model->updatePrivate($post['post_id'], Status::STATUS_PRIVATE);
                $updateStatus = $this->model->updateStatus($post['post_id'], Status::STATUS_DILAPORKAN);
                if($updateStatus){
                    $this->db->trans_commit();
                    $this->load->model('Notification_model');
                    $this->Notification_model->generate(Notification_model::ACTION_OPERATOR_CHANGED, $post['post_id'], SessionManagerWeb::getUserID(), $prev_op);
                    $this->Notification_model->generate(Notification_model::ACTION_OPERATOR_CHANGED_USER, $post['post_id'], SessionManagerWeb::getUserID());
                    echo "0";
                }else{
                    $this->db->trans_rollback();
                    echo "1";
                }
            }
            else{
                $this->db->trans_rollback();
                echo "1";
            }

        }else{
            echo "ERROR";
        }
        
    }

    /**
     * Halaman untuk melimpahkan ke dinas lain
     * @param post data post_id, dan dinas_id
     */
    public function changeDinas(){
        if(SessionManagerWeb::isAdministrator() || SessionManagerWeb::isOperator()){
            $post = $this->input->post();
            $this->load->model('Dinas_diversion_model');
            $ajukanDinas = $this->Dinas_diversion_model->create($post['post_id'], $post['dinas_id'],$post['petugas_id'],$post['reason'], SessionManagerWeb::isAdministrator());
            if($ajukanDinas){
                $this->db->trans_commit();
                if(SessionManagerWeb::isAdministrator()){
                    $dinas_id = $this->Dinas_diversion_model->filter("where id=".$diversion_id)->show_sql(false)->getOne('dinas_id');
                    $this->model->changeDinas($post['post_id'], $post['dinas_id']);
                    $updatePrivate1 = $this->model->updatePrivate($post['post_id'], Status::STATUS_PRIVATE);
                    $updateStatus1 = $this->model->updateStatus($post['post_id'], Status::STATUS_RESPON);
                    if($updateStatus1){
                        $this->db->trans_commit();
                        $this->load->model('Notification_model');
                        $this->Notification_model->generate(Notification_model::ACTION_DINAS_CHANGED, $post['post_id'],SessionManagerWeb::getUserID());
                        SessionManagerWeb::setFlashMsg(true, 'Berhasil memindahkan Dinas');
                    }else{
                        $this->db->trans_rollback();
                        SessionManagerWeb::setFlashMsg(false,'Gagal memindahkan Dinas');
                    }
                }else{
                    $updatePrivate = $this->model->updatePrivate($post['post_id'], Status::STATUS_PRIVATE);
                    $updateStatus = $this->model->updateStatus($post['post_id'], Status::STATUS_DIALIHKAN); 
                    if($updateStatus){
                        $this->db->trans_commit();
                        $this->load->model('Notification_model');
                        $this->Notification_model->generate(Notification_model::ACTION_DINAS_CHANGED_USER, $post['post_id'],SessionManagerWeb::getUserID());
                        $this->Notification_model->generate(Notification_model::ACTION_DINAS_CHANGED, $post['post_id'],SessionManagerWeb::getUserID());
                        SessionManagerWeb::setFlashMsg(true, 'Berhasil mengajukan peralihan Dinas');
                    }else{
                        $this->db->trans_rollback();
                        SessionManagerWeb::setFlashMsg(false,'Gagal mengajukan peralihan Dinas');
                    }
                }
            }else{
                $this->db->trans_rollback();
                SessionManagerWeb::setFlashMsg(false,'Gagal memindahkan Dinas 2');
            }
            redirect('web/post/detail/'.$post['post_id']);
        }else{
            die('Anda tidak bisa mengakses fungsi ini');
        }
        
    }

    /**
     * Halaman untuk admin untuk approve perubahan dinas
     * @param diversion_id
     */
    public function saveDiversionApprove($diversion_id){
        if(!SessionManagerWeb::isAdministrator()){
            die('Anda tidak bisa mengakses fungsi ini');
        }
        $post = $this->input->post();
        $this->load->model('Dinas_diversion_model');
        $approveDiversion = $this->Dinas_diversion_model->update($diversion_id, $post['approve'], $post['alasan'], $post['dinas']);
        if($approveDiversion){
            $updateCommit = false; 
            if($post['approve'] == Dinas_diversion_model::DIALIHKAN){
                $this->model->changeDinas($post['post_id'], $post['dinas']);
                $updateCommit = true; 
                $this->load->model('Notification_model');
                $this->Notification_model->generate(Notification_model::ACTION_APPROVED_DINAS_CHANGED_USER, $post['post_id'],SessionManagerWeb::getUserID());
            }
            elseif($post['approve'] == Dinas_diversion_model::DITERIMA){
                $dinas_id = $this->Dinas_diversion_model->filter("where id=".$diversion_id)->show_sql(false)->getOne('dinas_id');
                $this->model->changeDinas($post['post_id'], $dinas_id);
                $updateCommit = true; 
            }elseif($post['approve'] ==Dinas_diversion_model::DITOLAK){
                $updateCommit = true;
                $this->load->model('Notification_model');
                $this->Notification_model->generate(Notification_model::ACTION_APPROVED_DINAS_CHANGED_USER, $post['post_id'],SessionManagerWeb::getUserID());
            }
            if($updateCommit){
                $updateStatus = $this->model->updateStatus($post['post_id'], Status::STATUS_RESPON);
                if($updateStatus){
                    $this->db->trans_commit();
                    $this->load->model('Notification_model');
                    $this->Notification_model->generate(Notification_model::ACTION_APPROVED_DINAS_CHANGED, $post['post_id'],SessionManagerWeb::getUserID());
                    SessionManagerWeb::setFlashMsg(true,"Berhasil menyimpan pengalihan dinas");
                }else{
                    $this->db->trans_rollback();
                    SessionManagerWeb::setFlashMsg(false,"Gagal menyimpan pengalihan dinas");
                }
            }else{
                $this->db->trans_rollback();
                SessionManagerWeb::setFlashMsg(false,"Gagal menyimpan pengalihan dinas");
            }
            
        }else{
            $this->db->trans_rollback();
            SessionManagerWeb::setFlashMsg(false,"Gagal menyimpan pengalihan dinas");
        }
        redirect('web/post/detail/'.$post['post_id']);

    }
    

    /**
     * Halaman detail post
     * @param int $id
     */
    public function detail($id, $notif_user_id = null) {
        // set "sudah baca"
        $this->load->model('Notification_model');
        $this->load->model('Notification_user_model');
        $this->Notification_user_model->setAsRead($notif_user_id);

        $data = $this->model->getDetail($id, SessionManagerWeb::getUserID());
        $this->load->model('Dinas_diversion_model');
        $this->load->model('Comment_model');
        $this->load->model('Post_log_model');
        $this->data['detail_diversion'] = $this->Dinas_diversion_model->show_sql(false)->getDiversion($id);
        $this->data['discuss'] =  $this->Comment_model->getAll($id, Comment_model::DISKUSI);
        $this->data['response'] =  $this->Comment_model->show_sql(false)->getAll($id, Comment_model::KOMENTAR);
        $this->data['log'] =  $this->Post_log_model->show_sql(false)->column("pl.*")->filter("where (is_public=".Post_log_model::PUBLIC_ALL." or is_public=".Post_log_model::PUBLIC_DINAS.") and post_id=".$id)->order("id", "desc")->getAll();
        $this->load->model('Unitkerja_model');
        $this->load->model('User_model');
        $this->data['dinas'] = $this->Unitkerja_model->column('idunit, name')->order('name asc')->show_sql(false)->getAll('where level=2');
        $this->data['listOperator'] = $this->User_model->getListOperator($data['dinas_id']);
        $this->data['data'] = $data;
        $this->data['title'] = 'Post Detail';
        // echo "<pre>";print_r($this->data['log']);die();
        $this->template->viewDefault($this->view, $this->data);
    }

    /**
     * Halaman ubah status private
     * @param $post_id
     */
    public function updatePrivate($post_id = null) {
        if(SessionManagerWeb::isAdministrator() || $this->model->isOperator(SessionManagerWeb::getUserID(), $post_id)){
            $private = $this->input->post('private');
            $updatePrivate = $this->model->updatePrivate($post_id, $private);
            if($updatePrivate){
                $this->load->model('Notification_model');
                $this->Notification_model->generate(Notification_model::ACTION_PRIVATE_CHANGED, $post_id,SessionManagerWeb::getUserID());
                SessionManagerWeb::setFlashMsg(true, 'Berhasil mengubah privasi');
            }else{
                SessionManagerWeb::setFlashMsg(false, 'Gagal mengubah privasi');
            }
        }else{
            SessionManagerWeb::setFlashMsg(true, 'Anda tidak bisa mengubah privasi');
        }
        redirect($this->ctl . '/detail/'.$post_id);
    }


    /**
     * Halaman ubah status
     * @param $post_id
     */
    public function updateStatus($post_id = null) {
        if(SessionManagerWeb::isAdministrator() || $this->model->isOperator(SessionManagerWeb::getUserID(), $post_id)){
            $status = $this->input->post('status');
            $updateStatus = $this->model->updateStatus($post_id, $status);
            if($updateStatus){
                $this->load->model('Notification_model');
                $this->Notification_model->generate(Notification_model::ACTION_STATUS_CHANGED, $post_id,SessionManagerWeb::getUserID());
                SessionManagerWeb::setFlashMsg(true, 'Berhasil mengubah status');
            }else{
                SessionManagerWeb::setFlashMsg(false, 'Gagal mengubah status');
            }
        }else{
            SessionManagerWeb::setFlashMsg(true, 'Anda tidak bisa mengubah status');
        }
        redirect($this->ctl . '/detail/'.$post_id);
    }

    /**
     * Halaman edit post
     * @param int $id
     */
    public function edit($id) {
        $this->isAllowEditDeletePost($id);
        //parent::detail($id);
        $data = $this->model->getPost(SessionManagerWeb::getUserID(),$id);

        $tags=array();
        $tag_arr=$data['tags'];
        foreach ($tag_arr as $tag){
            $tags[]=$tag['name'];
        }
        $data['tags']=$tags;
        $this->data['data'] = $data;
        $this->load->model('Tag_model');

        $this->data['a_tag'] = $this->Tag_model->getListTags();
        $this->template->viewDefault($this->view, $this->data);
    }

    /**
     * Membuat data baru
     */
    /*
    public function create() {
        if ($_FILES['image']['error'] != UPLOAD_ERR_OK)
            unset($_FILES['image']);
        if ($_FILES['video']['error'] != UPLOAD_ERR_OK)
            unset($_FILES['video']);
        if ($_FILES['file']['error'] != UPLOAD_ERR_OK)
            unset($_FILES['file']);

        $data = $this->postData;
        $data['user_id'] = SessionManagerWeb::getUserID();
        $data['image'] = isset($_FILES['image']) ? $_FILES['image']['name'] : NULL;
        $data['video'] = isset($_FILES['video']) ? $_FILES['video']['name'] : NULL;
        $data['file'] = isset($_FILES['file']) ? $_FILES['file']['name'] : NULL;
        $data['postUsers'] = empty($data['postUsers']) ? NULL : implode(',', $data['postUsers']);

        $insert = $this->model->create($data);
        if ($insert === true) {
            SessionManagerWeb::setFlashMsg(true, 'Berhasil membuat postingan');
            $this->redirect();
        } else {
            if (!is_string($insert)) {
                $validation = $this->model->getErrorValidate();
                if (empty($validation))
                    $msg = 'Gagal membuat postingan';
                else
                    $msg = implode('<br />', $validation);
            } else
                $msg = $insert;

            SessionManagerWeb::setFlashMsg(false, $msg);
            redirect($this->ctl . '/add');
        }
    }
    */

    /**
     * Hapus data
     * @param int $id
     */
    public function delete($id) {
        die();
        $data = $this->model->get_by('posts.id', $id);
        if ($data['userId'] == SessionManagerWeb::getUserID()) {
            $delete = $this->model->delete($id);

            if ($delete === true) {
                $ok = true;
                $msg = 'Berhasil menghapus postingan';
            } else {
                $ok = false;
                $msg = 'Gagal menghapus postingan';
            }
        } else {
            $ok = false;
            $msg = 'Tidak dapat menghapus postingan ini';
        }

        SessionManagerWeb::setFlashMsg($ok, $msg);
        $this->redirect();
    }

    public function hapus($id) {
        $this->isAllowEditDeletePost($id);
        if (/*$this->model->delete($id)*/true) {
            $this->load->model("Post_quota_model");

            $sql = "select id, user_id, group_id, status
                    from posts
                    where id = $id";
            $postData = dbGetRow($sql);

            if($this->Post_quota_model->incQuota($postData['user_id'], $postData['group_id'])){
                $ok = true;
                $msg = 'Berhasil menghapus pesan';                
            }

        }
        else {
            $ok = false;
            $msg = 'Gagal menghapus pesan';            
        }
        SessionManagerWeb::setFlashMsg($ok, $msg);
        redirect('web/post');
    }

    /**
     * Daftar kirim ke
     */
    public function getListKirimKe() {
        return array(Post_model::TYPE_PUBLIC => 'Umum', Post_model::TYPE_GROUP => 'Grup', Post_model::TYPE_FRIEND => 'Teman');
    }

    /**
     * Daftar kategori
     */
    public function getListKategori() {
        return array(Category_model::INFO => 'INFORMATION', Category_model::EVENT => 'EVENT');
    }

    function kirim()  {
        if ($_POST) {

            $data = array();
            $data['user_id'] = SessionManagerWeb::getUserID();
            $data['id_organisasi'] = SessionManagerWeb::getOrganisasiID();
            if ($this->input->post('user_id_anggota')!=""){
                $data['user_id']=$this->input->post('user_id_anggota');
                $users = $this->User_model->getUser($data['user_id']);
                $data['id_organisasi'] = $users['id_organisasi'];
            }
            $data['title'] = $this->input->post('judul_kirim',true);
            if ($_POST['group_tujuan']=='Helpdesk')
                $data['title']="";
            $data['description'] = $this->input->post('text_kirim',true);
            $data['category_id'] = 1;
            $data['is_sticky'] = $_POST['stick_kirim'];
            if ($_POST['group_kirim'] and $_POST['helpdesk_kirim']=="") {
                $data['group_id'] = $_POST['group_kirim'];
                $data['type'] = Post_model::TYPE_GROUP;
            }
            else {
                $data['group_id']=$_POST['helpdesk_kirim'];
                $data['type'] = Post_model::TYPE_TICKET;
                $data['postTags'] = $this->input->post('tag', true);
            }
            $data['status'] = 'B';
            if ($data['description']=="" || $data['group_id']=="" ) {
                SessionManagerWeb::setFlashMsg(false, 'Gagal membuat Pesan');
                $this->redirect();
            }
            // echo "<pre>";print_r($data);die();
            $insert = $this->model->create($data, TRUE, TRUE);
            
            /* Cek Kuota */
            if($data['type'] == Post_model::TYPE_TICKET){
                $this->load->model("Post_quota_model");
                $data['quota_data'] = $this->Post_quota_model->getQuota(SessionManagerWeb::getUserID(), $data['group_id']);
                // $data['quota_data']['quota'] = (int) $data['quota_data']['quota'];
            }

            if ($insert) {
                if ($_POST['stick_kirim']) {
                    $this->model->setSticky($insert, 1);
                }

                $this->db->trans_begin();
                if (!$this->_moveFileTemp($insert)) {
                    $this->db->trans_rollback();
                    SessionManagerWeb::setFlashMsg(false, 'Gagal membuat Pesan');
                }
                else {
                    unset($_SESSION['sigap']['draft_post']);
                    $this->db->trans_commit();
                    if ($this->input->post('user_id_anggota')!="") {
                        $this->load->model('Comment_model', 'Comment');
                        $data = array();
                        $data['user_id'] = SessionManagerWeb::getUserID();
                        $data['post_id'] = $insert;
                        $data['candelete'] = 0;
                        $data['text'] = '<span style="color:ddd;font-size:11px">[membuat tiket sebagai ' . $users['name'] . ']</span>';

                        $comment_id = $this->Comment->create($data, TRUE, TRUE);
                    }
                    SessionManagerWeb::setFlashMsg(true, "Berhasil membuat Pesan<br>Anda telah menggunakan ".$data['quota_data']['quota']." dari ".$data['quota_data']['limit']." kuota tiket yang tersedia");
                }
                
                $this->redirect();
            } else {
                if (!is_string($insert)) {
                    $validation = $this->model->getErrorValidate();
                    if (empty($validation))
                        $msg = "Gagal membuat postingan<br>Anda telah menggunakan ".$data['quota_data']['quota']." dari ".$data['quota_data']['limit']." kuota tiket yang tersedia";
                    else
                        $msg = implode('<br />', $validation);
                } else
                    $msg = $insert;

                SessionManagerWeb::setFlashMsg(false, $msg);
                $this->redirect();
            }            
        }    
    }

    function kirim_disclaimer()  {
        if ($_POST) {
            $data = array();
            $data['user_id'] = SessionManagerWeb::getUserID();
            $data['description'] = $this->input->post('text_kirim',true);
            // $data['description'] = $this->input->post('text_kirim');
            $data['category_id'] = 1;
            $data['type'] = Post_model::TYPE_DISCLAIMER;
            $data['status'] = 'B';
            $data['id_organisasi'] = SessionManagerWeb::getOrganisasiID();
            $data['is_sticky']='0';

            $insert = $this->model->create($data, TRUE, TRUE);
            if ($insert) {

                $this->db->trans_begin();
                if (!$this->_moveFileTemp($insert)) {
                    $this->db->trans_rollback();
                    SessionManagerWeb::setFlashMsg(false, 'Gagal membuat Pesan');
                }
                else {
                    unset($_SESSION['sigap']['draft_post']);
                    $this->db->trans_commit();
                    SessionManagerWeb::setFlashMsg(true, 'Berhasil membuat Pesan');
                }
                
                $this->redirect();
            } else {
                if (!is_string($insert)) {
                    $validation = $this->model->getErrorValidate();
                    if (empty($validation))
                        $msg = 'Gagal membuat postingan';
                    else
                        $msg = implode('<br />', $validation);
                } else
                    $msg = $insert;

                SessionManagerWeb::setFlashMsg(false, $msg);
                redirect($this->ctl . '/add');
            }            
        }    
    }

    function update($id)  {
        if ($_POST) {
            $data = array();
            $data['description'] = $this->input->post('text_kirim',true);
            $data['postTags'] = $this->input->post('tag',true);
            // $data['description'] = $this->input->post('text_kirim');
            $data['is_sticky'] = $_POST['stick_kirim'];
            $ret = $this->model->update($data, $id);

            if ($ret) {
                if (!$this->_moveFileTemp($id)) {
                    $this->db->trans_rollback();
                    SessionManagerWeb::setFlashMsg(false, 'Gagal membuat Pesan');
                }else {
                    unset($_SESSION['sigap']['draft_post']);
                    $this->db->trans_commit();
                    SessionManagerWeb::setFlashMsg(true, 'Berhasil mengubah pesan');
                    
                    redirect('web/post/detail/' . $id);
                }
                
            } else {
                if (empty($validation))
                    $msg = 'Gagal mengubah pesan';
                else
                    $msg = implode('<br />', $validation);

                SessionManagerWeb::setFlashMsg(false, $msg);
                redirect('web/post/edit/' . $id);
            }            
        }    
    }

    function disclaimer(){
        $this->template->viewDefault($this->view,$this->data);
    }

    function ajaxreply($id) {
        $this->load->model('Replies_model', 'Reply');
        $row = $this->Reply->getRow($id);
        if (!$row)
            die();
        echo $row['reply'];
        die();
    }

    function ajaxgetpost() {
        $id = (int) $_POST['id'];
        $this->data['_data'] = $this->model->getPost(SessionManagerWeb::getUserID(),$id);
        // echo "<pre>";
        // var_dump($this->data['_data']);
        // die();
        $this->load->view('web/_post_detail', $this->data);
        return;
    }

    function ajaxsetstatus() {
        $id = (int) $_POST['id'];
        $status = $_POST['status'];
        if (!$status)
            return false;

        if ($this->model->setStatus($id, $status)) {
            $this->load->model('Comment_model', 'Comment');
            
            $data = array();
            $data['user_id'] = SessionManagerWeb::getUserID();
            $data['post_id'] = $id;
            $data['candelete'] = 0;
            $data['text'] = '<span style="color:ddd;font-size:11px">[mengubah status menjadi ' . Ticket::status($status) . ']</span>';

            $comment_id = $this->Comment->create($data, TRUE, TRUE);
        }

        $this->data['_data'] = $this->model->getPost(SessionManagerWeb::getUserID(),$id);
        $this->load->view('web/_post_detail', $this->data);
        return;
    }

    function sessionsavesalin(){
        $_SESSION['salin']['post_id'] = (int) $_POST['post_id'];
        $_SESSION['salin']['helpdesk'] = $_POST['helpdesk'];
        die();
    }

    function sessionsavepindah(){
        $_SESSION['pindah']['post_id'] = (int) $_POST['post_id'];
        $_SESSION['pindah']['helpdesk'] = $_POST['helpdesk'];
        die();
    }

    function ajaxsalintiket() {
        // $id = (int) $_POST['id'];
        // $helpdesk = $_POST['helpdesk'];
        $id = (int) $_SESSION['salin']['post_id'];
        $helpdesk = $_SESSION['salin']['helpdesk'];
        unset($_SESSION['salin']);
        $text = $_POST['text'];
        if (!$helpdesk)
            return false;
        $post = $this->model->getPostBy('id',$id);
        $post_images = $this->model->getPostImage($id);
        $post_files = $this->model->getPostFile($id);
        $data = $post;
        $data['group_id']=$helpdesk;
        $data['status'] = 'B';
        $data['parent_id'] = $data['id'];
        unset($data['created_at']);
        unset($data['id']);
        unset($data['updated_at']);
        unset($data['kode']);
        $insert = $this->model->create($data, TRUE, TRUE);
        if ($insert) {
            foreach ($post_images as $key => $image) {
                $this->model->insertSalinPostImage($insert,$image['image']);
            }
            foreach ($post_files as $key => $file) {
                $this->model->insertSalinPostFile($insert,$file['file']);
            }
            $this->load->model('Comment_model', 'Comment');
            $group = $this->Group_model->getGroupBy('id',$helpdesk);
            $data = array();
            $data['user_id'] = SessionManagerWeb::getUserID();
            $data['post_id'] = $insert;
            $data['candelete'] = 0;
            $data['text'] = '<span style="color:ddd;font-size:11px">[menyalin tiket ke ' . $group['name'] . ']</span> '.$text;

            $comment_id = $this->Comment->create($data, TRUE, TRUE);
            SessionManagerWeb::setFlashMsg(true, 'Berhasil menyalin Tiket');
            $this->redirect();
        }
        return;
    }

    function ajaxsethelpdesksession(){
        $id = $_POST['id'];
        $helpdesk = $_POST['helpdesk'];
        $_SESSION['helpdesk_selected'] = $helpdesk;
    }

    // function ajaxchangeknowledgesession(){
    //     $id = (int) $_POST['id'];
    //     $helpdesk = $_POST['helpdesk'];
    //     $_SESSION['helpdesk_knowledge'] =$helpdesk;
    // }

    // function ajaxchangeinformasisession(){
    //     $id = (int) $_POST['id'];
    //     $helpdesk = $_POST['helpdesk'];
    //     $_SESSION['helpdesk_informasi'] =$helpdesk;
    // }

    function ajaxpindahtiket() {
        // $id = (int) $_POST['id'];
        // $helpdesk = $_POST['helpdesk'];
        $id = (int) $_SESSION['pindah']['post_id'];
        $helpdesk = $_SESSION['pindah']['helpdesk'];
        unset($_SESSION['pindah']);
        $text = $_POST['text'];
        if (!$helpdesk)
            return false;
        $post = $this->model->getPostBy('id',$id);
        $post_images = $this->model->getPostImage($id);
        $post_files = $this->model->getPostFile($id);
        $data = $post;
        $data['group_id']=$helpdesk;
        $data['status'] = 'B';
        $data['parent_id'] = $data['id'];
        unset($data['created_at']);
        unset($data['id']);
        unset($data['updated_at']);
        unset($data['kode']);
        $insert = $this->model->create($data, TRUE, TRUE);
        if ($insert) { 

            foreach ($post_images as $key => $image) {
                $this->model->insertPindahPostImage($insert,$image['image']);
            }
            foreach ($post_files as $key => $file) {
                $this->model->insertPindahPostFile($insert,$file['file']);
            }
            $this->load->model('Comment_model', 'Comment');
            $group = $this->Group_model->getGroupBy('id',$helpdesk);
            $data = array();
            $data['user_id'] = SessionManagerWeb::getUserID();
            $data['post_id'] = $insert;
            $data['candelete'] = 0;
            $data['text'] = '<span style="color:ddd;font-size:11px">[memindahkan tiket ke ' . $group['name'] . ']</span> '.$text;

            $comment_id = $this->Comment->create($data, TRUE, TRUE);
            SessionManagerWeb::setFlashMsg(true, 'Berhasil memindahkan Tiket');
            $this->model->delete($id);
            $this->redirect();
        }
        return;
    }

    function ajaxassignticket() {
        $id = (int) $_POST['id'];
        $list_staff = $_POST['list_staff'];
        if (!$list_staff)
            return false;

        $this->model->assignTicket($id, $list_staff);

        # set proses ketika tiket diassign
        if ($this->model->setStatus($id, 'P')) {
            $list_staff = explode(',', $list_staff);
            foreach ($list_staff as $staff) {
                $staffs[] = "'{$staff}'";
            }

            $str_staff = implode(',', $staffs);

            $this->load->model('Notification_model');
            $this->Notification_model->generate(Notification_model::ACTION_TICKET_ASSIGN, $id,SessionManagerWeb::getUserID());
            // $this->Notification_model->mobileNotification(Notification_model::ACTION_TICKET_ASSIGN, $id,SessionManagerWeb::getUserID());

            $sql = "select name from users where id in ($str_staff)";
            $users = dbGetRows($sql);
            $str_staff = '';
            foreach ($users as $user) {
                $str_staff .= $user['name'] . ', ';
            }

            $str_staff = substr($str_staff, 0, strlen($str_staff)-2);

            $this->load->model('Comment_model', 'Comment');
            
            $data = array();
            $data['user_id'] = SessionManagerWeb::getUserID();
            $data['group_id'] = $this->Group_member_model->getMyHelpdeskGroup(SessionManagerWeb::getUserID())['id'];
            $data['post_id'] = $id;
            $data['candelete'] = 0;
            $data['text'] = '<span style="color:ddd;font-size:11px">[menugaskan pesan ini kepada: ' . $str_staff . ']</span>';

            $comment_id = $this->Comment->create($data, TRUE, TRUE);
        }

        $this->data['_data'] = $this->model->getPost(SessionManagerWeb::getUserID(),$id);
        $this->load->view('web/_post_detail', $this->data);
        return;
    }

    function ajaxsetrating() {
        $id = (int) $_POST['id'];
        $rating = (int) $_POST['rating'];
        if (!$rating)
            return;

        if ($this->model->setRating($id, $rating,SessionManagerWeb::getUserID())) {
        }

        $this->data['_data'] = $this->model->getPost(SessionManagerWeb::getUserID(),$id);
        if (!isset($this->data['_data']['post_id'])) {
            $this->data['_data'] = $this->model->getKnowledge($id,SessionManagerWeb::getUserID());
            $this->load->view('web/_knowledge_detail', $this->data);
        } else {
            $this->load->view('web/_post_detail', $this->data);
        }
        
        
        return;
    }

    public function lowRatingDashboard(){
        if($_GET['id_user'])
            $id_user = $_GET['id_user'];
        
        $page=1;
        if($this->input->get('checkshowall'))
            $_SESSION['showall'] = (bool) $this->input->get('showall');

        if (!isset($_SESSION['sort_by'])){
            $_SESSION['sort_by'] = 0;
        } 

        $_SESSION['group_selected'] = '0';
        $this->load->model('User_model', 'User');
        if ($_SESSION['helpdesk_selected']){
            $this->data['chosen_helpdesk'] = $_SESSION['helpdesk_selected'];
            $this->data['helpdesk'] = $this->Group_model->getGroupBy('name',$_SESSION['helpdesk_selected']);
            $list_staff_helpdesk = $this->Group_member_model->getAllMembers($this->data['helpdesk']['id']);
        } else {
            $list_staff_helpdesk = $this->User->getListStaff(SessionManagerWeb::getUserID());
        }

        $this->data['list_staff_helpdesk']['0'] = "Semua";
        
        foreach ($list_staff_helpdesk as $k_staff => $v_staff) {
            $this->data['list_staff_helpdesk'][$v_staff['id']] = $v_staff['name'];
        }
            
        if ($_GET['s']) {
            $s = $_GET['s'];
            unset($_SESSION['home_status']);
            $_SESSION['home_status'][$s] = 1;
            if (SessionManagerWeb::isStaff()){
                redirect('web/post/dashboard');   
            } else if (SessionManagerWeb::isOperator()) {
                redirect('web/post/dashboard/'.SessionManagerWeb::getUserID());
            } else if ($is_admin_group){
                redirect('web/post/dashboard');
            }             
        }

        if (isset($_GET['show_my_ticket'])) {
            if ($_GET['show_my_ticket']==1){
                $_SESSION['show_my_ticket'] = 1;
                $this->data['created_by_me']=1;
            }
            else if ($_GET['show_my_ticket']==0) {
                $this->data['created_by_me']=0;
                unset($_SESSION['show_my_ticket']);
            }
        } else 
        if ($id_user!=NULL) {
            unset($_SESSION['show_my_ticket']);
        }
        if (!isset($_SESSION['home_status']))
            $_SESSION['home_status']['D'] = 1;
        if (isset($_GET['status'])) {
            $this->data['status_selected'] = $_GET['status'];
        } else {
            unset($this->data['status_selected']);
        }
        // if (!SessionManagerWeb::isSuperAdministrator() and SessionManagerWeb::isStaff()) { 
        //     $this->data['chosen_helpdesk'] = $this->data['list_my_helpdesk'][0]['name'];
        // }
        // $condition = array();
        // $getData = $this->input->get();
        // if (!empty($getData['project']))
        //     $condition['posts.group_id'] = $getData['project'];
        // if (!empty($getData['jenis']))
        //     $condition['posts.category_id'] = $getData['jenis'];
        // if (!empty($getData['priority']))
        //     $condition['issues.priority'] = $getData['priority'];
        // if (!empty($getData['start_date']))
        //     $condition['issues.deadline >='] = FormatterWeb::formatDate($getData['start_date']);
        // if (!empty($getData['end_date']))
        //     $condition['issues.deadline <='] = FormatterWeb::formatDate($getData['end_date']);
        // if (!empty($getData['assignment_task']) && $getData['assignment_task'] == 'M')
        //     $condition['post_users.user_id'] = SessionManagerWeb::getUserID();

        $datefilter = array('date_started' => FormatterWeb::formatDate($_GET['start_date']),
                            'date_finished' => FormatterWeb::formatDate($_GET['end_date']));

        $data = $this->model->filter(" and pr.rating in (1,2) ")->getDashboard(SessionManagerWeb::getUserID(),$this->data['chosen_helpdesk'],$page,$id_user, $this->data['created_by_me'], $this->data['status_selected'], $_SESSION['showall'], $_SESSION['sort_by'], $datefilter);
        
        $this->data['data'] = $data['S'];
        $this->data['jml_post'] = $data['jml_post']['S'];
        unset($this->data['data']['jml_post']);
        // echo '<pre>';
        // var_dump($this->data['data']);
        // die();

        $this->data['container'] = 'container-fluid';
        $this->data['a_status'] = Post_model::getStatus();
        
        $this->data['list_staff'] = $this->User->getListStaff(SessionManagerWeb::getUserID());
        $this->data['list_staff_kopertis'] = $this->User->getListStaffKopertis();
        $this->data['this_user'] = $this->User->getUser($id_user);
        $this->data['is_admin_kopertis'] = $this->Group_member_model->isAdminKopertis($id_user);
        $this->data['group_kopertis'] = $this->Group_member_model->getAdminKopertis($id_user);

        $this->template->viewDefault('low_rate_post_dashboard', $this->data);       
    }



    function informasi() {
        $this->data['title'] = 'Daftar Informasi';
        $this->data['data'] = $this->model->getListInformasi();
        $this->template->viewDefault($this->view, $this->data);
    }

    function faq() {
        $this->template->viewDefault($this->view, $this->data);
    }

    function stick() {
        //die($_GET);

        if ($_GET) {
            if ($this->input->get('stick')=='1') {
                $data='0';
            } else {
                $data='1';
            }
            $insert = $this->model->setPinPost($_GET['id'],$data);

            $this->redirect();
        }
    }

    function ajaxSetSort(){
        $sort_by = $this->input->post('sort_by', true);
        $_SESSION['sort_by'] = $sort_by;
        echo true;
    }

}
