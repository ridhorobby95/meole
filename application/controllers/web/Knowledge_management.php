<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Knowledge_management extends WEB_Controller {

    protected $title = 'Knowledge Management';

    /**
     * Halaman daftar post public
     * @param int $page
     */
    public function index($page = 0) {
        // tombol
        $buttons = array();
        $buttons[] = array('label' => 'Ayo Posting !', 'type' => 'success', 'icon' => 'plus', 'click' => 'goAdd()');

        $this->data['buttons'] = $buttons;
        $this->data['title'] = 'Knowledge Management';
        $this->data['data'] = $this->model->getAll($page, $this->input->get('search'));
        $this->data['list_informasi'] = $this->model->getListInformasi();
        $this->template->viewDefault($this->view, $this->data);
    }

    public function tag($page = 0) {
        // tombol
        $buttons = array();
        $buttons[] = array('label' => 'Ayo Posting !', 'type' => 'success', 'icon' => 'plus', 'click' => 'goAdd()');

        $tag = $this->input->get('param');
        if (empty($tag)) {
            SessionManagerWeb::setFlashMsg(FALSE, 'Invalid Parameter');
            redirect($this->ctl);
        }

        $this->data['buttons'] = $buttons;
        $this->data['title'] = 'Knowledge Management - #' . $tag;
        $this->data['data'] = $this->model->getByTag($page, $tag);

        $this->template->viewDefault('knowledge_management_index', $this->data);
    }
    /**
     * import Knowledge from dashboard
     * @param int $id
     */


    /**
     * Halaman detail post
     * @param int $id
     */
    public function detail($id) {
        parent::detail($id);
        $this->data['buttons']['reuse'] = array('label' => 'Gunakan', 'type' => 'success', 'icon' => 'check', 'click' => 'goReuse()');

        // ambil detail
        $data = $this->model->getDetail($id);

        // cek user
        if ($data['userId'] != SessionManagerWeb::getUserID())
            unset($this->data['buttons']['edit'], $this->data['buttons']['delete']);

        $this->data['data'] = $data;

        if (!empty($data)) {
            $this->load->model('Tracker_model');
            $this->Tracker_model->post($id, SessionManagerWeb::getUserID());
        }

        $this->template->viewDefault($this->view, $this->data);
    }

    /**
     * Halaman edit post
     * @param int $id
     */
    public function edit($id = null) {
        parent::edit($id);

        if (isset($id)) {
            $data = $this->model->getDetail($id);
            $tags = array();
            foreach ($data['postTags'] as $tag) {
                $tags[] = '#' . $tag['name'];
            }
            $data['tags'] = implode(" ", $tags);
        } else {
            $data = array();
        }

        $this->data['data'] = $data;

        $this->template->viewDefault($this->view, $this->data);
    }

    /**
     * Membuat data baru
     */
    public function create() {
        if ($_FILES['image']['error'] != UPLOAD_ERR_OK)
            unset($_FILES['image']);
        if ($_FILES['video']['error'] != UPLOAD_ERR_OK)
            unset($_FILES['video']);
        if ($_FILES['file']['error'] != UPLOAD_ERR_OK)
            unset($_FILES['file']);

        $data = $this->postData;
        $data['user_id'] = SessionManagerWeb::getUserID();
        $data['image'] = isset($_FILES['image']) ? $_FILES['image']['name'] : NULL;
        $data['video'] = isset($_FILES['video']) ? $_FILES['video']['name'] : NULL;
        $data['file'] = isset($_FILES['file']) ? $_FILES['file']['name'] : NULL;
        $this->load->model('Category_model');
        $data['category_id'] = Category_model::KNOWLEDGE_MANAGEMENT;

        $etags = explode('#', $data['tags']);
        $tmpTags = array();
        foreach ($etags as $tag) {
            $tag = strtolower(str_replace(" ", "", $tag));
            if (!empty($tag))
                $tmpTags[] = $tag;
        }

        $data['postTags'] = implode(',', $tmpTags);
        $insert = $this->model->create($data);
        if ($insert === true) {
            SessionManagerWeb::setFlashMsg(true, 'Berhasil membuat postingan');
            $this->redirect();
        } else {
            if (!is_string($insert)) {
                $validation = $this->model->getErrorValidate();
                if (empty($validation))
                    $msg = 'Gagal membuat postingan';
                else
                    $msg = implode('<br />', $validation);
            } else
                $msg = $insert;

            SessionManagerWeb::setFlashMsg(false, $msg);
            redirect($this->ctl . '/add');
        }
    }

    public function update($id) {
        if ($_FILES['image']['error'] != UPLOAD_ERR_OK)
            unset($_FILES['image']);
        if ($_FILES['video']['error'] != UPLOAD_ERR_OK)
            unset($_FILES['video']);
        if ($_FILES['file']['error'] != UPLOAD_ERR_OK)
            unset($_FILES['file']);

        $data = $this->postData;
        $data['userIdUpdated'] = SessionManagerWeb::getUserID();
        if (isset($_FILES['image']))
            $data['image'] = $_FILES['image']['name'];
        if (isset($_FILES['video']))
            $data['video'] = $_FILES['video']['name'];
        if (isset($_FILES['file']))
            $data['file'] = $_FILES['file']['name'];

        $this->load->model('Category_model');
        $data['category_id'] = Category_model::KNOWLEDGE_MANAGEMENT;

        $etags = explode('#', $data['tags']);
        $tmpTags = array();
        foreach ($etags as $tag) {
            $tag = strtolower(str_replace(" ", "", $tag));
            if (!empty($tag))
                $tmpTags[] = $tag;
        }

        $data['postTags'] = implode(',', $tmpTags);
        $insert = $this->model->update($id, $data);
        if ($insert === true) {
            SessionManagerWeb::setFlashMsg(true, 'Berhasil merubah Knowledge Management');
            $this->redirect();
        } else {
            if (!is_string($insert)) {
                $validation = $this->model->getErrorValidate();
                if (empty($validation))
                    $msg = 'Gagal merubah Knowledge Management';
                else
                    $msg = implode('<br />', $validation);
            } else
                $msg = $insert;

            SessionManagerWeb::setFlashMsg(false, $msg);
            redirect($this->ctl . '/edit/' . $id);
        }
    }

    /**
     * Hapus data
     * @param int $id
     */
    public function delete($id) {
        $data = $this->model->get_by('posts.id', $id);
        if ($data['userId'] == SessionManagerWeb::getUserID()) {
            $delete = $this->model->delete($id);

            if ($delete === true) {
                $ok = true;
                $msg = 'Berhasil menghapus postingan';
            } else {
                $ok = false;
                $msg = 'Gagal menghapus postingan';
            }
        } else {
            $ok = false;
            $msg = 'Tidak dapat menghapus postingan ini';
        }

        SessionManagerWeb::setFlashMsg($ok, $msg);
        $this->redirect();
    }

    public function reuse($id) {
        $this->load->model('Comment_model');
        $data['userId'] = SessionManagerWeb::getUserID();
        $data['postId'] = $id;
        $data['candelete'] = '0';
        $data['reuse'] = '1';
        if (!empty($this->postData['description'])) {
            $data['text'] = '[REUSE] ' . $this->postData['description'];
        } else {
            SessionManagerWeb::setFlashMsg(FALSE, 'Deskripsi harus diisi');
            redirect($this->ctl . '/detail/' . $id);
        }
        $insert = $this->Comment_model->create($data);
        if ($insert === TRUE) {
            SessionManagerWeb::setFlashMsg(TRUE, 'Berhasil menggunakan KM');
        } else {
            SessionManagerWeb::setFlashMsg(FALSE, 'Gagal menggunakan KM');
        }

        redirect($this->ctl . '/detail/' . $id);
    }

}
