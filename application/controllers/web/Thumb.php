<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Thumb extends WEB_Controller {
	protected $ispublic = true;
	
	/**
	 * Watermarking dengan gambar pangkat
	 * @param int $userid
	 * @param int $size
	 */
	public function watermark($userid, $size=null) {
		// ambil user
		ini_set('display_errors','On');
		$this->load->model('User_model');
		$user = $this->User_model->get($userid);
		// echo "<pre>";print_r($user);die();
		// ambil image thumbnail
		$CI = & get_instance();
		$config = $CI->config->item('utils');
		$thumb = $user['photo'][strtolower(Image::IMAGE_THUMB)];
		// echo "<pre>";print_r($thumb);die();
		// proses image thumbnail
		// $image = new Imagick();
		$file = $config['full_upload_dir'].'users/photos/'.$thumb['name'];
		if (!file_exists($file) || is_dir($file)){
			$file = $config['full_upload_dir'] . 'users/photos/nopic.png';
			$thumb['link'] = base_url().$config['upload_dir'] .'users/photos/nopic.png';
			$thumb['mime'] = Image::getMime('nopic.png');
		}
		// $image->readImage($file);
		
		// $iWidth = $image->getImageWidth();
		// $iHeight = $image->getImageHeight();
		
		// if($iWidth != $iHeight) {
		// 	if($iWidth > $iHeight)
		// 		$iWidth = $iHeight;
		// 	else
		// 		$iHeight = $iWidth;
			
		// 	$image->cropImage($iWidth, $iHeight, 0, 0);
		// }
		
		// if(!empty($size)) {
		// 	$iWidth = $iHeight = $size;
			
		// 	$image->scaleImage($iWidth, $iHeight);
		// }
		// echo $thumb['link'];die();
		$image = file_get_contents($thumb['link']);
		header('Content-type: '.$thumb['mime']);
		
		echo $image;
	}

	public function profile($user_id, $size=Image::IMAGE_ROUNDED) {
		$CI = & get_instance();
		$config = $CI->config->item('utils');
		$folder = 'users/photos';

		// cek file yang ada namanya
		$user = $this->User_model->getUser($user_id);
		// $name = $user['photo'];
		$file = Image::getLocation($user_id, $size, $name, $folder);
		$arr_photo = explode('.',$user['photo']);
		$ext = $arr_photo[count($arr_photo)-1];
		$file .='.'.$ext;
		// $glob = glob($file . '*');
		// $file = $glob[0];

		// if (!file_exists($file) || is_dir($file)){
		// 	// cek file yang ga ada namanya
		// 	$name = '';
		// 	$file = Image::getLocation($user_id, $size, $name, $folder);
		// 	$glob = glob($file . '*');
		// 	$file = $glob[0];
			if (!file_exists($file) || is_dir($file)){
				$file = $config['full_upload_dir'] . 'users/photos/nopic.png';
			}
		// }
		
		header('Content-type: '. Image::getMime($file));
		$image = file_get_contents($file);
		// $image = readfile($file);
		echo $image;

	}

	public function cover($group_id, $name, $size=Image::IMAGE_ORIGINAL) {
		// $name=$_SESSION['group_cover_name'];
		$group = $this->Group_model->getGroupBy('id',$group_id);
		$name = $group['cover'];
		$CI = & get_instance();
		$config = $CI->config->item('utils');

		$folder = 'groups/cover';
		$file = Image::getLocation($group_id, $size, $name, $folder);
		$glob = glob($file . '*');
		$file = $glob[0];
		if (!file_exists($file) || is_dir($file))
			$file = $config['full_upload_dir'] . 'groups/cover/logo.png';
		header('Content-type: '. Image::getMime($file));
		$image = file_get_contents($file);
		// $image = readfile($file);
		echo $image;
	}

	public function group_photo($user_id, $name, $size=Image::IMAGE_THUMB) {
		$CI = & get_instance();
		$config = $CI->config->item('utils');

		$folder = 'groups/images';
		$file = Image::getLocation($user_id, $size, $name, $folder);
		
		$glob = glob($file . '*');
		$file = $glob[0];
		if (!file_exists($file) || is_dir($file))
			$file = $config['full_upload_dir'] . 'groups/cover/logo.png';
		header('Content-type: '. Image::getMime($file));
		$image = file_get_contents($file);
		// $image = readfile($file);
		echo $image;
	}

	public function image($name) {
		if ($name==''){
			echo "This directory is restricted.";
			die();
		}
		$CI = & get_instance();
		$config = $CI->config->item('utils');

		$folder = 'posts/photos';
		$file = $config['full_upload_dir'] . $folder . '/' . $name;
		$glob = glob($file . '*');
		$file = $glob[0];
		if (!file_exists($file) || is_dir($file))
			die('File not exist.');
		header('Content-type: '. Image::getMime($file));
		$image = file_get_contents($file);
		// $image = readfile($file);
		echo $image;
	}

	public function files($name) {
		if ($name==''){
			echo "This directory is restricted.";
			die();
		}
		$CI = & get_instance();
		$config = $CI->config->item('utils');

		$folder = 'posts/files';
		$file = $config['full_upload_dir'] . $folder . '/' . $name;
		$glob = glob($file . '*');
		$file = $glob[0];
		if (!file_exists($file) || is_dir($file))
			die('File not exist.');
		header('Content-type: '. Image::getMime($file));
		$image = file_get_contents($file);
		// $image = readfile($file);
		echo $image;
	}
}