<?php

defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * Controller Issue
 * @author Sevima
 * @version 1.0
 */
class Issue extends WEB_Controller {

    protected $title = 'Tugas';

    /**
     * Halaman default
     * @param int $page
     */
    public function index() {
        redirect($this->ctl . '/dashboard');
    }

    /**
     * Halaman daftar tugas dari user
     * @param int $page
     */
    public function me($page = 0) {
        // tombol
        $buttons = array();
        $buttons[] = array('label' => 'Beri Tugas', 'type' => 'success', 'icon' => 'plus', 'click' => 'goAdd()');

        $showall = $this->input->get('showall');

        $this->data['buttons'] = $buttons;
        $this->data['data'] = $this->model->getMe(SessionManagerWeb::getUserID(), $page, null, ($showall == 1 ? true : false));

        $this->data['a_status'] = Issue_model::getStatus();

        $this->template->viewDefault($this->class . '_index', $this->data);
    }

    /**
     * Halaman daftar tugas untuk user
     * @param int $page
     */
    public function for_me($page = 0) {
        $showall = $this->input->get('showall');
        if (SessionManagerWeb::isAdministrator())
            $this->data['data'] = $this->model->getAll($page);
        else
            $this->data['data'] = $this->model->getForMe(SessionManagerWeb::getUserID(), $page, null, ($showall == 1 ? true : false));

        $this->data['a_status'] = Issue_model::getStatus();

        $this->template->viewDefault($this->class . '_index', $this->data);
    }

    public function dashboard() {
        $buttons = array();
        $buttons[] = array('label' => 'Beri Tugas', 'type' => 'success', 'icon' => 'plus', 'click' => 'goAdd()');
        $this->data['buttons'] = $buttons;

        $condition = array();
        $getData = $this->input->get();
        if (!empty($getData['project']))
            $condition['posts.group_id'] = $getData['project'];
        if (!empty($getData['jenis']))
            $condition['posts.category_id'] = $getData['jenis'];
        if (!empty($getData['priority']))
            $condition['issues.priority'] = $getData['priority'];
        if (!empty($getData['start_date']))
            $condition['issues.deadline >='] = FormatterWeb::formatDate($getData['start_date']);
        if (!empty($getData['end_date']))
            $condition['issues.deadline <='] = FormatterWeb::formatDate($getData['end_date']);
        if (!empty($getData['assignment_task']) && $getData['assignment_task'] == 'M')
            $condition['post_users.user_id'] = SessionManagerWeb::getUserID();

        if (SessionManagerWeb::isAdministrator()) {
            $this->data['data'] = $this->model->getDashboard(NULL, $condition);
        } else {
            $this->data['data'] = $this->model->getDashboard(SessionManagerWeb::getUserID(), $condition);
        }
        $this->data['container'] = 'container-fluid';
        $this->data['a_status'] = Issue_model::getStatus();
        $this->data['selfId'] = SessionManagerWeb::getUserID();
        $this->load->model('Group_model');
        if (SessionManagerWeb::isAdministrator()) {
            $this->data['a_project'] = $this->Group_model->getDropdownProject(NULL, array('' => 'Semua Project'));
        } else {
            $this->data['a_project'] = $this->Group_model->getDropdownProject(SessionManagerWeb::getUserID(), array('' => 'Semua Project'));
        }
        $this->data['a_priority'] = Util::arrayMerge(array('' => 'Semua Priority'), Issue_model::getPriority());
        $this->data['a_type'] = Util::arrayMerge(array('' => 'Semua Jenis'), Issue_model::getType());
        $this->template->viewDefault($this->class . '_dashboard', $this->data);
    }

    /**
     * Halaman edit post
     * @param int $id
     */
    public function edit($id = null) {
        parent::edit($id);

        if (isset($id))
            $data = $this->model->get_by('posts.id', $id);
        else
            $data = array();

        // cek user
        if (isset($id) and $data['userId'] != SessionManagerWeb::getUserID()) {
            $ok = false;
            $msg = 'Tidak dapat mengubah tugas ini';

            SessionManagerWeb::setFlashMsg($ok, $msg);
            redirect($this->ctl . '/detail/' . $id);
        }

        // untuk multiselect
        if (!empty($data)) {
            $tdata = array();
            foreach ($data['postUsers'] as $v)
                $tdata[] = $v['id'];

            $data['usersID'] = $tdata;
        }

        $this->data['data'] = $data;

        // combo box
        $this->load->model('User_model');
        $rows = $this->User_model->order_by('name')->get_all();

        $a_user = array();
        foreach ($rows as $row)
            $a_user[$row['id']] = $row['name'];

        $this->data['a_kategori'] = $this->getListKategori();
        $this->data['a_prioritas'] = $this->getListPrioritas();
        $this->data['a_status'] = Issue_model::getStatus();
        $this->data['a_user'] = $a_user;

        $this->load->model('Group_model');
        $this->data['a_project'] = $this->Group_model->getDropdownProject(SessionManagerWeb::getUserID(), array('' => 'Tidak memiliki project'));

        $this->template->viewDefault($this->view, $this->data);
    }

    /**
     * Halaman detail tugas
     * @param int $id
     */
    public function detail($id) {
        parent::detail($id);

        // ambil detail
        $data = $this->model->get_by('posts.id', $id);

        // cek user
        if ($data['userId'] != SessionManagerWeb::getUserID())
            unset($this->data['buttons']['edit'], $this->data['buttons']['delete']);

        $this->data['data'] = $data;

        $this->data['a_status'] = Issue_model::getStatus();

        $this->template->viewDefault($this->view, $this->data);
    }

    /**
     * Membuat data baru
     */
    public function create() {
        if ($_FILES['image']['error'] != UPLOAD_ERR_OK)
            unset($_FILES['image']);
        if ($_FILES['video']['error'] != UPLOAD_ERR_OK)
            unset($_FILES['video']);
        if ($_FILES['file']['error'] != UPLOAD_ERR_OK)
            unset($_FILES['file']);

        $data = $this->input->post();
        $data['user_id'] = SessionManagerWeb::getUserID();
        $data['image'] = isset($_FILES['image']) ? $_FILES['image']['name'] : NULL;
        $data['video'] = isset($_FILES['video']) ? $_FILES['video']['name'] : NULL;
        $data['file'] = isset($_FILES['file']) ? $_FILES['file']['name'] : NULL;

        if (!empty($data['group_id'])) {
            $data['groupId'] = $data['group_id'];
        }
        unset($data['group_id']);

        $data['postUsers'] = empty($data['postUsers']) ? NULL : implode(',', $data['postUsers']);
        $data['date'] = empty($data['date']) ? NULL : FormatterWeb::formatDate($data['date']);
        $data['deadline'] = empty($data['deadline']) ? NULL : FormatterWeb::formatDate($data['deadline']);

        $insert = $this->model->create($data);
        if ($insert === true) {
            SessionManagerWeb::setFlashMsg(true, 'Berhasil membuat tugas');
            $this->redirect();
        } else {
            if (!is_string($insert)) {
                $validation = $this->model->getErrorValidate();
                if (empty($validation))
                    $msg = 'Gagal membuat tugas';
                else
                    $msg = implode('<br />', $validation);
            } else
                $msg = $insert;

            SessionManagerWeb::setFlashMsg(false, $msg);
            redirect($this->ctl . '/add');
        }
    }

    /**
     * Hapus data
     * @param int $id
     */
    public function delete($id) {
        $data = $this->model->get_by('posts.id', $id);
        if ($data['userId'] == SessionManagerWeb::getUserID()) {
            $this->load->model('Post_model');

            $delete = $this->Post_model->delete($id);

            if ($delete === true) {
                $ok = true;
                $msg = 'Berhasil menghapus tugas';
            } else {
                $ok = false;
                $msg = 'Gagal menghapus tugas';
            }
        } else {
            $ok = false;
            $msg = 'Tidak dapat menghapus tugas ini';
        }

        SessionManagerWeb::setFlashMsg($ok, $msg);
        redirect($_SERVER['HTTP_REFERER']);
    }

    /**
     * Edit data
     * @param int $id
     */
    public function update($id) {
        $issue = $this->model->get_by('posts.id', $id);
        if ($issue['userId'] == SessionManagerWeb::getUserID()) {
            if ($_FILES['image']['error'] != UPLOAD_ERR_OK)
                unset($_FILES['image']);
            if ($_FILES['video']['error'] != UPLOAD_ERR_OK)
                unset($_FILES['video']);
            if ($_FILES['file']['error'] != UPLOAD_ERR_OK)
                unset($_FILES['file']);

            $data = $this->input->post();
            if (isset($_FILES['image']))
                $data['image'] = $_FILES['image']['name'];
            if (isset($_FILES['video']))
                $data['video'] = $_FILES['video']['name'];
            if (isset($_FILES['file']))
                $data['file'] = $_FILES['file']['name'];

            $data['postUsers'] = empty($data['postUsers']) ? NULL : implode(',', $data['postUsers']);
            $data['date'] = empty($data['date']) ? NULL : FormatterWeb::formatDate($data['date']);
            $data['deadline'] = empty($data['deadline']) ? NULL : FormatterWeb::formatDate($data['deadline']);

            $insert = $this->model->save($data);
            if ($insert === true) {
                SessionManagerWeb::setFlashMsg(true, 'Berhasil mengubah tugas');
                $this->redirect();
            } else {
                if (!is_string($insert)) {
                    $validation = $this->model->getErrorValidate();
                    if (empty($validation))
                        $msg = 'Gagal mengubah tugas';
                    else
                        $msg = implode('<br />', $validation);
                } else
                    $msg = $insert;

                SessionManagerWeb::setFlashMsg(false, $msg);
                redirect($this->ctl . '/add');
            }
        }
        else {
            $ok = false;
            $msg = 'Tidak dapat mengubah tugas ini';
        }

        SessionManagerWeb::setFlashMsg($ok, $msg);
        redirect($this->ctl . '/me');
    }

    public function set_status($id) {
        $data = $this->model->get_by('posts.id', $id);
        $postUsers = Util::toList($data['postUsers'], 'id');
        $this->load->model('Group_member_model');
        $member = $this->Group_member_model->get_by(array('group_id' => $data['group']['id'], 'user_id' => SessionManagerWeb::getUserID()));
        if ($data['status'] != Issue_model::STATUS_CLOSE && $data['status'] != $this->postData['status'] && ($data['userId'] == SessionManagerWeb::getUserID() || in_array(SessionManagerWeb::getUserID(), $postUsers) || !empty($member))) {

//            if ($this->postData['status'] == Issue_model::STATUS_CLOSE && $data['userId'] != SessionManagerWeb::getUserID()) {
//                SessionManagerWeb::setFlashMsg(FALSE, 'Hanya pembuat yang dapat menutup issue ini');
//                redirect($_SERVER['HTTP_REFERER']);
//            }
            $update = $this->model->update($id, array('status' => $this->postData['status']));
            if ($update) {
                $statuses = Issue_model::getStatus();
                $this->load->model('Comment_model');
                $this->Comment_model->create(array(
                    'post_id' => $id,
                    'text' => '[' . $statuses[$this->postData['status']] . ']' . '  ' . $this->postData['description'],
                    'user_id' => SessionManagerWeb::getUserID(),
                    'candelete' => '0'
                        ), TRUE, TRUE, FALSE);
                $this->load->model('Notification_model');
                $this->Notification_model->generate(Notification_model::ACTION_ISSUE_STATUS, $id, $this->User_model->getUserID());
                SessionManagerWeb::setFlashMsg(TRUE, 'Berhasil mengubah status tugas ini menjadi ' . $statuses[$this->postData['status']]);
                redirect($_SERVER['HTTP_REFERER']);
            }
        }
        SessionManagerWeb::setFlashMsg(FALSE, 'Tidak dapat mengubah tugas ini');
        redirect($_SERVER['HTTP_REFERER']);
    }

    /**
     * Redirect, biasanya setelah tambah atau hapus data
     */
    protected function redirect() {
        $back = $this->postData['referer'];
        if (empty($back))
            redirect($this->ctl . '/me');
        else
            header('Location: ' . $back);
    }

    /**
     * Daftar kategori
     */
    public function getListKategori() {
        $data = array();
        $data[Category_model::TASK] = 'TASK';
        $data[Category_model::BUG] = 'BUG';
        $data[Category_model::ENHANCEMENT] = 'ENHANCEMENT';
        $data[Category_model::PROPOSAL] = 'PROPOSAL';

        return $data;
    }

    /**
     * Daftar prioritas
     */
    public function getListPrioritas() {
        $data = array();
        $data[Issue_model::PRIORITY_MINOR] = 'MINOR';
        $data[Issue_model::PRIORITY_NORMAL] = 'NORMAL';
        $data[Issue_model::PRIORITY_IMPORTANT] = 'IMPORTANT';
        $data[Issue_model::PRIORITY_URGENT] = 'URGENT';

        return $data;
    }

    /**
     * Daftar status
     */
    public function getListStatus() {
        $data = array();
        $data[Issue_model::STATUS_OPEN] = 'OPEN';
        $data[Issue_model::STATUS_START] = 'START';
        $data[Issue_model::STATUS_PROGRESS] = 'PROGRESS';
        $data[Issue_model::STATUS_DONE] = 'SELESAI';
        $data[Issue_model::STATUS_REOPEN] = 'REOPEN';
        $data[Issue_model::STATUS_CLOSE] = 'CLOSE';

        return $data;
    }

}
