<?php
    defined('BASEPATH') OR exit('No direct script access allowed');
    
    class Todo extends WEB_Controller {
		protected $title = 'To Do';
		
		/**
		 * Halaman daftar to do baru
		 * @param int $page
		 */
		public function for_new($page=0) {
			// tombol
			$buttons = array();
			$buttons[] = array('label' => 'Buat Pengingat !', 'type' => 'success', 'icon' => 'plus', 'click' => 'goAdd()');
			
			$this->data['buttons'] = $buttons;
			$this->data['data'] = $this->model->getNew(SessionManagerWeb::getUserID(),$page);
			
			$this->template->viewDefault($this->class.'_index',$this->data);
		}
		
		/**
		 * Halaman daftar to do lama
		 * @param int $page
		 */
		public function for_old($page=0) {
			$this->data['data'] = $this->model->getOld(SessionManagerWeb::getUserID(),$page);
			
			$this->template->viewDefault($this->class.'_index',$this->data);
		}
		
		/**
		 * Halaman edit to do
		 * @param int $id
		 */
        public function edit($id=null) {
			parent::edit($id);
			
			if(isset($id))
				$post = $this->model->get($id);
			else
				$post = array();
			
			// cek user
			if(isset($id) and $post['userId'] != SessionManagerWeb::getUserID()) {
				$ok = false;
				$msg = 'Tidak dapat mengubah to do ini';
				
				SessionManagerWeb::setFlashMsg($ok,$msg);
				redirect($this->ctl.'/for_new');
			}
			
			$this->data['data'] = $post;
			
			$this->template->viewDefault($this->view,$this->data);
		}
		
		/**
		 * Membuat data baru
		 */
		public function create() {
			$data = $this->postData;
			$data['user_id'] = SessionManagerWeb::getUserID();
			$data['date'] = empty($data['date']) ? NULL : FormatterWeb::formatDate($data['date']).' '.$data['time'].':00';
			
			$insert = $this->model->create($data);
			if($insert === true) {
				SessionManagerWeb::setFlashMsg(true,'Berhasil membuat to do');
				$this->redirect();
			}
			else {
				if(!is_string($insert)) {
					$validation = $this->model->getErrorValidate();
					if(empty($validation))
						$msg = 'Gagal membuat to do';
					else
						$msg = implode('<br />',$validation);
				}
				else
					$msg = $insert;
				
				SessionManagerWeb::setFlashMsg(false,$msg);
				redirect($this->ctl.'/add');
			}
		}
		
		/**
		 * Edit data
		 * @param int $id
		 */
		public function update($id) {
			$data = $this->model->get($id);
			if($data['userId'] == SessionManagerWeb::getUserID()) {
				$data = $this->postData;
				$data['date'] = empty($data['date']) ? NULL : FormatterWeb::formatDate($data['date']).' '.$data['time'].':00';
				
				unset($data['user_id']);
				
				$update = $this->model->save($id,$data,TRUE);
				if($update === true) {
					$ok = true;
					$msg = 'Berhasil mengubah to do';
				}
				else {
					$ok = false;
					if(!is_string($update)) {
						$validation = $this->model->getErrorValidate();
						if(empty($validation))
							$msg = 'Gagal mengubah to do';
						else
							$msg = implode('<br />',$validation);
					}
					else
						$msg = $update;
				}
			}
			else {
				$ok = true;
				$msg = 'Tidak dapat mengubah to do ini';
			}
			
			SessionManagerWeb::setFlashMsg($ok,$msg);
			$this->redirect();
		}
		
		/**
		 * Hapus data
		 * @param int $id
		 */
		public function delete($id) {
			$data = $this->model->get($id);
			if($data['userId'] == SessionManagerWeb::getUserID()) {
				$this->load->model('Post_model');
				
				$delete = $this->Post_model->delete($id);
			
				if($delete === true) {
					$ok = true;
					$msg = 'Berhasil menghapus to do';
				}
				else {
					$ok = false;
					$msg = 'Gagal menghapus to do';
				}
			}
			else {
				$ok = false;
				$msg = 'Tidak dapat menghapus to do ini';
			}
			
			SessionManagerWeb::setFlashMsg($ok,$msg);
			$this->redirect();
		}
		
		/**
		 * Redirect, biasanya setelah tambah atau hapus data
		 */
		protected function redirect() {
			$back = $this->postData['referer'];
			if(empty($back))
				redirect($this->ctl.'/for_new');
			else
				header('Location: '.$back);
		}
    }