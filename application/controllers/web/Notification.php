<?php
	defined('BASEPATH') OR exit('No direct script access allowed');
	
	class Notification extends WEB_Controller {
		protected $title = 'Notifikasi';
		
		/**
		 * Halaman default
		 * @param int $page
		 */
		public function index() {
			redirect($this->ctl.'/me');
		}
		
		/**
		 * Halaman daftar notifikasi
		 * @param int $page
		 */
		public function me($page=0) {
			// tombol
			$buttons = array();
			$buttons[] = array('label' => 'Set Baca Semua', 'type' => 'warning', 'icon' => 'check', 'click' => 'goReadAll()');
			
			$this->data['buttons'] = $buttons;
			$this->data['data'] = $this->model->getMe(SessionManagerWeb::getUserID(),$page);
			// echo "<pre>";print_r($this->data['data']);die();
			// $this->data['message'] = $this->model->generateMessage($this->data['data']); // nggak pakai
			$this->template->viewDefault($this->class.'_index',$this->data);
		}
		
		/**
		 * Set baca untuk semua notifikasi
		 * @param int $id
		 */
		public function read_all() {
			$this->load->model('Notification_user_model');
			$this->Notification_user_model->setAsUnRead(SessionManagerWeb::getUserID());
			
			$ok = true;
			$msg = 'Berhasil set baca semua notifikasi';
			
			SessionManagerWeb::setFlashMsg($ok, $msg);
			redirect($this->ctl.'/me');
		}
		
		/**
		 * Set baca untuk notifikasi
		 * @param int $id
		 */
		public function read($id) {
			$this->load->model('Notification_user_model');
			$this->Notification_user_model->update_by(array('user_id' => SessionManagerWeb::getUserID(), 'notification_id' => $id),array('status' => Notification_user_model::STATUS_READ));
		}
	}