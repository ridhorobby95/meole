<?php
//error_reporting(0);
session_start();
class Ajax extends WEB_Controller {
	
	protected $uri_segments = array();

    function __construct(){
		parent::__construct();
		$this->uri_segments = $this->uri->uri_to_assoc(2);
		$this->load->database();
	}

	function lst() {
		die();
	}

	function allusers() {
		parse_str($_SERVER['QUERY_STRING'], $_GET);
		$term = $this->input->get('term');
		
		$sql = "select id, name, username from users where role='O' and (name ilike '%{$term}%') and coalesce(username,'') != '' order by name limit 15 ";
		$list = dbGetRows($sql);
		
		$arr = array();
		foreach ($list as $row) {
			$label_user = $row['name'];
			$arr[] = array('id'=>$row['id'],
						   'label'=> '<img class="img-circle" style="width:25px" src="'. site_url('web/thumb/profile/'.$row['id']) .'"> ' . $row['name'] . ' - ' . $row['username'],
						   'value'=>$row['name'],
						   'imgsrc'=> base_assets().'images/agency1.png'
						   );
		}

		$json = json_encode($arr);
		echo $json;
		exit;
	}

	function groupusers() {
		parse_str($_SERVER['QUERY_STRING'], $_GET);
		$term = $this->input->get('term');
		$user_id = SessionManagerWeb::getUserId();
		$sql = "select u.id, u.name, u.username from group_members gm 
            left join users u on u.id=gm.user_id
            left join groups g on g.id=gm.group_id
            where role='O' and gm.group_id in (select group_id from group_members where user_id='{$user_id}' and is_admin=1)
            and (u.name ilike '%{$term}%') and coalesce(u.username,'') != '' order by u.name limit 15 ";

		$list = dbGetRows($sql);
		
		$arr = array();
		foreach ($list as $row) {
			$label_user = $row['name'];
			$arr[] = array('id'=>$row['id'],
						   'label'=> '<img class="img-circle" style="width:25px" src="'. site_url('web/thumb/profile/'.$row['id']) .'"> ' . $row['name'] . ' - ' . $row['username'],
						   'value'=>$row['name'],
						   'imgsrc'=> base_assets().'images/agency1.png'
						   );
		}

		$json = json_encode($arr);
		echo $json;
		exit;
	}


	function templatejawaban() {
        $this->load->model('Replies_model', 'Replies');
		$act = 'templatejawaban';
		$sess = 'ajax_' . $act;

		$this->Replies->limit = 10;
		
        $entry = $this->input->post('entry');
        if ($entry === '1') {
			$_SESSION["{$sess}_offset"] = 0;
			$_SESSION["{$sess}_keyword"] = 0;
			$_SESSION["{$sess}_keyword"] = xRemoveSpecial($this->input->post('keyword'));
		}
		$keyword = strtolower($_SESSION["{$sess}_keyword"]);

		if ($this->input->post('ret_function'))
			$_SESSION["{$sess}_retfunction"] = $this->input->post('ret_function');
		$ret_function = $_SESSION["{$sess}_retfunction"];

		$filter = 'where 1=1 ';
		if ($keyword != '') {
			$filter .= " and (lower(name) like '%{$keyword}%' ) ";
		}

        $this->Replies->setFilter($filter);

		$offset = (int) $_SESSION["{$sess}_offset"];
		$tot = $this->Replies->getCount();
		if ($offset > $tot) {
			$offset = floor(($offset-10)/10) * 10;
            $_SESSION["{$sess}_offset"] = $offset;
		}

		$page = abs((int) floor($offset/$this->Replies->limit)+1);
		// if (SessionManagerWeb::isSuperAdministrator()) {
		// 	$helpdesk = NULL;
		// } else {
		// 	$helpdesk = $this->data['list_my_helpdesk'][0]['name'];
		// }
        $rows = $this->Replies->getList($page, SessionManagerWeb::getUserID());
		$ret .=  "<table class=\"table table-striped table-condensed\"><tr><th>Nama</th><th width=\"40\">&nbsp;</th></tr>";
		$i = 0;
        foreach($rows as $row) {
            $i++;
            $class = ($i%2===1)?"class=\"odd\"":"";
            $ret .= "<tr {$class}>";
            if ($ret_function) {
				$nama = str_replace("'","",$row['name']);
                $ret .= "<td>{$row['name']}</td>";
                $ret .= "<td><span class=\"btn btn-xs btn-info\" onclick=\"{$ret_function}('{$row['id']}','$nama');resetLOV('$act');\">Pilih</span></td>";
            }
            $ret .= "</tr>";
        }
		if ($i==0) {
			$ret .= "<tr><td colspan=\"3\">Tidak ada data</td></tr>";
		}
		
		$ret .= "</table>";
		
 		$ret = '<div id="sevimalov_div2">'.$ret.'</div>';
		
		if (!$_POST['searchlov2'])
			$ret = $this->_headerLOV($act) . $ret;

		echo $ret;
		exit;
	}
		
################################################################################

	function prev() {
		$this->_nav();
	}
	
	function next() {
		$this->_nav();
	}
	
	function reset() {
		$this->_nav();
	}
	
	function eselon() {
		$this->_nav();
	}
	
	function _nav() {
//		var_dump($this->uri_segments);die();

		$act = $this->uri_segments['act'];
		//echo $act;die();
		$extra = $this->uri_segments['extra'];
		$this->_getNavigation($act, $this->router->fetch_method(), $extra);
	}

	function _getNavigation($act, $nav, $extra='') {
		$sess = 'ajax_' . $act;
		if ($nav == 'prev') {
			$_SESSION["{$sess}_offset"] = (int) $_SESSION["{$sess}_offset"]-10;
			if ($_SESSION["{$sess}_offset"] < 0)
				$_SESSION["{$sess}_offset"] = 0;	
			//echo $_SESSION["{$sess}_offset"];
			return $this->$act();
		}
		elseif ($nav == 'next') {
			$_SESSION["{$sess}_offset"] = (int) $_SESSION["{$sess}_offset"]+10;
			//echo $_SESSION["{$sess}_offset"];
			return $this->$act();
		}
		elseif ($nav == 'reset') {
            unset($_SESSION["{$sess}_offset"]);
            unset($_SESSION["{$sess}_keyword"]);
            unset($_SESSION['lov_eselon']);
            unset($_SESSION['lov_idunit']);
	
			return $this->$act();
		}
	}

	function _headerLOV($act, $extra_function=null) {
        $sess = 'ajax_' . $act;
		$keyword = $_SESSION["{$sess}_keyword"];

        $sess = 'ajax_' . $act;
		$keyword = $_SESSION["{$sess}_keyword"];

		$ret = "<table class=\"table table-condensed drag_handle\" style=\"margin:0\"><tr class=\"info\"><td>";
		$ret .= "<form onsubmit=\"searchLOV('$act');return false;\" style=\"float:left\">
			<table style=\"width:100%\">
			<tr>
			<td><input type=\"text\" id=\"sevimalov_keyword\" name=\"sevimalov_keyword\" value=\"$keyword\" size=\"40\" />&nbsp; </td>
			<td><span class=\"btn btn-xs btn-default\" onclick=\"navigateLOV('$act','reset')\" title=\"Reset\" value=\"Reset\">Reset</span>&nbsp; </td>
			<td><span class=\"btn btn-xs btn-default\" onclick=\"navigateLOV('$act','prev')\" title=\"Sebelumnya\"><i class=\"glyphicon glyphicon-chevron-left\"></i></span>&nbsp; </td>
			<td><span class=\"btn btn-xs btn-default\" onclick=\"navigateLOV('$act','next')\" title=\"Selanjutnya\"><i class=\"glyphicon glyphicon-chevron-right\"></i></span>&nbsp; </td>";
		if ($extra_function) {
			$ret .= "<td><span class=\"btn btn-xs btn-default\" id=\"btn_{$extra_function['nama']}\" onclick=\"{$extra_function['nama']}()\" title=\"{$extra_function['label']}\">{$extra_function['label']}</span></td>";
		}
		$ret .= "</tr></table>";

		$ret .=	"</form>";
		$ret .= "<span style=\"float:right\">";
		$ret .= xClosePop();
		$ret .=	"</span>";
		$ret .= "</td></tr></table>";
		
		# default focus
		$ret .= "<script type=\"text/javascript\">$('#sevimalov_keyword').focus()</script>";		
		$ret .= "<script>$('#sevimalov_keyword').on('change keyup', function(){ searchLOV2('$act');});</script>";
		return $ret;

	}
	
	function _headerLOV2($title) {
		$ret = "<table class=\"table table-condensed drag_handle\" style=\"width:100%;margin:0\"><tr class=\"info\"><td><span style=\"font-size:14px\">$title</span>".
		$ret .= "<span style=\"float:right\">".
				"<span class=\"btn btn-xs btn-danger\"onclick=\"closePop()\"><i class=\"glyphicon glyphicon-remove\"></i></span>".
				"</span>";
		$ret .= "</td></tr></table>";
		return $ret;
	}
	

	function _retParseAjax($inputRet) {
		$sep1 = '_##_';
		$sep2 = '__|__';
		$sep3 = '::';
		
		$ret = '';
		
		foreach ($inputRet as $arr) {
			$id = $arr['id'];
			$type = $arr['type'];
			$value = $arr['value'];
			$ret .= "{$id}{$sep3}{$type}{$sep2}{$value}{$sep1}";
		}
		return $ret;
	}

}
