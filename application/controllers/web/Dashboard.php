<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Dashboard extends WEB_Controller {

    protected $title = 'Dashboard';

    /**
     * Halaman default
     * @param int $page
     */
    public function index() {
        $buttons = array();
        $buttons[] = array('label' => 'Beri Tugas', 'type' => 'success', 'icon' => 'plus', 'click' => 'goAdd()');
        $this->data['buttons'] = $buttons;

        $condition = array();
        $getData = $this->input->get();
        if (!empty($getData['project']))
            $condition['posts.group_id'] = $getData['project'];
        if (!empty($getData['jenis']))
            $condition['posts.category_id'] = $getData['jenis'];
        if (!empty($getData['priority']))
            $condition['issues.priority'] = $getData['priority'];
        if (!empty($getData['start_date']))
            $condition['issues.deadline >='] = FormatterWeb::formatDate($getData['start_date']);
        if (!empty($getData['end_date']))
            $condition['issues.deadline <='] = FormatterWeb::formatDate($getData['end_date']);
        if (!empty($getData['assignment_task']) && $getData['assignment_task'] == 'M')
            $condition['post_users.user_id'] = SessionManagerWeb::getUserID();

        if (SessionManagerWeb::isAdministrator()) {
            $this->data['data'] = $this->model->getDashboard(NULL, $condition);
        } else {
            $this->data['data'] = $this->model->getDashboard(SessionManagerWeb::getUserID(), $condition);
        }
        $this->data['container'] = 'container-fluid';
        $this->data['a_status'] = Issue_model::getStatus();
        $this->data['selfId'] = SessionManagerWeb::getUserID();
        $this->load->model('Group_model');
        if (SessionManagerWeb::isAdministrator()) {
            $this->data['a_project'] = $this->Group_model->getDropdownProject(NULL, array('' => 'Semua Project'));
        } else {
            $this->data['a_project'] = $this->Group_model->getDropdownProject(SessionManagerWeb::getUserID(), array('' => 'Semua Project'));
        }
        $this->data['a_priority'] = Util::arrayMerge(array('' => 'Semua Priority'), Issue_model::getPriority());
        $this->data['a_type'] = Util::arrayMerge(array('' => 'Semua Jenis'), Issue_model::getType());
        
        $this->template->viewDefault($this->class . '_index', $this->data);
    }

}
