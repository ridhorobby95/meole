<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Site extends PublicApiController {

    public function login() {
        
        $data = AuthManager::login($this->input->post('username'), $this->input->post('password'), $this->input->post('device'));

        if (is_string($data)) {
            $this->setResponse($this->setSystem(ResponseStatus::ERROR, $data));
        } else {
            list($message, $token, $user) = $data;
            $this->load->model('User_token_model');
            $user['userToken'] = $this->User_token_model->with('Device')->get_by(array('token' => $token));
            $this->systemResponse->token = $token;
            $this->setResponse($this->setSystem(ResponseStatus::SUCCESS, $message), $user);
        }
    }

    public function register(){
        $date = date('m/d/Y', time());
        $postData = $this->input->post();
        $secure_key = md5('p05O#m30LE12tiga_'.$date);
        if($postData['secure_key'] == $secure_key){
            $this->load->model('User_model');
            $auth_key = SecurityManager::generateAuthKey();
            $dataInsert = array(
                'name'          => $postData['name'],
                'username'      => strtolower($postData['username']),
                'role'          => Role::WARGA,
                'updated_at'    => $date = date('m/d/Y h:i:s a', time()),
                'is_excluded'   => 1,
                'is_active'     => 0,
                'email'         => strtolower($postData['email']),
                'no_hp'         => $postData['no_hp'],
                'password'      => SecurityManager::hashPassword($postData['password'], $auth_key),
                'password_salt' => $auth_key
            );

            $this->data['password'] = $postData['password'];
            $this->data['auth_key'] = $auth_key;
            $this->data['has']      = SecurityManager::hashPassword($postData['password'], $auth_key);
            // $this->setResponse($this->setSystem(ResponseStatus::SUCCESS, "Berhasil mendaftarkan diri"), $this->data);

            // echo $postData['password']."<br>".$auth_key."<br>".SecurityManager::hashPassword($postData['password'], $auth_key);

            // die();

            $insert = dbInsert('users', $dataInsert);
            if($insert){
                $user_id = dbGetOne("select id from users where email='".strtolower($postData['email'])."'");
                $this->load->model('Email_notification_model');
                $dataActication = array(
                    'user_id'   => $user_id,
                    'activation_id' => md5($user_id.date("Y-m-d H:i:s"))
                );
                dbInsert("user_activation",$dataActication);
                $this->Email_notification_model->create(Email_notification_model::EMAIL_REGISTER, $user_id);
                $this->setResponse($this->setSystem(ResponseStatus::SUCCESS, "Berhasil mendaftarkan diri"), $this->data);
            }else{
                $this->setResponse($this->setSystem(ResponseStatus::ERROR, 'Gagal mendaftarkan diri'));
            }
        }else{
            $this->setResponse($this->setSystem(ResponseStatus::ERROR, 'You can\'t access this function '));
        }
    }

    public function checkEmail(){
        $email = $this->input->post('email');
        $this->load->model('User_model');
        $exist = $this->User_model->column('id')->filter(" where email='".strtolower($email)."'")->getOne();
        if($exist){
            $this->data['exist'] = true;
            $this->setResponse($this->setSystem(ResponseStatus::SUCCESS, "Email is exist"), $this->data);
        }else{
            $this->data['exist'] = false;
            $this->setResponse($this->setSystem(ResponseStatus::SUCCESS, "Email is not exist"), $this->data);
        }
    }


}

?>