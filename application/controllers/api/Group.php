<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Group extends PrivateApiController {

    /**
     * Digunakan untuk mengambil list group
     */
    public function list_me() {
        if ($this->user['role']['id'] == Role::ADMINISTRATOR) {
            $groups = $this->model->getAll();
        } else {
            $groups = $this->model->getMe($this->user['id']);
        }

        $this->setResponse($this->setSystem(ResponseStatus::SUCCESS), $groups);
    }

    public function list_for_me() {
        if ($this->user['role']['id'] == Role::ADMINISTRATOR) {
            $groups = $this->model->getAll();
        } else {
            $groups = $this->model->getForMe($this->user['id']);
        }

        $this->setResponse($this->setSystem(ResponseStatus::SUCCESS), $groups);
    }

    public function getListPerMember() {
        $data['user_id'] = $this->user['id'];
        $groups = $this->model->getListPerMember($data['user_id'],1);
        $this->setResponse($this->setSystem(ResponseStatus::SUCCESS),$groups);
    }

    public function getListHelpdesk() {
        $data['user_id'] = $this->user['id'];
        $groups = $this->model->getDaftarHelpdesk();
        $this->setResponse($this->setSystem(ResponseStatus::SUCCESS),$groups);
    }

    /**
     * Digunakan untuk mengambil detail group beserta member dari group dengan ID group
     */
    public function detail($id) {
        //SET READ NOTIFICATION
        $this->load->model('Notification_model');
        $this->load->model('Notification_user_model');
        $this->Notification_user_model->setAsRead($this->user['id'], Notification_model::TYPE_GROUP_DETAIL, $id);

        $group = $this->model->get($id);

        $this->setResponse($this->setSystem(ResponseStatus::SUCCESS), $group);
    }

    public function create() {
        $data = $this->postData;
        if (isset($_FILES['image'])) {
            $data['image'] = $_FILES['image']['name'];
        }
        $data['userId'] = (!empty($data['owner'])) ? $data['owner'] : $this->user['id'];
        $insert = $this->model->create($data);
        if ($insert === TRUE) {
            $this->setResponse($this->setSystem(ResponseStatus::SUCCESS, 'Berhasil membuat group'));
        } elseif (is_string($insert)) {
            $this->setResponse($this->setSystem(ResponseStatus::ERROR, $insert));
        } else {
            $validation = $this->model->getErrorValidate();
            if (empty($validation)) {
                $this->setResponse($this->setSystem(ResponseStatus::ERROR, 'Gagal membuat group'));
            } else {
                $this->setResponse($this->setSystem(ResponseStatus::VALIDATION_ERROR, NULL, $validation));
            }
        }
    }

    public function edit($id) {
        $group = $this->model->get($id);
        if ($group['owner']['id'] == $this->user['id']) {
            $data = $this->postData;
            if (isset($_FILES['image'])) {
                $data['image'] = $_FILES['image']['name'];
            }
            $data['userId'] = (!empty($data['owner'])) ? $data['owner'] : $this->user['id'];
            $insert = $this->model->save($id, $data);
            if ($insert === TRUE) {
                $this->setResponse($this->setSystem(ResponseStatus::SUCCESS, 'Berhasil mengubah grup'));
            } elseif (is_string($insert)) {
                $this->setResponse($this->setSystem(ResponseStatus::ERROR, $insert));
            } else {
                $validation = $this->model->getErrorValidate();
                if (empty($validation)) {
                    $this->setResponse($this->setSystem(ResponseStatus::ERROR, 'Gagal mengubah grup'));
                } else {
                    $this->setResponse($this->setSystem(ResponseStatus::VALIDATION_ERROR, NULL, $validation));
                }
            }
        } else {
            $this->setResponse($this->setSystem(ResponseStatus::ERROR, "Tidak dapat mengubah grup ini"));
        }
    }

    public function delete($id) {
        $group = $this->model->get($id);
        if ($group['owner']['id'] == $this->user['id']) {
            $delete = $this->model->delete($id);
            if ($delete === TRUE) {
                $this->setResponse($this->setSystem(ResponseStatus::SUCCESS, 'Berhasil menghapus grup'));
            } else {
                $this->setResponse($this->setSystem(ResponseStatus::ERROR, "Gagal menghapus grup"));
            }
        } else {
            $this->setResponse($this->setSystem(ResponseStatus::ERROR, "Tidak dapat menghapus grup ini"));
        }
    }

    public function saveCover() {
        $data = $this->postData;
        $_FILES['cover']['type'] = 'image/jpeg';
        $group = $this->model->savecover($data['group_id'],$data);
        if ($group===true) {
            $this->setResponse($this->setSystem(ResponseStatus::SUCCESS, "Berhasil mengganti Cover"));
        } else {
            $this->setResponse($this->setSystem(ResponseStatus::SUCCESS, "Gagal mengganti Cover"));
        }
    }

    public function saveIcon() {
        $data = $this->postData;
        $_FILES['image']['type'] = 'image/jpeg';
        $group = $this->model->savephoto($data['group_id'],$data);
        if ($group===true) {
            $this->setResponse($this->setSystem(ResponseStatus::SUCCESS, "Berhasil mengganti Icon"));
        } else {
            $this->setResponse($this->setSystem(ResponseStatus::SUCCESS, "Gagal mengganti Icon"));
        }
    }

    public function getGroup() {
        $rows = $this->model->getDaftar();
        $this->setResponse($this->setSystem(ResponseStatus::SUCCESS),$rows);
    }

    public function getMyHelpdesk() {
        $rows = $this->Group_member_model->getMyHelpdeskGroup($this->user['id']);
        $this->setResponse($this->setSystem(ResponseStatus::SUCCESS), $rows);
    }

    public function isThisGroupAdmin(){
        $data = $this->postData;
        $rows = $this->Group_member_model->isAdminThisGroup($data['group_id'],$this->user['id']);
        $this->setResponse($this->setSystem(ResponseStatus::SUCCESS, $rows));
    }

}

?>