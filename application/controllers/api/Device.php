<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Device extends PrivateApiController {

    public function register_id($id) {
        $this->load->model('User_token_model');
        $userToken = $this->User_token_model->get_by(array('token' => $this->token));
        if ($userToken['deviceId'] == $id) {
            $register = $this->model->update($id, array('reg_id' => $this->postData['regId']), TRUE);
            if ($register === TRUE) {
                $this->setResponse($this->setSystem(ResponseStatus::SUCCESS, 'Berhasil mendaftarkan ID'));
            } else {
                $this->setResponse($this->setSystem(ResponseStatus::ERROR, 'Gagal mendaftarkan ID'));
            }
        } else {
            $this->setResponse($this->setSystem(ResponseStatus::ERROR, 'Token tidak dapat mendaftarkan ID'));
        }
    }
    
    public function update_by_secure_id(){
	$this->load->model('User_token_model');
	$this->load->model('Device_model');
        $userToken = $this->User_token_model->get_by(array('token' => $this->token));
        $device = $this->Device_model->get_by('secure_id', $this->postData['secureId']);
        if ($userToken['deviceId'] == $device['id']) {
            $register = $this->model->update($device['id'], array('reg_id' => $this->postData['regId']), TRUE);
            if ($register === TRUE) {
                $this->setResponse($this->setSystem(ResponseStatus::SUCCESS, 'Berhasil mendaftarkan ID'));
            } else {
                $this->setResponse($this->setSystem(ResponseStatus::ERROR, 'Gagal mendaftarkan ID'));
            }
        } else {
            $this->setResponse($this->setSystem(ResponseStatus::ERROR, 'Token tidak dapat mendaftarkan ID'));
        }
    }

}

?>