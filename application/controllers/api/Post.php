<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Post extends PrivateApiController {

    function __construct() {
        parent::__construct();
        $this->load->model('User_model');
    }

    /**
     * Digunakan untuk mengambil list post public
     */
    public function index() {
        // $id = md5('10'. $this->config->item('encryption_key'));
        // echo $id;die();
        // $a = Image::getFileName($id,Image::IMAGE_ORIGINAL,'JPEG_20191024_143157_-1475482322.jpg');
        // echo "<pre>";print_r($a);die();
        // // $data = $this->postData;
        // // die();
        // $home_status = array($data['home_status'] => 1);
        // $sticky = $this->model->getStickyByType($this->user['id'],"G");

        // if($data['param']){
        //     $this->load->model("Search_log_model");
        //     $log_data = array(  "keyword" => $data['param'],
        //                         "user_id" => $this->user['id']);
        //     $insert = $this->Search_log_model->insert($log_data, TRUE, FALSE);
        // }
        $this->data = $this->model->getPosts($this->user['id'],$this->page, $this->postData['mine']);
        // $posts = $this->model->getPosts($this->user['id'],3, $this->postData['param'],$home_status,NULL);
        // if ($sticky==FALSE) {
        // } else {
            // $result = array_merge($sticky,$posts);
        // }
        $this->setResponse($this->setSystem(ResponseStatus::SUCCESS), $this->data);
    }


    public function vote($vote = 1){
        $data = $this->postData;
        if($this->postData){
            $vote = $data['vote'];
            $post_id = $data['post_id'];
        }
        $checkData = dbGetOne("select 1 from post_vote where user_id=".$this->user['id']." and post_id=$post_id ");
        if(!$checkData && $vote==1){ // belum ada data dan melakukan vote
            $addVote = $this->model->upVote($post_id, $this->user['id']);
            $this->load->model('Notification_model');
            $this->Notification_model->generate(Notification_model::ACTION_UPVOTE, $post_id, $this->user['id']);
            $this->setResponse($this->setSystem(ResponseStatus::SUCCESS), 'Berhasil memberikan vote');
        }
        elseif(!$checkData && $vote!=1){ // belum ada data dan melakukan unvote
            $this->setResponse($this->setSystem(ResponseStatus::ERROR, "Anda belum memberikan vote"));
        }
        elseif($checkData && $vote!=1){ // ada data dan melakukan unvote
            $deleteVote = $this->model->unVote($post_id, $this->user['id']);
            $this->setResponse($this->setSystem(ResponseStatus::SUCCESS), 'Berhasil melakukan unvote');
        }elseif($checkData && $vote==1){// ada data dan melakukan vote
            $this->setResponse($this->setSystem(ResponseStatus::ERROR, "Anda sudah memberikan vote"));
        }
    }

    /**
     * Digunakan untuk mengambil list tag post public
     */
    public function tag() {
        $data = $this->postData;

        $home_status = array($data['home_status'] => 1);

        if($data['param']){
            $this->load->model("Search_log_model");
            $log_data = array(  "keyword" => $data['param'],
                                "user_id" => $this->user['id']);
            $insert = $this->Search_log_model->insert($log_data, TRUE, FALSE);
        }

        // check tags
        $tag_param = $data['param'];
        $post_tag_ids = $this->model->checkPostTag($tag_param);
        $arr_post_ids = array();
        foreach ($post_tag_ids as $post_tag_id) {
            $arr_post_ids[] = $post_tag_id['post_id'];
        }
        $post_ids = implode(',', $arr_post_ids);

        $posts = $this->model->show_sql(false)
                            ->filter("and p.id in ($post_ids)")
                            ->getPosts($this->user['id'],$this->page, 'filter_for_tag_purpose12tiga',$home_status,$this->postData['group_selected']);

        // $posts = $this->model->getPosts($this->user['id'],$this->page, 'filter_for_tag_purpose12tiga',$home_status,$this->postData['group_selected']);
        $result = $posts;
        $this->setResponse($this->setSystem(ResponseStatus::SUCCESS), $result);
    }

    public function getKnowledge() {
        $tag = $this->postData['param'];
        $sticky = $this->model->getStickyByType($this->user['id'],"K");
        $posts = $this->model->getKnowledges($this->user['id'],$this->postData['helpdesk'],$this->page, NULL, $tag);
        if ($sticky==FALSE) {
            $result = $posts;
        } else {
            $result = array_merge($sticky,$posts);
        }
        $this->setResponse($this->setSystem(ResponseStatus::SUCCESS), $result);
    }

    public function getInformation() {
        $sticky = $this->model->getStickyByType($this->user['id'],"I");
        $posts = $this->model->getInformations($this->user['id'],$this->postData['helpdesk'],$this->page, $this->postData['param']);
        if ($sticky==FALSE) {
            $result = $posts;
        } else {
            $result = array_merge($sticky,$posts);
        }
        $this->setResponse($this->setSystem(ResponseStatus::SUCCESS), $result);
    }

    public function ratePost() {
        $data = $this->postData;
        $posts = $this->model->setRating($data['post_id'],$data['rating'],$this->user['id']);
        if ($posts === TRUE) {
            $this->setResponse($this->setSystem(ResponseStatus::SUCCESS, "Terima kasih telah memberi rating"));
        } else {
            $this->setResponse($this->setSystem(ResponseStatus::ERROR, "Gagal memberi rating"));
        }
    }

    public function setStatus() {
        $data = $this->postData;
        $posts = $this->model->setStatus($data['post_id'],$data['status']);
        if ($posts === TRUE) {
            $this->setResponse($this->setSystem(ResponseStatus::SUCCESS, "Berhasil mengganti status"));
        } else {
            $this->setResponse($this->setSystem(ResponseStatus::ERROR, "Gagal mengganti status"));
        }
    }

    public function tugaskanStaff() {
        $data = $this->postData;
        $posts = $this->model->assignTicket($data['post_id'],$data['list_staff']);
        if ($posts === TRUE) {
            $this->setResponse($this->setSystem(ResponseStatus::SUCCESS, "Berhasil menugaskan staff"));
        } else {
            $this->setResponse($this->setSystem(ResponseStatus::ERROR, "Gagal menugaskan staff"));
        }
    }


    public function testNotif(){
        // echo $this->user['id'];die();
        $this->load->model('Notification_model');
        // echo "asdasd";die();
        $this->Notification_model->generate(Notification_model::ACTION_POST_CREATE, 17266,$this->user['id']);
    }
    public function create() {
        $data = $this->postData;
        // $temp['user_id'] = $data['user_id'];

        if ($data['user_id']=="") {
            $data['user_id'] = $this->user['id'];
            $users = $this->User_model->getUser($data['user_id']);  
        }

        $insert = $this->model->create($data);
        if ($insert) {
            $this->db->trans_begin();
            if (!$this->_moveFileTemp($insert)) {
                $this->db->trans_rollback();
                $this->setResponse($this->setSystem(ResponseStatus::ERROR, 'Gagal membuat Laporan'));
            } else {
                $this->load->model('Notification_model');
                $this->Notification_model->generate(Notification_model::ACTION_POST_CREATE, $insert,$this->user['id']);
                $this->db->trans_commit();
                $this->setResponse($this->setSystem(ResponseStatus::SUCCESS, $insert));

            //     // Membuat log ketika 'kirim_sebagai'
                // if ($temp['user_id'] != "") {
                //     $this->load->model('Comment_model', 'Comment');
                //     $data = array();
                //     $data['user_id'] = $this->user['id'];
                //     $data['post_id'] = $insert;
                //     $data['candelete'] = 0;
                //     $users = $this->User_model->getUser($data['user_id']);
                //     $data['text'] = '<span style="color:ddd;font-size:11px">[membuat tiket sebagai ' . $users['name'] . ']</span>';

                //     $comment_id = $this->Comment->create($data, TRUE, TRUE);
                // }
            }
        } 
        else {
            $validation = $this->model->getErrorValidate();
            if (empty($validation)) {
                $this->setResponse($this->setSystem(ResponseStatus::ERROR, 'Gagal membuat Laporan'));
            } else {
                $this->setResponse($this->setSystem(ResponseStatus::VALIDATION_ERROR, NULL, $validation));
            }
        }

    }

    public function updatePost($id) {
        $data = $this->postData;

        if ($data['postTags']!=null) {
            $data['postTags'] = json_decode($data['postTags']);
        }

        $update = $this->model->update($data,$id);

        if ($update) {
            $this->db->trans_begin();
            if ($this->_moveFileTemp($id)) {
                $this->db->trans_commit();
                $this->setResponse($this->setSystem(ResponseStatus::SUCCESS, 'Berhasil update Pesan'));
            } else {
                $this->db->trans_rollback();
                $this->setResponse($this->setSystem(ResponseStatus::ERROR, 'Gagal update Pesan'));
            }
        } elseif (is_string($insert)) {
            $this->setResponse($this->setSystem(ResponseStatus::ERROR, $insert));
        } else {
            $validation = $this->model->getErrorValidate();
            if (empty($validation)) {
                $this->setResponse($this->setSystem(ResponseStatus::ERROR, 'Gagal update Pesan'));
            } else {
                $this->setResponse($this->setSystem(ResponseStatus::VALIDATION_ERROR, NULL, $validation));
            }
        }
    }

    public function getKuota() {
        $data = $this->postData;
        $this->load->model("Post_quota_model");
        $row_quota = array();
        $row_quota = $this->Post_quota_model->getQuota($this->user['id'], $data['group_id']);

        $kuota = (int) $row_quota['quota'];
        $limit = (int) $row_quota['limit'];

        if ($kuota < $limit || $row_quota === NULL) {
            $this->setResponse($this->setSystem(ResponseStatus::SUCCESS), $row_quota);
        } else {
            $this->setResponse($this->setSystem(ResponseStatus::ERROR, 'Anda telah menggunakan '.$kuota.' dari '.$limit.' kuota tiket yang tersedia'));
        }
    }

    public function sendImage($folder='posts/temp') {
        $folder_ori = $folder;

        if ($_FILES) {
            $images_arr = array();

            $id = md5($this->user['id'] . $this->config->item('encryption_key'));

            $folder .= '/' . $id;
            $ciConfig = $this->config->item('utils');
            $path = $ciConfig['full_upload_dir'] . $folder . '/';
            if (!is_dir($path)) {
               mkdir($path);         
            }
            
            $name = $_FILES['gambar']['name'];

            $file_name = Image::getFileName($id, Image::IMAGE_ORIGINAL, $name);
            unlink($path . $file_name);
            
            $file_mime = Image::getMime($folder.$file_name);
            $image_mime = explode('/',$file_mime);
            if ($image_mime[0]=='image') {
                Image::upload('gambar', $id, $name, $folder);
                $link = Image::generateLink($id, $name, $folder);
                $images_arr[] = $link['thumb'];
            }
            // kalau mau responsenya list dari 5 file image yang diconvert 
            $this->setResponse($this->setSystem(ResponseStatus::SUCCESS, "Success"));
        } 
    }

    public function sendFile($folder='posts/temp') {
        $folder_ori = $folder;

        if ($_FILES) {
            $files_arr = array();

            $id = md5($this->user['id'] . $this->config->item('encryption_key'));

            $folder .= '/' . $id;
            $ciConfig = $this->config->item('utils');
            $path = $ciConfig['full_upload_dir'] . $folder . '/';
            if (!is_dir($path)) {
               mkdir($path);         
            }

            $name = $_FILES['file_upload']['name'];
            $file_name = File::getFileName($id, $name);
            unlink($path . $file_name);

            File::uploadFile('file_upload', $id, $name, $folder);
            $link = File::generateLink($id, $name, $folder);
            // $files_arr = $link;

            $this->setResponse($this->setSystem(ResponseStatus::SUCCESS, ""));
        }    
    }

    function _moveFileTemp($post_id, $folder_source='posts/temp') {
        $id = md5($this->user['id']. $this->config->item('encryption_key'));
        $folder_source .= '/'.$id;
        $ciConfig = $this->config->item('utils');
        $path_source = $ciConfig['full_upload_dir'] . $folder_source . '/';
        if (is_dir($path_source)) {
            $files = glob($path_source.'*'); 
            $arr_image = array();
            $arr_files = array();
            foreach ($files as $file) {
                list($mime,$ext) = explode('/',Image::getMime($file));
                $basename = basename($file);
                if ($mime=='image') {
                    $folder_dest = 'posts/photos';
                    $path_dest = $ciConfig['full_upload_dir'] . $folder_dest . '/';
                    $image = Image::getName($basename);
                    if (!in_array($image, $arr_image)) {
                        $arr_image[] = $image;
                        if (!$this->model->insertPostImage($post_id, $image, $file)) {
                            return false;
                        }
                    }
                } else {
                    $folder_dest = 'posts/files';
                    $path_dest = $ciConfig['full_upload_dir'] . $folder_dest . '/';
                    $fil = File::getName($basename);
                    if (!in_array($fil, $arr_files)) {
                        $arr_files[] = $fil;

                        if (!$this->model->insertPostFile($post_id, $fil, $file)) {
                            return false;
                        }
                    }
                }
                

                if (!rename($file, $path_dest . '/' . $basename)) {
                    return false;
                }
            }
        }
        else {
            return true;
        }
        return $this->_deleteFiles($path_source);
    }

    function _getdraftfiles($folder='posts/temp') {
        $str = '';
        $id = md5($this->user['id'] . $this->config->item('encryption_key'));
        $folder .= '/' . $id;
        $ciConfig = $this->config->item('utils');
        $path = $ciConfig['full_upload_dir'] . $folder . '/';
        $rel_path = $ciConfig['upload_dir'] . $folder . '/';
        if (is_dir($path)) {
           $files = glob($path.'*'); 
           foreach ($files as $file) {
               $file_src = basename($file);
               $file_name = explode('-',$file_src);
               $arr_file_ext = explode('.',$file_name[count($file_name)-1]);
               $file_ext = $arr_file_ext[count($arr_file_ext)-1];
               if ($file_ext=="pdf" || $file_ext=="doc" || $file_ext=="docx" || $file_ext=="xls" || $file_ext=="xlsx" || $file_ext=="ppt" || $file_ext=="pptx"){
                    $index = 0;
                    $name = '';
                    foreach ($file_name as $fname){
                        if ($index>0) {
                            $name .=$fname;
                            if ($index<count($file_name)-1) {
                                $name .='-';
                            }
                        }
                        $index++;
                    }
                    $str .= $name . "<br>";
               }
           }
        }
        return $str;
    }

    function _deleteFiles($path){
        if (is_dir($path) === true) {
            $files = array_diff(scandir($path), array('.', '..'));
            foreach ($files as $file) {
                $this->_deleteFiles(realpath($path) . '/' . $file);
            }
            return rmdir($path);
        }

        else if (is_file($path) === true) {
            return unlink($path);
        }

        return false;
    }

    /**
     * Digunakan untuk mengambil post dari group dengan ID group
     */
    public function group($id, $page = 0) {
        $posts = $this->model->getGroup($id, $page, $this->postData['param']);

        $this->setResponse($this->setSystem(ResponseStatus::SUCCESS), $posts);
    }

    /**
     * Digunakan untuk mengambil post public, dari group yang diikuti, dan dari tag friend
     */
    public function me() {
        $posts = $this->model->getMe($this->user['id'], $this->page, $this->postData['param']);

        //SET RESPONSE
        $this->setResponse($this->setSystem(ResponseStatus::SUCCESS), $posts);
    }

    /**
     * Digunakan untuk mengambil post dengan ID post beserta commentnya
     */
    public function detail($id, $notif_user_id) {
        // set "sudah baca"
        $this->load->model('Notification_model');
        $this->load->model('Notification_user_model');
        $this->Notification_user_model->setAsRead($notif_user_id);


        // check view
        $this->model->checkView($id, $this->user['id']);

        $this->data = $this->model->getDetail($id, $this->user['id']);
        $this->load->model('Dinas_diversion_model');
        $this->load->model('Comment_model');
        $this->load->model('Post_log_model');
        $this->data['detail_diversion'] = $this->Dinas_diversion_model->show_sql(false)->getDiversion($id);
        $this->data['discuss'] =  $this->Comment_model->getAll($id, Comment_model::DISKUSI,true);
        $this->data['log'] =  $this->Post_log_model->show_sql(false)->column("pl.*")->filter("where (is_public=".Post_log_model::PUBLIC_ALL." or is_public=".Post_log_model::PUBLIC_MASYARAKAT.") and post_id=".$id)->order("id", "desc")->getAll();
        if(!$this->data['discuss']){
            $this->data['discuss'] = array();
        }
        $this->data['comment'] =  $this->Comment_model->show_sql(false)->getAll($id, Comment_model::KOMENTAR,true);
        if(!$this->data['comment']){
            $this->data['comment'] = array();
        }
        $this->load->model('Unitkerja_model');
        $this->load->model('User_model');
        // $this->data['data'] = $data;
        $this->setResponse($this->setSystem(ResponseStatus::SUCCESS), $this->data);
    }

    public function detailInfo($id) {
        $this->load->model('Notification_model');
        $this->load->model('Notification_user_model');
        $this->Notification_user_model->setAsRead($this->user['id'], Notification_model::TYPE_POST_DETAIL, $id);

        // $post = $this->model->with(array('Category', 'Comment' => 'User', 'User', 'Post_user', 'Group'))->get_by('posts.id', $id);
        $post = $this->model->getInformasi($this->user['id'],$id);

        if (!empty($post)) {
            $this->load->model('Tracker_model');
            $this->Tracker_model->post($id, $this->user['id']);
        }

        $this->setResponse($this->setSystem(ResponseStatus::SUCCESS), $post);
    }

    public function detailKM($id) {
        $this->load->model('Notification_model');
        $this->load->model('Notification_user_model');
        $this->Notification_user_model->setAsRead($this->user['id'], Notification_model::TYPE_POST_DETAIL, $id);

        // $post = $this->model->with(array('Category', 'Comment' => 'User', 'User', 'Post_user', 'Group'))->get_by('posts.id', $id);
        $post = $this->model->getKnowledge($id,$this->user['id']);

        if (!empty($post)) {
            $this->load->model('Tracker_model');
            $this->Tracker_model->post($id, $this->user['id']);
        }

        $this->setResponse($this->setSystem(ResponseStatus::SUCCESS), $post);
    }

    public function delete($id) {
        // $post = $this->model->get_by('posts.id', $id);
        // if ($post['userId'] == $this->user['id']) {
            $delete = $this->model->delete($id);
            if ($delete === TRUE) {
                $this->load->model("Post_quota_model");

                $sql = "select id, user_id, group_id, status
                        from posts
                        where id = $id";
                $postData = dbGetRow($sql);

                if($this->Post_quota_model->incQuota($postData['user_id'], $postData['group_id'])){
                    $this->setResponse($this->setSystem(ResponseStatus::SUCCESS, 'Berhasil menghapus postingan'));
                }
            } else {
                $this->setResponse($this->setSystem(ResponseStatus::ERROR, "Gagal menghapus postingan"));
            }
        // } else {
            // $this->setResponse($this->setSystem(ResponseStatus::ERROR, "Tidak dapat menghapus postingan ini"));
        // }
    }

    public function getPostStat() {
        $stats = $this->model->getPostStat($this->user['id']);
        if ($stats) {
            $this->setResponse($this->setSystem(ResponseStatus::SUCCESS), $stats);
        } else {
            $this->setResponse($this->setSystem(ResponseStatus::ERROR, "Gagal akses"));
        }
    }

    function postSalinTiket() {
        $post_data = $this->input->post(null,true);
        $id = (int)$post_data['post_id'];
        $helpdesk = $post_data['group_id'];
        if ($helpdesk==NULL or $helpdesk=='')
            $this->setResponse($this->setSystem(ResponseStatus::ERROR, "Gagal menyalin tiket"));
        $post = $this->model->getPostBy('id',$id);
        $post_images = $this->model->getPostImage($id);
        $post_files = $this->model->getPostFile($id);
        $data = $post;
        $data['group_id']=$helpdesk;
        $data['status'] = 'B';
        $data['parent_id'] = $data['id'];
        unset($data['created_at']);
        unset($data['id']);
        unset($data['updated_at']);
        unset($data['kode']);
        $insert = $this->model->create($data, TRUE, TRUE);
        if ($insert) {
            foreach ($post_images as $key => $image) {
                $this->model->insertSalinPostImage($insert,$image['image']);
            }
            foreach ($post_files as $key => $file) {
                $this->model->insertSalinPostFile($insert,$file['file']);
            }
            $this->load->model('Comment_model', 'Comment');
            $this->load->model('Group_model');
            $this->User_model->setUserID($this->user['id']);
            $group = $this->Group_model->getGroupBy('id',$helpdesk);
            $data = array();
            $data['user_id'] = $this->user['id'];
            $data['post_id'] = $insert;
            $data['candelete'] = 0;
            $data['text'] = '<span style="color:ddd;font-size:11px">[menyalin tiket ke ' . $group['name'] . ']</span>';

            $comment_id = $this->Comment->create($data, TRUE, TRUE);
            $this->setResponse($this->setSystem(ResponseStatus::SUCCESS), 'Berhasil menyalin Tiket');
        }
        return;
    }

    function postPindahTiket() {
        $post_data = $this->input->post(null,true);
        $id = $post_data['post_id'];
        $helpdesk = $post_data['group_id'];
        if (!$helpdesk)
            $this->setResponse($this->setSystem(ResponseStatus::ERROR, "Gagal memindahkan tiket"));
        $post = $this->model->getPostBy('id',$id);
        $post_images = $this->model->getPostImage($id);
        $post_files = $this->model->getPostFile($id);
        $data = $post;
        $data['group_id']=$helpdesk;
        $data['status'] = 'B';
        $data['parent_id'] = $data['id'];
        unset($data['created_at']);
        unset($data['id']);
        unset($data['updated_at']);
        unset($data['kode']);
        $insert = $this->model->create($data, TRUE, TRUE);
        if ($insert) { 

            foreach ($post_images as $key => $image) {
                $this->model->insertPindahPostImage($insert,$image['image']);
            }
            foreach ($post_files as $key => $file) {
                $this->model->insertPindahPostFile($insert,$file['file']);
            }
            $this->load->model('Comment_model');
            $this->load->model('Group_model');
            $this->User_model->setUserID($this->user['id']);
            $group = $this->Group_model->getGroupBy('id',$helpdesk);
            $data = array();
            $data['userId'] = $this->user['id'];
            $data['postId'] = $insert;
            $data['candelete'] = 0;
            $data['text'] = '<span style="color:ddd;font-size:11px">[memindahkan tiket ke ' . $group['name'] . ']<span>';
            $comment_id = $this->Comment_model->create($data, TRUE, TRUE);
            $this->model->delete($id);
            $this->setResponse($this->setSystem(ResponseStatus::SUCCESS), 'Berhasil memindahkan Tiket');
        }
        return;
    }

    public function getDashboard() {
        $data = $this->postData;
        
        $posts = $this->model->getDashboardForAPI($this->user['id'],$data['helpdesk'],$this->page,$data['status'],$data['id_user'], 0, $data['showall'], $data['sort_by']);
        $this->setResponse($this->setSystem(ResponseStatus::SUCCESS), $posts);
    }

    public function getSticky() {
        $data = $this->postData;
        $posts = $this->model->getStickyByType($this->user['id'],$data['type']);
        $this->setResponse($this->setSystem(ResponseStatus::SUCCESS), $posts);
    }

}

?>