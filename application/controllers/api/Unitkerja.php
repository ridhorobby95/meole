<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Unitkerja extends PrivateApiController {

    public function listUnitkerja(){
        $data = $this->model->column('idunit, name')->show_sql(false)->getAll('where level=2');
        if($data){
            $this->data = $data;
            $this->setResponse($this->setSystem(ResponseStatus::SUCCESS, "Sukses"), $data);
        }else{
            $this->setResponse($this->setSystem(ResponseStatus::ERROR, 'Gagal'));
        }
        // echo "<pre>";print_r($data);die();
    }

}

?>