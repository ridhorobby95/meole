<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Comment extends PrivateApiController {

    public function coba(){
        $this->load->model('Comment_model');
        $this->load->model('Notification_model');
        $this->Notification_model->generate(Notification_model::ACTION_COMMENT_CREATE, 7394,$this->user['id']);
    }

    public function create() {
        // post data: text, type, post_id
        $data = $this->postData;
        $data['user_id'] = $this->user['id'];
		
        $insert = $this->model->create($data);
        if ($insert) {
            $this->db->trans_commit();
            $this->load->model('Notification_model');
            $this->Notification_model->generate(Notification_model::ACTION_COMMENT_CREATE, $insert,$this->user['id']);
            $this->setResponse($this->setSystem(ResponseStatus::SUCCESS, 'Berhasil membuat komentar'));
        }
        else {
            $validation = $this->model->getErrorValidate();
            if (empty($validation)) {
                $this->setResponse($this->setSystem(ResponseStatus::ERROR, 'Gagal membuat komentar'));
            } else {
                $this->setResponse($this->setSystem(ResponseStatus::VALIDATION_ERROR, NULL, $validation));
            }
        }
    }

    public function getComment(){
        $data = $this->postData;
        $this->data =  $this->Comment_model->show_sql(false)->getAll($data['post_id'], $data['type']);
        $this->setResponse($this->setSystem(ResponseStatus::SUCCESS), $this->data);
    }

    public function delete($id) {
        $comment = $this->model->get($id);
        if ($comment['userId'] == $this->user['id']) {
            $delete = $this->model->deletes($id);
            if ($delete === TRUE) {
                $this->load->model('Notification_model');
                $delete = $this->Notification_model->delete_by(array('reference_id' => $id, 'reference_type' => Notification_model::TYPE_POST_DETAIL));
                $this->setResponse($this->setSystem(ResponseStatus::SUCCESS, 'Berhasil menghapus komentar'));
            } else {
                $this->setResponse($this->setSystem(ResponseStatus::ERROR, "Gagal menghapus komentar"));
            }
        } else {
            $this->setResponse($this->setSystem(ResponseStatus::ERROR, "Tidak dapat menghapus komentar ini"));
        }
    }

    public function post($id) {
        $comments = $this->model->with('User')->get_many_by(array('post_id' => $id));

        $this->setResponse($this->setSystem(ResponseStatus::SUCCESS), $comments);
    }

    public function sendImage($folder='posts/temp2') {
        $folder_ori = $folder;

        if ($_FILES) {
            $images_arr = array();

            $id = md5($this->user['id'] . $this->config->item('encryption_key'));

            $folder .= '/' . $id;
            $ciConfig = $this->config->item('utils');
            $path = $ciConfig['full_upload_dir'] . $folder . '/';
            if (!is_dir($path)) {
               mkdir($path);         
            }
            
            $name = $_FILES['gambar']['name'];

            $file_name = Image::getFileName($id, Image::IMAGE_ORIGINAL, $name);
            unlink($path . $file_name);
            
            $file_mime = Image::getMime($folder.$file_name);
            $image_mime = explode('/',$file_mime);
            if ($image_mime[0]=='image') {
                Image::upload('gambar', $id, $name, $folder);

                $link = Image::generateLink($id, $name, $folder);
                $images_arr[] = $link['thumb'];
            }  
            // kalau mau responsenya list dari 5 file image yang diconvert 
            $this->setResponse($this->setSystem(ResponseStatus::SUCCESS, ""));
        } 
    }

    public function sendFile($folder='posts/temp2') {
        $folder_ori = $folder;

        if ($_FILES) {
            $files_arr = array();

            $id = md5($this->user['id'] . $this->config->item('encryption_key'));

            $folder .= '/' . $id;
            $ciConfig = $this->config->item('utils');
            $path = $ciConfig['full_upload_dir'] . $folder . '/';
            if (!is_dir($path)) {
               mkdir($path);         
            }

            $name = $_FILES['file_upload']['name'];
            $file_name = File::getFileName($id, $name);
            unlink($path . $file_name);

            File::uploadFile('file_upload', $id, $name, $folder);
            $link = File::generateLink($id, $name, $folder);
            // $files_arr = $link;

            $this->setResponse($this->setSystem(ResponseStatus::SUCCESS, ""));
        }    
    }

    function _moveFileTemp($post_id, $folder_source='posts/temp2') {
        $id = md5($this->user['id']. $this->config->item('encryption_key'));
        $folder_source .= '/'.$id;
        $ciConfig = $this->config->item('utils');
        $path_source = $ciConfig['full_upload_dir'] . $folder_source . '/';
        if (is_dir($path_source)) {
            $files = glob($path_source.'*'); 
            $arr_image = array();
            $arr_files = array();
            foreach ($files as $file) {
                list($mime,$ext) = explode('/',Image::getMime($file));
                $basename = basename($file);
                if ($mime=='image') {
                    $folder_dest = 'posts/photos';
                    $path_dest = $ciConfig['full_upload_dir'] . $folder_dest . '/';
                    $image = Image::getName($basename);
                    if (!in_array($image, $arr_image)) {
                        $arr_image[] = $image;
                        if (!$this->model->insertPostImage($post_id, $image, $file)) {
                            return false;
                        }

                    }
                } else {
                    $folder_dest = 'posts/files';
                    $path_dest = $ciConfig['full_upload_dir'] . $folder_dest . '/';
                    $fil = File::getName($basename);
                    if (!in_array($fil, $arr_files)) {
                        $arr_files[] = $fil;

                        if (!$this->model->insertPostFile($post_id, $fil, $file)) {
                            return false;
                        }
                    }
                }
                

                if (!rename($file, $path_dest . '/' . $basename)) {
                    return false;
                }
            }
        }
        else {
            return true;
        }

        return $this->_deleteFiles($path_source);
    }

    function _deleteFiles($path){
        if (is_dir($path) === true) {
            $files = array_diff(scandir($path), array('.', '..'));
            foreach ($files as $file) {
                $this->_deleteFiles(realpath($path) . '/' . $file);
            }
            return rmdir($path);
        }

        else if (is_file($path) === true) {
            return unlink($path);
        }

        return false;
    }

}

?>