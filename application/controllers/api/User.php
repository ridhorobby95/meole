<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class User extends PrivateApiController {

    /**
     * Digunakan untuk mengambil list user
     */
    public function index() {
        $user = $this->model->get_all();

        $this->setResponse($this->setSystem(ResponseStatus::SUCCESS), $user);
    }

    /**
     * Digunakan untuk mengambil detail user dengan ID user
     */
    public function detail($id) {
        $user = $this->model->get($id);

        //SET RESPONSE
        $this->setResponse($this->setSystem(ResponseStatus::SUCCESS), $user);
    }

    /**
     * Digunakan untuk mengambil profil user dan role user yang sedang login
     */
    public function me() {
        $this->setResponse($this->setSystem(ResponseStatus::SUCCESS), $this->user);
    }

    /**
     * Digunakan untuk update profil user
     */
    public function update() {
        $data = $this->postData;
        if (isset($_FILES['photo'])) {
            $data['photo'] = $_FILES['photo']['name'];
        }

        if(isset($data['password'])){
            $user = dbGetRow("select password, password_salt from users where id=".$this->user['id']);
            // echo "<pre>";print_r($user);die();
            $check = SecurityManager::validate($data['password_lama'], $user['password'], $user['password_salt']);
            if(!$check){
                $this->setResponse($this->setSystem(ResponseStatus::ERROR, 'Password lama salah'));
            }
            $auth_key = SecurityManager::generateAuthKey();
            $data['password']      = SecurityManager::hashPassword($data['password'], $auth_key);
            $data['password_salt'] = $auth_key;
        }
        $update = $this->model->save($this->user['id'], $data);
        if ($update === TRUE) {
            $newData = $this->model->get($this->user['id']);
            $this->setResponse($this->setSystem(ResponseStatus::SUCCESS, 'Berhasil mengubah profil'), $newData);
        } elseif (is_string($update)) {
            $this->setResponse($this->setSystem(ResponseStatus::ERROR, $update));
        } else {
            $validation = $this->model->getErrorValidate();
            if (empty($validation)) {
                $this->setResponse($this->setSystem(ResponseStatus::ERROR, 'Gagal mengubah profil ' . $this->user['name']));
            } else {
                $this->setResponse($this->setSystem(ResponseStatus::VALIDATION_ERROR, NULL, $validation));
            }
        }
    }

    public function create() {
        $data = $this->postData;
        $data['photo'] = isset($_FILES['photo']) ? $_FILES['photo']['name'] : NULL;
        $insert = $this->model->create($data);
        if ($insert === TRUE) {
            $this->setResponse($this->setSystem(ResponseStatus::SUCCESS, NULL, array('Berhasil membuat user')));
        } elseif (is_string($insert)) {
            $this->setResponse($this->setSystem(ResponseStatus::ERROR, $insert));
        } else {
            $validation = $this->model->getErrorValidate();
            if (empty($validation)) {
                $this->setResponse($this->setSystem(ResponseStatus::ERROR, 'Gagal membuat user'));
            } else {
                $this->setResponse($this->setSystem(ResponseStatus::VALIDATION_ERROR, NULL, array(), $validation));
            }
        }
    }

    /**
     * Digunakan untuk mengganti password user
     */
    public function change_password() {
        $this->form_validation->set_rules('oldPassword', 'Password Lama', 'required');
        $this->form_validation->set_rules('newPassword', 'Password Baru', 'required|min_length[6]');
        $this->form_validation->set_rules('confirmPassword', 'Konfirmasi Password', 'required|min_length[6]|matches[newPassword]');
        $user = $this->model->is_private(TRUE)->get($this->user['id']);
        if ($user['password'] == SecurityManager::hashPassword($this->postData['oldPassword'], $user['passwordSalt'])) {
            if ($this->form_validation->run() == TRUE) {
                $update = $this->model->update($this->user['id'], SecurityManager::encode($this->postData['newPassword']), TRUE);
                if ($update) {
                    $this->setResponse($this->setSystem(ResponseStatus::SUCCESS, 'Berhasil mengganti password'));
                } else {
                    $this->setResponse($this->setSystem(ResponseStatus::ERROR, 'Gagal mengganti password'));
                }
            } else {
                $validation = $this->model->getErrorManualValidation(array('oldPassword', 'newPassword', 'confirmPassword'));
                $this->setResponse($this->setSystem(ResponseStatus::VALIDATION_ERROR, NULL, array(), $validation));
            }
        } else {
            $this->setResponse($this->setSystem(ResponseStatus::ERROR, 'Password lama tidak sesuai'));
        }
    }

    /**
     * Digunakan untuk mereset password (kirim notifikasi ke email)
     */
    public function forgot_password() {
        
    }

    public function getUser() {
        $users = $this->model->getUser($this->user['id'], true);
        $this->setResponse($this->setSystem(ResponseStatus::SUCCESS), $users);
    }

    public function list_me() {
        $users = $this->model->getListFor($this->user['id']);

        $this->setResponse($this->setSystem(ResponseStatus::SUCCESS), $users);
    }

    public function logout() {
        $logout = AuthManager::logout($this->token);
        if ($logout === TRUE) {
            $this->setResponse($this->setSystem(ResponseStatus::SUCCESS, 'Logout berhasil'));
        } else {
            $this->setResponse($this->setSystem(ResponseStatus::ERROR, $logout));
        }
    }

    public function getListStaff() {
        $staff = $this->model->getListStaff($this->user['id']);
        $this->setResponse($this->setSystem(ResponseStatus::SUCCESS), $staff);
    }

    public function getListStaffKopertis() {
        $staff = $this->model->getListStaffKopertis();
        $this->setResponse($this->setSystem(ResponseStatus::SUCCESS), $staff);
    }

    public function savePP() {
        $data = $this->postData;
        $user = $this->model->save($this->user['id'],$data);
        if ($user===true) {
            $this->setResponse($this->setSystem(ResponseStatus::SUCCESS, "Berhasil mengganti PP"));
        } else {
            $this->setResponse($this->setSystem(ResponseStatus::SUCCESS, "Gagal mengganti PP"));
        }
    }

    public function getUserOperator() {
        $data = $this->postData;
        $user = $this->model->getUsersForAPI($this->user['id'], $data['nama']);
        $this->setResponse($this->setSystem(ResponseStatus::SUCCESS),$user);
    }

    // public function tes(){
    //     $data = $this->model->get($this->user['id']);
    //     $this->setResponse($this->setSystem(ResponseStatus::SUCCESS, "Berhasil mengganti PP"), $data);
    //     // echo "<pre>";print_r($data);die();
    // }

    public function changepassword(){
        $auth_key = SecurityManager::generateAuthKey();

        $data = array(
                'password'      => SecurityManager::hashPassword('testing', $auth_key),
                'password_salt' => $auth_key
            );
        dbUpdate("users", $data, "id=16");
    }


}

?>