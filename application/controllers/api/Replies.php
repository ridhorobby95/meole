<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Replies extends PrivateApiController {

    public function getListReply() {
        $data = $this->postData; // keyword, helpdesk, token
        $replies = $this->model->filter(" reply like '%".$data['text']."%' or name like '%".$data['text']."%' ")->getList($this->page,$data['helpdesk'],0);
        $this->setResponse($this->setSystem(ResponseStatus::SUCCESS), $replies);
    }

    public function delete() {
        $data = $this->postData;
        $delete = $this->model->delete($data['id']);
        if ($delete === TRUE) {
            $this->setResponse($this->setSystem(ResponseStatus::SUCCESS, 'Berhasil menghapus template'));
        } else {
            $this->setResponse($this->setSystem(ResponseStatus::ERROR, "Gagal menghapus template"));
        }
    }

    public function create() {
        $data = $this->postData;
        $create = $this->model->create($data);
        if ($create === TRUE) {
            $this->setResponse($this->setSystem(ResponseStatus::SUCCESS, 'Berhasil membuat template'));
        } else {
            $this->setResponse($this->setSystem(ResponseStatus::ERROR, "Gagal membuat template"));
        }
    }

    public function update() {
        $data = $this->postData;
        $update = $this->model->update($data['id'],$data);
        if ($update === TRUE) {
            $this->setResponse($this->setSystem(ResponseStatus::SUCCESS, 'Berhasil update template'));
        } else {
            $this->setResponse($this->setSystem(ResponseStatus::ERROR, "Gagal update template"));
        }
    }

}

?>