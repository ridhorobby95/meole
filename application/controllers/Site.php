<?php
    defined('BASEPATH') OR exit('No direct script access allowed');
    
 
    class Site extends WEB_Controller {
		protected $ispublic = true;
		
        /**
		 * Halaman login user
		 */
        public function index() {
			// jika sudah login lempar ke post
			if(SessionManagerWeb::isAuthenticated()) {
				SessionManagerWeb::setFlashMsg(false,'Anda sudah login');
				redirect($this->getCTL('post?type=M'));
			}
			
			// lakukan validasi form
            $this->form_validation->set_rules('username','Username','required');
			$this->form_validation->set_rules('password','Password','required');
			
			if ($this->form_validation->run() === true) {
				list($ok,$msg,$user) = AuthManagerWeb::login($this->input->post('username'),$this->input->post('password'));
				if($ok) {
					// simpan cookie
					setcookie('username',$user['username'],time() + 2592000,'/'); // 30 hari
					SessionManagerWeb::setUser($user);
					redirect($this->getCTL('post?type=M'));
				}
				else {
					SessionManagerWeb::setFlash(array('errmsg' => array($msg)));
					redirect($this->ctl);
				}
			}
			else {
				$errstr = validation_errors();
				if(!empty($errstr)) {
					SessionManagerWeb::setFlash(array('errmsg' => array($errstr)));
					redirect($this->ctl);
				}
			}
			/*
			// load semua user
			$this->load->model('User_model');
			
			$users = $this->User_model->order_by('name')->get_many_by(array('status' => Status::ACTIVE));
			$userck = $_COOKIE['username'];
			
			foreach($users as $user) {
				if($user['username'] == $userck) {
					$this->data['user'] = $user;
					break;
				}
			}
			
			$this->data['data'] = $users;
			*/
			$this->data['title'] = 'MEOLE';
			$this->load->view(static::VIEW_PATH.$this->view,$this->data);
        }

    }