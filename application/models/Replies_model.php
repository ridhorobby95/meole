<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Replies_model extends AppModel {

    public $limit = 10;
    private $table = 'post_replies';
    private $filter = '';
    protected $_filter = '';

    protected $label = array(
        'id' => 'ID',
        'name' => 'Nama',
        'text' => 'Teks',
    );
    protected $validation = array(
        'name' => 'required',
        'teks' => 'required',
    );

    function getList($page=1, $user_id=NULL, $test=0) {
        $page = $this->limit*($page-1);
        $and_or_where = " where ";
        if (isset($this->filter) and $this->filter!=NULL and $this->filter!=""){
            $and_or_where = " and ";
        }
        if ($user_id!=NULL and $user_id!="") {
            $filter_helpdesk = " $and_or_where user_id=$user_id ";
            $and_or_where = " and ";
        }
        if ($this->_filter!=NULL and $this->_filter!="") {
            $filter_helpdesk .= $and_or_where . $this->_filter;
        }
        
        // $sql = "select * from $this->table {$this->filter} $filter_helpdesk order by name, id asc limit $this->limit offset $page";
        $sql = "select * from $this->table {$this->filter} $filter_helpdesk order by name, id asc limit $this->limit offset $page";
        return dbGetRows($sql);
    }

    function getCount($chosen_helpdesk = NULL) {
        if ($chosen_helpdesk!=NULL and $chosen_helpdesk!="") {
            $helpdesk = $this->Group_model->getGroupBy('name',$chosen_helpdesk);
            $helpdesk_id = $helpdesk['id'];
            $filter_helpdesk = " where helpdesk_id=$helpdesk_id ";
        }
        $sql = "select count(*) from $this->table $filter_helpdesk {$this->filter}";
        return dbGetOne($sql);
    }

    function getRow($id) {
        $sql = "select * from $this->table where id=$id";
        return dbGetRow($sql);
    }

    function setFilter($filter) {
        $this->filter = $filter;
    }

    function filter($filter=''){
        $this->_filter = $filter;
        return $this;
    }

    function create($data){
        $record = array();
        $record['name'] = $data['name'];
        $record['reply'] = $data['reply'];
        $record['_id'] = $data['helpdesk_kirim'];
        return dbInsert('post_replies', $record);
    }

    function update($id,$data){
        $record = array();
        $record['name'] = $data['name'];
        $record['reply'] = $data['reply'];
        return dbUpdate('post_replies', $record,"id=$id");
    }

    function delete($id){
        $sql = "delete from post_replies where id=$id";
        return dbQuery($sql);
    }

}
