<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Client_model extends AppModel {
    
    protected $has_many = array(
        'Group'
    );
    protected $label = array(
        'id' => 'ID',
        'name' => 'Nama',
    );
    protected $validation = array(
        'name' => 'required',
    );
    
    protected $mapper = array(
        'name' => 'fullName'
    );

    protected function property($row) {
//        $row['image'] = Image::getImage($row['id'], $row['image'], 'groups/images');

        return $row;
    }

}
