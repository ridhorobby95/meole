<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class User_model extends AppModel {

    private $_is_role = FALSE;
    private $_is_private = FALSE;
    public $limit = 20;
    public $user_id='';
    private $table = 'users';
    protected $_filter = "";
    protected $_column = "";

    /**
     * Digunakan untuk menampilkan data yang akan dikembalikan dari model 
     * Jika semua ditampilkan tidak perlu ditambahkan attribute $soft_data pada model
     * 
     * @var array 
     */
    protected $soft_data = array('id', 'name', 'username', 'photo', 'role', 'id_organisasi', 'email', 'no_hp');
    private $public_data = array('id', 'name', 'username', 'photo', 'role', 'id_organisasi', 'email', 'no_hp');

    /**
     * Digunakan untuk relasi antar model
     * 
     * @var array 
     */
    protected $has_many = array(
        'Post',
        'Comment',
        'Group_member',
        'Post_user',
        'Notification',
    );

    /**
     * Digunakan untuk mengcustom nama mapper
     * Jika penamaan standard (user_name => userName) tidak perlu ditambahkan pada attribute $mapper pada model
     * 
     * @var array 
     */
    protected $mapper = array(
        'password_reset_token' => 'resetToken',
        'no_hp'     => 'no_hp'
    );

    /**
     * Digunakan untuk labeling jika label (user_name => User Name) hanya menyertakan nama fieldnya saja.
     * @var array 
     */
    protected $label = array();

    /**
     * Digunakan untuk validasi create dan update data
     * 
     * @var array 
     */
    protected $validation = array(
        //'username' => 'required|min_length[3]|max_length[50]',
        //'email' => 'required|max_length[50]|valid_email',
    );

    public function filter($filter){
        $this->_filter= $filter;
        return $this;
    }

    public function column($column){
        $this->_column = $column;
        return $this;
    }

    public function getUserID(){
        return $this->user_id;
    }

    public function setUserID($user_id){
        $this->user_id = $user_id;
        return $this->user_id;
    }

    public function is_role($role = FALSE) {
        $this->_is_role = $role;
        return $this;
    }

    public function is_private($private = FALSE) {
        $this->_is_private = $private;
        return $this;
    }

    public function getRole($role) {
        $roleUser = array(
            'id' => $role,
            'name' => Role::name($role)
        );
        return $roleUser;
    }

    public function mapper($row) {
        $newRow = array();
        foreach ($row as $key => $val) {
            //MAPPER ATRIBUTE
            if (is_null($this->soft_data) || in_array($key, $this->soft_data) || $this->show_all || $this->_is_private) {
                $newKey = key_exists($key, $this->mapper) ? $this->mapper[$key] : camelize($key);
                $newRow[$newKey] = $val;
            }
        }
        return $newRow;
    }

    protected function property($row) {
        if ($this->_is_role || $this->_is_private) {
            $row['role'] = $this->getRole($row['role']);
        } else {
            foreach ($row as $key => $val) {
                if (!in_array($key, $this->public_data) && !$this->show_all) {
                    unset($row[$key]);
                }
            }
        }
        if ( preg_match('/\s/',$row['photo']) ){
            $row['photo'] = str_replace(' ', '_', $row['photo']);
        }
        if (isset($row['photo'])) {
            $row['photo'] = Image::getImage($row['id'], $row['photo'], 'users/photos');
        }
        return $row;
    }

    public function create($data, $skip_validation = FALSE, $return = TRUE) {
        $data['password_salt'] = $auth_key = SecurityManager::generateAuthKey();
        $data['password'] = SecurityManager::hashPassword($data['password'], $auth_key);
        $data['status'] = Status::ACTIVE;
        $data['role'] = Role::STAFF;

        $this->_database->trans_begin();
        $create = parent::create($data, $skip_validation, $return);
        if ($create) {
            $upload = TRUE;
            if (isset($_FILES['photo']) && !empty($_FILES['photo'])) {
                $upload = Image::upload('photo', $create, $_FILES['photo']['name'], 'users/photos');
            }
            if ($upload === TRUE) {
                return $this->_database->trans_commit();
            } else {
                $this->_database->trans_rollback();
                return $upload;
            }
        }
        $this->_database->trans_rollback();
        return FALSE;
    }

    public function save($primary_value, $data = array(), $skip_validation = FALSE) {
        $this->_database->trans_begin();
        $update = parent::save($primary_value, $data, $skip_validation);
        if ($update) {
            $upload = TRUE;
            if (isset($_FILES['photo']) && !empty($_FILES['photo'])) {
                $upload = Image::upload('photo', $primary_value, $_FILES['photo']['name'], 'users/photos');
            }
            if ($upload === TRUE) {
                return $this->_database->trans_commit();
            } else {
                $this->_database->trans_rollback();
                return $upload;
            }
        }
        $this->_database->trans_rollback();
        return FALSE;
    }

    public function getListFor($userId) {
        return $this->order_by('name', 'ASC')->get_many_by(array('id !=' => $userId));
    }

    function getCount($role=NULL,$param=NULL) {
        if ($role!=NULL) {
            $filter = " where role='$role' ";
            if (!empty($param)) {
                $filter2 = " and (name ilike '%" . $param . "%' or username ilike '%" . $param . "%' or nm_lemb ilike '%" . $param . "%')";
            }
        } else {
            if (!empty($param)) {
                $filter2 .= " where (name ilike '%" . $param . "%' or username ilike '%" . $param . "%' or nm_lemb ilike '%" . $param . "%')";
            }
        }
        $sql = "select count(*) from users u 
        left join organisasi o on o.id_organisasi=u.id_organisasi $filter $filter2";
        return dbGetOne($sql);
    }

    function getUserNonMemberGroup($id) {
        $this->load->model('Group_model');
        $group = $this->Group_model->getRow($id);
        if ($group['type']=='H'){
            $filter = " and role in ('A','S','R') ";
            $sql = "select * from users where not exists 
                    (select 1 from group_members left join groups g on g.id=group_members.group_id where users.id = group_members.user_id and g.type='H') $filter";
        }else {
            $sql = "select * from users where not exists (select 1 from group_members where users.id = group_members.user_id and group_members.group_id = $id) $filter";
        }
        
        
        // echo nl2br($sql);
        // die();
        return dbGetRows($sql);
    }

    function getUsers($user_id,$page=0,$role=NULL,$param=NULL) {
        $offset = $page*$this->limit;

    /*    if (!self::isSuperAdministrator($user_id)){
            $my_helpdesk_group = $this->Group_member_model->getMyHelpdeskGroup($user_id);
            $my_helpdesk_group_id=$my_helpdesk_group['id'];
            $filter_my_helpdesk = " and (gm.group_id is not null or u.role='O') ";
            $join_group_members = " left join group_members gm on gm.user_id=u.id and gm.group_id= $my_helpdesk_group_id ";

        }
    */    
        if ($role!=NULL) {
            $filter = " and u.role='$role' ";
            if (!empty($param)) {
                $filter2 = " and (u.name ilike '%" . $param . "%' or username ilike '%" . $param . "%' or uk.name ilike '%" . $param . "%')";
            }
        } else {
            if (!empty($param)) {
                $filter2 .= " and (u.name ilike '%" . $param . "%' or username ilike '%" . $param . "%' or uk.name ilike '%" . $param . "%')";
            }
        }

        $sql = "select u.*, uk.name as nm_lemb
            from users u 
                left join unitkerja uk on u.id_dinas = uk.iddinas and level = 2
            where u.id is not null $filter $filter2
            order by u.name asc limit $this->limit offset $offset";
        $users = dbGetRows($sql);

        $offset=($page+1)*$this->limit;
        $sql = "select u.*, uk.name as nm_lemb
            from users u 
                left join unitkerja uk on u.id_dinas = uk.iddinas and level = 2
            where u.id is not null $filter $filter2
            order by u.name asc limit $this->limit offset $offset";
        $data = dbGetRows($sql);
    /*    
        $sql = "select u.*, o.nm_lemb from users u 
        left join organisasi o on o.id_organisasi=u.id_organisasi 
        $join_group_members
        where u.id is not null $filter_my_helpdesk $filter $filter2 order by u.name asc limit $this->limit  offset $offset";
        $users = dbGetRows($sql);
        // die($sql);
        $offset=($page+1)*$this->limit;
        $sql = "select u.*, o.nm_lemb from users u 
        left join organisasi o on o.id_organisasi=u.id_organisasi 
        $join_group_members
        where u.id is not null $filter_my_helpdesk $filter $filter2 order by u.name asc limit $this->limit  offset $offset";
        
        $data = dbGetRows($sql);
    */
        foreach ($users as $key => $value) {
            $users[$key]['user_photo'] = $this->getUserPhoto($value['id'],$value['photo']);
            // if ($this->isSuperAdministrator($value['id'])){
            //     $users[$key]['helpdesk_name'] = "Semua Helpdesk";
            // }
            /*
            if ($this->isStaff($value['id']) and !$this->isSuperAdministrator($value['id'])) {
                $helpdesk = $this->Group_member_model->getMyHelpdeskGroup($value['id']);
                $users[$key]['helpdesk_name'] = $helpdesk['name'];
            */
                // if ($helpdesk['name']==NULL) {
                //     $users[$key]['helpdesk_name']="Belum Masuk Helpdesk";
                // } else {
                //     $users[$key]['helpdesk_name'] = 'Helpdesk '.$helpdesk['helpdesk_name'];
                // }
            // }
            
        }
        if (empty($data)) {
            $users[0]['user_offset']=0;
        } else {
            $users[0]['user_offset']=1;
        }
        // echo "<pre>";print_r($users);die();
        return $users;
    }
    
    function getUser($user_id, $show_other_attributes=false) {
        $sql = "select u.*, uk.name as nm_lemb
            from users u 
                left join unitkerja uk on u.id_dinas = uk.iddinas and level = 2
            where u.id='$user_id'";
        $user = dbGetRow($sql);
        /*
        $sql = "select u.*, o.nm_lemb from users u 
                left join organisasi o on o.id_organisasi=u.id_organisasi
                where u.id='$user_id'";
        $user = dbGetRow($sql);
        */
        if ($show_other_attributes) {
            /*
            $sql = "select u.*, o1.nm_lemb, o2.nm_lemb as nm_lemb_induk, p.nm_peran 
                    from user_peran u 
                    join peran p on u.id_peran=p.id_peran
                    left join organisasi o1 on o1.id_organisasi=u.id_organisasi
                    left join organisasi o2 on o2.id_organisasi=o1.id_organisasi_induk 
                    where user_id='$user_id' order by nm_lemb_induk, nm_lemb";
            $user['list_peran'] = dbGetRows($sql);
             */
            if ($this->isOperator($user['id'])){
                $sql = "select pa.user_id
                    , count(p.id) as jml
                    , count(case when p.status = 'L' then p.id end) as lapor
                    , count(case when p.status = 'A' then p.id end) as alih
                    , count(case when p.status = 'R' then p.id end) as respon
                    , count(case when p.status = 'P' then p.id end) as proses
                    , count(case when p.status = 'S' then p.id end) as selesai
                    , count(case when p.status = 'I' then p.id end) as arsip
                from posts as p
                    join post_assign as pa on pa.post_id=p.id 
                where pa.user_id = $user_id
                group by pa.user_id";
                $user['ticket'] = dbGetRow($sql);
            }
            else if ($this->isWarga($user['id'])){
                $sql ="select user_id, count(id) as jml from posts where user_id = $user_id group by user_id";
                $user['ticket'] = dbGetRow($sql);
            }
        }
        if ($this->isSuperAdministrator($user['id'])){
            $user['helpdesk_name'] = "Semua Helpdesk";
        }
        if ($this->isStaff($user['id']) and !$this->isSuperAdministrator($user['id'])) {
            $helpdesk = $this->Group_member_model->getMyHelpdeskGroup($user['id']);
            
            if ($helpdesk['name']==NULL) {
                $user['helpdesk_name']="Belum Masuk Helpdesk";
            } else {
                $user['helpdesk_name'] = 'Helpdesk '.$helpdesk['name'];
            }
        }

        return $user; 
    }   

    function getUserPhoto($user_id, $photo){
        if ($photo!=NULL) {
            $file = Image::getLocation($user_id, Image::IMAGE_ORIGINAL, $photo, 'users/photos');
            $arr_photo = explode('.',$photo);
            $ext = $arr_photo[count($arr_photo)-1];
            // $file .='.'.$ext;
            $user_photo = 'assets/uploads/users/photos/'.Image::getFileName($user_id, Image::IMAGE_ORIGINAL, $photo);
            // $user_photo .='.'.$ext;
            if (!file_exists($file) || is_dir($file)){
                $file = Image::getLocation($user_id, Image::IMAGE_ORIGINAL, $photo, 'users/photos');
                $file .='.'.$ext;
                $user_photo = 'assets/uploads/users/photos/'.Image::getFileName($user_id, Image::IMAGE_ORIGINAL, $photo);
                if (!file_exists($file) || is_dir($file)){
                    $user_photo = $config['full_upload_dir'] . 'assets/uploads/users/photos/nopic.png';
                }
            }
            
        } else {
            $user_photo = $config['full_upload_dir'] . 'assets/uploads/users/photos/nopic.png';
        }
        return $user_photo;
    }

    public function isAdministrator($user_id){
        $sql = "select 1 from users u where id='$user_id' and role in ('A','R')";
        if (dbGetOne($sql))
            return true;
        return false;
    }

    public function isSuperAdministrator($user_id){
        $sql = "select 1 from users u where id='$user_id' and role='R'";
        if (dbGetOne($sql))
            return true;
        return false;
    }

    public function isStaff($user_id){
        $sql = "select 1 from users u where id='$user_id' and role in ('A','S', 'R')";
        if (dbGetOne($sql))
            return true;
        return false;
    }

    public function isOperator($user_id){
        $sql = "select 1 from users u where id='$user_id' and role='O'";
        if (dbGetOne($sql))
            return true;
        return false;
    }
    public function isWarga($user_id){
        $sql = "select 1 from users u where id='$user_id' and role='W'";
        if (dbGetOne($sql))
            return true;
        return false;
    }

    function getListOperator($id_dinas) {
        $sql = "select id, name from users where role='".Role::OPERATOR."' and id_dinas=".$id_dinas;
        $data = dbGetRows($sql);
        if($data){
            return $data;
        }else{
            return null;
        }
    }


    function updateUserFromPersuratan($user) {

        //users
        $id_user_persuratan = $user['iduser'];
        $sql = "select id from users where id_user_persuratan=".$id_user_persuratan;
        $update_jabatan = false;
        $get = dbGetOne($sql);
        if ($get) {
            $record = array();
            $record['name'] = $user['nm_pengguna'];
            $record['username'] = $user['username'];
            $record['updated_at'] = date("Y-m-d H:i:s");
            $update = dbUpdate('users', $record, "id='$sql'");
            $id = $get;
            $update_jabatan = true;
        }
        else {
            $record = array(
                    'name'              => $user['nama'],
                    'username'          => $user['username'],
                    'password'          => $user['password'],
                    'password_salt'     => $user['salt'],
                    'is_active'         => 1,
                    'role'              => Role::OPERATOR,
                    'updated_at'        => date("Y-m-d H:i:s"),
                    'is_excluded'       => 0,
                    'email'             => $user['email'],
                    'no_hp'             => $user['nohp'],
                    'id_user_persuratan'=> $id_user_persuratan,
                    'nip'               => $user['nip']
            );
            $insert = dbInsert('users', $record);
            if($insert){
                $update_jabatan=true;
                $id = dbGetOne( "select id from users where id_user_persuratan=".$id_user_persuratan);
            }
        }

        if($update_jabatan){
            $jabatan_exist = dbGetOne("select 1 from user_jabatan where user_id=".$id." and jabatan_id=".$user['idjabatan']);
            if(!$jabatan_exist){
                dbInsert('user_jabatan', array('user_id' => $id, 'jabatan_id' => $user['idjabatan']));
            }
        }
        //jabatan
        
    }

    function updateRoleFromDikti($user_id, $roles) {
        $periodik_update = true;
        $sql = "delete from user_peran where user_id='$user_id'";

        dbQuery($sql);
        foreach ($roles as $role) {
            $id_peran = $role['id_peran'];
            $nm_peran = $role['nm_peran'];
            $id_organisasi = $role['id_organisasi'];
            $nm_lemb = $role['nm_lemb'];
            $id_organisasi_induk = $role['id_organisasi_induk'];
            $nm_lemb_induk = $role['nm_lemb_induk'];

            $sql = "select 1 from peran where id_peran=$id_peran";
            if (dbGetOne($sql)) {
                if ($periodik_update) {
                    $record = array();
                    $record['nm_peran'] = $nm_peran;
                    dbUpdate('peran', $record, "id_peran='$id_peran'");
                }
            }
            else {
                $record = array();
                $record['id_peran'] = $id_peran;
                $record['nm_peran'] = $nm_peran;
                dbInsert('peran', $record);
            }

            # 1021 Admin Helpdesk
            # 1022 Staff Helpdesk
            # 1023 Super Admin Helpdesk

            if ($id_peran == '1021') {
                $is_administrator = 1;
            }

            if ($id_peran == '1022') {
                $is_staff = 1;
            }

            if ($id_peran == '1023'){
                $is_super_administrator = 1;
            }

            if ($id_peran == '3') {
                $is_admin_pt = 1;
                $id_organisasi_admin_pt = $id_organisasi;
            }

            if ($id_peran == '6') {
                $is_admin_prodi = 1;
                $id_organisasi_admin_prodi = $id_organisasi_induk;
            }
 
            $record = array();
            $record['user_id'] = $user_id;
            $record['id_peran'] = $id_peran;
            $record['id_organisasi'] = $id_organisasi;
            dbInsert('user_peran', $record);

            $sql = "select 1 from organisasi where id_organisasi='$id_organisasi'";
            if (dbGetOne($sql)) {
                if ($periodik_update) {
                    $record = array();
                    $record['nm_lemb'] = $nm_lemb;
                    $record['id_organisasi_induk'] = $id_organisasi_induk;
                    dbUpdate('organisasi', $record, "id_organisasi='$id_organisasi'");
                }
            }
            else {
                $record = array();
                $record['id_organisasi'] = $id_organisasi;
                $record['nm_lemb'] = $nm_lemb;
                $record['id_organisasi_induk'] = $id_organisasi_induk;
                dbInsert('organisasi', $record);
            }

            
            # induk organisasi
            $sql = "select 1 from organisasi where id_organisasi='$id_organisasi_induk'";
            if (dbGetOne($sql)) {
                if ($periodik_update) {
                    $record = array();
                    $record['nm_lemb'] = $nm_lemb_induk;
                    dbUpdate('organisasi', $record, "id_organisasi='$id_organisasi_induk'");
                }
            }
            else {
                $record = array();
                $record['id_organisasi'] = $id_organisasi_induk;
                $record['nm_lemb'] = $nm_lemb_induk;
                dbInsert('organisasi', $record);
            }

        }

        // $sql = "select role, id_organisasi from users where id='$user_id'";
        // $user = dbGetRow($sql);
        // if (!$user['role']) {
            $role = 'O';
            if ($is_staff)
                $role = 'S';            
            if ($is_administrator)
                $role = 'A';
            if ($is_super_administrator)
                $role = 'R';
            $sql = "update users set role='$role', id_organisasi='$id_organisasi'  where id='$user_id'";
            dbQuery($sql);
        // }
        //if (!$user['id_organisasi']) {
            if ($is_admin_pt) {
                $sql = "update users set id_organisasi='$id_organisasi_admin_pt' where id='$user_id'";
            }
            else {
                if ($is_admin_prodi) {
                    $sql = "update users set id_organisasi='$id_organisasi_admin_prodi' where id='$user_id'";
                }
            }
            
            dbQuery($sql);
        //}
    }

    function updateUserRole($id, $role) {
        $record = array();
        $record['role'] = $role;
        return dbUpdate('users', $record, "id='$id'");
    }

    function updateUserExclude($id, $exclude) {
        $record = array();
        $record['is_excluded'] = $exclude;
        if ($exclude==NULL || $exclude==0){
            $record['is_excluded']=1;
        } else {
            $record['is_excluded']=0;
        }
        return dbUpdate('users', $record, "id='$id'");
    }

    //untuk ban user
    // function updateUserBanned($id, $is_banned) {
    //     $record = array();
    //     $record['is_banned'] = $is_banned;
    //     if ($is_banned==NULL || $is_banned==0){
    //         $record['is_banned']=1;
    //     } else {
    //         $record['is_banned']=0;
    //     }
    //     return dbUpdate('users', $record, "id='$id'");
    // }

    function getStaffFromForlap() {
        $sql = "select p.*, rp.id_peran, r.nm_peran, convert(nvarchar(36), rp.id_organisasi) as id_organisasi, o.nm_lemb 
                from man_akses.pengguna p 
                join man_akses.role_pengguna rp on p.id_pengguna=rp.id_pengguna
                join man_akses.peran r on rp.id_peran=r.id_peran 
                join man_akses.unit_organisasi o on rp.id_organisasi=o.id_organisasi 
                where rp.id_peran in (1021, 1022,1023) and p.soft_delete=0 and rp.soft_delete=0";
        $users = dbGetRow($sql);

    }

    function getUserBeban($page=0){
        $sql = "select id, name, sum(bobot) as bobot, sum(B) as B, sum(P) as P, sum(S) as S 
            from (select t.id, u.name, 
                sum(case when t.status='B' then 1 when t.status='P' then 1 when t.status='S' then 0.1 when t.status is null then 0 end) as bobot, 0 as B, 0 as P, 0 as S 
                from (select u.id, p.status from (select id from users where role in ('A', 'S') and (is_excluded is NULL or is_excluded=0)) u 
                left join post_assign a on u.id=a.user_id
                left join posts p on a.post_id=p.id and p.status in ('B', 'P', 'S')) t
            join users u on u.id=t.id
            group by t.id, u.name
        union
            select t.id, u.name,0 as bobot, sum(case when t.status='B' then 1 end) as B, sum(case when t.status='P' then 1 end) as P, sum(case when t.status='S' then 1 end) as S
            from (select u.id, p.status from (select id from users where role in ('A', 'S') and (is_excluded is NULL or is_excluded=0)) u 
            left join post_assign a on u.id=a.user_id
            left join posts p on a.post_id=p.id and p.status in ('B', 'P', 'S')) t
            join users u on u.id=t.id group by t.id, u.name) s
            group by id, name
            order by bobot, name";
        $data = dbGetRows($sql);
        return $data;
    }

    function getUserStatandBeban($page=0){
        //bobot + response time + processing time + solving time
        $sql = "select s.id, name, sum(bobot) as bobot, sum(B) as B, sum(P) as P, sum(S) as S,round(avg(r.response_time)) as response_time, round(avg(r.processing_time)) as processing_time, 
        round(avg(r.solving_time)) as solving_time 
    from (
        select t.id, u.name, sum(case when t.status='B' then 1 when t.status='P' then 1 when t.status='S' then 0.1 when t.status is null then 0 end) as bobot, 0 as B, 0 as P, 0 as S 
        from (select u.id, p.status from (select id from users where role in ('A', 'S') and (is_excluded is NULL or is_excluded=0)) u 
        left join post_assign a on u.id=a.user_id left join posts p on a.post_id=p.id and p.status in ('B', 'P', 'S')) t join users u on u.id=t.id group by t.id, u.name 
        union 
        select t.id, u.name,0 as bobot, sum(case when t.status='B' then 1 end) as B, sum(case when t.status='P' then 1 end) as P, sum(case when t.status='S' then 1 end) as S 
        from (select u.id, p.status from (select id from users where role in ('A', 'S') and (is_excluded is NULL or is_excluded=0)) u 
        left join post_assign a on u.id=a.user_id left join posts p on a.post_id=p.id and p.status in ('B', 'P', 'S')) t join users u on u.id=t.id group by t.id, u.name ) s 
    left join (select x.id,x.user_id,x.post_created_at,first_comment,last_comment,round(avg(x.response_time)) as response_time, round(avg(x.processing_time)) as processing_time,
        round(avg(x.solving_time)) as solving_time
        from (select c.id, a.user_id, 
        post_created_at, first_comment, last_comment, 
        DATE_PART('day',first_comment-post_created_at)*24*60+ DATE_PART('hour',first_comment-post_created_at)*60+ 
        DATE_PART('minute',first_comment-post_created_at) as response_time, 
        DATE_PART('day',last_comment-first_comment)*24*60+ DATE_PART('hour',last_comment-first_comment)*60+ 
        DATE_PART('minute',last_comment-first_comment) as processing_time,
        DATE_PART('day',last_comment-post_created_at)*24*60+ DATE_PART('hour',last_comment-post_created_at)*60+ 
        DATE_PART('minute',last_comment-post_created_at) as solving_time
        from (select p.id, p.created_at as post_created_at, min(c.created_at) as first_comment, max(c.created_at) as last_comment from posts p 
        inner join comments c on c.post_id=p.id group by p.id, post_created_at) c 
        left join post_assign a on a.post_id=c.id order by c.id asc) x
        group by id, post_created_at, first_comment,last_comment, user_id order by id) r on r.user_id=s.id 
    group by s.id, name order by name
    ";
        $data = dbGetRows($sql);
        return $data;  
    }

    function getUserStat($page=0, $start=NULL, $end=NULL){
        //backup
//         $sql = "select s.id, name, sum(S) as S,round(avg(r.response_time)) as response_time, round(avg(r.processing_time)) as processing_time, round(avg(r.solving_time)) as solving_time 
// from (
//     select t.id, u.name, 0 as S 
//     from (select u.id, p.status from (select id from users where role in ('A', 'S') and (is_excluded is NULL or is_excluded=0)) u 
//     left join post_assign a on u.id=a.user_id left join posts p on a.post_id=p.id and p.status='S') t join users u on u.id=t.id group by t.id, u.name 
//     union 
//     select t.id, u.name,sum(case when t.status='S' then 1 end) as S 
//     from (select u.id, p.status from (select id from users where role in ('A', 'S') and (is_excluded is NULL or is_excluded=0)) u 
//     left join post_assign a on u.id=a.user_id left join posts p on a.post_id=p.id and p.status='S') t join users u on u.id=t.id group by t.id, u.name ) s 
// left join (select x.id,x.user_id,x.post_created_at,first_comment,last_comment,round(avg(x.response_time)) as response_time, round(avg(x.processing_time)) as processing_time,
//     round(avg(x.solving_time)) as solving_time
//     from (select c.id, a.user_id, 
//     post_created_at, first_comment, last_comment, 
//     DATE_PART('day',first_comment-post_created_at)*24*60+ DATE_PART('hour',first_comment-post_created_at)*60+ 
//     DATE_PART('minute',first_comment-post_created_at) as response_time, 
//     DATE_PART('day',last_comment-first_comment)*24*60+ DATE_PART('hour',last_comment-first_comment)*60+ 
//     DATE_PART('minute',last_comment-first_comment) as processing_time,
//     DATE_PART('day',last_comment-post_created_at)*24*60+ DATE_PART('hour',last_comment-post_created_at)*60+ 
//     DATE_PART('minute',last_comment-post_created_at) as solving_time
//     from (select p.id, p.created_at as post_created_at, min(c.created_at) as first_comment, max(c.created_at) as last_comment from posts p 
//     inner join comments c on c.post_id=p.id group by p.id, post_created_at) c 
//     left join post_assign a on a.post_id=c.id order by c.id asc) x
//     group by id, post_created_at, first_comment,last_comment, user_id order by id) r on r.user_id=s.id 
// group by s.id, name order by name
// ";   
        $filter_post = '';
        $filter_user = '';
        if (!empty($start) and !empty($end)){
            $filter_post = " where to_date(to_char(post_created_at::date,'DD-MM-YYYY'),'DD-MM-YYYY') >= to_date('$start','DD-MM-YYYY') and to_date(to_char(post_created_at::date,'DD-MM-YYYY'),'DD-MM-YYYY') < to_date('$end','DD-MM-YYYY') ";
            $filter_user = " where (to_date(to_char(p.created_at::date,'DD-MM-YYYY'),'DD-MM-YYYY') >= to_date('$start','DD-MM-YYYY') and to_date(to_char(p.created_at::date,'DD-MM-YYYY'),'DD-MM-YYYY') < to_date('$end','DD-MM-YYYY')) or status is null ";
        }
        $sql="select s.id, name, S,COALESCE(round(avg(r.response_time)),0) as response_time, COALESCE(round(avg(r.processing_time)),0) as processing_time, COALESCE(round(avg(r.solving_time)),0) as solving_time, 
jml_rating, round(case when s=0 then 0 else CAST(jml_rating as FLOAT)/CAST(s as FLOAT)*100 end) as solvability
from (  select t.id, u.name, sum(case when t.status='S' then 1 else 0 end) as S, sum(case when t.rating>=3 then 1 else 0 end) as jml_rating
    from (select u.id, p.status, p.id as post_id, COALESCE(pr.rating,0) as rating, p.created_at
        from (select id from users where role in ('A', 'S') and (is_excluded is NULL or is_excluded=0)) u 
        left join post_assign a on u.id=a.user_id 
        left join posts p on a.post_id=p.id and p.status='S'
        left join post_rating pr on p.id=pr.post_id
        $filter_user order by post_id) t 
    join users u on u.id=t.id group by t.id, u.name) s 
left join (select x.id,x.user_id,x.post_created_at,first_comment,last_comment,COALESCE(round(avg(x.response_time)),0) as response_time, COALESCE(round(avg(x.processing_time)),0) as processing_time, 
    COALESCE(round(avg(x.solving_time)),0) as solving_time 
    from (select c.id, a.user_id, post_created_at, first_comment, last_comment, DATE_PART('day',first_comment-post_created_at)*24*60+ 
    DATE_PART('hour',first_comment-post_created_at)*60+ DATE_PART('minute',first_comment-post_created_at) as response_time, DATE_PART('day',last_comment-first_comment)*24*60+ 
    DATE_PART('hour',last_comment-first_comment)*60+ DATE_PART('minute',last_comment-first_comment) as processing_time, DATE_PART('day',last_comment-post_created_at)*24*60+ 
    DATE_PART('hour',last_comment-post_created_at)*60+ DATE_PART('minute',last_comment-post_created_at) as solving_time from (select p.id, p.created_at as post_created_at, 
    min(c.created_at) as first_comment, max(c.created_at) as last_comment from posts p inner join comments c on c.post_id=p.id
    where p.status='S'
     group by p.id, post_created_at) c 
    left join post_assign a on a.post_id=c.id order by c.id asc) x 
    $filter_post
    group by id, post_created_at, first_comment,last_comment, user_id order by id) r on r.user_id=s.id 
    
group by s.id, name, jml_rating,S order by solvability desc, s desc, processing_time asc, response_time asc,name";
        $data = dbGetRows($sql);
        return $data;
    }

    function getStd($page=0, $start=NULL, $end=NULL){
        $filter_post = '';
        $filter_user = '';
        if (!empty($start) and !empty($end)){
            $filter_post = " where to_date(to_char(post_created_at::date,'DD-MM-YYYY'),'DD-MM-YYYY') >= to_date('$start','DD-MM-YYYY') and to_date(to_char(post_created_at::date,'DD-MM-YYYY'),'DD-MM-YYYY') < to_date('$end','DD-MM-YYYY') ";
            $filter_user = " where (to_date(to_char(p.created_at::date,'DD-MM-YYYY'),'DD-MM-YYYY') >= to_date('$start','DD-MM-YYYY') and to_date(to_char(p.created_at::date,'DD-MM-YYYY'),'DD-MM-YYYY') < to_date('$end','DD-MM-YYYY')) or status is null ";
        }
        $sql="select sum(s) as total_s,avg(s) as avg_s, avg(response_time) as avg_response,avg(processing_time) as avg_process, avg(solving_time) as avg_solving, avg(solvability) as solvability from (select s.id, name, S,round(avg(r.response_time)) as response_time, round(avg(r.processing_time)) as processing_time, round(avg(r.solving_time)) as solving_time,jml_rating, round(case when s=0 then 0 else CAST(jml_rating as FLOAT)/CAST(s as FLOAT)*100 end) as solvability
from (  select t.id, u.name, sum(case when t.status='S' then 1 else 0 end) as S, sum(case when t.rating>=3 then 1 else 0 end) as jml_rating
    from (select u.id, p.status, p.id as post_id, COALESCE(pr.rating,0) as rating, p.created_at
        from (select id from users where role in ('A', 'S') and (is_excluded is NULL or is_excluded=0)) u 
        left join post_assign a on u.id=a.user_id 
        left join posts p on a.post_id=p.id and p.status='S'
        left join post_rating pr on p.id=pr.post_id
        $filter_user order by post_id) t 
    join users u on u.id=t.id group by t.id, u.name) s 
left join (select x.id,x.user_id,x.post_created_at,first_comment,last_comment,COALESCE(round(avg(x.response_time)),0) as response_time, COALESCE(round(avg(x.processing_time)),0) as processing_time, 
    COALESCE(round(avg(x.solving_time)),0) as solving_time 
    from (select c.id, a.user_id, post_created_at, first_comment, last_comment, DATE_PART('day',first_comment-post_created_at)*24*60+ 
    DATE_PART('hour',first_comment-post_created_at)*60+ DATE_PART('minute',first_comment-post_created_at) as response_time, DATE_PART('day',last_comment-first_comment)*24*60+ 
    DATE_PART('hour',last_comment-first_comment)*60+ DATE_PART('minute',last_comment-first_comment) as processing_time, DATE_PART('day',last_comment-post_created_at)*24*60+ 
    DATE_PART('hour',last_comment-post_created_at)*60+ DATE_PART('minute',last_comment-post_created_at) as solving_time from (select p.id, p.created_at as post_created_at, 
    min(c.created_at) as first_comment, max(c.created_at) as last_comment from posts p inner join comments c on c.post_id=p.id
    where p.status='S'
     group by p.id, post_created_at) c 
    left join post_assign a on a.post_id=c.id order by c.id asc) x 
    $filter_post
    group by id, post_created_at, first_comment,last_comment, user_id order by id) r on r.user_id=s.id 
group by s.id, name, jml_rating,S order by name) avg";
        $data = dbGetRows($sql);
        return $data;
    }

    public function getUsersForAPI($user_id, $nama=NULL){
        if ($this->isStaff($user_id)){
            $sql = "select id, name, username from users where role='O' and (name ilike '%$nama%') and coalesce(username,'') != '' order by name limit 15 ";
        } else {
            $sql = "select u.id, u.name, u.username from group_members gm 
                left join users u on u.id=gm.user_id
                left join groups g on g.id=gm.group_id
                where role='O' and gm.group_id in (select group_id from group_members where user_id='$user_id' and is_admin=1)
                and (u.name ilike '%$nama%') and coalesce(u.username,'') != '' order by u.name limit 15 ";
        }            
        $list = dbGetRows($sql);
        return $list;
        // $json = json_encode($list);
        // return $json;
    }

    public function getOne(){
        $sql = "select $this->_column from users $this->_filter";
        return dbGetOne($sql);
    }

    public function activate($user_id){
        $update = dbUpdate($this->table, array('is_active' => 1),"id=".$user_id);
        if($update){
            return true;
        }else{
            return false;
        }
    }



}