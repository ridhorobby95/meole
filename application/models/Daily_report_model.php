<?php

defined('BASEPATH') OR exit('No direct script access allowed');

$this->load->model('Post_model');

class Daily_report_model extends Post_model {

    protected $_table = 'posts';

    protected function property($row) {
        $row['isOverTime'] = ($row['isOverTime'] == 1) ? TRUE : FALSE;
        return parent::property($row);
    }

    protected function join_or_where($row) {
        $this->_database->select("daily_reports.*, posts.*");
        $this->_database->join('daily_reports', 'posts.id = daily_reports.post_id');
        $this->_database->join('users', 'users.id = posts.user_id');
        $this->_database->where('category_id', Category_model::DAILY_REPORT);
        $this->with(array('Category', 'User', 'Comment' => 'User', 'Post_user'));
        $this->order_by(array('daily_reports.date' => 'DESC', 'posts.id' => 'ASC'));
        return $row;
    }

    public function getAll($userId, $page = 0, $param = null) {
        $condition = array('posts.user_id !=' => $userId);
        if (!empty($param)) {
            $condition[] = "(users.name ilike '%" . $param . "%' or posts.description ilike '%" . $param . "%')";
        }
        $this->limit(10, ($page * 10));
        return $this->get_many_by($condition);
    }

    public function getMe($userId, $page = 0, $param = null) {
        $condition = array(
            'posts.user_id' => $userId
        );
        if (!empty($param)) {
            $condition[] = "(users.name ilike '%" . $param . "%' or posts.description ilike '%" . $param . "%')";
        }
        $this->limit(10, ($page * 10));
        return $this->get_many_by($condition);
    }

    public function getForMe($userId, $page = 0, $param = null, $userIdFrom = null) {
        $condition = array(
            '(SELECT COUNT(*) 
                    FROM post_users 
                    WHERE post_users.post_id = posts.id AND post_users.user_id = ' . $userId . ' 
                    LIMIT 1) = 1'
        );
        if (!empty($userIdFrom))
            $condition['posts.user_id'] = $userIdFrom;
        if (!empty($param))
            $condition[] = "(users.name ilike '%" . $param . "%' or posts.description ilike '%" . $param . "%')";

        $this->limit(10, ($page * 10));
        return $this->get_many_by($condition);
    }

    public function create($data, $skip_validation = FALSE, $return = TRUE) {
        if (empty($data['description']) && empty($data['link']) && empty($data['image']) && empty($data['video']) && empty($data['file'])) {
            return 'Data yang dikirim tidak lengkap.';
        }

        $this->_database->trans_begin();
        if (empty($data['postUsers'])) {
            return 'Tujuan laporan harian harus diisi.';
        }
        $data['type'] = self::TYPE_FRIEND;
        $data['categoryId'] = Category_model::DAILY_REPORT;
        $create = parent::defaultCreate($data, $skip_validation, $return);
        if ($create) {
            $data['postId'] = $create;
            $createDailyReport = $this->_database->insert('daily_reports', $this->setFieldDB($data, $this->_database->list_fields('daily_reports')));
            if (!$createDailyReport) {
                $this->_database->trans_rollback();
                return FALSE;
            }
            if (isset($data['postUsers'])) {
                $toUser = explode(',', $data['postUsers']);
                $this->load->model('Post_user_model');
                foreach ($toUser as $user) {
                    $dataToUser = array(
                        'post_id' => $create,
                        'user_id' => $user
                    );
                    $insert = $this->Post_user_model->insert($dataToUser, TRUE, FALSE);
                    if (!$insert) {
                        $this->_database->trans_rollback();
                        return FALSE;
                    }
                }
            }
            $upload = TRUE;
            if (isset($_FILES['image']) && !empty($_FILES['image'])) {
                $upload = Image::upload('image', $create, $_FILES['image']['name'], 'posts/photos');
            }
            if (isset($_FILES['video']) && !empty($_FILES['video'])) {
                $upload = File::upload('video', $create, 'posts/videos');
            }
            if (isset($_FILES['file']) && !empty($_FILES['file'])) {
                $upload = File::upload('file', $create, 'posts/files');
            }
            if ($upload === TRUE) {
                return $this->_database->trans_commit();
            } else {
                $this->_database->trans_rollback();
                return $upload;
            }
        }
        $this->_database->trans_rollback();
        return FALSE;
    }

    public function getRecapitulation($month, $year, $userId = NULL) {
        $month = empty($month) ? date('m') : $month;
        $year = empty($year) ? date('Y') : $year;

        $sql = "select u.id, coalesce(count(d.is_over_time),0) as overTime, coalesce((sum(extract(epoch from d.end_work)) - sum(extract(epoch from d.start_work))),0) as totalWorkOverTime
                from users u 
                left join posts p on u.id = p.user_id
                left join daily_reports d on d.post_id = p.id
                where to_char(d.date,'MM') = '$month' and to_char(d.date,'YYYY') = '$year' and d.is_over_time = 1 " . (!empty($userId) ? "and p.user_id='$userId'" : "") . "
                group by u.id
                order by u.id asc";
        $overtimes = Util::toMapObject($this->_database->query($sql)->result_array(), 'id');

        $sql = "select u.id, coalesce((sum(extract(epoch from d.end_work)) - sum(extract(epoch from d.start_work))),0) as totalWorkTime
                from users u 
                left join posts p on u.id = p.user_id
                left join daily_reports d on d.post_id = p.id
                where to_char(d.date,'MM') = '$month' and to_char(d.date,'YYYY') = '$year' and d.is_over_time <> 1 " . (!empty($userId) ? "and p.user_id='$userId'" : "") . "
                group by u.id
                order by u.id asc";
        $works = Util::toMapObject($this->_database->query($sql)->result_array(), 'id');

        $sql = "select u.id, count(distinct d.date) as countReport
                from users u 
                join posts p on u.id = p.user_id
                join daily_reports d on d.post_id = p.id
                where to_char(d.date,'MM') = '$month' and to_char(d.date,'YYYY') = '$year' " . (!empty($userId) ? "and p.user_id='$userId'" : "") . "
                group by u.id
                order by u.id asc";
        $reports = Util::toMapObject($this->_database->query($sql)->result_array(), 'id');

        $sql = "select u.id, count(distinct p.scan_date::date) as countPresence
                from users u 
                join presence_log p on u.id = p.user_id
                where to_char(p.scan_date,'MM') = '$month' and to_char(p.scan_date,'YYYY') = '$year' " . (!empty($userId) ? "and p.user_id='$userId'" : "") . "
                group by u.id
                order by u.id asc";
        $presences = Util::toMapObject($this->_database->query($sql)->result_array(), 'id');

        $sql = "select user_id, count(*) countlate from presence_detail
                where start_scan::int > 830 
                        and to_char(presence_date,'MM') = '$month' 
                        and to_char(presence_date,'YYYY') = '$year'
                " . (!empty($userId) ? "and user_id='$userId'" : "") . "
                group by user_id
                order by user_id";
        $lates = Util::toMapObject($this->_database->query($sql)->result_array(), 'user_id');

        $sql = "select id, name, username from users where status = 'A' " . (!empty($userId) ? "and id='$userId'" : "") . " order by name";
        $users = $this->_database->query($sql)->result_array();

        foreach ($users as $key => $user) {
            $users[$key]['countreport'] = $reports[$user['id']]['countreport'];
            $users[$key]['totalworktime'] = $works[$user['id']]['totalworktime'];
            $users[$key]['overtime'] = $overtimes[$user['id']]['overtime'];
            $users[$key]['totalworkovertime'] = $overtimes[$user['id']]['totalworkovertime'];
            $users[$key]['presence'] = $presences[$user['id']]['countpresence'];
            $users[$key]['late'] = $lates[$user['id']]['countlate'];
        }

        return $users;
    }

    public function getYear() {
        $sql = "select distinct to_char(date, 'YYYY') as year from daily_reports order by to_char(date, 'YYYY') desc";
        return $this->_database->query($sql)->result_array();
    }

    public function getMaxByMonth($month, $year) {
        $month = empty($month) ? date('m') : $month;
        $year = empty($year) ? date('Y') : $year;

        $sql = "select count(distinct d.date) as max from daily_reports d
                join posts p on d.post_id = p.id
                where to_char(d.date,'YYYYMM') = '$year$month'
                group by p.user_id
                order by count(distinct d.date) desc limit 1";
        return $this->_database->query($sql)->row()->max;
    }

}
