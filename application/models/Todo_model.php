<?php

defined('BASEPATH') OR exit('No direct script access allowed');

$this->load->model('Post_model');

class Todo_model extends Post_model {

    const STATUS_OPEN = 1;
    const STATUS_PROGRESS = 2;
    const STATUS_DONE = 3;

    protected $_table = 'posts';

    protected function property($row) {
        $row['isRemindMe'] = ($row['isRemindMe'] == 1) ? TRUE : FALSE;
        return parent::property($row);
    }

    protected function join_or_where($row) {
        $this->_database->join('todos', 'posts.id = todos.post_id');
        $this->_database->where("posts.category_id", Category_model::TODO);
        $this->with('User');
        return $row;
    }

    public function getAll($page = 0) {
        $condition = array();
        $this->limit(10, ($page * 10));
        return $this->get_many_by($condition);
    }

    public function getNew($userId, $page = 0) {
        $condition = array(
            'posts.user_id' => $userId,
            'todos.date >=' => Util::timeNow()
        );
        $this->limit(10, ($page * 10));
        return $this->order_by('todos.date', 'ASC')->get_many_by($condition);
    }

    public function getOld($userId, $page = 0) {
        $condition = array(
            'posts.user_id' => $userId,
            'todos.date <' => Util::timeNow()
        );
        $this->limit(10, ($page * 10));
        return $this->order_by('todos.date', 'DESC')->get_many_by($condition);
    }

    public function create($data, $skip_validation = FALSE, $return = TRUE) {
        $this->_database->trans_begin();
        $data['type'] = self::TYPE_PRIVATE;
        $data['categoryId'] = Category_model::TODO;
        $create = parent::defaultCreate($data, $skip_validation, $return);
        if ($create) {
            $data['postId'] = $create;
            $createTodo = $this->_database->insert('todos', $this->setFieldDB($data, $this->_database->list_fields('todos')));
            if (!$createTodo) {
                $this->_database->trans_rollback();
                return FALSE;
            }
            $upload = TRUE;
            if (isset($_FILES['image']) && !empty($_FILES['image'])) {
                $upload = Image::upload('image', $create, $_FILES['image']['name'], 'posts/photos');
            }
            if (isset($_FILES['video']) && !empty($_FILES['video'])) {
                $upload = File::upload('video', $create, 'posts/videos');
            }
            if (isset($_FILES['file']) && !empty($_FILES['file'])) {
                $upload = File::upload('file', $create, 'posts/files');
            }
            if ($upload === TRUE) {
                return $this->_database->trans_commit();
            } else {
                $this->_database->trans_rollback();
                return $upload;
            }
        }
        $this->_database->trans_rollback();
        return FALSE;
    }

    public function save($primary_value, $data = array(), $skip_validation = FALSE) {
        $this->_database->trans_begin();
        $update = parent::save($primary_value, $data, $skip_validation);
        if ($update) {
            $this->_database->where('post_id', $primary_value);
            $update = $this->_database->update('todos', $this->setFieldDB($data, $this->_database->list_fields('todos')));
            if ($update === TRUE) {
                return $this->_database->trans_commit();
            }
        }
        $this->_database->trans_rollback();
        return FALSE;
    }

}
