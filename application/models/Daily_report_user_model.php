<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Daily_report_user_model extends AppModel {

    protected $belongs_to = array(
        'User',
        'User_default' => array('primary_key' => 'user_default', 'model' => 'User_model')
    );
    protected $label = array(
        'user_id' => 'User',
        'user_default' => 'User untuk Laporan Harian',
    );
    protected $validation = array(
        'user_id' => 'required|integer',
        'user_default' => 'required|integer'
    );

}
