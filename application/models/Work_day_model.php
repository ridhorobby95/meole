<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Work_day_model extends AppModel {

    public static function daysInMonth($month, $year) {
        return $month == 2 ? ($year % 4 ? 28 : ($year % 100 ? 29 : ($year % 400 ? 28 : 29))) : (($month - 1) % 7 % 2 ? 30 : 31);
    }

    public static function getWorkDays($month, $year) {
        $days = self::daysInMonth($month, $year);
        $total = 0;
        for ($i = 1; $i <= $days; $i++) {
            $day = date('N', strtotime("$year-$month-$i"));
            if ($day != 6 && $day != 7) {
                $total++;
            }
        }
        return $total;
    }

    public function getCount($month, $year) {
        $month = empty($month) ? date('m') : $month;
        $year = empty($year) ? date('Y') : $year;

        $data = $this->get($year . $month);
        if (!empty($data)) {
            return $data['days'];
        } else {
            return self::getWorkDays($month, $year);
        }
    }

}
