<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Notification_model extends AppModel {

    //STATUS
    const STATUS_NOT_SEND = 'N';
    const STATUS_SEND = 'S';
    //TYPE
    const TYPE_POST_DETAIL = 'PD';
    const TYPE_DAILY_REPORT_DETAIL = 'DD';
    const TYPE_ISSUE_DETAIL = 'ID';
    const TYPE_TODO_DETAIL = 'TD';
    const TYPE_GROUP_DETAIL = 'GD';
    //ACTION
    const ACTION_POST_CREATE = 'PC';
    const ACTION_COMMENT_CREATE = 'CC';
    const ACTION_PRIVATE_CHANGED = 'VC';
    const ACTION_DINAS_CHANGED = 'DC';
    const ACTION_OPERATOR_CHANGED = 'OC';
    const ACTION_OPERATOR_CHANGED_USER = 'OCU';
    const ACTION_DINAS_CHANGED_USER = 'DCU';
    const ACTION_APPROVED_DINAS_CHANGED = 'ADC';
    const ACTION_APPROVED_DINAS_CHANGED_USER = 'ADCU';
    const ACTION_ISSUE_STATUS = 'IS';
    const ACTION_TICKET_ASSIGN = 'TA';
    const ACTION_STATUS_CHANGED = 'SC';
    const ACTION_UPVOTE = 'U';

    protected $many_to_many = array(
        'Notification_sender' => array('Notification', 'User'),
    );
    protected $has_many = array(
        'Notification_user'
    );

    public function save($action = NULL, $message = NULL, $senderId, $receiverIds = array(), $referenceId = NULL, $type) {
        if (empty($receiverIds)){
            return false;
        }
        $data = array(
            'reference_id' => $referenceId,
            'message' => $message,
            'action' => $action,
            'reference_type' => $type,
            'user_id' => $senderId,
        );
        // echo "<pre>";print_r($receiverIds);die();
        // echo "<pre>";print_r($data);die();
        $save = $this->create($data);
        if ($save) {
            $this->load->model('Notification_sender_model');
            $insert = $this->Notification_sender_model->insert(array('notification_id' => $save, 'user_id' => $senderId));
            $receiver = $updated = array();
            $this->load->model('Notification_user_model');
            foreach ($receiverIds as $receiverId) {
                // if (!in_array($receiverId, $receivers)) {
                    $receiver[] = array(
                        'user_id' => $receiverId,
                        'notification_id' => $save,
                        'status' => Notification_user_model::STATUS_NOTIFICATION,
                        'created_at' => Util::timeNow(),
                        'updated_at' => Util::timeNow()
                    );
                // } else {
                //     $updated[] = $receiverId;
                // }
            }
            if (!empty($receiver)) {
                $ins = $this->_database->insert_batch('notification_users', $receiver);
                return $save;
            }

        }
        // $this->_database->trans_rollback();

        return false;
    }

    public function getTypeByCategory($categoryId) {
        $this->load->model('Category_model');
        if (in_array($categoryId, array(Category_model::INFO, Category_model::EVENT, Category_model::KNOWLEDGE_MANAGEMENT))) {
            $type = self::TYPE_POST_DETAIL;
        } elseif (in_array($categoryId, array(Category_model::TASK, Category_model::BUG, Category_model::ENHANCEMENT, Category_model::PROPOSAL))) {
            $type = self::TYPE_ISSUE_DETAIL;
        } elseif ($categoryId == Category_model::DAILY_REPORT) {
            $type = self::TYPE_DAILY_REPORT_DETAIL;
        } elseif ($categoryId == Category_model::TODO) {
            $type = self::TYPE_TODO_DETAIL;
        }
        return $type;
    }

    public function generate2($action, $reference_id, $sender_id = NULL) {
        $this->load->model('User_model');
        switch ($action) {
            case self::ACTION_POST_CREATE:
                $this->load->model('Post_model');
                $post = $this->Post_model->getPost($sender_id, $reference_id);
                $type = self::TYPE_POST_DETAIL;
                $sender_id = $post['post_user_id'];
                $reference_id = $post['post_id'];
                switch ($post['post_type']) {
                    case Post_model::TYPE_GROUP:
                        $group_members = $this->Group_member_model->getAllMembers($post['group_id']);
                        $receiver_ids = Util::toList($group_members, 'id', array($sender_id));
                        break;
                    case Post_model::TYPE_TICKET:
                        $sql = "select user_id from post_assign where post_id=$reference_id";
                        $post_users = dbGetRows($sql);
                        $receiver_ids = Util::toList($post_users, 'user_id', array($sender_id));
                        break;
                }
                if (!empty($post['image'])) {
                    $message = 'memposting gambar ';
                } elseif (!empty($post['file'])) {
                    $message = 'memposting berkas "' . $post['file']['name'] . '"';
                } elseif (!empty($post['video'])) {
                    $message = 'memposting video "' . $post['video']['name'] . '"';
                } elseif (!empty($post['link'])) {
                    $message = 'memposting link "' . $post['link'] . '"';
                } elseif (!empty($post['post_description'])) {
                    if (strlen($post['post_description']) > 30) {
                        $str = substr($post['post_description'], 0, 30) . '...';
                    } else {
                        $str = $post['post_description'];
                    }
                    $message = 'memposting "' . $str . '"';
                }
                break;
            case self::ACTION_COMMENT_CREATE:
                $this->load->model('Comment_model');
                $comment = $this->Comment_model->get($reference_id);
                if(!empty($comment)){
                    $this->load->model('Post_model');
                    $comment['post'] = $this->Post_model->with(array('Group' => 'Group_member', 'Post_user'))->get_by("posts.id", $comment['postId']);
                }
                $sender_id = $comment['userId'];
                $reference_id = $comment['postId'];
                $type = $this->getTypeByCategory($comment['post']['categoryId']);
                switch ($comment['post']['type']) {
                    case Post_model::TYPE_PUBLIC:
                        $users = $this->User_model->getListFor($sender_id);
                        $receiver_ids = Util::toList($users, 'id');
                        break;
                    case Post_model::TYPE_GROUP:
                        $receiver_ids = Util::toList($comment['post']['group']['groupMembers'], 'userId', array($sender_id));
                        break;
                    case Post_model::TYPE_FRIEND:
                        $receiver_ids = Util::toList($comment['post']['postUsers'], 'id', array($sender_id));
                        if (!in_array($comment['post']['userId'], $receiver_ids)) {
                            $receiver_ids[] = $comment['post']['userId'];
                        }
                        break;
                    case Post_model::TYPE_TICKET:
                        $sql = "select user_id from post_assign where post_id=$referenceId";
                        $rows = dbGetRows($sql);
                        $temp1 = Util::toList($rows, 'user_id', array($sender_id));

                        $sql = "select distinct user_id from comments where post_id=$referenceId";
                        $rows = dbGetRows($sql);
                        $temp2 = Util::toList($rows, 'user_id', array($sender_id));

                        $sql = "select user_id from posts where id=$referenceId";
                        $rows = dbGetRows($sql);
                        $temp3 = Util::toList($rows, 'user_id', array($sender_id));

                        $receiver_ids = array_unique(array_merge($temp1, $temp2, $temp3));

                        break;
                }
                if (strlen($comment['text']) > 30) {
                    $str = substr($comment['text'], 0, 30) . '...';
                } else {
                    $str = $comment['text'];
                }
                $message = 'memberi tanggapan "' . $str . '"';
                break;
            case self::ACTION_GROUP_CREATE:
                $this->load->model('Group_model');
                $this->load->model('Group_member_model');
                $group = $this->Group_model->with('Group_member')->get($reference_id);
                $type = self::TYPE_GROUP_DETAIL;
                foreach ($group['groupMembers'] as $member) {
                    if ($member['groupMemberRole']['id'] == Group_member_model::ROLE_OUWNER) {
                        $sender_id = $member['user']['id'];
                    } else {
                        $receiver_ids[] = $member['user']['id'];
                    }
                }
                $message = 'menambahkan anda';
                break;
            case self::ACTION_TICKET_ASSIGN:
                $sql = "select user_id from post_assign where post_id=$reference_id";
                $post_users = dbGetRows($sql);
                $receiver_ids = Util::toList($post_users, 'user_id', array($sender_id));
                $type = self::ACTION_TICKET_ASSIGN;
                $message = 'menugaskan tiket';
            break;
        }
        // echo $action.'<br>'.$message.'<br>'.$sender_id.'<br>'.$type.'<br>'.$reference_id;
        // die();
        $this->save($action, $message, $sender_id, $receiver_ids, $reference_id);
    }

    public function generate($action, $referenceId, $senderId = NULL, $receiver_id = null) {
        $this->load->model('User_model');
        switch ($action) {
            case self::ACTION_POST_CREATE:
                $this->load->model('Post_model');
                $post = dbGetRow("select * from posts where id=".$referenceId);
                switch ($post['type']) {
                    case Post_model::POST_TICKET:
                        $type = Post_model::POST_TICKET;
                        $sql = "select user_id from post_assign where post_id=$referenceId";
                        $postUsers = dbGetRows($sql);
                        $receiverIds = Util::toList($postUsers, 'user_id', array($senderId));
                        if (strlen($post['description']) > 30) {
                            $str = substr($post['description'], 0, 30) . '...';
                        } else {
                            $str = $post['description'];
                        }
                        $message = 'memposting "' . $str . '"';
                        $this->load->model('Post_log_model');
                        $this->Post_log_model->generate(Post_log_model::LOG_POST_TICKET, $referenceId, $senderId, $message);
                    break;
                }
            break;
            case self::ACTION_PRIVATE_CHANGED:
                $this->load->model('Post_model');
                $post = dbGetRow("select * from posts where id=".$referenceId);
                $type = Post_model::PRIVATE_CHANGED;
                $receiverIds[0] = $post['user_id'];
                $privateName = Status::privateName($post['is_private']);
                if (strlen($post['description']) > 30) {
                    $str = substr($post['description'], 0, 30) . '...';
                } else {
                    $str = $post['description'];
                }
                $message = 'mengubah status privasi "' . $str . '" menjadi '.$privateName;
                $this->load->model('Post_log_model');

                $receiver_mobile[0] = $post['user_id'];
                $sendMobileNotification = true;

                $this->Post_log_model->generate(Post_log_model::LOG_PRIVATE_CHANGED, $referenceId, $senderId, $message);
            break;
            case self::ACTION_STATUS_CHANGED:
                $this->load->model('Post_model');
                $post = dbGetRow("select * from posts where id=".$referenceId);
                $type = Post_model::STATUS_CHANGED;
                $receiverIds[0] = $post['user_id'];
                $statusName = Status::statusName($post['status']);
                if (strlen($post['description']) > 30) {
                    $str = substr($post['description'], 0, 30) . '...';
                } else {
                    $str = $post['description'];
                }
                $message = 'mengubah status laporan "' . $str . '" menjadi '.$statusName;
                $this->load->model('Post_log_model');

                $receiver_mobile[0] = $post['user_id'];
                $sendMobileNotification = true;
                $this->Post_log_model->generate(Post_log_model::LOG_STATUS_CHANGED, $referenceId, $senderId, $message);
            break;
            case self::ACTION_COMMENT_CREATE:
                $this->load->model('Comment_model');
                $comment = $this->Comment_model->get($referenceId);
                if(!empty($comment)){
                    $this->load->model('Post_model');
                    $referenceId = $comment['postId'];
                    if($comment['type'] == Comment_model::DISKUSI){
                        $type = Comment_model::DISKUSI;
                        $jenis = 'Diskusi';
                        $role = dbGetOne("select role from users where id=".$senderId);
                        if($role == Role::OPERATOR || $role == Role::ADMINISTRATOR){
                            $pelapor = dbGetOne("select user_id from posts where id=".$comment['postId']);
                            $receiverIds[0] = $pelapor;
                            $receiver_mobile[0] = $pelapor;
                            $sendMobileNotification = true;
                        }else{
                            $petugas = dbGetOne("select user_id from post_assign where post_id=".$comment['postId']);
                            $receiverIds[0] = $petugas;
                        }
                        
                    }else{
                        $sendMobileNotification = true;
                        $type = Comment_model::KOMENTAR;
                        $jenis = 'Komentar';

                        $petugas = dbGetOne("select user_id from post_assign where post_id=".$comment['postId']);
                        $pelapor =  dbGetOne("select user_id from posts where id=".$comment['postId']);
                        $masyarakat = dbGetRows("select user_id from comments where post_id=".$comment['postId']." and user_id!=".$petugas." and user_id!=".$pelapor." and user_id!=".$senderId." group by user_id");
                        $index = 0;
                        if($masyarakat){
                            foreach ($masyarakat as $value) {
                                $penerima[$index] = $value['user_id'];
                                $receiver_mobile[$index] = $value['user_id'];
                                $index++;
                            }
                        }
                        if($senderId == $petugas){
                            $penerima[] = $pelapor;
                            $receiver_mobile[] = $pelapor;
                        }elseif($senderId == $pelapor){
                            $penerima[] = $petugas;
                        }else{
                            $penerima[] = $pelapor;
                            $receiver_mobile[] = $pelapor;
                            $penerima[] = $petugas;
                        }
                        $receiverIds = $penerima;

                    }
                    if (strlen($comment['text']) > 30) {
                        $str = substr($comment['text'], 0, 30) . '...';
                    } else {
                        $str = $comment['text'];
                    }
                    $message = 'memberi tanggapan '.$jenis.' "' . $str . '"';
                }
            break;
            case self::ACTION_DINAS_CHANGED:
                $this->load->model('Post_model');
                $role = $this->User_model->filter("where id=".$senderId)->column("role")->getOne();
                $post = dbGetRow("select * from posts where id=".$referenceId);
                $type = Post_model::DINAS_CHANGED;
                if($role == Role::ADMINISTRATOR){
                    $petugas_lama = dbGetOne("select petugas_id from dinas_diversion where post_id=".$referenceId." and status='A'");
                    $petugas_baru = dbGetOne("select user_id from post_assign where post_id=".$referenceId);
                    $dataDinas = dbGetRow("
                                select dd.dinas_id, u.name as nama_dinas 
                                from dinas_diversion dd
                                left join unitkerja u on dd.dinas_id = u.idunit
                                where dd.post_id=".$referenceId." and dd.status='A'");
                    $receiverIds = array(
                        0   => $petugas_lama,
                        1   => $post['user_id'],
                        2   => $petugas_baru
                    );
                    $sendMobileNotification = true;
                    $receiver_mobile[] = $post['user_id'];
                    $message = 'memindahkan postingan ke '.$dataDinas['nama_dinas'];
                }elseif($role == Role::OPERATOR){

                    $admin = dbGetOne("select id from users where role='".Role::ADMINISTRATOR."'");
                    $receiverIds[0] = $admin;
                    $dataDinas = dbGetRow("
                                select dd.dinas_id, u.name as nama_dinas 
                                from dinas_diversion dd
                                left join unitkerja u on dd.dinas_id = u.idunit
                                where dd.post_id=".$referenceId." and dd.status='S'");
                    $message = 'mengajukan pemindahan postingan ke '.$dataDinas['nama_dinas'];
                }else{
                    return false;
                }
                $this->load->model('Post_log_model');
                $this->Post_log_model->generate(Post_log_model::LOG_DINAS_CHANGED, $referenceId, $senderId, $message);
            break;
            case self::ACTION_DINAS_CHANGED_USER:
                $sendMobileNotification = true;
                $type = Post_model::DINAS_CHANGED;
                $post = dbGetRow("select * from posts where id=".$referenceId);
                $receiverIds[0] = $post['user_id'];
                if (strlen($post['description']) > 30) {
                    $str = substr($post['description'], 0, 30) . '...';
                } else {
                    $str = $post['description'];
                }
                $message = 'mengubah status postingan anda "'.$str.'" menjadi '.Status::statusName(Status::STATUS_DIALIHKAN);
                $this->load->model('Post_log_model');
                $this->Post_log_model->generate(Post_log_model::LOG_DINAS_CHANGED_USER, $referenceId, $senderId, $message);
            break;
            case self::ACTION_APPROVED_DINAS_CHANGED:
                $type = Post_model::DINAS_CHANGED;
                $this->load->model('Dinas_diversion_model');
                $detail_diversion = dbGetRow("select * from dinas_diversion where post_id=".$referenceId);
                $post = dbGetRow("select * from posts where id=".$referenceId);
                if($detail_diversion['status'] == Dinas_diversion_model::DITERIMA){
                    $petugas_lama = dbGetOne("select petugas_id from dinas_diversion where post_id=".$referenceId." and status='A'");
                    $petugas_baru = dbGetOne("select user_id from post_assign where post_id=".$referenceId);
                    $dataDinas = dbGetRow("
                                select dd.dinas_id, u.name as nama_dinas 
                                from dinas_diversion dd
                                left join unitkerja u on dd.dinas_id = u.idunit
                                where dd.post_id=".$referenceId." and dd.status='A'");
                    if (strlen($post['description']) > 30) {
                        $str = substr($post['description'], 0, 30) . '...';
                    } else {
                        $str = $post['description'];
                    }
                    $message = 'memindahkan postingan "'.$str.'" kepada '.$dataDinas['nama_dinas'];
                    $receiverIds[0] = $petugas_lama;
                    $receiverIds[1] = $petugas_baru;
                    $receiverIds[2] = $post['user_id'];
                    $receiver_mobile[] = $post['user_id'];
                    $sendMobileNotification = true;
                }elseif($detail_diversion['status'] == Dinas_diversion_model::DITOLAK){
                    $petugas_lama = dbGetOne("select petugas_id from dinas_diversion where post_id=".$referenceId." and status='R'");
                    $receiverIds[0] = $petugas_lama;
                    if (strlen($post['description']) > 30) {
                        $str = substr($post['description'], 0, 30) . '...';
                    } else {
                        $str = $post['description'];
                    }
                    $message = 'menolak pemindahan Dinas pada postingan "'.$str.'"';
                }
                elseif($detail_diversion['status'] == Dinas_diversion_model::DIALIHKAN){
                    $petugas_lama = dbGetOne("select petugas_id from dinas_diversion where post_id=".$referenceId." and status='D'");
                    $petugas_baru = dbGetOne("select user_id from post_assign where post_id=".$referenceId);
                    $dataDinas = dbGetRow("
                                select dd.dinas_id, u.name as nama_dinas 
                                from dinas_diversion dd
                                left join unitkerja u on dd.dinas_id_2 = u.idunit
                                where dd.post_id=".$referenceId." and dd.status='D'");
                    if (strlen($post['description']) > 30) {
                        $str = substr($post['description'], 0, 30) . '...';
                    } else {
                        $str = $post['description'];
                    }
                    $message = 'memindahkan postingan "'.$str.'" kepada '.$dataDinas['nama_dinas'];
                    $receiverIds[0] = $petugas_lama;
                    $receiverIds[1] = $petugas_baru;
                }
                $this->load->model('Post_log_model');
                $this->Post_log_model->generate(Post_log_model::LOG_APPROVED_DINAS_CHANGED, $referenceId, $senderId, $message);
            break;
            case self::ACTION_APPROVED_DINAS_CHANGED_USER:
                $type = Post_model::DINAS_CHANGED;
                $this->load->model('Dinas_diversion_model');
                $detail_diversion = dbGetRow("select * from dinas_diversion where post_id=".$referenceId);
                $post = dbGetRow("select * from posts where id=".$referenceId);
                $receiverIds[0] = $post['user_id'];
                $receiver_mobile[] = $post['user_id'];
                $sendMobileNotification = true;
                if($detail_diversion['status'] == Dinas_diversion_model::DITOLAK){
                    if (strlen($post['description']) > 30) {
                        $str = substr($post['description'], 0, 30) . '...';
                    } else {
                        $str = $post['description'];
                    }
                    $message = 'mengubah status postingan anda "'.$str.'" menjadi '.Status::statusName(Status::STATUS_RESPON);
                }elseif($detail_diversion['status'] == Dinas_diversion_model::DIALIHKAN){
                    $dataDinas = dbGetRow("
                                select dd.dinas_id, u.name as nama_dinas 
                                from dinas_diversion dd
                                left join unitkerja u on dd.dinas_id_2 = u.idunit
                                where dd.post_id=".$referenceId." and dd.status='D'");
                    if (strlen($post['description']) > 30) {
                        $str = substr($post['description'], 0, 30) . '...';
                    } else {
                        $str = $post['description'];
                    }
                    $message = 'memindahkan postingan anda "'.$str.'" kepada '.$dataDinas['nama_dinas'];
                }
                $this->load->model('Post_log_model');
                $this->Post_log_model->generate(Post_log_model::LOG_APPROVED_DINAS_CHANGED_USER, $referenceId, $senderId, $message);
            break;
            case self::ACTION_OPERATOR_CHANGED:
                $type = Post_model::OPERATOR_CHANGED;
                $post = dbGetRow("select * from posts where id=".$referenceId);
                $petugas_baru = dbGetRow("select pa.user_id as user_id, u.name as name 
                                          from post_assign pa 
                                          left join users u on pa.user_id = u.id
                                          where post_id=".$referenceId);
                if (strlen($post['description']) > 30) {
                    $str = substr($post['description'], 0, 30) . '...';
                } else {
                    $str = $post['description'];
                }
                $message = 'memindahkan penugasan kepada "'.$str.'" kepada '.$petugas_baru['name'];
                $receiverIds[0] = $receiver_id;
                $receiverIds[1] = $petugas_baru['user_id'];
                $receiver_mobile[] = $post['user_id'];
                $this->load->model('Post_log_model');
                $this->Post_log_model->generate(Post_log_model::LOG_OPERATOR_CHANGED, $referenceId, $senderId, $message);
            break;
            case self::ACTION_OPERATOR_CHANGED_USER:
                $type = Post_model::OPERATOR_CHANGED;
                $post = dbGetRow("select * from posts where id=".$referenceId);
                if (strlen($post['description']) > 30) {
                    $str = substr($post['description'], 0, 30) . '...';
                } else {
                    $str = $post['description'];
                }
                $message = 'mengganti operator pada postingan anda  "'.$str.'"';
                $receiverIds[0] = $post['user_id'];
                $receiver_mobile[] = $post['user_id'];
                $sendMobileNotification = true;
                $this->load->model('Post_log_model');
                $this->Post_log_model->generate(Post_log_model::LOG_OPERATOR_CHANGED_USER, $referenceId, $senderId, $message);
            break;
            case self::ACTION_UPVOTE:
                $type = Post_model::UPVOTE;
                $post = dbGetRow("select * from posts where id=".$referenceId);
                if (strlen($post['description']) > 30) {
                    $str = substr($post['description'], 0, 30) . '...';
                } else {
                    $str = $post['description'];
                }
                $receiverIds[0] = $post['user_id'];
                $receiver_mobile[] = $post['user_id'];
                $sendMobileNotification = true;
                $message = 'memberi vote pada postingan anda  "'.$str.'"';
            break;
        }

        $save = $this->save($action, $message, $senderId, $receiverIds, $referenceId, $type);
        if($sendMobileNotification && $save){
            $sender_detail = dbGetRow("select name,role from users where id=".$senderId);
            if($sender_detail['role'] == Role::OPERATOR && ($action!=self::ACTION_COMMENT_CREATE || ($action==self::ACTION_COMMENT_CREATE && $type=='D')) ){
                $message_mobile = 'OPERATOR DINAS '.$message;
            }else{
                $message_mobile = $sender_detail['name']." ".$message;
            }
            foreach ($receiver_mobile as $key => $value) {
                $notification_user_id = dbGetOne("select id from notification_users where notification_id=".$save." and user_id=".$value);
                $this->mobileNotification($value, $message_mobile, $referenceId, $notification_user_id);
            }
            
        }

    }


    protected function mobileNotification($receiver_id, $message, $post_id,$notification_user_id){
        $title = 'Meole';
        $url = $this->config->item('fcm_url');
        $google_api_key = $this->config->item('fcm_api_key');

        $device = dbGetRows("select d.reg_id, ut.device_id from user_tokens ut
                            left join devices d on ut.device_id = d.id
                            where ut.user_id = $receiver_id
                            group by d.reg_id, ut.device_id
                    ");

        foreach ($device as $row) {
            $user_id = $receiver_id;
            $reg_id = $row['reg_id'];

            $fields = array(
                'to' => $reg_id,
                'data' => array(
                    'title' => $title, 
                    'message' => $message, 
                    'post_id'=>$post_id,
                    'mobileNotification' => true,
                    'notification_user_id' => $notification_user_id
                )
                // 'notification' => array(
                //     'title' => $title, 
                //     'body' => $message, 
                //     'sound'=>'default',
                //     'vibrate' => 'true'
                // )
            );
            $headers = array(
                  'Authorization:key='.$google_api_key,
                  'Content-Type: application/json'
            );

            $ch = curl_init();
            curl_setopt($ch, CURLOPT_URL, $url);
            curl_setopt($ch, CURLOPT_POST, true);
            curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
            curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($fields));

            $result = curl_exec($ch);
            //if($result === false)    die('Curl failed ' . curl_error());
            curl_close($ch);
        }
        // echo "<pre>";print_r($device);die();
        
    }

    protected function join_or_where($row) {
        $this->_database->select("notifications.*, notification_senders.group_id, groups.name as group_name, groups.image as group_photo, notification_users.status");
        $this->_database->select("notifications.*, notification_users.status");
        $this->_database->join("notification_users", "notification_users.notification_id = notifications.id");
        $this->_database->join("notification_senders", "notification_senders.notification_id = notifications.id", "left");
        $this->_database->join("groups", "notification_senders.group_id = groups.id", "left");
        $this->with('Notification_sender');
        
        $this->order_by("case notification_users.status when 'N' then 1 when 'U' then 2 else 3 end");
        $this->order_by("notifications.id ","DESC");
    }

    public function getMe($userId, $page = 0, $mobile=false) {
        $ciConfig = $this->config->item('utils');
        if($mobile){
            $ip = $ciConfig['full_upload_dir_ip'];
        }
        $offset = $page * 20;
        $sql = "select nu.*, 
                          n.message,n.reference_id,n.reference_type,
                          ns.user_id as user_id,
                          u.name as sender_name,u.role as sender_role,
                          uk.name as dinas_name,
                          u.photo as sender_photo,
                          case
                                when u.photo is null then  '".$ip."users/photos/nopic.png'
                                else '".$ip."users/photos/thumb-'||MD5(CAST (ns.user_id AS character varying)) ||'-'||u.photo
                            end
                          as foto_profil,
                          '".$ip."posts/photos/thumb-'||MD5(MD5(CAST (p.user_id AS character varying)||'".$this->config->item('encryption_key')."')) ||'-'||p.file as nama_file
                        from notification_users nu
                          left join notifications n on nu.notification_id = n.id
                          left join posts p on n.reference_id = p.id
                          left join unitkerja uk on p.dinas_id = uk.idunit
                          left join notification_senders ns on n.id = ns.notification_id
                          left join users u on ns.user_id = u.id

         where nu.user_id=".$userId." order by n.id desc limit 20 offset ".$offset;
        $data = dbGetRows($sql);
        return $data;
        // $ouput = $this->generateMessage($notifications);
        // return $ouput;
    }

    public static function toObject($id, $message, $sender, $referenceId, $referenceType, $status, $createdAt, $updatedAt) {
        return array(
            'id' => (int) $id,
            'message' => $message,
            'sender' => $sender,
            'referenceId' => (int) $referenceId,
            'referenceType' => $referenceType,
            'status' => $status,
            'createdAt' => $createdAt,
            'updatedAt' => $updatedAt
        );
    }

    public function getNotification($userId) {
        $this->load->model('Notification_user_model');
        $this->_database->select("notifications.*, notification_users.status");
        $this->_database->join("notification_users", "notification_users.notification_id = notifications.id");
        return $this->order_by("case notification_users.status when 'N' then 1 when 'U' then 2 else 3 end","notifications.created_at DESC")->get_many_by(array('notification_users.user_id' => $userId, 'notification_users.status' => Notification_user_model::STATUS_NOTIFICATION));
    }

    public function generateMessage($notifications) {
        $types = array();

        foreach ($notifications as $notification) {
            $types[$notification['referenceType']][] = $notification['referenceId'];
        }
        $data = array();
        foreach ($types as $key => $value) {
            switch ($key) {
                case self::TYPE_POST_DETAIL:
                    $this->load->model('Post_model');
                    $data[$key] = Util::toMapObject($this->Post_model->with(array('Group', 'Post_user', 'User'))->get_many_by(array('posts.id' => $value)), 'id');
                    break;
                case self::TYPE_GROUP_DETAIL:
                    $this->load->model('Group_model');
                    $data[$key] = Util::toMapObject($this->Group_model->get_many_by(array('id' => $value)), 'id');
                    break;
            }
        }
        $ouput = array();

        foreach ($notifications as $notification) {
            $message = NULL;
            
            if (!empty($notification['message']) && empty($notification['action'])) {
                $message = $notification['message'];
            } elseif (!empty($notification['action']) && !empty($notification['message'])) {
                switch ($notification['action']) {
                    case self::ACTION_POST_CREATE:
                        $post = $data[self::TYPE_POST_DETAIL][$notification['referenceId']];
                        $helpdesk_or_group = "grup";
                        if ($post['type']=="T") {
                            $helpdesk_or_group = "helpdesk";
                        }
                        if (!empty($post['group'])) {
                            $message = $notification['message'] . " di $helpdesk_or_group " . $post['group']['name'];
                        } elseif (!empty($post['postUsers'])) {
                            $message = $notification['message'] . ' ke anda';
                            if ((count($post['postUsers']) == 2)) {
                                if ($post['postUsers'][0]['id'] == $userId) {
                                    $otherName = $post['postUsers'][1]['name'];
                                } else {
                                    $otherName = $post['postUsers'][0]['name'];
                                }
                                $message .= ' dan ' . $otherName;
                            } elseif (count($post['postUsers']) > 2) {
                                $message .= ' dan ' . (count($post['postUsers']) - 1) . ' orang lainnya';
                            }
                        } else {
                            $message = $notification['message'];
                        }
                        break;
                    case self::ACTION_COMMENT_CREATE:
                        $post = $data[self::TYPE_POST_DETAIL][$notification['referenceId']];
                        $helpdesk_or_group = "grup";
                        if ($post['type']=="T") {
                            $helpdesk_or_group = "helpdesk";
                        }
                        $senderCount = count($notification['notificationSenders']);
                        if ($senderCount == 1) {
                            $message = $notification['message'];
                        } elseif ($senderCount == 2) {
                            $message = "dan " . $notification['notificationSenders'][0]["name"];
                        } elseif ($senderCount > 2) {
                            $message = "dan " . ($senderCount - 1) . " orang lainnya";
                        }
                        if(strpos($message, 'mengomentari') == false){
			                 $message .= ' mengomentari';
                        }
                        $message .= ' pada postingan ' . $post['user']['name'];
                        if (!empty($post['group'])) {
                            $message .=" di $helpdesk_or_group " . $post['group']['name'];
                        }
                        // var_dump($notification, $message);
                        // die();
                        break;
                    case self::ACTION_GROUP_CREATE:
                        $group = $data[self::TYPE_GROUP_DETAIL][$notification['referenceId']];
                        $helpdesk_or_group = "grup";
                        if ($group['type']=="H") {
                            $helpdesk_or_group = "helpdesk";
                        }
                        $message = $notification['message'] . " di $helpdesk_or_group " . $group['name'];
                        break;
                    default:
        			     $message = $notification['message'];
        			break;
                }
            }

            if($notification['groupId']){
                $notification['notificationSenders'][0]['group'] = array(
                    'group_id' => $notification['groupId'],
                    'name' => $notification['groupName'],
                    'username' => $notification['groupName'],
                    'photo' => $notification['groupPhoto'],
                );   
            }

            $ouput[] = self::toObject($notification['id'], $message, end($notification['notificationSenders']), $notification['referenceId'], $notification['referenceType'], $notification['status'], $notification['createdAt'], $notification['updatedAt']);
        }

        return $ouput;
    }

    function sendToGCM($recepient, $message, $title, $link, $action, $actionId, $time = NULL) {
        $this->gcm->setMessage($message);

        if (is_string($recepient)) {
            $this->gcm->addRecepient($recepient);
        } else {
            foreach ($recepient as $client) {
                $this->gcm->addRecepient($client);
            }
        }

        $this->gcm->setData(array(
            'title' => $title,
            'link' => $link,
            'action' => $action,
            'actionId' => $actionId,
            'time' => empty($time) ? Util::timeNow() : $time
        ));

        $this->gcm->send();
    }

    function mobileNotification2($action, $post_id, $sender_id = NULL, $notification_id = NULL, $group_id = NULL) {
        $this->load->model('User_model');
        $title = 'Sigap';
        $message = 'Notifikasi untuk anda';
        $num_notifikasi = rand(0,10);
        switch ($action) {
            case self::ACTION_POST_CREATE:
                $type = "post created";
                $this->load->model('Post_model');
                $post = $this->Post_model->with(array('Group' => 'Group_member', 'Post_user'))->get_by('posts.id', $post_id);
                $post_type = $post['type'];
                $sender_id = $post['userId'];
                switch ($post['type']) {
                    case Post_model::TYPE_GROUP:
                        $receiver_ids = Util::toList($post['group']['groupMembers'], 'userId', array($sender_id));
                        break;
                    case Post_model::TYPE_TICKET:
                        $sql = "select user_id from post_assign where post_id=$post_id";
                        $post_assigns = dbGetRows($sql);
                        $receiver_ids = Util::toList($post_assigns, 'user_id', array($sender_id));
                        break;
                }
                if (!empty($post['image'])) {
                    $message = 'memposting gambar ';
                } elseif (!empty($post['file'])) {
                    $message = 'memposting berkas "' . $post['file']['name'] . '"';
                } elseif (!empty($post['video'])) {
                    $message = 'memposting video "' . $post['video']['name'] . '"';
                } elseif (!empty($post['link'])) {
                    $message = 'memposting link "' . $post['link'] . '"';
                } elseif (!empty($post['description'])) {
                    if (strlen($post['description']) > 30) {
                        $str = substr($post['description'], 0, 30) . '...';
                    } else {
                        $str = $post['description'];
                    }
                    $message = 'memposting "' . $str . '"';
                }
                break;
            case self::ACTION_COMMENT_CREATE:
                $type = "comment created";
                $this->load->model('Comment_model');
                $this->load->model('Post_model');
                $comment = $this->Comment_model->get($post_id);
                if(!empty($comment)){
                    $comment['post'] = $this->Post_model->with(array('Group' => 'Group_member', 'Post_user'))->get_by("posts.id", $comment['postId']);
                }
                $sender_id = $comment['userId'];
                $post_id = $comment['postId'];
                $post_type = $comment['post']['type'];
                switch ($comment['post']['type']) {
                    case Post_model::TYPE_GROUP:
                        $receiver_ids = Util::toList($comment['post']['group']['groupMembers'], 'userId', array($sender_id));
                        break;
                    case Post_model::TYPE_TICKET:
                        $sql = "select user_id from post_assign where post_id=$post_id";
                        $rows = dbGetRows($sql);
                        $temp1 = Util::toList($rows, 'user_id', array($sender_id));

                        $sql = "select distinct user_id from comments where post_id=$post_id";
                        $rows = dbGetRows($sql);
                        $temp2 = Util::toList($rows, 'user_id', array($sender_id));

                        $sql = "select user_id from posts where id=$post_id";
                        $rows = dbGetRows($sql);
                        $temp3 = Util::toList($rows, 'user_id', array($sender_id));

                        $receiver_ids = array_unique(array_merge($temp1, $temp2, $temp3));

                        break;
                }
                if (strlen($comment['text']) > 30) {
                    $str = substr($comment['text'], 0, 30) . '...';
                } else {
                    $str = $comment['text'];
                }
                $message = 'memberi tanggapan "' . $str . '"';
                break;
            case self::ACTION_TICKET_ASSIGN:
                $sql = "select user_id from post_assign where post_id=$post_id";
                $post_assigns = dbGetRows($sql);
                $receiver_ids = Util::toList($post_assigns, 'user_id', array($sender_id));
                $type = "penugasan";
                $post_type = 'T';
                $message = 'menugaskan tiket';
                break;
            case self::ACTION_STATUS_CHANGED:
                $this->load->model('Post_model');
                $sql = "select user_id from post_assign where post_id=$post_id";
                $post_assigns = dbGetRows($sql);
                $receiver_ids = Util::toList($post_assigns, 'user_id', array($sender_id));
                $type = "ganti status";
                $sql = "select p.id, p.status from posts p where p.id=$post_id";
                $post = dbGetRow($sql);
                $post_type = 'T';
                if ($post['status']=='B') {
                    $status = "Terbuka";
                } else if ($post['status']=='P'){
                    $status = "Progress";
                } else {
                    $status = "Selesai";
                }
                $message = "mengganti status tiket menjadi $status";
                break;
        }
        foreach ($receiver_ids as $key => $value) {
            $receiver_ids[$key] = "'".$value."'";
        }

        $this->load->model("Notification_user_model");

        $upd = TRUE;
        if($notification_id){
            $upd = $this->Notification_user_model->update_by(
                array('notification_id' => $notification_id), 
                array(  'status' => Notification_user_model::STATUS_NOTIFICATION, 
                        'updated_at' => Util::timeNow())
            );
        }

        if($upd){
            $str_iduser = implode(',', $receiver_ids);
            if (!$str_iduser)
                return false;
            // $registration_ids = array();
            // $user_ids = array();
            // $sql = "select d.id as device_id, ut.user_id, d.reg_id as registration_id from user_tokens ut left join devices d on d.id=ut.device_id where ut.user_id in ($str_iduser) order by ut.id desc";
            // $rows = dbGetRows($sql);
            // foreach ($rows as $row) {
            //     $user_ids[$row['user_id']] = $row['user_id'];
            //     $registration_ids[] = $row['registration_id'];
            // }

            // if (!count($registration_ids))
            //     return false;

            // $str_iduser = implode(',', $user_ids);

            $ci = &get_instance();
            if (!$this->config->item('fcm_allow_notification'))
                return false;
            $url = $this->config->item('fcm_url');
            $google_api_key = $this->config->item('fcm_api_key');
            // $fields = array(
            //                 'str_iduser'=>$str_iduser,
            //                 'post_id'=>$post_id,
            //                 'type'=>$type,
            //                 'title'=>$title,
            //                 'message'=>$message,
            //                 );

            // $url = site_url('web/publ1c/sendMobileNotification');
            // $url = str_replace('https', 'http', $url);
            // $this->curl($url,$fields);
             $sql = "select d.id as device_id, ut.user_id, d.manufacture, d.reg_id as registration_id from user_tokens ut left join devices d on d.id=ut.device_id where ut.user_id in ($str_iduser) and ut.status='I' order by ut.id desc";
            $rows = dbGetRows($sql);
            $sql = "select name from users u where u.id='$sender_id'";
            $users = dbGetRow($sql);
            $message = $users['name'].' '.$message;
            foreach ($rows as $row) {
                $user_id = $row['user_id'];
                // $num_notifikasi = $users[$iduser]['num_notifikasi'];

                // $sql = "select sum(num) from userbadge where iduser=$iduser";

                $registration_id = $row['registration_id'];

                if ($row['manufacture'] == 'apple') {
                    $fields = array(
                        'to' => $registration_id,
                        'data' => array('id'=>$id, 'type'=>$type),
                        'notification' => array(
                            'title' => $title, 
                            'message' => $message, 
                            'sound'=>'default',
                            'post_id' => $post_id,
                            'post_type' => $post_type,
                            'click_action' => 'NOTIFIKASI'
                        )
                    );
                }
                else {
                    $fields = array(
                        'to' => $registration_id,
                        'data' => array(
                            'title' => $title, 
                            'message' => $message, 
                            'post_id'=>$post_id, 
                            'type'=>$type,
                            'post_type' => $post_type,
                            'click_action' => 'id.co.integra.sigapmobile.activities.ActivityDetailPost'
                        // )
                        ),
                        'notification' => array(
                            'title' => $title, 
                            'body' => $message, 
                            'sound'=>'default',
                            'vibrate' => 'true',
                            'click_action' => 'id.co.integra.sigapmobile.activities.ActivityDetailPost'
                        )
                    );                
                }
                $headers = array(
                      'Authorization:key='.$google_api_key,
                      'Content-Type: application/json'
                 );
                // echo $url;
                // echo '<pre>';
                // var_dump($headers);
                // echo '<pre>';
                // var_dump($fields);
                // die();
                $ch = curl_init();
                curl_setopt($ch, CURLOPT_URL, $url);
                curl_setopt($ch, CURLOPT_POST, true);
                curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
                curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
                curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
                curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($fields));

                $result = curl_exec($ch);
                //if($result === false)    die('Curl failed ' . curl_error());
                curl_close($ch);
            }
            return true;
        }
    }
}
