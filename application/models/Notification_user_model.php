<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Notification_user_model extends AppModel {

    //STATUS
    const STATUS_NOTIFICATION = 'N';
    const STATUS_UNREAD = 'U';
    const STATUS_READ = 'R';

    protected $belongs_to = array(
        'User',
        'Notification'
    );

    public function setAsUnRead($userId){
        $sql = "update notification_users set status='R' where user_id='$userId' and status='N'";
        return dbQuery($sql);
    }

    // public function setAsUnRead($userId) {
    //     $condition = array('user_id' => $userId, 'status' => self::STATUS_NOTIFICATION);
    //     return $this->update_by($condition, array('status' => self::STATUS_READ));
    // }

    // public function setAsRead($userId, $referenceType, $referenceId) {
    //     $condition = array('user_id' => $userId);
    //     $this->load->model('Notification_model');
    //     $notifications = $this->Notification_model->get_many_by(array('reference_id' => $referenceId, 'reference_type' => $referenceType));
    //     if (!empty($notifications)) {
    //         $condition['notification_id'] = Util::toList($notifications, 'id');
    //         return $this->update_by($condition, array('status' => self::STATUS_READ));
    //     } else {
    //         return TRU;
    //     }
    // }

    public function setAsRead($id) {
        // $this->load->model('Notification_model');
        // $sql = "select * from notifications where reference_id=$referenceId and reference_type='$referenceType'";
        // $notifications = dbGetRow($sql);
        // // echo "<pre>";print_r($notifications);die();
        // if (!empty($notifications)) {
        //     $sql = "update notification_users set status='R' where notification_id=".$notifications['id']." and user_id=".$userId;
        //     return dbQuery($sql);
        // } else {
        //     return TRUE;
        // }
        
        $sql = dbUpdate("notification_users", array('status'  => 'R'), "id=".$id);
        return $sql;
    }

    public function getCountMe($userId) {
        return $this->count_by(array('user_id' => $userId, 'status' => self::STATUS_NOTIFICATION));
    }

}
