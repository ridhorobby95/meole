<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Category_model extends AppModel {

    const INFO = 1;
    const TASK = 2;
    const BUG = 3;
    const EVENT = 4;
    const DAILY_REPORT = 5;
    const ENHANCEMENT = 6;
    const PROPOSAL = 7;
    const TODO = 8;
    const KNOWLEDGE_MANAGEMENT = 9;

    protected $has_many = array(
        'Post',
    );

}
