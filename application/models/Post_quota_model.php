<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Post_quota_model extends AppModel {

    function getQuota($user_id, $group_id){
        $sql = "select pq.id, pq.user_id, pq.group_id, pq.quota, g.quota_limit as limit
                from post_quotas pq, groups g
                where pq.user_id = '$user_id'
                and pq.group_id = $group_id
                and pq.group_id = g.id";
        return dbGetRow($sql);
    }

    function decQuota($user_id, $group_id){
		$row_quota = $this->getQuota($user_id, $group_id);

        if($row_quota === NULL){
            $row_quota = array(
                "user_id" => $user_id,
                "group_id" => $group_id,
                "quota" => "1"
            );
            $return = $this->insert($row_quota, TRUE, FALSE);
        }else{
            $idQuota = $row_quota['id'];
	        $row_quota['quota'] = (int)$row_quota['quota'];
            $row_quota['quota']++;
            unset($row_quota['id'], $row_quota['limit']);
            $return = $this->update($idQuota, $row_quota);
        }
        return $return;
    }

    function incQuota($user_id, $group_id){
        $row_quota = $this->getQuota($user_id, $group_id);
        
        if($row_quota === NULL){
            $row_quota = array(
                "user_id" => $user_id,
                "group_id" => $group_id,
                "quota" => "0"
            );
            $return = $this->insert($row_quota, TRUE, FALSE);
        }else{
	        $idQuota = $row_quota['id'];
	        $row_quota['quota'] = (int)$row_quota['quota'];
	        $row_quota['quota']--;
	        unset($row_quota['id']);unset($row_quota['limit']);
	        $return = $this->update($idQuota, $row_quota);
        }
        return $return;
    }
}

?>
