<?php

(defined('BASEPATH')) OR exit('No direct script access allowed');

/**
 * Digunakan untuk autentifikasi user yang telah login
 */
class PrivateApiController extends ApiController {

    protected $token;
    protected $user;
    protected $page;

    public function __construct() {
        parent::__construct();
        $this->validateToken();

        $this->page = $this->uri->segment(4, 0);
    }

    /**
     * Digunakan untuk validasi token dan penyimpanan history request
     */
    private function validateToken() {

        if (!isset($this->postData['token'])) {

            $this->setResponse($this->setSystem(ResponseStatus::UNAUTHORIZED, 'Token tidak dikirim.'));
        }

        $auth = AuthManager::validateToken($this->postData['token']);
        if (is_string($auth)) {
            $this->setResponse($this->setSystem(ResponseStatus::UNAUTHORIZED, $auth));
        } elseif (is_array($auth)) {
            list($token, $user) = $auth;
            $this->token = $this->systemResponse->token = $token;
            $this->user = $user;
        }
        unset($this->postData['token']);

        $this->setSessionWeb();
    }

    private function setSessionWeb(){
        $session = array(
            'sigap' => array(
                'userid' => $this->user['id'],
                'username' => $this->user['username'],
                'name' => $this->user['name'],
                'id_organisasi' => $this->user['id_organisasi'],
                'nm_lemb' => $this->user['nm_lemb'],
                'isauthenticated' => 1
            ),
        );
        // SET ROLE USER FROM DIKTI

        $_SESSION = $session;
    }

    protected function setSystem($code = NULL, $message = NULL,  $infos = NULL, $validation = NULL) {
        if (empty($code)) {
            $code = ResponseStatus::SUCCESS;
        }
        $this->load->model('Notification_user_model');
        $notification = $this->Notification_user_model->getCountMe($this->user['id']);
        $this->systemResponse->setData($code, $message, $validation, $notification);
        return $this->systemResponse;
    }

}
