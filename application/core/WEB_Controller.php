<?php

(defined('BASEPATH')) OR exit('No direct script access allowed');

class WEB_Controller extends CI_Controller {
    const CTL_PATH = 'web/';
    const LIB_PATH = 'web/';
    const VIEW_PATH = 'web/';

    protected $class;
    protected $ctl;
    protected $data;
    protected $ispublic = 0;
    protected $method;
    protected $model;
    protected $postData;
    protected $template;
    protected $title = 'SIGAP';
    protected $view;

    protected $uri_segments = array();
    public $list_sql = array();
    /**
     * Constructor
     */
    public function __construct() {

        parent::__construct();

        // library
        $this->load->library(static::LIB_PATH . 'AuthManagerWeb');
        $this->load->library(static::LIB_PATH . 'FormatterWeb');
        $this->load->library(static::LIB_PATH . 'SessionManagerWeb');
        $this->load->library(static::LIB_PATH . 'TemplateManagerWeb');

        if ($this->class!='thumb'){
            // model
            $this->load->model('Notification_user_model');
            $this->load->model('Group_member_model');
            $this->load->model('User_model');
            $this->load->model('Group_model');
            $this->load->model('Post_model', 'Post');

            // inisialisasi atribut
            $this->class = $this->router->class;
            $this->ctl = $this->getCTL();
            $this->method = ($this->router->class == $this->router->method) ? 'index' : $this->router->method;
            $this->template = new TemplateManagerWeb();
            $this->view = $this->class . (empty($this->method) ? '' : '_' . $this->method);

            // cek login
            if (empty($this->ispublic) and ! SessionManagerWeb::isAuthenticated()) {
                AuthManagerWeb::logout();
                SessionManagerWeb::setFlash(array('errmsg' => array('Anda harus login terlebih dahuluuu')));
                redirect($this->getCTL('site'));
            }

            // inisialisasi model
            $model = ucfirst($this->class . '_model');
            if (file_exists(APPPATH . 'models/' . $model . EXT)) {
                $this->load->model($model);
                $this->model = new $model();
            }

            // inisialisasi post
            $post = $this->input->post();
            if (!empty($post)) {
                foreach ($post as $k => $v) {
                    if (!is_array($v) and ! is_object($v) and strlen($v) == 0)
                        $post[$k] = NULL;
                }

                $this->postData = $post;
            } else
                $this->postData = array();

            // inisialisasi data
            $data = array();
            $data['path'] = static::CTL_PATH;
            $data['class'] = $this->class;
            $data['method'] = $this->method;
            $data['title'] = $this->title;
            $data['arrmenu'] = $this->getMenu();
            // $data['nopic'] = base_url('assets/web/img/nopic.png');
            $data['jmlnotif'] = (int) $this->Notification_user_model->getCountMe(SessionManagerWeb::getUserID());
            // $data['isdev'] = (SessionManagerWeb::isDeveloper() ? true : false);
            // $data['user_id'] = $this->User_model->setUserID(SessionManagerWeb::getUserID());
            // $data['me'] = $this->User_model->getUser(SessionManagerWeb::getUserID());
            $data['me']['user_photo'] = $this->User_model->getUserPhoto($data['me']['id'],$data['me']['photo']);
            $this->data = $data;

            // ambil data flash
            foreach (SessionManagerWeb::getFlash() as $k => $v)
                $this->data[$k] = $v;

        
    		$user_id = SessionManagerWeb::getUserID();
            
		

            
        }
    }

    /**
     * Mendapatkan nama controller untuk redirect
     * @param string $ctl
     */
    protected function getCTL($ctl = null) {
        if (!isset($ctl))
            $ctl = $this->class;

        return $this::CTL_PATH . $ctl;
    }

    /**
     * Mendapatkan menu
     * @return array
     */
    protected function getMenu() {
        $menu = array();

        $menu[] = array('namamenu' => 'Portal', 'levelmenu' => 0, 'faicon' => 'desktop');
        $menu[] = array('namamenu' => 'Publik', 'namafile' => $this->getCTL('post'), 'levelmenu' => 1, 'faicon' => 'home');
        $menu[] = array('namamenu' => 'Pribadi', 'namafile' => $this->getCTL('post/me'), 'levelmenu' => 1, 'faicon' => 'user');
        $menu[] = array('namamenu' => 'Grup', 'namafile' => $this->getCTL('group/list_me'), 'levelmenu' => 1, 'faicon' => 'comments-o');

        $menu[] = array('namamenu' => 'Task', 'levelmenu' => 0, 'faicon' => 'line-chart');
        $menu[] = array('namamenu' => 'To Do', 'namafile' => $this->getCTL('todo/for_new'), 'levelmenu' => 1, 'faicon' => 'calendar');

        $menu[] = array('namamenu' => 'KM', 'namafile' => $this->getCTL('knowledge_management'), 'levelmenu' => 0, 'faicon' => 'home');

        $menu[] = array('namamenu' => 'Edit Profil', 'namafile' => $this->getCTL('user/me'), 'levelmenu' => 1, 'faicon' => 'user');
        if (SessionManagerWeb::isAdministrator() || SessionManagerWeb::isManagemement() || SessionManagerWeb::isSuperAdministrator())
            $menu[] = array('namamenu' => 'Pengguna', 'namafile' => $this->getCTL('user'), 'levelmenu' => 1, 'faicon' => 'users');

        return $menu;
    }

    /**
     * Redirect, biasanya setelah tambah atau hapus data
     */
    protected function redirect() {
        $back = $this->postData['referer'];
        if (empty($back))
            redirect($this->ctl);
        else
            header('Location: ' . $back);
    }

    /**
     * Menampilkan query lalu exit
     */
    protected function dbExit() {
        echo '<pre>';
        print_r(BenchmarkManager::query());
        echo '</pre>';
        exit;
    }

    /**
     * Halaman detail data
     * @param int $id
     */
    public function detail($id) {
        // tombol
        $buttons = array();
        $buttons['back'] = array('label' => 'Kembali', 'type' => 'primary', 'icon' => 'chevron-left', 'click' => 'goBack()');
        // $buttons['edit'] = array('label' => 'Edit', 'type' => 'success', 'icon' => 'pencil', 'click' => 'goEdit()');
        $buttons['delete'] = array('label' => 'Hapus', 'type' => 'danger', 'icon' => 'trash-o', 'click' => 'goDelete()');

        $this->data['id'] = $id;
        $this->data['buttons'] = $buttons;
        $this->data['title'] = 'Detail ' . $this->data['title'];
    }

    /**
     * Halaman tambah data
     */
    public function add() {
        $this->edit();
    }

    /**
     * Halaman edit data
     * @param int $id
     */
    public function edit($id = null) {
        // sementara disable edit, kecuali beberapa...
        if (isset($id) and ! in_array($this->class, array('group', 'issue', 'daily_report', 'user', 'knowledge_management')))
            redirect($this->getCTL() . '/detail/' . $id);

        $this->data['id'] = $id;

        // judul
        if (empty($this->data['title']))
            $this->data['title'] = (isset($id) ? 'Edit' : 'Tambah') . ' ' . $this->data['title'];

        // tombol
        if (empty($this->data['buttons'])) {
            $buttons = array();
            $buttons['back'] = array('label' => 'Kembali', 'type' => 'primary', 'icon' => 'chevron-left', 'click' => 'goBack()');
            $buttons['save'] = array('label' => 'Simpan', 'type' => 'success', 'icon' => 'save', 'click' => 'goSave()');

            $this->data['buttons'] = $buttons;
        }

        // bila add
        if ($this->method == 'add')
            $this->view = $this->class . '_edit';
    }

    protected function curl($url, $fields) {
        foreach($fields as $key=>$value) { $fields_string .= $key.'='.$value.'&'; }
        $fields_string = rtrim($fields_string,'&');
        
        if (!function_exists('curl_exec')) {
            # no curl
            die('ERROR: no curl');
        }
        

        $url = str_replace('103.56.190.35', 'localhost', $url);
        $ch = curl_init();
        curl_setopt($ch,CURLOPT_URL,$url);
        curl_setopt($ch, CURLOPT_HEADER, false);
        curl_setopt($ch, CURLOPT_BODY, true);
        //curl_setopt($ch, CURLOPT_FRESH_CONNECT, true);
        curl_setopt($ch, CURLOPT_TIMEOUT, 1);
        //curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 1);
        curl_setopt($ch,CURLOPT_POST,count($fields));
        curl_setopt($ch,CURLOPT_POSTFIELDS,$fields_string);
        curl_exec($ch);
        curl_close($ch);
    }

    public function toNoData($message=null) {
        //$this->view_items['content']  = $default_template.'error';

        $this->template->viewDefault('_error', $this->data);

        echo $this->output->get_output();
        exit;
    }

    function imageform($folder='posts/temp')  {
        $folder_ori = $folder;

        if ($_FILES['gambar']['name']) {
            $images_arr = array();

            $id = md5(SessionManagerWeb::getUserID() . $this->config->item('encryption_key'));

            $folder .= '/' . $id;

            $ciConfig = $this->config->item('utils');
            $path = $ciConfig['full_upload_dir'] . $folder . '/';
            if (!is_dir($path)) {
               mkdir($path);         
            }

            foreach($_FILES['gambar']['name'] as $key=>$val){

                $name = $_FILES['gambar']['name'][$key];

                $_FILES['userfile']['name']= $_FILES['gambar']['name'][$key];
                $_FILES['userfile']['type']= $_FILES['gambar']['type'][$key];
                $_FILES['userfile']['tmp_name']= $_FILES['gambar']['tmp_name'][$key];
                $_FILES['userfile']['error']= $_FILES['gambar']['error'][$key];
                $_FILES['userfile']['size']= $_FILES['gambar']['size'][$key];    


                $file_name = Image::getFileName($id, Image::IMAGE_ORIGINAL, $name);
                unlink($path . $file_name);
                
                $file_mime = Image::getMime($folder.$file_name);
                $image_mime = explode('/',$file_mime);
                if ($image_mime[0]=='image') {
                    Image::upload('userfile', $id, $name, $folder);

                    $link = Image::generateLink($id, $name, $folder);
                    
                    $images_arr[] = $link['thumb'];
                    $_SESSION['sigap']['draft_post'] = 1;
                }
                
            }  

            $str = $this->_getdraftimages($folder_ori);
            die($str);
        }    
        exit;
    }

    function fileform($folder='posts/temp')  {
        $folder_ori = $folder;

        if ($_FILES['file_upload']['name']) {
            $files_arr = array();

            $id = md5(SessionManagerWeb::getUserID() . $this->config->item('encryption_key'));

            $folder .= '/' . $id;
            $ciConfig = $this->config->item('utils');
            $path = $ciConfig['full_upload_dir'] . $folder . '/';
            if (!is_dir($path)) {
               mkdir($path);         
            }
            foreach($_FILES['file_upload']['name'] as $key=>$val){

                $name = $_FILES['file_upload']['name'][$key];
                
                $_FILES['userfile']['name']= $_FILES['file_upload']['name'][$key];
                $_FILES['userfile']['type']= $_FILES['file_upload']['type'][$key];
                $_FILES['userfile']['tmp_name']= $_FILES['file_upload']['tmp_name'][$key];
                $_FILES['userfile']['error']= $_FILES['file_upload']['error'][$key];
                $_FILES['userfile']['size']= $_FILES['file_upload']['size'][$key];

                $file_name = File::getFileName($id, $name);
                unlink($path . $file_name);

                File::uploadFile('userfile', $id, $name, $folder);
                $link = File::generateLink($id, $name, $folder);
                $files_arr = $link;
                
                $_SESSION['sigap']['draft_post'] = 1;
            }  

            $str = $this->_getdraftfiles($folder_ori);
            die($str);
        }    
        exit;
    }    

    function resetkirim($folder='posts/temp') {
        $id = md5(SessionManagerWeb::getUserID() . $this->config->item('encryption_key'));
        $folder .= '/' . $id;
        $ciConfig = $this->config->item('utils');
        $path = $ciConfig['full_upload_dir'] . $folder . '/';
        if (is_dir($path)) 
           $this->_deleteFiles($path);  

        unset($_SESSION['sigap']['draft_post']);
    }

    function getdraft($folder='posts/temp') {
        $str['images'] = $this->_getdraftimages($folder);
        $str['files'] = $this->_getdraftfiles($folder);
        die($str);
    }

    function _getdraftimages($folder='posts/temp') {

        $str = '';
        $id = md5(SessionManagerWeb::getUserID() . $this->config->item('encryption_key'));
        $folder .= '/' . $id;
        $ciConfig = $this->config->item('utils');
        $path = $ciConfig['full_upload_dir'] . $folder . '/';
        $rel_path = $ciConfig['upload_dir'] . $folder . '/';

        if (is_dir($path)) {
           $files = glob($path.'thumb*'); 
           foreach ($files as $file) {
               $image_src = base_url() . $rel_path . basename($file);
               $str .= "<img src=\"$image_src\" style=\"max-height:100px\"> ";
           }
        }
        return $str;
    }

    function _getdraftfiles($folder='posts/temp') {

        $str = '';
        $id = md5(SessionManagerWeb::getUserID() . $this->config->item('encryption_key'));
        $folder .= '/' . $id;
        $ciConfig = $this->config->item('utils');
        $path = $ciConfig['full_upload_dir'] . $folder . '/';
        $rel_path = $ciConfig['upload_dir'] . $folder . '/';
        if (is_dir($path)) {
           $files = glob($path.'*'); 
           foreach ($files as $file) {
               $file_src = basename($file);
               $file_name = explode('-',$file_src);
               $arr_file_ext = explode('.',$file_name[count($file_name)-1]);
               $file_ext = $arr_file_ext[count($arr_file_ext)-1];
               if ($file_ext=="pdf" || $file_ext=="doc" || $file_ext=="docx" || $file_ext=="xls" || $file_ext=="xlsx" || $file_ext=="ppt" || $file_ext=="pptx"){
                    $index = 0;
                    $name = '';
                    foreach ($file_name as $fname){
                        if ($index>0) {
                            $name .=$fname;
                            if ($index<count($file_name)-1) {
                                $name .='-';
                            }
                        }
                        $index++;
                    }
                    $str .= $name . "<br>";
               }
           }
        }
        return $str;
    }



    function _deleteFiles($path){
        if (is_dir($path) === true) {
            $files = array_diff(scandir($path), array('.', '..'));
            foreach ($files as $file) {
                $this->_deleteFiles(realpath($path) . '/' . $file);
            }
            return rmdir($path);
        }

        else if (is_file($path) === true) {
            return unlink($path);
        }

        return false;
    }

    function _moveFileTemp($post_id, $folder_source='posts/temp') {
        $id = md5(SessionManagerWeb::getUserID(). $this->config->item('encryption_key'));
        $folder_source .= '/'.$id;
        $ciConfig = $this->config->item('utils');
        $path_source = $ciConfig['full_upload_dir'] . $folder_source . '/';
        if (is_dir($path_source)) {
            $files = glob($path_source.'*'); 
            $arr_image = array();
            $arr_files = array();
            foreach ($files as $file) {
                list($mime,$ext) = explode('/',Image::getMime($file));
                $basename = basename($file);
                if ($mime=='image') {
                    $folder_dest = 'posts/photos';
                    $path_dest = $ciConfig['full_upload_dir'] . $folder_dest . '/';
                    $image = Image::getName($basename);
                    if (!in_array($image, $arr_image)) {
                        $arr_image[] = $image;
                        if (!$this->model->insertPostImage($post_id, $image, $file)) {
                            return false;
                        }
                    }
                } else {

                    $folder_dest = 'posts/files';
                    $path_dest = $ciConfig['full_upload_dir'] . $folder_dest . '/';
                    $fil = File::getName($basename);

                    if (!in_array($fil, $arr_files)) {
                        $arr_files[] = $fil;
                        if (!$this->model->insertPostFile($post_id, $fil, $file)) {
                            return false;
                        }
                    }
                }

                if (!rename($file, $path_dest . $basename)) {
                    return false;
                }
            }
        }
        else {
            return true;
        }
        return $this->_deleteFiles($path_source);
    }

    function getimages($post_id) {
        $post_id = (int) $post_id;

        $sql = "select user_id from posts where id=$post_id";
        $user_id = dbGetOne($sql);

        $sql = "select * from post_image where post_id=$post_id";
        $rows = dbGetRows($sql);
        $str = '';
        $id_image = md5($user_id . $this->config->item('encryption_key'));
        foreach ($rows as $row) {
            $thumb = Image::createLink($id_image, Image::IMAGE_THUMB, $row['image'], 'posts/photos');
            $image_id = $row['id'];   
            $str .= "<div style=\"display:inline-block;\"><img src=\"{$thumb}\" style=\"max-height:100px\"/><span class=\"btn btn-group btn-xs\" style=\"position:relative;left:-24px;top:-38px;background-color:black\" onClick=\"deleteimage({$post_id},{$image_id})\"><i class=\"glyphicon glyphicon-remove\" style=\"color:white\"></i></span></div>";
        }
        echo $str;
        die();
    }

    function getgroupname($id) {
        $id = (int) $id;
        if ($id==0) 
            die('Helpdesk');
        if ($id==-1)
            die("Group");
        $sql = "select name from groups where id=$id";
        $name= dbGetOne($sql);
        die($name);
    }

    function deleteimage($post_id,$image_id) {
        $post_id = (int) $post_id;

        $sql = "delete from post_image where post_id=$post_id and id=$image_id";
        $query = dbQuery($sql);
        $this->getimages($post_id);
    }

    function getfiles($post_id) {
        $post_id = (int) $post_id;
        $sql = "select user_id from posts where id=$post_id";
        $user_id = dbGetOne($sql);

        $sql = "select * from post_file where post_id=$post_id";
        $rows = dbGetRows($sql);
        $str = '';
        $id_file = md5($user_id . $this->config->item('encryption_key'));

        $ciConfig = $this->config->item('utils');
        foreach ($rows as $row) {
            $link = File::createLink($id_file, $row['file'], 'posts/files');
            $file_id = $row['id'];
            $file_name = explode('-',basename($link));
            $index=0;
            $file="";
            foreach ($file_name as $fname){
                if ($index>0) {
                    $file .=$fname;
                    if ($index<count($file_name)-1) {
                        $file .='-';
                    }
                }
                $index++;
            }
            $str .= "<div style=\"background-color: #f5f5f5;border-style:solid;border-width:1px;border-color: black;margin-top:1px\"><i class=\"glyphicon glyphicon-file\" style=\"color:black\"></i> <strong>".$file ."</strong><span class=\"btn btn-group btn-xs\" style=\"position:relative;float:right\" onClick=\"deletefile({$post_id},{$file_id})\"><i class=\"glyphicon glyphicon-remove\" style=\"color:black\"></i></span></div>";
        }
        echo $str;
        die();
    }
    
    function deletefile($post_id,$file_id) {
        $post_id = (int) $post_id;

        $sql = "delete from post_file where post_id=$post_id and id=$file_id";
        $query = dbQuery($sql);
        $this->getfiles($post_id);
        
    }

    function getusername($user_id){
        $sql = "select name from users where id='$user_id'";
        $name= dbGetOne($sql);
        die($name);
    }

    function getassigns($post_id){
        $sql = "select user_id from post_assign where post_id=$post_id";
        $assigns= dbGetRows($sql);
        $str = '';
        foreach ($assigns as $assign) {
            $str.=$assign['user_id'].' ';
        }
        die($str);
    }

    function getstatuspost($post_id){
        $sql = "select status from posts where id=$post_id";
        $status= dbGetOne($sql);
        die($status);
    }

    function _isAllowViewPost($id, $output=true) {
        if (SessionManagerWeb::isAdministrator() || SessionManagerWeb::isSuperAdministrator()){
            return true;
        }
        $id = (int) $id;
        $sql = "select * from posts where id=$id and status!='D'";
        $post = dbGetRow($sql);

        $user_id = SessionManagerWeb::getUserID();

        if ($post['type'] == 'T') {
            if (SessionManagerWeb::isStaff())
                return true;

            // echo '<pre>';
            // var_dump($post);
            // echo '</pre>';
            // die();
            $users[] = "'".$user_id."'";
            if($this->Group_member_model->isAdminKopertis($user_id)){
                $users = $this->User_model->getListMemberKopertis($user_id);
                $users = array_map(function($value){return "'".$value."'";}, $users);
                $users[] = "'".$user_id."'";
            }

            $sql = "select 1 from post_assign pa
                    left join posts p on p.id=pa.post_id
                    where pa.post_id=$id and (pa.user_id ='$user_id' or p.user_id in (".implode(',', $users)."))";
            if (dbGetOne($sql))
                return true;

            if ($post['user_id'] == $user_id)
                return true;
        }
        else if ($post['type'] == 'G') {
            $group_id = $post['group_id'];
            $sql = "select 1 from group_members where group_id=$group_id and user_id='$user_id'";
            if (dbGetOne($sql))
                return true;
        }
        else {
            return true;
        }

        if ($output) {
            $this->template->viewDefault('_error', $this->data);
            echo $this->output->get_output();
            exit;
        }

        return false;
    }

    function isAllowDeleteComment($id){
        if (SessionManagerWeb::isAdministrator() || SessionManagerWeb::isSuperAdministrator())
            return true;

        //apakah admin dari tempat post tersebut
        $group_id = (int) $_SESSION['group_selected'];
        $user_id = SessionManagerWeb::getUserID();
        $sql = "select 1 from group_members where group_id=$group_id and user_id='$user_id' and is_admin=1";
        if (dbGetOne($sql))
            return true;
        return false;
    }

    function isShowBtnEditDelete($id, $group_id=NULL){
        if ( SessionManagerWeb::isSuperAdministrator())
            return true;

        $id = (int) $id;
        $sql = "select * from posts where id=$id and status!='D'";
        $post = dbGetRow($sql);

        $user_id = SessionManagerWeb::getUserID();
        // echo '<pre>';
        // var_dump($post);
        // echo '</pre>';
        // die();
        if ($post['type'] == 'T') {
            if (SessionManagerWeb::isAdministrator()){
                $helpdesk_group = $this->Group_member_model->getMyHelpdeskGroup(SessionManagerWeb::getUserID());
                if ($post['group_id']==$helpdesk_group['id'])
                    return true;
            }  
            return false;
        }
        else if ($post['type'] == 'G') {
            $group_id = (int) $_SESSION['group_selected'];
            $group_id=(int)$group_id;
            $sql = "select 1 from group_members where group_id=$group_id and user_id='$user_id' and is_admin=1";
            if (dbGetOne($sql)){
                return true;
            }
        }

        if ($post['user_id'] == $user_id){
            return true;
        }

        return false;
    }

    function isAllowEditDeletePost($id, $output=true) {
        if (SessionManagerWeb::isAdministrator() || SessionManagerWeb::isSuperAdministrator())
            return true;

        $id = (int) $id;
        $sql = "select * from posts where id=$id and status!='D'";
        $post = dbGetRow($sql);

        $user_id = SessionManagerWeb::getUserID();
        if ($post['type'] == 'G') {
            if ($post['user_id'] == $user_id or $this->_isAdminThisGroup()){
                return true;
            }
        } else {
            if ($post['user_id'] == $user_id){
                return true;
            }
        }

        if ($output) {
            $this->template->viewDefault('_error', $this->data);
            echo $this->output->get_output();
            exit;
        }
        
        return false;
    }

    function _isAllowKirim($group_id) {
        if (SessionManagerWeb::isAdministrator() || SessionManagerWeb::isSuperAdministrator()){
            return true;
        }
        $group_id = (int) $_SESSION['group_selected'];
        if ($group_id==1)
            return false;
        $user_id = SessionManagerWeb::getUserID();
        $sql = "select 1 from group_members where group_id=$group_id and user_id='$user_id'";
        if (dbGetOne($sql))
            return true;
        return false;
    }

    public function _isAdminThisGroup($group_id) {
        $group_id = (int) $_SESSION['group_selected'];
        $user_id = SessionManagerWeb::getUserID();
        $sql = "select 1 from group_members where group_id=$group_id and user_id='$user_id' and is_admin=1";
        if (dbGetOne($sql))
            return true;
        return false;
    }

    function _isStaffGroup(){
        $this->load->model('User_model', 'User');
        $list_staff_kopertis = $this->User->getListStaffKopertis();
        foreach($list_staff_kopertis as $staff){
            if ($staff['id']==SessionManagerWeb::getUserID()){
                return true;
            }
        }
        return false;
    }

    function _isAllowKirimSebagai(){
        if (SessionManagerWeb::isStaff()) {
            return true;
        } 
        return $this->_isStaffGroup();
    }

    function _isAllowTugaskan ($post_id){
        if (SessionManagerWeb::isSuperAdministrator()){
            return TRUE;
        }
        $sql = "select * from posts where id=$post_id";
        $post = dbGetRow($sql);
        if (SessionManagerWeb::isAdministrator()){
            $group_default = $this->Group_model->getGroupDefault();
            $my_helpdesk_group = $this->Group_member_model->getMyHelpdeskGroup(SessionManagerWeb::getUserID());
            if ($post['group_id']==$my_helpdesk_group['id'])
                return true;
            if ($post['group_id']==NULL and $my_helpdesk_group['id']==$group_default['id'])
                return true;
        }
        
        if ($post['status']=='S')
            return false;
        if (SessionManagerWeb::isStaff()){
            $group_default = $this->Group_model->getGroupDefault();
            $my_helpdesk_group = $this->Group_member_model->getMyHelpdeskGroup(SessionManagerWeb::getUserID());
            if ($post['group_id']==$my_helpdesk_group['id'])
                return true;
            if ($post['group_id']==NULL and $my_helpdesk_group['id']==$group_default['id'])
                return true;
        }
        return FALSE;
    }

    function _isAllowPindahkan($post_id){
        if (SessionManagerWeb::isSuperAdministrator()){
            return TRUE;
        }
        $sql = "select * from posts where id=$post_id";
        $post = dbGetRow($sql);
        if (SessionManagerWeb::isAdministrator()){
            $group_default = $this->Group_model->getGroupDefault();
            $my_helpdesk_group = $this->Group_member_model->getMyHelpdeskGroup(SessionManagerWeb::getUserID());
            if ($post['group_id']==$my_helpdesk_group['id'])
                return true;
            if ($post['group_id']==NULL and $my_helpdesk_group['id']==$group_default['id'])
                return true;
        }
        
        // if ($post['status']=='S')
        //     return false;

        if (SessionManagerWeb::isStaff()){
            $user_id = SessionManagerWeb::getUserID();
            $sql = "select 1 from post_assign pa where pa.post_id=$post_id and pa.user_id='$user_id'";
            if (dbGetOne($sql))
                return true;
        }
        return FALSE;
    }

    function _isAllowSalin($post_id){
        if (SessionManagerWeb::isSuperAdministrator()){
            return TRUE;
        }
        $sql = "select * from posts where id=$post_id";
        $post = dbGetRow($sql);
        if (SessionManagerWeb::isAdministrator()){
            $group_default = $this->Group_model->getGroupDefault();
            $my_helpdesk_group = $this->Group_member_model->getMyHelpdeskGroup(SessionManagerWeb::getUserID());
            if ($post['group_id']==$my_helpdesk_group['id'])
                return true;
            if ($post['group_id']==NULL and $my_helpdesk_group['id']==$group_default['id'])
                return true;
        }
        
        // if ($post['status']=='S')
        //     return false;

        if (SessionManagerWeb::isStaff()){
            $user_id = SessionManagerWeb::getUserID();
            $sql = "select 1 from post_assign pa where pa.post_id=$post_id and pa.user_id='$user_id'";
            if (dbGetOne($sql))
                return true;
        }
        return FALSE;
    }

    function isAllowSetStatus($post_id){
        if (SessionManagerWeb::isSuperAdministrator()){
            return TRUE;
        }
        
        $post_id=(int)$post_id;
        $user_id=SessionManagerWeb::getUserID();
        $sql = "select * from posts where id=$post_id";
        $post = dbGetRow($sql);
        if (SessionManagerWeb::isAdministrator()){
            $group_default = $this->Group_model->getGroupDefault();
            $my_helpdesk_group = $this->Group_member_model->getMyHelpdeskGroup(SessionManagerWeb::getUserID());
            if ($post['group_id']==$my_helpdesk_group['id'])
                return true;
            if ($post['group_id']==NULL and $my_helpdesk_group['id']==$group_default['id'])
                return true;
        }
        if ($post['status']=='S')
            return false;
        $sql_staff = "select 1 from post_assign where post_id=$post_id and user_id='$user_id'";
        // die($sql_staff);
        // if (SessionManagerWeb::isStaff()){
            if (dbGetOne($sql_staff))
                return true;
            // else
            //     return false;
        // }
        // $sql_admin_group = "select 1 from group_members where user_id='$user_id' and is_admin=1";
        // if (dbGetOne($sql_admin_group)){
        //     if (dbGetOne($sql))
        //         return true;
        //     else
        //         return false;
        // } 
        return false;
    }

    function isAlreadyRatingKnowledge($post_id){
        $user_id = SessionManagerWeb::getUserID();
        $sql = "select 1 from post_rating where post_id=$post_id and user_id='$user_id'";
        if (dbGetOne($sql))
            return true;
        return false;
    }

    function isNotRatingYet(){
        $user_id = SessionManagerWeb::getUserID();
        $sql = "select 1 from posts p
                left join post_rating pr on p.id=pr.post_id
                where p.user_id='$user_id' and p.type='T' 
                and p.status='S' and pr.rating is null";
        if (dbGetOne($sql))
            return true;
        return false;
    }

    /*Get url user from description
    * Identified by @
    */
    public function checkURL($text){
        $lines = explode("\n", $text);
        // $reg_exUrl = '/((?:[\w\d]+\:\/\/)?(?:[\w\-\d]+\.)+[\w\-\d]+(?:\/[\w\-\d]+)*(?:\/|\.[\w\-\d]+)?(?:\?[\w\-\d]+\=[\w\-\d]+\&?)?(?:\#[\w\-\d]*)?)/';
        $reg_exUrl='/\b(?:(?:https?|ftp):\/\/|www\.)[-a-z0-9+&@#\/%?=~_|!:,.;]*[-a-z0-9+&@#\/%=~_|]/i ';
        foreach ($lines as $linekey => $line) {
            $words = explode(' ', $line);
            foreach ($words as $wordkey => $word) {
                /*Check if url*/
                $word = trim($word);
                if(filter_var($word, FILTER_VALIDATE_URL)){
                    $words[$wordkey] = "<a href='$word'>$word</a>";
                } else if(preg_match($reg_exUrl, $word)) {
                    $words[$wordkey] = "<a href='http://$word'>$word</a>";
                }
            }
            $lines[$linekey] = implode(' ', $words);
        }

        $text = implode("\n", $lines);

        return $text;
    }

    //String 2 lebih pendek dari string 1
    public function getMatchedString($first_string, $second_string){
        echo $first_string;
        echo $second_string;
        $str='';
        $position = stripos($first_string,$second_string);
        if (!empty($position)) {
            $str = substr($first_string, $position,strlen($second_string));
        } else {
            $str = $second_string;
        }
        return $str;
    }

    public function setSessionHelpdeskSelected(){
        $data = $this->input->post(null,true);
        $_SESSION['helpdesk_selected_id'] = $data['id'];
        $_SESSION['helpdesk_selected_name'] = $data['name'];
        $nama = $data['name'];
        $_SESSION['helpdesk_selected_description'] = $data['description'];
        $_SESSION['allow_kirim_sebagai'] = $this->_isAllowKirimSebagai(); 
        unset($_SESSION['group_selected_id']);
        unset($_SESSION['group_selected_name']);
        die("<strong>
            Anda akan mengirim pesan ke <i>Helpdesk $nama</i>.
        </strong>");
    }

    public function setSessionGroupSelected(){
        $data = $this->input->post(null,true);
        $_SESSION['group_selected_id'] = $data['id'];
        $_SESSION['group_selected_name'] = $data['name'];
        $nama = $data['name'];
        unset($_SESSION['allow_kirim_sebagai']);
        unset($_SESSION['helpdesk_selected_id']);
        unset($_SESSION['helpdesk_selected_name']);
        unset($_SESSION['helpdesk_selected_description']);
        die("<strong>
            Anda akan mengirim pesan ke <i>Group $nama</i>.
        </strong>");
    }
}
