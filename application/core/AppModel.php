<?php

(defined('BASEPATH')) OR exit('No direct script access allowed');

class AppModel extends BehaviorModel {

    /**
     * Initialize Behaviour Model 
     */
    protected $return_type = 'array';
    protected $protected_attributes = array('hash');
    protected $before_create = array('created_at', 'updated_at');
    protected $before_update = array('updated_at');
    protected $soft_data = NULL;
    protected $mapper = array();
    protected $label = array();
    protected $validation = array();
    protected $after_get = array('mapper', 'property');
    protected $before_get = array('join_or_where');
    protected $show_all = FALSE;
    protected $fields = array();

    public function __construct() {
        parent::__construct();

        $this->setLabel();

        $this->setValidation();

        $this->setField();
    }

    protected function setField() {
        
    }

    /*Get url user from description
    * Identified by @
    */
    public function checkURL($text){
        $lines = explode("\n", $text);
        // $reg_exUrl = '/((?:[\w\d]+\:\/\/)?(?:[\w\-\d]+\.)+[\w\-\d]+(?:\/[\w\-\d]+)*(?:\/|\.[\w\-\d]+)?(?:\?[\w\-\d]+\=[\w\-\d]+\&?)?(?:\#[\w\-\d]*)?)/';
        $reg_exUrl='/\b(?:(?:https?|ftp):\/\/|www\.)[-a-z0-9+&@#\/%?=~_|!:,.;]*[-a-z0-9+&@#\/%=~_|]/i ';
        foreach ($lines as $linekey => $line) {
            $words = explode(' ', $line);
            foreach ($words as $wordkey => $word) {
                /*Check if url*/
                $word = trim($word);
                if(filter_var($word, FILTER_VALIDATE_URL)){
                    $words[$wordkey] = "<a href='$word' target='_blank'>$word</a>";
                } else if(preg_match($reg_exUrl, $word)) {
                    $words[$wordkey] = "<a href='http://$word' target='_blank'>$word</a>";
                }
            }
            $lines[$linekey] = implode(' ', $words);
        }

        $text = implode("\n", $lines);

        return $text;
    }

    public function getYoutubeURL($text){
        preg_match_all('/<a[^>]+href=([\'"])(?<href>.+?)\1[^>]*>/i', $text, $result);
        $youtube_link;

        if (!empty($result)) {
            foreach ($result['href'] as $key => $link) {
                if(strpos($link, 'www.youtube.com')){
                    $youtube_link['full_link'] = $link;
                    $youtube_link['id'] = substr($link, strpos($link, 'v=')+2);
                }
            }
        }

        return $youtube_link;
    }

    protected function setLabel() {
        $label = array();
        foreach ($this->label as $key => $val) {
            if (is_numeric($key)) {
                $label[$val] = humanize($val);
            } else {
                $label[$key] = $val;
            }
        }
        $this->label = $label;
    }

    public function getLabel($field) {
        return $this->label[$field];
    }

    protected function setValidation() {
        foreach ($this->validation as $key => $val) {
            $this->validate[] = array(
                'field' => $key,
                'label' => (isset($this->label[$key]) ? $this->label[$key] : humanize($key)),
                'rules' => $val
            );
        }
    }

    public function getErrorValidate() {
        $error = array();
        foreach ($this->validation as $key => $val) {
            $err = form_error($key);
            if (!empty($err)) {
                $error[camelize($key)] = $err;
            }
        }
        return $error;
    }

    public function getErrorManualValidation($fields = array()) {
        $error = array();
        foreach ($fields as $field) {
            $err = form_error($field);
            if (!empty($err)) {
                $error[camelize($field)] = $err;
            }
        }
        return $error;
    }

    public function mapper($row) {
        $newRow = array();
        foreach ($row as $key => $val) {
            //MAPPER ATRIBUTE
            if (is_null($this->soft_data) || in_array($key, $this->soft_data) || $this->show_all) {
                $newKey = key_exists($key, $this->mapper) ? $this->mapper[$key] : camelize($key);
                $newRow[$newKey] = $val;
            }
        }
        return $newRow;
    }

    public function getClassName() {
        return str_replace(' ', '', humanize(singular(preg_replace('/(_m|_model)?$/', '', get_class($this)))));
    }

    public function setFieldDB($data = array(), $fields = array()) {
        if (empty($fields)) {
            if (empty($this->fields)) {
                $fields = $this->_database->list_fields($this->_table);
            } else {
                $fields = $this->fields;
            }
        }
// echo '<pre>';
//                     var_dump($fields);
//                     echo '</pre>';
//                     die();
        $mapperData = array();
        foreach ($data as $field => $value) {
            $field = Util::camelizeToUnderscore($field);

            if (in_array($field, $fields)) {
                $mapperData[$field] = $value;
            }
        }
        return $mapperData;
    }

    public function create($data, $skip_validation = FALSE, $return = TRUE) {
        return $this->insert($this->setFieldDB($data), $skip_validation, $return);
    }

    public function defaultCreate($data, $skip_validation = FALSE, $return = TRUE) {
        return $this->insert($this->setFieldDB($data), $skip_validation, $return);
    }

    public function save($primary_value, $data = array(), $skip_validation = FALSE) {
//        foreach ($this->validate as $key => $validate) {
//            $this->validate[$key]['rules'] = str_replace('is_unique', 'is_unique_exclude', $validate['field']);
//        }
        return $this->update($primary_value, $this->setFieldDB($data), $skip_validation);
    }

    protected function property($row) {
        return $row;
    }

    protected function join_or_where($row) {
        return $row;
    }

    public function show_all($show_all = FALSE) {
        $this->show_all = $show_all;
        return $this;
    }

    public function validateRequired($data, $validation = array()) {
        foreach ($validation as $key => $val) {
            if (!isset($data[$key]) || (isset($data[$key]) && empty($data[$key]))) {
                return $val;
            }
        }
        return TRUE;
    }

}
