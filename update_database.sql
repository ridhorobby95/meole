CREATE TABLE public.holidays
(
  id integer NOT NULL DEFAULT nextval('holidays_id_seq'::regclass),
  name character varying(255),
  date timestamp without time zone,
  description character varying(255),
  created_at timestamp without time zone,
  updated_at timestamp without time zone,
  created_by integer,
  updated_by integer
)
WITH (
  OIDS=FALSE
);

ALTER TABLE public.notification_users ADD COLUMN group_id integer;
ALTER TABLE public.notification_senders ADD COLUMN group_id integer;


## START ##
ALTER TABLE public.groups ADD COLUMN is_delete numeric(1,0) DEFAULT 0;
## END ##