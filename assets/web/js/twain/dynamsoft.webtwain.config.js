﻿//
// Dynamsoft JavaScript Library for Basic Initiation of Dynamic Web TWAIN
// More info on DWT: http://www.dynamsoft.com/Products/WebTWAIN_Overview.aspx
//
// Copyright 2016, Dynamsoft Corporation 
// Author: Dynamsoft Team
// Version: 12.1
//
/// <reference path="dynamsoft.webtwain.initiate.js" />
var Dynamsoft = Dynamsoft || { WebTwainEnv: {} };

Dynamsoft.WebTwainEnv.AutoLoad = true;
///
Dynamsoft.WebTwainEnv.Containers = [{ContainerId:'dwtcontrolContainer', Width:100, Height:100}];
///
Dynamsoft.WebTwainEnv.ProductKey = '31EBE98C00258005A802B6B4D6565925DB5CFBC7600E66FE1F4D89CFC7FE6CBB74BFCCDE826A85F0072251D92A825F8E3260CE2CCA3CFD85F5F8E6CB785C3F5E0BFA140B70492680D7A41FCBAB8E84774763460C6ED29F04E4756094847769FF31E25F227FC4BDF6DD08BAC7D496785DCCDA6F0F5665249AB52544ABFBE83325FFC6ABDEBD57D29317F2FDE9538BF48C';
///
Dynamsoft.WebTwainEnv.Trial = false;
///
Dynamsoft.WebTwainEnv.ActiveXInstallWithCAB = false;
///
Dynamsoft.WebTwainEnv.ResourcesPath = 'http://localhost/pustekkom/application/assets/css';

/// All callbacks are defined in the dynamsoft.webtwain.install.js file, you can customize them.

// Dynamsoft.WebTwainEnv.RegisterEvent('OnWebTwainReady', function(){
// 		// webtwain has been inited
// });

