var pop_windows = new Array();

function pushPopWindow(block_id) {
	//if ($("#"+block_id).css('display') != 'block') {
		pop_windows.push(block_id);
		zindex = pop_windows.length;
		$("#"+block_id).css('z-index',11000+zindex);

	//}
}

function closePop() {
	block_id = pop_windows.pop();
	if (block_id)
		$("#"+block_id).hide();
}

function openLOV(button_id, act, posx, posy, ret_function, width, height) {
	var button = document.getElementById(button_id);
	var block_id = "sevimalov_block";
	var block = document.getElementById(block_id);

	if (!posx)
		posx = 0;
	if (!posy)
		posy = 0;
	if (!width)
		width = '500px';
	if (!height)
		height = '100px';

	pushPopWindow(block_id);
	
	$('#sevimalov_div').html('<div style="height:100px;padding:20xp"><span style="color:white;background-color:#cc0000;padding:2px">Loading...</span></div>');
    
	ajaxurl = g_abs_url + "ajax/"+act;

	$.ajax({
		type: "POST",
		url: ajaxurl,
		data: {"ret_function":ret_function},
		success: function(ret){
			$('#sevimalov_div').html(ret);
		}
	});

	$("#"+block_id).centerLOV();
	$("#"+block_id).show();	
}

function openLOV2(act, html_input_id, lov_id, ret_function, remove_function, init) {
	var block_id = "sevimalov_block";

	pushPopWindow(block_id);
	
	$('#sevimalov_div').html('<span style="color:white;background-color:#cc0000">Loading...</span>');
    
	ajaxurl = g_abs_url + "ajax/"+act;
	
	if (!init)
		init = 0;

	$.ajax({
		type: "POST",
		url: ajaxurl,
		data: { 
				"init":init,
				"html_input_id":html_input_id,
				"lov_id":lov_id,
				"ret_function":ret_function,
				"remove_function":remove_function
				},
		success: function(ret){
			$('#sevimalov_div').html(ret);
		}
	});

	$("#"+block_id).centerLOV();
	$("#"+block_id).show();	
}

$(window).resize(function(){
	obj = '#sevimalov_block';

	var offset = 0;//$(document).scrollTop()
	var topPos = offset - ($(window).height() - $(obj).outerHeight())/2;

	$(obj).css({
		left: ($(window).width() - $(obj).outerWidth())/2,
		top: topPos > 0 ? topPos : 0
	});
});

function searchLOV(act){
	ajaxurl = g_abs_url + "ajax/"+act;
	$.ajax({
		type: "POST",
		url: ajaxurl,
		data: {"entry":1, "keyword":$("#sevimalov_keyword").val()},
		success: function(ret){
			$('#sevimalov_div').html(ret);
		}
	});
}

function searchLOV2(act){
	ajaxurl = g_abs_url + "ajax/"+act;
	$.ajax({
		type: "POST",
		url: ajaxurl,
		data: {"entry":1, "searchlov2":1, "keyword":$("#sevimalov_keyword").val()},
		success: function(ret){
			$('#sevimalov_div2').html(ret);
		}
	});
}

function navigateLOV(act, nav, extra) {
	ajaxurl = g_abs_url + "ajax/"+nav+"/act/"+act;
	if (extra)
		ajaxurl += "/extra/"+extra;
	$.ajax({
		type: "POST",
		url: ajaxurl,
		success: function(ret){
			$('#sevimalov_div').html(ret);
		}
	});
}

function resetLOV(act){
	ajaxurl = g_abs_url + "ajax/reset/act/"+act;
	$.ajax({
		type: "POST",
		url: ajaxurl,
		success: function(ret){
		}
	});
}

function generateSevimaBlock() {
	jQuery("<div />")
		.attr("id", "sevimalov_block")
		.attr("class", "section-main col-md-4 col-md-offset-4 col-xs-6 col-xs-offset-3 draggable modal-dialog modal-content")
		.attr("style", "z-index:100000;position:fixed;font-size:small;display:none;background-color:#efefef")
		.appendTo(jQuery("body"));

	$('#sevimalov_block').append('<div id="sevimalov_div"></div>');
}

$(document).ready(function() {
	generateSevimaBlock();
	$(function() {
		$(".draggable" ).draggable({ handle: ".drag_handle" })
	});
});

jQuery.fn.centerLOV = function(parent) {
    if (parent) {
        parent = this.parent();
    } else {
        parent = window;
    }
    this.css({
        "position": "absolute",
        "top": (((($(parent).height() - this.outerHeight()) / 2) - 200) + $(parent).scrollTop() + "px"),
        "left": ((($(parent).width() - this.outerWidth()) / 2) + $(parent).scrollLeft() + "px")
    });
return this;
}
