var btnclick;

$(function() {
	// load scroll
	$(window).scrollTop(localStorage.getItem("scroll"));
	localStorage.removeItem("scroll");
	
	// number
	var decimal, separator;
	$(".number").each(function() {
		decimal = $(this).attr("data-decimal");
		if(decimal)
			decimal = parseInt(decimal);
		else
			decimal = 0;
		
		separator = $(this).attr("data-separator");
		if(separator)
			separator = '.';
		else
			separator = '';
		
		if($(this).val())
			$(this).val($.number($(this).val(),decimal,',',separator));
		else if($(this).html())
			$(this).html($.number($(this).html(),decimal,',',separator));
		
		if($(this).is("input,textarea"))
			$(this).number(true,decimal,',',separator);
	});
	
	$(".number[data-max]").keyup(function() {
		var max = parseFloat($(this).attr("data-max"));
		if($(this).val() > max)
			$(this).val(max);
	});
	
    // tanggal
    $('.datepicker').datepicker({
        format: 'dd-mm-yyyy'
    });
    
    // autocomplete
    $("[data-cari]").not(".tt-hint").each(function() {
		var inkey;
        var input = $(this);
		var parent = input.parent();
		var form = input.parents("form:eq(0)");
		var param = input.attr("data-param");
		
		// cari key
		inkey = parent.find("[id='" + input.attr("data-inkey") + "']");
		if(inkey.length == 0)
			inkey = form.find("[id='" + input.attr("data-inkey") + "']");
		
		// buat url
		var url = input.attr("data-cari");
        
		// diberi typeahead
		if(input.is("input")) {
			input.typeahead({
				highlight: true,
				minLength: 3
			},{
				source: function (query, process) {
					return $.getJSON(g_abs_url + "ajax/" + url + getURLParam(parent,form,param) + "/" + query, function (data) {
						return process(data.options);
					});
				}
			}).on('typeahead:queryChanged', function() {
				inkey.val("");
			}).on('typeahead:selected', function(ev,obj,ds) {
				inkey.val(obj.key);
			}).on('typeahead:autocompleted', function(ev,obj,ds) {
				inkey.val(obj.key);
			}).on('typeahead:blurred',function() {
				if(!inkey.val())
					$(this).val("");
			});
		}
		
		// jika sudah ada datanya
		if(input.attr("data-key")) {
            xhrfGetStr(url + getURLParam(parent,form,param) + "/" + input.attr("data-key") + "/1", function(data) {
				if(input.is("input"))
                    input.typeahead("val",data);
                else
                    input.html(data);
            });
        }
    });
	
	// disable button jika diclick
	$("button:not(.close)").bind("click",function() {
		btnclick = $(this);
	});
	
	// upload
	$(".input-foto").on('change', function() {
		goSubmit(this);
	});
});

function getURLParam(parent,form,param) {
	if(param) {
		var inparam;
		var url = "";
		var arrdata = new Array();
		var arrparam = param.split("/");
		
		for(i=0;i<arrparam.length;i++) {
			inparam = parent.find("[id='" + arrparam[i] + "']");
			if(inparam.length == 0)
				inparam = form.find("[id='" + arrparam[i] + "']");
			if(inparam.length > 0)
				arrdata[i] = inparam.val();
		}
		
		return "/" + arrdata.join("/");
	}
	else
		return ""; // string kosong
}

function getForm(elem) {
	elem = $(elem);
	
    if(elem.is("form"))
        return elem;
    else
        return elem.parents("form").eq(0);
}

function cekRequired(elem,tag) {
	var error = false;
	var target;
	
	if(tag)
		target = $(elem).parents(tag).eq(0);
	else
		target = getForm(elem);
	
	target.find(".required:not([readonly])").each(function() {
		if($(this).val().length == 0) {
			if(!error) error = $(this);
			$(this).parent().addClass("has-error");
		}
		else
			$(this).parent().removeClass("has-error");
	});
	
	if(error) {
		alert("Mohon mengisi isian yang bergaris merah");
		error.focus();
		
		return false;
	}
	else
		return true;
}

function uploadFotoProfil() {
	$("#form_profil .input-foto").trigger('click');
}

function goSubmit(elem,act) {
	// simpan nilai scroll dulu
	localStorage.setItem("scroll",$(window).scrollTop());
	
	elem = $(elem); // diperlukan di bawah
	var form = getForm(elem);
    
    if(act) form.find("#act").val(act);
	
	// cek format laporan
	if($("#format").length == 0 || $("#format").val() == "html") {
		if(elem.is("button") && !elem.is("[data-active]")) elem.prop("disabled",true);
		else if(btnclick && !btnclick.is("[data-active]")) btnclick.prop("disabled",true);
	}
    
    form.submit();
}

function goSubmitBlank(elem,page) {
    var form = getForm(elem);
	var action = form.attr("action");
	var target = form.attr("target");
    
    if(page) form.attr("action",g_abs_url + page);
    
    form.attr("target","_blank");
    form.submit();
	
	if(action)
		form.attr("action",action);
	else
		form.removeAttr("action");
	
	if(target)
		form.attr("target",target);
	else
		form.removeAttr("target");
}

$.expr[':'].iContains = function(n,i,m) {
	return jQuery(n).text().toUpperCase().indexOf(m[3].toUpperCase()) >= 0;
};