var formlist;

$(document).ready(function() {
	formlist = $("#form_list");
	
    $("[data-type='edit']").click(function() {
		if(typeof(list) != "undefined")
			sessionStorage.setItem(detpage + ".list",list);
		
		location.href = detail + "/detail/" + $(this).attr("data-id");
	});
	$("[data-type='delete']").click(function() {
		if(confirm("Apakah anda yakin akan menghapus data ini?")) {
			formlist.find("#key").val($(this).attr("data-id"));
			goSubmit(this,"delete");
		}
	});
    
    // inplace (master)
    $("[data-type='undoip']").click(function() {
		location.href = location.href;
	});
    $("[data-type='editip']").click(function() {
		formlist.find("#key").val($(this).attr("data-id"));
		goSubmit(formlist,"edit");
	});
	$("[data-type='deleteip']").click(function() {
		var hapus = confirm("Apakah anda yakin akan menghapus data ini?");
		
		if(hapus) {
			formlist.find("#key").val($(this).attr("data-id"));
			goSubmit(formlist,"delete");
		}
	});
	$("[data-type='updateip']").click(function() {
		if(cekRequired(this,"tr")) {
			formlist.find("#key").val($(this).attr("data-id"));
			goSubmit(formlist,"update");
		}
	});
	$("[data-type='insertip']").click(function() {
		if(cekRequired(this,"tr"))
			goSubmit(formlist,"insert");
	});
    
    // sorting
	$("th[data-kolom]").css("cursor","pointer").click(function() {
		formlist.find("#key").val($(this).attr("data-kolom"));
		goSubmit(formlist,"sort");
	});
    
    // jumlah baris per halaman
    $("[name='example1_length']").change(function() {
        goSubmit(this);
    });
});

function goPage(page) {
    // simpan nilai scroll dulu
	localStorage.setItem("scroll",$(window).scrollTop());
    
    location.href = g_page + "/" + page;
}

function goFilter(col,val) {
    $("#form_filter #key").val(col + ":" + val);
    
    goSubmit($("#form_filter").get(0),"addfilter");
}

function goRemoveFilter(col,idx) {
    $("#form_filter #key").val(col + ":" + idx);
    
    goSubmit($("#form_filter").get(0),"removefilter");
}