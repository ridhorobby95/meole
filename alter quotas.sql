﻿CREATE TABLE public.post_quotas
(
  id serial NOT NULL,
  user_id character varying(255),
  group_id integer,
  quota integer,
  created_at timestamp without time zone,
  updated_at timestamp without time zone,
  created_by integer,
  updated_by integer,
  CONSTRAINT post_quotas_pkey PRIMARY KEY (id)
);


CREATE TABLE public.search_logs
(
  id serial NOT NULL,
  keyword character varying(255),
  user_id character varying(255),
  created_at timestamp without time zone,
  updated_at timestamp without time zone,
  CONSTRAINT log_search_pkey PRIMARY KEY (id)
);

CREATE TABLE public.warehouse_search_logs
(
  id serial NOT NULL,
  keyword character varying(255),
  count integer,
  date date,
  created_at timestamp without time zone,
  updated_at timestamp without time zone,
  CONSTRAINT warehouse_search_logs_pkey PRIMARY KEY (id)
);

ALTER TABLE public.groups ADD COLUMN quota_limit integer;