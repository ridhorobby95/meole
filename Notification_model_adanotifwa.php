<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Notification_model extends AppModel {

    //STATUS
    const STATUS_NOT_SEND = 'N';
    const STATUS_SEND = 'S';
    //TYPE
    const TYPE_POST_DETAIL = 'PD';
    const TYPE_DAILY_REPORT_DETAIL = 'DD';
    const TYPE_ISSUE_DETAIL = 'ID';
    const TYPE_TODO_DETAIL = 'TD';
    const TYPE_GROUP_DETAIL = 'GD';
    //ACTION
    const ACTION_POST_CREATE = 'PC';
    const ACTION_COMMENT_CREATE = 'CC';
    const ACTION_GROUP_CREATE = 'GC';
    const ACTION_ISSUE_STATUS = 'IS';
    const ACTION_TICKET_ASSIGN = 'TA';
    const ACTION_STATUS_CHANGED = 'SC';

    protected $many_to_many = array(
        'Notification_sender' => array('Notification', 'User'),
    );
    protected $has_many = array(
        'Notification_user'
    );

    public function save($action = NULL, $message = NULL, $senderId, $senderGroupId, $receiverIds = array(), $type = NULL, $referenceId = NULL, $receiverGroupIds = NULL) {
        if (empty($receiverIds) && empty($receiverGroupIds))
            return TRUE;

        // $this->_database->trans_begin();
        $notification = $this->with(array('Notification_user', 'Notification_sender'))->get_by(array(
            'reference_id' => $referenceId,
            'action' => $action,
            'reference_type' => $type,
        ));
        // if (empty($notification)) {
            $data = array(
                'reference_id' => $referenceId,
                'message' => $message,
                'action' => $action,
                'reference_type' => $type,
                'user_id' => $senderId,
            );

            $save = $this->create($data);
        // } else {
        //     $save = $notification['id'];
        // }
        if ($save) {
            $senders = Util::toList($notification['notificationSenders'], 'id');
            $receivers = Util::toList($notification['notificationUsers'], 'userId');
            // $groupReceivers = Util::toList($notification['notificationUsers'], 'groupId');
            // echo "<pre>";
            // var_dump($save, $notification, $senders, $senderId, !in_array($senderId, $senders));
            // die();

            /*  IF dihilangkan sementara karena jadi 
             *  penyebab aplikasi tidak mendapat notification_senders pada saat 
             *  pembuatan notifikasi 
             */
            // if (!in_array($senderId, $senders)) {
                $this->load->model('Notification_sender_model');
                $insert = $this->Notification_sender_model->insert(array('notification_id' => $save, 'user_id' => $senderId,));
            // }
            $receiver = $updated = array();
            $this->load->model('Notification_user_model');
            foreach ($receiverIds as $receiverId) {
                // if (!in_array($receiverId, $receivers)) {
                    $receiver[] = array(
                        'user_id' => $receiverId,
                        'notification_id' => $save,
                        'status' => Notification_user_model::STATUS_NOTIFICATION,
                        'created_at' => Util::timeNow(),
                        'updated_at' => Util::timeNow()
                    );
                // } else {
                //     $updated[] = $receiverId;
                // }
            }

            foreach ($receiverGroupIds as $receiverGroupId) {
                // if (!in_array($receiverGroupId, $groupReceivers)) {
                    $receiver[] = array(
                        'group_id' => $receiverGroupId,
                        'notification_id' => $save,
                        'status' => Notification_user_model::STATUS_NOTIFICATION,
                        'created_at' => Util::timeNow(),
                        'updated_at' => Util::timeNow(),
                    );
                // } else {
                //     $updated[] = $receiverGroupId;
                // }
            }

            $ins = TRUE;
            if (!empty($receiver)) {
                $ins = $this->_database->insert_batch('notification_users', $receiver);
            }
            $upd = TRUE;
            if (!empty($updated)) {
                $upd = $this->Notification_user_model->update_by(array('user_id' => $updated, 'notification_id' => $save), array('status' => Notification_user_model::STATUS_NOTIFICATION, 'sent_at' => NULL, 'updated_at' => Util::timeNow()));
            }
                /*
            if ($ins && $upd) {
                $this->load->model('Device_model');
                foreach ($receiverIds as $key => $idRec) {
                    if ($idRec == $senderId) {
                        unset($receiverIds[$key]);
                    }
                }
                $devices = $this->Device_model->getDeviceUser($receiverIds);

                $notification = $this->get_many_by(array('notifications.id' => $save));
                if (!empty($notification)) {
                    $this->load->model('User_model');
                    $user = $this->User_model->get($senderId);
                    $messages = $this->generateMessage($notification);
                    $this->sendToGCM(Util::toList($devices, 'regId'), $messages[0]['message'], $user['name'], $user['photo']['thumb']['link'], $messages[0]['referenceType'], $messages[0]['referenceId'], $messages[0]['createdAt']);
                }
                // return $this->_database->trans_complete();
            }
                */
        }
        // $this->_database->trans_rollback();

        return FALSE;
    }

    public function getTypeByCategory($categoryId) {
        $this->load->model('Category_model');
        if (in_array($categoryId, array(Category_model::INFO, Category_model::EVENT, Category_model::KNOWLEDGE_MANAGEMENT))) {
            $type = self::TYPE_POST_DETAIL;
        } elseif (in_array($categoryId, array(Category_model::TASK, Category_model::BUG, Category_model::ENHANCEMENT, Category_model::PROPOSAL))) {
            $type = self::TYPE_ISSUE_DETAIL;
        } elseif ($categoryId == Category_model::DAILY_REPORT) {
            $type = self::TYPE_DAILY_REPORT_DETAIL;
        } elseif ($categoryId == Category_model::TODO) {
            $type = self::TYPE_TODO_DETAIL;
        }
        return $type;
    }

    public function generate2($action, $reference_id, $sender_id = NULL) {
        $this->load->model('User_model');
        switch ($action) {
            case self::ACTION_POST_CREATE:
                $this->load->model('Post_model');
                $post = $this->Post_model->getPost($sender_id, $reference_id);
                $type = self::TYPE_POST_DETAIL;
                $sender_id = $post['post_user_id'];
                $reference_id = $post['post_id'];
                switch ($post['post_type']) {
                    case Post_model::TYPE_GROUP:
                        $group_members = $this->Group_member_model->getAllMembers($post['group_id']);
                        $receiver_ids = Util::toList($group_members, 'id', array($sender_id));
                        break;
                    case Post_model::TYPE_TICKET:
                        $sql = "select user_id from post_assign where post_id=$reference_id";
                        $post_users = dbGetRows($sql);
                        $receiver_ids = Util::toList($post_users, 'user_id', array($sender_id));
                        break;
                }
                if (!empty($post['image'])) {
                    $message = 'memposting gambar ';
                } elseif (!empty($post['file'])) {
                    $message = 'memposting berkas "' . $post['file']['name'] . '"';
                } elseif (!empty($post['video'])) {
                    $message = 'memposting video "' . $post['video']['name'] . '"';
                } elseif (!empty($post['link'])) {
                    $message = 'memposting link "' . $post['link'] . '"';
                } elseif (!empty($post['post_description'])) {
                    if (strlen($post['post_description']) > 30) {
                        $str = substr($post['post_description'], 0, 30) . '...';
                    } else {
                        $str = $post['post_description'];
                    }
                    $message = 'memposting "' . $str . '"';
                }
                break;
            case self::ACTION_COMMENT_CREATE:
                $this->load->model('Comment_model');
                $comment = $this->Comment_model->get($reference_id);
                if(!empty($comment)){
                    $this->load->model('Post_model');
                    $comment['post'] = $this->Post_model->with(array('Group' => 'Group_member', 'Post_user'))->get_by("posts.id", $comment['postId']);
                }
                $sender_id = $comment['userId'];
                $reference_id = $comment['postId'];
                $type = $this->getTypeByCategory($comment['post']['categoryId']);
                switch ($comment['post']['type']) {
                    case Post_model::TYPE_PUBLIC:
                        $users = $this->User_model->getListFor($sender_id);
                        $receiver_ids = Util::toList($users, 'id');
                        break;
                    case Post_model::TYPE_GROUP:
                        $receiver_ids = Util::toList($comment['post']['group']['groupMembers'], 'userId', array($sender_id));
                        break;
                    case Post_model::TYPE_FRIEND:
                        $receiver_ids = Util::toList($comment['post']['postUsers'], 'id', array($sender_id));
                        if (!in_array($comment['post']['userId'], $receiver_ids)) {
                            $receiver_ids[] = $comment['post']['userId'];
                        }
                        break;
                    case Post_model::TYPE_TICKET:
                        $sql = "select user_id from post_assign where post_id=$referenceId";
                        $rows = dbGetRows($sql);
                        $temp1 = Util::toList($rows, 'user_id', array($sender_id));

                        $sql = "select distinct user_id from comments where post_id=$referenceId";
                        $rows = dbGetRows($sql);
                        $temp2 = Util::toList($rows, 'user_id', array($sender_id));

                        $sql = "select user_id from posts where id=$referenceId";
                        $rows = dbGetRows($sql);
                        $temp3 = Util::toList($rows, 'user_id', array($sender_id));

                        $receiver_ids = array_unique(array_merge($temp1, $temp2, $temp3));

                        break;
                }
                if (strlen($comment['text']) > 30) {
                    $str = substr($comment['text'], 0, 30) . '...';
                } else {
                    $str = $comment['text'];
                }
                $message = 'memberi tanggapan "' . $str . '"';
                break;
            case self::ACTION_GROUP_CREATE:
                $this->load->model('Group_model');
                $this->load->model('Group_member_model');
                $group = $this->Group_model->with('Group_member')->get($reference_id);
                $type = self::TYPE_GROUP_DETAIL;
                foreach ($group['groupMembers'] as $member) {
                    if ($member['groupMemberRole']['id'] == Group_member_model::ROLE_OUWNER) {
                        $sender_id = $member['user']['id'];
                    } else {
                        $receiver_ids[] = $member['user']['id'];
                    }
                }
                $message = 'menambahkan anda';
                break;
            case self::ACTION_TICKET_ASSIGN:
                $sql = "select user_id from post_assign where post_id=$reference_id";
                $post_users = dbGetRows($sql);
                $receiver_ids = Util::toList($post_users, 'user_id', array($sender_id));
                $type = self::ACTION_TICKET_ASSIGN;
                $message = 'menugaskan tiket';
                break;
        }
        // echo $action.'<br>'.$message.'<br>'.$sender_id.'<br>'.$type.'<br>'.$reference_id;
        // die();
        $this->save($action, $message, $sender_id, $receiver_ids, $type, $reference_id);
    }

    public function generate($action, $referenceId, $senderId = NULL, $group_id = NULL) {
        $this->load->model('User_model');
        switch ($action) {
            case self::ACTION_POST_CREATE:
                $this->load->model('Post_model');

                $post = $this->Post_model->with(array('Post_user'))->get_by('posts.id', $referenceId);
                $post['group'] = dbGetRows("select * from groups where id = ".$post['groupId']);

                $senderId = $post['userId'];
                $type = $this->getTypeByCategory($post['categoryId']);
                switch ($post['type']) {
                    case Post_model::TYPE_PUBLIC:
                        $users = $this->User_model->getListFor($senderId);
                        $receiverIds = Util::toList($users, 'id');
                        break;
                    case Post_model::TYPE_GROUP:
                        // $receiverIds = Util::toList($post['group']['groupMembers'], 'userId', array($senderId));
                        $groupReceiverIds[] = $post['groupId'];
                        break;
                    case Post_model::TYPE_FRIEND:
                        $receiverIds = Util::toList($post['postUsers'], 'id', array($senderId));
                        break;
                    case Post_model::TYPE_TICKET:
                        $sql = "select user_id from post_assign where post_id=$referenceId";
                        $postUsers = dbGetRows($sql);
                        $receiverIds = Util::toList($postUsers, 'user_id', array($senderId));
                        break;
                }
                if (!empty($post['image'])) {
                    $message = 'memposting gambar ';
                } elseif (!empty($post['file'])) {
                    $message = 'memposting berkas "' . $post['file']['name'] . '"';
                } elseif (!empty($post['video'])) {
                    $message = 'memposting video "' . $post['video']['name'] . '"';
                } elseif (!empty($post['link'])) {
                    $message = 'memposting link "' . $post['link'] . '"';
                } elseif (!empty($post['description'])) {
                    if (strlen($post['description']) > 30) {
                        $str = substr($post['description'], 0, 30) . '...';
                    } else {
                        $str = $post['description'];
                    }
                    $message = 'memposting "' . $str . '"';
                }
                if ($post['type']==Post_model::TYPE_TICKET){
                    $arrReciever = [
                        [
                            'phone_no' => '6282131024512',
                            'name' => 'Budi',
                        ],[
                            'phone_no' => '6281331207233',
                            'name' => 'Aku',
                        ],[
                            'phone_no' => '6289678791516',
                            'name' => 'Adi',
                        ]
                        ,[
                            'phone_no' => '6281297563586',
                            'name' => 'Franova',
                        ]
                    ];
                    $objName = dbGetOne("select name from users where id='".$senderId."'");

                    foreach ($arrReciever as $key => $value) {

                        $data = array(
                             "phone_no" => $value['phone_no'],
                             "key" => 'eea33bbc7ea40d5fb8f722a98cf4366ea144900fc3b84c03',
                             "message" => "to: ".$value['name']."\n\n ".$objName." ".$message."\n\n SIGAP 2019" 
                        );
                        $data_string = json_encode($data);
                        $url='https://164.68.108.212/api/send_message';
                        $ch = curl_init($url);
                        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST"); 
                        curl_setopt($ch, CURLOPT_POSTFIELDS, $data_string);
                        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
                        curl_setopt($ch, CURLOPT_VERBOSE, 0);
                        curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 0);
                        curl_setopt($ch, CURLOPT_TIMEOUT, 0);
                        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
                        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
                        curl_setopt($ch, CURLOPT_HTTPHEADER, array(
                         'Content-Type: application/json',
                         'Content-Length: ' . strlen($data_string)) 
                        ); 

                        echo $response = curl_exec($ch);
                        echo "\n";
                        $err = curl_error($ch);
                        curl_close($ch);
                    }
                }
                break;
            case self::ACTION_COMMENT_CREATE:
                $this->load->model('Comment_model');
                $comment = $this->Comment_model->get($referenceId);
                if(!empty($comment)){
                    $this->load->model('Post_model');
                    $comment['post'] = $this->Post_model->with(array('Group' => 'Group_member', 'Post_user'))->get_by("posts.id", $comment['postId']);
                }

                $senderId = $comment['userId'];
                $referenceId = $comment['postId'];
                $type = $this->getTypeByCategory($comment['post']['categoryId']);
                switch ($comment['post']['type']) {
                    // case Post_model::TYPE_PUBLIC:
                    //     $users = $this->User_model->getListFor($senderId);
                    //     $receiverIds = Util::toList($users, 'id');
                    //     break;
                    // case Post_model::TYPE_GROUP:
                    //     $receiverIds = Util::toList($comment['post']['group']['groupMembers'], 'userId', array($senderId));
                    //     break;
                    // case Post_model::TYPE_FRIEND:
                    //     $receiverIds = Util::toList($comment['post']['postUsers'], 'id', array($senderId));
                    //     if (!in_array($comment['post']['userId'], $receiverIds)) {
                    //         $receiverIds[] = $comment['post']['userId'];
                    //     }
                    //     break;
                    case Post_model::TYPE_TICKET:
                        $sql = "select user_id from post_assign where post_id=$referenceId";
                        $rows = dbGetRows($sql);
                        $temp1 = Util::toList($rows, 'user_id', array($senderId));

                        $sql = "select distinct user_id from comments where post_id=$referenceId";
                        $rows = dbGetRows($sql);
                        $temp2 = Util::toList($rows, 'user_id', array($senderId));

                        $sql = "select user_id from posts where id=$referenceId";
                        $rows = dbGetRows($sql);
                        $temp3 = Util::toList($rows, 'user_id', array($senderId));

                        $receiverIds = array_unique(array_merge($temp1, $temp2, $temp3));

                        break;
                    default:
                        $sql = "select distinct user_id from comments where post_id=$referenceId";
                        $rows = dbGetRows($sql);
                        $receiverIds = Util::toList($rows, 'user_id', array($senderId));

                        if (!in_array($comment['post']['userId'], $receiverIds)) {
                            $receiverIds[] = $comment['post']['userId'];
                        }

                        break;
                }

                if (strlen($comment['text']) > 30) {
                    $str = substr($comment['text'], 0, 30) . '...';
                } else {
                    $str = $comment['text'];
                }
                $message = 'memberi tanggapan "' . $str . '"';
                break;
            case self::ACTION_GROUP_CREATE:
                $this->load->model('Group_model');
                $this->load->model('Group_member_model');
                $group = $this->Group_model->with('Group_member')->get($referenceId);
                $type = self::TYPE_GROUP_DETAIL;
                foreach ($group['groupMembers'] as $member) {
                    if ($member['groupMemberRole']['id'] == Group_member_model::ROLE_OUWNER) {
                        $senderId = $member['user']['id'];
                    } else {
                        $receiverIds[] = $member['user']['id'];
                    }
                }
                $message = 'menambahkan anda';
                break;
	    case self::ACTION_ISSUE_STATUS:
    		$this->load->model('Issue_model');
    		$issue = $this->Issue_model->with(array('Post_user'))->get_by('posts.id', $referenceId);
    		$status = Issue_model::getStatus();
    		$message = 'Mengganti status issue "'.$issue['title'].'" menjadi '.$status[$issue['status']];
    		
    		$receiverIds = Util::toList($issue['postUsers'], 'id', array($senderId));
    		if($senderId != $issue['userId'])
    		  $receiverIds[] = $issue['userId'];
    		
    		$type = self::TYPE_ISSUE_DETAIL;
    		break;
        case self::ACTION_TICKET_ASSIGN:

            $sql = "select user_id from post_assign where post_id=$referenceId";
            $postUsers = dbGetRows($sql);
            $receiverIds = Util::toList($postUsers, 'user_id', array($senderId));
            $type = self::ACTION_TICKET_ASSIGN;
            $message = 'menugaskan tiket';
            break;
        case self::ACTION_STATUS_CHANGED:
            $this->load->model('Post_model');
            $sql = "select user_id from post_assign where post_id=$post_id";
            $post_assigns = dbGetRows($sql);
            $receiver_ids = Util::toList($post_assigns, 'user_id', array($sender_id));
            $type = "ganti status";
            $sql = "select p.id, p.status from posts p where p.id=$post_id";
            $post = dbGetRow($sql);
            if ($post['status']=='B') {
                $status = "Terbuka";
            } else if ($post['status']=='P'){
                $status = "Progress";
            } else {
                $status = "Selesai";
            }
            $message = "mengganti status tiket menjadi $status";
            break;
        }
        // echo "<pre>";
        // var_dump($message, $senderId, $receiverIds);
        // die();
        // die("a");
//file_put_contents('e:/log.txt', "\r\n action: " . $action . ' message: ' . $message . ' senderId: ' . $senderId . ' type: ' . $type . ' receivers: ' . json_encode($receiverIds) . ' reference: ' . $referenceId, FILE_APPEND);
        $this->save($action, $message, $senderId, $group_id, $receiverIds, $type, $referenceId, $groupReceiverIds);
    }

    protected function join_or_where($row) {
        $this->_database->select("notifications.*, notification_senders.group_id, groups.name as group_name, groups.image as group_photo, notification_users.status");
        $this->_database->select("notifications.*, notification_users.status");
        $this->_database->join("notification_users", "notification_users.notification_id = notifications.id");
        $this->_database->join("notification_senders", "notification_senders.notification_id = notifications.id", "left");
        $this->_database->join("groups", "notification_senders.group_id = groups.id", "left");
        $this->with('Notification_sender');
        
        $this->order_by("case notification_users.status when 'N' then 1 when 'U' then 2 else 3 end");
        $this->order_by("notifications.id ","DESC");
    }

    public function getMe($userId, $page = 0) {
        $condition = array('notification_users.user_id' => $userId);
        $this->limit(20, ($page * 20));
        $notifications = $this->get_many_by($condition);

        $ouput = $this->generateMessage($notifications);
        return $ouput;
    }

    public static function toObject($id, $message, $sender, $referenceId, $referenceType, $status, $createdAt, $updatedAt) {
        return array(
            'id' => (int) $id,
            'message' => $message,
            'sender' => $sender,
            'referenceId' => (int) $referenceId,
            'referenceType' => $referenceType,
            'status' => $status,
            'createdAt' => $createdAt,
            'updatedAt' => $updatedAt
        );
    }

    public function getNotification($userId) {
        $this->load->model('Notification_user_model');
        $this->_database->select("notifications.*, notification_users.status");
        $this->_database->join("notification_users", "notification_users.notification_id = notifications.id");
        return $this->order_by("case notification_users.status when 'N' then 1 when 'U' then 2 else 3 end","notifications.created_at DESC")->get_many_by(array('notification_users.user_id' => $userId, 'notification_users.status' => Notification_user_model::STATUS_NOTIFICATION));
    }

    public function generateMessage($notifications) {
        $types = array();

        foreach ($notifications as $notification) {
            $types[$notification['referenceType']][] = $notification['referenceId'];
        }
        $data = array();
        foreach ($types as $key => $value) {
            switch ($key) {
                case self::TYPE_POST_DETAIL:
                    $this->load->model('Post_model');
                    $data[$key] = Util::toMapObject($this->Post_model->with(array('Group', 'Post_user', 'User'))->get_many_by(array('posts.id' => $value)), 'id');
                    break;
                case self::TYPE_GROUP_DETAIL:
                    $this->load->model('Group_model');
                    $data[$key] = Util::toMapObject($this->Group_model->get_many_by(array('id' => $value)), 'id');
                    break;
            }
        }
        $ouput = array();

        foreach ($notifications as $notification) {
            $message = NULL;
            
            if (!empty($notification['message']) && empty($notification['action'])) {
                $message = $notification['message'];
            } elseif (!empty($notification['action']) && !empty($notification['message'])) {
                switch ($notification['action']) {
                    case self::ACTION_POST_CREATE:
                        $post = $data[self::TYPE_POST_DETAIL][$notification['referenceId']];
                        $helpdesk_or_group = "grup";
                        if ($post['type']=="T") {
                            $helpdesk_or_group = "helpdesk";
                        }
                        if (!empty($post['group'])) {
                            $message = $notification['message'] . " di $helpdesk_or_group " . $post['group']['name'];
                        } elseif (!empty($post['postUsers'])) {
                            $message = $notification['message'] . ' ke anda';
                            if ((count($post['postUsers']) == 2)) {
                                if ($post['postUsers'][0]['id'] == $userId) {
                                    $otherName = $post['postUsers'][1]['name'];
                                } else {
                                    $otherName = $post['postUsers'][0]['name'];
                                }
                                $message .= ' dan ' . $otherName;
                            } elseif (count($post['postUsers']) > 2) {
                                $message .= ' dan ' . (count($post['postUsers']) - 1) . ' orang lainnya';
                            }
                        } else {
                            $message = $notification['message'];
                        }
                        break;
                    case self::ACTION_COMMENT_CREATE:
                        $post = $data[self::TYPE_POST_DETAIL][$notification['referenceId']];
                        $helpdesk_or_group = "grup";
                        if ($post['type']=="T") {
                            $helpdesk_or_group = "helpdesk";
                        }
                        $senderCount = count($notification['notificationSenders']);
                        if ($senderCount == 1) {
                            $message = $notification['message'];
                        } elseif ($senderCount == 2) {
                            $message = "dan " . $notification['notificationSenders'][0]["name"];
                        } elseif ($senderCount > 2) {
                            $message = "dan " . ($senderCount - 1) . " orang lainnya";
                        }
                        if(strpos($message, 'mengomentari') == false){
			                 $message .= ' mengomentari';
                        }
                        $message .= ' pada postingan ' . $post['user']['name'];
                        if (!empty($post['group'])) {
                            $message .=" di $helpdesk_or_group " . $post['group']['name'];
                        }
                        // var_dump($notification, $message);
                        // die();
                        break;
                    case self::ACTION_GROUP_CREATE:
                        $group = $data[self::TYPE_GROUP_DETAIL][$notification['referenceId']];
                        $helpdesk_or_group = "grup";
                        if ($group['type']=="H") {
                            $helpdesk_or_group = "helpdesk";
                        }
                        $message = $notification['message'] . " di $helpdesk_or_group " . $group['name'];
                        break;
                    default:
        			     $message = $notification['message'];
        			break;
                }
            }

            if($notification['groupId']){
                $notification['notificationSenders'][0]['group'] = array(
                    'group_id' => $notification['groupId'],
                    'name' => $notification['groupName'],
                    'username' => $notification['groupName'],
                    'photo' => $notification['groupPhoto'],
                );   
            }

            $ouput[] = self::toObject($notification['id'], $message, end($notification['notificationSenders']), $notification['referenceId'], $notification['referenceType'], $notification['status'], $notification['createdAt'], $notification['updatedAt']);
        }

        return $ouput;
    }

    function sendToGCM($recepient, $message, $title, $link, $action, $actionId, $time = NULL) {
        $this->gcm->setMessage($message);

        if (is_string($recepient)) {
            $this->gcm->addRecepient($recepient);
        } else {
            foreach ($recepient as $client) {
                $this->gcm->addRecepient($client);
            }
        }

        $this->gcm->setData(array(
            'title' => $title,
            'link' => $link,
            'action' => $action,
            'actionId' => $actionId,
            'time' => empty($time) ? Util::timeNow() : $time
        ));

        $this->gcm->send();
    }

    function mobileNotification($action, $post_id, $sender_id = NULL, $notification_id = NULL, $group_id = NULL) {
        $this->load->model('User_model');
        $title = 'Sigap';
        $message = 'Notifikasi untuk anda';
        $num_notifikasi = rand(0,10);
        switch ($action) {
            case self::ACTION_POST_CREATE:
                $type = "post created";
                $this->load->model('Post_model');
                $post = $this->Post_model->with(array('Group' => 'Group_member', 'Post_user'))->get_by('posts.id', $post_id);
                $post_type = $post['type'];
                $sender_id = $post['userId'];
                switch ($post['type']) {
                    case Post_model::TYPE_GROUP:
                        $receiver_ids = Util::toList($post['group']['groupMembers'], 'userId', array($sender_id));
                        break;
                    case Post_model::TYPE_TICKET:
                        $sql = "select user_id from post_assign where post_id=$post_id";
                        $post_assigns = dbGetRows($sql);
                        $receiver_ids = Util::toList($post_assigns, 'user_id', array($sender_id));
                        break;
                }
                if (!empty($post['image'])) {
                    $message = 'memposting gambar ';
                } elseif (!empty($post['file'])) {
                    $message = 'memposting berkas "' . $post['file']['name'] . '"';
                } elseif (!empty($post['video'])) {
                    $message = 'memposting video "' . $post['video']['name'] . '"';
                } elseif (!empty($post['link'])) {
                    $message = 'memposting link "' . $post['link'] . '"';
                } elseif (!empty($post['description'])) {
                    if (strlen($post['description']) > 30) {
                        $str = substr($post['description'], 0, 30) . '...';
                    } else {
                        $str = $post['description'];
                    }
                    $message = 'memposting "' . $str . '"';
                }
                break;
            case self::ACTION_COMMENT_CREATE:
                $type = "comment created";
                $this->load->model('Comment_model');
                $this->load->model('Post_model');
                $comment = $this->Comment_model->get($post_id);
                if(!empty($comment)){
                    $comment['post'] = $this->Post_model->with(array('Group' => 'Group_member', 'Post_user'))->get_by("posts.id", $comment['postId']);
                }
                $sender_id = $comment['userId'];
                $post_id = $comment['postId'];
                $post_type = $comment['post']['type'];
                switch ($comment['post']['type']) {
                    case Post_model::TYPE_GROUP:
                        $receiver_ids = Util::toList($comment['post']['group']['groupMembers'], 'userId', array($sender_id));
                        break;
                    case Post_model::TYPE_TICKET:
                        $sql = "select user_id from post_assign where post_id=$post_id";
                        $rows = dbGetRows($sql);
                        $temp1 = Util::toList($rows, 'user_id', array($sender_id));

                        $sql = "select distinct user_id from comments where post_id=$post_id";
                        $rows = dbGetRows($sql);
                        $temp2 = Util::toList($rows, 'user_id', array($sender_id));

                        $sql = "select user_id from posts where id=$post_id";
                        $rows = dbGetRows($sql);
                        $temp3 = Util::toList($rows, 'user_id', array($sender_id));

                        $receiver_ids = array_unique(array_merge($temp1, $temp2, $temp3));

                        break;
                }
                if (strlen($comment['text']) > 30) {
                    $str = substr($comment['text'], 0, 30) . '...';
                } else {
                    $str = $comment['text'];
                }
                $message = 'memberi tanggapan "' . $str . '"';
                break;
            case self::ACTION_TICKET_ASSIGN:
                $sql = "select user_id from post_assign where post_id=$post_id";
                $post_assigns = dbGetRows($sql);
                $receiver_ids = Util::toList($post_assigns, 'user_id', array($sender_id));
                $type = "penugasan";
                $post_type = 'T';
                $message = 'menugaskan tiket';
                break;
            case self::ACTION_STATUS_CHANGED:
                $this->load->model('Post_model');
                $sql = "select user_id from post_assign where post_id=$post_id";
                $post_assigns = dbGetRows($sql);
                $receiver_ids = Util::toList($post_assigns, 'user_id', array($sender_id));
                $type = "ganti status";
                $sql = "select p.id, p.status from posts p where p.id=$post_id";
                $post = dbGetRow($sql);
                $post_type = 'T';
                if ($post['status']=='B') {
                    $status = "Terbuka";
                } else if ($post['status']=='P'){
                    $status = "Progress";
                } else {
                    $status = "Selesai";
                }
                $message = "mengganti status tiket menjadi $status";
                break;
        }
        foreach ($receiver_ids as $key => $value) {
            $receiver_ids[$key] = "'".$value."'";
        }

        $this->load->model("Notification_user_model");

        $upd = TRUE;
        if($notification_id){
            $upd = $this->Notification_user_model->update_by(
                array('notification_id' => $notification_id), 
                array(  'status' => Notification_user_model::STATUS_NOTIFICATION, 
                        'updated_at' => Util::timeNow())
            );
        }

        if($upd){
            $str_iduser = implode(',', $receiver_ids);
            if (!$str_iduser)
                return false;
            // $registration_ids = array();
            // $user_ids = array();
            // $sql = "select d.id as device_id, ut.user_id, d.reg_id as registration_id from user_tokens ut left join devices d on d.id=ut.device_id where ut.user_id in ($str_iduser) order by ut.id desc";
            // $rows = dbGetRows($sql);
            // foreach ($rows as $row) {
            //     $user_ids[$row['user_id']] = $row['user_id'];
            //     $registration_ids[] = $row['registration_id'];
            // }

            // if (!count($registration_ids))
            //     return false;

            // $str_iduser = implode(',', $user_ids);

            $ci = &get_instance();
            if (!$this->config->item('fcm_allow_notification'))
                return false;
            $url = $this->config->item('fcm_url');
            $google_api_key = $this->config->item('fcm_api_key');
            // $fields = array(
            //                 'str_iduser'=>$str_iduser,
            //                 'post_id'=>$post_id,
            //                 'type'=>$type,
            //                 'title'=>$title,
            //                 'message'=>$message,
            //                 );

            // $url = site_url('web/publ1c/sendMobileNotification');
            // $url = str_replace('https', 'http', $url);
            // $this->curl($url,$fields);
             $sql = "select d.id as device_id, ut.user_id, d.manufacture, d.reg_id as registration_id from user_tokens ut left join devices d on d.id=ut.device_id where ut.user_id in ($str_iduser) and ut.status='I' order by ut.id desc";
            $rows = dbGetRows($sql);
            $sql = "select name from users u where u.id='$sender_id'";
            $users = dbGetRow($sql);
            $message = $users['name'].' '.$message;
            foreach ($rows as $row) {
                $user_id = $row['user_id'];
                // $num_notifikasi = $users[$iduser]['num_notifikasi'];

                // $sql = "select sum(num) from userbadge where iduser=$iduser";

                $registration_id = $row['registration_id'];

                if ($row['manufacture'] == 'apple') {
                    $fields = array(
                        'to' => $registration_id,
                        'data' => array('id'=>$id, 'type'=>$type),
                        'notification' => array(
                            'title' => $title, 
                            'message' => $message, 
                            'sound'=>'default',
                            'post_id' => $post_id,
                            'post_type' => $post_type,
                            'click_action' => 'NOTIFIKASI'
                        )
                    );
                }
                else {
                    $fields = array(
                        'to' => $registration_id,
                        'data' => array(
                            'title' => $title, 
                            'message' => $message, 
                            'post_id'=>$post_id, 
                            'type'=>$type,
                            'post_type' => $post_type,
                            'click_action' => 'id.co.integra.sigapmobile.activities.ActivityDetailPost'
                        // )
                        ),
                        'notification' => array(
                            'title' => $title, 
                            'body' => $message, 
                            'sound'=>'default',
                            'vibrate' => 'true',
                            'click_action' => 'id.co.integra.sigapmobile.activities.ActivityDetailPost'
                        )
                    );                
                }
                $headers = array(
                      'Authorization:key='.$google_api_key,
                      'Content-Type: application/json'
                 );
                // echo $url;
                // echo '<pre>';
                // var_dump($headers);
                // echo '<pre>';
                // var_dump($fields);
                // die();
                $ch = curl_init();
                curl_setopt($ch, CURLOPT_URL, $url);
                curl_setopt($ch, CURLOPT_POST, true);
                curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
                curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
                curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
                curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($fields));

                $result = curl_exec($ch);
                //if($result === false)    die('Curl failed ' . curl_error());
                curl_close($ch);
            }
            return true;
        }
    }
}
